<?php
require_once('../base.inc');
require_once('../classes/user.inc');
require_once('../classes/common.inc');
require_once('../classes/region.inc');
require_once('../classes/yahoo.inc');
require_once('../classes/google.inc');
require_once('../classes/used_freemiles.inc');
if (CConfig::RUN_IN_FB)
    require_once('../classes/facebook.inc');

// Validate any current session
CWebSession::init();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
if (CConfig::RUN_IN_FB)
    $isValidSession = $login->requireSession();
else
    $isValidSession = $login->isValidSession();
CWebSession::set('login', serialize($login));
$user = new CUser();
/*  */

 $page = $_POST['page'];

        // get how many rows we want to have into the grid - rowNum parameter in the grid
        $limit =$_REQUEST['rp'];

        // get index row - i.e. user click to sort. At first time sortname parameter -
        // after that the index from colModel
         $sidx =$_REQUEST['sortname'];

        // sorting order - at first time sortorder
            $sord =$_REQUEST['sortorder'];

        // if we not pass at first time index use the first column for the index or what you want
        if(!$sidx) $sidx =1;
          
        $search_box='';
        // calculate the number of rows for the query. We need this for paging the result
        if(isset($_REQUEST['type']) && $_REQUEST['type']==1) {
           
           if($_REQUEST['email']!='')
              $email=$_REQUEST['emal']; 
           if($_REQUEST['name']!='')
              $name=$_REQUEST['name']; 
            $row_num=$user->registered_details_rows('',$email,$name);
         
          
            $row=$row_num;
          
        }else {
            $row_num=$user->registered_details_rows('All','','');
         
           
            $row=$row_num;
          
        }
        $count = $row;

        // calculate the total pages for the query
        if( $count > 0 ) {
            $total_pages = ceil($count/$limit);
        } else {
            $total_pages = 0;
        }


        // if for some reasons the requested page is greater than the total
        // set the requested page to total page
        if ($page > $total_pages) $page=$total_pages;

        // calculate the starting position of the rows
        $start = $limit*$page - $limit;

        // if for some reasons start position is negative set it to 0
        // typical case is that the user type 0 for the requested page
        if($start <0) $start = 0;

        // the actual query for the grid data
        $limit_range = $start.",".$limit;
        $sort_range = $sidx." ".$sord;
       
       if(isset($_REQUEST['type']) && $_REQUEST['type']==1) {

           
          
           if($_REQUEST['email']!='')
              $email=$_REQUEST['email']; 
            if($_REQUEST['name']!='')
              $name=$_REQUEST['name']; 
            $registered_details=$user->registered_details($sort_range,$limit_range,'',$email,$name);
          
           
        }else {
            $registered_details=$user->registered_details($sort_range,$limit_range,'All','','');
           

        }

/*if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
              header("Content-type: application/xhtml+xml;charset=utf-8");
} else {
          header("Content-type: text/xml;charset=utf-8");
}*/
$y=1;
if($start==0)
$start=1;
else
$start=$start+1;
echo "<?xml version='1.0' encoding='utf-8'?>";
if($count==0)
{
echo "<rows>";
echo "<row>";
echo "<cell></cell>";
echo "<cell></cell>";
echo "<cell>No Record Found</cell>";
echo "<cell></cell>";
echo "<cell></cell>";
echo "<cell></cell>";
echo "<cell></cell>";
echo "</row>";
echo "</rows>";
}
else
{
echo "<rows>";
echo "<page>".$page."</page>";
echo "<total>".$count."</total>";
echo "<records>".$count."</records>";

foreach($registered_details as $userlist)  {
$user_id=$userlist['id'];
$used_free_miles = new CUsedFreeMiles();
$out_free_miles=$used_free_miles->assignedUserFreeMiles($user_id);
$free = floor($out_free_miles['sum_miles']);
echo "<row id='".$user_id."'>";
echo "<cell><![CDATA[".$start."]]></cell>";
echo "<cell><![CDATA[".$userlist['first_name']."]]></cell>";
echo "<cell><![CDATA[".$userlist['last_name']."]]></cell>";
echo "<cell><![CDATA[".$userlist['email']."]]></cell>";
echo "<cell><![CDATA[".$userlist['mobile']."]]></cell>";
echo "<cell><![CDATA[".$free."]]></cell>";
 $permission="<cell><a onclick=\"assignMiles($user_id,$free)\">Add Free miles</a></cell>";
 echo "<cell>".htmlentities($permission)."</cell>";

echo "</row>";
$start++;
}
echo "</rows>";
}
exit;

/* */



?>
