<?php
//ini_set('display_errors', 1);
require_once('../base.inc');
require_once('../classes/user.inc');
require_once('../classes/common.inc');
require_once('../classes/region.inc');
require_once('../classes/yahoo.inc');
require_once('../classes/google.inc');
require_once('../classes/defaultmiles.inc');
require_once('../classes/user_freemiles.inc');
if (CConfig::RUN_IN_FB)
    require_once('../classes/facebook.inc');

// Validate any current session
CWebSession::init();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
if(!isset($_SESSION['id']))
    header('location: index.php');
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
if (CConfig::RUN_IN_FB)
    $isValidSession = $login->requireSession();
else
    $isValidSession = $login->isValidSession();
CWebSession::set('login', serialize($login));
$user = new CUser();

// Establish language context
$region = new CRegion('register');
$default_miles = new CDefaultMiles();
$checkgroups=$default_miles->displayDefaultMiles();

if(CCommon::getRequestValue('save_button')==1)
{
  
  $out=$default_miles->editDefaultMiles(CCommon::getRequestValue('free_distance'),CCommon::getRequestValue('free_miles'));
 // print_r($out);echo json_encode($out);
  die(json_encode($out));
}
if($_POST['update_button']==1)
{
   //echo CCommon::getRequestValue('user_id')."==".$_SESSION['id'];exit;
    $userFreemiles = new CUserFreeMiles();
    $userFreemiles->set(CUserFreeMiles::USERID, CCommon::getRequestValue('user_id'));
    $userFreemiles->set(CUserFreeMiles::ALLOCATEDBY, $_SESSION['id']);
    $userFreemiles->set(CUserFreeMiles::USERTYPE, 1);
    $userFreemiles->set(CUserFreeMiles::MILES, CCommon::getRequestValue('assign_free_miles'));
    $userFreemiles->set(CUserFreeMiles::CREATEDBY, 'now()');

    if ($userFreemiles->save() == false) {
      $val=1;
    }
 else {
        $val=0;
    }
    echo $val;
    exit;
}
// Load any user passed as a parameter
$groups = 0;
$userId = ($isValidSession ? (int) $login->userId() : 0);
$remaining_groups = '0';
if ($userId) {
    $user->loadWithUserId($userId);
    $existing_groups = CUser::getExistingGroupsOfUser($userId);
    $groups = json_encode($existing_groups);
    $email_domain = substr(strrchr($user->get(CUser::EMAIL), "@"), 1);
    $domain_groups = CUser::getMatchingGroups($email_domain);
    $domain_matching_groups = array();
    $domain_matching_groups['group_id'] = array_values(array_diff($domain_groups['group_id'], $existing_groups['group_id']));
    $domain_matching_groups['group_tag'] = array_values(array_diff($domain_groups['group_tag'], $existing_groups['group_tag']));
    if (count($domain_matching_groups['group_id']) > 0)
        $remaining_groups = json_encode($domain_matching_groups);
    //$remaining_groups = json_encode($domain_groups);
    $group_exists = $user->loadWithGroupUserIdList($userId);
}
// Get posted data

$op = CCommon::getRequestValue('op');
$refer = new CUrl(CCommon::getRequestValue('refer'));

$session_id = ($isValidSession ? $user->get(CUser::ID) : session_id());
$oldmask = umask(0);
if (!file_exists('upload/' . $session_id))
    mkdir('upload/' . $session_id, 0777);
umask($oldmask);
// XmlHttpRequest:- Register
if ($op == 'register') {
    $mailStatus = CCommon::getRequestValue('mailStatus');
    $newgroup = json_decode(CCommon::getRequestValue('newGroup'));

    $domain_groups = CCommon::getRequestValue('domainGroups');
    $searched_groups = CCommon::getRequestValue('searchedGroups');
    /*  $captcha_code = CCommon::getRequestValue('captcha_code');
      if($captcha_code){

      echo $_SESSION['captcha_code'].'-->  '.$captcha_code;
      if(empty($captcha_code ) || strcasecmp($_SESSION['captcha_code'], $captcha_code) != 0){
      $msg="<span style='color:red'>The Validation code does not match!</span>";// Captcha verification is incorrect.
      }else{// Captcha verification is Correct. Final Code Execute here!
      $msg="<span style='color:green'>The Validation code has been matched.</span>";
      }
      echo $msg;


      die("==>");
      } */
    //  echo 'sdasd';
    //die("llll");
    $out = register($login, $user, $region, $isValidSession, $session_id, $newgroup, $domain_groups, $searched_groups, $mailStatus);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
if ($op == 'deleteMemebers') {
    if (!$isValidSession) {
        $out = array('infoText' => $region->msg(1049));
        CCommon::xhrSend(CCommon::toJson($out));
        exit;
    }
    $searched_group_members = CCommon::getRequestValue('searched_group_members');
    $groupId = CCommon::getRequestValue('selected_group_id');
    $delete_status = CUserGroup::RemoveGroupMemebrsByGroupAdmin($searched_group_members);
    if ($delete_status) {
        $searched_group_members_array = explode(",", $searched_group_members);
        foreach ($searched_group_members_array as $group_members) {
            $userdetail = CUserGroup::getNameById($group_members);
            $GRoupNames = CGroup::getGroupNamesById($groupId);
            CUserGroup::removeMembermailbody($userdetail, $GRoupNames[0]);
        }
        $out = array('infoText' => $region->msg(1037));
        CCommon::xhrSend(CCommon::toJson($out));
    }
    exit;
}
if ($op == 'deleteGroup') {
    //ini_set('display_errors',1);
    if (!$isValidSession) {
        $out = array('infoText' => $region->msg(1049));
        CCommon::xhrSend(CCommon::toJson($out));
        exit;
    }
    $groupId = CCommon::getRequestValue('selected_group_id');
    $GRoupNames = CGroup::getGroupNamesById($groupId);


    if ($GRoupNames) {

        $userMailIds = CUserGroup::getallMemebersByGroupId($groupId);
        $delete_group = CGroup::RemoveGroupByGroupId($groupId);
        if ($userMailIds) {
            $delete_members = CUserGroup::RemoveGroupMemebrsByGroupId($groupId);
        }
        if ($delete_members) {
            $mailds = explode(",", $userMailIds);
            foreach ($mailds as $userMailId) {
                $remove_group_mail = CUserGroup::removeGroupmailbody($userMailId, $GRoupNames[0]);
            }
        }
        if ($delete_group) {
            $out = array('infoText' => $region->msg(1044));
        }
    } else {
        $out = array('infoText' => $region->msg(1047));
    }
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}

if ($op == 'chanagegroupownership') {
    if (!$isValidSession) {
        $out = array('infoText' => $region->msg(1049));
        CCommon::xhrSend(CCommon::toJson($out));
        exit;
    }
    $searched_group_member_for_admin = CCommon::getRequestValue('searched_group_member_for_admin');
    $groupId = CCommon::getRequestValue('selected_group_id');
    $userdetail = CUserGroup::getNameById($searched_group_member_for_admin);
    $GRoupNames = CGroup::getGroupNamesById($groupId);
    $encryotedurl = CCommon::mc_encrypt($groupId . "," . $userId . "," . $searched_group_member_for_admin, CConfig::ENCRYPTION_KEY);
    $encryotedurl = urlencode($encryotedurl);
    $mail_status = CUserGroup::changeMemberToAdminmailbody($userdetail, $GRoupNames[0], $encryotedurl);
    if ($mail_status) {
        $out = array('infoText' => $region->msg(1042));
        CCommon::xhrSend(CCommon::toJson($out));
    } else {
        $out = array('infoText' => $region->msg(1045));
        CCommon::xhrSend(CCommon::toJson($out));
    }
    exit;
}

if ($op == "validateGroup") {
    $group_tag = CCommon::getRequestValue('group_tag');
    validateNewGroup($group_tag, $region);
}
// XmlHttpRequest:- Register
if ($op == 'request_phone_code') {
    $out = request_phone_code($login, $user, $region, $isValidSession);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
if ($op == 'send_co_savings') {
    //$userId=179;
   
    $u_name=$user->get(CUser::FIRST_NAME)." ".$user->get(CUser::LAST_NAME);
    $out = $user->loadWithGroupUserIdPdf($userId);
    $out_nUsersScheme_taken = $user->nUsersScheme($userId);
    $out_numberUsersScheme = $user->numberUsersScheme($userId);
    $out_numberMilesShared = $user->numberMilesShared($userId);
    $out_kg_C0_saved = $user->kg_C0_saved($userId);
    $files = array();
    $first_date = date('d/m/Y', strtotime('first day of last month'));
    $last_date = date('d/m/Y', strtotime('last day of last month'));
    $month = date('F , Y', strtotime('first day of last month'));
    $month = date('F , Y', strtotime('first day of last month'));
    $footermonth=date("F  Y");
    $footermonth_prev=date("F  Y", strtotime('first day of last month'));
    $r=0;
    foreach ($out as $lists) {
        $name = $lists['name'];
        graphCreation($name, $lists);
        $no_miles_shared=0;
        if(isset($out_numberMilesShared[$r]['no_miles_shared']))
            $no_miles_shared=$out_numberMilesShared[$r]['no_miles_shared'];
        $no_total_miles=0;
        if(isset($out_kg_C0_saved[$r]['total_miles']))
            $no_total_miles=$out_kg_C0_saved[$r]['total_miles'];
        $html = "";
        // sleep(10);
        $html .= "
<html class='wf-myriadpro-n7-active wf-active'>
<head>
<link rel='stylesheet' href='https://use.typekit.net/c/defd9c/1w;myriad-pro,7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191,ftk:W:n7/d?3bb2a6e53c9684ffdc9a9aff1b5b2a6276adde0ff798449a3bfbdbe0b2e5780037c0af0a3201003e9db4c5b07b439a79071d32ae21e6518d79bc000c62be4549de1184af99fa2089e330ddc2e638861566cf8173a4dfc73fe168e3' media='all'>
<link rel='stylesheet' href='https://www.google.com/fonts#UsePlace:use/Collection:Open+Sans:400,600,700' media='all'>
<style>
body{
font-family:'myriad-pro','Open Sans',sans-serif;
}
.tk-myriad-pro{font-family:'myriad-pro',sans-serif;}
.outer {
font-family:'myriad-pro','Open Sans',sans-serif;
background-image: url('certificate.jpg');
height: 612px;
width:792px;
 text-align: center;
margin: 0 auto;
}
h1,h4,p{margin:0px;}
.heading h4{
font-size:20px;
height: 22px;
font-weight: normal;
 padding-top: 20px;
}
.heading{
padding-top: 122px;
text-align:center;
}
.group-heading h1
{
font-size:32px;
height: 77px;
 width: 554px;
  padding-top: 5px;
font-weight: bold;
color: #00523d;
text-transform: capitalize;
}
.group-heading
{

padding-left: 110px;
color:00523d;
text-align:center;
}
.group-content p{
 /*color: #484747;*/
 color:#000;
   height: 72px;
   font-size:20px;

font-weight: normal;
  padding-left: 150px;
    text-align: center;
    width: 500px;
}
.content{
width: 667px;
}
.box-content
{
  padding-left: 68px;
    width: 700px;
}
.footer
{
font-size:12px;
font-weight: normal;
}
.box-1
{
   background-color: #57b175;
    border-radius: 10px;
    float: left;
    height: 163px;
 font-size:12px;

font-weight: normal;
    width: 305px;
}
.box-3
{
  
    float: left;
    height: 163px;
 
    width: 50px;
}
.box-2
{
   background-color: #57b175;
    border-radius: 10px;
    float: left;
    height: 163px;
    width: 300px;
}
img {
    max-width: 100%;
    max-height: 100%;
}
 
   @page {
      size: A3;   /* auto is the initial value */
      margin: 10%;
   }
  
</style>
</head>
<body>
<div class='outer'>
<div class='heading'><h4>First Thumb Limited Certifies that</h4></div>
<div class='content'>
<div class='group-heading'><h1>".$name."</h1></div>
<div class='group-content'><p>has made reductions in carbon savings by promoting First Thumb's business travel sharing service.</p></div>
</div>
<div class='box-content'>
<div class='box-1'>
<div style='color:#fff;text-align:left;padding-left:15px;'>
<div style='margin-top:10px;line-height:18px;'><span style='color:#ffff4d;padding-right:10px;'>Period 1 </span>".$month."</div>
<div style='line-height:20pxline-height:18px;'>Date : ".$first_date." to ".$last_date."</div>
<div style='color:#ffff4d;margin-top:10px;line-height:18px;'>Absolute Monthly Data</div>
<div style='line-height:18px;'>Number of Users in the scheme : ".$out_numberUsersScheme[$r]['no_user_scheme']."</div>
<div style='line-height:18px;'>No of journeys shared : ".$out_nUsersScheme_taken[$r]['no_journey_taken']."</div>
<div style='line-height:18px;'>Number of Miles Shared : ".$no_miles_shared."</div>
<div style='line-height:18px;'>Kg of Co2 saved : ".$no_total_miles."</div>
</div>
</div><div class='box-3'></div>
<div class='box-2'><img src='pdf/images/" . $lists['name'] . ".png' width='260' style='margin-top:10px;'></div>
</div>
<div style='clear:both;'></div>

<div  style='padding-top:25px' class='footer'>
<div style='border-top: 1px solid black;; margin: 0px auto; width: 158px;color:#000;'></div>
<div>Steve Dickson</div><div>CEO,First Thumb</div><div>".$footermonth."</div></div>
</div>
</body>
</html>
";

        /* $mpdf=new mPDF();
          $mpdf->AddPage('L');
          $mpdf->WriteHTML($html);
          $mpdf->SetDisplayMode('fullpage');

          $mpdf->Output(); */
        //print_r($out);






        $mpdf = new mPDF();
        $mpdf->AddPage('L');
        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullpage');
//$content = $mpdf->Output('', 'S');
//$content = chunk_split(base64_encode($content));
        $mpdf->Output('pdf/' . $lists['name'] . ".pdf", 'F');
        $mailto = '';
        $from_name = '';
        $from_mail = '';
        $replyto = '';
        $uid = md5(uniqid(time()));
        $subject = '';
        $message = '';
        array_push($files, $lists['name'] . ".pdf");
  $r++;
        }
//exit;
   
// email fields: to, from, subject, and so on
    $to = $user->get(CUser::EMAIL);
   // $to = 'saneesh.ak@ipsrsolutions.com';
    $from = CConfig::INFO_EMAIL;
    $subject = "First Thumb : Carbon Savings Report ".$footermonth_prev;
    $message = '<html>
<head>

</head>
<body>
<table style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000; font-weight:normal; padding:0px; margin:0px; line-height:20px;" width="650" cellpadding="0" cellspacing="0" border="0">
 <tbody><tr style="border-bottom:solid 1px #d51c5c;" bgcolor="#fff">
    <td style="border-bottom:solid 5px #00523d;">&nbsp;</td>
    <td style="border-bottom:solid 5px #00523d;"><img src="https://firsthumb.com/content/web/assets/images/1st_logo.png"></td>
    <td style="border-bottom:solid 5px #00523d;">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td width="20">&nbsp;</td>
    <td><p style="padding-bottom:5px; margin:0px;">Hi <strong> '.$u_name.',</strong></p></td>
    <td width="20">&nbsp;</td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><p>Greetings from First Thumb Team.

Thanks for your support in building a green and healthy world.

We look forward to improve the efforts in Carbon savings.

The Carbon Savings Report for the month of '.$footermonth_prev.' is attached.</p>
  
      </td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td></p>

    <p>Many thanks,</p>
    <p style="font-size:11px">First Thumb</p>

</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 
  
  
  <tr bgcolor="#00523d">
    <td>&nbsp;</td>
    <td bgcolor="#00523d">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</tbody></table>
</body>
</html>';
    $headers = "From: $from";

    // boundary 
    $semi_rand = md5(time());
    $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

    // headers for attachment 
    $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";

    // multipart boundary 
    $message = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
    $message .= "--{$mime_boundary}\n";

    // preparing attachments
    for ($x = 0; $x < count($files); $x++) {
        $file = fopen("/home1/eridesh1/public_html/demo/pdf/".$files[$x], "rb");
        $data = fread($file, filesize("/home1/eridesh1/public_html/demo/pdf/".$files[$x]));
        fclose($file);
        $data = chunk_split(base64_encode($data));
        $name = $files[$x];
        $message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$name\"\n" .
                "Content-Disposition: attachment;\n" . " filename=\"$name\"\n" .
                "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
        $message .= "--{$mime_boundary}\n";
    }
    // send

    $ok = mail($to, $subject, $message, $headers);
    if ($ok) {
        $out1 = 'Mail Send successfully';
    } else {
       $out1 = 'Mail Send failed';
    }
 CCommon::xhrSend(CCommon::toJson($out1));
    exit;
    $out = $user->loadWithGroupUserId($userId);
    $to = $user->get(CUser::EMAIL);
    $first_date = date('d-m-Y', strtotime('first day of last month'));
    $last_date = date('d-m-Y', strtotime('last day of last month'));
    $uid = strtoupper(md5(uniqid(time())));
    $from = CConfig::INFO_EMAIL;

    $ok = mail($to, $subject, $message, $header);
    if ($ok)
        $out1 = 'Mail Send successfully';
    else {
        $out1 = 'Mail Send failed';
    }
    CCommon::xhrSend(CCommon::toJson($out1));
    exit;
}
/* if ($op == 'send_co_savings') {

  $out=$user->loadWithGroupUserId($userId);
  $to=$user->get(CUser::EMAIL);
  $first_date = date('d-m-Y', strtotime('first day of last month'));
  $last_date = date('d-m-Y', strtotime('last day of last month'));
  $uid = strtoupper(md5(uniqid(time())));
  $from=CConfig::INFO_EMAIL;
  //$from='saneeshcherai@gmail.com';
  $subject='Monthly Report';
  $header = "From:$from" . "\r\n" .
  "Reply-To: $from" . "\r\n" .
  'X-Mailer: PHP/' . $uid;
  $header .= "MIME-Version: 1.0" . "\r\n";
  $header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $message='<!DOCTYPE html>
  <html>
  <head>

  </head>
  <body>

  <h2 style="text-decoration: underline;">Monthly Report '.$first_date.' - '.$last_date.'</h2>

  <table style="border-color: #666;" cellpadding="10">
  <tr style="background: #eee;font-weight:bold;font-size:14px;">
  <th>Sl No</th>
  <th>Group Name</th>
  <th>Co2 Savings</th>
  </tr>';
  $i=1;
  foreach($out as $lists){
  $total=0;
  if(isset($lists['total_miles']))
  $total= $lists['total_miles'];
  $message.='<tr>
  <td>'.$i.'</td>
  <td>'.$lists['name'].'</td>
  <td>'.$total.'</td>
  </tr>';
  $i++; }
  $message.='</table>

  </body>
  </html>';
  $ok = mail($to, $subject, $message, $header);
  if($ok)
  $out1='Mail Send successfully';
  else {
  $out1='Mail Send failed';
  }
  CCommon::xhrSend(CCommon::toJson($out1));
  exit;
  } */
if ($op == 'search_group') {
    $str = CCommon::getRequestValue('q');
    $groupDetails = new CGroup();
    $out = $groupDetails->searchGroup($str);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
if ($op == 'search_group_list') {
    $str = CCommon::getRequestValue('q');
    $groupDetails = new CGroup();
    $out = $groupDetails->searchGroupListUser($str, $userId);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
//Email domain comparison for group listing
if ($op == 'domain') {
    $domain = CCommon::getRequestValue('value');
    $out = getGroups($domain);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
// XmlHttpRequest:- Register
if (isset($_GET['op'])) {

    $out = uploadFile($user, $region, $session_id, $login);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
// XmlHttpRequest:- Terms or Privacy HTML
if ($op == 'tos' || $op == 'pp') {
    $out = getHtml($op, $region);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}

if ($op == 'groupmanagement') {


    $GRoupNames = CGroup::getGroupNamesByOwnerId($userId);
    CCommon::xhrSend(CCommon::toJson($GRoupNames));
    // approveMail($url_parameter);
    exit;
}

if ($op == 'search_group_member') {
    $str = CCommon::getRequestValue('q');
    $group_id = CCommon::getRequestValue('g');

    $usergroup = new CUserGroup();
    $out = $usergroup->searchGroupMember($str, $group_id, $session_id);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
$refer = new CUrl(CCommon::getRequestValue('refer'));
// Output HTML page
$rplc = array();
$rplc[1] = ($isValidSession ? sprintf('%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, 'common')) : "");

$tmp = $region->msg(1016);
$tmp = str_replace('%1', '<a target="_blank" href="terms_and_conditions.php">', $tmp);
$tmp = str_replace('%2', '<a target="_blank" href="privacy.php">', $tmp);
$tmp = str_replace('%3', '</a>', $tmp);
$tmp = str_replace('%4', $region->msg(8, 'common'), $tmp);
$rplc[2] = $tmp;
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(($isValidSession ? 1101 : 1100));
$rplc[19] = scriptLinks();
$rplc[20] = script($user, $region, $refer, $isValidSession, $session_id, $groups, $remaining_groups,$free_groups);

$rplc[21] = $region->msg(($isValidSession ? 1023 : 1022));
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
$rplc[35] = $region->msg(1025);
$rplc[1111] = $region->msg(1111);
$rplc[11111] = CConfig::SITE_BASE.'/Admin-Area/';
$checked='';
if($checkgroups['active']==1)
    $checked='checked';
$rplc[11112] ='<input type="checkbox"  value="1" class="form-control" name="free-distance" id="free-distance" '.$checked.'>';
$rplc[11113] =$checkgroups['free_miles'];
if(isset($_SESSION['id']))
$user_welcome="Welcome, ".$_SESSION['username'];
$rplc[36] = $user_welcome;
$send_co_savings = '';
//$send_co_savings = '<a href="javascript:void(0)" onClick="send_co_report();" style="float:right">auto-print CO2 REPORT</a>';
if (isset($group_exists)) {
    $send_co_savings = '<a href="javascript:void(0)" onClick="send_co_report();" style="float:right">EMAIL CO2 SAVINGS REPORT</a>';
}

if(isset($_SESSION['id'])){
    $right_side_bar = '';
    $menu_header = file_get_contents('../admin_header_menus.php');
    $heading = 'FREE MILES ALLOCATION';
}

$rplc[777] = $menu_header;
$rplc[778] = $heading;
$rplc[779] = $right_side_bar;
$rplc[780] = $send_co_savings;
//var_dump($rplc);
$out = CCommon::htmlReplace('user_lists.html', $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
    @include 'google_analytics.html';

/*
 * Do the register or update
 * 
 * @param $user    A CUser object to fill in
 * @param $region  A CRegion object to supply messages
 * @return         An array of error strings. Empty array means register was successfull
 */

function graphCreation($name, $lists) {
    // Some (random) data
    unset($active_user);
    unset($co_savings);
    unset($months);
    unset($ydata);
    $count = count($lists['total_miles']);
    $active_user[0] = 0;
    $co_savings[0] = 0;
    $months[0] = 0;
    //$ydata[0]=2;
    if ($count == 0) {
        $active_user = array();
        $co_savings = array();
        $months = array();
         $ydata = array();
    }

    for ($i = 0; $i < $count; $i++) {
        $active_user[] = $lists['active_user_counts'][$i];
        $co_savings[] = $lists['total_miles'][$i];
       
        $months[] = $lists['months'][$i];
        //$ydata[]=2;
    }
//print_r($active_user);print_r($months);

// Size of the overall graph
    $width = 400;
    $height = 220;
 $max_co=max($co_savings);
 $counts=count($months);
$x=0.1;
if($max_co>30)
{
  $x=2;
}
  for($t=0;$t<$counts;$t++)
    {
     $ydata[$t]= $x;  
    }
// Create the graph and set a scale.
// These two calls are always required
    $graph = new Graph($width, $height);
//$graph->SetScale('intlin');
    $graph->SetScale('intlin', 0, 0, 0, 12);
    if ($count == 0) {
        $active_user = array(0);
        $co_savings = array(0);
        $months = array(0);
        $ydata=array(0);
        $graph->SetScale('intlin', 0, 6000, 0, 12);
    }
//$graph->yscale->ticks->Set(1000,1);
    $graph->xscale->ticks->Set(1, 1);

    $graph->SetShadow();

// Setup margin and titles
//$graph->img->SetMargin(40,125,10,0);
    $graph->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
    $graph->xaxis->title->SetFont(FF_FONT1, FS_BOLD);
    $graph->SetMargin(40, 130, 10, 10);
// Create the first data series
 
    $lineplot = new LinePlot($ydata,$months);
    $lineplot->SetWeight(2);   // Two pixel wide
// Add the plot to the graph
    $graph->Add($lineplot);
    $lineplot->SetColor("#3871b4");
// Create the second data series
   
    $lineplot2 = new LinePlot($co_savings);
    $lineplot2->SetWeight(1);   // Two pixel wide
//$lineplot2->SetColor("orange");
// Add the second plot to the graph
    $graph->Add($lineplot2);
    $lineplot2->SetColor("#ba413e");
// Create the third data series
    
    $lineplot3 = new LinePlot($active_user);
    $lineplot3->SetWeight(1);   // Two pixel wide
// Add the second plot to the graph
    $graph->Add($lineplot3);
    $lineplot3->SetColor("#87ae39");
    $lineplot3->SetLegend("Active users");
    $lineplot2->SetLegend("Kg of Co2 saved");
    $lineplot->SetLegend("Months");
    $graph->legend->SetColumns(1);

// Adjust the position of the grid box
    $graph->legend->Pos(0, 0.5, "right", "center");

// Display the graph
    $graph->Stroke("pdf/images/" . $name . ".png");
}

function register($login, $user, $region, $isValidSession, $session_id, $newgroup, $domain_groups, $searched_groups, $mailStatus = 0) {
    CCommon::dumpRequestVals();

    $oldemail = $user->get(CUser::EMAIL);
    $oldpwd = $user->get(CUser::PASSWORD);
    $oldmobile = $user->get(CUser::MOBILE);
    $image = $user->get(CUser::DRIVING_LICENSE_IMAGE);

    // Map POSTed values into "CUser" members
    $keyMap = array();
    $keyMap[] = CCommon::makeRequestKeyMap("firstname", CUser::FIRST_NAME);
    $keyMap[] = CCommon::makeRequestKeyMap("surname", CUser::LAST_NAME);
    $keyMap[] = CCommon::makeRequestKeyMap("address", CUser::ADDRESS1);
    $keyMap[] = CCommon::makeRequestKeyMap("address1", CUser::ADDRESS2);
    $keyMap[] = CCommon::makeRequestKeyMap("address2", CUser::ADDRESS3);
    $keyMap[] = CCommon::makeRequestKeyMap("postcode", CUser::POSTCODE);
    $keyMap[] = CCommon::makeRequestKeyMap("email", CUser::EMAIL);
    $keyMap[] = CCommon::makeRequestKeyMap("paypalEmail", CUser::PAYPAL_EMAIL);
    $keyMap[] = CCommon::makeRequestKeyMap("mobile", CUser::MOBILE);
    $keyMap[] = CCommon::makeRequestKeyMap("carreg", CUser::CARREG);
    $keyMap[] = CCommon::makeRequestKeyMap("password", CUser::PASSWORD);
    $keyMap[] = CCommon::makeRequestKeyMap("phone_code", CUser::PHONE_CODE);
    $keyMap[] = CCommon::makeRequestKeyMap("photo", CUser::PHOTO);
    $keyMap[] = CCommon::makeRequestKeyMap("driving_license_number", CUser::DRIVING_LICENSE_NUMBER);
    $keyMap[] = CCommon::makeRequestKeyMap("driving_license_image", CUser::DRIVING_LICENSE_IMAGE);
    $keyMap[] = CCommon::makeRequestKeyMap("photo_id", CUser::PHOTO_ID);
    $keyMap[] = CCommon::makeRequestKeyMap("sex", CUser::SEX);
    $keyMap[] = CCommon::makeRequestKeyMap("smoking_habit", CUser::SMOKING_HABIT);
    $keyMap[] = CCommon::makeRequestKeyMap("social_media_ids", CUser::SOCIAL_MEDIA_IDS);
    $keyMap[] = CCommon::makeRequestKeyMap("driver", CUser::DRIVER);
    $keyMap[] = CCommon::makeRequestKeyMap("passenger", CUser::PASSENGER);
    $keyMap[] = CCommon::makeRequestKeyMap("crop_x", CUser::CROP_X);
    $keyMap[] = CCommon::makeRequestKeyMap("crop_y", CUser::CROP_Y);
    $keyMap[] = CCommon::makeRequestKeyMap("crop_w", CUser::CROP_W);
    $keyMap[] = CCommon::makeRequestKeyMap("crop_h", CUser::CROP_H);
    $keyMap[] = CCommon::makeRequestKeyMap("crop_img_original", CUser::CROP_IMG_ORIGINAL);


    $user->set(CUser::SEX, 'f');

    CCommon::mapRequestValues($keyMap, $user);

    if (CCommon::getRequestValue("sex") == 'f')
        $user->set(CUser::SEX, 'f');
    else
        $user->set(CUser::SEX, 'm');
    if (CCommon::getRequestValue("smoking_habit") == 'n')
        $user->set(CUser::SMOKING_HABIT, 'n');
    else
        $user->set(CUser::SMOKING_HABIT, 'y');
    $social_media_ids = json_encode(array("facebook" => CCommon::getRequestValue("facebook_id"),
        "linkedin" => CCommon::getRequestValue("linked_id"), "twitter" => CCommon::getRequestValue("twitter_id")));
    $user->set(CUser::SOCIAL_MEDIA_IDS, $social_media_ids);
    $dir = 'upload/' . $session_id;






    $crop_img_original = $user->get(CUser::CROP_IMG_ORIGINAL);

    // $data = getimagesize($crop_img_original);
    //echo  $crop_img_original . "++++<br>";
    $crop_x = (int) $user->get(CUser::CROP_X);
    $crop_y = (int) $user->get(CUser::CROP_Y);
    $crop_w = (int) $user->get(CUser::CROP_W);
    $crop_h = (int) $user->get(CUser::CROP_H);
    if ($crop_img_original != '') {
        $extfile = explode('.', $crop_img_original);
        $inFile = $dir . "/" . $crop_img_original;
        $outFile = $dir . "/" . $session_id . "_Photo." . $extfile[1];

        $image = new Imagick($inFile);
        $image->cropImage($crop_w, $crop_h, $crop_x, $crop_y);
        $image->writeImage($outFile);



        $outFile_140 = $dir . "/" . $session_id . '_140_' . "Photo." . $extfile[1];
        $image2 = new Imagick($outFile);
        $image2->thumbnailImage(140, 140);
        $image2->writeImage($outFile_140);

        $outFile_200 = $dir . "/" . $session_id . '_200_' . "Photo." . $extfile[1];
        $image1 = new Imagick($outFile);
        $image1->thumbnailImage(200, 200);
        $image1->writeImage($outFile_200);
    }

//unlink( $dir . "/" .$crop_img_original);
    // die("hngkjhnkghjnk");










    $driving_licence = '';
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                $filename = explode(".", $file);

                /*   if (strcmp($filename[0], $session_id . "_Photo") == 0)
                  $user->set(CUser::PHOTO, $file);
                  if (strcmp($filename[0], $session_id . "_PhotoId") == 0)
                  $user->set(CUser::PHOTO_ID, $file);

                  if (strcmp($filename[0], $session_id . "_DrivingLicense_Active") == 0)
                  $driving_licence = $file; */
                //$pos = strpos($file, $out['id']."_".$width);
                // if ($pos !== false) 
                if (strpos($file, $out['id'] . "_Photo.") !== false)
                    $user->set(CUser::PHOTO, $file);

                if (strpos($file, $out['id'] . "_PhotoId") !== false)
                    $user->set(CUser::PHOTO_ID, $file);

                //if (strcmp($filename[0], $session_id . "_DrivingLicense_Active")!== false)
                if (strpos($file, $out['id'] . "_DrivingLicense_Active") !== false)
                    $driving_licence = $file;
            }
            closedir($dh);
        }
    }

//print_r( $user);
    // die("driver up");


    if ((int) CCommon::getRequestValue("driver") == 1) {

        if ($isValidSession) {

            $license = json_decode($image, true);

            $old_active_photo = $license['active'];

            $license['active'] = $driving_licence;
            $license[] = $old_active_photo;

            $new_photo_json = json_encode($license);
        } else
            $new_photo_json = json_encode(array("active" => $driving_licence));



        $user->set(CUser::DRIVER, 1);
        $user->set(CUser::DRIVING_LICENSE_IMAGE, $new_photo_json);
    } else {

        $user->set(CUser::DRIVER, 0);
        $user->set(CUser::DRIVING_LICENSE_IMAGE, NULL);
        $user->set(CUser::DRIVING_LICENSE_NUMBER, 0);
        $user->set(CUser::CARREG, NULL);


        //  print_r($user);
    }

    if ((int) CCommon::getRequestValue("passenger") == 2) {
        $user->set(CUser::PASSENGER, 1);
        if ((int) CCommon::getRequestValue("id_type") == 1) {
            $user->set(CUser::PHOTO_ID, 0);
        }
    } else {

        $user->set(CUser::PASSENGER, 0);
        $user->set(CUser::PHOTO_ID, 0);
    }

    // Lower case email addresses
    $user->set(CUser::EMAIL, strtolower($user->get(CUser::EMAIL)));
    $user->set(CUser::PAYPAL_EMAIL, strtolower($user->get(CUser::PAYPAL_EMAIL)));
    {
        // For a few select domains add user to group based on their email domain. {
//        $email_domain = substr(strrchr($user->get(CUser::EMAIL), "@"), 1);
//        switch ($email_domain) {
//            case "bbc.co.uk":
//            case "mediacityuk.co.uk":
//            case "itv.com":
//            case "dock10.co.uk":
//            case "salford.ac.uk":
//            case "firsthumb.com":
//            case "gmail.com":
//            case "yahoo.com":
//                // Add all these users to the same group.
//                $user->set(CUser::GROUP, CConfig::GROUP_MEDIACITY);
//                break;
//            default:
//                // Unrecognised extension - do nothing.
//                break;
//        }
    }


    // uppercase car reg
    $user->set(CUser::CARREG, strtoupper($user->get(CUser::CARREG)));

    // Replace "07" mobile prefix with UK country code.
    // e.g.  07966123456 -> 447966123456
    if (0 == strncmp($user->get(CUser::MOBILE), "07", 2)) {
        $user->set(CUser::MOBILE, "447" . substr($user->get(CUser::MOBILE), 2));
    }

    // No credit initially, and an SMS user
    if (!$isValidSession) {
        $user->set(CUser::CREDIT, '0');
        $user->set(CUser::BONUS_CREDIT, 0);
        $user->set(CUser::APPUSER, '0');
        $user->set(CUser::LAST_ACK_ID, '0');
        $user->set(CUser::PASS_LAST_ACK_ID, '0');
        $user->set(CUser::LAST_IM_ACK_ID, '0');
        $user->set(CUser::PUSH_HANDLER_ID, '0');
    }
    if ($isValidSession) {
        $email = $user->get(CUser::EMAIL);
        if (strcmp($email, $oldemail) != 0)
            $user->removeDomainGroups($session_id);
    }
    // Must have username if registering or if they checked the box on the profile page
    if (CCommon::getRequestValue("setEmail") || $isValidSession == false) {
        $email = $user->get(CUser::EMAIL);
    } else {
        $email = $oldemail;
        $user->set(CUser::EMAIL, $email);
    }
    if ($email == '')
        return CCommon::makeErrorObj(CCommon::BAD_EMAIL_ADDR, $region->msg(1003));

    $pwd = $user->get(CUser::PASSWORD);


    // Check values required if not running via Facebook
    if (CConfig::RUN_IN_FB == false) {
        // Check if user already exists
        if ($isValidSession && $oldemail == $email)
            ; /* do nothing */
        else {
            $user2 = new CUser();
            if ($user2->load($email) == false) {
                CLogging::error('Failed to load user with email ' . $email);

                return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
            }
            if ($user2->get(CUser::ID))
                return CCommon::makeErrorObj(CCommon::USER_EXISTS, sprintf($region->msg(1000), $email));
        }

        // die('hdgjdhg');
        // Make password (changing email address forces a change of password)
        if (CCommon::getRequestValue("setPassword") || CCommon::getRequestValue("setEmail") || $isValidSession == false) {
            if ($pwd == '')
                return CCommon::makeErrorObj(CCommon::INVALID_PASSWORD, $region->msg(1001));
            $pwd = CUser::makePassword($email, $pwd);
            $user->set(CUser::PASSWORD, $pwd);
        }
        else {
            $user->set(CUser::PASSWORD, $oldpwd);
        }
    } else {
        // XXX Deal with facebook
        // Set the account password to "password"
        // XXX What?  Does this let FB users log in via the web with 'password'?
        $pwd = CUser::makePassword($email, 'password');
        $user->set(CUser::PASSWORD, $pwd);
    }

    // CLogging::info('Mobile was |' . $oldmobile . '| now |' . $user->get(CUser::MOBILE) . '|');
    if ($user->get(CUser::MOBILE) != '' && 'x' . $user->get(CUser::MOBILE) != 'x' . $oldmobile)
        $user->verifyPhoneMail($user, $region);
    // Store the user
    $out = array();
    $email_domain = substr(strrchr($user->get(CUser::EMAIL), "@"), 1);

    //Check if the verification word was entered correctly
    if (CCaptcha::check(CCommon::getRequestValue("captcha")) == false) {
        return CCommon::makeErrorObj(CCommon::BAD_CAPTCHA, $region->msg(1002));
    }

    if ($isValidSession) {
        $user->set(CUser::VERIFIED, 1);
        if ($user->save() == false) {
            CLogging::error('Failed to save user');
            return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
        }
        // Update the current websession with new user details
        $login->getUser(true);
        if (count($newgroup) > 0) {
            $res = $user->createGroup($user->get(CUser::ID), $newgroup, $region, 1);

            if (count($res) != 0)
                $user->sendInvitation($mailStatus, $email_domain, $user, $res);
        }

        if (strcmp(trim($domain_groups), "0") != 0 || strcmp(trim($searched_groups), "0") != 0)
            $res = $user->saveUserGroup($user->get(CUser::ID), $domain_groups, $searched_groups, $newgroup);
        $login->getUser(true);
        CWebSession::set('login', serialize($login));
        //$out = array('infoText' => $region->msg(1043));
    } else {
        // $user->set(CUser::VERIFIED, CConfig::RUN_IN_FB ? 1 : 0);
        $user->set(CUser::VERIFIED, 1);

        if ($user->save() == false) {
            CLogging::error('Failed to save user');
            return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
        }

        if (count($newgroup) > 0) {
            $res = $user->createGroup($user->get(CUser::ID), $newgroup, $region, 0);
            if ($res == 0)
                $user->sendInvitation($mailStatus, $email_domain, $user, $newgroup);
            else
                return $res;
        }

        if (strcmp(trim($domain_groups), "0") != 0 || strcmp(trim($searched_groups), "0") != 0)
            $res = $user->saveUserGroup($user->get(CUser::ID), $domain_groups, $searched_groups, $newgroup);

        // Send mail depending on what system we're on
        if (CConfig::RUN_IN_FB)
            $user->registerCompleteEmail($user, $region);
        else {
            if ($res == false)
                $out = array('infoText' => $region->msg(1021) . " " . $region->msg(1036));
            else
                $out = array('infoText' => $region->msg(1021));
            $user->verifyAccountMail($user);
        }
    }

    return $out;
}

function request_phone_code($login, $user, $region, $isValidSession) {
    $user->verifyPhoneMail($user, $region);

    // Store the user
    if ($isValidSession) {
        if ($user->save() == false) {
            CLogging::error('Failed to save user');
            return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
        }
        // Update the current websession with new user details
        $login->getUser(true);
        CWebSession::set('login', serialize($login));
    } else {
        $user->set(CUser::VERIFIED, CConfig::RUN_IN_FB ? 1 : 0);

        // print_r($user);
        // echo 'register  ';
        if ($user->save() == false) {
            CLogging::error('Failed to save user');
            return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
        }
    }
    // $out = array('infoText' => 'A verification code has been sent to your mobile');
    $out = array();

    return $out;
}

function uploadFile($user, $region, $session_id, $login) {


//    if ($login->isValidSession() == false) {
//        $out = array('infoText' => 'Invalid or expired session specified');
//        die(json_encode($out));
//        //      die(CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified'));
//    } else {
    header('Content-Type: application/json');

    // Check for errors
    if (isset($_FILES['PhotoId'])) {
        $selectedFile = 'PhotoId';
        $dbColumn = 'PhotoId';
    }
    if (isset($_FILES['Photo'])) {
        $selectedFile = 'Photo';
        $dbColumn = 'Photo';
    }

    if (isset($_FILES['BirthCertificate'])) {
        $selectedFile = 'BirthCertificate';
        $dbColumn = 'BirthCertificate';
    }
    if (isset($_FILES['DrivingLicense'])) {
        $selectedFile = 'DrivingLicense';
        $dbColumn = 'DrivingLicense_Active';
    }
    if ($_FILES[$selectedFile]['error'] > 0) {
        //  outputJSON('An error ocurred when uploading.');
        $out = array('infoText' => $region->msg(1032));
        die(json_encode($out));
    }
    $ok = false;
    if (($_FILES[$selectedFile]["type"] == "application/octet-stream") || ($_FILES[$selectedFile]["type"] == "image/gif") || ($_FILES[$selectedFile]["type"] == "image/jpeg") ||
            ($_FILES[$selectedFile]["type"] == "image/gif") || ($_FILES[$selectedFile]["type"] == "image/jpg") ||
            ($_FILES[$selectedFile]["type"] == "image/png") || ($_FILES[$selectedFile]["type"] == "application/pdf")) {
        $ok = true;
    }

//$resultsss = print_r($_FILES[$selectedFile], true);
    if (!$ok) {
        //$out = array('infoText' => $resultsss);
        $out = array('infoText' => $region->msg(1031));
        die(json_encode($out));
    }

// Check filesize
    if ($_FILES[$selectedFile]['size'] > 2097152) {

        $out = array('infoText' => $region->msg(1029));
        die(json_encode($out));
    }
    $dir = 'upload/' . $session_id;
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                $filename = explode(".", $file);

                if (strpos($file, $session_id . "_" . $dbColumn . ".") !== false) {
                    unlink($dir . "/" . $file);
                }
                if (strpos($file, $session_id . "_200_" . $dbColumn . ".") !== false) {
                    unlink($dir . "/" . $file);
                }
                if (strpos($file, $session_id . "_140_" . $dbColumn . ".") !== false) {
                    unlink($dir . "/" . $file);
                }
            }
            closedir($dh);
        }
    }

    $path = $_FILES[$selectedFile]['name'];
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    if ($_FILES[$selectedFile]["type"] == "application/octet-stream") {
        $imageMime = getimagesize($_FILES[$selectedFile]['tmp_name']); // get temporary file REAL info
        $type = $imageMime['mime']; //set in our array the correct mime
        $ext = str_replace("image/", "", $type);
    }

    //$ext = $path;
    // Check if the file exists
    if (file_exists('upload/' . $session_id . '/' . $session_id . '_' . $dbColumn . "." . $ext) && strcmp($dbColumn, 'DrivingLicense_Active') == 0) {
        //unlink('upload/' . $session_id . '/' . $session_id . '_' . $dbColumn . "." . $ext);
        rename('upload/' . $session_id . '/' . $session_id . '_' . $dbColumn . "." . $ext, 'upload/' . $session_id . '/' . $selectedFile . '_' . date('Y:m:d h:i:s') . '.' . $ext);
    }

// Upload file
    if (!move_uploaded_file($_FILES[$selectedFile]['tmp_name'], 'upload/' . $session_id . '/' . $session_id . '_' . $dbColumn . '.' . $ext)) {

        $out = array('infoText' => $region->msg(1032), 'infoText1' => $session_id);
        die(json_encode($out));
    }


    $out = array('infoText' => $region->msg(1028), 'infoText1' => $session_id, 'extensions' => $session_id . '_' . $dbColumn . '.' . $ext);

    die(json_encode($out));
    // }
}

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks() {
    $out = array();
   // $out[] = CGoogle::scriptHtml();
    //$out[] = CYahoo::scriptHtml(array('json', 'container', 'dom', 'connection', 'button'));
    if (CConfig::RUN_IN_FB)
        $out[] = CFacebook::scriptHtml();
    $out[] = '<script type="text/javascript" src="js/flexigrid.pack.js"></script>';
    $out[] = '<script src="js/user_listing.js" type="text/javascript"></script>';
      $out[] = '<link rel="stylesheet" href="css/flexigrid.pack.css" type="text/css" />';
       $out[] = '<script type="text/javascript" src="js/flexigrid.pack.js"></script>';
    return join("\n", $out);
}

/*
 * Print <script> element to output
 * 
 * @param $user        A CUser object
 * @param $login       A CLogin object
 * @param $refer       A CUrl object
 * @return HTML stream
 */

function script($user, $region, $refer, $isValidSession, $session_id, $groups, $remaining_groups,$free_groups) {
    $out = array();
    $out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($region->msgList()));

    $out[] = join('', CRoot::formatClassAsJs('CUser'));
    $out[] = sprintf("var _user='%s';", $user->toJsonForUser());
    $out[] = sprintf("var _groups='%s';", $groups);
    $out[] = sprintf("var _remainingGroups='%s';", $remaining_groups);
    $out[] = sprintf("var _remainingGroups='%s';", $remaining_groups);
    $out[] = sprintf("var _freeMiles='%s';", $free_groups);
    if (CConfig::RUN_IN_FB)
        $out[] = sprintf('new CFacebook(%d);', CFacebook::isActive() ? 1 : 0);
    $out[] = sprintf("var _passwordReq=%d;", (CConfig::RUN_IN_FB ? 0 : 1));
    $out[] = sprintf("var _register=%d;", ($isValidSession ? 0 : 1));
    $out[] = sprintf("var _sessionid='%s';", $session_id);
    $out[] = sprintf("var _uploadStatusPhoto=%d;", 0);
    $out[] = sprintf("var _uploadStatusPhotoId=%d;", 0);
    $out[] = sprintf("var _uploadStatusLicense=%d;", 0);
    $out[] = sprintf("var _uploadStatusBirthCert=%d;", 0);
    $out[] = sprintf("var _selectedGroups=%s;", '0');
    $out[] = sprintf("var _searchedSelectedGroup=%s;", '0');
    $out[] = sprintf("var _mailAllOrganizationUsers=%d;", 0);
    $out[] = sprintf("var _emailExists=%s;", '0');
    return join('', $out);
}

/*
 * Get TOS or PP HTML text
 * @param $op       Opcode 'tos' or 'pp'
 * @param $region   A CRegion object
 * @return 			stdClass object with 'header' and 'body' members
 */

function getHtml($op, $region) {
    $out = new stdClass;
    if ($op == 'tos') {
        $out->header = $region->msg(8, 'common') . ' ' . $region->msg(1018);
        $out->body = file_get_contents(sprintf('%s/terms_and_conditions_popup.html', CConfig::CONTENT_DIR));
    } elseif ($op == 'pp') {
        $out->header = $region->msg(8, 'common') . ' ' . $region->msg(1019);
        $out->body = file_get_contents(sprintf('%s/privacy_popup.html', CConfig::CONTENT_DIR));
    }
    $out->body = str_replace('<!--#rplc8#-->', $region->msg(8, 'common'), $out->body);
    $out->body = str_replace('<!--#rplc9#-->', $region->msg(9, 'common'), $out->body);
    return $out;
}

/*
 * Get matching groups for email domain
 * 
 * @param $domain    email domain
 * 
 * @return 0 if no match else json string of matched groups
 */

function getGroups($domain) {
//    if ($login->isValidSession() == false) {
//        $out = array('infoText' => 'Invalid or expired session specified');
//        die(json_encode($out));
//        //      die(CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified'));
//    } else {
    $group_details = CUser::getMatchingGroups($domain);
    if (count($group_details['group_id']) > 0)
        die(json_encode($group_details));
    else
        die(0);
    // }
}

/*
 * Validation for new group
 * 
 * @param $group_tag    Organization_Location
 * @param $region    A Region object
 * @param $email_domain  email domain of user
 * @return 0 if no match else error string for alert
 */

function validateNewGroup($group_tag, $region) {
//    if ($login->isValidSession() == false) {
//        $out = array('infoText' => 'Invalid or expired session specified');
//        die(json_encode($out));
//        //      die(CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified'));
//    } else {
    $group = new CGroup();
    $count = $group->validateGroupname($group_tag);
    if ($count > 0) {
        $out = array('infoText' => sprintf($region->msg(1035), $group_tag));
        die(json_encode($out));
    } else {
        die(json_encode(array('infoText' => 0)));
    }
    // }
}

?>
