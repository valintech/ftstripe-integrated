var _googleMap;

function bodyOnload ()
{
	_googleMap = new CGoogleMap();
	YAHOO_Require(['json', 'button', 'connection', 'container'], _yuiLoaded);
	// Any custom destinations
	_destinations = CCommon.fromJson(decodeURIComponent(_destinations));
	if (_destinations.length)
	{
		Autocomplete.createFromInput('source', _destinations, '', 9001);
		Autocomplete.createFromInput('destination', _destinations, '', 9000);
	}
}

/*
 * YUI modules loaded
 */

function _yuiLoaded ()
{
	var inp = DOM_getElem('searchBtn');
	new YAHOO.widget.Button(inp, {type: 'submit', label: inp.value});
	DOM_getElem('source').focus();
}

function sourceOnblur (e)
{
	var source = document.getElementById("source");
	_googleMap.geocode(source.value, _response);
	
	function _response (response)
	{ 
		if (response.length)
			_googleMap.setMarker(0, response[0].lat, response[0].lng, response[0].label);
	}	
}

function destinationOnblur (e)
{
	var source = document.getElementById("destination");
	_googleMap.geocode(destination.value, _response);
	
	function _response (response)
	{ 
		if (response.length)
			_googleMap.setMarker(1, response[0].lat, response[0].lng, response[0].label);
	}	
}

function signIn ()
{
	var frm = document.getElementById("indexForm");
	frm.op.value = "signin";
	frm.submit();
}

function signOut ()
{
	var frm = document.getElementById("indexForm");
	frm.op.value = "signout";
	frm.submit();
}

/*
 * "onkeydown" event handler
 * 
 * @param e   DOM "Event" object
 */

function onKeyDown (e)
{
	e = CXPlat.eventObj(e);
	if (e.keyCode == 13)
	{
		var src = XPlat.eventSrcElement(e);
			if (src.id == "email" || src.id == "password")
				signIn();
	}
}

function findShare ()
{
	var frm = document.getElementById('indexForm');
	frm.submit();
}

/*
 * "onsubmit" event for <form> element
 */

function onSubmit ()
{
	var source = document.getElementById('source');
	var destination = document.getElementById('destination');
	if (source.value == '' && destination.value == '')
		return false;
	var frm = document.getElementById('indexForm');
	frm.action = 'arrange_travel_results.php';
	return true;
}

function refund (n)
{
	CConnection.doGet('index.php', {credit: n}, _response, null, null);

	function _response (o)
	{
		if (o.errorId == 0)
		{
			new YUI_DIALOG_Popup(o.errorText, 'Success', 'info', 'OK', function(KeyNo){CCommon.redirect('index.php');});
		}
		else
		{
			new YUI_DIALOG_Popup(o.errorText, 'Failed', 'block', 'OK');
		}
	}
}

