<?php

require_once('base.inc');
require_once('classes/session.inc');
require_once('classes/region.inc');
require_once('classes/yahoo.inc');
if (CConfig::RUN_IN_FB)
	require_once('classes/facebook.inc');

// Validate any current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->requireSession(false);
CWebSession::set('login', serialize($login));

// Get posted data
$op = CCommon::getRequestValue('op');
$email = strtolower(CCommon::getRequestValue("email"));
$password = CCommon::getRequestValue("password");
$refer = new CUrl(CCommon::getRequestValue('refer'));

// sign out
if ($op == 'signOut')
	if ($isValidSession)
	{
		$login->logout();
		CWebSession::set('login', serialize($login));
		$isValidSession = false;
		  CCommon::redirect('home.php'); /*commented for new design*/
               // CCommon::redirect('intro.php'); /*commented for new design*/
	}

// If Facebook active, redirect to index page if got a valid session
if ($isValidSession)
	if (CConfig::RUN_IN_FB)
		CFacebook::redirect('index.php');
	else
		CCommon::redirect('index.php');

// If we get here in F/B mode then theres something wrong
if (CConfig::RUN_IN_FB)
{
	CLogging::info('login.php - redirecting to F/B home page');
	CFacebook::redirect(CConfig::FB_HOME_URL);
}
		
$region = new CRegion('login');

// XmlHttpRequest:- Change password
if ($op == 'sendVerifyEmail')
{
	$out = sendVerifyEmail($email, $region);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// XmlHttpRequest:- Change password
if ($op == 'changePassword')
{
	$out = changePassword($email, $region);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// XmlHttpRequest:- sign in
if ($op == 'signin')
{
	$out = signIn($login, $email, $password, $region);
	CWebSession::set('login', serialize($login));
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// Output HTML page
$rplc = array();
$rplc[1] = str_replace('%1', $region->msg(8, 'common'), $region->msg(1000));
$rplc[2] = str_replace('%1', $region->msg(8, 'common'), $region->msg(1001));
$rplc[1111] = $region->msg(1111);
$rplc[3] = $region->msg(1002);
$rplc[4] = scriptLinks();
$rplc[5] = script($region, $refer);
$rplc[6] = $region->msg(1003);
$rplc[7] = $region->msg(1018);
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1100);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace('admin-login.html', $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
	@include 'google_analytics.html';

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks ()
{
	$out = array();
	$out[] = CYahoo::scriptHtml(array('button', 'connection', 'container'));
	$out[] = '<script type="text/javascript" src="thirdparty/webtoolkit/base64.js"></script>';
	$out[] = '<script type="text/javascript" src="js/common.js"></script>';
	$out[] = '<script type="text/javascript" src="login.js"></script>';
	return join("\n", $out);
}

/*
 * Generate <script> statements
 * 
 * @param $region  A CRegion object
 * @param $refer   A CUrl object
 * @return         HTML
 */

function script ($region, $refer)
{
	$out = array();
	$out[] = '<script type="text/javascript">';
	$msgs = $region->msgList();
	$out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($msgs));
	$out[] = sprintf("var _refer=new CUrl('%s');", CCommon::toJson($refer));
	$out[] = sprintf('var NOT_VERIFIED_ERROR=%d', CLogin::NOT_VERIFIED_ERROR);
	$out[] = '</script>';
	return join("\n", $out);
}

/*
 * Chanage password request
 * 
 * @param $email   Email of account to change password for
 * @param $region  A CRegion object
 * @return         stdClass obhject with 'errorText' member if error
 */

function changePassword ($email, $region)
{
	$user = new CUser;
	if ($email == '' || $user->loadBy(CUser::EMAIL, $email) == false
		|| $user->get(CUser::ID) == 0)
		return CCommon::makeErrorObj(CCommon::BAD_EMAIL_ADDR, $region->msg(1004));
	$rplc = array();
	$urlParams = array();
	$urlParams['userId'] = $user->get(CUser::ID);
	$urlParams['userKey'] = $user->getUserKey();
	$rplc[1] = sprintf('%s/resetpassword.php?v=%s', CConfig::SITE_BASE,
					   rawurlencode(base64_encode(CCommon::toJson($urlParams))));
	$rplc[2] = $user->get(CUser::EMAIL);
	$rplc[8] = $region->msg(8, 'common');
	$cfg = new stdClass;
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('resetpasswordrequestmail.html'), $rplc);
	$cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile('resetpasswordrequestmail.txt'), $rplc, false);
	$cfg->subject = str_replace('%1', $region->msg(8, 'common'), $region->msg(1005));
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->toName = $user->getFullName();
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->to = $user->get(CUser::EMAIL);
	CCommon::sendMail($cfg);
	return new stdClass;
}

/*
 * Sign in
 * 
 * @param $login    A CLogin object
 * @param $email    Login email
 * @param $password Login password
 * @param $region   A CRegion object
 * @return          stdClass object with "errorId", "errorText"
 *                  members if failed
 */

function signIn ($login, $email, $password, $region)
{
	$isValidSession = $login->login($email, $password);
	if ($isValidSession)
		$out = new stdClass;
	elseif ($login->lastError() == CLogin::INVALID_LOGIN_ERROR)
		$out = CCommon::makeErrorObj(CLogin::INVALID_LOGIN_ERROR, $region->msg(1004));
	elseif ($login->lastError() == CLogin::NOT_VERIFIED_ERROR)
	{
		$user = $login->getUser();
		$tmp = str_replace('%1', $region->msg(8, 'common'), $region->msg(1012));
		$out = CCommon::makeErrorObj(CLogin::NOT_VERIFIED_ERROR, $tmp);
	}
	else
	{
		$tmp = str_replace('%1', $region->msg(8, 'common'), $region->msg(1013));
		$out = CCommon::makeErrorObj(CLogin::UNKNOWN_ERROR, $tmp);
	}
	return $out;
}

/*
 * Send verification email
 * 
 * @param $email   Email address to send to
 * @param $region  A CRegion object
 * @return         stdClass object with "errorId", "errorText"
 *                 members if failed
 */

function sendVerifyEmail ($email, $region)
{
	$user = new CUser();
	if ($user->load($email) == false || $user->get(CUser::ID) == 0)
		$out = CCommon::makeErrorObj(CLogin::BAD_EMAIL_ADDR, $region->msg(1004));
	else
	{
		verifyAccountMail($user);
		$out = new stdClass;
		$out->infoText = $region->msg(1017);
	}
	return $out;	
}

/*
 * Send a mail asking for account verification
 * 
 * @param $user     A CUser object
 */

function verifyAccountMail ($user)
{
	$region = new CRegion;
	$rplc = array();
	$urlParams = array();
	$urlParams['userId'] = $user->get(CUser::ID);
	$urlParams['userKey'] = $user->getUserKey();
	$rplc[1] = sprintf('%s/confirmaccount.php?v=%s', CConfig::SITE_BASE,
					   rawurlencode(base64_encode(CCommon::toJson($urlParams))));
	$rplc[8] = $region->msg(8, 'common');
	$cfg = new stdClass;
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('verifyaccountmail.html'), $rplc);;
	$cfg->subject = $region->msg(8, 'common') . ' registration';
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->toName = $user->getFullName();
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->to = $user->get(CUser::EMAIL);
	CCommon::sendMail($cfg);
}
?>
