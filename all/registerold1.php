<?php
require_once('base.inc');
require_once('classes/user.inc');
require_once('classes/common.inc');
require_once('classes/region.inc');
require_once('classes/yahoo.inc');
require_once('classes/google.inc');
require_once('classes/captcha.inc');
require_once('classes/smsgw.inc');
if (CConfig::RUN_IN_FB)
	require_once('classes/facebook.inc');

// Validate any current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
if (CConfig::RUN_IN_FB)
	$isValidSession = $login->requireSession();
else
	$isValidSession = $login->isValidSession();
CWebSession::set('login', serialize($login));

// Establish language context
$region = new CRegion('register');

// Load any user passed as a parameter
$user = new CUser();
$userId = ($isValidSession ? (int)$login->userId() : 0);
if ($userId)
	$user->loadWithUserId($userId);

// Get posted data
$op = CCommon::getRequestValue('op');
$refer = new CUrl(CCommon::getRequestValue('refer'));

// XmlHttpRequest:- Register
if ($op == 'register')
{
	$out = register($login, $user, $region, $isValidSession);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// XmlHttpRequest:- Register
if ($op == 'request_phone_code')
{
	$out = request_phone_code($login, $user, $region, $isValidSession);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// XmlHttpRequest:- Terms or Privacy HTML
if ($op == 'tos' || $op == 'pp')
{
	$out = getHtml($op, $region);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

$refer = new CUrl(CCommon::getRequestValue('refer'));

// Output HTML page
$rplc = array();
$rplc[1] = ($isValidSession ? sprintf('%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, 'common')) : "");
$tmp = $region->msg(1016);
$tmp = str_replace('%1', '<a href="javascript:_toc()">', $tmp);
$tmp = str_replace('%2', '<a href="javascript:_pp()">', $tmp);
$tmp = str_replace('%3', '</a>', $tmp);
$tmp = str_replace('%4', $region->msg(8, 'common'), $tmp);
$rplc[2] = $tmp;
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(($isValidSession ? 1101 : 1100));
$rplc[19] = scriptLinks();
$rplc[20] = script($user, $region, $refer, $isValidSession);
$rplc[21] = $region->msg(($isValidSession ? 1023 : 1022));
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
$rplc[35] = $region->msg(1025);
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace('register.htm', $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
	@include 'google_analytics.html';

/*
 * Do the register or update
 * 
 * @param $user    A CUser object to fill in
 * @param $region  A CRegion object to supply messages
 * @return         An array of error strings. Empty array means register was successfull
 */

function register ($login, $user, $region, $isValidSession)
{
	CCommon::dumpRequestVals();

	$oldemail = $user->get(CUser::EMAIL);
	$oldpwd = $user->get(CUser::PASSWORD);
	$oldmobile = $user->get(CUser::MOBILE);

	// Map POSTed values into "CUser" members
	$keyMap = array();
	$keyMap[] = CCommon::makeRequestKeyMap("firstname", CUser::FIRST_NAME);
	$keyMap[] = CCommon::makeRequestKeyMap("surname", CUser::LAST_NAME);
	$keyMap[] = CCommon::makeRequestKeyMap("address", CUser::ADDRESS1);
	$keyMap[] = CCommon::makeRequestKeyMap("address1", CUser::ADDRESS2);
	$keyMap[] = CCommon::makeRequestKeyMap("address2", CUser::ADDRESS3);
	$keyMap[] = CCommon::makeRequestKeyMap("postcode", CUser::POSTCODE);
	$keyMap[] = CCommon::makeRequestKeyMap("email", CUser::EMAIL);
	$keyMap[] = CCommon::makeRequestKeyMap("paypalEmail", CUser::PAYPAL_EMAIL);
	$keyMap[] = CCommon::makeRequestKeyMap("mobile", CUser::MOBILE);
	$keyMap[] = CCommon::makeRequestKeyMap("carreg", CUser::CARREG);
	$keyMap[] = CCommon::makeRequestKeyMap("password", CUser::PASSWORD);
	$keyMap[] = CCommon::makeRequestKeyMap("phone_code", CUser::PHONE_CODE);
	CCommon::mapRequestValues($keyMap, $user);
	// Lower case email addresses
	$user->set(CUser::EMAIL, strtolower($user->get(CUser::EMAIL)));
	$user->set(CUser::PAYPAL_EMAIL, strtolower($user->get(CUser::PAYPAL_EMAIL)));

	// For a few select domains add user to group based on their email domain.
	{
		$email_domain = substr(strrchr($user->get(CUser::EMAIL), "@"), 1);
		switch ($email_domain) {
			case "bbc.co.uk":
			case "mediacityuk.co.uk":
			case "itv.com":
			case "dock10.co.uk":
			case "salford.ac.uk":
				// Add user to group of the same name.
				$user->set(CUser::GROUP, $email_domain);
		        break;
            default:
				// Unrecognised extension - do nothing.
				break;
		}
	}

	// uppercase car reg
	$user->set(CUser::CARREG, strtoupper($user->get(CUser::CARREG)));

	// Replace "07" mobile prefix with UK country code.
	// e.g.  07966123456 -> 447966123456
	if (0 == strncmp($user->get(CUser::MOBILE), "07", 2))
	{
		$user->set(CUser::MOBILE, "447" . substr($user->get(CUser::MOBILE), 2));
	}

	// No credit initially, and an SMS user
	if (!$isValidSession) {
		$user->set(CUser::CREDIT, '0');
		$user->set(CUser::BONUS_CREDIT, 0);
		$user->set(CUser::APPUSER, '0');
		$user->set(CUser::LAST_ACK_ID, '0');
		$user->set(CUser::LAST_IM_ACK_ID, '0');
		$user->set(CUser::PUSH_HANDLER_ID, '0');
	}

	// Must have username if registering or if they checked the box on the profile page
	if (CCommon::getRequestValue("setEmail") || $isValidSession == false)
	{
		$email = $user->get(CUser::EMAIL);
	}
	else
	{
		$email = $oldemail;
		$user->set(CUser::EMAIL, $email);
	}
	if ($email == '')
		return CCommon::makeErrorObj(CCommon::BAD_EMAIL_ADDR, $region->msg(1003));

	$pwd = $user->get(CUser::PASSWORD);

	// Check values required if not running via Facebook
	if (CConfig::RUN_IN_FB == false)
	{
		// Check if user already exists
		if ($isValidSession && $oldemail == $email)
			; /* do nothing */
		else
		{
			$user2 = new CUser();
			if ($user2->load($email) == false) {
				CLogging::error('Failed to load user with email '.$email);
				return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
			}
			if ($user2->get(CUser::ID))
				return CCommon::makeErrorObj(CCommon::USER_EXISTS, sprintf($region->msg(1000), $email));
		}
		// Check if the verification word was entered correctly
		if (!$isValidSession && CCaptcha::check(CCommon::getRequestValue("captcha")) == false)			   
			return CCommon::makeErrorObj(CCommon::BAD_CAPTCHA, $region->msg(1002));
		// Make password (changing email address forces a change of password)
		if (CCommon::getRequestValue("setPassword") || CCommon::getRequestValue("setEmail") || $isValidSession == false)
		{
			if ($pwd == '')
				return CCommon::makeErrorObj(CCommon::INVALID_PASSWORD, $region->msg(1001));
			$pwd = CUser::makePassword($email, $pwd);
			$user->set(CUser::PASSWORD, $pwd);
		}
		else
		{
			 $user->set(CUser::PASSWORD, $oldpwd);
		}
	}
	else
	{
		// XXX Deal with facebook
		// Set the account password to "password"
		// XXX What?  Does this let FB users log in via the web with 'password'?
		$pwd = CUser::makePassword($email, 'password');
		$user->set(CUser::PASSWORD, $pwd);
	}

	CLogging::info('Mobile was |' . $oldmobile . '| now |' . $user->get(CUser::MOBILE) . '|');
	if ($user->get(CUser::MOBILE) != '' && 'x'.$user->get(CUser::MOBILE) != 'x'.$oldmobile)
		verifyPhoneMail($user, $region);

	// Store the user
	$out = array();
	if ($isValidSession)
	{
		if ($user->save() == false) {
			CLogging::error('Failed to save user');
			return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
		}
		// Update the current websession with new user details
		$login->getUser(true);
		CWebSession::set('login', serialize($login));
	}
	else
	{
		$user->set(CUser::VERIFIED, CConfig::RUN_IN_FB ? 1 : 0);
		if ($user->save() == false) {
			CLogging::error('Failed to save user');
			return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
		}

		// Send mail depending on what system we're on
		if (CConfig::RUN_IN_FB)
			registerCompleteEmail($user, $region);
		else
		{
			$out = array('infoText' => $region->msg(1021));
			verifyAccountMail($user);
		}
	}

	return $out;
}

function request_phone_code ($login, $user, $region, $isValidSession)
{
	verifyPhoneMail($user, $region);

	// Store the user
	if ($isValidSession)
	{
		if ($user->save() == false) {
			CLogging::error('Failed to save user');
			return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
		}
		// Update the current websession with new user details
		$login->getUser(true);
		CWebSession::set('login', serialize($login));
	}
	else
	{
		$user->set(CUser::VERIFIED, CConfig::RUN_IN_FB ? 1 : 0);
		if ($user->save() == false) {
			CLogging::error('Failed to save user');
			return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
		}
	}
	// $out = array('infoText' => 'A verification code has been sent to your mobile');
	$out = array();

	return $out;
}

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks ()
{
	$out = array();
	$out[] = CGoogle::scriptHtml();
	$out[] = CYahoo::scriptHtml(array('json', 'container', 'dom', 'connection', 'button'));
	if (CConfig::RUN_IN_FB)
		$out[] = CFacebook::scriptHtml();
	$out[] = '<script type="text/javascript" src="js/common.js"></script>';
	$out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
	$out[] = '<script type="text/javascript" src="register.js"></script>';
	return join("\n", $out);
}

/*
 * Print <script> element to output
 * 
 * @param $user        A CUser object
 * @param $login       A CLogin object
 * @param $refer       A CUrl object
 * @return HTML stream
 */

function script ($user, $region, $refer, $isValidSession)
{
	$out = array();
	$out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($region->msgList()));
	$out[] = join('', CRoot::formatClassAsJs('CUser'));
	$out[] = sprintf("var _user='%s';", $user->toJson());
	$out[] = sprintf("var _refer=new CUrl('%s');", CCommon::toJson($refer));
	if (CConfig::RUN_IN_FB)
		$out[] = sprintf('new CFacebook(%d);', CFacebook::isActive() ? 1 : 0);
	$out[] = sprintf("var _passwordReq=%d;", (CConfig::RUN_IN_FB ? 0 : 1));
	$out[] = sprintf("var _register=%d;", ($isValidSession ? 0 : 1));
	return join('', $out);
}

/*
 * Get TOS or PP HTML text
 * @param $op       Opcode 'tos' or 'pp'
 * @param $region   A CRegion object
 * @return 			stdClass object with 'header' and 'body' members
 */

function getHtml ($op, $region)
{
	$out = new stdClass;
	if ($op == 'tos')
	{
		$out->header = $region->msg(8, 'common') . ' ' . $region->msg(1018);
		$out->body = file_get_contents(sprintf('%s/terms_and_conditions_popup.html', CConfig::CONTENT_DIR));
	}
	elseif ($op == 'pp')
	{
		$out->header = $region->msg(8, 'common') . ' ' . $region->msg(1019);
		$out->body = file_get_contents(sprintf('%s/privacy_popup.html', CConfig::CONTENT_DIR));
		
	}
	$out->body = str_replace('<!--#rplc8#-->', $region->msg(8, 'common'), $out->body);
	$out->body = str_replace('<!--#rplc9#-->', $region->msg(9, 'common'), $out->body);
	return $out;
}

/*
 * Send a mail asking for account verification
 * 
 * @param $user     A CUser object
 */

function verifyAccountMail ($user)
{
	$region = new CRegion;
	$rplc = array();
	$urlParams = array();
	$urlParams['userId'] = $user->get(CUser::ID);
	$urlParams['userKey'] = $user->getUserKey();
	$rplc[1] = sprintf('%s/confirmaccount.php?v=%s', CConfig::SITE_BASE,
					   rawurlencode(base64_encode(CCommon::toJson($urlParams))));
	$rplc[8] = $region->msg(8, 'common');
	$cfg = new stdClass;
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('verifyaccountmail.html'), $rplc);;
	$cfg->subject = $region->msg(8, 'common') . ' registration';
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->toName = $user->getFullName();
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->to = $user->get(CUser::EMAIL);
	CCommon::sendMail($cfg);
}

function verifyPhoneMail($user, $region)
{
	$code = rand(1000,9999);
	$user->set(CUser::PHONE_CODE, $code);

	$rplc = array();
	$rplc[1] = $user->getFullName();
	$rplc[2] = $code;
	$rplc[3] = CConfig::PHONE_NO;
	$rplc[8] = $region->msg(8, 'common');
	$cfg = new stdClass;
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('phoneverificationemail.html'), $rplc);
	$cfg->subject = $region->msg(8, 'common') . ' phone verification';
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->toName = $user->getFullName();
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->to = $user->get(CUser::EMAIL);
	CCommon::sendMail($cfg);
}

/*
 * Send a mail to indicate registration complete
 * 
 * @param $user     A CUser object
 * @param $region   A CRegion object
 */

function registerCompleteEmail ($user, $region)
{
	$cfg = new stdClass;
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->to = $user->get(CUser::EMAIL);
	$cfg->subject = $region->msg(8, 'common') . ' registration';
	$rplc = array();
	$rplc[1] = $user->getFullName();
	$rplc[2] = sprintf('%s/%s/', CConfig::FB_APP_URL, CConfig::FB_CANVAS_APP_NAME);
	$rplc[8] = $region->msg(8, 'common');
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('registeremail.html'), $rplc);
	$cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile('registeremail.txt'), $rplc, false);
	CCommon::sendMail($cfg);
}

?>
