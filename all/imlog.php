<?php
require_once('base.inc');
require_once('classes/common.inc');
require_once('classes/imlog.inc');
require_once('classes/journey.inc');

function htmlify ($text)
{
  /* There must be a better way than this */
  $text = ereg_replace ('&', '&amp;', $text);
  $text = ereg_replace ('<', '&lt;', $text);
  $text = ereg_replace ('>', '&gt;', $text);
  return $text;
}

printf("<HTML><HEAD></HEAD><BODY>\n");

printf("<h2>Im log:</h2>\n");
{
	$ids = CImLog::getLatestIds(180);
	$n = count($ids);

	printf("<TABLE BORDER=\"1\">\n");
	printf("<tr><td>id</td><td>from</td><td>to</td><td>Time</td><td>Text</td></tr>\n");
	for ($i = $n; $i; $i--) {
		$log = new CImLog();
		$log->load($ids[$i-1]);
		{
			$fromUser = new CUser();
			$fromUser->loadBy(CUser::ID, $log->get(CImLog::FROM_UID));
		}
		{
			$toUser = new CUser();
			$toUser->loadBy(CUser::ID, $log->get(CImLog::TO_UID));
		}
		printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n",
			$log->get(CImLog::ID),
			$fromUser->get(CUser::EMAIL),
			$toUser->get(CUser::EMAIL),
			strftime("%F %T", $log->get(CImLog::CREATED)),
			htmlify($log->get(CImLog::TEXT))
		);
	}
	printf("</TABLE>\n");
}

printf("</BODY></HTML>\n");

?>
