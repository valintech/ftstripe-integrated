<?php
require_once('base.inc');
require_once('classes/common.inc');
require_once('classes/calllog.inc');
require_once('classes/journey.inc');

function htmlify ($text)
{
  /* There must be a better way than this */
  $text = ereg_replace ('&', '&amp;', $text);
  $text = ereg_replace ('<', '&lt;', $text);
  $text = ereg_replace ('>', '&gt;', $text);
  return $text;
}

function do_one_ride($ids, $cnt, $idx)
{
	$canvas = "map_canvas_" + $idx;
	$log = new CCallLog();
	$log->load($ids[$idx]);
	$usr = $log->get('user_id');
	$msg = CCommon::fromJson($log->get('text'));
	printf("<br><br>Record %s, %s started ride at %s:<br>", $log->get('id'), $log->get('mobile'), strftime("%F %T", $log->get('created')));
	printf("<div id=\"%s\" style=\"width:600px;height:600px;\"></div>\n", $canvas);
	printf("<script type=\"text/javascript\">\n");
	printf("{\n");
	printf("var data = [];\n");
	if ($msg->location) {
   	printf("data.push(%s);\n", CCommon::toJson($msg->location));
	}
	while ($idx >= 0) {
		$log = new CCallLog();
		$log->load($ids[$idx]);
		$usr = $log->get('user_id');
		$msg = CCommon::fromJson($log->get('text'));
		if ($log->get('user_id') == $usr && $msg->type == 'updateLocation') {
			printf("data.push(%s);\n", CCommon::toJson($msg->location));
		} else if ($log->get('user_id') == $usr && $msg->type == 'endRide') {
			if ($msg->location) {
   			printf("data.push(%s);\n", CCommon::toJson($msg->location));
			}
			break;
		}
		$idx--;
	}

   printf("var myLatLng = new google.maps.LatLng(data[0].lat, data[0].lng);\n");
	printf("var myOptions = { zoom: 15, center: myLatLng, mapTypeId: google.maps.MapTypeId.ROADMAP };\n");
   printf("var map = new google.maps.Map(document.getElementById(\"%s\"), myOptions);\n", $canvas);

	printf("var trackCoords = [];");
	printf("for(i=0; i<data.length; i++){");
	printf("    new google.maps.Marker({position: {lat: data[i].lat, lng: data[i].lng}, map: map});");
	printf("    trackCoords.push(new google.maps.LatLng(data[i].lat, data[i].lng));");
	printf("}");
	printf("var trackPath = new google.maps.Polyline({");
	printf("  path: trackCoords,");
	printf("  strokeColor: \"#FF0000\",");
	printf("  strokeOpacity: 1.0,");
	printf("  strokeWeight: 2,");
	printf("});");
	printf("trackPath.setMap(map);");

	printf("}\n");
	printf("</script>\n");
}

printf("<HTML><HEAD><script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyCrsrko4kNeGljN1rM21blArPuUjNDthcw&sensor=false\"></script></HEAD><BODY>\n");

printf("<h2>Recent journeys:</h2>\n");
{
	$ids = CCallLog::getLatestIds(400);

	$n = count($ids);
	for ($i = 0; $i <$n; $i++) {
		$log = new CCallLog();
		$log->load($ids[$i]);
		$msg = CCommon::fromJson($log->get('text'));
		if ($log->get('direction') == 'R' && $msg->type == 'startRide')
			do_one_ride($ids, $n, $i-1);
	}
}

printf("</BODY></HTML>\n");

?>
