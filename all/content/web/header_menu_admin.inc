<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9"><![endif]-->
<!--[if IE 8]><html class="preIE9"><![endif]-->
<!--[if gte IE 9]><!--><html><!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		 <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <!--<link href="http://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese" rel="stylesheet" type="text/css">-->
        <title>Firsthumb - Driving People Together!<!--#rplc11#--> @ <!--#rplc8#--> UK</title>
        <meta name="keywords" content="lift share, carpools, rideshare, ridesharing, carpool, carpooling, carpool information, share a ride, ride board, ride share, road trip, cross country, commute, carsharing, carshare, car share, search, find, information, long distance, together, map, ride, rider, car pool, car rental, travel, commuter share, vanpool, car pooling, driver, liftshare, lift">
        <meta name="description" content="Lift sharing, carpool and ride sharing site, worldwide coverage.">
        <link rel="stylesheet" href="../content/web/assets/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="" type="text/css" id="firstcheck">
       <!-- <script src="../content/web/assets/jquery/jquery.min.js" type="text/javascript"></script>-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>

 var ua = window.navigator.userAgent;
    var iPhone = ua.indexOf("iPhone");
    var iPod = ua.indexOf("iPod");
    var iPad = ua.indexOf("iPad");

    if (iPhone > 0 || iPod >0 || iPad >0 ) 
    {
        document.getElementById('firstcheck').href="../content/web/assets/style-ios.css";
    }else
{
document.getElementById('firstcheck').href="../content/web/assets/style.css";
}

</script>
<script>
$(function() {
     var pgurl = window.location.href.substr(window.location.href
.lastIndexOf("/")+1);
     $("#nav li a").each(function(){
          if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
         $(this).closest('li').addClass('active');
     })
});
</script>

<script>
$(document).ready(function(){
   $(".click-menu").click(function(){
       $(".dropdown-menu-section").toggleClass("dispaly");
	   $(".overlay-div").toggleClass("dispaly");
   });
   $(".overlay-div").click(function(){
       $(".overlay-div").removeClass("dispaly");
	   $(".dropdown-menu-section").removeClass("dispaly");
   });
   $(".main-menu-icon").click(function(){
       $(".main-menu").toggleClass("dispaly");
   });
   
});
</script>
      
        <!--External scripts goes here-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>
    <body>
        <!-- { inverse | static | collapsed | black | mbr-nav-collapse } -->
		<div class="overlay-div"></div>
        <section class="menu-1 mbr-fixed-top mbr-nav-collapse inverse static">
            <div class="container">
                <div class="row">
				
                    <div class="col-sm-12">
                        <div class="brand">
						<div class="company-logo">
                            <a href="home.htm"><img src="../content/web/assets/images/1st_logo.png" alt="" class="img-responsive"> </a>
							</div>
                            <div class="main-menu-icon"></div>
                        </div>

                        
						
					<!--#rplc777#-->	
						
                        <div class="welcome-customer">  <!--#rplc36#-->

</div>
                    </div>
                </div>
            </div>
		</div>
    </section>
       
