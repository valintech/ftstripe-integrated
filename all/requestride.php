<?php
require_once('base.inc');
require_once('classes/region.inc');
require_once('classes/schedule.inc');
if (CConfig::RUN_IN_FB)
	require_once('classes/facebook.inc');

// Get current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$login->requireSession();
CWebSession::set('login', serialize($login));

// Get posted data
$op = CCommon::getRequestValue('op');

// XmlHttpRequest:- Initialise dialog
if ($op == 'init')
{	
	$journeyId = CCommon::getRequestValue('journeyId');
	$out = init($login, $journeyId);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// XmlHttpRequest:- Send request
if ($op == 'send')
{
	$journeyId = CCommon::getRequestValue('journeyId');
	$requestText = CCommon::getRequestValue('requestText');
	$out = sendRequest($login, $journeyId, $requestText);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

/*
 * Initialise dialog
 * 
 * @param $journeyId   Journey id.
 * @return             stdClass object containing exit values:- 'html'
 */

function init ($login, $journeyId)
{
	$out = new stdClass;
	$region = new CRegion('requestride');
	$out->msgList = $region->msgList();
	
//	// Check if request already submitted
	$userId = $login->userId();
//	$journeyRequest = new CJourneyRequest;
//	$journeyRequest->loadByJourneyAndUser($journeyId, $userId);
//	if ($journeyRequest->get(CJourneyRequest::ID))
//	{
//		$out->errorText = $region->msg(1007);
//		return $out;
//	}

	$journeys = CSchedule::load($journeyId);
	if ($userId == $journeys[0]->get(CSchedule::USER_ID))
	{
		$out->errorText = $region->msg(1014);
		return $out;
	}

	// Create output
	$rplc = array();
	$rplc[1] = $region->msg(1000);
	$rplc[2] = sprintf($region->msg(1003), $journeys[0]->get(CSchedule::START_ADDRESS));
	$rplc[3] = sprintf($region->msg(1004), $journeys[0]->get(CSchedule::DESTINATION_ADDRESS));
	$rplc[4] = $region->msg(1005);
	$rplc[8] = $region->msg(8, 'common');
	$rplc[9] = $region->msg(9, 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
	$rplc[11] = $region->msg(1100);
	$out->html = CCommon::htmlReplace('requestride.html', $rplc);

	return $out;
}

/*
 * Send a ride request
 * 
 * @param $login            A CLogin object
 * @param $journeyRequest   A CScheduleRequest object
 * @param $requestText      Request text
 * @return                  stdClass object, may contain 'errorText' member if
 *                          send request failed
 */

function sendRequest ($login, $journeyId, $requestText)
{
	$fromUser = $login->getUser(true);
	$region = new CRegion('requestride');
	$journeys = CSchedule::load($journeyId);
	$toUser = new CUser;
	$toUser->loadWithUserId($journeys[0]->get(CSchedule::USER_ID));
//	$journeyRequest = new CJourneyRequest;
//	$journeyRequest->set(CJourneyRequest::JOURNEY_ID, $journeyId);
//	$journeyRequest->set(CJourneyRequest::USER_ID, $fromUser->get(CUser::ID));
//	$journeyRequest->set(CJourneyRequest::REQUEST_TEXT, $requestText);
//	$journeyRequest->set(CJourneyRequest::SENT_TO_USER_ID, $toUser->get(CUser::ID));
//	$journeyRequest->genUniqueId();
//	// Check if request already submitted
//	$journeyRequest->loadByJourneyAndUser($journeyId, $fromUser->get(CUser::ID));
	$out = new stdClass;
//	if ($journeyRequest->get(CJourneyRequest::ID))
//		$out->errorText = $region->msg(1007);
//	elseif ($journeyRequest->save() == false)
//		$out->errorText = $region->msg(1008);
//	else
	{
		CLogging::info($fromUser->toJson());
		CLogging::info($toUser->toJson());
		$rplc = array();
		$rplc[1] = $fromUser->getFullName();
		$rplc[2] = $journeys[0]->get(CSchedule::START_ADDRESS);
		$rplc[3] = $journeys[0]->get(CSchedule::DESTINATION_ADDRESS);
//		if ($fromUser->get(CUser::FACEBOOK_ID) != '') {
//			$template = 'requestridefbmail';
//			$rplc[4] = $fromUser->get(CUser::FACEBOOK_ID);
//		} else {
			$template = 'requestridemail';
			$rplc[4] = $fromUser->get(CUser::EMAIL);
//		}
//		$urlParams = array();
//		$urlParams['journeyId'] = $journeyId;
//		$urlParams['requestId'] = $journeyRequest->get(CJourneyRequest::ID);
//		$urlParams['requestKey'] = $journeyRequest->get(CJourneyRequest::UNIQUE_ID);
//		$rplc[5] = sprintf('%s/confirmrequest.php?v=%s', CConfig::SITE_BASE,
//						   rawurlencode(base64_encode(CCommon::toJson($urlParams))));
		$rplc[6] = $fromUser->getFullName();
		$cfg = new stdClass;
		//$requestText = 'Hmm thingy whatsit'; //$journeyRequest->get(CJourneyRequest::REQUEST_TEXT);
		$rplc[7] = ($requestText != '' ? $requestText : $region->msg(1011));
		$cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile($template . '.txt'), $rplc, false);
		$rplc[7] = ($requestText != '' ? $requestText : htmlentities($region->msg(1011)));
		$rplc[8] = $region->msg(8, 'common');
		$cfg->body = CCommon::htmlReplace(CRegion::regionFile($template . '.html'), $rplc);
		$cfg->subject = $region->msg(8, 'common') . ' ' . $region->msg(1012);
		$cfg->fromName = CConfig::INFO_EMAIL_NAME;
		$cfg->toName = $toUser->getFullName();
		$cfg->from = CConfig::INFO_EMAIL;
		$cfg->to = $toUser->get(CUser::EMAIL);
		CCommon::sendMail($cfg);
	}
	return $out;	
}
?>
