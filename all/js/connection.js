//
// Member reference for the XMLHttpRequest object:-
//
// onreadystatechange - An event handler for an event that fires
// at every state change. Takes one parameter which is a reference
// to a XMLHttpRequest object.
//
// readyState    - Returns the state of the object:
//
// 	0 = uninitialized
// 	1 = loading
// 	2 = loaded
// 	3 = interactive
// 	4 = complete
//
// 	responseText - Returns the response as a string
//
// 	responseXML  - Returns the response as XML. This property returns
//                 an XML document object, which can be examined and
//                 parsed using W3C DOM node tree methods and
//                 properties
//
// 	status       - Returns the status as a number (e.g. 404 for "Not
//                 Found" or 200 for "OK")
//
// 	statusText   - Returns the status as a string (e.g. "Not Found"
//                 or "OK")
//

// ------------------------------------------------------------------------

///
/// Perform an XMLHttpRequest GET
/// @param[in] uri - URI to post to
/// @param[in] paramList - Parameter list as [name][value] [name][value] ...
/// @param[in] callback - Callback function or definition object if "async" request required.
/// Call back object is { callbackFunc: func, me: obj }, where "me" is the "this" pointer
/// needed to properly call "func"
/// @param[in] form - Form id. or object used to specify file upload paths etc.
/// @return If "callback" is specified, a DOM XMLHttpRequest object ( See above )
/// will be posted to that function otherwise if "callback" is not defined, the return
/// value is the object. The "callbackFunc" takes 2 parameters, "reqResult" and "reqObj",
/// the first is true if the request was successful and the sencond if the XMLHttpRequest
/// object.
///

function CONNECTION_Get ( uri, paramList, callback, form )
{
	if ( typeof form != "undefined" )
		YAHOO.util.Connect.setForm( form );
	if ( typeof callback != "object" )
		callback = { success: callback, failure: callback };
	__requestCore( "GET", uri, paramList, callback );
}

function CONNECTION_Post ( uri, paramList, callback, form )
{
	if ( typeof form != "undefined" )
		YAHOO.util.Connect.setForm( form );
	if ( typeof callback != "object" )
		callback = { success: callback, failure: callback };
	__requestCore( "POST", uri, paramList, callback );
}

function CONNECTION_Upload ( uri, paramList, callback, form )
{
	if ( typeof form != "undefined" )
		YAHOO.util.Connect.setForm( form, true, true );
	if ( typeof callback != "object" )
		callback = { upload: callback };
	__requestCore( "POST", uri, paramList, callback );
}

function __requestCore ( reqMethod, uri, paramList, callback )
{
	if ( typeof paramList == "undefined" )
		paramList = [];
	tmp = [];
	for ( key in paramList )
		tmp.push( key + '=' + encodeURIComponent( paramList[key] ) );
	uri = ( reqMethod == "GET" ? uri + ( tmp.length ? "?"+tmp.join("&") : "" ) : uri );
	postData = ( reqMethod == "GET" ? null : tmp.join("&") );
	YAHOO.util.Connect.asyncRequest( reqMethod, uri, callback, postData );
}

/*
 * CConnection class
 */

function CConnection ()
{
	// Class constants
	CConnection.OK = 200;
	
	CConnection.SESSION_EXPIRED = -2;
	
	/*
	 * Perform XmlHttpRequest "GET"
	 * 
	 * @param url           Request URL
	 * @param paramList     [optional] Object literal containing parameters
	 * @param callback      [optional] callback function (takes object parameter)
	 * @param callbackScope [optional] scope for callback function
	 * @param form          [optional] DOM <form> element to submit values from
	 */
	
	CConnection.doGet = function (url, paramList, callback, callbackScope, form)
	{
		this._requestCore('GET', url, paramList, callback, callbackScope, form);
	};

	/*
	 * Perform XmlHttpRequest "POST"
	 * 
	 * @param url           Request URL
	 * @param paramList     [optional] Object literal containing parameters
	 * @param callback      [optional] callback function (takes object parameter)
	 * @param callbackScope [optional] scope for callback function
	 * @param form          [optional] DOM <form> element to submit values from
	 */
	
	CConnection.doPost = function (url, paramList, callback, callbackScope, form)
	{
		this._requestCore('POST', url, paramList, callback, callbackScope, form);
	};

	/*
	 * Core method for GET and POST operations
	 * 
	 * @param url           Request URL
	 * @param paramList     [optional] Object literal containing parameters
	 * @param callback      [optional] callback object
	 * @param callbackScope [optional] scope for callback function
	 * @param form          [optional] DOM <form> element or form id. to submit values from
	 */

	CConnection._requestCore = function (reqMethod, uri, paramList, callback, callbackScope, form)
	{
		if (form != null)
			YAHOO.util.Connect.setForm(form);
		if (paramList == null)
			paramList = [];

		var tmp = ['_xmlHttpReq=1'];
		for (key in paramList)
			tmp.push(key + '=' + encodeURIComponent(paramList[key]));
		uri = (reqMethod == 'GET' ? uri + (tmp.length ? '?' + tmp.join('&') : '') : uri);
		postData = (reqMethod == 'GET' ? null : tmp.join('&') );
		YAHOO.util.Connect.asyncRequest(reqMethod, uri, {success: _response, failure: _response}, postData);
		
		// XmlHttpRequest response
		function _response (o)
		{
			if (o.status == CConnection.OK)
			{
				var tmp = (o.responseText == '' ? {} : COMMON.fromJson(o.responseText));
				if (tmp.errorId && tmp.errorId == CConnection.SESSION_EXPIRED)
					window.open(tmp.url, '_top');
				if (callback != null)
					if (callbackScope == null)
						callback(tmp);
					else
						callback.call(callbackScope, tmp);
			}
			else
			{
				var tmp = 'A communication problem has occured. The error id. is [%1] and the message is [%2]';
				tmp = tmp.replace(/%1/, o.status).replace(/%2/, o.statusText);
				alert(tmp);
			}
		}		
	};
}

// Instantiate to make static members visibile
new CConnection();
