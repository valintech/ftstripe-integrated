/*
 * TIME object
 */

var TIME =
{
	_yuiMenu: null,
	_callback: null,
	_focusId: null,
		
	showPicker: function (callback, id)
	{
		this._callback = callback;
		this._focusId = id;
		if (this._yuiMenu == null)
			this._createMenu();
		var xy = YAHOO.util.Dom.getXY(id);
		this._yuiMenu.moveTo(xy[0], xy[1]);
		this._yuiMenu.show();
	},
	
	_createMenu: function ()
	{
		var hours = [];
		for (hour = 1; hour <= 12; hour++)
		{
			var mins = [];
			for (var min = 0; min < 60; min += 5)
			{
				var amUrl = 'javascript:TIME._timeSelected(' + (hour % 12) + "," + min + ')';
				var pmUrl = 'javascript:TIME._timeSelected(' + (12 + (hour % 12)) + "," + min + ')';
				var am = new YAHOO.widget.MenuItem("am", {url: amUrl});
				var pm = new YAHOO.widget.MenuItem("pm", {url: pmUrl});
				var minMenu = new YAHOO.widget.Menu("h" + hour + "m" + min, {lazyload: true, iframe: false});
				minMenu.addItems([am, pm]);
				mins.push(new YAHOO.widget.MenuItem((min + 100).toString().substring(1, 99), {submenu: minMenu}));
			}
			var hourMenu = new YAHOO.widget.Menu("h" + hour, {lazyload: true, iframe: false});
			hourMenu.addItems(mins);
			hours.push(new YAHOO.widget.MenuItem((hour + 100).toString().substring(1, 99), {submenu: hourMenu}));
		}    
		this._yuiMenu = new YAHOO.widget.Menu("time1Container", {lazyload: true, iframe: false});
		this._yuiMenu.addItems(hours);
		this._yuiMenu.render(document.body);
	},
	
	_timeSelected: function (hour, minute)
	{
		if (this._callback)
			this._callback(this._focusId, hour, minute);
	}	
}