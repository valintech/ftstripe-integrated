/*
 * Wrapper for a YUI menu button control
 * 
*/

function MenuButton (id)
{
	this._id = id;
	this._btn = new YAHOO.widget.Button(id, {type: 'menu', menu: id + 'Menu'});
	var menu = this._btn.getMenu(); 
	menu.subscribe('click', this._onClick, this);
}

/*
 * ===================================================================
 * Class / instance variables
 * ===================================================================
 */

MenuButton.prototype._btn;
MenuButton.prototype._id;
MenuButton._instances = [];

/*
 * ===================================================================
 * Class / instance methods
 * ===================================================================
 */

MenuButton.createFrom = function (id)
{
	return new MenuButton(id);
};

/*
 * "click" event handler 
 */

MenuButton.prototype._onClick = function (p_sType, p_aArgs, obj)
{
	var oEvent = p_aArgs[0],    //  DOM event 
	oMenuItem = p_aArgs[1]; 	//  MenuItem instance that was the target of the event 
	if (oMenuItem)
	{
		var text = oMenuItem.cfg.getProperty('text');
		obj._btn.set('label', text);
		DOM_getElem(obj._id + 'Val').value = text;;
	}
};
