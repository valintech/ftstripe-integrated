var Y;
var _ttId = 1;					// Tooltip id. start


/*
 * YUI support class
 */

function YahooYui ()
{
};


/*
 * Load YUI modules
 * 
 * @param   modules   Array of required module ids.
 * @param   callback  [optional] called when modules loaded
 */

function YAHOO_Require (modules, callback)
{
	if (callback)
		callback();
}

/*
 * Load YUI modules
 * 
 * @param   modules   Array of required module ids.
 * @param   callback  [optional] called when modules loaded
 */

function _YAHOO_Require (modules, callback)
{
	// Always make DOM modules available
	if (modules.find('dom') == -1)
		modules.push('dom');
	Y = new YAHOO.util.YUILoader({base: "thirdparty/yui-2.8.2r1/build/", loadOptional: true, require: modules, onSuccess: _callback});
	Y.insert();
	
	function _callback ()
	{
		var extraModules = [];
		if (modules.find('autocomplete') != -1)
			extraModules.push({name: 'autocomplete_utils', type: 'js', path: 'autocomplete.js'});
		if (modules.find("connection") != -1)
			extraModules.push({name: "connection_utils", type: "js", path: "connection.js"});
		if (modules.find("calendar") != -1)
		{
			extraModules.push({name: "calendar_utils", type: "js", path: "calendar.js"});
			extraModules.push({name: "time_utils", type: "js", path: "time.js"});
		}
		if (modules.find('container') != -1)
			extraModules.push({name: 'container_utils', type: 'js', path: "container.js"});
		if (modules.find('dom') != -1)
			extraModules.push({name: 'dom_utils', type: 'js', path: "dom.js"});
		if (extraModules.length)
		{
			function _callback1 ()
			{
				if (typeof callback != "undefined")
					callback();
			}
			var extras = new YAHOO.util.YUILoader({base: "js/", onSuccess: _callback1});
			for (var i = 0; i < extraModules.length; i++)
			{
				extras.addModule(extraModules[i]);
				extras.require(extraModules[i].name);
			}
			extras.insert();
		}
		else if (typeof callback != "undefined")
			callback();
	}
}

/*
 * Add a YAHOO tooltip to an element
 * 
 * @param elem  Element reference or element id.
 * @param text  Tooltip text
 */

function YAHOO_addToolTip (elem, text)
{
	var ttId = 'yuiTt%1'.format(_ttId);
	_ttId++;
	new YAHOO.widget.Tooltip(ttId, {context: elem, text: text});
	return ttId;
}
function yuiConfirm(title, message, yesHandler, noHandler) {
    var handleYes = function() {
        confirmDialog.destroy();
        if (yesHandler)
            yesHandler();
    };

    var handleNo = function() {
        confirmDialog.destroy();
        if (noHandler)
            noHandler();
    };


    confirmDialog = new YAHOO.widget.SimpleDialog("yuiConfirm",
            {width: "300px",
                fixedcenter: true,
                visible: false,
                draggable: false,
                close: false,
                modal: true,
                text: message,
                icon: YAHOO.widget.SimpleDialog.ICON_HELP,
                constraintoviewport: true,
                buttons: [{text: "Yes", handler: handleYes, isDefault: true},
                    {text: "No", handler: handleNo}]
            });

    confirmDialog.setHeader(title);
    confirmDialog.render(document.body);
    confirmDialog.show();

}
