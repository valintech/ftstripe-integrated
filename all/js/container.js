var _overlayMgr = new YAHOO.widget.OverlayManager();
var _waitPanel;

var _yuiPopupIcons = [];
_yuiPopupIcons["alarm"] = YAHOO.widget.SimpleDialog.ICON_ALARM;
_yuiPopupIcons["block"] = YAHOO.widget.SimpleDialog.ICON_BLOCK;
_yuiPopupIcons["help"] = YAHOO.widget.SimpleDialog.ICON_HELP;
_yuiPopupIcons["info"] = YAHOO.widget.SimpleDialog.ICON_INFO;
_yuiPopupIcons["warn"] = YAHOO.widget.SimpleDialog.ICON_WARN;
_yuiPopupIcons["tip"] = YAHOO.widget.SimpleDialog.ICON_TIP;

///
/// YUI popup class
/// @param[in] text - Text in body area
/// @param[in] header - [optional] Text in header bar
/// @param[in] icon - [optional] Icon to show:- "alarm", "block", "help",
/// "info", "warn", "tip"
/// @param[in] buttonText - [optional] A string or array of strings to use as
/// labels for individual buttons
/// @param[in] callback - [optional] A user defined function that will receive an
/// indication of what key was pressed to close the popup. An index of 0, 1, 2 etc
/// will be returned correspding to the labels given in the "buttonText" list.
/// 2param[in] scope - [optional] object scope for "callback" function
/// A return of -1 indicates the X button was used to close the popup
///

function YUI_DIALOG_Popup (text, header, icon, buttonText, callback, scope)
{
	// Object passed?
	var modal = true;
	if (typeof text == 'object')
	{
		scope = text.scope;
		callback = text.callback;
		buttonText = text.buttonText;
		icon = text.icon;
		header = text.header;
		modal = (text.modal === null ? true : text.modal);
		text = text.text;
	}
	
	YUI_DIALOG_Popup.prototype.keyNo = -1;
	YUI_DIALOG_Popup.prototype._dlg;
	YUI_DIALOG_Popup.prototype._callback = callback;
	YUI_DIALOG_Popup.prototype._callbackScope = scope;

	// Make single string into an array
	if (typeof buttonText == "string")
		buttonText = [buttonText];
	
	if (header == null)
		header = "Error";
	icon = (typeof icon == "undefined" || typeof _yuiPopupIcons[icon] == "undefined"
			? _yuiPopupIcons["alarm"] : _yuiPopupIcons[icon]);
	var cfg = {width: "300px", fixedcenter: true, visible: false, draggable: true, 
			   close: false, text: text, icon: icon, constraintoviewport: true,
			   modal: modal, zIndex: 9999,
			   effect: { effect: YAHOO.widget.ContainerEffect.FADE, duration: 1 }};

	this._dlg = new YAHOO.widget.SimpleDialog("dlgPopup", cfg);
	this._dlg.hideEvent.subscribe(function(type, args){this._hideEvent(type, args);}, this, true);

	// Create a YUI button list. The callback will receive the index of pressed button
	// based upon the list passed in the "buttonText" array parameter
	var buttons = [];
	if (typeof buttonText != "undefined")
		for (var i = 0; i < buttonText.length; i++)
			buttons.push({text: buttonText[i],
						  handler: {fn: function(e,o){this._buttonClicked(e,o);}, obj: {keyNo: i}, scope: this}});
	if (buttons.length)
		this._dlg.cfg.queueProperty("buttons", buttons);
	
	this._dlg.setHeader(header);
	this._dlg.render(document.body);
	this._dlg.show();
	_overlayMgr.register(this._dlg);

	// Local callback. "hideEvent" event handler
	YUI_DIALOG_Popup.prototype._hideEvent = function (type, args)
	{
		this.callback(-1);
	};

	// Local callback. Button pressed
	YUI_DIALOG_Popup.prototype._buttonClicked = function (e, o)
	{
		this.callback(o.keyNo);
		this._dlg.destroy();
	};

	//
	// Execute any user defined callback
	// @param[in] keyNo  Key number 0-n or -1
	//

	YUI_DIALOG_Popup.prototype.callback = function (keyNo)
	{
		if (this._callback != null)
			if (this._callbackScope == null)
				this._callback(keyNo);
			else
				this._callback.call(this._callbackScope, keyNo);
	};
}

/*
 * Show a "loading" or "wait" panel
 * 
 * @param panelHead   Header text
 * @param container   Container for the panel
 */

function YUI_CONTAINER_Wait (panelHead, container)
{
	// Make sure we have a container of some kind
	if (container == null)
		container = document.body;

	// Create and render the loading panel
	_waitPanel = new YAHOO.widget.Panel('wait', 
									    {fixedcenter: true, close: false, draggable: false, 
									    zindex: 4, modal: true, visible: true});
	_waitPanel.setHeader(panelHead);
	_waitPanel.setBody('<div style="width:240px" align="center"><img src="images/loading1.gif"/></div>'); 
	_waitPanel.render(container); 
}

/*
 * Remove wait panel
 */

function YUI_CONTAINER_KillWait ()
{
	_waitPanel.hide();
	_waitPanel.destroy();
}

function CContainer ()
{
	/*
	 * Register a dialog with overlay manager
	 * @param dlg - A 'Module' based dialog or panel object
	 */
	
	CContainer.registerDlg  = function (dlg)
	{
		_overlayMgr.register(dlg);
	};
}

new CContainer;