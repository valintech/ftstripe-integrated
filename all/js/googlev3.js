/*
 * Google map class ( Google maps API 3 )
 */

function CGoogleMap (cfg)
{
	// Class constants
	CGoogleMap.RED = 0;
	CGoogleMap.BLUE = 1;
	CGoogleMap.GREEN = 2;
	CGoogleMap.YELLOW = 3;
	CGoogleMap.PURPLE = 4;

	if (typeof cfg == 'undefined')
		cfg = {};
	CGoogleMap.prototype._callback = (typeof cfg.callback != 'undefined' ? cfg.callback : null);	// Callback
	CGoogleMap.prototype._map = null;				// Google "Map" object
	CGoogleMap.prototype._geocoder = null;			// Google "Geocoder" object
	CGoogleMap.prototype._markers = [];				// List of markers
	CGoogleMap.prototype._baseUri = null;			// Base URI for icon images
	CGoogleMap.prototype._directionsService;		// Google "DirectionsService" object
	CGoogleMap.prototype._directionDisplay = [];	// Array of Google "Directionsrenderer" objects
	
	if (CGoogleMap._markerIconsCreated == false)
		CGoogleMap._createMarkerIcons();

	/*
	 * Show Google map
	 * 
	 * @param cfg    Config object ( elem = id. or reference of map container element )
	 */
	
	CGoogleMap.prototype.showMap = function (cfg)
	{
		var elem = (typeof cfg.elem == 'object' ? cfg.elem : document.getElementById(cfg.elem));
	    var myOptions = {center: new google.maps.LatLng(59.26, -12.3), zoom: 8, mapTypeId: google.maps.MapTypeId.ROADMAP};
		this._map = new google.maps.Map(elem, myOptions);
	    // Zoom in on GB!!!
		var viewBounds = new google.maps.LatLngBounds();
		viewBounds.extend(new google.maps.LatLng(59.26, -12.3));
		viewBounds.extend(new google.maps.LatLng(48.37, 5.4));
		this._map.panToBounds(viewBounds);
	};
	
	/*
	 * Set a marker onto the map
	 * 
	 * @param markerId  Marker id 1-n or -1 to create new marker
	 * @param locatio   Location object ( lat, lng, label )
	 * @param center    [optional] center map on marker
	 * @param colour    [optional] 0-4 ( red, green, blue, yellow, purple )
	 * @return          Marker id. to reference this marker
	 */
	
	CGoogleMap.prototype.setMarker = function (markerId, location, centre, colour)
	{
		if (this._map === null)
			return;
		if (colour === null)
			colour = 0;
		if (markerId == -1)
		{
			for (markerId = 0; markerId < this._markers.length; markerId++)
				if (this._markers[markerId] == null)
					break;
		}
		var marker = this._markers[markerId];
		var position = new google.maps.LatLng(location.lat, location.lng);
		if (marker == null)
		{
			marker = new google.maps.Marker({position: position, map: this._map, 
											 draggable: true, title: location.label,
											 icon: CGoogleMap._pinIcons[colour],
											 shadow: CGoogleMap._pinShadow});
			this._markers[markerId] = marker;
			// Add a listener for the "dragend" event
			var thisObj = this;
			google.maps.event.addListener(marker, "dragend", function (pos) {thisObj._markerDragEnd(this, pos);});
		}
		else
	    	marker.setPosition(position);
		if (typeof center != 'undefined' && center)
			this._map.setCenter(position);
		return markerId;
	};
	
	/*
	 * Remove marker from map
	 * @param markerId  Marker id 0-n
	 */

	CGoogleMap.prototype.deleteMarker = function (markerId)
	{
		if (this._markers[markerId] != null)
		{
			this._markers[markerId].setMap(null);
			delete this._markers[markerId];
			this._markers[markerId] = null;
		}
	};
	
	/*
	 * Reverse geocode an address
	 * 
	 * @param address        An address (string) or a location (object - CLocation)
	 * @param callback       Function to receive geocoded result. Receives single
	 *                       parameter of CLocation object
	 * @param defaultRegion  [optional] Region to favour, defaults to "UK"
	 */
	
	CGoogleMap.prototype.geocode = function (address, callback, region)
	{
		function _response (response, status)
		{
			var out = [];
			if (status == google.maps.GeocoderStatus.OK)
				out = thisObj.getResponseData(response);
			callback(out);
		}
		if (this._geocoder === null)
			this._geocoder = new google.maps.Geocoder();
		if (typeof region == 'undefined')
			region = 'UK';
		var req = {region: region};
		if (typeof address == 'string')
			req.address = address;
		else if (typeof address == 'object')
			req.location = new google.maps.LatLng(address.lat, address.lng);
		else
			return;
		var thisObj = this;
		this._geocoder.geocode(req, _response);
	};
	
	/*
	 * Return response data
	 * 
	 * @param response  A response object returned from 'geocode' method
	 * @return          Array of CLocation objects
	 */
	
	CGoogleMap.prototype.getResponseData = function (response)
	{
		var out = [];
		for (var i = 0; i < response.length; i++)
			out.push(new CLocation(response[i].formatted_address,
						  		   response[i].geometry.location.lat(),
						  		   response[i].geometry.location.lng(),
						  		   response[i]));
		return out;
	};

	/*
	 * GMarker "dragend" event handler
	 * 
	 * @param pos   An object containing a Google "latLng" object member
	 */
	
	CGoogleMap.prototype._markerDragEnd = function (marker, pos)
	{
		var thisObj = this;
		
		for (var markerId = 0; markerId < this._markers.length; markerId++)
			if (this._markers[markerId] == marker)
			{
				marker.setPosition(pos.latLng);
				var location = new CLocation('', pos.latLng.lat(), pos.latLng.lng());
				this.geocode(location, _response)
				break;
			}
		
		function _response (response)
		{
			if (response.length && thisObj._callback !== null)
				thisObj._callback('markerDrag', {id: markerId, location: response[0]});
		}
	};

	/*
	 * Calculate and set zoom level from applied points
	 * 
	 * @param zoomLevel  [optional] Preferred zoom level
	 */
	
	CGoogleMap.prototype.calculateZoom = function (zoomLevel)
	{
		var bounds = new google.maps.LatLngBounds();
		for (var markerId = 0; markerId < this._markers.length; markerId++)
			if (this._markers[markerId] != null)
				bounds.extend(this._markers[markerId].getPosition());
		if (bounds.isEmpty() == false)
		{
			this._map.fitBounds(bounds);
			zoomLevel = this._map.getZoom();
			if (zoomLevel > 12)
				zoomLevel = 12;
			this._map.setCenter(bounds.getCenter());
		}
		else if (zoomLevel == null)
			zoomLevel = 12;
		this._map.setZoom(zoomLevel);
	};
	
	/*
	 * Show a journey on the map
	 * 
	 * @param start   A CLocation object
	 * @param end     A CLocatiom object
	 * @param index   Entry index to use
	 * @return        Entry index
	 */
	
	CGoogleMap.prototype.showRoute = function (start, end, index)
	{
		if (this._map == null)
			return;
		if (index == null)
			index = 0;
		if (this._directionsService == null)
			this._directionsService = new google.maps.DirectionsService();
		if (this._directionDisplay[index] == null)
		{
			this._directionDisplay[index] = new google.maps.DirectionsRenderer();
			this._directionDisplay[index].setMap(this._map);
		}
		var directionsDisplay = this._directionDisplay[index];
		var request = {origin: new google.maps.LatLng(start.lat, start.lng),
					   destination: new google.maps.LatLng(end.lat, end.lng),
					   travelMode: google.maps.DirectionsTravelMode.DRIVING};
		function _complete (result, status)
		{
		    if (status == google.maps.DirectionsStatus.OK)
			      directionsDisplay.setDirections(result);
		}
		this._directionsService.route(request, _complete)
		return index;
	};
	
	/*
	 * Clear route or routes from map
	 * 
	 * @paran index  [optional] Route index to remove. Removes all
	 *                          routes if not given
	 */
	
	CGoogleMap.prototype.clearRoute = function (index)
	{
		if (index != null)
		{
			if (this._directionDisplay[index])
				this._directionDisplay[index].setMap(null);
			this._directionDisplay[index] = null;
			return;
		}
		for (var i = 0; i < this._directionDisplay.length; i++)
			this.clearRoute(i);
	};
}

/*
 * Location class
 * 
 * @param label            Friendly label to use for this location
 * @param lat              Location latitude
 * @param lng              Location longitude
 * @param googleResponse   [optional] Google "GeocoderResult" object
*/

function CLocation (label, lat, lng, googleResponse)
{
	CLocation.prototype.lat;
	CLocation.prototype.lng;
	CLocation.prototype.label;
	CLocation.prototype._googleResponse;
	
	/*
	 * Set location object
	 * 
	 * @param label  Location label
	 * @poarm lat    Lattitude
	 * @param lng    Longitude
	 */
	
	CLocation.prototype.setLocation = function (label, lat, lng)
	{
		this.label = (label == null ? '' : label);
		this.lat = (lat == null || lat != '' ? lat : '');
		this.lng = (lng == null || lng != '' ? lng : '');
	};

	this._googleResponse = googleResponse;
	this.setLocation(label, lat, lng);
	
	/*
	 * Return postcode for a Google response
	 * 
	 * @return  Postcode
	 */
	
	CLocation.prototype.googlePostCode = function ()
	{
		var postCode = '';
		if (this._googleResponse)
			for (var i = 0; postCode.length == 0 && i < this._googleResponse.address_components.length; i++)
				if (this._googleResponse.address_components[i].types[0] == CLocation.GOOGLE_POSTAL_CODE)
					postCode = this._googleResponse.address_components[i].long_name;
		return postCode;
	};
	
	/*
	 * Return address parts for a google response
	 * 
	 * @return  Array of address parts 
	 */
	
	CLocation.prototype.googleAddressParts = function (id)
	{
		var out = [];
		if (this._googleResponse)
			for (var i = 0; i < this._googleResponse.address_components.length; i++)
				out.push(this._googleResponse.address_components[i].long_name);
		return out;
	};
}

/*
 * Static members for CLocation class
 */

CLocation.GOOGLE_POSTAL_CODE = 'postal_code';				// Google response identifier for post code

/*
 * Create an instance of 'CLocation'
 * 
 * @param data   Initialise data
 * @return       A CLocation instance
 */

CLocation.instance = function (data)
{
	return new CLocation(data.label, data.lat, data.lng);
};

//Static members
CGoogleMap._markerIconsCreated = false;
CGoogleMap._pinIcons = [];
CGoogleMap._pinShadow = null;

/*
 * Create marker icons
 */

CGoogleMap._createMarkerIcons = function ()
{
	CGoogleMap._pinIcons[0] = "RedPin.png";			// red
	CGoogleMap._pinIcons[1] = "BluePin.png";		// blue
	CGoogleMap._pinIcons[2] = "GreenPin.png";		// green
	CGoogleMap._pinIcons[3] = "YellowPin.png";		// yellow
	CGoogleMap._pinIcons[4] = "PurplePin.png";		// purple

	parsedUri = parseUri(window.location.href);
	baseUri = parsedUri.protocol + "://" + parsedUri.host + parsedUri.directory + 'images/';	
	
	for (var i = 0; i < CGoogleMap._pinIcons.length; i++)
	{
		var tmp = new google.maps.MarkerImage(baseUri + CGoogleMap._pinIcons[i],
											  new google.maps.Size(32,32),
											  new google.maps.Point(0,0),
											  new google.maps.Point(15,31));
		CGoogleMap._pinIcons[i] = tmp;	
	}
	
	CGoogleMap._pinShadow = new google.maps.MarkerImage(baseUri + 'pinshadow.png',
														new google.maps.Size(42,32));
};
