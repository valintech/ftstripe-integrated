/*
 * Google map class ( Google maps API 2 )
 * 
 * @param cfg  [optional] Config. object ( 'callback' = callback for map events )
 */

function CGoogleMap (cfg)
{
	// Class constants
	CGoogleMap.RED = 0;
	CGoogleMap.BLUE = 1;
	CGoogleMap.GREEN = 2;
	CGoogleMap.YELLOW = 3;
	CGoogleMap.PURPLE = 4;
	
	if (typeof cfg == 'undefined')
		cfg = {};
	CGoogleMap.prototype._callback = (typeof cfg.callback != 'undefined' ? cfg.callback : null);	// Callback
	CGoogleMap.prototype._map = null;			// Google "Map" object
	CGoogleMap.prototype._geocoder = null;		// Google "Geocoder" object
	CGoogleMap.prototype._markers = [];			// List of markers
	CGoogleMap.prototype._baseUri = null;		// Base URI for icon images
	
	if (CGoogleMap._markerIconsCreated == false)
		CGoogleMap._createMarkerIcons();

	/*
	 * Show Google map
	 * 
	 * @param cfg    Config object ( elem = id. or reference of map container element )
	 */
	
	CGoogleMap.prototype.showMap = function (cfg)
	{
		var elem = (typeof cfg.elem == 'object' ? cfg.elem : document.getElementById(cfg.elem));
		this._map = new GMap2(elem);
		this._map.setCenter(new GLatLng(37.4419, -122.1419), 13);
	    this._map.setUIToDefault();

	    // Zoom in on GB!!!
		var viewBounds = new GLatLngBounds();
		viewBounds.extend(new GLatLng(59.26, -12.3));
		viewBounds.extend(new GLatLng(48.37, 5.4));
		var zoomLevel = this._map.getBoundsZoomLevel(viewBounds);
		this._map.setCenter(viewBounds.getCenter(), zoomLevel); 
	};
	
	/*
	 * Set a marker onto the map
	 * 
	 * @param markerId  Marker id 1-n or -1 to create new marker
	 * @param location  CLocation object
	 * @param center    [optional] center map on marker
	 * @param colour    [optional] 0-4 ( red, green, blue, yellow, purple )
	 * @return          Marker id. to reference this marker
	 */
	
	CGoogleMap.prototype.setMarker = function (markerId, location, centre, colour)
	{
		if (colour === null)
			colour = 0;
		if (markerId == -1)
		{
			for (markerId = 0; markerId < this._markers.length; markerId++)
				if (this._markers[markerId] == null)
					break;
		}
		var marker = this._markers[markerId];
	    var position = new GLatLng(location.lat, location.lng);
		if (marker == null)
		{
			marker = new GMarker(position, {draggable: true,
								 icon: CGoogleMap._pinIcons[colour]});
			this._map.addOverlay(marker);
			this._markers[markerId] = marker;
			// Add a listener for the "dragend" event
			var thisObj = this;
			GEvent.addListener(marker, "dragend", function (pos) {thisObj._markerDragEnd(this, pos);});
		}
		else
	    	marker.setLatLng(position);
		if (typeof center != 'undefined' && center)
			this._map.setCenter(position);
		return markerId;
	};
	
	/*
	 * Remove marker from map
	 * @param markerId  Marker id 0-n
	 */

	CGoogleMap.prototype.deleteMarker = function (markerId)
	{
		if (this._markers[markerId] != null)
		{
			this._map.removeOverlay(this._markers[markerId]);
			delete this._markers[markerId];
			this._markers[markerId] = null;
		}
	};
	
	/*
	 * Reverse geocode an address
	 * 
	 * @param address        An address (string) or a location (GLatLng)
	 * @param callback       Function to receive geocoded result. Receives single
	 *                       parameter of CLocation object
	 * @param defaultRegion  [optional] Region to favour, defaults to "UK"
	 */
	
	CGoogleMap.prototype.geocode = function (address, callback, region)
	{
		if (this._geocoder === null)
			this._geocoder = new GClientGeocoder();
		if (typeof region == 'undefined')
			region = 'uk';
		this._geocoder.setBaseCountryCode(region);
		var thisObj = this;
		this._geocoder.getLocations(address, _response);
		
		function _response (response)
		{
			var out = [];
			if (response.Status.code == G_GEO_SUCCESS)
				out = thisObj.getResponseData(response);
			callback(out);
		}
	};
	
	/*
	 * Return response data
	 * 
	 * @param response  A response object returned from 'geocode' method
	 * @return          Array of CLocation objects
	 */
	
	CGoogleMap.prototype.getResponseData = function (response)
	{
		var out = [];
		for (var i = 0; i < response.Placemark.length; i++)
			out.push(new CLocation(response.Placemark[i].address,
								   response.Placemark[i].Point.coordinates[1],
				      			   response.Placemark[i].Point.coordinates[0]));
		return out;
	};

	/*
	 * GMarker "dragend" event handler
	 * 
	 * @param pos   A "GLatLng" object containing marker position
	 */
	
	CGoogleMap.prototype._markerDragEnd = function (marker, pos)
	{
		var thisObj = this;
		
		for (var markerId = 0; markerId < this._markers.length; markerId++)
			if (this._markers[markerId] == marker)
			{
				marker.setLatLng(pos);
				this.geocode(pos, _response)
				break;
			}
		
		function _response (response)
		{
			if (response.length && thisObj._callback !== null)
				thisObj._callback('markerDrag', {id: markerId, location: response[0]});
		}
	};
	
	/*
	 * Calculate and set zoom level from applied points
	 * 
	 * @param zoomLevel  [optional] Preferred zoom level
	 */
	
	CGoogleMap.prototype.calculateZoom = function (zoomLevel)
	{
		var bounds = new GLatLngBounds();
		for (var markerId = 0; markerId < this._markers.length; markerId++)
			if (this._markers[markerId] != null)
				bounds.extend(this._markers[markerId].getLatLng());
		if (bounds.isEmpty() == false)
		{
			zoomLevel = this._map.getBoundsZoomLevel(bounds);
			if (zoomLevel > 12)
				zoomLevel = 12;
			this._map.setCenter(bounds.getCenter());
		}
		else if (zoomLevel == null)
			zoomLevel = 12;
		this._map.setZoom(zoomLevel);
	};
}

/*
 * Location class
*/

function CLocation (label, lat, lng)
{
	CLocation.prototype.lat;
	CLocation.prototype.lng;
	CLocation.prototype.label;
	
	/*
	 * Set location object
	 * 
	 * @param label  Location label
	 * @poarm lat    Lattitude
	 * @param lng    Longitude
	 */
	
	CLocation.prototype.setLocation = function (label, lat, lng)
	{
		this.label = (label == null ? '' : label);
		this.lat = (label == null || label != '' ? lat : '');
		this.lng = (label == null || label != '' ? lng : '');
	};

	/*
	 * Create object JSON data
	 * 
	 * @param data  A JSON encoded CLocation object
	 */
	
	CLocation.createFromJson = function (data)
	{
		var data = COMMON.fromJson(data);
		var o = new CLocation();
		o.setLocation(data.label, data.lat, data.lng);
		return o;
	};
	
	this.setLocation(label, lat, lng);
}

new CLocation();

// Static members
CGoogleMap._markerIconsCreated = false;
CGoogleMap._pinIcons = [];

/*
 * Create marker icons
 */

CGoogleMap._createMarkerIcons = function ()
{
	CGoogleMap._pinIcons[0] = "RedPin.png";			// red
	CGoogleMap._pinIcons[1] = "BluePin.png";		// blue
	CGoogleMap._pinIcons[2] = "GreenPin.png";		// green
	CGoogleMap._pinIcons[3] = "YellowPin.png";		// yellow
	CGoogleMap._pinIcons[4] = "PurplePin.png";		// purple

	parsedUri = parseUri(window.location.href);
	baseUri = parsedUri.protocol + "://" + parsedUri.host + parsedUri.directory + 'images/';	
	
	for (var i = 0; i < CGoogleMap._pinIcons.length; i++)
	{
		var tmp = new GIcon();
		tmp.image = baseUri + CGoogleMap._pinIcons[i];			// * The image URL of the icon
		tmp.shadow = baseUri + 'pinshadow.png';					// * The shadow image URL of the icon.
		tmp.iconSize = new GSize(32,32);						// * The pixel size of the foreground image of the icon.
		tmp.shadowSize = new GSize(42,32);						// * The pixel size of the shadow image.
		tmp.iconAnchor = new GPoint(15,31);						// * The pixel coordinate relative to the top left corner of the icon
																//   image at which this icon is anchored to the map.
		tmp.infoWindowAnchor = new GPoint(16,16);				// * The pixel coordinate relative to the top left corner of the icon image
																//   at which the info window is anchored to this icon.
		tmp.printImage = baseUri + CGoogleMap._pinIcons[i];		// * The URL of an alternate foreground icon image used for printing on browsers
																//   incapable of handling the default GIcon.image. Versions of IE typically require
																//   an alternative image in these cases as they cannot print the icons as transparent
																//   PNGs. Note that browsers capable of printing the default image will ignore this property.
		tmp.mozPrintImage = baseUri + 'pinshadow.png';			// * The URL of an alternate non-transparent icon image used for printing on browsers incapable
																//   of handling either transparent PNGs (provided in the default GIcon.image) or transparent GIFs
																//   (provided in GIcon.printImage). Older versions of Firefox/Mozilla typically require
																//   non-transparent images for printing. Note that browsers capable of printing the default
																//   image will ignore this property.
		tmp.printShadow = baseUri + 'pinshadow.png';			// * The URL of the shadow image used for printed maps. It should be a GIF image since most
																//   browsers cannot print PNG images.
		tmp.transparent = "";									// * The URL of a virtually transparent version of the foreground icon image used to capture
																//   click events in Internet Explorer. This image should be a 24-bit PNG version of the main
																//   icon image with 1% opacity, but the same shape and size as the main icon.
		tmp.imageMap = [ 0, 0, 32, 32 ];						// * An array of integers representing the x/y coordinates of the image map we should use to
																//   specify the clickable part of the icon image in browsers other than Internet Explorer.
		tmp.maxHeight = 4;										// * Specifies the distance in pixels in which a marker will visually "rise" vertically when dragged.
		tmp.dragCrossImage = '';								// * Specifies the cross image URL when an icon is dragged.
		tmp.dragCrossSize = GSize(0,0);							// * Specifies the pixel size of the cross image when an icon is dragged.
		tmp.dragCrossAnchor = GSize(0,0);						// * Specifies the pixel coordinate offsets (relative to the iconAnchor) of the cross image when
																//   an icon is dragged.
		CGoogleMap._pinIcons[i] = tmp;	
	}
}
