$(document).ready(function() {

    var _searchedGroupMember = '';
    var  _groups='';
    
    var _currentGroup = 0;
    $('#myGroups').on('change', function() {
     
        _currentGroup = this.value;
        $("#search_group_member").tokenInput("clear");

    });


    function getData()
    {
        return "register.php?op=search_group_member&g=" + _currentGroup;
    }

    $("#demo-input-prevent-duplicates").change(function() {

        $('#search_group .token-input-input-token').insertAfter('#search_group .token-input-list li:last');
        // show_dropdown();
        $("#token-input-demo-input-prevent-duplicates").focus();
        // $('#search_group .token-input-list').attr('style', 'top:'+$("#search_group .token-input-list").offset().top + $("#search_group .token-input-list").outerHeight()+' !important');


        _searchedSelectedGroup =  $("#demo-input-prevent-duplicates").val();
        $('#grouped_id').val(_searchedSelectedGroup);
    });
    
    /* if (_groups != '0') {
        _groups = COMMON.fromJson(_groups);
        var searches = new Array();
        var j=0;
        if (typeof _groups.group_tag != 'undefined' && _groups.group_tag.length > 0) {
            while (i < _groups.group_tag.length) {
                if (_groups.type[i] === "s") {
                    var search = {};
                    search['id'] = parseInt(_groups.group_id[i]);
                    search['name'] = _groups.group_tag[i];
                    searches[j] = search;
                    _searchedSelectedGroup += ',' + _groups.group_id[i];
                    j++;
                }

                i++;
            }
        }
    }
    else
    {*/
        searches = "";
       // }console.log(JSON.stringify(searches));
    $("#demo-input-prevent-duplicates").tokenInput("register.php?op=search_group_list", {
        method: "POST", preventDuplicates: true,
        prePopulate: JSON.parse(JSON.stringify(searches))
    });

    $("#search_group_member").change(function() {

        $('#group_member .token-input-input-token').insertAfter('#group_member .token-input-list li:last');
        // show_dropdown();
        $("#token-input-search_group_member").focus();
        // $('#group_member .token-input-list').attr('style', 'top:'+$("#group_member .token-input-list").offset().top + $("#group_member .token-input-list").outerHeight()+' !important');

        _searchedGroupMember = $("#search_group_member").val();
        document.getElementById("searched_group_members").value = _searchedGroupMember;


    });


    $("#search_group_member").tokenInput(getData, {
        method: "POST", preventDuplicates: true
    });


    $("#search_group_memberforadmin").change(function() {
        $("#search_group_memberforadmin").tokenInput("remove", {id: _searchedGroupMember});
        $('#group_member_toadmin .token-input-input-token').insertAfter('#group_member_toadmin .token-input-list li:last');
        // show_dropdown();
        $("#token-input-search_group_memberforadmin").focus();
        // $('#group_member .token-input-list').attr('style', 'top:'+$("#group_member .token-input-list").offset().top + $("#group_member .token-input-list").outerHeight()+' !important');

        _searchedGroupMember = $("#search_group_memberforadmin").val();
        document.getElementById("searched_group_member_for_admin").value = _searchedGroupMember;


    });


    $("#search_group_memberforadmin").tokenInput(getData, {
        method: "POST", preventDuplicates: true
    });



});


