var _googleMap;						// A CGoogleMap object
var _markerIds = [-1, -1];			// Marker ids.
var _locations;						// Array of obects [0]=source, [1]=destination
									// {id: 0 or 1, location: CLocation object, inp: input element}
var _journeyData;					// Array of CSchedule objects returned from query
var _journeyMenu;					// A YUI "Menu" object
var _mouseXy;						// Mouse location [x, y]
var _createBtn = null;				// YAHOO.widget.Button object for create buttons
var search_submit=1;
/*
 * "onload" event
 */

function bodyOnload ()
{
	_googleMap = new CGoogleMap({callback: googleMapCallback});
	_googleMap.showMap({elem: 'map_canvas'});
	YAHOO_Require(['json', 'connection', 'container', 'menu', 'button'], _yuiLoaded);
}

/*
 * YUI modules loaded
 */

function _yuiLoaded ()
{
	_showJourneys(_locations[0], _locations[1],
				  function(){if (_journeyRequestId) _requestRide(_journeyRequestId);});
	var inp = DOM_getElem('searchBtn');
	new YAHOO.widget.Button(inp, {type: 'submit', label: inp.value});
	var inp = DOM_getElem('createBtn');
	if (inp)
	{
		var btn = new YAHOO.widget.Button('createBtn', {type: 'push', label: inp.value});
		btn.on('click', function(){_arrangeTravel();});
	}
}

/*
 * Geocode a list of locations
 * 
 * @param placeList   List of CLocation objects to reverse geocode.
 * @param callback    [optional] function to call when complete
 * @param index       [optional] Used internally by the method for recursion. Do not set.
 */

function _geocodeLocations (locationList, callback, index)
{
	if (index == null)
		index = 0;
	
	if (index >= locationList.length)
	{
		if (callback != null)
			callback();
		return;
	}
	
	var location = locationList[index];
	if (location.label == '')
	{
		_geocodeLocations(locationList, callback, index + 1);
		return;
	}
	
	_googleMap.geocode(location.label, _callback);
	
	function _callback (r)
	{
		if (r.length)
		{
			locationList[index].lat = r[0].lat;
			locationList[index].lng = r[0].lng;
			locationList[index].label = r[0].label;
		}
		_geocodeLocations(locationList, callback, index + 1);
	}
}

/*
 * Set markers for a journey
 * 
 * @param fromLocation   A CLocation object
 * @param toLocation     A CLocation object
 * @param zoomIn         [optional] true to zoom in to marker area. default = true
 */

function _setJourneyMarkers (fromLocation, toLocation, zoomIn)
{
	_googleMap.deleteMarker(0);
	if (fromLocation.label != '')
	{
		document.getElementById('source').value = fromLocation.label;
		_googleMap.setMarker(0, fromLocation, false, CGoogleMap.RED);
	}

	_googleMap.deleteMarker(1);
	if (toLocation.label != '')
	{
		document.getElementById('destination').value = toLocation.label;
		_googleMap.setMarker(1, toLocation, false, CGoogleMap.BLUE);
	}

	if (zoomIn == null || zoomIn)
		_googleMap.calculateZoom();
}

/*

 * Define columns for 'YAHOO.widget.DataTable'
 * 
 * @return Array of 'YAHOO.widget.Column' objects
 * @see <a href="http://developer.yahoo.com/yui/docs/YAHOO.widget.Column.html">Here</a>
 */

function _defineDtColummns ()
{
	var cols = [];
	cols.push({key: '_icon', label: '', formatter: _iconFormatter});
 	cols.push({key: CSchedule.START_ADDRESS, label: "From", width: 200, formatter: _fromFormatter});
 	cols.push({key: CSchedule.DESTINATION_ADDRESS, label: "To", width: 200});
 	return cols;
}

/*
 * Define schema for 'YAHOO.widget.DataSource'
 * 
 * @return Array of fields objects
 * @see <a href="http://developer.yahoo.com/yui/docs/YAHOO.util.DataSourceBase.html#property_responseSchema">Here</a>
 */

function _defineDsSchema ()
{
	var fields = [];
	fields.push({key: CSchedule.START_ADDRESS});
	fields.push({key: CSchedule.START_LAT});
	fields.push({key: CSchedule.START_LNG});
	fields.push({key: CSchedule.DESTINATION_LAT});
	fields.push({key: CSchedule.DESTINATION_LNG});
	fields.push({key: CSchedule.DESTINATION_ADDRESS});
	fields.push({key: CSchedule.JOURNEY_TYPE});
	var schema = {};
	schema.fields = fields;
	return schema;
}

/*
 * Cell formatter (hyperlink cell text with Google link)
 */

function _fromFormatter (el, record, column, data)
{
	var tmp = '<a href="javascript:CArrangeTravelResults._googleShow(%1,%2,%3,%4)">%5</a>';
	tmp = tmp.replace(/%1/, record.getData(CSchedule.START_LAT));
	tmp = tmp.replace(/%2/, record.getData(CSchedule.START_LNG));
	tmp = tmp.replace(/%3/, record.getData(CSchedule.DESTINATION_LAT));
	tmp = tmp.replace(/%4/, record.getData(CSchedule.DESTINATION_LNG));
	tmp = tmp.replace(/%5/, record.getData(CSchedule.START_ADDRESS));
	el.innerHTML = tmp;
}

/*
 * Cell formatter (appropriate icon)
 */

function _iconFormatter (el, record, column, data)
{
	var journeyType = record.getData(CSchedule.JOURNEY_TYPE);
	var icon = (journeyType == CSchedule.JOURNEY_TYPE_REGULAR ? 'car.gif' : 'person.gif');
	var tmp = '<img src="%2/images/%1" border="0"/>'.replace(/%1/, icon);
	tmp = tmp.replace(/%2/, CConfig.CONTENT_DIR);
	el.innerHTML = tmp;
}

/*
 * Callback for "CGoogleMap" events
 * 
 * @param eventType  Event type
 * @param eventData  Event data
 * 
 */

function googleMapCallback (eventType, eventData)
{
	switch (eventType)
	{
		// eventData:-
		// 'id' - Marker id.
		// 'location' - CLocation object
		case "markerDrag":
		if (eventData.id == _markerIds[0])
		{
			document.getElementById('source').value = eventData.location.label;
			_locations[0].location = eventData.location;
		}
		else if (eventData.id == _markerIds[1])
		{
			document.getElementById('destination').value = eventData.location.label;
			_locations[1].location = eventData.location;
		}
		break;
	}
}

/*
 * Show start/end of google map
 */

_googleShow = function (journey)
{
	_googleMap.deleteMarker(_markerIds[0]);
	_googleMap.deleteMarker(_markerIds[1]);
	var lat = journey[CSchedule.START_LAT];
	var lng = journey[CSchedule.START_LNG];
	var label = journey[CSchedule.START_ADDRESS];
	var startLoc = new CLocation(label, lat, lng);
	lat = journey[CSchedule.DESTINATION_LAT];
	lng = journey[CSchedule.DESTINATION_LNG];
	label = journey[CSchedule.DESTINATION_ADDRESS];
	var endLoc = new CLocation(label, lat, lng);
	_markerIds[0] = _googleMap.setMarker(0, startLoc, false, CGoogleMap.RED);
	_markerIds[1] = _googleMap.setMarker(1, endLoc, false, CGoogleMap.BLUE);
	this._googleMap.calculateZoom();
}

/*
 * "onsubmit" event for <form> element
 */

function onSubmit ()
{       search_submit=2;
	_locations[0].setLocation(document.getElementById('source').value);
	_locations[1].setLocation(document.getElementById('destination').value);
	_showJourneys(_locations[0], _locations[1]);
	return false;
}

/*
 * Find journeys and display in list
 * 
 * @param from       CLocation object
 * @param to         CLocation object
 * @param callback   [optional] called when Google goecoding completed
 */

function _showJourneys (from, to, callback)
{
	YUI_CONTAINER_Wait('Finding journeys ... ');
	var locations = [from, to];
	_geocodeLocations(locations, _geocodeDone);

	function _geocodeDone ()
	{
		document.getElementById('source').value = locations[0].label;
		document.getElementById('destination').value = locations[1].label;
		_setJourneyMarkers(locations[0], locations[1]);
		_getJourneyList(locations[0], locations[1], _getJourneyListDone);
	}

	function _getJourneyListDone ()
	{
		YUI_CONTAINER_KillWait();
		if (callback != null)
			callback();
	}
}

/*
 * Retrieve journey list
 * 
 * @param source  		Source location ( A CLocation object)
 * @param destination   Destination location ( A CLocation object )
 * @param callback      [optional] function to call when complete
 */

function _getJourneyList (source, destination, callback)
{
    var group_list_id=document.getElementById('grouped_id').value;
	_googleMap.clearRoute();
	CConnection.doGet('search_results.php',
					   {group_list: group_list_id,op: 'search', source: COMMON.toJson(source), destination: COMMON.toJson(destination)},
				       _response);
	
	// XmlHttpResponse - r = JS object
	function _response (r)
	{call_automatic();
		CXPlat.setContent(document.getElementById('results'), r['html']);
		_journeyData = r.journeys;
		if (callback != null)
			callback();
	}
}

/*
 * "onblur" event handler
 * 
 * @param[in] e   "event" object
 */

function onBlur (e)
{
	var elem = CXPlat.eventSrcElement(e);
	switch (elem.id)
	{
		case 'source':
		//_locations[0].location = new CLocation('', '', elem.value);
		//_setLocations(_locations);
		break;
		
		case 'destination':
		//_locations[1].location = new CLocation('', '', elem.value);
		//_setLocations(_locations);
		break;
		
		default:
		break;
	}
}

/*
 * "onmouseenter" event handler
 */

function onMouseOver (e)
{
	var elem = CXPlat.eventSrcElement(e);
	if (elem.id.startsWith('result'))
	{
		var index = parseInt(elem.id.substring('result'.length, 99), 10);
		//_googleShow(_journeyData[index]);
	}
}

/*
 * Journey link clicked
 * 
 * @param index  Journey index 0-n
 */

function _journeyClick (index)
{
	// Dont use menu code yet, just jump straight to request code
	var journey = _journeyData[index];
	var journeyId = journey[CSchedule.ID];
	_requestRide(journeyId);
	return;
	
	var journey = _journeyData[index];
	var journeyId = journey[CSchedule.ID];
	if (_journeyMenu == null)
	{
		div = document.createElement('div');
		div.id = 'journeyMenu';
		_journeyMenu = new YAHOO.widget.Menu(div)
		var items = [];
		items.push({ text: CCommon.msg(1003), url: 'javascript:_requestRide(%1)'.format(journeyId)})
		//items.push({ text: CCommon.msg(1004), url: 'javascript:_moreInfo(%1)'.format(journeyId)})
		_journeyMenu.addItems(items);
		_journeyMenu.render(document.body);
	}
	else
	{
		var item = _journeyMenu.getItem(0);
		item.cfg.setProperty('url', 'javascript:_requestRide(%1)'.format(journeyId));
		item = _journeyMenu.getItem(1);
		item.cfg.setProperty('url', 'javascript:_moreInfo(%1)'.format(journeyId));
	}
	_journeyMenu.cfg.setProperty('x', _mouseXy[0]);
	_journeyMenu.cfg.setProperty('y', _mouseXy[1]);
	_journeyMenu.show();
}

/*
 * "onmousemove" event
 */

function onMouseMove (e)
{
	e = CXPlat.eventObj(e);
	_mouseXy = [
	           CXPlat.getScrollX(document, window) + e.clientX,
	           CXPlat.getScrollY(document, window) + e.clientY
	           ];
}

/*
 * Request a ride from a journey
 * 
 * @param journeyId   Journey id.
 */

function _requestRide (journeyId)
{
	var referUrl = 'search_results.php?journeyId=%3&source=%1&destination=%2'.format(
			DOM_getElem('source').value, DOM_getElem('destination').value, journeyId);

	function _buttonClick (keyNo)
	{
		// Register?
		if (keyNo == 0)
			window.location.href = 'register.php?refer=' + encodeURIComponent(referUrl);
	}

	if (_isValidSession == 0)
		window.location.href = 'login.php?refer=' + encodeURIComponent(referUrl);
	else if (_isUserRegistered)
		new CRequestRide(journeyId).show();
	else
		new YUI_DIALOG_Popup(CCommon.msg(1000), CCommon.msg(1005), 'info',
							 [CCommon.msg(1001), CCommon.msg(1002)], _buttonClick);
}

/*
 * Load "Arrange Travel" form
 */

function _arrangeTravel ()
{
	var referUrl = 'search.php?from=%1&to=%2'.format(DOM_getElem('source').value,
															 DOM_getElem('destination').value);

	function _buttonClick (keyNo)
	{
		// Register?
		if (keyNo == 0)
			CCommon.redirect('register.php?refer=' + encodeURIComponent(referUrl));
	}
	
	if (_isValidSession == 0)
		CCommon.redirect('login.php?refer=' + encodeURIComponent(referUrl));
	else if (_isUserRegistered)
		CCommon.redirect('search.php?from=' + encodeURIComponent(DOM_getElem('source').value)
						 + '&to=' + encodeURIComponent(DOM_getElem('destination').value));
	else
		new YUI_DIALOG_Popup(CCommon.msg(1000), CCommon.msg(1005), 'info',
							 [CCommon.msg(1001), CCommon.msg(1002)], _buttonClick);
}

/*
 * Show journey on Google map
 * 
 * @param index  Journey index
 */

function _showRoute (index)
{
	var journey = _journeyData[index];
	var journeyId = journey[CSchedule.ID];
	_googleMap.showRoute(new CLocation('', journey.from_lat, journey.from_lng),
						 new CLocation('', journey.to_lat, journey.to_lng));
}

// Create datatable
/*
		var myDataSource = new YAHOO.util.LocalDataSource(r.journeyData);
		myDataSource.responseType = YAHOO.util.XHRDataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = _defineDsSchema();
		var myDataTable = new YAHOO.widget.DataTable("results", _defineDtColummns(), myDataSource, {selectionMode: 'single'});
*/
$(document).ready(function() {

    var _searchedGroupMember = '';
    var  _groups='';
    
    var _currentGroup = 0;
    $('#myGroups').on('change', function() {
     
        _currentGroup = this.value;
        $("#search_group_member").tokenInput("clear");

    });


    function getData()
    {
        return "register.php?op=search_group_member&g=" + _currentGroup;
    }

    $("#demo-input-prevent-duplicates").change(function() {

        $('#search_group .token-input-input-token').insertAfter('#search_group .token-input-list li:last');
        // show_dropdown();
        $("#token-input-demo-input-prevent-duplicates").focus();
        // $('#search_group .token-input-list').attr('style', 'top:'+$("#search_group .token-input-list").offset().top + $("#search_group .token-input-list").outerHeight()+' !important');


        _searchedSelectedGroup =  $("#demo-input-prevent-duplicates").val();
        $('#grouped_id').val(_searchedSelectedGroup);
    });
   
  /* if (_groups != '0') {
        _groups = COMMON.fromJson(_groups);
        var searches = new Array();
        var j=0;
        if (typeof _groups.group_tag != 'undefined' && _groups.group_tag.length > 0) {
            while (i < _groups.group_tag.length) {
                if (_groups.type[i] === "s") {
                    var search = {};
                    search['id'] = parseInt(_groups.group_id[i]);
                    search['name'] = _groups.group_tag[i];
                    searches[j] = search;
                    _searchedSelectedGroup += ',' + _groups.group_id[i];
                    j++;
                }

                i++;
            }
        }
    }
    else
    {*/
        searches = "";
      // }
 /*   $("#demo-input-prevent-duplicates").tokenInput("register.php?op=search_group", {
        method: "POST", preventDuplicates: true,
        prePopulate: JSON.parse(JSON.stringify(searches))
    });*/

    $("#search_group_member").change(function() {

        $('#group_member .token-input-input-token').insertAfter('#group_member .token-input-list li:last');
        // show_dropdown();
        $("#token-input-search_group_member").focus();
        // $('#group_member .token-input-list').attr('style', 'top:'+$("#group_member .token-input-list").offset().top + $("#group_member .token-input-list").outerHeight()+' !important');

        _searchedGroupMember = $("#search_group_member").val();
        document.getElementById("searched_group_members").value = _searchedGroupMember;


    });


    $("#search_group_member").tokenInput(getData, {
        method: "POST", preventDuplicates: true
    });


    $("#search_group_memberforadmin").change(function() {
        $("#search_group_memberforadmin").tokenInput("remove", {id: _searchedGroupMember});
        $('#group_member_toadmin .token-input-input-token').insertAfter('#group_member_toadmin .token-input-list li:last');
        // show_dropdown();
        $("#token-input-search_group_memberforadmin").focus();
        // $('#group_member .token-input-list').attr('style', 'top:'+$("#group_member .token-input-list").offset().top + $("#group_member .token-input-list").outerHeight()+' !important');

        _searchedGroupMember = $("#search_group_memberforadmin").val();
        document.getElementById("searched_group_member_for_admin").value = _searchedGroupMember;


    });


    $("#search_group_memberforadmin").tokenInput(getData, {
        method: "POST", preventDuplicates: true
    });



});
function call_automatic()
{
    var i = 0;
   if (_groups != '0' && search_submit==1) {
        _groups = COMMON.fromJson(_groups);
        var searches = new Array();
        var j=0;
        if (typeof _groups.group_tag != 'undefined' && _groups.group_tag.length > 0) {
            while (i < _groups.group_tag.length) {
                //if (_groups.type[i] === "s") {
                    var search = {};
                    search['id'] = parseInt(_groups.group_id[i]);
                    search['name'] = _groups.group_tag[i];
                    searches[j] = search;
                   // _searchedSelectedGroup += ',' + _groups.group_id[i];
                    j++;
               // }

                i++;
            }
        }
    }
    else
    {
        searches = "";
       }
       if(search_submit==1){
    $("#demo-input-prevent-duplicates").tokenInput("register.php?op=search_group_list", {
        method: "POST", preventDuplicates: true,
        prePopulate: JSON.parse(JSON.stringify(searches))
    });
    }
}