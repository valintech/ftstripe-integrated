var _journeyData;					// Array of CJourney objects returned from query

/*
 * "onload" event
 */

function bodyOnload ()
{
	YAHOO_Require(['json', 'connection', 'container', 'menu', 'button'], _yuiLoaded);
}

/*
 * YUI modules loaded
 */

function _yuiLoaded ()
{
	_showJourneys();
}

/*
 * Find journeys and display in list
 * 
 * @param from       CLocation object
 * @param to         CLocation object
 * @param callback   [optional] called when Google goecoding completed
 */

function _showJourneys (callback)
{
	YUI_CONTAINER_Wait('Finding journeys ... ');
	_getJourneyList(_getJourneyListDone);

	function _getJourneyListDone ()
	{
		YUI_CONTAINER_KillWait();
		if (callback != null)
			callback();
	}
}

/*
 * Retrieve journey list
 * 
 * @param source  		Source location ( A CLocation object)
 * @param destination   Destination location ( A CLocation object )
 * @param callback      [optional] function to call when complete
 */

function _getJourneyList (callback)
{
	CConnection.doGet('mycars.php', {op: 'search'}, _response);
	
	// XmlHttpResponse - r = JS object
	function _response (r)
	{
		CXPlat.setContent(document.getElementById('results'), r['html']);
		_journeyData = r.journeys;
		if (callback != null)
			callback();
	}
}

/*
 * Delete a journey
 * 
 * @param index   _journeyData[] index
 */

function _deleteJourney (index)
{
	var journey = _journeyData[index];
	var journeyId = journey[CJourney.ID];
	var url = 'mycars.php?op=deleteJourney&journeyId=%1'.format(journeyId);

	function _buttonClick (keyNo)
	{
		if (keyNo == 0)
			window.location.href = url;
	}

	new YUI_DIALOG_Popup(CCommon.msg(1000), CCommon.msg(1005), 'warn',
							 [CCommon.msg(1001), CCommon.msg(1002)], _buttonClick);
}

/*
 * Search for matches on a journey
 * 
 * @param index   _journeyData[] index
 */

function _searchJourney (index)
{
	var journey = _journeyData[index];

	var referUrl = 'arrange_travel_results.php?source=%1&destination=%2'.format(encodeURIComponent(journey[CJourney.START_ADDRESS]), encodeURIComponent(journey[CJourney.DESTINATION_ADDRESS]));

	function _buttonClick (keyNo)
	{
		// Register?
		if (keyNo == 0)
			CCommon.redirect('register.php?refer=' + encodeURIComponent(referUrl));
	}

	if (_isValidSession == 0)
		CCommon.redirect('login.php?refer=' + encodeURIComponent(referUrl));
	else if (_isUserRegistered)
		CCommon.redirect('arrange_travel_results.php?source=' + encodeURIComponent(journey[CJourney.START_ADDRESS])
						 + '&destination=' + encodeURIComponent(journey[CJourney.DESTINATION_ADDRESS]));
	else
		new YUI_DIALOG_Popup(CCommon.msg(1000), CCommon.msg(1005), 'info',
							 [CCommon.msg(1001), CCommon.msg(1002)], _buttonClick);
}

/*
 * "onmousemove" event
 */

function onMouseMove (e)
{
	e = CXPlat.eventObj(e);
	_mouseXy = [
	           CXPlat.getScrollX(document, window) + e.clientX,
	           CXPlat.getScrollY(document, window) + e.clientY
	           ];
}

