var Y, yuiLoaded = false;

/*
 * "onload" event for <body> element
 */

function onLoad()
{
    YAHOO_Require(['button', 'connection', 'container'], _yuiLoaded);
}

/*
 * Callback executed when YUI modules loaded
 */


function _yuiLoaded()
{
    yuiLoaded = true;
    new YAHOO.widget.Button('loginButton', {type: 'submit'});
    /*
     // Load login fields from cookie if required
     var cookie = CCommon.getCookie('ers');
     cookie  = (cookie ? CCommon.fromJson(Base64.decode(cookie)) : {});
     if (cookie.remember == 1)
     {
     DOM_setAttribute('email', 'value', cookie.user);
     DOM_setAttribute('password', 'value', cookie.pwd);
     DOM_setAttribute('rememberMe', 'checked', true);
     }
     */
    DOM_getElem('email').focus();
}

/*
 * "onsubmit" event
 */

function onSubmit()
{


    /*
     var cookie;
     if (DOM_getElem('rememberMe').checked)
     cookie = {remember: 1, user: DOM_getElem('email').value,
     pwd: DOM_getElem('password').value}
     else
     cookie = {remember: 0}
     CCommon.setCookie('ers', Base64.encode(CCommon.toJson(cookie)));
     */

    CConnection.doPost('login.php', {op: 'signin'}, _response, null, 'loginForm');

    // XmlHttpRequest response handler
    function _response(o)
    {
        if (o.errorText)
        {
            var buttonText = [];
            if (o.errorId == NOT_VERIFIED_ERROR)
                buttonText.push(_msgList.msg(1016));
            buttonText.push(_msgList.msg(1015));
            new YUI_DIALOG_Popup(o.errorText, _msgList.msg(1014), 'block', buttonText,
                    (buttonText.length == 2 ? function(keyNo) {
                        if (keyNo == 0)
                            sendVerifyEmail();
                    } : null));
        }
        else
        {


            var url = _refer.getUrl();
            var parameter = getUrlParameters("approveparam", "", true);
             var approve_parameter = getUrlParameters("approveownerparam", "", false);
           
            if (parameter) {
                CCommon.redirect('approve.php?approveparam=' + parameter);
            }else if (approve_parameter) {
           
                CCommon.redirect('ownershipappoval.php?approveownerparam=' + approve_parameter);
            } else {

                CCommon.redirect(url == '' ? 'index.php' : url);
            }

        }
    }

    // Stop standard event behaviour
    return false;
}

/*
 * Send verification email
 */

function sendVerifyEmail()
{
    CConnection.doGet('login.php', {op: 'sendVerifyEmail'}, _response, null, 'loginForm');

    function _response(o)
    {
        if (o.errorText)
            new YUI_DIALOG_Popup(o.errorText, _msgList.msg(1014), 'block',
                    [_msgList.msg(1011)]);
        else if (o.infoText)
            new YUI_DIALOG_Popup(o.infoText, _msgList.msg(1007), 'info',
                    [_msgList.msg(1011)]);
    }
}

/*
 * Change password request
 */

function changePassword()
{$( "#forgot_pass" ).prop( "disabled", true );
    CConnection.doGet('forgot_password.php', {op: 'changePassword'}, _response, null, 'loginForm');

    // XmlHttpRequest response handler
    function _response(o)
    {
        if (o['errorText'])
            new YUI_DIALOG_Popup(o.errorText, _msgList.msg(1014), 'block',
                    [_msgList.msg(1011)]);
        else
            new YUI_DIALOG_Popup(_msgList.msg(1006), _msgList.msg(1007),
                    'info', _msgList.msg(1011));
                    $( "#forgot_pass" ).prop( "disabled", false );
    }
}


function getUrlParameters(parameter, staticURL, decode) {
   
  try
           {  var currLocation = (staticURL.length) ? staticURL : window.location.search,          
           
            parArr = currLocation.split("?")[1].split("&"),
            returnBool = true;
            }
            catch(err){
            
            return false;
            }

    for (var i = 0; i < parArr.length; i++) {
        parr = parArr[i].split("=");
        if (parr[0] == parameter) {
            return (decode) ? decodeURIComponent(parr[1]) : parr[1];
            returnBool = true;
        } else {
            returnBool = false;
        }
    }

    if (!returnBool)
        return false;
}