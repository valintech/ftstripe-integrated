<?php 
require_once('includes/config.inc');

function get_facebook_cookie($app_id, $application_secret) {
    $args = array();
    parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
    ksort($args);
    $payload = '';
    foreach ($args as $key => $value) {
        if ($key != 'sig') {
            $payload .= $key . '=' . $value;
        }
    }
    if (md5($payload . $application_secret) != $args['sig']) {
      return null;
    }
    return $args;
}

$cookie = get_facebook_cookie(CConfig::FB_APP_ID, CConfig::FB_SECRET);
echo 'The ID of the current user is ' . $cookie['uid'];

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
		<script type="text/javascript" src="js/facebook.js"></script>
	</head>
	<body>
    <?php if ($cookie) { ?>
      Your user ID is <?= $cookie['uid'] ?>
    <?php } else { ?>
      <fb:login-button perms="email">Install Example App</fb:login-button>
    <?php } ?>
	
		<div id="fb-root"></div>
		<script type="text/javascript">
		new CFacebook(<?php echo sprintf('{isActive: 1, appId: "%s"}', CConfig::FB_APP_ID) ?>);
		CFacebook.init();

	      FB.Event.subscribe('auth.login', function(response) {
	          // Reload the application in the logged-in state
	          window.top.location = 'http://apps.facebook.com/erideshare/';
	        });
		</script>
		<br/><br/><a href="index.php">Go to First Thumb UK</a>
	</body>
</html>
