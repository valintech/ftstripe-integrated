
var DataFormatVersion = 1;

var base_url = '';
var wsurl = '';
var notification_id = '';

var save_signin_in_browser = true;
var IdleTimeoutInSeconds = 0;		// Zero means no idle timeout
var RefreshPeriodInSeconds = 60;
var msgTable;
var sessionId;
var signedIn = 0;
var active = '';
var IdleCounter = 0;
var RefreshCounter = 0;
var LastMsgIdReceived = 0;
var cur_page = '';
var cookieData;
var globalData;
var im_detail_contact = '';
var im_detail_group;
var pushRegistrationTimeout = 1000;
var WsMsgId = 1;
var splash_time = 0;
var splash_cnt = 0;

var WsRequests = {
	Active	: false,
	Queue		: [],
	Pending	: {},
};

var monthNames = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

$(function() {
	var $menu = $('nav#menu');

	$menu.mmenu({
		slidingSubmenus: false
	});

	$menu.find( 'li span' ).not( '.mm-subopen' ).not( '.mm-subclose' ).bind(
		'click.example',
		function( e )
		{
			var $option = $.trim( $(this).text() );
			e.preventDefault();
			$('#menu').trigger( 'close' );
			set_page($option);
			$(this).trigger( 'setSelected.mm' );
		}
	);
});

$.fn.mmenu.debug = function( msg ) {
    alert( msg );
};

function show_refreshing(active)
{
	if (active) {
		document.getElementById( "refresh_button" ).style.backgroundImage = "url(img/refreshing.gif)";
	} else {
		document.getElementById( "refresh_button" ).style.backgroundImage = "url(img/refresh.png)";
	}
}

$(document).ready(function(){
	set_page('Sign In');
	msgTable = document.getElementById("msgTable");
	setInterval("refresh()", 1000);
	document.getElementById('showViewport').innerHTML = "Viewport: " + window.innerWidth + "x" + window.innerHeight;
	$('#menu_button').hide();
	$('#back_button').hide();
	$('#refresh_button').hide();
	window.onresize = resize;
	load_global_data();
});

function load_global_data()
{
	globalData = null;
	if (localStorage.getItem('firstthumb') !== null) {
		var value = localStorage.getItem('firstthumb');
		try {
		   globalData = jQuery.parseJSON(unescape(value));
		} catch(e) {
		}
	}
	if (globalData == null || globalData == undefined ||
			!globalData.hasOwnProperty('DataFormatVersion') ||
			globalData.DataFormatVersion != DataFormatVersion)
	{
		globalData = { DataFormatVersion:DataFormatVersion, TestSite:false };
	}
	if (globalData.hasOwnProperty('username'))
		document.getElementById('username').value = globalData.username;
	if (globalData.hasOwnProperty('password'))
		document.getElementById('password').value = globalData.password;
	if (globalData.TestSite) {
		$('#site_name').show().text('First Thumb (test site)');
		base_url = 'https://firsthumb.com';
		notification_id = '591175046284';
	} else {
		$('#site_name').show().text('First Thumb');
		base_url = 'http://firsthumb.com/main';
		notification_id = '591175046284';
   }
	wsurl = base_url + '/ws/query.php';
	save_global_data();
}

function save_global_data()
{
	if (!(window.cordova || save_signin_in_browser)) {
		globalData.username = '';
		globalData.password = '';
	}
	localStorage.setItem('firstthumb', escape(JSON.stringify(globalData, null, '\t')));
}

function splash_clicked()
{
	var now = Date.now();
	if (now - splash_time > 10000) {
		splash_time = now;
		splash_cnt = 0;
	} else if (++splash_cnt > 8) {
		splash_cnt = 0;
		globalData.TestSite = !globalData.TestSite;
		save_global_data();
		load_global_data();
	}
}

function resize()
{
	$('#debugTable').height(window.innerHeight - $('#debugTable').offset().top);
	$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
	$('#imSummaryTable').height(window.innerHeight - $('#imSummaryTable').offset().top);
	$('#imDetailTable').height(window.innerHeight - $('#imDetailTable').offset().top - 40);
}

function register_for_push_notifications()
{
	if (window.cordova)
		window.plugins.pushNotification.register(successHandler, errorHandler,{senderID:notification_id, ecb:"onNotificationGCM"});
	else
		do_post('addUserDevice', { 'deviceToken':'None', 'pushHandlerId':3 }, process_add_user_device);
}

function successHandler(result)
{
	debugInfo.innerHTML += "pushReg OK<br>";
}

function errorHandler(error) {
	pushRegistrationTimeout *= 2;
	setTimeout(register_for_push_notifications(), pushRegistrationTimeout);
}

function onNotificationGCM(e) {
        switch( e.event )
        {
            case 'registered':
                if ( e.regid.length > 0 )
                {
                    console.log("Regid " + e.regid);
	debugInfo.innerHTML += "RegId OK<br>";
	do_post('addUserDevice', { 'deviceToken':e.regid, 'pushHandlerId':2 }, process_add_user_device);
                    //alert('registration id = '+e.regid);
                }
                break;

            case 'message':
                // this is the actual push notification. its format depends on the data model from the push server
                get_ims(); //alert('message = '+e.message+' msgcnt = '+e.msgcnt);
                break;

            case 'error':
                alert('GCM error = '+e.msg);
                break;

            default:
                alert('An unknown GCM event has occurred');
                break;
        }
}

function process_add_user_device(response)
{
  if (response.error != null) {
    alert("addUserDevice: " + response.error.message);
  }
}

var entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;'
};

function escapeHtml(string) {
  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}

function set_page(page)
{
	var pages = {
		'Sign In'					: 'signin',
		'Passenger'					: 'passenger',
		'Driver'						: 'driver',
		'Recent Journeys'			: 'recentjourneys',
		'Message Centre'			: 'messagecentre',
		'Settings'					: 'settings',
		'Debug'						: 'debug',
		'GPS'							: 'gps',
		'Terms and Conditions'	: 'termsandconditions',
		'Sign Out'					: 'signout'
	};

	if (active == 'driver' && page == 'Passenger')
		page = 'Driver';
	if (active == 'passenger' && page == 'Driver')
		page = 'Passenger';

	$('#menu_button').show();
	$('#back_button').hide();
	$('#refresh_button').show();
	im_detail_contact = '';
	for (var txt in pages) {
		if (txt == page)
			document.getElementById(pages[txt]).style.display = 'block';
		else
			document.getElementById(pages[txt]).style.display = 'none';
	}
	$('#header2').show().text(page);
	if (page == 'Sign Out') {
		do_logout();
	} else if (page == 'Message Centre') {
		$('#messagecentredetail').hide();
		$('#messagecentresummary').show();
		render_im_list();
	} else if (page == 'Debug') {
		$('#debugTable').height(window.innerHeight - $('#debugTable').offset().top);
	} else if (page == 'Driver') {
		$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
	} else if (page == 'Terms and Conditions') {
		window.open(base_url + '/terms_and_conditions.php', '_blank', 'location=yes,clearcache=yes,clearsessioncache=yes');
		set_page(cur_page);
		return;
	}
	cur_page = page;
}

function register_button_pressed()
{
	window.open(base_url + '/register.php', '_blank', 'location=yes,clearcache=yes,clearsessioncache=yes');
}

function signin_button_pressed()
{
  // Try to log in
  IdleCounter = 0;
  globalData.username = document.getElementById("username").value;
  globalData.password = document.getElementById("password").value;

  do_post('login', { 'userId':globalData.username, 'pwd':globalData.password }, process_login_result);
	$('#refresh_button').show();
  show_refreshing(true);
}

function load_cookie()
{
	var save = false;

	if (localStorage.getItem(globalData.username) === null) {
		alert("No localstorage for " + globalData.username);
		cookieData = new Object;
		cookieData.DataFormatVersion = DataFormatVersion;
		save = true;
	} else {
		var value = localStorage.getItem(globalData.username);
		try {
		   cookieData = jQuery.parseJSON(unescape(value));
		} catch(e) {
		   console.log("Failed to parse '" + unescape(value) + "'");
			cookieData = new Object;
			cookieData.DataFormatVersion = DataFormatVersion;
			save = true;
		}
	}
	if (cookieData == null || cookieData == undefined) {
		alert("No data!");
		cookieData = new Object;
		cookieData.DataFormatVersion = DataFormatVersion;
		save = true;
	}
	if (!cookieData.hasOwnProperty('DataFormatVersion') || cookieData.DataFormatVersion != DataFormatVersion) {
		cookieData = new Object;
		cookieData.DataFormatVersion = DataFormatVersion;
		save = true;
		alert("Data and settings deleted due to format change");
	}
	if (cookieData.hasOwnProperty('RefreshEnable')) {
	   document.getElementById('RefreshEnable').checked = cookieData.RefreshEnable;
	} else {
		cookieData.RefreshEnable = false;
		save = true;
	}
	if (cookieData.hasOwnProperty('DebugEnable')) {
	   document.getElementById('debugEnable').checked = cookieData.DebugEnable;
	} else {
		cookieData.DebugEnable = false;
		save = true;
	}
	if (!cookieData.hasOwnProperty('Groups')) {
		cookieData.Groups = [];
		save = true;
	}
	if (!cookieData.hasOwnProperty('IMs')) {
		cookieData.IMs = {};
		save = true;
	}
	if (!cookieData.hasOwnProperty('IMExpiryDays')) {
		cookieData.IMExpiryDays = 5;
		save = true;
	}
	document.getElementById('im_expiry_days').value = cookieData.IMExpiryDays;
	if (save) {
		save_cookie();
	}
}

function save_cookie()
{
	var value = escape(JSON.stringify(cookieData, null, '\t'));
	console.log("Saving cookie: " + JSON.stringify(cookieData, null, ' '));
	localStorage.setItem(globalData.username, value);
}

function refresh_enable_clicked()
{
	cookieData.RefreshEnable = document.getElementById('RefreshEnable').checked;
	save_cookie();
}

function debug_enable_clicked()
{
	cookieData.DebugEnable = document.getElementById('debugEnable').checked;
	save_cookie();
}

function process_login_result(response)
{
	if (!(window.cordova || save_signin_in_browser)) {
		document.getElementById("password").value = "";
	}
	$('#refresh_button').hide();
	if (response.error !== null) {
	 	alert(response.error.message);
	} else {
		load_cookie();
		msgTable.innerHTML = "";
		sessionId = response.result.sessionId;
		signedIn = 1;
		// Make it look like a idle passenger until we know better
		active = '';
		document.getElementById("passengerActivePanel").style.display="none";
		$('#passenger_credit').show().text('???');
		$('#driver_credit').show().text('Credit: ??? miles');
		set_page("Passenger");
		get_status();
		get_messages();
		get_group_list();
		get_ims();
		register_for_push_notifications();
		window.plugins.powerManagement = new PowerManagement();
		save_global_data();
	}
}

function do_logout()
{
	set_page('Sign In');
	$('#menu_button').hide();
	$('#refresh_button').hide();
   do_post('logout', { }, process_logout);
	$('#passengerPassphrase').show().text('');
	msgTable.innerHTML = "";
	sessionId = "";
	active = '';
	LastMsgIdReceived = 0;
	signedIn = 0;
}

function process_logout(response)
{
}

function refresh()
{
  if (signedIn == 1) {
    if (IdleTimeoutInSeconds > 0 && ++IdleCounter > IdleTimeoutInSeconds) {
      do_logout();
    } else if (++RefreshCounter > RefreshPeriodInSeconds && RefreshEnable.checked == true) {
      RefreshCounter = 0;
      get_status();
      get_messages();
		get_group_list();
		get_ims();
    }
  }
}

function refresh_button_pressed()
{
	IdleCounter = 0;
	if (signedIn == 1) {
		get_status();
		get_messages();
		get_group_list();
		get_ims();
		show_refreshing(true);
	}
}

function get_status()
{
  do_post('status', { }, process_status);
}

function process_status(response)
{
  if (response.error != null) {
    alert("GetStatus: " + response.error.message);
		do_logout();
  } else if (signedIn == 0) {
    ;
  } else {
	 $('#passenger_credit').show().text(response.result.credit);
	 $('#driver_credit').show().text('Credit: ' + response.result.credit + ' miles');
    if (response.result.passengers == undefined && response.result.driver == undefined) {
      active = '';
    }
    if (response.result.passengers == undefined) {
      document.getElementById("driverActivePanel").style.display="none";
		$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
    } else {
		if (cur_page == 'Passenger') {
      	set_page('Driver');
		}
      active = 'driver';
      document.getElementById("driverActivePanel").style.display="block";
      if (response.result.passengers.length == 1) {
        document.getElementById("driverPassengers").innerHTML = "Your passenger is ";
      } else {
        document.getElementById("driverPassengers").innerHTML = "Your passengers are ";
      }
      document.getElementById("driverPassengers").innerHTML += response.result.passengers.join(", ");
		$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
    }
    if (response.result.driver == undefined) {
      document.getElementById("passengerActivePanel").style.display="none";
      document.getElementById("passengerIdlePanel").style.display="block";
    } else {
		if (cur_page == 'Driver') {
      	set_page('Passenger');
		}
      active = 'passenger';
      document.getElementById("passengerActivePanel").style.display="block";
      document.getElementById("passengerIdlePanel").style.display="none";
      document.getElementById("passengerDriver").innerHTML = "Your driver is " + response.result.driver;
    }
  }
}

function get_group_list()
{
   do_post('getGroupList', { }, process_group_list);
}

function process_group_list(response)
{
	var MsgId = 0;

	if (response.error !== null) {
		if (!globalData.TestSite && response.error.code == -32601) {
			// "Method not found" - live site not yet supporting messaging
		} else {
			alert("GetGroupList: " + response.error.message);
			do_logout();
		}
	} else if (signedIn == 0) {
		;
	} else {
		cookieData.Groups.sort();
		response.result.groups.sort();
		if (JSON.stringify(cookieData.Groups) !== JSON.stringify(response.result.groups)) {
			cookieData.Groups = response.result.groups;
			save_cookie();
		}
	}
}

function get_ims()
{
   do_post('getIMs', { }, process_ims);
}

function process_ims(response)
{
	if (response.error !== null) {
		if (!globalData.TestSite && response.error.code == -32601) {
			// "Method not found" - live site not yet supporting messaging
		} else {
			alert("GetIMs: " + response.error.message);
			do_logout();
		}
	} else if (signedIn == 0) {
		;
	} else {
		// Add any IMs to our cookieData, then ack them all
		var lastId = 0;

		for (var id in response.result.msgs) {
			var msg = response.result.msgs[id];
			if (id > lastId)
				lastId = id;
			if (!cookieData.hasOwnProperty('IMs'))
				cookieData['IMs'] = new Array;
			if (!cookieData.IMs[msg.from])
				cookieData.IMs[msg.from] = { 'New':1, 'Msgs':[] };
			cookieData.IMs[msg.from].New = 1;
			cookieData.IMs[msg.from].Msgs.push({ 'Incoming':1, 'Time':msg.time, 'Text':escapeHtml(msg.text) });
		}
		if (lastId > 0) {
			do_post('ackIMs', { 'msgId':lastId }, process_ack_ims);
			save_cookie();
		}
		// Delete any old messages and threads that become empty as a result
		var d = new Date();
		var cutoff = d.getTime()/1000 - cookieData.IMExpiryDays * 24 * 60 * 60;
		var filtered_ims = {};
		var save = false;
		for (var contact in cookieData.IMs) {
			var filtered_msgs = [];
			for (var index in cookieData.IMs[contact].Msgs) {
				if (cookieData.IMs[contact].Msgs[index].Time > cutoff) {
					filtered_msgs.push(cookieData.IMs[contact].Msgs[index]);
				} else {
					save = true; // We just deleted one
				}
			}
			if (filtered_msgs.length > 0) {
				filtered_ims[contact] = { 'New':cookieData.IMs[contact].New, 'Msgs':filtered_msgs };
			}
		}
		if (save) {
			cookieData.IMs = filtered_ims;
			save_cookie();
		}
		render_im_list();
		if (im_detail_contact != '') {
			im_render_detail(im_detail_group, im_detail_contact);
		}
	}
}

function process_ack_ims(response)
{
	if (response.error !== null) {
		alert("AckIMs: " + response.error.message);
		do_logout();
	}
}

function render_im_list()
{
	for (var contact in cookieData.IMs) {
		cookieData.IMs[contact].Msgs.sort(function(a, b) { return(b.Time - a.Time); });
	}

	var contacts = [];
	for (var key in cookieData.IMs)
		contacts.push(key);
	contacts.sort(function(a, b) { return cookieData.IMs[b].Msgs[0].Time - cookieData.IMs[a].Msgs[0].Time; });
	cookieData.Groups.sort();
	var new_content = '<div><b>Groups</b></div>';
	for (var index in cookieData.Groups) {
		new_content += '<div class="oldIMThread" onclick="im_render_detail(true, \'' + cookieData.Groups[index] + '\')">' + cookieData.Groups[index] + '</div>';
	}
	new_content += '<div><b>Messages</b></div>';
	for (var index in contacts) {
		contact = contacts[index];
		var d = new Date(cookieData.IMs[contact].Msgs[0].Time * 1000);
		var dmy = d.getDate()+' '+monthNames[d.getMonth()]+' '+d.getFullYear();
		var m = d.getMinutes();
		if (m < 10)
			m = "0" + m;
		var hm = d.getHours()+':'+m;
		if (cookieData.IMs[contact].New == 1)
			state = 'newIMThread';
		else
			state = 'oldIMThread';
		new_content +=
			'<div class="' + state + '" onclick="im_render_detail(false, \'' + contact + '\')">' +
				'<span style="display:inline; float: right;">' + dmy + '</span>' +
				'<span> <span id="im_list_contact">' + contact + '</span></span>' +
				'<span style="display:inline; float: right;">' + hm + '</span>' +
				'<span> <span id="im_list_text">' + cookieData.IMs[contact].Msgs[0].Text + '</span></span>' +
			'</div>';
	}
	$('#imSummaryTable').height(window.innerHeight - $('#imSummaryTable').offset().top);
	if (imSummaryTable.innerHTML != new_content) {
		imSummaryTable.innerHTML = new_content;
		$('#imSummaryTable').animate({ scrollTop: 0 }, "slow");
	}
}

function im_render_detail(is_group, name)
{
	$('#menu_button').hide();
	$('#back_button').show();
	if (im_detail_contact == '') {
		// First display of page, scroll to top
		$('#imDetailTable').scrollTop(0);
	}
	im_detail_contact = name;
	im_detail_group = is_group;
	$('#messagecentresummary').hide();
	$('#messagecentredetail').show();
	$('#header2').show().text(name);
	var new_content = '';
	if (is_group) {
		;
	} else if (cookieData.IMs.hasOwnProperty(name)) {
		var msgs = cookieData.IMs[name].Msgs;

		if (cookieData.IMs[name].New == 1) {
			cookieData.IMs[name].New = 0;
			save_cookie();
		}
		msgs.sort(function(a, b) { return a.Time - b.Time; });
		for (var index in msgs) {
			var d = new Date(msgs[index].Time * 1000);
			var m = d.getMinutes();
			if (m < 9)
				m = "0" + m;
			var dmyhm = d.getDate()+' '+monthNames[d.getMonth()]+' '+d.getFullYear()+' '+d.getHours()+':'+m;
			new_content += '<center id="im_detail_date">'+dmyhm+'</center>';
			if (msgs[index].Incoming == 1)
				new_content += '<table width="100%"><tr><td class="incoming_im" width="80%">'+msgs[index].Text+'</td><td width="20%">&nbsp;</td></tr></table>';
			else
				new_content += '<table width="100%"><tr><td width="20%">&nbsp;</td><td class="outgoing_im" width="80%">'+msgs[index].Text+'</td></tr></table>';
		}
	}
	$('#imDetailTable').height(window.innerHeight - $('#imDetailTable').offset().top - 40);
	if (imDetailTable.innerHTML != new_content) {
		imDetailTable.innerHTML = new_content;
		$('#imDetailTable').stop().animate({
		scrollTop: $("#imDetailTable")[0].scrollHeight
		}, 800);
	}
}

function im_send_clicked()
{
	var txt = document.getElementById('imSendText').value;
	var params;
	var d = new Date();

	document.getElementById('imSendText').value = '';
	if (txt != '') {
		if (im_detail_group) {
			params = { 'emailList':[], 'groupList':[ im_detail_contact ], 'message':txt };
		} else {
			params = { 'emailList':[ im_detail_contact ], 'groupList':[], 'message':txt };
			if (!cookieData.IMs.hasOwnProperty(im_detail_contact)) {
				cookieData.IMs[im_detail_contact] = { 'New':0, 'Msgs':[] };
			}
			cookieData.IMs[im_detail_contact].Msgs.push({ 'Incoming':0, 'Time':d.getTime()/1000, 'Text':escapeHtml(txt) });
			save_cookie();
			im_render_detail(im_detail_group, im_detail_contact);
		}
		do_post('sendIM', params, process_send_im);
		show_refreshing(true);
	}
}

function process_send_im(response)
{
	if (response.error !== null) {
		alert("sendIM: " + response.error.message);
		//do_logout();
	}
}

function signin_keypress(e)
{
	var keyCode = e.keyCode || e.which;

	if (keyCode == 13)
		signin_button_pressed();
}

function do_post(type, params, handler)
{
	console.log('do_post('+type+')');
   if (WsRequests.Pending.hasOwnProperty(type) && WsRequests.Pending[type] > 0) {
     console.log('Ignoring ' + type + ' request as one is pending');
     return;
   }
	WsRequests.Pending[type] = 1;
	WsRequests.Queue.unshift({ 'type':type, 'params':params, 'handler':handler });
	if (!WsRequests.Active) {
		issue_next_ws_request();
	}
}

function issue_next_ws_request()
{
	var msg = new Object;

	if (WsRequests.Queue.length == 0) {
		WsRequests.Active = false;
		show_refreshing(false);
		return;
	}

	WsRequests.Active = true;
	req = WsRequests.Queue.pop();
	msg.jsonrpc = '2.0';
	msg.Id = ++WsMsgId;
	msg.method = req.type;
	if (req.type != 'login')
	   req.params.sessionId = sessionId;
	msg.params = req.params;
	debug(JSON.stringify(msg, null, '\t'));
	$.post(wsurl, 'q=' + JSON.stringify(msg, null, '\t'), function(response, textStatus, xhr) {
		//alert("Success: " + textStatus + "\nStatus: " + xhr.status + "\nstatusText: " + xhr.statusText);
		do_posted(req.type, req.params, req.handler, response)})
		.fail(function(xhr, textStatus, errorThrown) {
			//alert("Failed: \"" + req.type + "\"\nStatus: " + xhr.status + "\nStatusText: " + xhr.statusText);
			errTxt = xhr.statusText + "(" + xhr.status + ")";
			console.log(req.type + ' failed: ' + errTxt);
			WsRequests.Pending[req.type] = 0;
			req.handler({error:{code:32000-99, message:errTxt}});
			issue_next_ws_request();
		});
}

function do_posted(type, params, handler, response)
{
	console.log("do_posted: " + type);
	if (WsMsgId != response.id)
		console.log('WARNING: Got ' + type + ' response with id ' + response.id + ', expecting ' + WsMsgId);
	debug(JSON.stringify(response, null, '\t'));
	if (type != 'login' && response.error != null && response.error.code == -32014) {
		console.log(type + ' request failed, session expired, requeuing');
		WsRequests.Queue.push({ 'type':type, 'params':params, 'handler':handler});
		WsRequests.Queue.push({ 'type':'login', 'params':{ 'userId':globalData.username, 'pwd':globalData.password }, 'handler':relogin_handler});
		WsRequests.Pending['login'] = 1;
	} else {
		console.log(type + ' completed');
		WsRequests.Pending[type] = 0;
		handler(response);
	}
	issue_next_ws_request();
}

function relogin_handler(response)
{
	if (response.error != null) {
		alert("login: " + response.error.message);
		console.log('Relogin attempt failed');
		while (WsRequests.Queue.length != 0) {
			req = WsRequests.Queue.pop();
			console.log('Failing ' + req.type + ' request');
			Ws.Pending[req.type] = 0;
			req.handler({ error:{code:32000-14}});
		}
		do_logout();
	} else {
		sessionId = response.result.sessionId;
		console.log('Logged in ok');
	}
}

function send_im(emails, groups, text)
{
	var params = { 'emailList':emails, 'groupList':groups, 'message':text };

	do_post('sendIM', params, process_send_im2);
}

function process_send_im2(response)
{
	if (response.error !== null) {
		alert("sendIM: " + response.error.message);
		do_logout();
	} else {
	}
}

function get_messages()
{
  do_post('getMessages', { }, process_messages);
}

function process_messages(response)
{
  var MsgId = 0;

  if (response.error !== null) {
    alert("GetMessages: " + response.error.message);
		do_logout();
  } else if (signedIn == 0) {
    ;
  } else {
    msgTable.innerHTML = "";
    if (response.result.msgs) {
      $.each(response.result.msgs, function(id, msgdata) { MsgId = id; process_one_message(id, msgdata); });
    }
  }
  if (MsgId != LastMsgIdReceived) {
    $('#msgTable').stop().animate({
      scrollTop: $("#msgTable")[0].scrollHeight
    }, 800);
    LastMsgIdReceived = MsgId;
  }
}

function process_one_message(id, msgdata)
{
  txt = 'huh?';
  if ("errorText" in msgdata)
     txt = msgdata.errorText;
  else if (msgdata.type == 'startRide') {
    if ("driver" in msgdata) {
      txt = 'Your driver is ' + msgdata.driver + ' and the passphrase is \'' + msgdata.magic + '\'';
    } else {
      txt = msgdata.passenger + ' is requesting a ride, and the passphase is \'' + msgdata.magic + '\'.';
    }
  } else if (msgdata.type == 'endRide') {
    if ("passenger" in msgdata) {
      txt = 'Your passenger ' + msgdata.passenger + ' has completed their journey, you have been credited ';
    } else {
      txt = 'End of journey, you have been charged';
    }
    txt += ' ' + msgdata.miles + ' miles.';
  }
  else {
    txt = 'Funny msg type ' + msgdata.type;
  }
  msgTable.innerHTML += "<hr width=\"94%\"><p onclick=\"delete_messages(" + id + ")\">" + txt;
}

function delete_messages(id)
{
  IdleCounter = 0;
  if (confirm("Delete messages?") == true) {
    do_post('ackMessages', { 'msgId':id }, process_delete_messages);
  }
}

function process_delete_messages(response)
{
  if (response.error != null) {
    alert(esponse.error.message);
  } else {
    get_messages();
  }
}

function endride_button_press()
{
	IdleCounter = 0;
	var miles = document.getElementById("miles").value;
	if (miles == "") {
		alert("You must enter the miles travelled");
	} else {
		navigator.geolocation.getCurrentPosition(
			function(position) { endride_post_location(true, position, miles); },
			function(error) { endride_post_location(false, error, miles); },
			{ maximumAge: 10000, timeout: 10000, enableHighAccuracy: true });
		show_refreshing(true);
	}
}

function endride_post_location(success, position, miles)
{
	var loc;

	// if it failed or gave us a cached value more than 5 minutes old, report 0,0
	if (!success) {
		var error = position;
		debug('GPS request failed, code: ' + error.code    + ' / ' + 'message: ' + error.message);
		loc = { lat:0, lng:0, accuracy:0 };
	} else if ((Date.now() - position.timestamp) > (5*60*1000)) {
		debug("GPS result was too old");
		loc = { lat:0, lng:0, accuracy:0 };
	} else {
		loc = { lat:position.coords.latitude, lng:position.coords.longitude, accuracy:position.coords.accuracy};
		debug('endRide: ' + JSON.stringify(loc));
	}
	do_post('endRide', { miles:miles, location:loc }, process_endride);
	show_refreshing(true);
}

function process_endride(response)
{
  if (response.error != null) {
    alert(esponse.error.message);
  } else {
    document.getElementById("miles").value = "";
    $('#passengerPassphrase').show().text('');
    get_status();
  }
}

function startride_button_press()
{
	IdleCounter = 0;
	var registration = document.getElementById("registration").value;
	if (registration == "") {
		alert("You must enter the car registration");
	} else {
		navigator.geolocation.getCurrentPosition(
			function(position) { startride_post_location(true, position, registration); },
			function(error) { startride_post_location(false, error, registration); },
			{ maximumAge: 10000, timeout: 10000, enableHighAccuracy: true });
		show_refreshing(true);
	}
}

function startride_post_location(success, position, registration)
{
	var loc;

	// if it failed or gave us a cached value more than 5 minutes old, report 0,0
	if (!success) {
		var error = position;
		debug('GPS request failed, code: ' + error.code    + ' / ' + 'message: ' + error.message);
		loc = { lat:0, lng:0, accuracy:0 };
	} else if ((Date.now() - position.timestamp) > (5*60*1000)) {
		debug("GPS result was too old");
		loc = { lat:0, lng:0, accuracy:0 };
	} else {
		loc = { lat:position.coords.latitude, lng:position.coords.longitude, accuracy:position.coords.accuracy};
		debug('startRide: ' + JSON.stringify(loc));
	}
	do_post('startRide', { regno:registration, location:loc }, process_startride);
	show_refreshing(true);
}

function process_startride(response)
{
  if (response.error != null) {
    alert(response.error.message);
  } else {
    document.getElementById("registration").value = "";
    get_status();
    $('#passengerPassphrase').show().text('The passphrase is "' + response.result.magic + '"');
  }
}

function get_recent_journeys()
{
	do_post('getRecentJourneys', { 'maxNoOfJourneys':30 }, process_recent_journeys);
}

function process_recent_journeys(response)
{
	if (response.error !== null) {
		alert("getRecentJourneys: " + response.error.message);
		do_logout();
	} else if (signedIn == 0) {
		;
	} else {
		response.result.journeys.sort(function(a,b) { return a.starttime - b.starttime; });
		content = '';
		for (var index in response.result.journeys) {
			journey = response.result.journeys[index];
			content += '<table id="journey-table" width="100%"><tr><td width="30%" align="right">Driver</td><td width="40%">'
							+ journey.driver_first_name + ' ' + journey.driver_last_name + '</td><td width="30%">&nbsp;</td></tr>'
							+ '<tr><td align="right">Passenger</td><td>' + journey.passenger_first_name + ' ' + journey.passenger_last_name + '</td><td></td></tr>'
							+ '<tr><td>Start</td><td>End</td><td align="right">Miles</td></tr>'
							+ '<tr><td></td><td></td><td align="right">' + journey.miles + '</td>'
							+ '</table><hr>';
		}
		$('#JourneyTable').height(window.innerHeight - $('#JourneyTable').offset().top);
		JourneyTable.innerHTML = content;
	}
}

function cleardebug_button_pressed()
{
  IdleCounter = 0;
  debugTable.innerHTML = "";
}

function debug(str)
{
  if (debugEnable.checked == true) {
    debugTable.innerHTML += str + "<hr>";
  }
}

function debug_always(str)
{
	debugTable.innerHTML += str + "<hr>";
}

function im_expiry_days_changed()
{
	cookieData.IMExpiryDays = document.getElementById('im_expiry_days').value;
	save_cookie();
}

function clear_user_data_clicked()
{
	if (confirm("Clear user data?")) {
		cookieData = new Object;
		cookieData.DataFormatVersion = DataFormatVersion;
		save_cookie();
		do_logout();
	}
}

var gpsgo = null;

function gps_wakelock()
{
	window.plugins.powerManagement.dim(dimok, dimfail);
}

function dimok()
{
	document.getElementById('gps_lockstat').innerHTML = 'Locked';
}

function dimfail()
{
	document.getElementById('gps_lockstat').innerHTML = 'LockError';
}

function gps_release()
{
	window.plugins.powerManagement.release(relok, relfail);
}

function relok()
{
	document.getElementById('gps_lockstat').innerHTML = 'Unlocked';
}

function relfail()
{
	document.getElementById('gps_lockstat').innerHTML = 'UnlockError';
}

function gps_test_clicked()
{
	var element = document.getElementById('geolocation');
	if (gpsgo != null) {
		clearInterval(gpsgo);
		document.getElementById('gps_runstat').innerHTML = 'Stopped';
		gpsgo = null;
	} else {
		element.innerHTML = '';
		document.getElementById('gps_runstat').innerHTML = 'Running';
		gpsget();
		gpsgo = setInterval(function(){gpsget()}, 60000);
	}
}

function gpsget()
{
	navigator.geolocation.getCurrentPosition(onSuccessGet, onErrorGet, { maximumAge: 10000, timeout: 10000, enableHighAccuracy: true });
}

// onSuccess Callback
// This method accepts a Position object, which contains the
// current GPS coordinates
//
var onSuccessGet1 = function(position) {
    alert('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + position.timestamp                + '\n');
};

// onError Callback receives a PositionError object
//
function onErrorGet(error) {
    debug_always('GPS request failed, code: '    + error.code    + ' / ' +
          'message: ' + error.message);
}

function fmt_time(ms)
{
	var d;
	if (ms == 0)
		d = new Date();
	else
		d = new Date(ms);

	var m = d.getMinutes();
	if (m <= 9)
		m = "0" + m;
	return d.getDate()+' '+monthNames[d.getMonth()]+' '+d.getFullYear()+' '+d.getHours()+':'+m;
}

// onSuccess Callback
//   This method accepts a `Position` object, which contains
//   the current GPS coordinates
//
function onSuccessGet(position) {
	var element = document.getElementById('geolocation');
	element.innerHTML += fmt_time(0) + ' / ' + fmt_time(position.timestamp) + ' ' + position.coords.latitude + ',' + position.coords.longitude + ' (' + position.coords.accuracy + ')<br>';
	do_post('updateLocation', { 'location':{ 'lat':position.coords.latitude, 'lng':position.coords.longitude, 'accuracy':position.coords.accuracy } }, process_update_location);
}

// onError Callback receives a PositionError object
//
function onErrorWatch(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

function process_update_location(response)
{
	if (response.error !== null) {
		debug_always("updateLocation: " + response.error.message);
	} else if (signedIn == 0) {
		;
	} else {
	}
}

