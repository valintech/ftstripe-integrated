<?php

require_once 'base.inc';
require_once 'classes/session.inc';
require_once 'classes/region.inc';
require_once 'classes/schedule.inc';
require_once 'classes/google.inc';
require_once 'classes/yahoo.inc';
require_once('classes/user.inc');
require_once 'classes/php2js.inc';
if (CConfig::RUN_IN_FB)
    require_once 'classes/facebook.inc';

//
// Potential request values:-
// "op" - operation code for specific script functions
// "source" - source address for query
// "destination" - destination address for query
// "journeyId" - journey id. to load into "request ride" popup
//
// Get current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();
$userId = ($isValidSession ? (int) $login->userId() : 0);
// Get posted data
$op = CCommon::getRequestValue('op');

// XmlHttpRequest:- Find journeys
$groups=0;
if ($op == 'search') {
    $grouped_id = CCommon::getRequestValue('group_list');
    $existing_groups = CUser::getExistingGroupsOfUserListingSearch($grouped_id);
    $groups = json_encode($existing_groups);
}
if ($op == '') {
    $grouped_id = CCommon::getRequestValue('grouped_id');
  //  $grouped_id = CCommon::getRequestValue('group_list');
    $existing_groups = CUser::getExistingGroupsOfUserListingSearch($grouped_id);
    $groups = json_encode($existing_groups);
}
if ($op == 'search') {
    $source = CCommon::fromJson(CCommon::getRequestValue('source'));
    $destination = CCommon::fromJson(CCommon::getRequestValue('destination'));
    $grouped_id = CCommon::getRequestValue('group_list');
    $journeyList = CSchedule::distance($source, $destination, $grouped_id,$userId);
    $journeyList = CSchedule::removeExpired($journeyList);
    $out = search($journeyList);
    CCommon::xhrSend(CCommon::toJson($out));
    // $locations = array();
    //  $rplc = array();
   
 //  script($locations, $login, $isValidSession,$groups);
    exit;
}
$rplc = array();
 $rplc[4542] = "<script type='text/javascript'>var _groups='".$groups."'</script>";//echo "====".$rplc[4542]."====";
//$rplc[4542] = sprintf("<script type='text/javascript'>var _groups='%s';</script>", $groups);
// Initialise if no opcode given
if ($op == '') {
    // Pickup any source/destinatjon given
    $source = CCommon::getRequestValue('source');
    $destination = CCommon::getRequestValue('destination');
  
    $source = new CLocation($source);
    $destination = new CLocation($destination);
    $locations = array();
    $locations[] = $source;
    $locations[] = $destination;

    // Output HTML page
    $region = new CRegion("search_results");
    
    if ($isValidSession)
        $rplc[1] = sprintf('|%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
    else
        $rplc[1] = sprintf('|&nbsp;<a href="javascript:signIn()">%s</a>', $region->msg(3, "common"));
    $rplc[2] = script($locations, $login, $isValidSession);
    //$rplc[4542] = script($locations, $login, $isValidSession,$groups);
    $rplc[3] = $source->label;
    $rplc[333] = $grouped_id;
    $rplc[4] = scriptLinks();
    $rplc[5] = $destination->label;
    $rplc[8] = $region->msg(8, 'common');
    $rplc[9] = $region->msg(9, 'common');
    $rplc[11] = $region->msg(1100);
    $rplc[1111] = $region->msg(1111);
    $rplc[1155] = $region->msg(1155);
    $rplc[30] = $region->msg(10, 'common');
    $rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
    $rplc[32] = $region->msg(13, 'common');
    $rplc[33] = $region->msg(14, 'common');
    $rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
    if ($isValidSession)
        $menu_header = file_get_contents('header_menus_login.php');
    else
        $menu_header = file_get_contents('header_menus.php');

    $rplc[777] = $menu_header;
    $rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
    $out = CCommon::htmlReplace("search_results.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
    print($out);
    if (CConfig::RUN_IN_FB == 0)
        @include 'google_analytics.html';
}


/*
 * Print <script> element to output
 * 
 * @param $locations        Array of CLocation objects [0]=source, [1]=destination
 * @param $login            A CLogin object
 * @param $isValidSession   true if have current session otherwise false
 * @return                  HTML <script> stream
 */

function script($locations, $login, $isValidSession) {
  //$var_info = print_r($groups,true);
    $php2Js = new Php2Js();
    $out = array();
  //  $out[] = sprintf("var _groups='%s';", $groups);
    // $out[] = '<script type="text/javascript">';
     // $out[] ='var _groups='.$groups;
    //   $out[] = '</script>';
     //$out[] ="var _groups='".$var_info;
    $out[] = '<script type="text/javascript">';
    $out = array_merge($out, CRoot::formatClassAsJs('CSchedule'));
    $out = array_merge($out, CRoot::formatClassAsJs('CConfig', array(CConfig::CONTENT_DIR)));
    $php2Js->add('_locations', $locations);
    require_once('classes/region.inc');
    $region = new CRegion('search_results');
    $php2Js->add('_msgList', $region->msgList());
    $user = $login->getUser(true);
    $php2Js->add('_isUserRegistered', ($user && $user->isRegistered() ? true : false));
   // $php2Js->add(' _groups',$var_info);
    $php2Js->add('_journeyRequestId', CCommon::getRequestValue('journeyId'));
    $php2Js->add('_isValidSession', ($isValidSession ? true : false));
    $out[] = $php2Js->generateJs();
    $out[] = '</script>';
    return join('', $out);
}

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks() {
    $out = array();
    $out[] = CGoogle::scriptHtml();
    $out[] = CYahoo::scriptHtml(array('json', 'connection', 'container', 'menu', 'button'));
    $out[] = '<script type="text/javascript" src="js/common.js"></script>';
    $out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
   
    $out[] = '<script type="text/javascript" src="requestride.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.tokeninput.js"></script>'; 
    $out[] = '<script type="text/javascript" src="search_results.js"></script>';
    //$out[] = '<script type="text/javascript" src="js/jquery_support_search.js"></script>';
    return join("\n", $out);
}

/*
 * Search for locations
 * 
 * @param $journeyList  List of CSchedule objects
 */

function search($journeyList) {
    $region = new CRegion('search_results');
    $html = array();
    $html[] = '<table class="resultsTable" cellpadding="0" cellspacing="0">';
    if (count($journeyList)) {
        $journeyInfo = CSchedule::scheduleInfo($journeyList);
        $html[] = sprintf('<h3>%s</h3><br>', $region->msg(1010));
        $index = 0;
        foreach ($journeyList as $journey) {
            if ($index)
                $html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>', CConfig::CONTENT_DIR);
            //   $html[] =$journey->;
            $html[] = journeyHtml($index++, $journey, $region, $journeyInfo[$journey->get(CSchedule::ID)]);
        }
        // Put in extras separator to split ride list and link to create new ride
        if ($index)
            $html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>', CConfig::CONTENT_DIR);
    } else
        $html[] = sprintf('<h3>%s</h3><br>', $region->msg(1011));
    $html[] = sprintf('<tr><td><a href="javascript:_arrangeTravel()">%s</a></td></tr>', $region->msg(1012));
    $html[] = '</table>';
    $out = array();
    $out['html'] = join('', $html);
    $out['journeys'] = $journeyList;
    return $out;
}

/*
 * Create HTML representation of the journey
 * 
 * @param $index        Journey index
 * @param $journey      A CSchedule object
 * @param $region       A CRegion object
 * @param $journeyInfo  stdClass object containing extra journey
 *                      information "placesTaken", "placesAvailable",
 *                      "smoker", "femaleOnly"
 *                      members
 * @return              HTML
 */

function journeyHtml($index, $journey, $region, $journeyInfo) {
   
    // Female only, smoker information
    $tmp = array();
    if ($journeyInfo->smoker)
        $tmp[] = $region->msg(1013);
    if ($journeyInfo->femaleOnly)
        $tmp[] = $region->msg(1014);
    $hostPrefsHtml = (count($tmp) ? join(', ', $tmp) : '');

    $tmp2 = array();
    $tmp2[] = '<p></p>' . $journeyInfo->firstName . ' ';
    if ($journey->get(CSchedule::REQUEST_TYPE) == CSchedule::REQUEST_TYPE_SEEKING) {
        $tmp2[] = 'is seeking';
    } else if ($journey->get(CSchedule::REQUEST_TYPE) == CSchedule::REQUEST_TYPE_OFFERING) {
        $tmp2[] = 'is offering';
    } else {
        $tmp2[] = 'is offering to share';
    }
    $tmp2[] = ' a lift';
    $requestorHtml = join('', $tmp2);

    $html = array();
    $html[] = '<tr><td>';

    if ($journey->get(CSchedule::JOURNEY_TYPE) == CSchedule::JOURNEY_TYPE_ONEOFF) {
        $html[] = '<strong>';
        $html[] = $region->msg(1015);
        $html[] = '</strong><p></p>';
        $html[] = $requestorHtml;
        $html[] = '<p></p><strong>Group :' . $journey->get('group_tags') . '</strong>';
        $html[] = '<span class="resultsDimmed">';
        $html[] = sprintf('<p></p>On %s @ %s', strftime('%x', CCommon::tsToPhp($journey->get(CSchedule::ONEOFF_DATE))), strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CSchedule::ONEOFF_TIME))));
        if ($hostPrefsHtml != '')
            $html[] = '<p></p>' . $hostPrefsHtml;
        $html[] = '</span><p></p>';
    }
    else {
        $html[] = '<strong>';
        $html[] = $region->msg(1016);
        $html[] = '</strong><p></p>';
        $html[] = $requestorHtml;
        $html[] = '<p></p><strong>Group :' . $journey->get('group_tags') . '</strong>';
        $html[] = '<p>' . $journey->group_tags . '</p>';
        $html[] = '<span class="resultsDimmed">';
        if ($journey->get(CSchedule::SCHEDULE_TYPE) == CSchedule::SCHEDULE_TYPE_STANDARD)
            $html[] = sprintf('<p></p>%s %s %s - Start @ %s / Return @ %s', $region->msg(17, 'common'), $region->msg(24, 'common'), $region->msg(21, 'common'), strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CSchedule::STANDARD_START_TIME))), strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CSchedule::STANDARD_RETURN_TIME))));
        elseif ($journey->get(CSchedule::SCHEDULE_TYPE) == CSchedule::SCHEDULE_TYPE_CUSTOM)
            $html[] = customTimesHtml($journey, $region);
        if ($hostPrefsHtml != '')
            $html[] = '<p></p>' . $hostPrefsHtml;
        $html[] = '</span><p></p>';
    }
    $html[] = sprintf('%s<p></p><img align="absmiddle" src="%s/images/arrow.gif" width="12" height="12" align="absmiddle" />&nbsp;%s<p></p>', $journey->get(CSchedule::START_ADDRESS), CConfig::CONTENT_DIR, $journey->get(CSchedule::DESTINATION_ADDRESS));
    $html[] = '</td></tr>';

    $html[] = '<tr><td align="right">';
    $tmp = array();
    $tmp[] = sprintf('<a href="javascript:_journeyClick(%d)">%s</a>', $index, $region->msg(1017));
    $tmp[] = sprintf('<a href="javascript:_showRoute(%d)">%s</a>', $index, $region->msg(1018));
    $html[] = join('<span class="resultsOptions">&nbsp;|&nbsp;</span>', $tmp);
    $html[] = '</td></tr>';

    return join('', $html);
}

/*
 * Create HTML for custom journey times
 * 
 * @param $journey    A CSchedule object
 * @param $region     A CRegion object
 * @return            HTML
 */

function customTimesHtml($journey, $region) {
    $dayMsgs = array(17, 18, 19, 20, 21, 22, 23);
    $times = array(
        array(CSchedule::MON_START_TIME, CSchedule::MON_RETURN_TIME),
        array(CSchedule::TUE_START_TIME, CSchedule::TUE_RETURN_TIME),
        array(CSchedule::WED_START_TIME, CSchedule::WED_RETURN_TIME),
        array(CSchedule::THU_START_TIME, CSchedule::THU_RETURN_TIME),
        array(CSchedule::FRI_START_TIME, CSchedule::FRI_RETURN_TIME),
        array(CSchedule::SAT_START_TIME, CSchedule::SAT_RETURN_TIME),
        array(CSchedule::SUN_START_TIME, CSchedule::SUN_RETURN_TIME));
    $out = array();
    foreach ($times as $k => $time) {
        $start = $journey->get($time[0]);
        $end = $journey->get($time[1]);
        if ($start == '' || $end == '')
            continue;
        $out[] = sprintf('%s - Start @ %s / Return @ %s', $region->msg($dayMsgs[$k], 'common'), strftime($region->msg(25, 'common'), CCommon::tmToPhp($start)), strftime($region->msg(25, 'common'), CCommon::tmToPhp($end)));
    }
    $out = join('<p></p>', $out);
    return $out != '' ? '<p></p>' . $out : $out;
}

?>
