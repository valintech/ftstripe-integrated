<?php
require_once('../base.inc');
require_once('class.model.php');

CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
if (CConfig::RUN_IN_FB)
	$isValidSession = $login->requireSession();
else
	$isValidSession = $login->isValidSession();
CWebSession::set('login', serialize($login));

$login_user = ($isValidSession ? (int) $login->userId() : 0);

if($login_user > 0 ){
	header('Location: ../register.php');	
}else{

	$model = new model();
	$user_id= CWebSession::get('regid');


	if($user_id || isset($_GET['code'])){

		define('CLIENT_ID', 'ca_AGangtIe1qEIkFTMTXCAD8iOXH3ye3kc');
		define('API_KEY', 'sk_test_L3BKVL8dqC6DcPSLUzW1UW3l');

		define('TOKEN_URI', 'https://connect.stripe.com/oauth/token');
		define('AUTHORIZE_URI', 'https://connect.stripe.com/oauth/authorize');

	if (isset($_GET['code'])) { // Redirect w/ code

		$code = $_GET['code'];

		$token_request_body = array(
			'client_secret' => API_KEY,
			'grant_type' => 'authorization_code',
			'client_id' => CLIENT_ID,
			'code' => $code
			);

		$req = curl_init(TOKEN_URI);
		curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($req, CURLOPT_POST, true );
		curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

		// TODO: Additional error handling
		$respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
		$resp = json_decode(curl_exec($req), true);
		curl_close($req);

		$data = Array (
			'stripe_connect' => $resp['stripe_user_id'],
			);
		$model->where ("id", $user_id);

		if ($model->update ('user', $data)){

			header('Location: ../register.php?sucess=stripe-connect');

		}else{

			header('Location: ../register.php?error=stripe-connect');
		}

	} else if (isset($_GET['error'])) { // Error

		header('Location: ../register.php?error=' . $_GET['error_description']);

	} else { 

		$model->where ("id", $user_id);
		$user = $model->getOne ("user");

		$authorize_request_body = array(
			'response_type' => 'code',
			'scope' => 'read_write',
			'client_id' => CLIENT_ID,
			'stripe_user[first_name]' => $user['first_name'],
			'stripe_user[last_name]' => $user['last_name'],
			'stripe_user[email]' => $user['email'],
			'stripe_user[phone_number]' => $user['mobile'],

			'stripe_user[url]'=>'https://firsthumb.com/',
			'stripe_user[business_type]' => 'sole_prop',
			'stripe_user[business_name]' => 'Car Lifts',
			'stripe_user[product_description]' => 'It\'s a social ridesharing servie',
			'stripe_user[product_category]' => 'professional_services',
			'stripe_user[street_address]' => 'First Thumb Ltd, Colmworth Business Park,  4 Eaton Court',
			'stripe_user[city]' => 'Eaton Socon',
			'stripe_user[state]' => '',
			'stripe_user[zip]' => 'PE19 8ER',

			);

		$url = AUTHORIZE_URI . '?' . http_build_query($authorize_request_body);

		header('Location: ' . $url);	
	}


	}
}