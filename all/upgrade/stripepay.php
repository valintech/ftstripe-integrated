<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 'On');
session_start ();
require_once ('../base.inc');
require_once ('../classes/user.inc');
require_once ('../classes/common.inc');
require_once ('../classes/region.inc');

require_once ('../classes/group.inc');
require_once ('../classes/usergroup.inc');
require_once ('../classes/journey.inc');
require_once ('../classes/used_freemiles.inc');
require_once ('../classes/used_freemiles.inc');
require_once ('class.model.php');

require_once ('stripe-php/init.php');

$user_id = isset ( $_POST ['user'] ) ? $_POST ['user'] : 0;
$token = isset ( $_POST ['id'] ) ? $_POST ['id'] : 0;
$card_save = isset ( $_POST ['card_save'] ) ? $_POST ['card_save'] : 0;
$card_name = isset ( $_POST ['card_name'] ) ? $_POST ['card_name'] : '';
$reuse = isset ( $_POST ['reuse'] ) ? $_POST ['reuse'] : 0;
// ........fetching the journey details according to passenger_id, payment_status=1 and status=p.
$jDetails = CJourney::getRecentPostionByJourneyIdStatus ( $user_id );

$driver_id = isset ( $jDetails ['driver_id'] ) ? $jDetails ['driver_id'] : 0;
$driver_amount = isset ( $jDetails ['driver_amount'] ) ? $jDetails ['driver_amount'] : 0;
$owner_amount = isset ( $jDetails ['owner_amount'] ) ? $jDetails ['owner_amount'] : 0;

/*
 * if($driver_amount < 0.50){
 * $driver_amount = 0.50;
 * }
 */

$free_miles = isset ( $jDetails ['free_miles_used'] ) ? round ( $jDetails ['free_miles_used'] ) : 0;
$ft_driver_payout = isset ( $jDetails ['ft_driver_payout'] ) ? $jDetails ['ft_driver_payout'] : 0;
$journey_id = isset ( $jDetails ['id'] ) ? $jDetails ['id'] : 0;

\Stripe\Stripe::setApiKey ( "sk_test_L3BKVL8dqC6DcPSLUzW1UW3l" );
// ........fetching driver details from the user table
$model = new model ();
$model->where ( "id", $driver_id );
$driver = $model->getOne ( "user" );
$stripe_account = isset ( $driver ['stripe_connect'] ) ? $driver ['stripe_connect'] : '';
if (isset ( $stripe_account )) {
	if ($free_miles > 0) {
		
		try {
			\Stripe\Transfer::create ( array (
					'amount' => round ( $ft_driver_payout * 100 ),
					'currency' => "gbp",
					'destination' => $stripe_account 
			) );		
			} catch ( \Stripe\Error\ApiConnection $e ) {
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Network communication with Stripe failed, perhaps try again.
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
			}  catch ( \Stripe\Error\Api $e ) {
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Stripe's servers are down!
			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				$out ['message'] = 'Whoops! payment failed, please try again!';
				$out ['status'] = false;
			} catch ( \Stripe\Error\Base $e ) {
				$out ['message'] = 'Whoops! payment failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error occured during the payment process.Please try again!
			} catch ( Exception $e ) {
				$out ['message'] = 'Whoops! payment failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error, completely unrelated to Stripe
			}
	}//end if (freemiles>0)
	
	if ($reuse) {
		// ..if the user save card details[pay-saved-btn->click]. retreiveing the card details of passenger from user table
		$get_user = new model ();
		$get_user->where ( "id", $user_id );
		$user = $get_user->getOne ( "user" );
		$card_key = isset ( $user ['card_key'] ) ? $user ['card_key'] : '';
		if (isset ( $card_key )) {
			try {
				$ttoken = \Stripe\Token::create ( array (
						"customer" => $card_key 
				), array (
						"stripe_account" => $stripe_account 
				) );
				
				$charge = \Stripe\Charge::create ( array (
						"amount" => round ( $driver_amount * 100 ),
						"currency" => "GBP",
						"source" => $ttoken->id,
						"description" => "Journey " . $journey_id,
						"application_fee" => round ( $owner_amount * 100 ) 
				), array (
						"stripe_account" => $stripe_account 
				) );
				if ($charge->paid == true) {
					$journey = new CJourney ();
					$journey->getSetFetchSend ( $journey_id );
					// .........updating the payout_transaction and payment_status.
					$journey->getRecentUpdatePayDriverJourneyIdStatusPayout ( $journey_id, $charge ['id'] );
					$out ['message'] = 'Successfully Paid';
					$out ['status'] = true;
				} else {				
					// Charge was not paid!
					$error_msg = 'Whoops! Your payment could not be processed. Please try again or use another card.';
					$out ['message'] =$error_msg;
					$out ['status'] = false;
				}
			} catch ( \Stripe\Error\InvalidRequest $e ) {
				$out ['message'] = 'Whoops! Payment failed due to one of several reasons!';
				$out ['status'] = false;
			} catch ( \Stripe\Error\Card $e ) {
				// Card was declined.
				$out ['message'] = 'Whoops Sorry your card was declined. Please contact your card company or add another card to your stripe account.';
				$out ['status'] = false;
			} catch ( \Stripe\Error\ApiConnection $e ) {
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Network communication with Stripe failed, perhaps try again.
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
			} catch ( \Stripe\Error\Api $e ) {
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Stripe's servers are down!
			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				$out ['message'] = 'Whoops! payment failed, please try again!';
				$out ['status'] = false;
			} catch ( \Stripe\Error\Base $e ) {
				$out ['message'] = 'Whoops! payment failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error occured during the payment process.Please try again!
			} catch ( Exception $e ) {
				$out ['message'] = 'Whoops! payment failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error, completely unrelated to Stripe
			}
		} else {
			// error: check card key
			$out ['message'] = 'Failed to retrieve Saved card details ';
			$out ['status'] = false;
		}
	} else {//else of reuse
		// ....the card details send through the POST [pay_btn click]

	    if($card_save){
			
			try {
		       $customer = \Stripe\Customer::create(array(
			   "source" => $token,
			    ));

		       /*$ttoken = \Stripe\Token::create(array(
			   "customer" => $customer->id,
			    ), array("stripe_account" => $stripe_account));

		      $charge = \Stripe\Charge::create(
			  array(
				"amount" => round($driver_amount * 100), 
				"currency" => "GBP",
				"source" => $ttoken->id,
				"description" => "Journey " . $journey_id,
				"application_fee" => round($owner_amount * 100)
				),
			  array("stripe_account" => $stripe_account)
			  );
			if ($charge->paid == true) {*/
			// ..........saving card details to user table.
			$card_save = new model();
			$card_save->where ("id", $user_id);
			$card_save_data = array(
			'card_key' => $customer->id,
			'card_name' => $card_name
			);
			$card_save->update ('user', $card_save_data);


			/*$journey = new CJourney();
			$journey->getSetFetchSend($journey_id);

			$journey_save = new model();
			$journey_save->where ("id", $journey_id);
			$journey_save_data = array(
			'transaction_id' => $charge['id'],
			'payment_status' => 2 
			);
			$journey_save->update ('journey', $journey_save_data);*/

		//$journey->getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id, $charge['id']);	
				$out ['message'] = 'Card Successfully Saved';
					$out ['status'] = true;					
				/*} else {
					// Charge was not paid!
					$error_msg = 'Whoops! Your Card could not be saved. Please try again or use another card.';
					$out ['message'] =$error_msg;
					$out ['status'] = false;
				}*/
			} catch ( \Stripe\Error\InvalidRequest $e ) {
				$out ['message'] = 'Whoops! Card could not be saved due to one of several reasons!';
				$out ['status'] = false;
			} catch ( \Stripe\Error\Card $e ) {
				// Card was declined.
				$out ['message'] = 'Whoops Sorry your card was declined. Please contact your card company or add another card to your stripe account.';
				$out ['status'] = false;
			} catch ( \Stripe\Error\ApiConnection $e ) {
				$out ['message'] = 'Whoops process failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Network communication with Stripe failed, perhaps try again.
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				$out ['message'] = 'Whoops process failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
			}  catch ( \Stripe\Error\Api $e ) {
				$out ['message'] = 'Whoops process failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Stripe's servers are down!
			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				$out ['message'] = 'Whoops! process failed, please try again!';
				$out ['status'] = false;
			} 	catch ( \Stripe\Error\Base $e ) {	
				$out ['message'] = 'Whoops! process failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error occured during the payment process.Please try again!
			} catch ( Exception $e ) {
				$out ['message'] = 'Whoops! process failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error, completely unrelated to Stripe
			}
		} else {
			try {
				$charge = \Stripe\Charge::create ( array (
				"amount" => round($driver_amount * 100), 
				"currency" => "GBP",
				"source" => $token,
				"description" => "Journey " . $journey_id,
				"application_fee" => round($owner_amount * 100)
				),
			array("stripe_account" => $stripe_account)
			);
		if ($charge->paid == true) {
		$journey = new CJourney();
		$journey->getSetFetchSend($journey_id);
		
		$journey_save = new model();
		$journey_save->where ("id", $journey_id);
		$journey_save_data = array(
			'transaction_id' => $charge['id'],
			'payment_status' => 2 
			);
		$journey_save->update ('journey', $journey_save_data);

		//$journey->getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id, $charge['id']);	
					$out ['message'] = 'Successfully Paid';
					$out ['status'] = true;
				
				} else {
					// Charge was not paid!
					$error_msg ='Whoops! Your payment could not be processed. Please try again or use another card.';
					$out ['message'] = $error_msg;
					$out ['status'] = false;
				}
			}
			catch ( \Stripe\Error\InvalidRequest $e ) {
				$out ['message'] = 'Whoops! Payment failed due to one of several reasons!';
				$out ['status'] = false;
			} catch ( \Stripe\Error\Card $e ) {
				// Card was declined.
				$out ['message'] = 'Whoops Sorry your card was declined. Please contact your card company or add another card to your stripe account.';
				$out ['status'] = false;
			} catch ( \Stripe\Error\ApiConnection $e ) {
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Network communication with Stripe failed, perhaps try again.
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
			}  catch ( \Stripe\Error\Api $e ) {
				$out ['message'] = 'Whoops Payment failed to go through due to poor network connection! Please try again later.';
				$out ['status'] = false;
				// Stripe's servers are down!
			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				$out ['message'] = 'Whoops! payment failed, please try again!';
				$out ['status'] = false;
			} 	catch ( \Stripe\Error\Base $e ) {	
				$out ['message'] = 'Whoops! payment failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error occured during the payment process.Please try again!
			} catch ( Exception $e ) {
				$out ['message'] = 'Whoops! payment failed, pleased try again!';
				$out ['status'] = false;
				// Unexpected error, completely unrelated to Stripe
			}
		}
	}//endif(reuse)
} else {
	   // error that stripe_connect required for payment.
	$out ['message'] = 'Whoops Sorry! Driver needs a stripe account to be considered as a First Thumb driver and to accept payments from passengers.';
	   $out ['status'] = false;
}
die ( json_encode ( $out ) );

?>
