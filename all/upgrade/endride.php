<?php session_start();
require_once('../base.inc');
require_once('../classes/user.inc');
require_once('../classes/common.inc');
require_once('../classes/region.inc');

require_once('../classes/group.inc');
require_once('../classes/usergroup.inc');
require_once('../classes/journey.inc');
require_once('../classes/used_freemiles.inc');
require_once('../classes/used_freemiles.inc');
require_once('class.model.php');

$user_id = isset($_GET['id']) ? $_GET['id'] : 0;
$jDetails = CJourney::getRecentPostionByJourneyIdStatus($user_id);


$model = new model();
$model->where ("id", $user_id);
$user = $model->getOne ("user");

$out['miles'] = isset($jDetails['miles']) ? $jDetails['miles'] : 0;
$out['paid_miles'] = isset($jDetails['paid_miles']) ? round($jDetails['paid_miles']) : 0;
$out['free_miles'] = isset($jDetails['free_miles_used']) ? round($jDetails['free_miles_used']) : 0;
$out['amount'] = isset($jDetails['amount']) ? $jDetails['amount'] : 0;
$out['stripe'] = isset($user['stripe_connect']) ? true : false;
$out['card'] = isset($user['card_name']) ? $user['card_name'] : false;

die(json_encode($out));