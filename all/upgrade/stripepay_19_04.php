<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 'On');
session_start();
require_once('../base.inc');
require_once('../classes/user.inc');
require_once('../classes/common.inc');
require_once('../classes/region.inc');

require_once('../classes/group.inc');
require_once('../classes/usergroup.inc');
require_once('../classes/journey.inc');
require_once('../classes/used_freemiles.inc');
require_once('../classes/used_freemiles.inc');
require_once('class.model.php');

require_once('stripe-php/init.php');

$user_id = isset($_POST['user']) ? $_POST['user'] : 0;
$token = isset($_POST['id']) ? $_POST['id'] : 0;
$card_save = isset($_POST['card_save']) ? $_POST['card_save'] : 0;
$card_name = isset($_POST['card_name']) ? $_POST['card_name'] : '';
$reuse = isset($_POST['reuse']) ? $_POST['reuse'] : 0;

$jDetails = CJourney::getRecentPostionByJourneyIdStatus($user_id);

$driver_id = isset($jDetails['driver_id']) ? $jDetails['driver_id'] : 0;
$driver_amount = isset($jDetails['driver_amount']) ? $jDetails['driver_amount'] : 0;
$owner_amount = isset($jDetails['owner_amount']) ? $jDetails['owner_amount'] : 0;

if($driver_amount < 0.50){
	$driver_amount =  0.50;
}

$free_miles = isset($jDetails['free_miles_used']) ? round($jDetails['free_miles_used']) : 0;
$ft_driver_payout = isset($jDetails['ft_driver_payout']) ? $jDetails['ft_driver_payout'] : 0;
$journey_id = isset($jDetails['id']) ? $jDetails['id'] : 0;
 
\Stripe\Stripe::setApiKey("sk_test_L3BKVL8dqC6DcPSLUzW1UW3l");

$model = new model();
$model->where ("id", $driver_id);
$driver = $model->getOne ("user");
$stripe_account = isset($driver['stripe_connect']) ? $driver['stripe_connect'] : '';

if($free_miles > 0 ){

	\Stripe\Transfer::create(array(
      'amount' => round($ft_driver_payout * 100),
      'currency' => "gbp",
      'destination' => $stripe_account
      ));
}

if($reuse){

	$get_user = new model();
	$get_user->where ("id", $user_id);
	$user = $get_user->getOne ("user");
	$card_key = isset($user['card_key']) ? $user['card_key'] : '';


	$ttoken = \Stripe\Token::create(array(
		"customer" => $card_key,
		), array("stripe_account" => $stripe_account));


	$charge = \Stripe\Charge::create(
		array(
			"amount" => round($driver_amount * 100), 
			"currency" => "GBP",
			"source" => $ttoken->id,
			"description" => "Journey " . $journey_id,
			"application_fee" => round($owner_amount * 100)
			),
		array("stripe_account" => $stripe_account)
		);

	$journey = new CJourney();
	$journey->getSetFetchSend($journey_id);
	$journey->getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id, $charge['id']);	

}else{

	if($card_save){

		$customer = \Stripe\Customer::create(array(
			"source" => $token,
			));

		$ttoken = \Stripe\Token::create(array(
			"customer" => $customer->id,
			), array("stripe_account" => $stripe_account));

		$charge = \Stripe\Charge::create(
			array(
				"amount" => round($driver_amount * 100), 
				"currency" => "GBP",
				"source" => $ttoken->id,
				"description" => "Journey " . $journey_id,
				"application_fee" => round($owner_amount * 100)
				),
			array("stripe_account" => $stripe_account)
			);

		$card_save = new model();
		$card_save->where ("id", $user_id);
		$card_save_data = array(
			'card_key' => $customer->id,
			'card_name' => $card_name
			);
		$card_save->update ('user', $card_save_data);


		$journey = new CJourney();
		$journey->getSetFetchSend($journey_id);

		$journey_save = new model();
		$journey_save->where ("id", $journey_id);
		$journey_save_data = array(
			'transaction_id' => $charge['id'],
			);
		$journey_save->update ('journey', $journey_save_data);

		//$journey->getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id, $charge['id']);	

	}else{


		$charge = \Stripe\Charge::create(
			array(
				"amount" => round($driver_amount * 100), 
				"currency" => "GBP",
				"source" => $token,
				"description" => "Journey " . $journey_id,
				"application_fee" => round($owner_amount * 100)
				),
			array("stripe_account" => $stripe_account)
			);


		$journey = new CJourney();
		$journey->getSetFetchSend($journey_id);
		
		$journey_save = new model();
		$journey_save->where ("id", $journey_id);
		$journey_save_data = array(
			'transaction_id' => $charge['id'],
			);
		$journey_save->update ('journey', $journey_save_data);

		//$journey->getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id, $charge['id']);	
	}

}

$out['message'] = 'Successfully Paid';
$out['status'] = true;


die(json_encode($out));