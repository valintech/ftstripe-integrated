<?php
/*
 * Interface providing methods to define specific database members
 * of an object
 */

interface IDefineDbMembers
{
	/*
	 * Get a list of DB members
	 * 
	 * @return   Keyed array where key os the member id. and the value for each
	 *           entry is an stdClass object containing a definition for the
	 *           member data. Members are 'type' which will be 's', 'i', 'f',
	 *           'd', 't', 'dt' and 'length'
	 */
	public function getDbMembers();
}