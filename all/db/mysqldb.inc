<?php
require_once('classes/common.inc');

/*
 * Database support ( MySql )
 */

class CDb
{
	const BEGIN_TRANS_SQL = "BEGIN";
	const COMMIT_TRANS_SQL = "COMMIT";
	const ROLLBACK_TRANS_SQL = "ROLLBACK";
	
	private static $conn = null;
	private static $server = null;
	private static $dbName = null;
	private static $user = null;
	private static $pwd = null;
	
	/*
	 * Set database information
	 * 
	 * @param $type   DB type (i.e MySql )
	 * @param $server Server name
	 * @param $db     Database name
	 * @param $user   Login user
	 * @param $pwd    Login password
	 */
	
	public static function setDb ($type, $server, $db, $user, $pwd)
	{
		CDb::$server = $server;
		CDb::$dbName = $db;
		CDb::$user = $user;
		CDb::$pwd = $pwd;
	}

	/*
	 * Return id. generated from last auto-id operation
	 * 
	 * @return id.
	 */
	
	public static function getLastAutoId ()
	{
		return mysql_insert_id(CDb::getConn());	
	}
	
	/*
	 * Get database connection
	 * @return Connection handle or false
	 */
	
	public static function getConn ()
	{
		if (CDb::$conn)
			return CDb::$conn;
		if (CDb::$server == false)
			return false;	
		CLogging::info(sprintf("%s %s %s", CDb::$server, CDb::$user, CDb::$pwd));
		CDb::$conn = mysql_connect(CDb::$server, CDb::$user, CDb::$pwd);
		if (CDb::$conn)
		{
			CLogging::info(sprintf("Selecting DB %s", CDb::$dbName));
			if (mysql_select_db(CDb::$dbName, CDb::$conn) == false)
				CLogging::error(sprintf('MySQL:- %s', mysql_error()));
		}
		return CDb::$conn;
	}

	/*
	 * Release database connection
	 */
	
	public static function releaseConn ()
	{
		if (CDb::$conn)
		{
			mysql_close(CDb::$conn);
			CDb::$conn = null;
		}		
	}
	
	/*
	 * Execute SQL query
	 * 
	 * @param $sql - The SQL query
	 * @ereturn true or false
	 */
	
	public static function query ($sql)
	{
		if (CDb::getConn() == false)
			return false;
		CLogging::info($sql);
		$out = mysql_query($sql, CDb::$conn);
		if ($out == false)
			CLogging::error(sprintf('MySQL:- %s', mysql_error()));
		return $out;
	}

	/*
	 * Retrieve row as object
	 * 
	 * @param $res   Connection resource
	 * @return       Object containing data
	 */
	
	public static function getRowObject ($res)
	{
		if (CDb::getConn() == false)
			return false;
		$out = mysql_fetch_object($res);
		return $out;
	}
	
	/*
	 * Retrieve row as array
	 * 
	 * @param $res   Connection resource
	 * @return       Associative array containing data
	 */
	
	public static function getRowArray ($res)
	{
		if (CDb::getConn() == false)
			return false;
		$out = mysql_fetch_assoc($res);
		return $out;
	}
	
	/*
	 * Start transation
	 * 
	 * @param $apply    true to apply transaction command
	 * @return          false if failed
	 */
	
	public static function beginTrans ($apply = true)
	{
		return $apply ? CDb::query(CDb::BEGIN_TRANS_SQL) : true;
	}

	/*
	 * Commit transation
	 * 
	 * @param $apply    true to apply transaction command
	 * @return          false if failed
	 */
	
	public static function commitTrans ($apply = true)
	{
		return $apply ? CDb::query(CDb::COMMIT_TRANS_SQL) : true;
	}
	
	/*
	 * Rollback transaction
	 * 
	 * @param $apply    true to apply transaction command
	 * @return          false if failed
	 */
	
	public static function rollbackTrans ($apply = true)
	{
	
		return $apply ? CDb::query(CDb::ROLLBACK_TRANS_SQL) : true;
		//return true;
	}
	
	/*
	 * Format a value for SQL SELECT
	 * 
	 * @param $colId  Column id.
	 * @param $type   Value type s=string, i=integer, d=date, dt=date/time
	 * @param $value  [optional] Overriding date value i.e now()
	 * @return        Formatted value
	 */
	
	public static function makeSelectValue ($colId, $type, $value = false)
	{
		if ($value == false)
			$value = $colId;
		
		$out = "";

		switch($type)
		{
			case CCommon::DATETIME_TYPE:			// date/time
			$format = "%Y%m%d%H%i%s";
			$out = sprintf("DATE_FORMAT(%s,'%s') %s", $value, $format, $colId);
			break;
			
			case CCommon::DATE_TYPE:				// date
			$format = "%Y%m%d";
			$out = sprintf("DATE_FORMAT(%s,'%s') %s", $value, $format, $colId);
			break;
			
			case CCommon::TIME_TYPE:				// time
			$format = "%H%i%s";
			$out = sprintf("DATE_FORMAT(%s,'%s') %s", $value, $format, $colId);
			break;
			
			default:
			$out = $colId;
			break;
		}
		
		return $out;
	}
	
	/*
	 * Format a value for SQL INSERT/UPDATE
	 * 
	 * @param $val           The value
	 * @param $type          Value type s=string, i=integer, d=date, dt=date/time
	 * @param $emptyIsNull   Make empty values NULL
	 * @return               Formatted value
	 */
	
	public static function makeSqlValue ($val, $type, $emptyIsNull = true)
	{
		if ($val == "" && $emptyIsNull)
			return "NULL";
		
		$out = $val;
		
		switch($type)
		{
			case CCommon::STRING_TYPE:		// string
			$out = sprintf("'%s'", mysql_real_escape_string($val));
			break;

			case CCommon::INT_TYPE:			// integer
			$out = (int)$val;
			break;

			case CCommon::DATETIME_TYPE:	// date/time
			$out = sprintf("'%s'", date("YmdHis", $val));
			break;
			
			case CCommon::DATE_TYPE:		// date
			$out = sprintf("'%s'", date("Ymd", $val));
			break;
			
			case CCommon::TIME_TYPE:		// time
			$out = sprintf("'%s'", date("His", $val));
			break;
			
                        case CCommon::DECIMAL_TYPE:		// time
			$out = sprintf("%01.2f",$val);
			break;
                    
			default:
			break;
		}
		
		return $out;
	}
	
	/*
	 * Translate an SQL value into internal format
	 * 
	 * @param $val           The value
	 * @param $type          Value type s=string, i=integer, d=date, dt=date/time
	 * @return               PHP value
	 */
	
	public static function makePhpValue ($val, $type)
	{
		$out = "";

		if (isset($val))
			switch($type)
			{
				case CCommon::DATETIME_TYPE:		// date/time
				// WTF? Why do I need these replaces sometimes and
				// not others?
				$val = str_replace(' ','',$val);
				$val = str_replace('-','',$val);
				$val = str_replace(':','',$val);
				$year = (int)substr($val, 0, 4);
				$month = (int)substr($val, 4, 2);
				$day = (int)substr($val, 6, 2);
				$hour = (int)substr($val, 8, 2);
				$minute = (int)substr($val, 10, 2);
				$second = (int)substr($val, 12, 2);
				$out = mktime($hour, $minute, $second, $month, $day, $year);
				break;
				
				case CCommon::DATE_TYPE:			// date
				$year = (int)substr($val, 0, 4);
				$month = (int)substr($val, 4, 2);
				$day = (int)substr($val, 6, 2);
				$out = mktime(0, 0, 0, $month, $day, $year);
				break;
				
				case CCommon::TIME_TYPE:			// time
				$hour = (int)substr($val, 0, 2);
				$minute = (int)substr($val, 2, 2);
				$second = (int)substr($val, 4, 2);
				$out = mktime($hour, $minute, $second);
				break;
				
				default:
				$out = $val;
				break;
			}
		
		return $out;
	}
}