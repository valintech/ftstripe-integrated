<?php
class CSessionDb
{
	const CREATE_SESSION_SQL = "INSERT INTO session (%s) VALUES (%s)";
	const UPDATE_SESSION_SQL = "UPDATE session SET %s WHERE id=%ld";
	const GET_SESSION_SQL = "SELECT %s from session WHERE id=%ld";
	const DELETE_SESSION_SQL = "DELETE from session WHERE id=%ld";
	const GET_SERVER_TIME_SQL = 'SELECT %s from session';
	
	/*
	 * Poll a session
	 * 
	 * @param $session  A CSession object to store
	 * @return          true or false
	 */
	
	public static function poll ($session)
	{
		return CSessionDb::save($session, array(CSession::UPDATED));
	}
	
	/*
	 * Store a session
	 * 
	 * @param $session  A CSession object to store
	 * @param $colIds   Array of columns ids. to update
	 * @return          true or false
	 */
	
	public static function save ($session, $colIds = false)
	{
		// Get list of columns. Include id. column if session already exists
		$id = $session->get(CSession::ID);
		$cols = CRoot::getFilteredDbMembers('CSession', $colIds);
		
		// Turn user data into SQL compatible strings
		$vals = array();
		foreach ($cols as $colId => $col)
			$vals[$colId] = CDb::makeSqlValue($session->get($colId), $col['type']);
		
		// Create SQL statement for insert or update
		unset($cols[CSession::ID]);
		unset($vals[CSession::ID]);
		$vals[CSession::UPDATED] = 'now()';
		if ($id == 0)
		{
			$vals[CSession::CREATED] = 'now()';
			$sql = sprintf(CSessionDB::CREATE_SESSION_SQL, join(',', array_keys($cols)), join(',', $vals));
		}
		else
		{
			unset($cols[CSession::CREATED]);
			unset($vals[CSession::CREATED]);
			$tmp = array();
			foreach ($cols as $colId => $col)
				$tmp[] = sprintf('%s=%s', $colId, $vals[$colId]);
			$sql = sprintf(CSessionDb::UPDATE_SESSION_SQL, join(',', $tmp), $id);
		}

		CDb::BeginTrans();
		$res = CDb::query($sql);
		if ($res)
		{
			// Retrieve the auto ID if "insert" performed
			if ($id == 0)
			{
				$id = CDb::getLastAutoId();
				$session->set(CSession::ID, $id);
			}
			CDb::commitTrans();
		}
		else
			CDb::rollbackTrans();
		
		return $res ? true : false;
	}

	/*
	 * Load session
	 * 
	 * As well as the normal DB column members assigned into the '$session'
	 * object, an extra member 'serverTime' is also stored.
	 * 
	 * @param $sessionId   Session id.
	 * @param $session     CSession object
	 * @return             true of false. 
	 */
	
	public static function load ($sessionId, $session)
	{
		$cols = CRoot::getDbMembers('CSession');
		$tmp = array();
		foreach ($cols as $colId => $col)
			$tmp[] = CDb::makeSelectValue($colId, $col['type']);
		$sql = sprintf(CSessionDb::GET_SESSION_SQL, join(',', $tmp), $sessionId);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = mysql_fetch_assoc($res);
		if ($o == false)
			return false;
		foreach ($o as $colId => $val)
			$session->set($colId, CDb::makePhpValue($val, $cols[$colId]['type']));
		// Get server time additionally
		$sql = sprintf(CSessionDb::GET_SERVER_TIME_SQL,
					   CDb::makeSelectValue('server_time', CCommon::DATETIME_TYPE, 'now()')); 
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = mysql_fetch_assoc($res);
		if ($o == false)
			return false;
		$session->set('serverTime', CDb::makePhpValue($o['server_time'], CCommon::DATETIME_TYPE));
		return true;
	}

	/*
	 * Delete session
	 * 
	 * @param $sessionId   Session id. to delete
	 * @return             true of false
	 */
	
	public static function delete ($sessionId)
	{
		if ($sessionId == 0)
			return;
		$sql = sprintf(CSessionDb::DELETE_SESSION_SQL, $sessionId);
		CDb::BeginTrans();
		$res = CDb::query($sql);
		if ($res)
			CDb::commitTrans();
		else
			CDb::rollbackTrans();
		return $res;
	}
}
?>
