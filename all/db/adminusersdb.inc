<?php

/*
 * Class handler for journey requests
 */

class CAdminUsersDb {

    // SQL statements

    const NEW_SQL = 'INSERT INTO admin_users (%s) VALUES (%s)';
    const GET_SQL = 'SELECT %s FROM admin_users WHERE %s';
    const UPDATE_SQL = 'UPDATE admin_users SET %s WHERE id=%s';

    /*
     * Load request by id.
     *
     * @param $adminusers         A CJourney object to fill
     * @param $id              Journey request id.
     * @return                 true or false
     */

    public static function load($adminusers, $id) {
        return self::_loadCore($adminusers, array(array('fieldId' => CJourney::ID, 'fieldVal' => $id)));
    }

    /*
     * Load active journey for a given passenger
     *
     * @param $adminusers         A CJourney object to fill
     * @param $passId          Passenger id.
     * @return                 true or false
     */

    public static function loadAdminInfo($adminusers, $username, $str_password) {
        $active = "1";
        $queryFields = array();
        $queryFields[] = array('fieldId' => CAdminusers::USERNAME, 'fieldVal' => "'$username'");
        $queryFields[] = array('fieldId' => CAdminusers::PASSWORD, 'fieldVal' => "'$str_password'");
        $queryFields[] = array('fieldId' => CAdminusers::ACTIVE, 'fieldVal' => "'$active'");
        return self::_loadCore($adminusers, $queryFields);
    }

    /*
     * Core code for 'loadxxx' methods
     *
     * @param $adminusers         A CJourney object to fill
     * @param $queryFields     Array of query arrays containing 'fieldId' and
     *                         'fieldVal' members
     * @return                 true or false
     */

    protected static function _loadCore($adminusers, $queryFields) {
        $cols = CRoot::getDbMembers('CAdminusers');
        $types = array();
        foreach ($cols as $colId => $col) {
            $cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
            $types[$colId] = $col['type'];
        }
        $where = array();
        foreach ($queryFields as $queryField)
            $where[] = sprintf('%s=%s', $queryField['fieldId'], $queryField['fieldVal']);
        $sql = sprintf(self::GET_SQL, join(',', array_keys($cols)), join(' AND ', $where));
        $res = CDb::query($sql);

        $o = mysql_fetch_object($res);
        $num_rows = mysql_num_rows($res);
        $result = 1;
        if ($num_rows == 0)
            $result = 0;
        if ($o) {
            foreach ($o as $fieldId => $fieldVal) {
                session_start();
                $_SESSION[$fieldId] = $fieldVal;
            
                $adminusers->set($fieldId, CDb::makePhpValue($fieldVal, $types[$fieldId]));
            }
        }print_r($_SESSION);
        return $result;
    }

}

?>
