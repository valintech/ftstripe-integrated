<?php
class CWebSessionDb
{
	// SQL constants
	const GET_SESSION_SQL = "SELECT data,updated FROM websession WHERE id='%s'";
	const UPDATE_SESSION_SQL = "UPDATE websession SET data='%s', updated=now() WHERE id='%s'";
	const INSERT_SESSION_SQL = "INSERT INTO websession (id,data,updated) VALUES ('%s','%s',now())";
	const DESTROY_SESSION_SQL = "DELETE FROM websession WHERE id='%s'";
	
	// -1 = undefined state, true = session exists, false = session does not exist
	private static $sessionExists = -1;
	
	/*
	 * Get session data
	 * 
	 * @param $res - A database resource
	 * @return Session data or false if none available
	 */
	
	public static function restore ($sessionId)
	{
		CWebSessionDb::$sessionExists = false;
		$sql = sprintf(CWebSessionDb::GET_SESSION_SQL, $sessionId);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = CDb::getRowObject($res);
		if ($o == false)
			return false;
		CWebSessionDb::$sessionExists = true;
		return $o->data;
	}
	
	public static function save ($sessionId, $sessionData)
	{
		CDb::beginTrans();
		$sql = CWebSessionDb::$sessionExists
			   ? sprintf(CWebSessionDb::UPDATE_SESSION_SQL, $sessionData, $sessionId)
			   : sprintf(CWebSessionDb::INSERT_SESSION_SQL, $sessionId, $sessionData);
		$out = CDb::query($sql);
		if ($out)
		{
			CDb::commitTrans();
			CWebSessionDb::$sessionExists = true;
		}
		else
			CDb::rollbackTrans();
		return $out;					
	}

	public static function destroy ($sessionId)
	{
		$sql = sprintf(CWebSessionDb::DESTROY_SESSION_SQL, $sessionId);
		CDb::beginTrans();
		$out = CDb::query($sql);
		if ($out)
			CDb::commitTrans();
		else
			CDb::rollbackTrans();
		return $out;
	}
}
