<?php
/*
 * Carpool database access class
 */

class CCarpoolDb extends CRoot
{
	const NEW_SQL = "INSERT INTO carpool (%s) VALUES (%s)";
	const UPDATE_SQL = "UPDATE carpool SET %s WHERE id=%s";
	const GET_SQL = "SELECT %s FROM carpool WHERE %s=%s";
	
	/*
	 * Constructor
	 */
	
	function __construct ()
	{
		parent::__construct();
	}
	
	/*
	 * Load carpool by user id.
	 * 
	 * @param $userId    User id.
	 * @param $carpool   CCarpool object to load
	 * @return           true or false
	 */
	
	public static function loadByUserId ($userId, $carpool)
	{
		return CCarpoolDb::_loadCore(CCarpool::USER_ID, $userId, $carpool);
	}
	
	/*
	 * Load carpool by id.
	 * 
	 * @param $id        Carpool id.
	 * @param $carpool   CCarpool object to load
	 * @return           true or false
	 */
	
	public static function load ($id, $carpool)
	{
		return CCarpoolDb::_loadCore(CCarpool::ID, $id, $carpool);
	}

	/*
	 * Core for "load..." methods
	 * 
	 * @param $fieldId   Id. of field to query
	 * @param $fieldVal  Query field value
	 * @param $user      A CCarpool object to load into
	 * @return           CCarpool object or false
	 */
	
	protected static function _loadCore ($fieldId, $fieldVal, $carpool)
	{
		$cols = CRoot::getDbMembers('CCarpool');
		foreach ($cols as $colId => $col)
			$cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
		$sql = sprintf(CCarpoolDb::GET_SQL, join(',', $cols), $fieldId, $fieldVal);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = mysql_fetch_object($res);
		if ($o == false)
			return false;
		foreach ($o as $colId => $val)
			$carpool->set($colId, CDb::makePhpValue($val, $cols[$colId]['type']));
		return true;
	}
	
	/*
	 * Save
	 * 
	 * @param $carpool   A CCarpool object to save
	 * @return           true or false
	 */
	
	public static function save ($carpool)
	{
		// Get list of columns. Include id. column if already exists
		$id = $carpool->get(CCarpool::ID);
		$cols = CRoot::getFilteredDbMembers('CCarpool', false, array(CCarpool::ID));
		
		// Turn data into SQL compatible strings
		$vals = array();
		foreach ($cols as $colId => $col)
			$vals[$colId] = CDb::makeSqlValue($carpool->get($colId), $col['type']);
		
		// Create SQL statement for insert or update
		$vals[CCarpool::UPDATED] = 'now()';
		if ($id == 0)
		{
			$vals[CCarpool::CREATED] = 'now()';
			$sql = sprintf(CCarpoolDb::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
		}
		else
		{
			unset($cols[CCarpool::CREATED]);
			unset($vals[CCarpool::CREATED]);
			$tmp = array();
			foreach ($cols as $colId => $col)
				$tmp[] = sprintf("%s=%s", $colId, $vals[$colId]);
			$sql = sprintf(CCarpoolDb::UPDATE_SQL, join(",", $tmp), $id);
		}
			
		CDb::BeginTrans();
		$res = CDb::query($sql);
		if ($res)
		{
			// Retrieve the auto ID if "insert" performed
			if ($id == 0)
			{
				$id = CDb::getLastAutoId();
				$carpool->set(CCarpool::ID, $id);
			}
			CDb::commitTrans();
		}
		else
			CDb::rollbackTrans();
		
		return $res ? true : false;
	}
}
?>
