<?php

/*
 * Class handler for journey requests
 */

class CUserFreeMilesDb {

    // SQL statements

    const NEW_SQL = 'INSERT INTO user_freemiles (%s) VALUES (%s)';
    const GET_SQL = 'SELECT %s FROM user_freemiles WHERE %s';
    const UPDATE_SQL = 'UPDATE user_freemiles SET %s WHERE id=%s';

    /*
     * Load request by id.
     *
     * @param $adminusers         A CJourney object to fill
     * @param $id              Journey request id.
     * @return                 true or false
     */
 
   public static function save($userfreemiles) {

        $id = $userfreemiles->get(CUserFreeMiles::ID);
                   
        $cols = CRoot::getFilteredDbMembers('CUserFreeMiles', false,array(CUserFreeMiles::ID));
        // Turn data into SQL compatible strings
        $vals = array();

        foreach ($cols as $colId => $col)
            $vals[$colId] = CDb::makeSqlValue($userfreemiles->get($colId), $col['type']);
        $vals[CUserFreeMiles::CREATEDBY] = 'now()';

        // Create SQL statement for insert or update
        if ($id == 0) {

            $sql = sprintf(self::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
        } 

        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
            if ($id == 0) {
                $id = CDb::getLastAutoId();
                $userfreemiles->set(CUserFreeMiles::ID, $id);
            }
            CDb::commitTrans();
        }
        else
            CDb::rollbackTrans();

        return $res ? true : false;
    }
}
?>
