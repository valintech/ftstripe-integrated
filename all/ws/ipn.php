<?php
require_once('wsbase.inc');
require_once('db/mysqldb.inc');

// Turn pounds into miles (do nothing if currency is incorrect)
function BuyMilesWithGbPounds($user, $payment_amount)
{
	// credit is in miles, while payment_amount is GBP.
	// Current purchase rate is 20p per mile
	$miles_purchased = ($payment_amount * 100) / CConfig::PENCE_PER_MILE_PURCHASE_RATE;
	$user->set(CUser::CREDIT, $user->get(CUser::CREDIT) + $miles_purchased);
	$user->save();
}


// TODO Change this to use CCommon::sendMail()
$do_mail = true;

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
foreach ($_POST as $key => $value) {
	$value = urlencode(stripslashes($value));
	$req .= "&$key=$value";
}

// post back to PayPal system to validate
$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
$fp = fsockopen (CConfig::getPaypalIpnAckUrl(), 443, $errno, $errstr, 30);

if (!$fp) {
	// HTTP ERROR
	CLogging::Error("Failed to connect to paypal, post data was:\n" . $req);
	exit;
}

//send POST data back to paypal
fputs ($fp, $header . $req);

// Read response; last line should be "VERIFIED"; keep full headers
// to for debug
$res = "";
$fullres = "";

while (!feof($fp)) {
	$res = fgets($fp, 1024);
	$fullres .= $res;
}
fclose ($fp);

// The main function of this class is to enable the post data to be easily passed around
class PostData
{
	public $raw_post_data;
	public $item_name;
	public $item_number;
	public $payment_status;
	public $payment_gross;
	public $payment_fee;
	public $payment_currency;
	public $txn_id;
	public $receiver_email;
	public $payer_email;
	public $payment_net;	//derived
	
	function __construct (
		$raw_data,
		$name, $number, $status, $gross,
		$fee, $currency, $id,
		$rx_email, $tx_email
	)
	{
		$this->raw_post_data = $raw_data;
		$this->item_name = $name;
		$this->item_number = $number;
		$this->payment_status = $status;
		$this->payment_gross = $gross;
		$this->payment_fee = $fee;
		$this->payment_currency = $currency;
		$this->txn_id = $id;
		$this->receiver_email = $rx_email;
		$this->payer_email = $tx_email;
		
		$this->payment_net = $gross - $fee;
	}
}

$postData = new PostData(
	$req,
	$_POST['item_name'],
	$_POST['item_number'],
	$_POST['payment_status'],
	$_POST['mc_gross'],
	$_POST['mc_fee'],
	$_POST['mc_currency'],
	$_POST['txn_id'],
	$_POST['receiver_email'],
	$_POST['payer_email']
);

// assign posted variables to local variables
$item_name = $_POST['item_name'];
$item_number = $_POST['item_number'];
$payment_status = $_POST['payment_status'];
$payment_gross = $_POST['mc_gross'];
$payment_fee = $_POST['mc_fee'];
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$receiver_email = $_POST['receiver_email'];
$payer_email = $_POST['payer_email'];
$payment_amount = $payment_gross - $payment_fee;


class IpnEmail
{
	private $emailHeading;
	private $postDataref;
	
	function __construct ( $heading, $postData )
	{
		$this->emailHeading = $heading;
		$this->postDataref = $postData;
	}
	
	// send email containing raw post data (replace '&' with \n\t for readability though)
	public function send_raw()
	{
		$to      = CConfig::PAYMENT_LOGGING_EMAIL;  
		$subject = 'Paypal Instant Payment Notification' . $this->emailHeading;
		$headers = 'From:noreply@eco-gogo.co.uk' . "\r\n";
		$message = preg_replace('/&/', '\r\n\t', $this->postDataref->raw_post_data);
		if ($do_mail)
		{
			mail($to, $subject, $message, $headers);
		}
	}
	
	// Send the formatted post data and miss what we don't think we're interested in.
	public function send()
	{
		$to      = CConfig::PAYMENT_LOGGING_EMAIL;  
		$subject = 'Paypal Instant Payment Notification' . $this->emailHeading;
		$headers = 'From:noreply@eco-gogo.co.uk' . "\r\n";
		//TODO - replace this with nice formatting
		$message = preg_replace('/&/', '\r\n\t', $this->postDataref->raw_post_data);
		if ($do_mail)
		{
			mail($to, $subject, $message, $headers);
		}
	}
}

//common email data
$to      = CConfig::PAYMENT_LOGGING_EMAIL;  
$subject = 'Paypal Instant Payment Notification';  
$message = "";
$headers = 'From:noreply@eco-gogo.co.uk' . "\r\n"; 

if (strcmp ($res, "VERIFIED")) {
	// Transaction did not come from paypal!
	$subject .= ' - Bogus notification';  
	$message .= "Bogus payment:\r\n" . $req;
	$message .= "Response was:\r\n" . $fullres . "\r\n";
	CLogging::Error($subject . "\r\n" . $message);
	if ($do_mail)
		mail($to, $subject, $message, $headers); 
	exit;
}

// Transaction truly came from paypal!
// Check that receiver_email is your Primary PayPal email
if (0 != strcmp ($receiver_email, CConfig::PRIMARY_PAYPAL_EMAIL)) {
	$subject .= ' - bad receiver_email';
	$message .= "sent to: $receiver_email\r\n" . $req;
	CLogging::Error($subject . "\r\n" . $message);
	if ($do_mail)
		mail($to, $subject, $message, $headers); 
	exit;
}

// Check that the payment_status is either Completed or Refunded
if (
	 !	(
				(0 == strcmp ($payment_status, "Completed")) 
			||	(0 == strcmp ($payment_status, "Refunded"))
		)
)
{
	$subject .= ' - not Completed';
	$message .= "state: " . $payment_status . "\r\n" . $req;
	CLogging::Error($subject . "\r\n" . $message);
	if ($do_mail)
		mail($to, $subject, $message, $headers); 
	exit;
}


// If we get this far then we have a Completed payment to us or refund by us.

// Record the time for later storage in the database.
$timeofday = gettimeofday();

// Check that txn_id has not been previously processed
$conn = CDb::getConn();
$txn_id = mysql_real_escape_string($txn_id);
if(
		$field = CDb::getRowArray(
			CDb::query(
				"SELECT `TransactionID` FROM `paypaltransactions` WHERE `TransactionID`='$txn_id'"
			)
		)
) 
{
	$subject .= ' - Duplicate TransactionID';  
	$message .= "Duplicate Transaction from: " . $payer_email . " to: " . $receiver_email . "\r\n";
	$message .= $req;
	CLogging::Error($subject . "\r\n" . $message);
	if ($do_mail)
		mail($to, $subject, $message, $headers); 
	exit;
}

//N.B.  Refunds are processed as a negative payment from payer_email to receiver_email.
//The user is always "$payer_email".

// If the user is in our database, process payment, else send a warning email.
// The code tries to find the user first by any recorded paypal email then by
// their standard account email.
// Not going to exit in any case though as this is a real payment and will
// still need recording in the database.
$user = new CUser();
if (
			( $user->loadBy(CUser::PAYPAL_EMAIL, $payer_email) == true )
		&&	( $user->get(CUser::ID) !== 0 )
)
{
	$main_account_email = $user->get(CUser::EMAIL);
}
else if (
			( $user->loadBy(CUser::EMAIL, $payer_email) == true )
		&&	( $user->get(CUser::ID) !== 0 )
)
{
	$main_account_email = $payer_email;
}
else
{
	// Unable to find account.
	$main_account_email = '';
}

if ( '' !== $main_account_email )
{
	// N.B.  We don't decrease user's credit for refunds here.  That shall already
	// have been done when the "Recover Credit" button was hit.  If we only decremented
	// here (after paypal have processed) then there would be a large window during which
	// the user could recover credit they did not have.

	$subject .= ' - ' . $payment_status;	// "Completed" or "Refunded"
	if ( $payment_amount > 0 )	// "Completed"
	{
		// We can only process payment in GBP
		if ( 0 == strcmp ($payment_currency, "GBP"))
		{
			//N.B. We increment by the gross payment amount ($payment_gross)
			//rather than the net ($payment_amount) as Steve's directed
			//that we will absorb the paypal charges.
			BuyMilesWithGbPounds($user, $payment_gross);

			$message .= "Transaction complete\n"
			. "Increasing account balance of " . $main_account_email . "(" . $payer_email . ")"
			. "\nby " . $payment_currency . ' ' . $payment_amount	. "\n"
			. "Transaction Data:\n"
			. "Gross payment of: ". $payment_currency . ' ' . $payment_gross
			. "\nfrom:" . $payer_email
			. "\nto: " . $receiver_email 
			. "\ntransaction id: " . $txn_id 
			. "\nPaypal fee: " . $payment_currency . ' ' . $payment_fee
			. "\n";
		}
		else
		{
			// Not GBP, so not increasing balance.
			$subject .= ' - Incorrect currency';  
			$message .= "Incorrect currency from : " . $main_account_email . "(" . $payer_email . ")" . "\r\n"
			. "\n State = " . $payment_status . "\n"
			. "\npayment of: " . $payment_currency . ' ' . $payment_amount	. "\n"
			. "Transaction Data:\n"
			. "Gross payment of: ". $payment_currency . ' ' . $payment_gross
			. "\nfrom:" . $payer_email
			. "\nto: " . $receiver_email 
			. "\ntransaction id: " . $txn_id 
			. "\nPaypal fee: " . $payment_currency . ' ' . $payment_fee
			. "\n\n";
			$message .= "Payment:\r\n" . $req;
			$message .= "Response was:\r\n" . $fullres . "\r\n";
			CLogging::Error($subject . "\r\n" . $message);
		}
	}
	else	// "Refunded"
	{
		$message .= "Refund complete\n"
		. "Confirmation of refund of " 
		. $payment_currency . ' ' . -$payment_amount	. " to " . $main_account_email . "(" . $payer_email . ")" . "\n"
		. "Website balance not altered on receipt of this IPN, as should already have been done at refund request.\n" 
		. "Transaction Data:\n"
		. "Gross payment of: ". $payment_currency . ' ' . $payment_gross
		. "\nfrom:" . $payer_email
		. "\nto: " . $receiver_email 
		. "\ntransaction id: " . $txn_id 
		. "\nPaypal fee: " . $payment_currency . ' ' . $payment_fee
		. "\n";
	}
	CLogging::Info($subject . "\r\n" . $message);
}
else
{
	// Oops, we didn't recognize the user
	$subject .= ' - Unknown user';  
	$message .= "Transaction from unrecognized user: " . $payer_email . "\r\n"
	. "\n State = " . $payment_status . "\n"
	. "\npayment of: " . $payment_currency . ' ' . $payment_amount	. "\n"
	. "Transaction Data:\n"
	. "Gross payment of: ". $payment_currency . ' ' . $payment_gross
	. "\nfrom:" . $payer_email
	. "\nto: " . $receiver_email 
	. "\ntransaction id: " . $txn_id 
	. "\nPaypal fee: " . $payment_currency . ' ' . $payment_fee
	. "\n\n";
	$message .= "Payment:\r\n" . $req;
	$message .= "Response was:\r\n" . $fullres . "\r\n";
	CLogging::Error($subject . "\r\n" . $message);
}


//Sanitise all posted data and save to database
{
	$item_name = mysql_real_escape_string($item_name);
	$item_number = mysql_real_escape_string($item_number);
	$payment_status = mysql_real_escape_string($payment_status);
	$payment_gross = mysql_real_escape_string($payment_gross);
	$payment_fee = mysql_real_escape_string($payment_fee);
	$payment_currency = mysql_real_escape_string($payment_currency);
	$txn_id = mysql_real_escape_string($txn_id);
	$receiver_email = mysql_real_escape_string($receiver_email);
	$payer_email = mysql_real_escape_string($payer_email);
	$payment_amount = mysql_real_escape_string($payment_amount);
	$ipn_datetime = $timeofday["sec"];	// Seconds since the unix epoch (UTC)

	CDb::query(
		"INSERT INTO paypaltransactions (
		ItemName,ItemNumber,PaymentStatus,PaymentGross,PaymentFee,
		PaymentCurrency,TransactionID,ReceiverEmail,PayerEmail,
		PaymentAmount,IpnNotificationDateTime)
		VALUES 
		('$item_name','$item_number','$payment_status','$payment_gross','$payment_fee',
		'$payment_currency','$txn_id','$receiver_email','$payer_email',
		'$payment_amount','$ipn_datetime')"
	) or CLogging::Error(mysql_error());
}

if ($do_mail)
{
	mail($to, $subject, $message, $headers); 
}

// Confirmation email to customer should not be necessary
// as paypal do that already
?>

