<?php
set_include_path(get_include_path().PATH_SEPARATOR.'../'.PATH_SEPARATOR.'../thirdparty/ZendFramework-1.10.5/library/');

require_once("includes/config.inc");
require_once("classes/logging.inc");	
require_once("classes/login.inc");
require_once(CConfig::DB_INCLUDE);

CDb::setDb(CConfig::DB_TYPE, CConfig::DB_HOST, CConfig::DB_NAME, CConfig::DB_USER, CConfig::DB_PWD);

/*
 * Returns a compacted version number:-
 * Version 4.1.2 = (4*100) + (1*10) + 2 = 412
 * Version 5.1.2 = (5*100) + (1*10) + 2 = 512
 */

function phpVer()
{
	$aryTemp = explode( '.', PHP_VERSION );
	return ( (int)$aryTemp[0] * 100 ) + ( (int)$aryTemp[1] * 10 ) + (int)$aryTemp[2];
}
?>
