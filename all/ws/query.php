<?php
require_once('wsbase.inc');
require_once('classes/loginws.inc');
require_once('classes/logoutws.inc');
require_once('classes/journeyws.inc');
require_once('classes/smsws.inc');
require_once('classes/appws.inc');
require_once('classes/smsgwws.inc');
require_once('classes/userws.inc');
//require_once('classes/searchws.inc');

require_once('thirdparty/ZendFramework-1.10.5/library/Zend/Server/Exception.php');
require_once('thirdparty/ZendFramework-1.10.5/library/Zend/Json/Server.php');
require_once('thirdparty/ZendFramework-1.10.5/library/Zend/Json/Server/Error.php');
require_once('thirdparty/ZendFramework-1.10.5/library/Zend/Json/Server/Request.php');

CCommon::dumpRequestVals();
// We do not want session cookies as we include the session id (which is the
// same thing) in each of our requests.  In fact, if you allow session cookies
// then my Android app ends up using the same session id of '' for everyone
// when talking to test.firsthumb.com.  I don't know if that is an Android
// webview thing or what, but unsetting the cookie here seems to fix it.  When
// my app sends a request to test.firsthumb.com I see PHPSESSID and 2 or 3
// other cookies in the request, but when my app sends to my local server
// I don't see any cookies.  Very confused.
unset($_COOKIE['PHPSESSID']);

try
{	
	// Setup server
	$server = new Zend_Json_Server();
	$server->setClass('CLoginWs');
	$server->setClass('CLogoutWs');
	$server->setClass('CJourneyWs');
	$server->setClass('CSmsWs');
	$server->setClass('CAppWs');
	$server->setClass('CSmsGwWs');
        $server->setClass('CUserWs');
       // $server->setClass('CSearchWs');
	$fp = fopen('newfile.txt', 'a+');
fwrite($fp, print_r($_POST, TRUE));
fclose($fp);
	// Sevice interrogation?
	if ('GET' == $_SERVER['REQUEST_METHOD'])
	{
		CLogging::debug('WS - IN ... Service map request');
		// Indicate the URL endpoint, and the JSON-RPC version used:
	    $server->setTarget('/json-rpc.php')->setEnvelope(Zend_Json_Server_Smd::ENV_JSONRPC_2);
	    // Grab the SMD
	    $out = $server->getServiceMap()->toJson();
	}
	else
	{
		// Check for "q" parameter
		$query = CCommon::getRequestValue('q');

                CLogging::debug(sprintf('WS - IN [%s]', $query));
		if (isset($query) == false)
			throw new Exception('Missing query parameter',
				Zend_Json_Server_Error::ERROR_OTHER);
				
		// Process request
		$request = new Zend_Json_Server_Request;
		$request->loadJson($query);
		
		// Method invocation
		$server->setAutoEmitResponse(false);	
		$response = $server->handle($request);
		$result = $response->getResult();
		if (is_object($result) && property_exists($result, 'errorId'))
			throw new Exception($result->errorText,
				Zend_Json_Server_Error::ERROR_OTHER - $result->errorId);
		else
			$out = $response->toJson();
	}
	
	// Send output to consumer
	CLogging::debug(sprintf('WS - OUT [%s]', $out));
	CCommon::xhrSend($out, 'application/json');
}
catch (Exception $e)
{
	$server->fault($e->getMessage(), $e->getCode());
	$out = $server->getResponse()->toJson();
	CLogging::debug(sprintf('WS - ERROR [%s]', $out));
	CCommon::xhrSend($out, 'application/json');
}
?>
