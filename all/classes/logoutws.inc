<?php
require_once('classes/common.inc');
require_once('classes/login.inc');

/*
 * Logout class (Webservice)
 */

class CLogoutWs
{
	/*
	 * Logout
	 * 
	 * @param int $sessionId       Session id.
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 */
	
	public static function logout ($sessionId)
	{
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));

		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$login->logout();
		CWebSession::set('login', serialize($login));
		$out = new stdClass;
		return $out;
	}
}
?>
