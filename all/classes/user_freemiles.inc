<?php
require_once('db/user_freemilesdb.inc');
//require_once('classes/user.inc');
require_once("classes/root.inc");
/*
 * Journey
 */

class CUserFreeMiles extends CRoot
{
	// Members relating to databsae columns
	const ID = 'id';
	const USERID = 'user_id';
	const ALLOCATEDBY = 'allocated_by';
        const TRANSFERREDBY = 'transferred_by';
	const USERTYPE = 'user_type';
        const MILES = 'miles';
        const CREATEDBY = 'created_at';
	// Database column member definitions
	private static $DBM01 = array('key' => CUserFreeMiles::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CUserFreeMiles::USERID, 'type' => CCommon::INT_TYPE, 'size' => 11);
        private static $DBM03 = array('key' => CUserFreeMiles::ALLOCATEDBY, 'type' => CCommon::INT_TYPE, 'size' => 11);
        private static $DBM04 = array('key' => CUserFreeMiles::USERTYPE, 'type' => CCommon::INT_TYPE, 'size' => 2);
        private static $DBM05 = array('key' => CUserFreeMiles::MILES, 'type' => CCommon::INT_TYPE, 'size' => 5);
        private static $DBM06 = array('key' => CUserFreeMiles::CREATEDBY, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
        private static $DBM07 = array('key' => CUserFreeMiles::TRANSFERREDBY, 'type' => CCommon::INT_TYPE, 'size' => 11);
 

	/*
	 * Constructor
	 */

	function __construct ()
	{
		parent::__construct();
	}


         public function save() {
        return CUserFreeMilesDb::save($this);
    }



   
}
?>
