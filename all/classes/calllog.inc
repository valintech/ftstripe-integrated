<?php
require_once("classes/root.inc");
require_once("classes/calllogdb.inc");

/*
 * Call log class
 */

class CCallLog extends CRoot
{
	// Members relating to databsae columns
	const ID = "id";
	const MOBILE = "mobile";
	const TEXT = "text";
	const CREATED = 'created';
	const DIRECTION = 'direction'; 
	const USER_ID = 'user_id';
	const QUEUED = 'queued';
        const JOURNEY_ID = 'journey_id';
	
	// Database column member definitions
	private static $DBM01 = array('key' => CCallLog::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CCallLog::USER_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM03 = array('key' => CCallLog::MOBILE, 'type' => CCommon::STRING_TYPE, 'size' => 16);
	private static $DBM04 = array('key' => CCallLog::TEXT, 'type' => CCommon::STRING_TYPE, 'size' => 140);
	private static $DBM05 = array('key' => CCallLog::CREATED, 'type' => CCommon::DATETIME_TYPE);
	private static $DBM06 = array('key' => CCallLog::DIRECTION, 'type' => CCommon::STRING_TYPE, 'size' => 1);
	private static $DBM07 = array('key' => CCallLog::QUEUED, 'type' => CCommon::STRING_TYPE, 'size' => 1);
        private static $DBM08 = array('key' => CCallLog::JOURNEY_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	
	/*
	 * Constructor
	 */
	
	function __construct ()
	{
		parent::__construct('calllog');
	}

	/*
	 * Get record count
	 * 
	 * @return count or false
	 */
	
	public static function records ()
	{
		return CCallLogDb::records();
	}

	public static function log($user_id, $phoneNo, $direction, $msgText, $queued = 'n',$jurney_id=0)
	{
		$entry = new CCallLog();

		$entry->set('user_id', $user_id);
		$entry->set('mobile', $phoneNo);
		$entry->set('direction', $direction);
		$entry->set('text', $msgText);
		$entry->set('queued', $queued);
                $entry->set('journey_id', $jurney_id);

		return $entry->save();
	}

	/*
	 * Save log entry
	 * 
	 * @param $includeMembers    [optional] Member keys to include
	 * @return                   true or false 
	 */
	
	public function save ($includeMembers = false)
	{
		return CCallLogDb::save($this, $includeMembers);
	}
	
	/*
	 * Load log entry by id
	 * 
	 * @param $id             Entry id
	 * @return 		  true or false
	 */
	
	public function load ($id)
	{
		return CCallLogDb::load($id, $this);
	}

	/*
	 * Return array of N most recent record IDs (newest first)
	 *
	 * @param $cnt       Number of record IDs to return
	 * @return           Array of record IDs
	 */

	public static function getLatestIds($cnt)
	{
		return CCallLogDb::getLatestIds($cnt);
	}
public static function getacceptDeclineStatus($journey_id)
	{
		return CCallLogDb::getacceptDeclineStatus($journey_id);
	}
	/*
	 * Return array of unacknowledged messages for a given user
	 *
	 * @param $user_id       Number of record IDs to return
	 * @param $last_acked    Last message user ack'ed
	 * @return               Array of record IDs
	 */

	public static function getActiveIds($user_id, $last_acked)
	{
		return CCallLogDb::getActiveIds($user_id, $last_acked);
	}
        public static function getRideStartPassenger($journey_id)
	{
		return CCallLogDb::getRideStartPassenger($journey_id);
	}
        public static function getRideStartDriver($journey_id)
	{
		return CCallLogDb::getRideStartDriver($journey_id);
	}
        public static function getActiveIdsPassenger($user_id, $last_acked)
	{
		return CCallLogDb::getActiveIdsPassenger($user_id, $last_acked);
	}
}
?>
