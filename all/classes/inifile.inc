<?php
require_once('root.inc');

/*
 * Class to handle load/save of Windows style INI files
 */

class CIniFile extends CRoot
{
	/*
	 * Class constants
	 */
	
	const PUNCT_CHARS = ';#';
	const DEFAULT_SECTION_NAME = 'default';
	
	/*
	 * Constructor
	 */
	
	public function __construct ()
	{
		parent::__construct();
	}

	/*
	 * Load ini file
	 * 
	 * @param $path   Ini file path
	 * @return        true or false
	 */
	
	public function load ($path)
	{
		$lines = @file($path, FILE_TEXT | FILE_USE_INCLUDE_PATH);
		if ($lines == false)
			return false;
		$this->set('LINES', $lines);

		$section = '';
		foreach ($lines as $line)
		{
			// Look for lines containing a variable.
			$line = trim($line);

			// Ignore blanks lines or comment lines.
			if	(
				$line == ''
				|| strstr(CIniFile::PUNCT_CHARS, substr($line, 0, 1))
				)
				continue;

			if (ereg("^\[([a-zA-Z0-9_]+)\]", $line, $regs))
			{
				$section = strtoupper($regs[1]);
				$aryTemp[$strSection] = array();
			}
			elseif (($tmp = strpos($line, '=')) !== false)
			{
				$key = substr($line, 0, $tmp);
				$val = substr($line, $tmp + 1);
				$val = str_replace("\\n", "\n", $val);
				$this->setIni($key, $val, $section);
			}
		}

		return true;
	}

	/*
	 * Save INI file
	 * 
	 * @param $path   Ini file path
	 */
	
	public function save ($path)
	{
		$lines = $this->Member('LINES');
		if ( isset($lines) == false )
			$lines = array();

		$sectionsWritten = array();
		$fp = fopen($path, 'w+');
		
		foreach ($lines as $line)
		{
			// Write blank or comment lines as they are.
			$line = trim($line);
			if	(
				$line == ''
				|| strstr(CIniFile::PUNCT_CHARS, substr($line, 0, 1))
				)
			{
				fwrite($fp, $line."\r\n");
				continue;
			}

			// Section identifier?
			if (ereg( "^\[([a-zA-Z0-9_]+)\]$", $line, $regs))
			{
				fwrite($fp, $line."\r\n");
				$section = $this->getSection($regs[1]);
				foreach ($section as $key => $val)
					fwrite($fp, "$key=$val\r\n" );
				$sectionsWritten[] = strtoupper($regs[1]);
			}
			elseif (strstr($line, '=') == true)
			{
				// Ignore old value lines.
			}
		}

		// Put out any remaining sections.
		$sections = $this->_getSectionList();
		foreach ($sections as $sectionName => $section)
			if (in_array(strtoupper($sectionName), $sectionsWritten) == false)
			{
				fwrite($fp, '['.$sectionName.']'."\r\n");
				foreach ($section as $key => $val)
					fwrite($fp, "$key=$val\r\n");
			}
	}

	/*
	 * Set INI file entry
	 * 
	 * @param $key          Entry key
	 * @param $val          Entry value
	 * @param $sectionName  Section name ( defaults to 'default' )
	 */
	
	public function setIni ($key, $val, $sectionName = '')
	{
		if ($sectionName == '')
			$sectionName = CIniFile::DEFAULT_SECTION_NAME;
		$section = $this->getSection($sectionName);
		$section[$key] = $val;
		$this->_putSection($sectionName, $section);
	}

	/*
	 * Get INI file entry
	 * 
	 * @param $key          Entry key
	 * @param $sectionName  Section name ( defaults to 'default' )
	 * @return              The value
	 */
	public function getIni ($key, $sectionName = '')
	{
		if ($sectionName == '')
			$sectionName = CIniFile::DEFAULT_SECTION_NAME;
		$section = $this->getSection($sectionName);
		foreach ($section as $iniKey => $iniVal)
			if (strcasecmp($key, $iniKey) == 0)
				return $iniVal;
	}

	/*
	 * Kill INI file entry
	 * 
	 * @param $key          Entry key
	 * @param $sectionName  Section name ( defaults to 'default' )
	 * 
	 */

	public function killIni ($key, $sectionName = '')
	{
		if ($sectionName == '')
			$sectionName = CIniFile::DEFAULT_SECTION_NAME;
		$section = $this->getSection($sectionName);
		foreach ($section as $iniKey => $iniVal)
			if (strcasecmp($key, $iniKey) == 0)
				unset($section[$iniKey]);
		$this->_putSection($sectionName, $section);
	}

	/*
	 * Get section
	 * 
	 * @param $sectionName  [optional] Section name
	 * @return              Section as array or key/val pairs
	 */

	public function getSection ($sectionName = '')
	{
		if ($sectionName == '')
			$sectionName = CIniFile::DEFAULT_SECTION_NAME;
		$sectionList = $this->_getSectionList();
		return isset($sectionList[strtoupper($sectionName)]) ? $sectionList[strtoupper($sectionName)] : array();
	}

	/*
	 * Put section
	 * 
	 * @param $sectionName  Section name
	 * @return              Section as array or key/val pairs
	 */
	
	protected function _putSection ($sectionName, $section)
	{
		$sectionList = $this->_getSectionList();
		$sectionList[strtoupper($sectionName)] = $section;
		$this->set('SECTIONS', $sectionList);
	}

	/*
	 * Kill section
	 * 
	 * @param $sectionName  Section name
	 */
	
	protected function _killSection ($sectionName)
	{
		$sectionList = $this->_getSectionList();
		unset($sectionList[strtoupper($sectionName)] );
		$this->est('SECTIONS', $sectionList);
	}

	/*
	 * Get section list
	 * 
	 * @return  Section list as array or key/val pairs
	 */

	protected function _getSectionList ()
	{
		$sectionList = $this->get('SECTIONS');
		return is_array($sectionList) ? $sectionList : array();
	}
}
?>
