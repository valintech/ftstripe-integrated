<?php

require_once("classes/root.inc");
require_once("classes/websession.inc");
require_once("db/usergroupdb.inc");
require_once('classes/common.inc');
require_once('classes/region.inc');

/*
 * Group class
 */

class CUserGroup extends CRoot {
    // Standard values

    const ID = 'id';
    const GROUP_ID = 'group_id';
    const USER_ID = 'user_id';
    const CREATED_TYPE = 'created_type';
    const STATUS = 'status';
    const CREATED = 'created';

    private static $DBM01 = array('key' => CUserGroup::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM02 = array('key' => CUserGroup::GROUP_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM03 = array('key' => CUserGroup::USER_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM04 = array('key' => CUserGroup::CREATED_TYPE, 'type' => CCommon::STRING_TYPE, 'size' => 1);
    private static $DBM05 = array('key' => CUserGroup::STATUS, 'type' => CCommon::STRING_TYPE, 'size' => 1);
    private static $DBM06 = array('key' => CUserGroup::CREATED, 'type' => CCommon::DATETIME_TYPE);

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();
    }

    /*
     * Save user group joinings
     * @return   true or false 
     */

    public function save($values,$from) {
        // New group?

        return CUserGroupDb::save($values,$from);
        }

    /*
     * Load a group by id.
     * 
     * @param $id   Id
     * @return      true of false
     */

    public function load($id) {
        CUserGroupDb::load($id, $this);
    }

     /*
     * Load a group by id.
     * 
     * @param $id   Id
     * @return      true of false
     */
    public function loadExistingGroups($id) {
        return CUserGroupDb::loadExistingGroups($id, $this);
    }
    
    
    public function loadNewGroups($id) {
        return CUserGroupDb::loadNewGroups($id, $this);
    }
    
    public function loadNewGroupsDel($id,$newgroup){
     return CUserGroupDb::loadNewGroupsDel($id,$newgroup, $this);
    
    }
    
     /*
     * Delete user group
     * 
     * @param $extra_groups   extra groups in current session
      * @param $userid user id
     * @return     none
     */
    public function deleteUserGroup($extra_groups,$userid)
    {
        CUserGroupDb::deleteUserGroup($extra_groups,$userid);
    }
    
    /*
     * Load unapproved groups
     * 
     * @return      array of group ids
     */
    public function loadUnapprovedGroups($userid) 
    {
        return CUserGroupDb::loadUnapprovedGroups($userid) ;
    }

    /*
     * Establish group for a user
     * 
     * @param $userId      User id.
     * @param $groupName   Carpool name
     */

    public function establishGroup($userId, $groupName = '') {
        
    }

    public function getGroupOwnerMemeberDetails($groupId) {


        return CUserGroupDb::getGroupOwnerMemeberDetails($groupId);
    }
       public function getUserStatusInGroup($groupId, $userId) {


        return CUserGroupDb::getUserStatusInGroup($groupId, $userId);
    }
    
    

    public function getNameById($userId) {


        return CUserGroupDb::getNameById($userId);
    }

    public function loadIdByOwnerIdAndUserId($groupId, $userId) {


        return CUserGroupDb::loadIdByOwnerIdAndUserId($groupId, $userId);
    }

    public function updateStatus() {
        return CUserGroupDb::updateStatus($this);
    }

    /*
     * Mail body creation
     * 
     * @param $sender      Sender Id.
     * @param $userdetail  Requested memeber details
     *  @param $encryotedurl  encrypted url
     * $ownerId group owner and group name details
     *  @param $type  Requested mail type 
     */


    function approvemailbody($sender, $userdetail, $encryotedurl, $ownerId, $type) {
        $rplc = array();
        $rplc[1] = $sender['first_name'];
        $rplc[2] = $sender['last_name'];
        $rplc[3] = $userdetail['first_name'];
        $rplc[4] = $userdetail['last_name'];
        $rplc[5] = $ownerId['group_tag'];
        $rplc[6] = CConfig::SITE_BASE."/approve.php?approveparam=".$encryotedurl;
        if ($type == 'aprove_req') {
            $params['subject'] = "New Member wishes to join your group";
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('groupregistrationApprove.html'), $rplc);
        }
        if ($type == 'req_notification') {

            $params['subject'] = "Member Request Pending in " . $ownerId['group_tag'];

            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('requestNotification.html'), $rplc);
        }
        if ($type == 'approve_notification') {
            $params['subject'] = "Membership Approved in " . $ownerId['group_tag'];
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('approveNotification.html'), $rplc);
        }
         if ($type == 'memeber_to_admin') {
            $params['subject'] = "Ownership is changed in " . $ownerId['group_tag'];
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('approveOwnerNotification.html'), $rplc);
        }
         if ($type == 'admin_status_changed') {
            $params['subject'] = "Ownership is changed in " . $ownerId['group_tag'];
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('removeOwnerNotification.html'), $rplc);
        }
        if ($type == 'memeber_to_admin_declain') {
            $params['subject'] = "Ownership Request is declained in " . $ownerId['group_tag'];
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('declainOwnershipNotification.html'), $rplc);
        }
        
        
        
        $params['from'] = "FT@firsthumb.com";
        $params['to'] = $sender['email'];
        return CCommon::sendEMail($params);
    }
    /*
     * Removes domain matching groups of user
     */

     function removeGroupmailbody($sendermail, $GRoupNames) {
        $rplc = array();
        $rplc[1] = $GRoupNames['group_tag'];
        $params['subject'] = $GRoupNames['group_tag']." is Deleted";
        $params['body'] = CCommon::htmlReplace(CRegion::regionFile('deleted_group.html'), $rplc);
        $params['from'] = "FT@firsthumb.com";
        $params['to'] = $sendermail;
        return CCommon::sendEMail($params);
    }
  public function removeUserGroups($userid) {
         return CUserGroupDb::removeUserGroups($userid,'d') ;
    }
    function removeMembermailbody($userdetail, $GRoupNames) {
        $rplc = array();
        $rplc[1] = $userdetail['first_name'];
        $rplc[2] = $userdetail['last_name'];
        $rplc[3] = $GRoupNames['group_tag'];
            $params['subject'] = "Canceling the  Memebership from the group ".$GRoupNames['group_tag'];
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('groupMembershipRemoval.html'), $rplc);
        
        $params['from'] = "FT@firsthumb.com";
        $params['to'] = $userdetail['email'];
        return CCommon::sendEMail($params);
    }
    

    function changeMemberToAdminmailbody($userdetail, $GRoupNames,$encryotedurl) {
        
        $rplc = array();
        $rplc[1] = $userdetail['first_name'];
        $rplc[2] = $userdetail['last_name'];
        $rplc[3] = $GRoupNames['group_tag'];
        $rplc[4] =  CConfig::SITE_BASE."/ownershipappoval.php?approveownerparam=".$encryotedurl;
        $params['subject'] = "Invitation to be the Owner of  " . $GRoupNames['group_tag'];
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('MembertoAdminRequest.html'), $rplc);
        
        $params['from'] = "FT@firsthumb.com";
        $params['to'] = $userdetail['email'];
        return CCommon::sendEMail($params);
    }
    
 function searchGroupMember($str,$group_id,$user){
        
        return CUserGroupDb::getUserGroupMemebers($str,$group_id,$user);
    }   
    
  function   RemoveGroupMemebrsByGroupAdmin($searched_group_members){
     return CUserGroupDb::RemoveGroupMemebrsByGroupAdmin($searched_group_members);  
  }
    
    
     function changeOwnershipInUserGroup($groupId,$currentOwnerId,$NewRequestedOwnerId){

         return CUserGroupDb::changeOwnershipInUserGroup($groupId,$currentOwnerId,$NewRequestedOwnerId) ;
    }
    
    
    
    
    
    function getallMemebersByGroupId($groupId){
      return CUserGroupDb::getallMemebersByGroupId($groupId); 
}

    function RemoveGroupMemebrsByGroupId($groupId){
        return CUserGroupDb::RemoveGroupMemebrsByGroupId($groupId);
    }

}

?>