<?php

require_once("classes/root.inc");
require_once("classes/websession.inc");
require_once("db/groupdb.inc");

/*
 * Group class
 */

class CGroup extends CRoot {
    // Standard values

    const ID = 'id';
    const ORGANIZATION = 'organization';
    const LOCATION = 'location';
    const GROUP_TAG = 'group_tag';
    const CREATED = 'created';
    const MODIFIED = 'modified';
    const USER_ID = 'user_id';

    // Database column member definitions
    private static $DBM01 = array('key' => CGroup::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM02 = array('key' => CGroup::ORGANIZATION, 'type' => CCommon::STRING_TYPE, 'size' => 256);
    private static $DBM03 = array('key' => CGroup::LOCATION, 'type' => CCommon::STRING_TYPE, 'size' => 256);
    private static $DBM04 = array('key' => CGroup::GROUP_TAG, 'type' => CCommon::STRING_TYPE, 'size' => 500);
    private static $DBM05 = array('key' => CGroup::CREATED, 'type' => CCommon::DATETIME_TYPE);
    private static $DBM06 = array('key' => CGroup::MODIFIED, 'type' => CCommon::DATETIME_TYPE);
    private static $DBM07 = array('key' => CGroup::USER_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();
    }

    /*
     * Save group details
     * @return   true or false 
     */

    public static function createGroup($user, $newgorup) {
        return CUserDb::createGroup($user, $newgorup);
    }

     /*
     * Validate group name
     * 
     * @param $str    Group tag
     * @return        count of group tag matches
     */
    public function validateGroupname($str)
    {
        return CGroupDb::validateGroupname($str);
    }
    
    
    /*
     * Save new group
     * @return   true or false 
     */
    public function save($userid, $group_tag, $group_organization, $group_location) {
        // New group?
        $tmp = date('Y-m-d h:i:s');
        $this->set(CGroup::GROUP_TAG, $group_tag);
        $this->set(CGroup::LOCATION, $group_location);
        $this->set(CGroup::ORGANIZATION, $group_organization);
            $this->set(CGroup::CREATED, $tmp);
            $this->set(CGroup::MODIFIED, $tmp);
        $this->set(CGroup::USER_ID, $userid);
        return CGroupDb::save($this);
    }

    /*
     * Load a group by id.
     * 
     * @param $id   Id
     * @return      true of false
     */

    public function load($id) {
        CGroupDb::load($id, $this);
    }
    
    /*
     * search for matching group
     * 
     * @param $str   group_tags
      *
     * @return     group details
     */
     public function searchGroup($str) {
        return CGroupDb::loadSearch($str, $this);
    }
     public function searchGroupListUser($str,$userId) {
        return CGroupDb::loadSearchUserList($str,$userId, $this);
    }
    /*
     * Load all groups.
     * 
     */

    public function loadAll() {
        return CGroupDb::loadAll($this);
    }

    /*
     * Establish group for a user
     * 
     * @param $userId      User id.
     * @param $groupName   Carpool name
     */

    public function establishGroup($userId, $groupName = '') {
        
    }
    
   public function  getGroupNamesByOwnerId($userId){
       return CGroupDb::getGroupNamesByOwnerId($userId); 
   }
   public function getGroupNamesById($groupId){
       return CGroupDb::getGroupNamesById($groupId);
   }
   
public function loadIdByOwnerIdAndgroupId($groupId,$userId){
     return CGroupDb::loadIdByOwnerIdAndgroupId($groupId,$userId);
    
}
 public function updateStatus() {
        return CGroupDb::updateStatus($this);
    }
     public function RemoveGroupByGroupId($groupId) {
        return CGroupDb::RemoveGroupByGroupId($groupId);
    }
    
    
    
}
?>
