<?php
require_once("classes/root.inc");
require_once("classes/websession.inc");
require_once("db/carpooldb.inc");

/*
 * Carpool class
 */

class CCarpool extends CRoot
{
	// Members relating to databsae columns
	const ID = 'id';
	const USER_ID = 'user_id';
	const NAME = 'name';
	const CREATED = 'created';
	const UPDATED = 'updated';
	const PLACES = 'places';
	
	// Database column member definitions
	private static $DBM01 = array('key' => CCarpool::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CCarpool::USER_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM03 = array('key' => CCarpool::NAME, 'type' => CCommon::STRING_TYPE, 'size' => 20);
	private static $DBM04 = array('key' => CCarpool::CREATED, 'type' => CCommon::DATETIME_TYPE);
	private static $DBM05 = array('key' => CCarpool::UPDATED, 'type' => CCommon::DATETIME_TYPE);
	private static $DBM06 = array('key' => CCarpool::PLACES, 'type' => CCommon::INT_TYPE, 'size' => 2);
	
	/*
	 * Constructor
	 */
	
	function __construct ()
	{
		parent::__construct();
		$this->set(self::PLACES, 2);
	}
	
	/*
	 * Save carpool details
	 * @return   true or false 
	 */
	
	public function save ()
	{
		return CCarpoolDb::save($this);
	}
	
	/*
	 * Load a carpool by id.
	 * 
	 * @param $id   Id
	 * @return      true of false
	 */
	
	public function load ($id)
	{
		$this->clear();
		return CCarpoolDb::load($id, $this);
	}
	
	/*
	 * Load a carpool by user id.
	 * 
	 * @param $id   User Id.
	 * @return      true or false
	 */
	
	public function loadByUserId ($id)
	{
		$this->clear();
		return CCarpoolDb::loadByUserId($id, $this);
	}
	
	/*
	 * Establish group for a user
	 * 
	 * @param $userId   User id.
	 * @param $name     Carpool name
	 * @param $places   Carpool places
	 * @return          true or false
	 */
	
	public function establishCarpool ($userId, $name = 'My Carpool', $places = 2)
	{
		if ($this->loadByUserId($userId) == false)
			return false;
		if ($this->get(self::ID) == 0)
		{
			$this->set(self::PLACES, $places);
			$this->set(self::USER_ID, $userId);
		}
		$this->set(self::NAME, $name);
		if ($this->save() == false)
			return false;
		return true;
	}
}
?>
