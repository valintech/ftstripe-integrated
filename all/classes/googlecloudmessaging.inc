<?php
require_once('classes/common.inc');

class CGoogleCloudMessaging
{
	public static function alertDevice($deviceToken, $sender, $message)
	{
		//Google cloud messaging GCM-API url
		$url = 'https://android.googleapis.com/gcm/send';
		$data = array(
			'message' => $message,
			'title' => $sender,
			//'msgcnt' => 1,
			'soundname' => 'beep.wav',
			'timeToLive' => 3000,
		);
		$registration_ids = array( $deviceToken );
		$fields = array(
			'registration_ids' => $registration_ids,
			'data' => $data,
		);
		// Google Cloud Messaging GCM API Key
		define('GCM_API_KEY', CConfig::GCM_API_KEY);
		define("GOOGLE_API_KEY", "AIzaSyDmJ0FFtc8h-I_cPGCDZa9yJ4SWUpo7VAA");    
		$headers = array(
			'Authorization: key=' . GCM_API_KEY,
			'Content-Type: application/json'
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$content = curl_exec($ch);       
		CLogging::debug("CGoogleCloudMessaging::alertDevice() response ".$content);

		$response = curl_getinfo($ch);
		if (!( ($response['http_code'] === 200) || ($response['http_code'] === 202) )) {
			CLogging::debug("CGoogleCloudMessaging::alertDevice() error - ".$response['http_code']);
		} else {
		}
		curl_close($ch);

		return 0;
	}
}
