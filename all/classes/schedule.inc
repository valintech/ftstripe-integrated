<?php
require_once('db/scheduledb.inc');
require_once('classes/user.inc');

/*
 * Class handler for schedules
 */

class CSchedule extends CRoot
{
	// Class constants
	const SCHEDULE_MON = 1;
	const SCHEDULE_TUE = 2;
	const SCHEDULE_WED = 4;
	const SCHEDULE_THU = 8;
	const SCHEDULE_FRI = 16;
	const SCHEDULE_SAT = 32;
	const SCHEDULE_SUN = 64;
	const JOURNEY_TYPE_REGULAR = 'r';
	const JOURNEY_TYPE_ONEOFF = 'o';
	const REQUEST_TYPE_SEEKING = 's';
	const REQUEST_TYPE_OFFERING = 'o';
	const REQUEST_TYPE_EITHER = 'e';
	const SCHEDULE_TYPE_STANDARD = 's';
	const SCHEDULE_TYPE_CUSTOM = 'c';
	const START_ADDRESS_HOME = 'h';
	const START_ADDRESS_ALT = 'a';
	
	// Members relating to databsae columns
	const ID = 'id';
	const USER_ID = 'user_id';
	const CREATED = 'created';
	const UPDATED = 'updated';
	const DESCRIPTION = 'description';
	const SCHEDULE_DAYS = 'schedule_days';
	const SCHEDULE_TYPE = 'schedule_type';
	const JOURNEY_TYPE = 'journey_type';
	const REQUEST_TYPE = 'request_type';
	const MON_START_TIME = 'mon_start';
	const TUE_START_TIME = 'tue_start';
	const WED_START_TIME = 'wed_start';
	const THU_START_TIME = 'thu_start';
	const FRI_START_TIME = 'fri_start';
	const SAT_START_TIME = 'sat_start';
	const SUN_START_TIME = 'sun_start';
	const MON_RETURN_TIME = 'mon_return';
	const TUE_RETURN_TIME = 'tue_return';
	const WED_RETURN_TIME = 'wed_return';
	const THU_RETURN_TIME = 'thu_return';
	const FRI_RETURN_TIME = 'fri_return';
	const SAT_RETURN_TIME = 'sat_return';
	const SUN_RETURN_TIME = 'sun_return';
	const ONEOFF_DATE = 'oneoff_date';
	const ONEOFF_TIME = 'oneoff_time';
	const STANDARD_START_TIME = 'std_start';
	const STANDARD_RETURN_TIME = 'std_return';
	const START_ADDRESS = 'from_location';
	const START_LAT = 'from_lat';
	const START_LNG = 'from_lng';
	const DESTINATION_ADDRESS = 'to_location';
	const DESTINATION_LAT = 'to_lat';
	const DESTINATION_LNG = 'to_lng';
        const GROUPS_ID = 'groups_id';

	// Database column member definitions
	private static $DBM01 = array('key' => CSchedule::ID, 'type' => CCommon::INT_TYPE, size => 11, 'wsKey' => 'id');
	private static $DBM02 = array('key' => CSchedule::USER_ID, 'type' => CCommon::INT_TYPE, size => 11);
	private static $DBM03 = array('key' => CSchedule::CREATED, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
	private static $DBM04 = array('key' => CSchedule::UPDATED, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
	private static $DBM05 = array('key' => CSchedule::DESCRIPTION, 'type' => CCommon::STRING_TYPE, 'size' => 100);
	private static $DBM06 = array('key' => CSchedule::SCHEDULE_DAYS, 'type' => CCommon::INT_TYPE, 'size' => 1);
	private static $DBM07 = array('key' => CSchedule::SCHEDULE_TYPE, 'type' => CCommon::STRING_TYPE, 'size' => 1, 'wsKey' => 'scheduleType');
	private static $DBM08 = array('key' => CSchedule::MON_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM09 = array('key' => CSchedule::TUE_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM10 = array('key' => CSchedule::WED_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM11 = array('key' => CSchedule::THU_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM12 = array('key' => CSchedule::FRI_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM13 = array('key' => CSchedule::SAT_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM14 = array('key' => CSchedule::SUN_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM15 = array('key' => CSchedule::MON_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM16 = array('key' => CSchedule::TUE_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM17 = array('key' => CSchedule::WED_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM18 = array('key' => CSchedule::THU_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM19 = array('key' => CSchedule::FRI_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM20 = array('key' => CSchedule::SAT_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM21 = array('key' => CSchedule::SUN_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0);
	private static $DBM22 = array('key' => CSchedule::ONEOFF_DATE, 'type' => CCommon::DATE_TYPE, 'size' => 0,'wsKey' => 'oneOffDate');
	private static $DBM23 = array('key' => CSchedule::STANDARD_START_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0, 'wsKey' => 'stdStartTime');
	private static $DBM24 = array('key' => CSchedule::STANDARD_RETURN_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0, 'wsKey' => 'stdReturnTime');
	private static $DBM25 = array('key' => CSchedule::START_ADDRESS, 'type' => CCommon::STRING_TYPE, 'size' => 100, 'wsKey' => 'startLocation');
	private static $DBM26 = array('key' => CSchedule::START_LAT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0, 'wsKey' => 'startLat');
	private static $DBM27 = array('key' => CSchedule::START_LNG, 'type' => CCommon::FLOAT_TYPE, 'size' => 0, 'wsKey' => 'startLng');
	private static $DBM28 = array('key' => CSchedule::DESTINATION_ADDRESS, 'type' => CCommon::STRING_TYPE, 'size' => 100, 'wsKey' => 'endLocation');
	private static $DBM29 = array('key' => CSchedule::DESTINATION_LAT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0, 'wsKey' => 'endLat');
	private static $DBM30 = array('key' => CSchedule::DESTINATION_LNG, 'type' => CCommon::FLOAT_TYPE, 'size' => 0, 'wsKey' => 'endLng');
	private static $DBM31 = array('key' => CSchedule::ONEOFF_TIME, 'type' => CCommon::TIME_TYPE, 'size' => 0, 'wsKey' => 'oneOffTime');
	private static $DBM32 = array('key' => CSchedule::JOURNEY_TYPE, 'type' => CCommon::STRING_TYPE, 'size' => 1, 'wsKey' => 'journeyType');
	private static $DBM33 = array('key' => CSchedule::REQUEST_TYPE, 'type' => CCommon::STRING_TYPE, 'size' => 1, 'wsKey' => 'requestType');
        private static $DBM34 = array('key' => CSchedule::GROUPS_ID, 'type' => CCommon::STRING_TYPE, 'size' => 1, 'wsKey' => 'groupsId');
	
	/*
	 * Constructor
	 */
	
	public function __construct ()
	{
		parent::__construct('schedule');
	}
	
	/*
	 * Return schedules for given user(s)
	 * 
	 * @param $userId User id whose schedule ids to return
	 * @return       Array of schedule ids or false if
	 *               error during retrieval
	 */
	
	public static function getUserScheduleIds ($userId)
	{
		$scheduleIdList = CScheduleDb::getUserScheduleIds($userId);
		return $scheduleIdList;
	}

	/*
	 * Return schedule destinations close to a location
	 * 
	 * @param $start CLocation object for start
	 * @param $end   CLocation object for end
	 * @return       Array of matching CSchedule objects or false if
	 *               error during retrieval
	 */
	
	public static function distance ($start, $end,$grouped_id,$userId)
	{
		$scheduleList = CScheduleDb::distance($start, $end,$grouped_id,$userId);
		return $scheduleList;
	}

	/*
	 * Remove expired schedules from list
	 * 
	 * @param $scheduleList   Array of CSchedule objects
	 * @return               Array of CSchedule objects
	 */
	
	public static function removeExpired ($scheduleList)
	{
		$now = time();
		$startCount = count($scheduleList);
		foreach ($scheduleList as $k => $schedule)
			if ($schedule->get(self::JOURNEY_TYPE) == self::JOURNEY_TYPE_ONEOFF)
			{
				$ts = CCommon::tsToPhp($schedule->get(self::ONEOFF_DATE).$schedule->get(self::ONEOFF_TIME));
				if ($ts < $now)
					unset($scheduleList[$k]);			
			}
		return array_values($scheduleList);
	}
	
	/*
	 * Save the hourney
	 * 
	 * @return  true or false
	 */
	
	public function save ()
	{
		return CScheduleDB::save($this);	
	}
	
	/*
	 * Load one or more schedule objects
	 * 
	 * @param $scheduleId  Schedule id. or array of ids. to load
	 * @return            Array of CSchedule objects or false if failed
	 */
	
	public static function load ($scheduleId)
	{
		return CScheduleDB::load(is_array($scheduleId) && count($scheduleId) == 1
								? $scheduleId[0] : $scheduleId);
	}
	
	/*
	 * Delete a schedule
	 *
	 * @param $scheduleId  ID of schedule to delete
	 */

	public static function delete ($scheduleId)
	{
		CScheduleDB::delete($scheduleId);
	}

	/*
	 * Return extra schedule information like host preferences
	 * and count of places taken or available
	 * 
	 * @param $scheduleList  Array of schedules to analyse
	 * @return              Array of stdClass methods, key is schedule
	 * 						id. and object contains 'placesAvailable',
	 * 						'placeTaken', 'smoker' and 'femaleOnly'
	 */
	
	public static function scheduleInfo ($scheduleList)
	{
		$scheduleIds = array();
		foreach ($scheduleList as $schedule)
		{
			$scheduleIds[] = $schedule->get(CSchedule::ID);
		}
		
		$out = array();
		foreach ($scheduleList as $key => $schedule)
		{
			$scheduleId = $schedule->get(CSchedule::ID);
			// Female only, smoker information
			$user = new CUser;
			$user->loadWithUserId($schedule->get(CSchedule::USER_ID));
			$smoker = 0; //($user->get(CUser::SMOKER) ? 1 : 0);
			$femaleOnly = 0; //($user->get(CUser::ONLY_FEMALE) ? 1 : 0);	
			$firstName = $user->get(CUser::FIRST_NAME);

			// Store info.
			$tmp = new stdClass;
			//$tmp->placesAvailable = $available;
			//$tmp->placesTaken = $taken;
			//$tmp->smoker = $smoker;
			//$tmp->femaleOnly = $femaleOnly;
			$tmp->firstName = $firstName;
			$out[$scheduleId] = $tmp;
		}

		return $out;
	}
	
	/*
	 * Request ride for a specific schedule
	 * 
	 * @param $UserId          User id.
	 * @param $scheduleId       Schedule id.
	 * @param $requestText     [optional] Additional comments to be added
	 *                         to ride request
	 * @param $emailSubject    Subject line for request email
	 * @param $emailNoComments Text to include if no comments specified
	 * @return                 0 = success, request submitted,
	 * 			   else some eror code.
	 */
	
	public static function requestRide ($userId, $scheduleId, $requestText = '',
										$emailSubject = '', $emailNoComments = '')
	{
		// Schedule valid?
		$schedules = CSchedule::load($scheduleId);
		if ($schedules == false || count($schedules) == 0)
			return CCommon::JOURNEY_NOT_FOUND;
		// Schedule user valid?
		$toUser = new CUser;
		$tmp = $toUser->loadWithUserId($schedules[0]->get(CSchedule::USER_ID));
		if ($tmp == false || $toUser->get(CUser::ID) == 0)
			return CCommon::JOURNEY_USER_NOT_FOUND;
		$fromUser = new CUser;
		$fromUser->loadWithUserId($userId);
		// Check if request already submitted
		$scheduleRequest = new CScheduleRequest;
		$scheduleRequest->loadByScheduleAndUser($scheduleId, $fromUser->get(CUser::ID));
		$out = new stdClass;
		if ($scheduleRequest->get(CScheduleRequest::ID))
			return CCommon::JOURNEY_ALREADY_REQUESTED;
		// Requesting user cannt be same as organiser
		if ($userId == $toUser->get(CUser::ID))
			return CCommon::CANNOT_JOIN_OWN_JOURNEY;
		// Save the request
		$scheduleRequest->set(CScheduleRequest::SCHEDULE_ID, $scheduleId);
		$scheduleRequest->set(CScheduleRequest::USER_ID, $fromUser->get(CUser::ID));
		$scheduleRequest->set(CScheduleRequest::REQUEST_TEXT, $requestText);
		$scheduleRequest->set(CScheduleRequest::SENT_TO_USER_ID, $toUser->get(CUser::ID));
		$scheduleRequest->genUniqueId();
		if ($scheduleRequest->save() == false)
			return CCommon::DATABASE_ERROR;
		// Send request email
		$rplc = array();
		$rplc[1] = $fromUser->getFullName();
		$rplc[2] = $schedules[0]->get(CSchedule::START_ADDRESS);
		$rplc[3] = $schedules[0]->get(CSchedule::DESTINATION_ADDRESS);
		$rplc[4] = $fromUser->get(CUser::FACEBOOK_ID);
		$urlParams = array();
		$urlParams['scheduleId'] = $scheduleId;
		$urlParams['requestId'] = $scheduleRequest->get(CScheduleRequest::ID);
		$urlParams['requestKey'] = $scheduleRequest->get(CScheduleRequest::UNIQUE_ID);
		$rplc[5] = sprintf('%s/confirmrequest.php?v=%s', CConfig::SITE_BASE,
						   rawurlencode(base64_encode(CCommon::toJson($urlParams))));
		$rplc[6] = $fromUser->getFullName();
		$cfg = new stdClass;
		$requestText = $scheduleRequest->get(CScheduleRequest::REQUEST_TEXT);
		$rplc[7] = ($requestText != '' ? $requestText : $emailNoComments);
		$cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile('requestridemail.txt'), $rplc, false);
		$rplc[7] = ($requestText != '' ? htmlentities($requestText) : htmlentities($emailNoComments));
		$cfg->body = CCommon::htmlReplace(CRegion::regionFile('requestridemail.html'), $rplc);
		$cfg->subject = $emailSubject;
		$cfg->fromName = CConfig::INFO_EMAIL_NAME;
		$cfg->toName = $toUser->getFullName();
		$cfg->from = CConfig::INFO_EMAIL;
		$cfg->to = $toUser->get(CUser::EMAIL1);
		CCommon::sendMail($cfg);
		return 0;
	}		
}
?>
