<?php
/*
 * Instant Message log class
 */

class CImLogDb
{
	const RECORDS_SQL = "SELECT COUNT(*) imlog_count FROM imlog";
	const NEW_SQL = "INSERT INTO imlog (%s) VALUES (%s)";
	const GET_IMLOG_SQL = "SELECT %s FROM imlog WHERE %s='%s'";

	/*
	 * Constructor
	 */

	function __construct ()
	{
	}

	/*
	 * Get an entry using CallLog ID.
	 *
	 * @param $id      Entry id.
	 * @param $log     A CImLog object to load into
	 * @return         true or false
	 */

	public static function load ($id, $log)
	{
		return self::_loadCore(CImLog::ID, $id, $log);
	}

	/*
	 * Core for "load..." methods
	 *
	 * @param $fieldId   Id. of field to query
	 * @param $fieldVal  Query field value
	 * @param $log       A CImLog object to load into
	 * @return           true or false
	 */

	protected static function _loadCore ($fieldId, $fieldVal, $log)
	{
		$cols = CRoot::getDbMembers('CImLog');
		$types = array();
		foreach ($cols as $colId => $col) {
			$cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
			$types[$colId] = $col['type'];
		}
		$sql = sprintf(CImLogDb::GET_IMLOG_SQL, join(',', $cols), $fieldId, $fieldVal);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = mysql_fetch_object($res);
		if ($o) {
			foreach ($o as $colId => $val)
				$log->set($colId, CDb::makePhpValue($val, $types[$colId]));
		}
		return true;
	}

	/*
	 * Save a log entry
	 *
	 * @param $log               A CImLog object to save
	 * @param $includeMembers    [optional] Member keys to include
	 * @return                   true if no reported DB error otherwise false
	 */

	public static function save ($log, $includeMembers = false)
	{
		// Get list of columns. Include id. column if already exists
		$id = $log->get(CImLog::ID);
		$cols = CRoot::getFilteredDbMembers('CImLog', $includeMembers, array(CImLog::ID));

		// Turn data into SQL compatible strings
		$vals = array();
		foreach ($cols as $colId => $col)
			$vals[$colId] = CDb::makeSqlValue($log->get($colId), $col['type']);

		// Create SQL statement for insert or update
		if ($id == 0)
		{
			$vals[CImLog::CREATED] = 'now()';
			$sql = sprintf(CImLogDb::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
		}
		else
		{
			return false;
		}

		CDb::BeginTrans();
		$res = CDb::query($sql);
		if ($res)
		{
			// Retrieve the auto ID if "insert" performed
			if ($id == 0)
			{
				$id = CDb::getLastAutoId();
				$log->set(CImLog::ID, $id);
			}
			CDb::commitTrans();
		}
		else
			CDb::rollbackTrans();

		return $res ? true : false;
	}

	/*
	 * Get count of log records
	 *
	 * @return count
	 */

	public static function records ()
	{
		$sql = sprintf(CImLogDb::RECORDS_SQL);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = CDb::getRowObject($res);
		if ($o == false)
			return false;
		return (int)$o->imlog_count;
	}

	/*
	 * Return array of N most recent record IDs (newest first)
	 *
	 * @param $cnt       Number of record IDs to return
	 * @return           Array of record IDs
	 */

	public static function getLatestIds($cnt)
	{
		$sql = sprintf("select id from imlog order by id desc limit %d", $cnt);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$out = array();
		while (($o = CDb::getRowArray($res)))
		{
			$out[] = $o[CImLog::ID];
		}
		return $out;
	}

	/*
	 * Return array of IDs of unacknowledged messages for a given user
	 *
	 * @param $to_user_id       User ID of intended message recipient
	 * @param $last_acked       Last message acked
	 * @return                  Array of record IDs
	 */

	public static function getActiveIds($to_user_id, $last_acked)
	{
		$sql = sprintf("select id from imlog where id > '%d' and to_user_id = '%d' order by id asc", $last_acked, $to_user_id);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$out = array();
		while (($o = CDb::getRowArray($res)))
		{
			$out[] = $o[CImLog::ID];
		}
		return $out;
	}
}
?>
