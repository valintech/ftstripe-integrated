<?php
require_once('classes/common.inc');
require_once('classes/login.inc');
require_once('classes/journey.inc');
require_once('classes/urbanairship.inc');

/*
 * App class (Webservice)
 */

class CAppWs
{
	/*
	 * Add device to user account
	 * Called by the app when a user has just logged in.  Associates the device
	 * in question with the user account.
	 *
	 * @param int $sessionId       Session id.
	 * @param string $deviceToken  Token uniquely identifying the apple device (64 char hex number)
	 *
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 *
	 * Returns [success | user not found | per-user token limit reached]
	 */
	public static function addUserDevice($sessionId, $deviceToken)
	{
		// Remove any non-hex chars from deviceToken.
		$deviceToken = preg_replace("/[^A-Fa-f0-9]/", "",$deviceToken);
		CLogging::debug("addUserDevice(".$sessionId.", ".$deviceToken.")");
		/* boilerplate */
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$userId = $login->userId();
		/* end boilerplate */

		CCallLog::log($userId, $login->getUser()->getFullName(), 'R', CCommon::toJson(array('type' => 'addUserDevice', 'deviceToken' => $deviceToken)));

		// If user already has a recorded device token
		// AND it differs from this one:
		$currentDeviceToken = $user->get(CUser::DEVICE_TOKEN);
		if (
			(NULL != $currentDeviceToken) &&
			($currentDeviceToken != $deviceToken)
		)
		{
			// Then update Urbanairship group with new Device Token.
			$group = $user->get(CUser::GROUP);
			if(NULL != $group)
			{
				// Make before break...
				CUrbanAirship::associateDeviceWithTag($deviceToken, $group);
				CUrbanAirship::deleteDeviceToken($currentDeviceToken);
			}
		}

		// Save new devicetoken to current user's record in user table.
		$user->set(CUser::DEVICE_TOKEN, $deviceToken);
		if ($user->save() == false) {
			CLogging::error('Failed to save user');
			return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
		}
		return true;
	}

	/*
	 * Remove device from user account
	 * Called by the app when a user logs out or the app is exited.  Removes
	 * this device from the list associated with the user in question.
	 *
	 * @param int $sessionId       Session id.
	 * @param string $deviceToken  Token uniquely identifying the apple device
	 *
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 *
	 * Returns [success | user not found]
	 */
	public static function removeUserDevice($sessionId, $deviceToken)
	{
		CLogging::debug("removeUserDevice(".$sessionId.", ".$deviceToken.")");
		/* boilerplate */
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$userId = $login->userId();
		/* end boilerplate */

		// TODO - call removeUserDevice - requires:
		//	*removeUserDevice() method
		//	*extra table in database to hold devices
		//	*Extra field in user database to associate a device with a user

		CCallLog::log($userId, $login->getUser()->getFullName(), 'R', CCommon::toJson(array('type' => 'removeUserDevice', 'deviceToken' => $deviceToken)));
		return CCommon::makeErrorObj(CCommon::UNKNOWN_ERROR, 'Function not yet implemented');
	}

	/*
	 * Journey start request
	 *
	 * @param int $sessionId       Session id.
	 * @param string $regno        Car registration number
	 *                                 or
	 *                             message (if starts with '#'<groupname>)
	 *
	 * @param $location            Where you are at
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 */
	public static function startRide($sessionId, $regno, $location)
	{
		/*
		 * Determine whether this is a regno or message based on first char of
		 * $regno.  If '#' then it's a message, else treat as a reg number.
		 */
		CLogging::debug("startride(".$sessionId.", ".$regno.", ".$location.")");
		$regno = ltrim($regno, " \t");		//Remove leading spaces or tabs
		if (substr($regno,0,1) == '#') {
			$message = preg_replace('/^#/', '', $regno);	// remove leading '#'
			$ret = CAppWs::sendGroupMessage($sessionId, $message);
		}
		else {
			$regno = preg_replace('/ /', '', $regno); //remove any remaining spaces from reg
			$ret = CAppWs::actual_startRide($sessionId, $regno, $location);
		}
		return $ret;
	}



	/*
	 * Send group message
	 *
	 * @param int $sessionId       Session id. (only used to determine current user)
	 * @param string $message      Message string of the following format:
	 *									#<groupname> <message>
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 */
	private static function sendGroupMessage($sessionId, $message)
	{
		CLogging::debug("sendGroupMessage(".$sessionId.", ".$message.")");
		$message = ltrim($message, " \t");		//Remove leading spaces or tabs
		$idx = stripos($message,' ');
		if ($idx !== false)
		{
			$group=substr($message,0,($idx));
			$message=substr($message,$idx+1);

			//TODO - check group exists before sending?
			$ret = CUrbanAirship::broadcastToTag($group,$message);
		}
		else
		{
			//Definitely no group - report error
			$ret = CCommon::makeErrorObj(CCommon::UNKNOWN_ERROR, 'No group specified');
		}
		return $ret;
	}
	/*
	 * Journey start request
	 *
	 * @param int $sessionId       Session id.
	 * @param string $regno        Car registration number
	 * @param $location            Where you are at
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 */
	private static function actual_startRide($sessionId, $regno, $location)
	{
		CLogging::debug("actual_startRide(".$sessionId.", ".$regno.", ".$location.")");
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$userId = $login->userId();
		CCallLog::log($userId, $login->getUser()->getFullName(), 'R', CCommon::toJson(array('type' => 'startRide', 'regno' => $regno)));
		return CMsg::startRide($userId, $regno, $location);
	}

	/*
	 * Journey end request
	 *
	 * @param int $sessionId       Session id.
	 * @param string $miles        Distance travelled
	 * @param $location            Where you are at
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 */
	public static function endRide($sessionId, $miles, $location)
	{
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$userId = $login->userId();
		CCallLog::log($userId, $login->getUser()->getFullName(), 'R', CCommon::toJson(array('type' => 'endRide', 'miles' => $miles)));
		return CMsg::endRide($userId, $miles, $location);
	}

	/**
	 * getMessages - retrieve pending messages plus current balance
	 *
	 * @param int $sessionId       Session id.
	 * @return object              stdClass object containing method return or
         *                             'errorId' and 'errorText' members on error
         */

	public static function getMessages ($sessionId)
	{
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$userId = $login->userId();
		$msgIds = CCallLog::getActiveIds($userId, $user->get('last_ack_id'));
		$out = new stdClass;
		$out->credit = intval($user->get('credit'));
		$msgs = array();
		foreach ($msgIds as $i) {
			$log = new CCallLog();
			$log->load($i);
			$msgs[$i] = CCommon::fromJson($log->get('text'));
		}
		$out->msgs = $msgs;

		return $out;
	}




	/**
	 * ackMessages - ack received messages
	 *
	 * @param int $sessionId       Session id.
	 * @param int $msgId           ID of most recent message received
	 * @return object              true or
         *                             'errorId' and 'errorText' members on error
         */

	public static function ackMessages ($sessionId, $msgId)
	{
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$user->set('last_ack_id', $msgId);
		$user->save();

		return new stdClass;
	}

	/**
	 * getRecentJourneys - retrieve a list of the most recent "n" journeys,
	 * where:
	 *	 "n" <= [total no of journeys taken by user]
	 * (an empty list will be returned if no journeys have yet been taken)
	 * and
	 *   "n" <= maxNoOfJourneys
	 * and
	 *	 "n" < 200  (it will be truncated if necessary)
	 *
	 * @param int $sessionId       Session id.
	 * @param int $maxNoOfJourneys Number of journeys requested in list.
	 *
	 * @return object              stdClass object containing method return or
     *                             'errorId' and 'errorText' members on error
     */

	public static function getRecentJourneys ($sessionId, $maxNoOfJourneys)
	{
		// limit number of journeys
		if ($maxNoOfJourneys > 200) { $maxNoOfJourneys = 200; }
		CLogging::debug("getRecentJourneys(".$sessionId.", ".$maxNoOfJourneys.")");

		/* boilerplate */
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$userId = $login->userId();
		/* end boilerplate */

		// Get list of journeys
		$jlist = CJourney::getUsersJourneys($userId,false,$maxNoOfJourneys);

		// Create array of journeys, using only the parameters we're
		// interested in.
		$out = new stdClass;
		$journeys = array();
		$i=0;
		foreach ($jlist as $j) {
			$journeys[$i]->driver_first_name = $j['driver_first_name'];
			$journeys[$i]->driver_last_name = $j['driver_last_name'];
			$journeys[$i]->passenger_first_name = $j['passenger_first_name'];
			$journeys[$i]->passenger_last_name = $j['passenger_last_name'];
			$journeys[$i]->starttime = $j['starttime'];
			$journeys[$i]->endtime = $j['endtime'];
			$journeys[$i]->miles = $j['miles'];
			$i+=1;
		}
		$out->journeys = $journeys;
		return $out;
	}

	public static function status ($sessionId)
	{
		session_id($sessionId);
		CWebSession::init();
		$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$user = $login->getUser(true);
		if ($user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
			$user->save();
			$login->getUser(true);
		}
		CWebSession::set('login', serialize($login));
		$userId = $user->get('id');
		$out = new stdClass;
		$out->credit = intval($user->get('credit'));
		$journeys = CJourney::getUsersJourneys($userId, true);
		$passengers = array();
		foreach ($journeys as $j) {
			if ($j['passenger_id'] == $userId)
				$out->driver = $j['driver_first_name'];
			else
				$passengers[] = $j['passenger_first_name'];
		}
		if (count($passengers))
			$out->passengers = $passengers;
		return $out;
	}

	// GROUP MESSAGING

	/**
	 * Returns a list of all groups the user associated with the passed
	 * session ID is subscribed to.
	 */
	public static function getGroupList($sessionId)
	{
		$out = new stdClass;
		$out->groups = array("Tesco WGC","BBC Salford");
		return $out;
	}
	public static function sendImToGroup($sessionId, $group, $message)
	{
		return CUrbanAirship::broadcastToTag($group,$message);
	}
	public static function sendImToUser($sessionId, $userEmail, $message)
	{
		return CUrbanAirship::broadcastToTag("all",$message);
	}
	public static function getIMs ($sessionId)
	{
		$out = new stdClass;
		$out->msgs = array(
			1083 => array(
				"time" => 1382381569,
				"from" => "mickeymouse@bbc.co.uk" ,
            "text" => "Anyone driving to Manchester in the next half hour?"
			),
			1086 => array(
				"time" => 1382381723,
            "from" => "pluto@bbc.co.uk",
            "text" => "I'm offering a lift for 3 people from White City to Broadcasting house, leaving at 6pm"
			)
		);
		return $out;
	}
	public static function ackIMs ($sessionId, $msgId)
	{
		return true;
	}
}
