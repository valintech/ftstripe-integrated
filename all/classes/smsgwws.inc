<?php
require_once('classes/common.inc');
require_once('classes/calllog.inc');
require_once('classes/smsgw.inc');

/*
 * SMS gateway class (Webservice)
 */

class CSmsGwWs
{
	/**
	 * register
	 * 
	 * @param $name
	 * @param string $url
	 * @return object	null error on success or error plus 
	 *               	message string on failure.
	 */
	
	public static function register ($name, $url)
	{
		$gw = new CSmsGw();
		$gw->load($name);
		$gw->set('url', $url);
		$gw->set('name', $name);	// In case this is the first register
		$gw->save();
		CLogging::info("Gateway ".$name." registered with URL ".$url);

		return new stdClass;
	}
}
?>
