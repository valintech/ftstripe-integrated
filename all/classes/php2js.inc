<?php
require_once('includes/config.inc');
require_once('classes/common.inc');

/*
 * Class to convert PHP variables into Javascript
 */

class Php2Js
{
	protected $list;

	/*
	 * Constructor
	 */
	
	public function __construct()
	{
		$this->list = array();
	}
	
	/*
	 * Add a variable to the output list
	 * 
	 * @param $name    Variable name
	 * @param $value   The variable
	 */
	
	public function add ($name, $value)
	{
		if (is_numeric($value))
			$this->_haveNumeric($name, $value);
		elseif (is_array($value))
			$this->_haveArray($name, $value);
		elseif (is_bool($value))
			$this->_haveBool($name, $value);
		elseif (is_float($value))
			$this->_haveFloat($name, $value);
		elseif (is_string($value))
			$this->_haveString($name, $value);
		elseif (is_object($value))
			$this->_haveObject($name, $value);
		elseif (is_null($value))
			$this->_haveNull($name, $value);
		else
			die(sprintf('cannot add %s, its type is invalid [%s]', $name. gettype($value)));
	}
	
	/*
	 * Have a number or numeric strings
	 * 
	 * @param $name    Name to use
	 * @param $value   Value to convert
	 * @return         true or false
	 */
	
	protected function _haveNumeric ($name, $value)
	{
		$this->list[] = sprintf('var %s=%ld;', $name, $value);
		return true;
	}
	
	/*
	 * Have an array
	 * 
	 * @param $name    Name to use
	 * @param $value   Value to convert
	 * @return         true or false
	 */
	
	protected function _haveArray ($name, $value)
	{
		// Array of objects?
		reset($value);
		if (is_object(current($value)))
		{
			$this->list[] = sprintf('var %s=[];', $name);
			foreach ($value as $k => $v)
			{
				$c = get_class($v);
				if ($c == 'stdClass')
					$this->list[] = sprintf('%s.push(%s);', $name, CCommon::toJson($v));
				else
					$this->list[] = sprintf('%s.push(%s.instance(%s));', $name, $c, CCommon::toJson($v));
			}			
		}
		else
			$this->list[] = sprintf('var %s=%s;', $name, CCommon::toJson($value));
		return true;
	}
	
	/*
	 * Have a boolean
	 * 
	 * @param $name    Name to use
	 * @param $value   Value to convert
	 * @return         true or false
	 */
	
	protected function _haveBool ($name, $value)
	{
		$this->list[] = sprintf('var %s=%s;', $name, ($value ? 'true' : 'false'));
		return true;
	}
	
	/*
	 * Have a float
	 * 
	 * @param $name    Name to use
	 * @param $value   Value to convert
	 * @return         true or false
	 */
	
	protected function _haveFloat ($name, $value)
	{
		$this->list[] = sprintf('var %s=%s;', $name, $value);
		return true;
	}
	
	/*
	 * Have a string
	 * 
	 * @param $name    Name to use
	 * @param $value   Value to convert
	 * @return         true or false
	 */
	
	protected function _haveString ($name, $value)
	{
		$this->list[] = sprintf("var %s='%s';", $name, str_replace("'", "\\'", $value));
		return true;
	}
	
	/*
	 * Have an object
	 * 
	 * @param $name    Name to use
	 * @param $value   Value to convert
	 * @return         true or false
	 */
	
	protected function _haveObject ($name, $value)
	{
		$this->list[] = sprintf("var %s=%s;", $name, CCommon::toJson($value));
		return true;
	}
	
	/*
	 * Have null
	 * 
	 * @param $name    Name to use
	 * @param $value   Value to convert
	 * @return         true or false
	 */
	
	protected function _haveNull ($name, $value)
	{
		$this->list[] = sprintf("var %s=null;", $name);
		return true;
	}
	
	/*
	 * Generate Javascript from accumlated variables
	 * 
	 * @param $withWrapper   true to include <script> wrapper
	 * @return               String containing Javascript
	 */
	
	public function generateJs ($withWrapper = false)
	{
		$out = array();
		if ($withWrapper)
			$out[] = sprintf('<script type="text/javascript">');
		$sep = (CConfig::DEBUG ? "\n" : '');
		$out[] = join($sep, $this->list);
		if ($withWrapper)
			$out[] = sprintf('</script>');
		return join($sep, $out);
	}
}
