<?php
require_once('thirdparty/yui-loader/phploader/loader.php');
require_once('includes/config.inc');

class CYahoo
{
	/*
	 * Return <script> statement
	 * 
	 * @return  An HTML <script> statement containing link to Google API script
	 */
	
	public static function scriptHtml ($modules)
	{
		if (in_array('json', $modules) == false)
			$modules[] = 'json';
		if (in_array('dom', $modules) == false)
			$modules[] = 'dom';
		if (CConfig::DEBUG && in_array('logger', $modules) == false)
			$modules[] = 'logger';

		$loader = new YAHOO_util_Loader('2.8.1');
		$loader->base = 'thirdparty/yui-2.8.2r1/build/';
		$loader->combine = false;
		$loader->allowRollups = false;
		$loader->loadOptional = true;
		if (CConfig::DEBUG)
			$loader->filter = YUI_DEBUG;
		call_user_func_array(array($loader, 'load'), $modules);
		$out = array($loader->tags());
			
		$extraLinks = array();
		$extraLinks[] = 'js/yahoo.js';
		$extraLinks[] = 'js/dom.js';
		if (in_array('autocomplete', $modules))
			$extraLinks[] = 'js/autocomplete.js';
		if (in_array('connection', $modules))
			$extraLinks[] = 'js/connection.js';
		if (in_array('calendar', $modules))
		{
			$extraLinks[] = 'js/calendar.js';
			$extraLinks[] = 'js/time.js';
		}
		if (in_array('container', $modules))
			$extraLinks[] = 'js/container.js';
		if (in_array('button', $modules))
			$extraLinks[] = 'js/button.js';
		foreach ($extraLinks as $extraLink)
			$out[] = sprintf('<script type="text/javascript" src="%s"></script>', $extraLink);
			
		return join('', $out);
	}
	
	
	/*
	 * Return <script> statement
	 * 
	 * @return  An HTML <script> statement containing link to Google API script
	 */
	
	public static function _scriptHtml ()
	{
		$out = array();
		$out[] = '<script src="thirdparty/yui-2.8.2r1/build/yuiloader/yuiloader-min.js" type="text/javascript"></script>';
		$out[] = '<script type="text/javascript" src="js/yahoo.js"></script>';
		// Make sure JSON is always loaded outside of loader
		$out[] = '<script src="thirdparty/yui-2.8.2r1/build/yahoo/yahoo-min.js"></script>';
 		$out[] = '<script src="thirdparty/yui-2.8.2r1/build/json/json-min.js"></script>';
		return join('', $out);
	}
	
	/*
	 * Create markup for a YUI autocomplete widget
	 * 
	 * @param $id       The base id. to use.
	 * @param $value    Initial value for the edit box
	 * @param $values   Array of values to add to the list
	 * @param $width    Width of control
	 * @param $zIndex   [optional] CSS z-Index layer
	 * @return          HTML
	 */
	
	public static function autocompleteHtml ($id, $value, $values, $width, $zIndex = 9000)
	{
		$out = array();
		$style = ($zIndex != '' ? sprintf('style="vertical-align:middle;z-index:%ld"', $zIndex) : '');
		$out[] = sprintf('<div id="YUI_AC_%s" %s>', $id, $style );
    	$out[] = sprintf('<input id="%s" style="width:%s" name="%s" type="text" value="%s">', $id, $width, $id, $value);
		$out[] = sprintf('<span id="%s_btn"></span>', $id);
    	$out[] = sprintf('<div style="width:%s" id="%s_list"></div>', $width, $id);
		$out[] = '</div>';
		$out[] = sprintf('<input type="hidden" id="%s_values" value="%s"/>', $id, rawurlencode(CCommon::toJson($values)));
    	return join('', $out);
	}
	
	/*
	 * Create HTML markkup for a YUI menu button
	 * 
	 * @param $id        Id for this button
	 * @param $options   Array of stdClass objects defining the
	 * 					 menu entries {text:xxx, value:xxx}
	 * @param $default   [optional] Index into "$options" of default
	 *                   selection for menu.
	 * @return           HTML
	 */
	
	public static function menuButtonHtml ($id, $options, $default = 0)
	{
		$out = array();
		$out[] = sprintf('<input type="button" id="%s" value="%s"/>', $id,
			(count($options) && isset($options[$default]) ? $options[$default]->text : ''));
		$out[] = sprintf('<select id="%sMenu">', $id);
		foreach ($options as $option)
		{
			$text = $option->text;
			$value = $option->value;
			$out[] = sprintf('<option value="%s">%s</option>', ($value != '' ? $value : $text), $text);
		}
		$out[] = '</select>';
		$out[] = sprintf('<input type="hidden" name="%s" id="%sVal"/>', $id, $id);
		return join('', $out);
	}
}
?>
