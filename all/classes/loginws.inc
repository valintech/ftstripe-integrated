<?php
require_once('classes/common.inc');
require_once('classes/login.inc');

/*
 * Login class (Webservice)
 */

class CLoginWs
{
	/**
	 * Login to system
	 * 
	 * @param string $userId  User id. (email)
	 * @param string $pwd     Password
	 * @return object         stdClass object containing method return or
	 *                        'errorId' and 'errorText' members on error
	 *                        'errorId' and 'errorText' members on error
	 */
	
	public static function login ($userId, $pwd)
	{
		CWebSession::init();
		$login = new CLogin;
		if ($login->login($userId, $pwd, true))
		{
			CWebSession::set('login', serialize($login));
			$out = new stdClass;
			$out->sessionId = session_id();
			$out->id = $login->userId();
		}
		elseif ($login->lastError() == CLogin::INVALID_LOGIN_ERROR)
			$out = CCommon::makeErrorObj(CLogin::INVALID_LOGIN_ERROR, 'Invalid login id. or password');
		elseif ($login->lastError() == CLogin::NOT_VERIFIED_ERROR)
			$out = CCommon::makeErrorObj(CLogin::NOT_VERIFIED_ERROR, 'Account not verified');
		else
			$out = CCommon::makeErrorObj(CLogin::UNKNOWN_ERROR, 'Unknown login problem');
		return $out;
	}
}
