<?php

/*
 * User class
 */

class CUserDb {

    const USER_EXISTS_SQL = "SELECT COUNT(*) user_count FROM user WHERE login='%s'";
    const NEW_SQL = "INSERT INTO user (%s) VALUES (%s)";
    const UPDATE_SQL = "UPDATE user SET %s WHERE id=%s";
    const GET_USER_SQL = "SELECT %s FROM user  WHERE %s='%s'";
    const GET_USER_LIKESQL = "SELECT %s FROM user WHERE %s like '%s'";
    const GROUP_MATCHES = "SELECT %s from user inner join groups on groups.user_id=user.id where user.email like '%s' limit 0,5";
    const USER_GROUPS = "SELECT g.group_tag, g.id, ug.created_type, ug.status FROM user AS u LEFT JOIN `user_group` AS ug ON u.id = ug.user_id
LEFT JOIN groups AS g ON g.id = ug.group_id WHERE u.id=%d";
    const USER_GROUPSLIST = "SELECT g.group_tag, g.id FROM groups AS g  WHERE g.id in (%s)";

    /*
     * Constructor
     */

    function __construct() {
        
    }

    function searchUserMember($str,$user){
        
        
     $sql = "SELECT u.id,u.first_name,u.last_name FROM user u  where  CONCAT(first_name, ' ', last_name) LIKE '%$str%' limit 0,5";
       
       
        $res = CDb::query($sql);
        if ($res == false)
            return false;
      
        while (($o = CDb::getRowArray($res))) {
            $o['id'] = CDb::makePhpValue($o['id'], $o['type']);
            $o['name'] = CDb::makePhpValue($o['first_name'], $o['type']).' '.CDb::makePhpValue($o['last_name'], $o['type']);
            unset($o['first_name']);
            unset($o['last_name']);
            $out[] = $o;
        }
        return $out;
    } 
    /*
     * Get a user using Facebook id.
     *
     * @param $facebookId   Facebook id.
     * @param $user         A CUser object to load into
     * @return              true or false
     */

    public static function loadWithFbId($facebookId, $user) {
        return self::_loadCore(CUser::FACEBOOK_ID, $facebookId, $user);
    }

    /*
     * Get a user using login id.
     *
     * @param $login   Login id.
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    public static function load($loginId, $user) {
        return self::_loadCore(CUser::EMAIL, $loginId, $user);
    }

    public static function loadWithGroupUserIdList($user_id, $o) {
        $sql = "SELECT id as id,group_tag as name FROM groups where user_id=$user_id";
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        if ($res == false)
            return false;
        $out = array();

        $i = 0;
        while (($olist = CDb::getRowArray($res))) {
            return true;
        }
    }
        public static function registered_details_rows($all,$email,$name, $o) {
         $where=' where verified=1 ';
        if($all=='All')
        {
           $where .='';
        }
        if($email!='')
        {
            $where .=' and email="'.$email.'"';
        }
         if($name!='')
        {
            $where .=' and first_name="'.$name.'"';
        }
        $sql = "SELECT id FROM user $where";
        $res = CDb::query($sql);
	return mysql_num_rows($res);
         }
     
    
          public static function registered_details($sort_range,$limit_range,$all,$email,$name, $o) {
         $where=' where verified=1 ';
        if($all=='All')
        {
           $where .='';
        }
        if($email!='')
        {
            $where .=' and email="'.$email.'"';
        }
         if($name!='')
        {
            $where .=' and first_name="'.$name.'"';
        }
      $sql = "SELECT id,first_name,last_name,email,mobile FROM user $where order by $sort_range limit $limit_range";
        $res = CDb::query($sql);
                $res = CDb::query($sql);

        if ($res == false)
            return false;
    $out = array();
        while (($o = CDb::getRowArray($res))) {

            $out[] = $o;
        }

        return $out;
     
     
    }
  public static function loadUpdateUserLocationList($lat,$lng,$accuracy,$userId, $o) {
        $sql = "update user set last_lat='$lat',last_lng='$lng',last_acc='$accuracy' where id=$userId";
        $res = CDb::query($sql);
       
        if ($res == false)
            return false;
   
    }
    public static function loadWithGroupUserId($user_id, $o) {
        $sql = "SELECT id as id,group_tag as name FROM groups where user_id=$user_id";
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        if ($res == false)
            return false;
        $out = array();

        $i = 0;
        while (($olist = CDb::getRowArray($res))) {
            $id = $olist['id'];

            $out[$i]['name'] = $olist['name'];
            $out[$i]['date'] = $first_date;
            $out[$i]['end_date'] = $last_date;
            $sql_total = "SELECT  (( 430 * sum( miles ) ) /1000) as total_miles FROM journey WHERE DATE(journey.endtime) BETWEEN '$first_date' AND '$last_date' and journey.passenger_id in(SELECT ug.user_id FROM user_group as ug where ug.group_id=$id)";
            $res_total = CDb::query($sql_total);
            if ($res_total == false)
                return false;
            while (($ol = CDb::getRowArray($res_total))) {
                $out[$i]['total_miles'] = $ol['total_miles'];
            }
            $i++;
        }
        return $out;
    }

    public static function loadWithGroupUserIdPdf($user_id, $o) {
        $sql = "SELECT id as id,group_tag as name FROM groups where user_id=$user_id";
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('Dec 31'));
        if ($res == false)
            return false;
        $out = array();

        $i = 0;
        while (($olist = CDb::getRowArray($res))) {
            $id = $olist['id'];

            $out[$i]['name'] = $olist['name'];
            $out[$i]['date'] = $first_date;
            $out[$i]['end_date'] = $last_date;
            $sql_total = "SELECT count(distinct(journey.passenger_id)) as active_user_counts,MONTH( journey.endtime ) as months_name,(( 430 * sum( miles ) ) /1000) as total_miles FROM journey WHERE DATE(journey.endtime) BETWEEN '$first_date' AND '$last_date' and journey.passenger_id in(SELECT ug.user_id FROM user_group as ug where ug.group_id=$id and ug.status='y')  GROUP BY MONTH( journey.endtime )";
            $res_total = CDb::query($sql_total);
            if ($res_total == false)
                return false;
            while (($ol = CDb::getRowArray($res_total))) {
                $out[$i]['total_miles'][] = $ol['total_miles'];
                $out[$i]['months'][] = $ol['months_name'];
                $out[$i]['active_user_counts'][] = $ol['active_user_counts'];
            }
            $i++;
        }
        return $out;
    }

    /*
     * Get a user using user id.
     *
     * @param $userId  User id.
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    public static function loadWithUserId($userId, $user) {
        return self::_loadCore(CUser::ID, $userId, $user);
    }

    /*
     * Get a user using car registration number.
     *
     * @param $carReg  Car registration
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    public static function loadWithCarReg($carReg, $user) {
        return self::_loadCore(CUser::CARREG, $carReg, $user);
    }

    /*
     * Get a user using phone number.
     *
     * @param $phoneNo Phone number
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    //smsgw sends +44
    public static function loadWithPhoneNo($phoneNo, $user) {
        // deal with smsgw sending leading '+44' rather than '0', and v-a-v
        if (substr($phoneNo, 0, 1) == '0')
            $phone2 = preg_replace('/^0/', '+44', $phoneNo);
        else if (substr($phoneNo, 0, 3) == '+44')
            $phone2 = preg_replace('/^\+44/', '0', $phoneNo);
        else
            $phone2 = false;

        // also match any 44 numbers in the database
        if (substr($phoneNo, 0, 3) == '+44')
            $phone3 = preg_replace('/^\+44/', '44', $phoneNo);
        else
            $phone3 = false;

        $cols = CRoot::getDbMembers('CUser');
        foreach ($cols as $colId => $col)
            $cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
        $sql = sprintf(CUserDb::GET_USER_SQL, join(',', $cols), CUser::MOBILE, $phoneNo);
        if ($phone2)
            $sql .= " OR mobile='" . $phone2 . "'";
        if ($phone3)
            $sql .= " OR mobile='" . $phone3 . "'";
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = mysql_fetch_object($res);
        if ($o)
            foreach ($o as $colId => $val)
                $user->set($colId, CDb::makePhpValue($val, $cols[$colId]['type']));
        return true;
    }

    /*
     * Load user by given criteria
     *
     * @param $fieldId   Id. of field to query
     * @param $fieldVal  Query field value
     * @param $user      A CUser object to load into
     * @return          true or false
     */

    public static function loadBy($fieldId, $fieldVal, $user) {
        return self::_loadCore($fieldId, $fieldVal, $user);
    }

    /*
     * Core for "load..." methods
     *
     * @param $fieldId   Id. of field to query
     * @param $fieldVal  Query field value
     * @param $user      A CUser object to load into
     * @return           true or false
     */

    protected static function _loadCore($fieldId, $fieldVal, $user) {
        $cols = CRoot::getDbMembers('CUser');
        $selectVals = array();
        foreach ($cols as $colId => $col)
            $selectVals[$colId] = CDb::makeSelectValue($colId, $col['type']);
        $sql = sprintf(CUserDb::GET_USER_SQL, join(',', $selectVals), $fieldId, $fieldVal);
        $res = CDb::query($sql);
        if ($res === false)
            return false;
        $o = mysql_fetch_object($res);
        if ($o)
            foreach ($o as $colId => $val)
                $user->set($colId, CDb::makePhpValue($val, $cols[$colId]['type']));

        return true;
    }

    /*
     * Save the user
     *
     * @param $user              A CUser object to save
     * @param $includeMembers    [optional] Member keys to include
     * @return                   true if no reported DB error otherwise false
     */

    public static function save($user, $includeMembers = false) {
        // Get list of columns. Include id. column if already exists
        //print_r($user);
        //echo 'user';

        $id = $user->get(CUser::ID);
        $cols = CRoot::getFilteredDbMembers('CUser', $includeMembers, array(CUser::ID));

        //   print_r($cols);
        //  echo 'cols';
        // Turn data into SQL compatible strings
        $vals = array();
        foreach ($cols as $colId => $col)
            $vals[$colId] = CDb::makeSqlValue($user->get($colId), $col['type']);

        // Create SQL statement for insert or update
        $vals[CUser::UPDATED] = 'now()';
//print_r($vals);
//echo 'vals';
        $status = true;
        if ($id == 0) {
            $vals[CUser::CREATED] = 'now()';
            $status = true;
            $sql = sprintf(CUserDb::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));

            //  print_r($sql);die("llll");
        } else {
            unset($cols[CUser::CREATED]);
            unset($vals[CUser::CREATED]);
            $tmp = array();
            foreach ($cols as $colId => $col)
                $tmp[] = sprintf("%s=%s", $colId, $vals[$colId]);
            $sql = sprintf(CUserDb::UPDATE_SQL, join(",", $tmp), $id);
            $status = false;
        }

//print_r($sql);
        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
            if ($id == 0) {
                $id = CDb::getLastAutoId();

                CWebSession::set('regid',$id);

                $user->set(CUser::ID, $id);
                $sql_default_miles="SELECT count(*) as count,free_miles FROM default_miles where active=1";
                $res_default_miles = CDb::query($sql_default_miles);
                $out_default_miles = mysql_fetch_object($res_default_miles);
              
              
                if($out_default_miles->count>0)
                {
                    $free_miles=$out_default_miles->free_miles;
                     $sql_free_miles="INSERT INTO user_freemiles (user_id,allocated_by,user_type,miles,created_at,transferred_by) VALUES ($id,1,1,$free_miles,now(),NULL)";
                    CDb::query($sql_free_miles);
                }
            }
            CDb::commitTrans();

            CUserDb::moveFiles($id, $status, $user, $cols);
        } else {

            CDb::rollbackTrans();
        }

        return $res ? true : false;
    }

    /*
     * Move uploaded files to user directory
     *
     * @param $id - User id
     * @return none
     */

    private static function moveFiles($id, $status, $user) {


        $session_id = session_id();
        if (!$status)
            $session_id = $id;

        $dir = 'upload/' . $id;
        rename('upload/' . $session_id, $dir);

        $tmp = array();

        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    $filename = str_replace(session_id(), $id, $file);
                    if (!file_exists('upload/' . $id . '/' . $filename))
                        rename('upload/' . $id . '/' . $file, 'upload/' . $id . '/' . $filename);

                    $filenameStart = explode(".", $file);

                    // if (strcmp($filenameStart[0], $session_id . "_Photo") == 0 && ($user->get(CUser::PHOTO) != 0 || $user->get(CUser::PHOTO) != NULL ))
                    if (strpos($file, $session_id . "_Photo.") !== false && ($user->get(CUser::PHOTO) != 0 || $user->get(CUser::PHOTO) != NULL ))
                        $tmp[] = sprintf("%s='%s'", 'photo', $filename);
                    //   if (strcmp($filenameStart[0], $session_id . "_PhotoId") == 0 && ($user->get(CUser::PHOTO_ID) != 0 || $user->get(CUser::PHOTO_ID) != NULL ))
                    if (strpos($file, $session_id . "_PhotoId") !== false && ($user->get(CUser::PHOTO_ID) != 0 || $user->get(CUser::PHOTO_ID) != NULL ))
                        $tmp[] = sprintf("%s='%s'", 'photo_id', $filename);
                    //if (strcmp($filenameStart[0], $session_id . "_DrivingLicense_Active") == 0) {
                    if (strpos($file, $session_id . "_DrivingLicense_Active") !== false) {
                        if (!$status) {
                            $image = $user->get(CUser::DRIVING_LICENSE_IMAGE);
                            $license = json_decode($image, true);

                            $old_active_photo = $license['active'];

                            $license['active'] = $filename;
                            $license[] = $old_active_photo;

                            $new_photo_json = json_encode($license);
                        } else
                            $new_photo_json = json_encode(array("active" => $filename));

                        if ($user->get(CUser::DRIVER) == 1) {
                            $tmp[] = sprintf("%s='%s'", 'driving_license_image', $new_photo_json);
                        }
                    }
                }
                closedir($dh);
            }
        }
        $sql = sprintf(CUserDb::UPDATE_SQL, join(",", $tmp), $id);

        CDb::BeginTrans();

        $res = CDb::query($sql);
        if ($res) {

            CDb::commitTrans();
        } else {

            CDb::rollbackTrans();
        }
    }

    /*
     * Check if a user login id. already exists
     *
     * @param $login - User login name
     * @return true or false
     */

    public static function userExists($login) {
        $sql = sprintf(CUserDb::USER_EXISTS_SQL, trim($login));
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = CDb::getRowObject($res);
        if ($o == false)
            return false;
        return (int) $o->user_count ? true : false;
    }

    /*
     * Return array all user Ids
     *
     * @return           Array of record IDs
     */

    public static function getAllIds() {
        $sql = sprintf("select id from user order by id");
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUser::ID];
        }
        return $out;
    }

    public static function nUsersScheme($user_id, $o) {
        $sql = "SELECT id as id,group_tag as name FROM groups where user_id=$user_id";
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        if ($res == false)
            return false;
        $out = array();

        $i = 0;
        while (($olist = CDb::getRowArray($res))) {
            $id = $olist['id'];
            $sql_total = "SELECT  count(*) as no_journey_taken  FROM journey WHERE DATE(journey.endtime) BETWEEN '$first_date' AND '$last_date' and journey.passenger_id in(SELECT ug.user_id FROM user_group as ug where ug.group_id=$id and ug.status='y')";
            $res_total = CDb::query($sql_total);
            if ($res_total == false)
                return false;
            while (($ol = CDb::getRowArray($res_total))) {
                $out[$i]['no_journey_taken'] = $ol['no_journey_taken'];
            }
            $i++;
        }
        return $out;
    }

    public static function numberUsersScheme($user_id, $o) {
        $sql = "SELECT id as id,group_tag as name FROM groups where user_id=$user_id";
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        if ($res == false)
            return false;
        $out = array();

        $i = 0;
        while (($olist = CDb::getRowArray($res))) {
            $id = $olist['id'];
            $sql_total = "SELECT   count( DISTINCT (journey.passenger_id) ) AS no_user_scheme  FROM journey WHERE DATE(journey.endtime) BETWEEN '$first_date' AND '$last_date' and journey.passenger_id in(SELECT ug.user_id FROM user_group as ug where ug.group_id=$id  and ug.status='y')";
            $res_total = CDb::query($sql_total);
            if ($res_total == false)
                return false;
            while (($ol = CDb::getRowArray($res_total))) {
                $out[$i]['no_user_scheme'] = $ol['no_user_scheme'];
            }
            $i++;
        }
        return $out;
    }

    public static function numberMilesShared($user_id, $o) {
        $sql = "SELECT id as id,group_tag as name FROM groups where user_id=$user_id";
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        if ($res == false)
            return false;
        $out = array();

        $i = 0;
        while (($olist = CDb::getRowArray($res))) {
            $id = $olist['id'];
            $sql_total = "SELECT   sum( journey.miles ) AS no_miles_shared  FROM journey WHERE DATE(journey.endtime) BETWEEN '$first_date' AND '$last_date' and journey.passenger_id in(SELECT ug.user_id FROM user_group as ug where ug.group_id=$id and ug.status='y')";
            $res_total = CDb::query($sql_total);
            if ($res_total == false)
                return false;
            while (($ol = CDb::getRowArray($res_total))) {
                $out[$i]['no_miles_shared'] = $ol['no_miles_shared'];
            }
            $i++;
        }
        return $out;
    }

    public static function kg_C0_saved($user_id, $o) {
        $sql = "SELECT id as id,group_tag as name FROM groups where user_id=$user_id";
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        if ($res == false)
            return false;
        $out = array();

        $i = 0;
        while (($olist = CDb::getRowArray($res))) {
            $id = $olist['id'];
            $sql_total = "SELECT  (( 430 * sum( miles ) ) /1000) as total_miles  FROM journey WHERE DATE(journey.endtime) BETWEEN '$first_date' AND '$last_date' and journey.passenger_id in(SELECT ug.user_id FROM user_group as ug where ug.group_id=$id and ug.status='y')";
            $res_total = CDb::query($sql_total);
            if ($res_total == false)
                return false;
            while (($ol = CDb::getRowArray($res_total))) {
                $out[$i]['total_miles'] = $ol['total_miles'];
            }
            $i++;
        }
        return $out;
    }

    /*
     * Return array of all user group details
     *
     * @return           Array of record IDs
     */

    public static function getExistingGroupsOfUserListingSearchDb($userId) {
        $sql = sprintf(CUserDb::USER_GROUPSLIST, $userId);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            //$out['type'][] = $o[CUserGroup::CREATED_TYPE];
            //$out['status'][] = $o[CUserGroup::STATUS];
            $out['group_id'][] = $o[CGroup::ID];
            $out['group_tag'][] = $o[CGroup::GROUP_TAG];
        }
        return $out;
    }

    public static function getExistingGroupsOfUser($userId) {
        $sql = sprintf(CUserDb::USER_GROUPS, $userId);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out['type'][] = $o[CUserGroup::CREATED_TYPE];
            $out['status'][] = $o[CUserGroup::STATUS];
            $out['group_id'][] = $o[CGroup::ID];
            $out['group_tag'][] = $o[CGroup::GROUP_TAG];
        }
        return $out;
    }

    /*
     * Return array of all groups
     * Ignore "null" which is the default value when
     * someone isn't in a group.
     *
     * @return           Array of groups.
     */

    public static function getAllGroups() {
        $sql = sprintf("SELECT DISTINCT(%s) FROM user", CUser::GROUP);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $group = $o[CUser::GROUP];
            if (null !== $group) {
                $out[] = $group;
            }
        }
        return $out;
    }

    public static function getAllGroupsOfThisUser($userId) {
        //$sql = "SELECT * FROM user_group WHERE `user_id`=$userId AND created_type IN ('n','s','d') ORDER BY FIELD( created_type, 'n','s','d') LIMIT 6";
        $sql = "SELECT groups.group_tag,groups.id  FROM groups  JOIN user_group ON groups.id=user_group.group_id WHERE  user_group.status='y' AND  user_group.user_id =$userId AND user_group.created_type IN ('n','s','d') ORDER BY FIELD( user_group.created_type, 'n','s','d') LIMIT 6";
        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $group = $o;
            if (null !== $group) {
                $out[] = $group;
            }
        }
        sort($out);
        return $out;
    }

    /*
     * Return array of all groups of which $user
     * is a member.
     * Ignore "null" which is the default value when
     * someone isn't in a group.
     * @param 			 User ID
     * @return           Array of groups.
     */

    public static function getAllGroupsOfUser($userId) {
        $sql = sprintf("SELECT %s FROM user WHERE id=%s", CUser::GROUP, $userId);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {

            $group = $o;
            if (null !== $group) {
                $out[] = $group;
            }
        }
        return $out;
    }

    /*
     * Return array of all user emails in group.
     *
     * @return           Array of email addresses.
     */

    public static function getAllEmailsInGroup($group) {
        $sql = sprintf(CUserDb::GET_USER_SQL, CUser::EMAIL, CUser::GROUP, trim($group));
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUser::EMAIL];
        }
        return $out;
    }

    public static function getRecentMessageInGroup($userId, $group) {
        $sql = "SELECT user.email,imlog.text,imlog.created,imlog.from_user_id  FROM imlog  JOIN user ON user.id=imlog.from_user_id WHERE imlog.to_user_id=$userId AND imlog.group_id = $group ORDER BY imlog.created DESC LIMIT 0,6";
        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {

            if (null !== $o) {
                $out[] = $o;
            }
        }
        return $out;
    }
 public static function loadWithCarRegCheck($carReg) {
        $sql = "SELECT id FROM user where carreg='$carReg'";
        $res = CDb::query($sql);

        if ($res == false)
            return 0;
     $out = array();
        $out = CDb::getRowArray($res);
        if (is_array($out)) {
            return $out['id'];
        }
         
    }
    public static function getAllEmailsInUserGroup($group) {
        $sql = "SELECT user.email,user.id FROM user  JOIN user_group ON user.id=user_group.user_id WHERE  user_group.status='y' AND  user_group.group_id =$group";
        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $group = $o;
            if (null !== $group) {
                $out[] = $group;
            }
        }
        return $out;
    }

    public static function getUserDetails($userId, $width, $height) {
        //$sql = sprintf(CUserDb::GET_USER_SQL, CUser::FIRST_NAME,CUser::LAST_NAME, CUser::PHOTO, CUser::ID, trim($userId));
        //$sql = $sql = "SELECT first_name,last_name,photo FROM `user` WHERE `id` = $userId ";
        $sql = "SELECT id,first_name,last_name,photo FROM `user` WHERE `id` = $userId ";


        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out_details = null;
        $out = array();
        $out = CDb::getRowArray($res);
        if (is_array($out)) {
            $out_details['first_name'] = $out['first_name'];
            $out_details['last_name'] = $out['last_name'];
            $out_details['photo'] = '';
            $out_details['width'] = $width;
            $out_details['height'] = $height;
            $file = '';
            $filett = '';
            $dir = '../upload/' . $out['id'] . "/";
            if (isset($out['photo'])) {
                if (is_dir($dir)) {
                    if ($dh = opendir($dir)) {
                        while (($file = readdir($dh)) !== false) {



                            $pos = strpos($file, $out['id'] . "_" . $width);
                            if ($pos !== false)
                                $filett = $file;
                        }
                        closedir($dh);
                    }
                }
            }
            $out_details['photo'] = 'upload/' . $out['id'] . '/' . $filett;
        }
        return $out_details;
    }

    public static function getMatchingGroups($domain) {

        $sql = sprintf(CUserDb::GROUP_MATCHES, "email,groups.id,group_tag", "%" . $domain . "%");

        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out['mail'][] = $o[CUser::EMAIL];
            $out['group_id'][] = $o['id'];
            $out['group_tag'][] = $o['group_tag'];
        }

        return $out;
        //SELECT user.email,groups.id,groups.group_tag from user inner join groups on groups.user_id=user.id where user.email like "%gmail.com%"
    }

    public static function getMatchingEmails($domain) {

        $sql = sprintf(CUserDb::GET_USER_LIKESQL, CUser::EMAIL . "," . CUser::FIRST_NAME, CUser::EMAIL, "%" . $domain . "%");
        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out['mail'][] = $o[CUser::EMAIL];
            $out['name'][] = $o[CUser::FIRST_NAME];
        }
        return $out;
        //SELECT user.email,groups.id,groups.group_tag from user inner join groups on groups.user_id=user.id where user.email like "%gmail.com%"
    }

}

?>
