<?php

require_once('db/journeydb.inc');
require_once('classes/user.inc');

/*
 * Journey
 */

class CJourney extends CRoot {

    // Members relating to databsae columns
    const ID = 'id';
    const CAR_ID = 'car_id';
    const DRIVER_ID = 'driver_id';
    const PASSENGER_ID = 'passenger_id';
    const FROM_LAT = 'from_lat';
    const FROM_LNG = 'from_lng';
    const FROM_ACC = 'from_acc';
    const TO_LAT = 'to_lat';
    const TO_LNG = 'to_lng';
    const TO_ACC = 'to_acc';
    const STARTTIME = 'starttime';
    const ENDTIME = 'endtime';
    const MILES = 'miles';
    const TRACKED_MILES = 'tracked_miles';
    const DEBIT = 'debit';
    const CREDIT = 'credit';
    const STATUS = 'status';
    const RATEDBY_PASSENGER = 'ratedby_passenger';
    const RATEDBY_DRIVER = 'ratedby_driver';
    const PAYMENTSTATUS = 'payment_status';
    const AMOUNT = 'amount';
    const PAYMENTMODE = 'payment_mode';
    const PAIDMILES = 'paid_miles';
    const OWNERAMOUNT = 'owner_amount';
    const DRIVERAMOUNT = 'driver_amount';
    const DRIVERGOTAMOUNTDATETIME = 'driver_got_amount_date_time';
    const PASSENGERDATETIME = 'passenger_pay_date_time';
    const PAYDONEBY = 'paydoneby';
    const TRANSACTIONID = 'transaction_id';
    const PAYKEY = 'pay_key';
    const REQUEST = 'request';
    const PAYOUTTRANSATIONID = 'payout_transation';
    const FTDRIVERPAYOUT = 'ft_driver_payout';
    const FREEMILSUSED = 'free_miles_used';

    // Database column member definitions 
    private static $DBM01 = array('key' => CJourney::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM02 = array('key' => CJourney::CAR_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM03 = array('key' => CJourney::DRIVER_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM04 = array('key' => CJourney::PASSENGER_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM05 = array('key' => CJourney::FROM_LAT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM06 = array('key' => CJourney::FROM_LNG, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM07 = array('key' => CJourney::FROM_ACC, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM08 = array('key' => CJourney::TO_LAT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM09 = array('key' => CJourney::TO_LNG, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM10 = array('key' => CJourney::TO_ACC, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM11 = array('key' => CJourney::STARTTIME, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
    private static $DBM12 = array('key' => CJourney::ENDTIME, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
    private static $DBM13 = array('key' => CJourney::MILES, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM14 = array('key' => CJourney::DEBIT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM15 = array('key' => CJourney::CREDIT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM16 = array('key' => CJourney::STATUS, 'type' => CCommon::STRING_TYPE, 'size' => 1);
    private static $DBM17 = array('key' => CJourney::TRACKED_MILES, 'type' => CCommon::STRING_TYPE, 'size' => 10);
    private static $DBM18 = array('key' => CJourney::RATEDBY_PASSENGER, 'type' => CCommon::INT_TYPE, 'size' => 10);
    private static $DBM19 = array('key' => CJourney::RATEDBY_DRIVER, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM20 = array('key' => CJourney::PAYMENTSTATUS, 'type' => CCommon::INT_TYPE, 'size' => 2);
    private static $DBM21 = array('key' => CJourney::AMOUNT, 'type' => CCommon::INT_TYPE, 'size' => 10);
    private static $DBM22 = array('key' => CJourney::PAYMENTMODE, 'type' => CCommon::INT_TYPE, 'size' => 2);
    private static $DBM23 = array('key' => CJourney::PAIDMILES, 'type' => CCommon::INT_TYPE, 'size' => 10);
    private static $DBM24 = array('key' => CJourney::OWNERAMOUNT, 'type' => CCommon::INT_TYPE, 'size' => 10);
    private static $DBM25 = array('key' => CJourney::DRIVERAMOUNT, 'type' => CCommon::INT_TYPE, 'size' => 10);
    private static $DBM26 = array('key' => CJourney::DRIVERGOTAMOUNTDATETIME, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
    private static $DBM27 = array('key' => CJourney::PASSENGERDATETIME, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
    private static $DBM28 = array('key' => CJourney::PAYDONEBY, 'type' => CCommon::INT_TYPE, 'size' => 2);
    private static $DBM29 = array('key' => CJourney::TRANSACTIONID, 'type' => CCommon::STRING_TYPE, 'size' => 350);
    private static $DBM30 = array('key' => CJourney::PAYKEY, 'type' => CCommon::STRING_TYPE, 'size' => 200);
    private static $DBM31 = array('key' => CJourney::REQUEST, 'type' => CCommon::INT_TYPE, 'size' => 1);
        private static $DBM32 = array('key' => CJourney::PAYOUTTRANSATIONID, 'type' => CCommon::STRING_TYPE, 'size' => 350);
private static $DBM33 = array('key' => CJourney::FTDRIVERPAYOUT, 'type' => CCommon::INT_TYPE, 'size' => 10);
private static $DBM34 = array('key' => CJourney::FREEMILSUSED, 'type' => CCommon::INT_TYPE, 'size' => 10);

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();
    }

    /*
     * Save journey request
     *
     * @return  true or false
     */

    public function save() {
        return CJourneyDb::save($this);
    }

    /*
     * Load active journey for given passenger
     *
     * @param $passId       Passenger id.
     * @return              true or false
     */

    public function loadActiveByPassengerId($passId) {
        return CJourneyDb::loadActiveByPassengerId($this, $passId);
    }

    public function loadActiveByAverageRating($driver_id) {
        return CJourneyDb::loadActiveByAverageRating($this, $driver_id);
    }

    public static function getJourneysAverageRating($id, $type_id) {

        return CJourneyDb::loadJourneysAverageRating($id, $type_id);
    }

    public static function getJourneysdriverDetails($driver_id, $width, $height) {

        return CJourneyDb::loadJGetDriverDetails($driver_id, $width, $height);
    }

    public static function getPassengerPassPhrase($journey_id) {

        return CJourneyDb::loadPassengerPassPhrase($journey_id);
    }

    /*
     * Load request by id.
     *
     * @param $id     Journey request id.
     * @return        true or false
     */

    public function load($id) {
        return CJourneyDb::load($this, $id);
    }

    /*
     * Return journeys for given passenger
     *
     * @param $userId User id
     * @return       Array of journey ids or false if failed
     */

    public static function getWherePassenger($userId) {
        $journeyIdList = CJourneyDb::getWherePassenger($userId);
        return $journeyIdList;
    }

    /*
     * Return journeys for given driver
     *
     * @param $userId User id
     * @return       Array of journey ids or false if failed
     */

    public static function getWhereDriver($userId) {
        $journeyIdList = CJourneyDb::getWhereDriver($userId);
        return $journeyIdList;
    }

    public static function getWhereJID($userId) {
        $journeyIdList = CJourneyDb::getWhereJID($userId);
        return $journeyIdList;
    }

    public static function getSetFetchSend($journey_id) {
        $journeyIdList = CJourneyDb::getSetFetchSend($journey_id);
        return $journeyIdList;
    }
   public static function getFetchJID($journey_id) {
        $journeyIdList = CJourneyDb::getFetchJID($journey_id);
        return $journeyIdList;
    }
    public static function updatedJourneyAmountStatus($journey_id, $payment_status, $amount, $payment_mode, $paidmiles, $driver_amount, $owner_amount, $paydonebyowner,$ft_driver_payout,$free_miles_used) {
        $journeyIdList = CJourneyDb::updatedJourneyAmountStatus($journey_id, $payment_status, $amount, $payment_mode, $paidmiles, $driver_amount, $owner_amount, $paydonebyowner,$ft_driver_payout,$free_miles_used);
        return $journeyIdList;
    }

    /*
     * Return "$cnt" most recent journeys of all users at once,
     * active or not.
     * Used only by the calllog.php page at the time of writing (June 2014)
     *
     * @param $cnt       Number of records to return
     * @return           Array of records
     */

    public static function getRecentJourneysAllUsers($cnt) {
        return CJourneyDb::getRecentJourneysAllUsers($cnt);
    }

    /*
     * Return array of $cnt most recent records (oldest first) for $userId.
     * If $cnt==0 then all are returned.
     * Returns only active journeys if true == $active, else returns
     * both completed and active journeys.
     *
     * @param $cnt       Number of records to return
     * @param $active 	Boolean to determine whether to return list of
     * 						active or completed journeys.
     * @return           Array of records
     */

    public static function getRecentJourneysOneUser($userId, $active = false, $cnt = 0) {
//		$actstr = sprintf("%s",($active ? "active" : "complete"));
//		CLogging::debug("CJourney::getRecentJourneysOneUser(".$userId.",".$actstr.",".$cnt.")");
        return CJourneyDb::getRecentJourneysOneUser($userId, $active, $cnt);
    }

    /*
     * Return the saved latitude,longitude and tracked miles with the current journey
     * @param $journeyId 	current journey of the user
     */

    public static function getRecentPostionByJourneyId($journeyId) {
        return CJourneyDb::getRecentPostionByJourneyId($journeyId);
    }
  public static function getFetchJIDReport($date_from, $date_to) {
        return CJourneyDb::getFetchJIDReport($date_from, $date_to);
    }
    public static function getRecentPostionByJourneyIdStatus($userId) {
        return CJourneyDb::getRecentPostionByJourneyIdStatus($userId);
    }
public static function paymentStatusMissingInternet($userId) {
        return CJourneyDb::paymentStatusMissingInternet($userId);
    }
    public static function getRecentUpdatePayJourneyIdStatus($jid) {
        return CJourneyDb::getRecentUpdatePayJourneyIdStatus($jid);
    }

    public static function getRecentUpdatePayDriverJourneyIdStatus($journey_id,$date_time,$payKey,$transaction_id,$status) {
        return CJourneyDb::getRecentUpdatePayDriverJourneyIdStatus($journey_id,$date_time,$payKey,$transaction_id,$status);
    }
  public static function getRecentUpdatePayDriverJourneyIdStatusMicro($journey_id,$payKey,$date_time, $transaction_id,$status) {
        return CJourneyDb::getRecentUpdatePayDriverJourneyIdStatusMicro($journey_id,$payKey,$date_time, $transaction_id,$status);
    }
      public static function getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id,$transaction_id) {
        return CJourneyDb::getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id,$transaction_id);
    }
    public static function driveremail($userId) {
        return CJourneyDb::driveremail($userId);
    }

    public static function loaddriverid($j_id) {
        return CJourneyDb::loaddriverid($j_id);
    }

}

?>
