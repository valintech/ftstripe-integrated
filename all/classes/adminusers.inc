<?php
require_once('db/adminusersdb.inc');
//require_once('classes/user.inc');
require_once("classes/root.inc");
/*
 * Journey
 */

class CAdminusers extends CRoot
{
	// Members relating to databsae columns
	const ID = 'id';
	const USERNAME = 'username';
	const PASSWORD = 'password';
	const ACTIVE = 'active';
        const CREATEDAT = 'created_at';

	// Database column member definitions
	private static $DBM01 = array('key' => CAdminusers::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CAdminusers::USERNAME, 'type' => CCommon::STRING_TYPE, 'size' => 15);
        private static $DBM03 = array('key' => CAdminusers::PASSWORD, 'type' => CCommon::STRING_TYPE, 'size' => 20);
        private static $DBM04 = array('key' => CAdminusers::ACTIVE, 'type' => CCommon::INT_TYPE, 'size' => 2);
        private static $DBM05 = array('key' => CAdminusers::CREATEDAT, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
 

	/*
	 * Constructor
	 */

	function __construct ()
	{
		parent::__construct();
	}


        

    public function loadAdminInfo($username,$str_password) { 
        return CAdminUsersDb::loadAdminInfo(new CAdminusers(), $username,$str_password);
    }

   
}
?>
