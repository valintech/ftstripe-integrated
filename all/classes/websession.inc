<?php
require_once("db/websessiondb.inc");

/**
 * Web session class
 */

class CWebSession
{
	const GZ_COMPRESS_LEVEL = 9;      	// 1=lowest, 9=highest
	const SESSION_BASE = 'ers0_1';		// Base variable name in session storage
	
	/*
	 * Initialise PHP session
	 */
	
	public static function init ()
	{
		session_set_save_handler("openCallback", "closeCallback", "readCallback", "writeCallback",
								 "destroyCallback", "gcCallback");
		session_start();
	}
	
	/*
	 * Save session data
	 * 
	 * @param $sessionId    Session id.
	 * @param $sessionData  Data to story
	 */
	
	public static function save ($sessionId, $sessionData)
	{
		$sessionData = base64_encode(gzcompress($sessionData, CWebSession::GZ_COMPRESS_LEVEL));
		return CWebSessionDb::save($sessionId, $sessionData);
	}

	/*
	 * Restore session data
	 * @param $sessionId    Session id.
	 * @return              Session data or false if failed to restore
	 */
	
	public static function restore ($sessionId)
	{
		$sessionData = CWebSessionDb::restore($sessionId);
		if ($sessionData)
			$sessionData = gzuncompress(base64_decode($sessionData));
		return $sessionData;
	}

	/*
	 * Close session
	 */
	
	public static function closeSession ()
	{
		return true;
	}

	/*
	 * Destroy session
	 * @param $sessionId    Session id.
	 */
	public static function destroy ($sessionId)
	{
		return CWebSessionDb::destroy($sessionId);
	}
	
	/*
	 * Set a value
	 * 
	 * @param $key       A key path for the given value i.e "val1", "val2", "section1/val1",
	 *                   "section1/val2"
	 * @param $value     Value to store against the key
	 */

	public static function set ($key, $value)
	{
		if (isset($_SESSION[CWebSession::SESSION_BASE]) == false)
			$_SESSION[CWebSession::SESSION_BASE] = array();
		
		if (strpos($key, "/") === false)
		{
			$_SESSION[CWebSession::SESSION_BASE][$key] = $value;
			return;
		}
		
		$pathParts = explode("/", $key);
		$dest = &$_SESSION[CWebSession::SESSION_BASE];
		for ($i = 0; $i < count($pathParts) - 1; $i++)
		{
			$nextPart = $pathParts[$i];
			if (isset($dest[$nextPart]) == false)
				$dest[$nextPart] = array();
			$dest = &$dest[$nextPart];
		}

		$dest[$pathParts[$i]] = $value;
	}
	
	/*
	 * Get a value
	 * 
	 * @param $key       A key path for the given value i.e "val1", "val2", "section1/val1",
	 *                   "section1/val2"
	 */
	
	public static function get ($key)
	{
		if (isset($_SESSION[CWebSession::SESSION_BASE]) == false)
			$_SESSION[CWebSession::SESSION_BASE] = array();
		
		if (strpos($key, "/") === false)
			return $_SESSION[CWebSession::SESSION_BASE][$key];
		
		$pathParts = explode("/", $key);
		$dest = &$_SESSION[CWebSession::SESSION_BASE];
		for ($i = 0; $i < count($pathParts) - 1; $i++)
		{
			$nextPart = $pathParts[$i];
			if (isset($dest[$nextPart]))
				$dest = &$dest[$nextPart];
			else
				return;
		}
		
		return $dest[$pathParts[$i]];
	}
	
	/*
	 * Delete a value
	 * 
	 * @param $key  A key path for the given value i.e "val1", "val2", "section1/val1",
	 *              "section1/val2"
	 */
	
	public static function delete ($key)
	{
		if (isset($_SESSION[CWebSession::SESSION_BASE]) == false)
			$_SESSION[CWebSession::SESSION_BASE] = array();
		
		if (strpos($key, "/") === false)
		{
			unset($_SESSION[CWebSession::SESSION_BASE][$key]);
			return;
		}

		$pathParts = explode("/", $key);
		$dest = &$_SESSION[CWebSession::SESSION_BASE];
		for ($i = 0; $i < count($pathParts) - 1; $i++)
		{
			$nextPart = $pathParts[$i];
			if (isset($dest[$nextPart]))
				$dest = &$dest[$nextPart];
			else
				return;
		}
		
		unset($dest[$pathParts[$i]]);
	}
	
	/*
	 * Clear all session data
	 */
	
	public static function clearAll ()
	{
		$_SESSION[CWebSession::SESSION_BASE] = array();
	}
}

function openCallback ($savePath, $sessionId)
{
	CLogging::info(sprintf("openCallback - %s", $sessionId));
}

function closeCallback ()
{
	CLogging::info("closeCallback");
	return CWebSession::closeSession();
}

function readCallback ($sessionId)
{
	CLogging::info(sprintf("readCallback - %s", $sessionId));
	return CWebSession::restore($sessionId);
}

function writeCallback ($sessionId, $sessionData)
{
	CLogging::info(sprintf("writeCallback - %s", $sessionId));
	return CWebSession::save($sessionId, $sessionData);
}

function destroyCallback ($sessionId)
{
	CLogging::info(sprintf("destroyCallback - %s", $sessionId));
	return CWebSession::destroy($sessionId);
}

function gcCallback ($maxLifetime)
{
	CLogging::info(sprintf("gcCallback %ld", $maxLifetime));
}

/////////////////////////////////////////////////////////////////////////////////////////////

//require_once('Zend/Session.php');

class CWebSession_Zend
{
	protected static $ns;

	const SESSION_BASE = 'ers0_1';		// Base variable name in session storage
	
	public static function init ()
	{
		self::$ns = new Zend_Session_Namespace(self::SESSION_BASE);
		self::$ns->vals = array();
	}

	/*
	 * Set a value
	 * 
	 * @param $key       A key path for the given value i.e "val1", "val2", "section1/val1",
	 *                   "section1/val2"
	 * @param $value     Value to store against the key
	 */

	public static function set ($key, $value)
	{
		if (strpos($key, "/") === false)
		{
			self::$ns->vals[$key] = $value;
			return;
		}
		
		$pathParts = explode("/", $key);
		$dest = &self::$ns->vals;
		for ($i = 0; $i < count($pathParts) - 1; $i++)
		{
			$nextPart = $pathParts[$i];
			if (isset($dest[$nextPart]) == false)
				$dest[$nextPart] = array();
			$dest = &$dest[$nextPart];
		}

		$dest[$pathParts[$i]] = $value;
	}

	/*
	 * Get a value
	 * 
	 * @param $key       A key path for the given value i.e "val1", "val2", "section1/val1",
	 *                   "section1/val2"
	 */
	
	public static function get ($key)
	{
		if (strpos($key, "/") === false)
			return self::$ns->vals[$key];
		
		$pathParts = explode("/", $key);
		$dest = &self::$ns->vals;
		for ($i = 0; $i < count($pathParts) - 1; $i++)
		{
			$nextPart = $pathParts[$i];
			if (isset($dest[$nextPart]))
				$dest = &$dest[$nextPart];
			else
				return;
		}
		
		return $dest[$pathParts[$i]];
	}
	
	/*
	 * Delete a value
	 * 
	 * @param $key  A key path for the given value i.e "val1", "val2", "section1/val1",
	 *              "section1/val2"
	 */
	
	public static function delete ($key)
	{
		if (strpos($key, "/") === false)
		{
			unset(self::$ns->vals[$key]);
			return;
		}

		$pathParts = explode("/", $key);
		$dest = &self::$ns->vals;
		for ($i = 0; $i < count($pathParts) - 1; $i++)
		{
			$nextPart = $pathParts[$i];
			if (isset($dest[$nextPart]))
				$dest = &$dest[$nextPart];
			else
				return;
		}
		
		unset($dest[$pathParts[$i]]);
	}
	
	/*
	 * Clear all session data
	 */
	
	public static function clearAll ()
	{
		self::$ns->vals = array();
	}
}
?>
