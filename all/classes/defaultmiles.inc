<?php
require_once('db/defaultmilesdb.inc');
//require_once('classes/user.inc');
require_once("classes/root.inc");
/*
 * Journey
 */

class CDefaultMiles extends CRoot
{
	// Members relating to databsae columns
	const ID = 'id';
        const ACTIVE = 'active';
        const FREEMILES = 'free_miles';

	// Database column member definitions
	private static $DBM01 = array('key' => CDefaultMiles::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
        private static $DBM02 = array('key' => CDefaultMiles::ACTIVE, 'type' => CCommon::INT_TYPE, 'size' => 2);
        private static $DBM03 = array('key' => CDefaultMiles::FREEMILES, 'type' => CCommon::INT_TYPE, 'size' => 10);
 
 

	/*
	 * Constructor
	 */

	function __construct ()
	{
		parent::__construct();
	}


        

    public function editDefaultMiles($enabled,$free_miles) { 
        return CDefaultMilesDb::editDefaultMiles(new CDefaultMiles(),$enabled,$free_miles);
    }
    public function displayDefaultMiles() { 
        return CDefaultMilesDb::displayDefaultMiles();
    }
   
}
?>
