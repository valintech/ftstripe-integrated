<?php
require_once('classes/common.inc');

class CPayPal
{
	//This function expects $refund_miles to be a positive number.  For
	//example passing a $refund_miles of 5 will credit the user's account
	//which will be a negative number.  For example a refund of
	//50 miles will be represented by $credit == -50
	//and will result in us adding -50 miles to the user's local balance,
	//as we are crediting their remote paypal account with £(50*16/100)
	//
	// If successful, it returns the refund amount in GBP.
	// If unsuccessful, it returns -1.
	public static function refund($refund_miles, $receiverEmail)
	{
		// Set request-specific fields.
		$emailSubject =urlencode('Funds from eco-gogo');
		$receiverType = urlencode('EmailAddress');
		$currency = urlencode('GBP');
		$refund_gbp = ($refund_miles * CConfig::PENCE_PER_MILE_SALE_RATE) / 100;

		if (CConfig::SITE_PAYMENT_FRAMEWORK === 'DumbButtons')
		{
			$ret = $refund_gbp;
		}
		else
		{
			// Add request-specific fields to the request string.
			$nvpStr="&EMAILSUBJECT=$emailSubject&RECEIVERTYPE=$receiverType&CURRENCYCODE=$currency";
	
			$i = 0;
			$rcvEmail = urlencode($receiverEmail);
			$amount = urlencode($refund_gbp);
			$uniqueID = urlencode(mt_rand());			// Cannot have spaces.
			$note = urlencode('Recovered Credit.');		// This note appears in the customer's email from paypal.
			$nvpStr .= "&L_EMAIL$i=$rcvEmail&L_Amt$i=$amount&L_UNIQUEID$i=$uniqueID&L_NOTE$i=$note";
	
			// Execute the API operation
			$httpParsedResponseAr = self::PPHttpPost('MassPay', $nvpStr);
	
			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
				CLogging::info('MassPay Completed Successfully: ' . 'COMMAND= ' . $nvpStr . ' RESPONSE=' . print_r($httpParsedResponseAr, true));
				$ret = $refund_gbp;

				// Store successful refunds in database.
				self::LogRefund(urldecode($receiverEmail), $refund_gbp, $currency, true);
			} else  {
				CLogging::error('MassPay failed: ' . 'COMMAND= ' . $nvpStr . ' RESPONSE=' . print_r($httpParsedResponseAr, true));
				$ret = -1;
			}		
		}
		return $ret;
	}


	private static function LogRefund($receiverEmail, $refund_gbp, $currency, $fulfilled)
	{
		// Log refund request in database

		// Create a transaction ID based upon sha256(salt + datetime)
		$salt='3jhvds5iuybrn6bioOxcSzyoufx09r8t543s';
		$timeofday = gettimeofday();
		$secondsSinceEpoch = $timeofday["sec"];
		$RequestID = hash("sha256", $salt . $timeofday["sec"] . $timeofday["usec"]);

		// Save request into refundrequests table.
		$re = mysql_real_escape_string($receiverEmail);
		$am = mysql_real_escape_string($refund_gbp);
		CDb::query(
						"INSERT INTO refundrequests (
						RequestID,DateTime,ReceiverEmail,Amount,Currency,Fulfilled)
						VALUES
						('$RequestID','$secondsSinceEpoch','$re','$am','$currency','$fulfilled')"
		) or CLogging::Error(mysql_error());
	}

	/**
	 * Send HTTP POST Request
	 *
	 * @param	string	The API method name
	 * @param	string	The POST Message fields in &name=value pair format
	 * @return	array	Parsed HTTP Response body
	 */
	private static function PPHttpPost($methodName_, $nvpStr_) {
		$API_UserName = urlencode(CConfig::PAYPAL_API_USERNAME);
		$API_Password = urlencode(CConfig::PAYPAL_API_PASSWORD);
		$API_Signature = urlencode(CConfig::PAYPAL_API_SIGNATURE);
		$API_Endpoint = CConfig::getPaypalApiEndpoint();
		$version = urlencode('51.0');

		// setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// turning off the server and peer verification(TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// NVPRequest for submitting to server
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

		// setting the nvpreq as POST FIELD to curl
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// getting response from server
		$httpResponse = curl_exec($ch);

		if(!$httpResponse) {
			CLogging::error("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
			return false;
		}

		// Extract the RefundTransaction response details
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}

		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			CLogging::error("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
			return false;
		}

		return $httpParsedResponseAr;
	}
}
?>
