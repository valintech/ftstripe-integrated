<?php
require_once('classes/common.inc');
require_once('classes/calllog.inc');
require_once('classes/words.inc');

/*
 * Msg class
 */

class CMsg
{
	/**
	 * startRide - retrieve pending messages
	 * 
	 * @param int $user_id
	 *        	Session id.
	 * @param string $reg
	 *        	Session id.
	 * @param $location Session
	 *        	id.
	 * @return object stdClass object containing method return or
	 *         'errorId' and 'errorText' members on error
	 */
	public static function startRide($user_id, $reg, $location, $width) {
		
		$user = new CUser ();
		$user->loadWithUserId ( $user_id );
		$driver = new CUser ();
		$driver_id = null;
		$reg_no_exists = $driver->loadWithCarRegCheck ( $reg );
		if (empty ( $reg_no_exists )) {
			$txt = 'Registration number not in database';
			$r_out = CMsg::processErrorLog ( $txt, $user_id, $user, CCommon::REG_NOT_IN_DB );
			return $r_out;
		} else{
			$driver->loadWithCarReg ( $reg );
			$driver_id = $driver->get ( 'id' );
			$driver_pic = $driver->get ( 'photo');
		}
		
		if ($user->get ( CUser::PHONE_CODE ) != '') {
			$txt = 'Passenger needs to verify  mobile number.';
			$r_out =CMsg:: processErrorLog ( $txt, $user_id, $user, CCommon::MOBILE_NOT_VERIFIED );
			return $r_out;
		}
		if ($driver->get ( CUser::PHONE_CODE ) != '') {
			$txt = 'Driver needs to verify mobile number.';
			$r_out = CMsg::processErrorLog ( $txt, $driver_id, $driver, CCommon::MOBILE_NOT_VERIFIED );
			return $r_out;
		}
		
		if ( $driver->get ( 'stripe_connect' )== '' ) {
			$txt ="Whoops Sorry! Driver, ".$driver->get ( 'first_name' ). " needs a stripe account to be considered as a First Thumb driver and to accept payments from passengers.";
			$r_out =CMsg:: processErrorLog ( $txt, $driver_id, $driver, CCommon::NO_STRIPE_ACCOUNT );
			return $r_out;
		}
		
		// Check user has sufficient balance
		/*
		 * if ( $user->get(CUser::CREDIT) < CConfig::MIN_CREDIT_IN_MILES )
		 * {
		 * $txt = 'Insufficient Credit. You cannot start a ride with a balance of less than ' .
		 * CConfig::MIN_CREDIT_IN_MILES . ' miles. Your credit is ' .
		 * $user->get(CUser::CREDIT) .' miles.';
		 * CLogging::error($txt);
		 * if ($user->get('appuser') == 0) {
		 * CCallLog::log($user_id, $user->getFullname(), 'T', $txt);
		 * CSmsGw::send($user->get('mobile'), $txt);
		 * return new stdClass;
		 * } else {
		 * $out = CCommon::makeErrorObj(CCommon::INSUFFICIENT_CREDIT, $txt);
		 * CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out));
		 * return $out;
		 * }
		 * }
		 */
		
		$journey = new CJourney ();
		$journey->loadActiveByPassengerId ( $user_id );
		
		if ($journey->get ( 'id' ) != 0) {
			$txt = 'Please END your previous journey before starting a new one';
			$r_out = CMsg::processErrorLog ( $txt, $user_id, $user, CCommon::ALREADY_IN_JOURNEY );
			return $r_out;
		}
		$driver->loadWithCarReg ( $reg );
		$driver_id = $driver->get ( 'id' );
		$driver_pic = $driver->get ( 'photo');
		
		$journey->set ( 'passenger_id', $user_id );
		$journey->set ( 'driver_id', $driver_id );
		$journey->set ( 'status', 'A' );
		$journey->set ( 'from_lat', $location ['lat'] );
		$journey->set ( 'from_lng', $location ['lng'] );
		$journey->set ( 'from_acc', $location ['accuracy'] );
		$journey->save ();
		// OK, send both parties the magic word
		
			$out = new stdClass ();
			$word = CWords::getone ();
			if ($user->get ( 'appuser' ) == 0) {
				$txt = "Your driver is " . $driver->get ( 'first_name' ) . " and the passphrase is '$word'. " . "Your credit is currently " . $user->get ( CUser::CREDIT ) . " miles";
				CCallLog::log ( $user_id, $user->getFullname (), 'T', $txt );
				CSmsGw::send ( $user->get ( 'mobile' ), $txt );
			} else {
				// construct response
				$out->type = 'startRide';
				$out->driver = $driver->get ('first_name' );
				$out->formatDate = date ("d-m-Y H:i:s" );
				$file = '';
				$filett = '';
				$dir = '../upload/' . $driver_id . "/";
				if (isset ( $driver_pic )) {
					if (is_dir ( $dir )) {
						if ($dh = opendir ( $dir )) {
							while ( ($file = readdir ( $dh )) !== false ) {								
								$pos = strpos ( $file, $driver_id . "_" . $width );
								if ($pos !== false)
									$filett = $file;
							}
							closedir ( $dh );
						}
					}
				}
				$photo = 'upload/' . $driver_id . '/' . $filett;
				
				$out->magic = $word;
				$out->credit = $user->get ( 'credit' );
				$out->photo = $photo;
				CCallLog::log ( $user_id, $user->getFullname (), 'T', CCommon::toJson ( $out ), 'n', $journey->get ( CJourney::ID ) );
			}
			if ($driver->get ( 'appuser' ) == 0) {
				$txt = $user->get ( 'first_name' ) . " is requesting a ride and the passphrase is '$word'. " . "For trips more than " . CConfig::MIN_CREDIT_IN_MILES . " miles ask the passenger to show you their credit balance.";
				CCallLog::log ( $driver_id, $driver->getFullname (), 'T', $txt );
				CSmsGw::send ( $driver->get ( 'mobile' ), $txt );
			} else {
				// queue message
				$msg = new stdClass ();
				$msg->type = 'startRide';
				$msg->passenger = $user->get ('first_name' );
				$msg->magic = $word;
				$msg->formatDate = date ( "d-m-Y H:i:s" );
				$file1 = '';
				$filett1 = '';
				$dir1 = '../upload/' . $user_id . "/";
				$pass_pic = $user->get ( 'photo' );
				if (isset ( $pass_pic )) {
					if (is_dir ( $dir1 )) {
						if ($dh1 = opendir ( $dir1 )) {
							while ( ($file1 = readdir ( $dh1 )) !== false ) {
								
								$pos1 = strpos ( $file1, $user_id . "_" . $width );
								if ($pos1 !== false)
									$filett1 = $file1;
							}
							closedir ( $dh1 );
						}
					}
				}
				$photo1 = 'upload/' . $user_id . '/' . $filett1;
				$out->photoPass = $photo1;
				$out->driver_first_name = $driver->get ( 'first_name' );
				$file = '';
				$filett = '';
				$dir = '../upload/' . $driver_id . "/";
				
				if (isset ( $driver_pic )) {
					if (is_dir ( $dir )) {
						if ($dh = opendir ( $dir )) {
							while ( ($file = readdir ( $dh )) !== false ) {
								
								$pos = strpos ( $file, $driver_id . "_" . $width );
								if ($pos !== false)
									$filett = $file;
							}
							closedir ( $dh );
						}
					}
				}
				$driver_photo = 'upload/' . $driver_id . '/' . $filett;
				$out->DriverphotoPass = $driver_photo;
				// CCallLog::log($user_id, $user->get('first_name'), 'T', CCommon::toJson($msg), 'y',$journey->get(CJourney::ID));
				CCallLog::log ( $driver_id, $driver->getFullname (), 'T', CCommon::toJson ( $msg ), 'y', $journey->get ( CJourney::ID ) );
			}
			return $out;
	}
	private  static function processErrorLog($txt, $user_id, $user, $error_id) {
		CLogging::error ( $txt );
		if ($user->get ( 'appuser' ) == 0) {
			CCallLog::log ( $user_id, $user->getFullname (), 'T', $txt );
			CSmsGw::send ( $user->get ( 'mobile' ), $txt );
			return new stdClass ();
		} else {
			$out = CCommon::makeErrorObj ( $error_id, $txt );
			CCallLog::log ( $user_id, $user->getFullname (), 'T', CCommon::toJson ( $out ) );
			return $out;
		}
	}
	public static function endRide($user_id, $miles, $location)
	{
         
            //writing object dump to file
            
		$user = new CUser();
		$user->loadWithUserId($user_id);
		$available_credit = $user->get('credit');
                $journey = new CJourney();
                
		$journey->loadActiveByPassengerId($user_id);
		/*if (preg_match('/^\d+$/', $miles) == 0 || 
			$miles < 0 || $miles > CConfig::MAX_JOURNEY_LENGTH_IN_MILES ||
			$miles > $available_credit )
		{
			CLogging::error($txt);
			if ($miles <= $available_credit)
			{
				// sufficient credit so either bad syntax or amount out of bounds
				if ($user->get('appuser') == 0) {
					$txt = 'Message format should be "END#nn" '.
						'where "nn" is the number of miles travelled';
					CCallLog::log($user_id, $user->getFullname(), 'T', $txt);
					CSmsGw::send($user->get('mobile'), $txt);
					return new stdClass;
				} else {
					$txt = "Please enter a valid distance";
					$out = CCommon::makeErrorObj(CCommon::INVALID_MILES_VALUE, $txt);
					CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out));
					return $out;
				}
			}
			else
			{
				$txt = 'Insufficient credit to pay for journey. Your credit is '.$available_credit.' miles';
				if ($user->get('appuser') == 0) {
					CCallLog::log($user_id, $user->getFullname(), 'T', $txt);
					CSmsGw::send($user->get('mobile'), $txt);
					return new stdClass;
				} else {
					$out = CCommon::makeErrorObj(CCommon::INSUFFICIENT_CREDIT, $txt);
					CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out));
					return $out;
				}
			}
		}*/
		
		if ($journey->get('id') == 0) {
			$txt = 'Your last journey has already been ended';
			CLogging::error($txt);
			if ($user->get('appuser') == 0) {
				CSmsGw::send($user->get('mobile'), $txt);
				CCallLog::log($user_id, $user->getFullname(), 'T', $txt);
				return new stdClass;
			} else {
				$out = CCommon::makeErrorObj(CCommon::NOT_IN_JOURNEY, $txt);
				CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out));
				return $out;
			}
		}

		// If somehow the user manages to terminate the journey with more miles than credit,
		// ensure miles is truncated suitably. **This should never run**
		if  ($miles > $available_credit)
		{
			CLogging::error('Truncating mileage from '.$miles.' to '.$available_credit.' miles.');
			// $miles = $available_credit;
		}
		$journey->set('status', 'P');
		$journey->set('miles', $miles);
		$journey->save();
		$driver_id = $journey->get('driver_id');
		$driver = new CUser();
		$driver->loadWithUserId($driver_id);
		{
			$out = new stdClass;
                        
			$user->set('credit', intval($user->get('credit')) - $miles);
			$driver->set('credit', intval($driver->get('credit')) + $miles);
			$ptxt = sprintf("You have been charged %d miles for this trip, your credit is now %d miles",
					$miles, $user->get('credit'));
			$dtxt = sprintf($user->get('first_name')." has completed a journey of %d miles; your credit is now %d miles.",
					$miles, $driver->get('credit'));
			$user->save();
			$driver->save();
			if ($user->get('appuser') == 0) {
				// text passenger
                           
				CCallLog::log($user_id, $user->getFullname(), 'T', $ptxt);
				CSmsGw::send($user->get('mobile'), $ptxt);
			} else {
				$out->type = 'endRide';
				$out->miles = $miles;
				$out->charged = $miles;
				$out->credit = $user->get('credit');
                                $out->formatDate =date("d-m-Y H:i:s");
				CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out),'n',$journey->get(CJourney::ID));
			}
			if ($driver->get('appuser') == 0) {
				// text driver
				CCallLog::log($driver_id, $driver->getFullname(), 'T', $dtxt);
				CSmsGw::send($driver->get('mobile'), $dtxt);
			} else {
				// queue message to driver
				$msg = new stdClass();
				$msg->type = 'endRide';
				$msg->passenger = $user->get('first_name');
				$msg->miles = $miles;
				$msg->credited = $miles;
				$msg->credit = $driver->get('credit');
                                $msg->formatDate =date("d-m-Y H:i:s");
				CCallLog::log($driver_id, $driver->getFullname(), 'T', CCommon::toJson($msg), 'y',$journey->get(CJourney::ID));
                                $msgp = new stdClass();
				$msgp->type = 'endRidepassenger';
				$msgp->driver = $driver->getFullname();
				$msgp->miles = $miles;
				$msgp->credited = $miles;
				$msgp->credit = $driver->get('credit');
                                $msgp->formatDate =date("d-m-Y H:i:s");
                                CCallLog::log($user_id, $driver->getFullname(), 'T', CCommon::toJson($msgp), 'y',$journey->get(CJourney::ID));
			}
                        $out->journey_id = $journey->get(CJourney::ID);
                        $out->driverEndride = 2;
                        $out->formatDate =date("d-m-Y H:i:s");
			return $out;
		}
	}

    public static function driverEndRide($user_id, $miles, $location,$driver_id,$journey_id)
	{
         
            //writing object dump to file
            
		$user = new CUser();
		$user->loadWithUserId($user_id);
		//$available_credit = $miles + 1;//$user->get('credit'); // Fix
        $journey = new CJourney();
                
		$journey->loadActiveByPassengerId($user_id);
 
		// if (preg_match('/^\d+$/', $miles) == 0 || 
		// 	$miles < 0 || $miles > CConfig::MAX_JOURNEY_LENGTH_IN_MILES ||
		// 	$miles > $available_credit )
		// {
		// 	CLogging::error($txt);
		// 	if ($miles <= $available_credit)
		// 	{
		// 		// sufficient credit so either bad syntax or amount out of bounds
		// 		if ($user->get('appuser') == 0) {
		// 			$txt = 'Message format should be "END#nn" '.
		// 				'where "nn" is the number of miles travelled';
		// 			CCallLog::log($user_id, $user->getFullname(), 'T', $txt);
		// 			CSmsGw::send($user->get('mobile'), $txt);
		// 			return new stdClass;
		// 		} else {
		// 			$txt = "Please enter a valid distance";
		// 			$out = CCommon::makeErrorObj(CCommon::INVALID_MILES_VALUE, $txt);
		// 			CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out));
		// 			return $out;
		// 		}
		// 	}
		// 	else
		// 	{
		// 		$txt = 'Insufficient credit to pay for journey. Your credit is '.$available_credit.' miles';
		// 		if ($user->get('appuser') == 0) {
		// 			CCallLog::log($user_id, $user->getFullname(), 'T', $txt);
		// 			CSmsGw::send($user->get('mobile'), $txt);
		// 			return new stdClass;
		// 		} else {
		// 			$out = CCommon::makeErrorObj(CCommon::INSUFFICIENT_CREDIT, $txt);
		// 			CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out));
		// 			return $out;
		// 		}
		// 	}
		// }
		
		if ($journey_id == 0) {
			$txt = '1Your last journey has already been ended';
			CLogging::error($txt);
			if ($user->get('appuser') == 0) {
				CSmsGw::send($user->get('mobile'), $txt);
				CCallLog::log($user_id, $user->getFullname(), 'T', $txt);
				return new stdClass;
			} else {
				$out = CCommon::makeErrorObj(CCommon::NOT_IN_JOURNEY, $txt);
				CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out));
				return $out;
			}
		}

		// If somehow the user manages to terminate the journey with more miles than credit,
		// // ensure miles is truncated suitably. **This should never run**
		// if  ($miles > $available_credit)
		// {
		// 	CLogging::error('Truncating mileage from '.$miles.' to '.$available_credit.' miles.');
		// 	//$miles = $available_credit;
		// }
                $journey->set('id', $journey_id);
		$journey->set('status', 'P');
		$journey->set('miles', $miles);
		$journey->save();
		$driver_id = $driver_id;
		$driver = new CUser();
		$driver->loadWithUserId($driver_id);
		{
			$out = new stdClass;
			$user->set('credit', intval($user->get('credit')) - $miles);
			$driver->set('credit', intval($driver->get('credit')) + $miles);
			$ptxt = sprintf("You have been charged %d miles for this trip, your credit is now %d miles",
					$miles, $user->get('credit'));
			$dtxt = sprintf($user->get('first_name')." has completed a journey of %d miles; your credit is now %d miles.",
					$miles, $driver->get('credit'));
			$user->save();
			$driver->save();
			if ($user->get('appuser') == 0) {
				// text passenger
                           
				CCallLog::log($user_id, $user->getFullname(), 'T', $ptxt);
				CSmsGw::send($user->get('mobile'), $ptxt);
			} else {
				$out->type = 'endRide';
				$out->miles = $miles;
				$out->charged = $miles;
				$out->credit = $user->get('credit');
                                $out->formatDate =date("d-m-Y H:i:s");
				CCallLog::log($user_id, $user->getFullname(), 'T', CCommon::toJson($out),'n',$journey_id);
			}
			if ($driver->get('appuser') == 0) {
				// text driver
				CCallLog::log($driver_id, $driver->getFullname(), 'T', $dtxt);
				CSmsGw::send($driver->get('mobile'), $dtxt);
			} else {
				// queue message to driver
				$msg = new stdClass();
				$msg->type = 'endRide';
				$msg->passenger = $user->get('first_name');
				$msg->miles = $miles;
				$msg->credited = $miles;
				$msg->credit = $driver->get('credit');
                                 $msg->formatDate =date("d-m-Y H:i:s");
				CCallLog::log($driver_id, $driver->getFullname(), 'T', CCommon::toJson($msg), 'y',$journey_id);
                                 $msgp = new stdClass();
                                 $msgp->formatDate =date("d-m-Y H:i:s");
				$msgp->type = 'endRidepassenger';
				$msgp->driver = $driver->getFullname();
				$msgp->miles = $miles;
				$msgp->credited = $miles;
				$msgp->credit = $driver->get('credit');
                                CCallLog::log($user_id, $driver->getFullname(), 'T', CCommon::toJson($msgp), 'y',$journey_id);
			}
                        $out->journey_id = $journey_id;
                        $out->driverEndride = 1;
                        $out->formatDate =date("d-m-Y H:i:s");
			return $out;
		}
	}

 
}

