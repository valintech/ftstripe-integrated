<?php
require_once 'base.inc';
require_once 'classes/session.inc';
require_once 'classes/region.inc';
require_once 'classes/journey.inc';
require_once 'classes/google.inc';
require_once 'classes/yahoo.inc';
require_once 'classes/php2js.inc';
if (CConfig::RUN_IN_FB)
	require_once 'classes/facebook.inc';

//
// Potential request values:-
// "op" - operation code for specific script functions
// "journeyId" - journey id. to delete / leave / etc
//

// Get current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->requireSession();
CWebSession::set('login', serialize($login));

{
	// Output HTML page
	$region = new CRegion("myhistory");
	$rplc = array();
	if ($isValidSession)
		$rplc[1] = sprintf('|%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
	else
		$rplc[1] = sprintf('|&nbsp;<a href="javascript:signIn()">%s</a>', $region->msg(3, "common"));
	//$rplc[2] = script($login, $isValidSession);
	//$rplc[4] = scriptLinks();
	$rplc[5]  = search($login->userId());
	$rplc[8] = $region->msg(8, 'common');
	$rplc[9] = $region->msg(9, 'common');
	$rplc[11] = $region->msg(1100);
        $rplc[1155] = $region->msg(1155);
	$rplc[30] = $region->msg(10, 'common');
	$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
	$rplc[32] = $region->msg(13, 'common');
	$rplc[33] = $region->msg(14, 'common');
	$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
	$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
	$out = CCommon::htmlReplace("myhistory.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
	print($out);
	if (CConfig::RUN_IN_FB == 0)
		@include 'google_analytics.html';
}

/*
 * Print <script> element to output
 *
 * @param $login            A CLogin object
 * @param $isValidSession   true if have current session otherwise false
 * @return                  HTML <script> stream
 */

function script ($login, $isValidSession)
{
	$php2Js = new Php2Js();
	$out = array();
	$out[] = '<script type="text/javascript">';
	$out = array_merge($out, CRoot::formatClassAsJs('CJourney'));
	$out = array_merge($out, CRoot::formatClassAsJs('CConfig', array(CConfig::CONTENT_DIR)));
	require_once('classes/region.inc');
	$region = new CRegion('myhistory');
	$php2Js->add('_msgList', $region->msgList());
	$user = $login->getUser(true);
	$php2Js->add('_isUserRegistered', ($user && $user->isRegistered() ? true : false));
	$php2Js->add('_isValidSession', ($isValidSession ? true : false));
	$out[] = $php2Js->generateJs();
	$out[] = '</script>';
	return join('', $out);
}

/*
 * Generate <script> links
 *
 * @return HTML <script> links
 */

function scriptLinks ()
{
	$out = array();
	$out[] = CGoogle::scriptHtml();
	$out[] = CYahoo::scriptHtml(array('json', 'connection', 'container', 'menu', 'button'));
	$out[] = '<script type="text/javascript" src="js/common.js"></script>';
	$out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
	$out[] = '<script type="text/javascript" src="myhistory.js"></script>';
	return join("\n", $out);
}

/*
 * Search for locations
 *
 * @param $journeyList  List of CJourney objects
 */

function search ($userId)
{
	define('NUM_JOURNEYS', '30');
	$region = new CRegion('myhistory');
	$html = array();
	$html[] = '<table class="table table-striped" cellpadding="0" cellspacing="0">';

	$html[] = sprintf('<tr><td colspan="4"><h3>Your %d most recent completed journeys:</h3><br></td></tr>', NUM_JOURNEYS);
	$journeys = CJourney::getRecentJourneysOneUser($userId, false, NUM_JOURNEYS);
	$n = count($journeys);
	$html[] = '<tr><th>Driver</th><th>Passenger</th><th>Started</th><th>Miles&nbsp;&nbsp;</th></tr>';
	if ($n != '0') {
		$earned = 0;
		$spent = 0;
		$pmiles = 0;
		for ($i = 0; $i < $n; $i++) {
			$j = $journeys[$i];
			$html[] = '<tr>';
			if ($j['driver_id'] == $userId)
				$html[] = '<td>You</td>';
			else
				$html[] = '<td>'.$j['driver_first_name'].' '.$j['driver_last_name'].'</td>';
			if ($j['passenger_id'] == $userId)
				$html[] = '<td>You</td>';
			else
				$html[] = '<td>'.$j['passenger_first_name'].' '.$j['passenger_last_name'].'</td>';
			$html[] = sprintf('<td>%s</td>', strftime('%a %d %b %Y at %R', $j['starttime']));
			$html[] = '<td>'.$j['miles'].'</td>';
			if ($j['driver_id'] == $userId) {
				$earned += $j['miles'];
			}
			else {
				$spent += $j['miles'];
				$pmiles += $j['miles'];
			}
			$html[] = '</tr>';
			//$html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>', CConfig::CONTENT_DIR);
		}
		$html[] = '<tr></tr>';
		$html[] = sprintf('<tr><td colspan="5">In this period you spent %d miles as a passenger and earned %d miles as a driver.', $spent, $earned);
	}
	else
		$html[] = sprintf('<tr><td></td><td></td><td><span class="resultsDimmed">%s</span><br></td><td></td></tr>', $region->msg(1011));

	$html[] = '</table>';

	return join('', $html);
}

/*
 * Create HTML representation of the journey
 *
 * @param $index        Journey index
 * @param $journey      A CJourney object
 * @param $region       A CRegion object
 * @param $journeyInfo  stdClass object containing extra journey
 *                      information "placesTaken", "placesAvailable",
 *                      "smoker", "femaleOnly"
 *                      members
 * @return              HTML
 */

function journeyHtml ($index, $journey, $region, $journeyInfo, $type)
{
	// Female only, smoker information
	$tmp = array();
	if ($journeyInfo->smoker)
		$tmp[] = $region->msg(1013);
	if ($journeyInfo->femaleOnly)
		$tmp[] = $region->msg(1014);
	$hostPrefsHtml = (count($tmp) ? join(', ', $tmp) : '');

	$tmp2 = array();
	$tmp2[] = '<br/>I am ';
	if ($journey->get(CJourney::REQUEST_TYPE) == CJourney::REQUEST_TYPE_SEEKING) {
		$tmp2[] = 'seeking';
	} else if ($journey->get(CJourney::REQUEST_TYPE) == CJourney::REQUEST_TYPE_OFFERING) {
		$tmp2[] = 'offering';
	} else {
		$tmp2[] = 'offering to share';
	}
	$tmp2[] = ' a lift';
	$requestorHtml = join('', $tmp2);

	$html = array();
	$html[] = '<tr><td>';

	if ($journey->get(CJourney::JOURNEY_TYPE) == CJourney::JOURNEY_TYPE_ONEOFF)
	{
		$html[] = '<strong>';
		$html[] = $region->msg(1015);
		$html[] = '</strong>';
		$html[] = $requestorHtml;
		$html[] = '<span class="resultsDimmed">';
		$html[] = sprintf('<br/>On %s @ %s', strftime('%x', CCommon::tsToPhp($journey->get(CJourney::ONEOFF_DATE))),
						  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CJourney::ONEOFF_TIME))));
		if ($hostPrefsHtml != '')
			$html[] = '<br/>' . $hostPrefsHtml;
		$html[] = '</span><br/>';
	}
	else
	{
		$html[] = '<strong>';
		$html[]  = $region->msg(1016);
		$html[] = '</strong>';
		$html[] = $requestorHtml;
		$html[] = '<span class="resultsDimmed">';
		if ($journey->get(CJourney::SCHEDULE_TYPE) == CJourney::SCHEDULE_TYPE_STANDARD)
			$html[] = sprintf('<br/>%s %s %s - Start @ %s / Return @ %s',
							  $region->msg(17, 'common'), $region->msg(24, 'common'), $region->msg(21, 'common'),
							  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CJourney::STANDARD_START_TIME))),
							  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CJourney::STANDARD_RETURN_TIME))));
		elseif ($journey->get(CJourney::SCHEDULE_TYPE) == CJourney::SCHEDULE_TYPE_CUSTOM)
			$html[] = customTimesHtml($journey, $region);
		if ($hostPrefsHtml != '')
			$html[] = '<br/>' . $hostPrefsHtml;
		$html[] = '</span><br/>';
	}
	$html[] = sprintf('%s<br/><img align="absmiddle" src="%s/images/arrow.gif" width="12" height="12" align="absmiddle" />&nbsp;%s<br/>',
					  $journey->get(CJourney::START_ADDRESS),
					  CConfig::CONTENT_DIR,
					  $journey->get(CJourney::DESTINATION_ADDRESS));
	$html[] = '</td></tr>';

	$html[] = '<tr><td align="right">';
	$tmp = array();
//	if ($type == 'driver')
		$tmp[] = sprintf('<a href="javascript:_deleteJourney(%d)">%s</a>', $index, $region->msg(1017));
	$tmp[] = sprintf('<a href="javascript:_searchJourney(%d)">%s</a>', $index, $region->msg(1018));
	$html[] = join('<span class="resultsOptions">&nbsp;|&nbsp;</span>', $tmp);
	$html[] = '</td></tr>';

	return join('', $html);
}

/*
 * Create HTML for custom journey times
 *
 * @param $journey    A CJourney object
 * @param $region     A CRegion object
 * @return            HTML
 */

function customTimesHtml ($journey, $region)
{
	$dayMsgs = array(17, 18, 19, 20, 21, 22, 23);
	$times = array(
		array(CJourney::MON_START_TIME, CJourney::MON_RETURN_TIME),
		array(CJourney::TUE_START_TIME, CJourney::TUE_RETURN_TIME),
		array(CJourney::WED_START_TIME, CJourney::WED_RETURN_TIME),
		array(CJourney::THU_START_TIME, CJourney::THU_RETURN_TIME),
		array(CJourney::FRI_START_TIME, CJourney::FRI_RETURN_TIME),
		array(CJourney::SAT_START_TIME, CJourney::SAT_RETURN_TIME),
		array(CJourney::SUN_START_TIME, CJourney::SUN_RETURN_TIME));
	$out = array();
	foreach ($times as $k => $time)
	{
		$start = $journey->get($time[0]);
		$end = $journey->get($time[1]);
		if ($start == '' || $end == '')
			continue;
		$out[] = sprintf('%s - Start @ %s / Return @ %s',
			$region->msg($dayMsgs[$k], 'common'),
			strftime($region->msg(25, 'common'), CCommon::tmToPhp($start)),
			strftime($region->msg(25, 'common'), CCommon::tmToPhp($end)));
	}
	$out = join('<br/>', $out);
	return $out != '' ? '<br/>'.$out : $out;
}
?>
