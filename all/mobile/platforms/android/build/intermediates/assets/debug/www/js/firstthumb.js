var status_netchange = 0;
var DataFormatVersion = 1;
var name_of_driver = '';
var msg_passenger = '';
var travelled_miles = '';
var base_url = '';
var wsurl = '';
var notification_id = '';
var live_test_mode = 1;
var click_count = 0;
var initial;
var save_signin_in_browser = true;
var IdleTimeoutInSeconds = 0; // Zero means no idle timeout
var RefreshPeriodInSeconds = 60;
var msgTable;
var msgTablep;
var sessionId;
var signedIn = 0;
var active = '';
var IdleCounter = 0;
var RefreshCounter = 0;
var LastMsgIdReceived = 0;
var LastMsgIdReceivedp = 0;
var cur_page = '';
var cookieData;
var globalData;
var im_detail_contact = '';
var im_detail_group;
var pushRegistrationTimeout = 1000;
var WsMsgId = 1;
var splash_time = 0;
var splash_cnt = 0;
var watch_id = null;
var lastreport_timestamp = 0;
var data = [];
var map;
var myVar = '';
var request_stop = 0;
var setTimeIntervalClear = 7000;
var WsRequests = {
    Active: false,
    Queue: [],
    Pending: {},
};

var end_reload = true;

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
 
$(function() {
    /*var $menu = $('nav#menu');
     
     $menu.mmenu({
     slidingSubmenus: false
     });
     
     $menu.find( 'li span' ).not( '.mm-subopen' ).not( '.mm-subclose' ).bind(
     'click.example',
     function( e )
     {
     var $option = $.trim( $(this).text() );
     e.preventDefault();
     $('#menu').trigger( 'close' );
     set_page($option);
     $(this).trigger( 'setSelected.mm' );
     }
     );*/
    $("#cssmenu ul li .active_link").click(function(event) {
        var $option = $.trim($(this).text());
        //e.preventDefault();

        console.log($option);

        set_page($option);
    });
    $("#forgot_button").click(function(event) {

        set_page('Forgot');
    });
    $("#start_new_group").click(function(event) {

        set_page('Start New Group');
    });
    $("#go_to_home").click(function(event) {

        set_page('Sign In');
    });

    $("#register_button").click(function(event) {

        //set_page('Register');


        var ref = window.open(encodeURI(base_url + '/register.php'), '_system', 'location=no');
        ref.addEventListener('loadstop', function(event) {        
            
            console.log(event);
            if (event.url == "https://firsthumb.com/register.php?sucess=stripe-connect") {            
                $('#reg-sucess').show();
                ref.close();
            }
        });

    });
});

/////////////////////////////////automatic net reconnection 
var nIntervId;

function startConnectionCheck() {
    nIntervId = setInterval(connectionEstablish, 1000);
}

function connectionEstablish() {
    var networkState = navigator.connection.type;
    if (networkState != 'none') {
        $('.net_issues').html('');
        stopConnectionAlert();
    }
}

function stopConnectionAlert() {
    clearInterval(nIntervId);
}

/*$.fn.mmenu.debug = function( msg ) {
 alert( msg );
};*/
/*setInterval(function(){
   checkConnection; }, 3000);*/

function show_refreshing(active) {
    if (active) {
        //document.getElementById( "refresh_button" ).style.backgroundImage = "url(img/refreshing.gif)";
        $("#overlay").remove();
        refresh_icon();
        //  pages_height();
    } else {
        //document.getElementById( "refresh_button" ).style.backgroundImage = "url(img/refresh.png)";


        $("#overlay").remove();
        //  pages_height();
    }
}

function pages_height() {
    var screen_height = $(window).height();
    var signin_height = $("#signin-box").height();
    var forgot_height = $("#forgot-box").height();
    var webpages_height = $("#webpages-box").height();
    var messagecentre_height = $("#messagecentre-box").height();
    var settings_height = $("#settings-box").height();
    var passenger_height = $("#passenger-box").height();
    var driver_height = $("#driver-box").height();
    // alert(messagecentre_height + '==' + $(window).height());
    $('#signin-box').attr('style', 'height: ' + screen_height + 'px !important');
    $('#forgot-box').attr('style', 'height: ' + screen_height + 'px !important');
    $('#webpages-box').attr('style', 'height: ' + screen_height + 'px !important');
    $('#messagecentre-box').attr('style', 'height: ' + screen_height + 'px !important');
    $('#settings-box').attr('style', 'height: ' + screen_height + 'px !important');
    $('#passenger-box').attr('style', 'height: ' + screen_height + 'px !important');
    $('#driver-box').attr('style', 'height: ' + screen_height + 'px !important');
    if (screen_height < signin_height)
        $('#signin-box').attr('style', 'height: ' + signin_height + 'px !important');
    if (screen_height < forgot_height)
        $('#forgot-box').attr('style', 'height: ' + forgot_height + 'px !important');
    if (screen_height < webpages_height)
        $('#webpages-box').attr('style', 'height: ' + webpages_height + 'px !important');
    if (screen_height < messagecentre_height)
        $('#messagecentre-box').attr('style', 'height: ' + messagecentre_height + 'px !important');
    if (screen_height < settings_height)
        $('#settings-box').attr('style', 'height: ' + settings_height + 'px !important');
    if (screen_height < passenger_height)
        $('#passenger-box').attr('style', 'height: ' + passenger_height + 'px !important');
    if (screen_height < driver_height)
        $('#driver-box').attr('style', 'height: ' + driver_height + 'px !important');
    //  alert("2wwww1" + window.innerHeight + " - " + $('#messagecentre-box').offset().top + "====" + screen_height + "sigh" + messagecentre_height)
}

function refresh_icon() { //checkConnection();


    text = '<div id="fountainG">' +
        '<div id="fountainG_1" class="fountainG"></div>' +
        '<div id="fountainG_2" class="fountainG"></div>' +
        '<div id="fountainG_3" class="fountainG"></div>' +
        '<div id="fountainG_4" class="fountainG"></div>' +
        '<div id="fountainG_5" class="fountainG"></div>' +
        '<div id="fountainG_6" class="fountainG"></div>' +
        '<div id="fountainG_7" class="fountainG"></div>' +
        '<div id="fountainG_8" class="fountainG"></div>' +
        '</div>';
    $("<table id='overlay'><tbody><tr><td>" + text + "</td></tr></tbody></table>").css({
        "position": "fixed",
        "top": "0px",
        "left": "0px",
        "width": "100%",
        "height": "100%",
        "background-color": "rgba(0,0,0,.5)",
        "z-index": "10000000000000000000000000000",
        "vertical-align": "middle",
        "text-align": "center",
        "color": "#fff",
        "font-size": "40px",
        "font-weight": "bold",
        "cursor": "wait"
    }).appendTo("body");
    var networkState = navigator.connection.type;
    if (networkState == 'none') {
        $("#overlay").remove();
    }
    // pages_height();

}

$(document).ready(function() {

    $('#endridestatus').hide();

    $('#totopscroller').totopscroller({});
    set_page('Sign In');
    msgTable = document.getElementById("msgTable");
    msgTablep = document.getElementById("msgTablep");
    setInterval("refresh()", 1000);
    //document.getElementById('showViewport').innerHTML = "Viewport: " + window.innerWidth + "x" + window.innerHeight;
    $('#webpage_name').css('height', $(window).height() + 'px');
    $('#buttonset').hide();
    $('#menu_button').hide();
    $('#back_button').hide();
    $('#refresh_button').hide();
    window.onresize = resize;
    load_global_data();
});

function load_global_data() {
    globalData = null;
    if (localStorage.getItem('firstthumb') !== null) {
        var value = localStorage.getItem('firstthumb');
        try {
            globalData = jQuery.parseJSON(unescape(value));
        } catch (e) {}
    }
    if (globalData == null || globalData == undefined ||
        !globalData.hasOwnProperty('DataFormatVersion') ||
        globalData.DataFormatVersion != DataFormatVersion) {
        globalData = {
            DataFormatVersion: DataFormatVersion,
            TestSite: false
        };
    }
    if (globalData.hasOwnProperty('username'))
        document.getElementById('username').value = globalData.username;
    if (globalData.hasOwnProperty('password'))
        document.getElementById('password').value = globalData.password;
    if (globalData.TestSite) {
        $('#site_name').show().text('First Thumb (test site)');
        //base_url = 'http://host.ipsrtraining.com/~phonegap/app/eco-gogo_valin-master/web';
        base_url = 'https://firsthumb.com';
        // base_url = 'http://station6.ipsrkochi.com/test_firsthumb22-sep-2016';
        notification_id = '591175046284';
    } else {
        $('#site_name').show().text('First Thumb');
        //base_url = 'http://host.ipsrtraining.com/~phonegap/app/eco-gogo_valin-master/web';
        base_url = 'https://firsthumb.com';
        //base_url = 'http://station6.ipsrkochi.com/test_firsthumb22-sep-2016';
        notification_id = '591175046284';
    }
    wsurl = base_url + '/ws/query.php?redirect=false';
    save_global_data();
}

function save_global_data() {
    if (!(window.cordova || save_signin_in_browser)) {
        globalData.username = '';
        globalData.password = '';
    }
    localStorage.setItem('firstthumb', escape(JSON.stringify(globalData, null, '\t')));
}

function splash_clicked() {
    var now = Date.now();
    if (now - splash_time > 10000) {
        splash_time = now;
        splash_cnt = 0;
    } else if (++splash_cnt > 8) {
        splash_cnt = 0;
        globalData.TestSite = !globalData.TestSite;
        save_global_data();
        load_global_data();
    }
}

function resize() {
    //$('#debugTable').height(window.innerHeight - $('#debugTable').offset().top);
    //$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
    //$('#imSummaryTable').height(window.innerHeight - $('#imSummaryTable').offset().top);
    //$('#imDetailTable').height(window.innerHeight - $('#imDetailTable').offset().top - 40);
}

function register_for_push_notifications() {
    if (window.cordova)
        window.plugins.pushNotification.register(successHandler, errorHandler, {
            senderID: notification_id,
            ecb: "onNotificationGCM"
        });
    else
        do_post('addUserDevice', {
            'deviceToken': 'None',
            'pushHandlerId': 3
        }, process_add_user_device);
}

function successHandler(result) {
    debugInfo.innerHTML += "pushReg OK<br>";
}

function errorHandler(error) {
    pushRegistrationTimeout *= 2;
    setTimeout(register_for_push_notifications(), pushRegistrationTimeout);
}

function onNotificationGCM(e) {
    switch (e.event) {
        case 'registered':
            if (e.regid.length > 0) {
                console.log("Regid " + e.regid);
                debugInfo.innerHTML += "RegId OK<br>";
                do_post('addUserDevice', {
                    'deviceToken': e.regid,
                    'pushHandlerId': 2
                }, process_add_user_device);
                //alert('registration id = '+e.regid);
            }
            break;

        case 'message':
            // this is the actual push notification. its format depends on the data model from the push server
            get_ims(); //alert('message = '+e.message+' msgcnt = '+e.msgcnt);
            break;

        case 'error':
            alert('GCM error = ' + e.msg);
            break;

        default:
            alert('An unknown GCM event has occurred');
            break;
    }
}

function process_add_user_device(response) {
    if (response.error != null) {
        // alert("addUserDevice: " + response.error.message);
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        }
    }
}

var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function(s) {
        return entityMap[s];
    });
}

function set_page(page) {

    $('body').css({
        'height': '100%'
    });
    if (page != 'Sign In' & page != 'Log Out' & page != 'Register') {
        $('#buttonset').show();
        $("#nav_list").trigger("click");
    }
    var pages = {
        'Sign In': 'signin',
        'Passenger': 'passenger',
        'Driver': 'driver',
        //'Buy-Sell Miles': 'buymiles',
        'Broadcast Message': 'broadcast_message',
        //'My History': 'recentjourneys',
        'Message Centre': 'messagecentre',
        'Settings': 'settings',
        'Debug': 'debug',
        'GPS': 'gps',
        'Terms & Conditions': 'termsandconditions',
        'Privacy Policy': 'privacypolicy',
        'Log Out': 'signout',
        'Start New Group': 'start_new_group',
        'Register': 'register',
        'Forgot': 'forgot',
        'How It Works': 'howitworks',
        'Become A Driver': 'becomedriver',
        'Become A Passenger': 'become_passenger',
        'Safety First': 'safetyfirst',
        'Fare Payment': 'farepayment',
        'App Guide': 'appguide',
        'About': 'aboutapp',
        'Test APP': 'tesapp'
    };

    if (active == 'driver' && page == 'Passenger')
        page = 'Driver';
    if (active == 'passenger' && page == 'Driver')
        page = 'Passenger';
    $('#group_name_headings_outer').hide();
    $('#menu_button').show();
    $('#back_button').hide();
    $('#refresh_button').show();
    im_detail_contact = '';
    /*for (var txt in pages) {
     if (txt == page)
     document.getElementById(pages[txt]).style.display = 'block';
     else
     document.getElementById(pages[txt]).style.display = 'none';
}*/
    $(".site-wrap").hide();
    if (page)
        $("#" + pages[page]).show();
    if (page == 'Forgot') {
        $('#buttonset').hide();
        $("#nav_list").trigger("click");
    }
    $('#header2').html(page);

    if (page == 'Log Out') {
        do_logout();
    } else if (page == 'Message Centre') {
        $('#messagecentredetail').hide();
        $('#messagecentresummary').show();
        $('#msgbackbtn').hide();


        render_im_list();
    } else if (page == 'Debug') {
        $('#debugTable').height(window.innerHeight - $('#debugTable').offset().top);
    } else if (page == 'Driver') {
        /*$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);*/
    } else if (page == 'Terms & Conditions') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();
        $('#webpage_name').attr('src', base_url + '/terms_and_conditions.php');
        //window.open(base_url + '/terms_and_conditions.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Register') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();
        $('#webpage_name').attr('src', base_url + '/register.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Start New Group') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();
        $('#webpage_name').attr('src', base_url + '/register.php');
        $("#nav_list").trigger("click");
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'App Guide') {

        $('body').css({
            'height': ''
        });

        $("#app_guide").show();
        // $('#webpage_name').contents().empty();
        // $( "#webpages").show();

        // $('#webpage_name').attr('src', base_url + '/privacy.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    }else if (page == 'About') {

        $('body').css({
            'height': ''
        });

        $("#aboutapp").show();
        // $('#webpage_name').contents().empty();
        // $( "#webpages").show();

        // $('#webpage_name').attr('src', base_url + '/privacy.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    }
    //App Guide
    else if (page == 'Privacy Policy') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();

        $('#webpage_name').attr('src', base_url + '/privacy.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Buy-Sell Miles') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();

        $('#webpage_name').attr('src', base_url + '/index.php');
        //window.open(base_url + '/index.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Broadcast Message') {
        $("#broadcast_message").show();
    } else if (page == 'My History') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();
        $('#webpage_name').attr('src', base_url + '/myhistory.php');
        //window.open(base_url + '/index.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'How It Works') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();

        $('#webpage_name').attr('src', base_url + '/how_it_work.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Become A Driver') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();

        $('#webpage_name').attr('src', base_url + '/become_driver.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Become A Passenger') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();

        $('#webpage_name').attr('src', base_url + '/become_passenger.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Safety First') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();

        $('#webpage_name').attr('src', base_url + '/safty_project.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    } else if (page == 'Fare Payment') {
        $('#webpage_name').contents().empty();
        $("#webpages").show();

        $('#webpage_name').attr('src', base_url + '/fare_paymetslist.php');
        //window.open(base_url + '/privacy.php', '_blank', 'location=yes');
        //set_page(cur_page);
        //return;
    }
    //pages_height();
    $("#header2").html(page);
    cur_page = page;

}

function forgot_password() {

    var userEmail = document.getElementById("email").value;
    if (userEmail == "") {
        alert("Enter Valid Email-ID");
    } else {
        do_post('forgotPassword', {
            'emailId': userEmail
        }, process_forgot_result);
        $('#refresh_button').show();
        show_refreshing(true);
    }
}

function process_forgot_result(response) {

    var email_Id = document.getElementById("email").value;

    //$('#refresh_button').hide();
    if (response.error !== null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        }
    } else {
        if (response.result.msg == 1) {
            document.getElementById('email').value = '';
            alert('Your password reset request has been accepted. An email has been sent containing instructions to complete your request.');
            set_page("Forgot");
        } else {
            alert('Mail sending failed.');
        }

        //   $('label[for="nav-trigger"]').hide();
        //$( "#nav-trigger" ).trigger( "click" );

    }
}

function register_button_pressed() {
    window.open(base_url + '/register.php', '_blank', 'location=yes,clearcache=yes,clearsessioncache=yes');
}

function signin_button_pressed() { //navigator.app.loadUrl('https://www.sandbox.paypal.com/webscr&cmd=_ap-payment&paykey=AP-2JH32822KV7580902', '_blank');
    // window.open('https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?paykey=AP-01J42392LG6777149&expType=mini', '_blank', 'location=yes,clearcache=yes,clearsessioncache=yes');
    //var url_clocse=base_url + '/samples/Pay.php';

    reloadPayment();

    document.getElementById("driverPassengers").innerHTML = '';
    // Try to log in
    IdleCounter = 0;
    globalData.username = document.getElementById("username").value;
    globalData.password = document.getElementById("password").value;

    do_post('login', {
        'userId': globalData.username,
        'pwd': globalData.password
    }, process_login_result);
    $('#refresh_button').show();
    show_refreshing(true);

    //  var ref = navigator.app.loadUrl('https://firsthumb.com/samples/Pay.php', '_blank');
    //ref.close();
}

function load_cookie() {
    var save = false;

    if (localStorage.getItem(globalData.username) === null) {
        //alert("No localstorage for " + globalData.username);
        cookieData = new Object;
        cookieData.DataFormatVersion = DataFormatVersion;
        save = true;
    } else {
        var value = localStorage.getItem(globalData.username);
        try {
            cookieData = jQuery.parseJSON(unescape(value));
        } catch (e) {
            console.log("Failed to parse '" + unescape(value) + "'");
            cookieData = new Object;
            cookieData.DataFormatVersion = DataFormatVersion;
            save = true;
        }
    }
    if (cookieData == null || cookieData == undefined) {
        alert("No data!");
        cookieData = new Object;
        cookieData.DataFormatVersion = DataFormatVersion;
        save = true;
    }
    if (!cookieData.hasOwnProperty('DataFormatVersion') || cookieData.DataFormatVersion != DataFormatVersion) {
        cookieData = new Object;
        cookieData.DataFormatVersion = DataFormatVersion;
        save = true;
        alert("Data and settings deleted due to format change");
    }
    if (cookieData.hasOwnProperty('RefreshEnable')) {
        document.getElementById('RefreshEnable').checked = cookieData.RefreshEnable;
    } else {
        cookieData.RefreshEnable = false;
        save = true;
    }
    if (cookieData.hasOwnProperty('DebugEnable')) {
        document.getElementById('debugEnable').checked = cookieData.DebugEnable;
    } else {
        cookieData.DebugEnable = false;
        save = true;
    }
    if (!cookieData.hasOwnProperty('Groups')) {
        cookieData.Groups = [];
        save = true;
    }
    if (!cookieData.hasOwnProperty('IMs')) {
        cookieData.IMs = {};
        save = true;
    }
    if (!cookieData.hasOwnProperty('IMExpiryDays')) {
        cookieData.IMExpiryDays = 5;
        save = true;
    }
    document.getElementById('im_expiry_days').value = cookieData.IMExpiryDays;
    if (save) {
        save_cookie();
    }
}

function save_cookie() {
    var value = escape(JSON.stringify(cookieData, null, '\t'));
    console.log("Saving cookie: " + JSON.stringify(cookieData, null, ' '));
    localStorage.setItem(globalData.username, value);
}

function refresh_enable_clicked() {
    cookieData.RefreshEnable = document.getElementById('RefreshEnable').checked;
    save_cookie();
}

function debug_enable_clicked() {
    cookieData.DebugEnable = document.getElementById('debugEnable').checked;
    save_cookie();
}

function myStopFunction() {
    clearInterval(myVar);
}

function reloadPayment() {
    do_post('getPaymentStatus', {}, process_getPaymentStatus);
}

function process_getPaymentStatus(response) {
    console.log(response);
    console.log('!!!!!!!!');

    if (typeof response.result.payment_mode != "undefined") {
        if (response.result.payment_mode != 0) {
            var user_pass_id = response.result.user_list_id;
            var driver_amount = response.result.driver_amount_pay;
            var journey_id = response.result.journey_id;
            if (response.result.payment_mode == 1 || response.result.payment_mode == 2) {
                //var ref =window.open(base_url + '/samples/Pay.php?user_id='+user_pass_id, '_self', 'location=yes,clearcache=yes,clearsessioncache=yes');   

                //var ref = window.open(base_url + '/samples/Pay.php?user_id=' + user_pass_id, '_blank', 'location=yes');

                /*ref.addEventListener('loadstop', function (event) {
                    myStopFunction();
                });*/
                /* ref.addEventListener('exit', function (event) {
                     myVar = setInterval(function () {
                         reloadPayment();   
                     }, 2500);
                 });*/

                 end_reload = false;


                 // $(function(){

                 $.ajax({
                      type: 'GET',
                      url: 'https://firsthumb.com/upgrade/endride.php',
                      dataType: "json",
                      data: {'id' : user_pass_id}, 
                      success: function(json){
                   
                        $('#card-payment-total-miles-traveled').html(json.miles);
                        $('#card-payment-free-miles-used').html(json.free_miles);
                        $('#card-payment-payment-needed-miles').html(json.paid_miles);
                        $('#card-payment-total-amount').html(json.amount);
 
                        if(json.card){
                          $('#pay-saved-btn').html('Pay with ' + json.card);
                          $('#pay-saved-btn').show();
                        }

                        if(json.stripe){
                          $('#card-details').show();
                          $('#pay-btn-block').show();
                        }else{
                          $('#no-card-details').show();
                        }
 
                        $('#pay-saved-btn').on('click',function(){

                                $(this).hide();
                                $('#pay-btn').val('Processing...').prop('disabled', true);

                                var card_number = $('#card-number');
                                var card_expiry_month = $('#card-expiry-month');
                                var card_expiry_year = $('#card-expiry-year');
                                var card_cvv = $('#card-cvv');
                                var card_name = $('#card-name');

                                card_number.val('');
                                card_expiry_month.val('');
                                card_expiry_year.val('');
                                card_cvv.val('');
                                card_name.val('');

                                $.ajax({
                                  type: 'POST',
                                  url: 'https://firsthumb.com/upgrade/stripepay.php',
                                  dataType: "json",
                                  data: {'user': user_pass_id, 'reuse' : 1}, 
                                  success: function(json){

                                    $('#payment-staus p').html(json.message);
                                    $('#card-details').hide();
                                    $('#pay-btn-block').hide();
                                    $('#payment-staus').show();
                                    $('#gohome-btn-block').show();
                                  },
                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    console.log(XMLHttpRequest);
                                    $('#pay-btn').val('Pay Now').prop('disabled', false);
                                  }
                                });

                                return false;
                        });


                        $('#pay-btn').on('click',function(){

                          $('#pay-btn').val('Processing...').prop('disabled', true);

                          var card_number = $('#card-number');
                          var card_expiry_month = $('#card-expiry-month');
                          var card_expiry_year = $('#card-expiry-year');
                          var card_cvv = $('#card-cvv');
                          var card_name = $('#card-name');
                          var card_name = $('#card-name');
                          var card_save = 0;  

                          if ($('#card-save').is(':checked')) {
                            card_save = 1;  
                          }

                          var validate = true;

                          if(card_number.val()){
                            card_number.css({borderColor:'#b9b9b9'});
                          }else{
                            card_number.css({borderColor:'#fd2525'});
                            validate = false;
                          }      

                          if(card_expiry_month.val()){
                            card_expiry_month.css({borderColor:'#b9b9b9'});
                          }else{
                            card_expiry_month.css({borderColor:'#fd2525'});
                            validate = false;
                          }  

                          if(card_expiry_year.val()){
                            card_expiry_year.css({borderColor:'#b9b9b9'});
                          }else{
                            card_expiry_year.css({borderColor:'#fd2525'});
                            validate = false;
                          }  

                          if(card_cvv.val()){
                            card_cvv.css({borderColor:'#b9b9b9'});
                          }else{
                            card_cvv.css({borderColor:'#fd2525'});
                            validate = false;
                          }  

                          if(card_name.val()){
                            card_name.css({borderColor:'#b9b9b9'});
                          }else{
                            card_name.css({borderColor:'#fd2525'});
                            validate = false;
                          }  

                          if(validate){

                            Stripe.setPublishableKey('pk_test_IrOaYg1Woc5DWK1T2h3dPFcU');
                            Stripe.card.createToken({
                              number: card_number.val(),
                              cvc: card_cvv.val(),
                              exp_month: card_expiry_month.val(),
                              exp_year: card_expiry_year.val()
                            }, stripeResponseHandler);

                            function stripeResponseHandler(status, response) {

                                $.ajax({
                                  type: 'POST',
                                  url: 'https://firsthumb.com/upgrade/stripepay.php',
                                  dataType: "json",
                                  data: {'user': user_pass_id, 'id' : response.id, 'card_save': card_save, 'card_name' : card_name.val() + ' xxxx' + card_number.val().substr(card_number.val().length - 4) }, 
                                  success: function(json){
                                    card_number.val('');
                                    card_expiry_month.val('');
                                    card_expiry_year.val('');
                                    card_cvv.val('');
                                    card_name.val('');

                                    $('#payment-staus p').html(json.message);
                                    $('#card-details').hide();
                                    $('#pay-btn-block').hide();
                                    $('#payment-staus').show();
                                    $('#gohome-btn-block').show();
                                  },
                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    console.log(XMLHttpRequest);
                                    $('#pay-btn').val('Pay Now').prop('disabled', false);
                                  }
                                });

                            }
                          }

                        });

                        $('#gohome-btn').on('click',function(){
                           $('#card-payment').hide();
                           end_reload = true;
                        });

                      },
                      error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.log(XMLHttpRequest);
                        $('#pay-btn').val('Pay Now').prop('disabled', false);
                      }
                    });
         
                    $('#card-payment').show();
                    $('#payment-staus').hide();
                    $('#gohome-btn-block').hide();
                    $('#pay-btn').val('Pay Now').prop('disabled', false);

                 // });

 


            } else if (response.result.payment_mode == 3) {
                do_post('getPayoutCheckout', {
                    driver_amount: driver_amount,
                    journey_id: journey_id
                }, process_getPayoutCheckout);
            } else if (response.result.payment_mode == 0) {

            }
        }
    }
    //    window.open(base_url + '/samples/Pay.php?user_id='+user_pass_id, '_self', 'location=yes,clearcache=yes,clearsessioncache=yes');   

}

function process_getPayoutCheckout(response) {
    if (response.result.check == 1)
        alert('Your journey of ' + response.result.paid_miles + ' miles has been paid with your Free Miles. Your Free Miles balance is ' + response.result.sum_miles + '.');

}

function process_login_result(response) {

    if (!(window.cordova || save_signin_in_browser)) {
        document.getElementById("password").value = "";
    }
    $('#refresh_button').hide();
    if (response.error !== null) {

        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        }
    } else {
        load_cookie();
        msgTable.innerHTML = "";
        msgTablep.innerHTML = "";
        sessionId = response.result.sessionId;
        signedIn = 1;
        // Make it look like a idle passenger until we know better
        active = '';
        document.getElementById("passengerActivePanel").style.display = "none";
        $('#passenger_credit').show().text('???');
        $('#driver_credit').show().text('???');
        set_page("Passenger");
        $('#buttonset').show();
        $("#nav_list").trigger("click");

        get_status();
        get_messages();
        get_messages_passenger();
        get_group_list();
        get_ims();

        //reloadPayment();
        //  $("#ff").trigger("click");
        //set_page('Test APP');
        //  $('#webpage_name').contents().empty();
        //   $("#webpages").show();
        //  $('#webpage_name').attr('src', base_url + '/samples/Pay.php');
        //register_for_push_notifications();
        window.plugins.powerManagement = new PowerManagement();
        save_global_data();
        setInterval(function() {
            get_messages();
            get_messages_passenger();


            $(".milesupdate").each(function() {

                if ($(this).val() != '') {
                    request_stop = $(this).val();
                    return false;
                }
            });
            if (request_stop == 0)
                get_status();
            get_status_passangershide();
        }, 10000);
    }
}

function do_logout() {
    set_page('Sign In');
    //$('#menu_button').hide();
    $('#refresh_button').hide();
    do_post('logout', {}, process_logout);
    $('#buttonset').hide();
    $("#nav_list").trigger("click");
    $('#passengerPassphrase').show().text('');
    msgTable.innerHTML = "";
    msgTablep.innerHTML = "";
    document.getElementById("driverPassengers").innerHTML = '';
    sessionId = "";
    active = '';
    LastMsgIdReceived = 0;
    signedIn = 0;
}

function process_logout(response) {
    document.getElementById("postcredits").innerHTML = '';
}

function refresh() {
    if (signedIn == 1) {
        if (IdleTimeoutInSeconds > 0 && ++IdleCounter > IdleTimeoutInSeconds) {
            do_logout();
        } else if (++RefreshCounter > RefreshPeriodInSeconds && RefreshEnable.checked == true) {
            RefreshCounter = 0;
            get_status();
            get_messages();
            get_messages_passenger();
            get_group_list();
            get_ims();
        }
    }
}

function refresh_button_pressed() {
    IdleCounter = 0;
    if (signedIn == 1) {
        get_status();
        get_messages();
        get_messages_passenger();
        get_group_list();
        get_ims();
        show_refreshing(true);
    }
}

function get_status_passangershide() {
    do_post('passengerHideButtonstatus', {
        width: '140',
        height: '140'
    }, process_status_passangershide);
}

function process_status_passangershide(response) {
    // alert(JSON.stringify(response));
    if (response.error != null) {
        // alert("passengerHideButtonstatus: " + response.error.message);
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        }
        //        $('#buttonset').hide();
        //    $("#nav_list").trigger("click");
        //do_logout();    commented by Saneesh
    } else if (signedIn == 0) {

    } else {

        if (response.result.hidebuttons == 0 && response.result.hidebuttonsP == 0) {
            $('#response_msg').hide();
            $('#Accept_Ride').show();
            $('#map_box_list').show();
            $('#Decline_Ride').show();
            $('#endridestatus').hide();

        } else if (response.result.hidebuttons == 1 && response.result.hidebuttonsP == 1) {
            $('#response_msg').hide();
            $('#Accept_Ride').hide();
            $('#Decline_Ride').hide();
            $('#endridestatus').show();
        } else if (response.result.hidebuttons == 1) {
            document.getElementById("response_msg").innerHTML = '"Accept" to start ride'; // "Accept's to start ride";
            $('#response_msg').show();
            $('#endridestatus').hide();

        } else if (response.result.hidebuttonsP == 1) {
            $('#response_msg').show();
            document.getElementById("response_msg").innerHTML = "Awaiting driver's acceptance";
            $('#endridestatus').hide();
        }
    }
}

function get_status() {
    do_post('status', {
        width: '140',
        height: '140'
    }, process_status);
}

function process_status(response) {
    // alert(JSON.stringify(response));
    var existind_id = [];
    if (response.error != null) {
        // alert("GetStatus: " + response.error.message);
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        }
        //        $('#buttonset').hide();
        //    $("#nav_list").trigger("click");
        //do_logout();    commented by Saneesh
    } else if (signedIn == 0) {

    } else {
        $('[id^=journey_id]').each(function() {
            existind_id.push($(this).attr('id'));
        });
        //if(response.result.credit>0){
        $('#passenger_credit').show().text(response.result.credit);
        //}else{
        //$('#passenger_credit').show().text("0");
        // }
        $('#driver_credit').show().text(response.result.credit);
        if (response.result.passengers == undefined && response.result.driver == undefined) {
            active = '';
        }
        if (response.result.passengers == undefined) {
            document.getElementById("driverActivePanel").style.display = "none";
            /*$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);*/
        } else {
            if (cur_page == 'Passenger') {
                $("#nav_list").trigger("click");
                set_page('Driver');
            }
            active = 'driver';
            document.getElementById("driverActivePanel").style.display = "block";
            if (response.result.passengers.length == 1) {
                //document.getElementById("driverPassengers").innerHTML = "Your passenger is ";
            } else {
                //document.getElementById("driverPassengers").innerHTML = "Your passengers are ";
            }
            // edited by joyal start
            //var passanger_names=new Array();


            var strHtml = "";
            // document.getElementById("driverPassengers").innerHTML = '';
            for (var key in response.result.ratingpassenger) {
                var journey_id = response.result.ratingpassenger[key].journey_id;
                var passenger_id = response.result.ratingpassenger[key].passenger_id;
                var imgPass = "img/dummy_staff.gif";
                var pass_name = "";
                var buttons = '';
                var passenger_request_status = 0;
                var driver_request_status = 0;
                if (response.result.ratingpassenger[key].photo != '') {
                    imgPass = response.result.ratingpassenger[key].photo;
                }

                if (response.result.ratingpassenger[key].passenger_name != '') {
                    var pss_name = '"' + response.result.ratingpassenger[key].passenger_name + '"';
                    pass_name = "<div style='font-weight:bold;font-size:15px; padding-bottom:10px;'>Your Passenger's name is " + pss_name + "</div>";
                }
                if (response.result.ratingpassenger[key].pass_phrase != null) {
                    pass_name += '<div style="font-weight:bold;font-size:15px; padding-bottom:10px;"> The pass phrase is "' + response.result.ratingpassenger[key].pass_phrase + '"</div>';
                }


                if (typeof(response.result.ratingpassenger[key].displaybuttons.driver_request_status) == 'undefined' && typeof(response.result.ratingpassenger[key].displaybuttons.passenger_request_status) == 'undefined') {

                    buttons = '<div id="replacerespone' + journey_id + '"><div style="color:red;" id="response_msg_' + journey_id + '"></div><input name="accept_passenger" type="button" value="Accept Ride" class="button" id="Accept_Ride_' + journey_id + '" onclick="driver_request(1,' + journey_id + ',' + passenger_id + ')"/><br><br><input name="decline_passenger" type="button" value="Decline Ride" class="button" id="Decline Ride_journey_id" onclick="driver_request(2,' + journey_id + ',' + passenger_id + ')"/></div>';
                } else if (response.result.ratingpassenger[key].displaybuttons.driver_request_status == 1 && response.result.ratingpassenger[key].displaybuttons.passenger_request_status == 1) {
                    driver_request_status = 1;
                    passenger_request_status = 1;
                    buttons = '<div id="replacerespone' + journey_id + '"><input type="text" id="miles' + journey_id + '" name="miles" size="8" class="form-curve milesupdate"><br><br><input name="start_ride" type="button" value="End Ride" class="button" id="start_ride" onclick="driver_endride_button_press(' + journey_id + ',' + passenger_id + ')"/></div>';
                    if ($("#journey_id" + journey_id).length > 0) {
                        console.log($("#journey_id" + journey_id).hasClass("journey" + journey_id + '_' + driver_request_status + '_' + passenger_request_status) + '--' + driver_request_status + 'already exists chage-' + passenger_request_status + '-' + journey_id);
                        if ($("#journey_id" + journey_id).hasClass("journey" + journey_id + '_' + driver_request_status + '_' + passenger_request_status) == false) {

                            console.log($("#journey_id" + journey_id).length + '--already exists chage--' + journey_id);
                            var className = $('#journey_id' + journey_id).attr('class');
                            $('#journey_id' + journey_id).addClass('journey' + journey_id + '_1_1').removeClass(className);
                            $("#journey_id" + journey_id).html('<div style="width:37%;float:left;">' +
                                '<img src="' + base_url + '/' + imgPass + '" width="100" height="100" class="imgpass" /></div>' +
                                '<div style="float:left; width:60%; text-align:left;">' +
                                '<div class="drivpass">' + pass_name + '</div>' + buttons + '</div><div style="clear:both;"></div><div class="under-line"></div>');


                        }
                    }
                    //if ($("#journey_id" + journey_id).hasClass("journey" + journey_id + '_' + driver_request_status + '_' + passenger_request_status) == false)
                    // {

                    //  }


                } else if (response.result.ratingpassenger[key].displaybuttons.driver_request_status == 1) {
                    driver_request_status = 1;
                    buttons = '<div id="replacerespone' + journey_id + '"><div style="color:red;" id="response_msg_' + journey_id + '">Awaiting passenger\'\s acceptance</div><br><br><input name="decline_passenger" type="button" value="Decline Ride" class="button" id="Decline Ride_journey_id" onclick="driver_request(2,' + journey_id + ',' + passenger_id + ')"/></div>';
                } else if (response.result.ratingpassenger[key].displaybuttons.passenger_request_status == 1) {
                    passenger_request_status = 1;
                    if ($("#journey_id" + journey_id).length > 0) {
                        document.getElementById("replacerespone" + journey_id).innerHTML = '"Accept" to start ride</div><input name="accept_passenger" type="button" value="Accept Ride" class="button" id="Accept_Ride_' + journey_id + '" onclick="driver_request(1,' + journey_id + ',' + passenger_id + ')"/><br><br><input name="decline_passenger" type="button" value="Decline Ride" class="button" id="Decline Ride_journey_id" onclick="driver_request(2,' + journey_id + ',' + passenger_id + ')"/>';
                    } else {

                        buttons = '<div id="replacerespone' + journey_id + '"><div style="color:red;" id="response_msg_' + journey_id + '">"Accept" to start ride</div><input name="accept_passenger" type="button" value="Accept Ride" class="button" id="Accept_Ride_' + journey_id + '" onclick="driver_request(1,' + journey_id + ',' + passenger_id + ')"/><br><br><input name="decline_passenger" type="button" value="Decline Ride" class="button" id="Decline Ride_journey_id" onclick="driver_request(2,' + journey_id + ',' + passenger_id + ')"/></div>';
                    }
                }
                console.log($("#journey_id" + journey_id).length + '----' + journey_id);
                if ($("#journey_id" + journey_id).length == 0) {
                    console.log($("#journey_id" + journey_id).length + '--testsaneesh--' + journey_id);
                    strHtml += '<div id="journey_id' + journey_id + '" class="journey' + journey_id + '_' + driver_request_status + '_' + passenger_request_status + '" style="padding-top:10px;width:100%;" ><div style="width:37%;float:left;">' +
                        '<img src="' + base_url + '/' + imgPass + '" width="100" height="100" class="imgpass" /></div>' +
                        '<div style="float:left; width:60%; text-align:left;">' +
                        '<div class="drivpass">' + pass_name + '</div>' + buttons + '</div><div style="clear:both;"></div><div class="under-line"></div></div>';


                }


                existind_id = $.grep(existind_id, function(value) {
                    return value != 'journey_id' + journey_id;
                });


            }
            $.each(existind_id, function(index, value) {

                $('#' + value).remove();
            });
            document.getElementById("driverPassengers").innerHTML += strHtml;

        }


        if (response.result.driver == undefined) {
            //document.getElementById("passengerActivePanel").style.display="none";
            //document.getElementById("passengerIdlePanel").style.display="block";
            $("#passengerActivePanel").hide();
            $("#map_box_list").hide();
            if (response.result.payment_mode == 1 || response.result.payment_mode == 2) { //Pay.php?payment_mode=1&owner=2&driver=2 user_list_id
                /*   var user_pass_id=response.result.user_list_id;
                 window.open(base_url + '/samples/Pay.php?user_id='+user_pass_id, '_self', 'location=yes,clearcache=yes,clearsessioncache=yes');   
             */
            } else {
                $("#passengerIdlePanel").show();
            }
        } else {
            if (cur_page == 'Driver') {
                set_page('Passenger');
            }
            active = 'passenger';
            // document.getElementById("passengerActivePanel").style.display="block";
            //document.getElementById("passengerIdlePanel").style.display="none";
            $("#passengerIdlePanel").hide();
            $("#passengerActivePanel").show();

            $("#map_box_list").show();
            document.getElementById("passengerDriver").innerHTML = "Your driver is " + response.result.driver;
            name_of_driver = response.result.driver;
            initMap("T");
        }
        var msg_text = 0;
        $('[id^=journey_id]').each(function() {
            msg_text = 1;
        });
        if (msg_text == 0) {
            $('#no_result').show();
        } else {
            $('#no_result').hide();
        }
    }
}

function get_group_list() {
    do_post('getGroupList', {}, process_group_list);
}

function process_group_list(response) {
    var MsgId = 0;

    if (response.error !== null) {
        if (!globalData.TestSite && response.error.code == -32601) {
            // "Method not found" - live site not yet supporting messaging
        } else {
            if (response.error.message == 'Server Not Responding') {
                messageClear();
            } else {
                $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
                clearMessageData();
            }
            //alert("GetGroupList: " + response.error.message);
            //            $('#buttonset').hide();
            //    $("#nav_list").trigger("click");
            //do_logout();    commented by Saneesh
        }
    } else if (signedIn == 0) {;
    } else {
        cookieData.Groups.sort();
        response.result.groups.sort();
        if (JSON.stringify(cookieData.Groups) !== JSON.stringify(response.result.groups)) {
            cookieData.Groups = response.result.groups;
            save_cookie();
        }
    }
}

function get_ims() {
    do_post('getIMs', {}, process_ims);
}

function process_ims(response) {
    if (response.error !== null) {
        if (!globalData.TestSite && response.error.code == -32601) {
            // "Method not found" - live site not yet supporting messaging
        } else {
            if (response.error.message == 'Server Not Responding') {
                messageClear();
            } else {
                $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
                clearMessageData();
            } // alert("GetIMs: " + response.error.message);
            //            $('#buttonset').hide();
            //    $("#nav_list").trigger("click");
            //do_logout();    commented by Saneesh
        }
    } else if (signedIn == 0) {;
    } else {
        // Add any IMs to our cookieData, then ack them all
        var lastId = 0;

        for (var id in response.result.msgs) {
            var msg = response.result.msgs[id];
            if (id > lastId)
                lastId = id;
            if (!cookieData.hasOwnProperty('IMs'))
                cookieData['IMs'] = new Array;
            if (!cookieData.IMs[msg.from])
                cookieData.IMs[msg.from] = {
                    'New': 1,
                    'Msgs': []
                };
            cookieData.IMs[msg.from].New = 1;
            cookieData.IMs[msg.from].Msgs.push({
                'Incoming': 1,
                'Time': msg.time,
                'Text': escapeHtml(msg.text)
            });
        }
        if (lastId > 0) {
            do_post('ackIMs', {
                'msgId': lastId
            }, process_ack_ims);
            save_cookie();
        }
        // Delete any old messages and threads that become empty as a result
        var d = new Date();
        var cutoff = d.getTime() / 1000 - cookieData.IMExpiryDays * 24 * 60 * 60;
        var filtered_ims = {};
        var save = false;
        for (var contact in cookieData.IMs) {
            var filtered_msgs = [];
            for (var index in cookieData.IMs[contact].Msgs) {
                if (cookieData.IMs[contact].Msgs[index].Time > cutoff) {
                    filtered_msgs.push(cookieData.IMs[contact].Msgs[index]);
                } else {
                    save = true; // We just deleted one
                }
            }
            if (filtered_msgs.length > 0) {
                filtered_ims[contact] = {
                    'New': cookieData.IMs[contact].New,
                    'Msgs': filtered_msgs
                };
            }
        }
        if (save) {
            cookieData.IMs = filtered_ims;
            save_cookie();
        }
        render_im_list();
        if (im_detail_contact != '') {
            im_render_detail(im_detail_group, im_detail_contact);
        }
    }
}

function process_ack_ims(response) {
    if (response.error !== null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } // alert("AckIMs: " + response.error.message);
        //        $('#buttonset').hide();
        //    $("#nav_list").trigger("click");
        //do_logout();    commented by Saneesh
    }
}

function render_im_list() {
    for (var contact in cookieData.IMs) {
        cookieData.IMs[contact].Msgs.sort(function(a, b) {
            return (b.Time - a.Time);
        });
    }

    var contacts = [];
    for (var key in cookieData.IMs)
        contacts.push(key);
    contacts.sort(function(a, b) {
        return cookieData.IMs[b].Msgs[0].Time - cookieData.IMs[a].Msgs[0].Time;
    });
    cookieData.Groups.sort();
    var new_content = '<div><b>Groups</b></div><ul class="menu-box-lists">';

    grouparrays = cookieData.Groups;

    $.each(grouparrays, function(idx, obj) {

        //new_content += '<div class="oldIMThread" onclick="im_render_detail(true, \'' + obj.id + '\')">' + obj.group_tag + '</div>';

        new_content += '<li style="font-weight:400;font-size:14px;"><div style="width:80%;float:left;" onclick="im_render_detail(true, \'' + obj.id + '\', \'' + obj.group_tag + '\')">' + obj.group_tag + '</div><img src="img/chevron-right.png" class="nav_list-left"/></li>';


    });
    new_content += '</ul>';
    /*  for (var index in cookieData.Groups) {
     new_content += '<div class="oldIMThread" onclick="im_render_detail(true, \'' + cookieData.Groups[id] + '\')">' + cookieData.Groups[group_tag] + '</div>';
     }
     new_content += '<div><b>Messages</b></div>';
     for (var index in contacts) {
     contact = contacts[index];
     var d = new Date(cookieData.IMs[contact].Msgs[0].Time * 1000);
     var dmy = d.getDate()+' '+monthNames[d.getMonth()]+' '+d.getFullYear();
     var m = d.getMinutes();
     if (m < 10)
     m = "0" + m;
     var hm = d.getHours()+':'+m;
     if (cookieData.IMs[contact].New == 1)
     state = 'newIMThread';
     else
     state = 'oldIMThread';
     new_content +=
     '<div class="' + state + '" onclick="im_render_detail(false, \'' + contact + '\')">' +
     '<span style="display:inline; float: right;">' + dmy + '</span>' +
     '<span> <span id="im_list_contact">' + contact + '</span></span>' +
     '<span style="display:inline; float: right;">' + hm + '</span>' +
     '<span> <span id="im_list_text">' + cookieData.IMs[contact].Msgs[0].Text + '</span></span>' +
     '</div>';
 }*/

    //$('#imSummaryTable').height(window.innerHeight - $('#imSummaryTable').offset().top);

    if (imSummaryTable.innerHTML != new_content) {

        $('#imSummaryTable').html(new_content);
        //imSummaryTable.innerHTML = new_content;

        $('#imSummaryTable').animate({
            scrollTop: 0
        }, "slow");
    }
}

function callAlGroups(group_id) {


    var params = {
        'groupList': group_id
    };

    do_post('getMsgList', params, process_getMsgList);

}

function showmsgback() {

    $('#group_name_headings_outer').hide();
    $('#menu_button').hide();
    $('#back_button').hide();
    $('#msgbackbtn').hide();

    $('#messagecentresummary').show();
    $('#messagecentredetail').hide();
    $('#header2').show().text("Message Centre");

}

function im_render_detail(is_group, name, group_name) {
    $('#menu_button').hide();
    $('#back_button').show();
    $('#msgbackbtn').show();
    if (im_detail_contact == '') {
        // First display of page, scroll to top
        $('#imDetailTable').scrollTop(0);
    }
    im_detail_contact = name;
    im_detail_group = is_group;
    $('#messagecentresummary').hide();
    $('#messagecentredetail').show();
    //  $('#header2').show().text(group_name);

    $('#group_name_headings_outer').show();
    $('#group_name_headings').show().text(group_name);
    var new_content = '';
    if (is_group) {

        var params = {
            'groupList': name
        };
        show_refreshing(true);
        do_post('getMsgList', params, process_getMsgList);



    } else if (cookieData.IMs.hasOwnProperty(name)) {
        var msgs = cookieData.IMs[name].Msgs;

        if (cookieData.IMs[name].New == 1) {
            cookieData.IMs[name].New = 0;
            save_cookie();
        }
        msgs.sort(function(a, b) {
            return a.Time - b.Time;
        });
        for (var index in msgs) {
            var d = new Date(msgs[index].Time * 1000);
            var m = d.getMinutes();
            if (m < 9)
                m = "0" + m;
            var dmyhm = d.getDate() + ' ' + monthNames[d.getMonth()] + ' ' + d.getFullYear() + ' ' + d.getHours() + ':' + m;
            new_content += '<center id="im_detail_date">' + dmyhm + '</center>';
            if (msgs[index].Incoming == 1)
                new_content += '<table width="100%"><tr><td class="incoming_im" width="80%">' + msgs[index].Text + '</td><td width="20%">&nbsp;</td></tr></table>';
            else
                new_content += '<table width="100%"><tr><td width="20%">&nbsp;</td><td class="outgoing_im" width="80%">' + msgs[index].Text + '</td></tr></table>';
        }


        //$('#imDetailTable').height(window.innerHeight - $('#imDetailTable').offset().top - 40);
        if (imDetailTable.innerHTML != new_content) {
            imDetailTable.innerHTML = new_content;
            $('#imDetailTable').stop().animate({
                scrollTop: $("#imDetailTable")[0].scrollHeight
            }, 800);
        }


    }

}


function process_getMsgList(response) {


    //<input style="text-align:left; padding: 3px 0px 15px 20px;" type="text" id="imSendText" style="width:60%"   >

    var GroupMsgs = response.result.GroupMsgs;
    var new_content = '';
    new_content += '<div  id="imSendField" style="text-align:left; line-height:25px;margin-bottom:18px;" > <div style="float:left;width:100%;"><div style="float:left;"><textarea class="form-control" cols="25" rows="2"  id="imSendText"></textarea></div><div style="float:left;"><button  style="text-align:center; padding:7px;" onclick="im_send_clicked()">Send</button></div></div></div><br><br>';
    $.each(GroupMsgs, function(idx, obj) {
        //  new_content += '<center id="im_detail_date">' + obj.created + '</center>';
        //  new_content += '<table width="100%"><tr><td class="incoming_im" width="80%">' + obj.text + '</td><td width="20%">&nbsp;</td></tr></table>';


        /*new_content +=   '<div class="' + state + '" onclick="im_render_detail(false, \'' + obj.email + '\')">' +
         '<span> <span id="im_list_contact">' + obj.email + '</span></span>' +
         '<span> <span id="im_list_text">' + obj.text + '</span></span>' +
         '</div>';*/

        if (obj.owner == 'notme') {

            // new_content +='<div class="curve-box" style="text-align:left; line-height:25px;"> <p><strong> '+obj.from+'</strong></p> <p>' + obj.text + ' </p> </div><p>&nbsp;</p>';


            new_content += '<div class="curve-box" style="text-align:left; line-height:25px;margin-top:18px;"  > <p><strong> ' + obj.from + '</strong></p><p>' + obj.text + '  </p></div>';
        } else {

            //new_content += '<div class="curve-box" style="text-align:right; line-height:25px;"> <p><strong>'+obj.from+'</strong></p> <p>' + obj.text + '</p>';
            new_content += '<div class="curve-box" style="text-align:right; line-height:25px;margin-top:18px;" > <p><strong> ' + obj.from + '</strong></p><p>' + obj.text + '  </p></div>';
        }


    });

    //new_content += '<div style="width:300px;display: inline-block;padding-bottom: 20px;padding-top: 20px;"   id="imSendField" >  <div style="float:left;width:60%"><input id="imSendText" style="text-align:left; padding: 3px 0px 15px 20px;" type="text"></div> <div style="float:left;width:30%"><button onclick="im_send_clicked()" style="text-align:center; padding:10px;">Send</button></div>  </div>';

    //$('#imDetailTable').height(window.innerHeight - $('#imDetailTable').offset().top - 40);

    $('#imDetailTable').html(new_content);

    $('#imDetailTable').stop().animate({
        scrollTop: $("#imDetailTable")[0].scrollHeight
    }, 800);
    show_refreshing(false);
}

function im_send_clicked() {
    var txt = document.getElementById('imSendText').value;
    var params;
    var d = new Date();

    document.getElementById('imSendText').value = '';
    if (txt != '') {

        if (im_detail_group) {
            params = {
                'emailList': [],
                'groupList': [im_detail_contact],
                'message': txt
            };
        } else {
            params = {
                'emailList': [im_detail_contact],
                'groupList': [],
                'message': txt
            };
            if (!cookieData.IMs.hasOwnProperty(im_detail_contact)) {
                cookieData.IMs[im_detail_contact] = {
                    'New': 0,
                    'Msgs': []
                };
            }
            cookieData.IMs[im_detail_contact].Msgs.push({
                'Incoming': 0,
                'Time': d.getTime() / 1000,
                'Text': escapeHtml(txt)
            });
            save_cookie();
            im_render_detail(im_detail_group, im_detail_contact);
        }

        do_post('sendIM', params, process_send_im);
        show_refreshing(true);
    }
}

function process_send_im(response) {
    if (response.error !== null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } // alert("sendIM: " + response.error.message);
        $('#buttonset').hide();
        $("#nav_list").trigger("click");
        //do_logout();
    }

    var params = {
        'groupList': im_detail_contact
    };

    do_post('getMsgList', params, process_getMsgList);

}

function signin_keypress(e) {
    var keyCode = e.keyCode || e.which;

    if (keyCode == 13)
        signin_button_pressed();
}

function do_post(type, params, handler) {

    var networkState = navigator.connection.type;
    if (WsRequests.Pending.hasOwnProperty(type) && WsRequests.Pending[type] > 0 && networkState != 'none') {
        console.log('Ignoring ' + type + ' request as one is pending');
        return;
    }
    if (networkState == 'none') {
        WsRequests.Pending[type] = 0;

    } else {
        WsRequests.Pending[type] = 1;

    }
    WsRequests.Queue.unshift({
        'type': type,
        'params': params,
        'handler': handler
    });
    if (!WsRequests.Active) {

        issue_next_ws_request();

    }
}

function issue_next_ws_request() {

    var networkState = navigator.connection.type;
    if (networkState == 'none') {
        //$('.net_issues').show();
        $('.net_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >Please check your internet connectivity</div></div>');
        startConnectionCheck();

        WsRequests.Queue.splice(0, WsRequests.Queue.length);
        WsRequests.Active = false;

        $("#overlay").remove();
        return false;
    } else {
        $('.net_issues').html('');
        var msg = new Object;

        if (WsRequests.Queue.length == 0) {
            WsRequests.Active = false;
            show_refreshing(false);
            return;
        }

        WsRequests.Active = true;
        req = WsRequests.Queue.pop();
        msg.jsonrpc = '2.0';
        msg.Id = ++WsMsgId;
        msg.method = req.type;
        if (req.type != 'login')
            req.params.sessionId = sessionId;
        msg.params = req.params;
        //debug(JSON.stringify(msg, null, '\t'));
        /* $.post(wsurl, 'q=' + JSON.stringify(msg, null, '\t'), function (response, textStatus, xhr) {
         //alert("Success: " + textStatus + "\nStatus: " + xhr.status + "\nstatusText: " + xhr.statusText);
         do_posted(req.type, req.params, req.handler, response)
         })
         .fail(function (xhr, textStatus, errorThrown) {
         //alert("Failed: \"" + req.type + "\"\nStatus: " + xhr.status + "\nStatusText: " + xhr.statusText);
         errTxt = xhr.statusText + "(" + xhr.status + ")";
         console.log(req.type + ' failed: ' + errTxt);
         WsRequests.Pending[req.type] = 0;
         req.handler({error: {code: 32000 - 99, message: errTxt}});
         issue_next_ws_request();
     });*/

        $.ajax({
            url: wsurl,
            data: 'q=' + JSON.stringify(msg, null, '\t'),
            type: "POST",
            error: function(xhr, textStatus, errorThrown) {
                //alert("Failed: \"" + req.type + "\"\nStatus: " + xhr.status + "\nStatusText: " + xhr.statusText);
                var networkState = navigator.connection.type;
                if (textStatus === "timeout") {
                    $('.net_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >Please check your internet connectivity</div></div>');
                    $("#overlay").remove();
                }
                errTxt = xhr.statusText + "(" + xhr.status + ")";
                if (xhr.status == 0)
                    errTxt = "Server Not Responding";
                console.log(req.type + ' failed: ' + errTxt);
                WsRequests.Pending[req.type] = 0;
                req.handler({
                    error: {
                        code: 32000 - 99,
                        message: errTxt
                    }
                });
                issue_next_ws_request();
            },
            success: function(result, status, xhr) {

                //alert("Success: " + textStatus + "\nStatus: " + xhr.status + "\nstatusText: " + xhr.statusText);
                do_posted(req.type, req.params, req.handler, result);
            },
            timeout: 60000
        });
    }
}

function do_posted(type, params, handler, response) {
    console.log("do_posted: " + type);
    //alert("do_posted: " + type);
    //console.log(JSON.stringify(response, null, '\t'));
    if (WsMsgId != response.id)
        console.log('WARNING: Got ' + type + ' response with id ' + response.id + ', expecting ' + WsMsgId);
    debug(JSON.stringify(response, null, '\t'));
    if (type != 'login' && response.error != null && response.error.code == -32014) {
        console.log(type + ' request failed, session expired, requeuing');
        WsRequests.Queue.push({
            'type': type,
            'params': params,
            'handler': handler
        });
        WsRequests.Queue.push({
            'type': 'login',
            'params': {
                'userId': globalData.username,
                'pwd': globalData.password
            },
            'handler': relogin_handler
        });
        WsRequests.Pending['login'] = 1;
    } else {
        console.log(type + ' completed');
        WsRequests.Pending[type] = 0;
        handler(response);
    }
    issue_next_ws_request();
}

function relogin_handler(response) {
    if (response.error != null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //   alert("login: " + response.error.message);
        console.log('Relogin attempt failed');
        while (WsRequests.Queue.length != 0) {
            req = WsRequests.Queue.pop();
            console.log('Failing ' + req.type + ' request');
            Ws.Pending[req.type] = 0;
            req.handler({
                error: {
                    code: 32000 - 14
                }
            });
        }
        do_logout();
    } else {
        sessionId = response.result.sessionId;
        console.log('Logged in ok');
    }
}

function send_im(emails, groups, text) {
    var params = {
        'emailList': emails,
        'groupList': groups,
        'message': text
    };

    do_post('sendIM', params, process_send_im2);
}

function process_send_im2(response) {
    if (response.error !== null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } // alert("sendIM: " + response.error.message);
        //        $('#buttonset').hide();
        //    $("#nav_list").trigger("click");
        //do_logout();    commented by Saneesh
    } else {}
}

function delete_messages_passenger(id) {
    IdleCounter = 0;
    if (confirm("Delete messages?") == true) {
        do_post('ackMessagesPassenger', {
            'msgId': id
        }, process_delete_messages_passenger);
    }
}

function process_delete_messages_passenger(response) {
    if (response.error != null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //alert(response.error.message);
    } else {
        get_messages_passenger();
    }
}

function get_messages_passenger() {
    do_post('getMessagesPassenger', {}, process_messages_passenger);
}

function process_messages_passenger(response) {
    var MsgIdp = 0;
    var response_ids = [];
    if (response.error !== null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } // alert("getMessagesPassenger: " + response.error.message);
        //        $('#buttonset').hide();
        //    $("#nav_list").trigger("click");
        //do_logout();    commented by Saneesh
    } else if (signedIn == 0) {

    } else {
        msgTablep.innerHTML = "";
        if (response.result.msgs) {
            $.each(response.result.msgs, function(id, value) {
                response_ids.unshift(parseInt(id));
            });
        }
        if (response_ids) {
            $.each(response_ids, function(k, id) {
                MsgIdp = id;
                var obj = [];
                obj = response.result.msgs[id];
                process_one_message_passenger(id, obj);
            });
        }
        //        if (response.result.msgs) {
        //            $.each(response.result.msgs, function (id, msgdata) {
        //                MsgId = id;
        //                process_one_message_passenger(id, msgdata);
        //            });
        //        }
    }
    if (MsgIdp != LastMsgIdReceivedp) {
        $('#msgTablep').stop().animate({
            scrollTop: $("#msgTablep")[0].scrollHeight
        }, 800);
        LastMsgIdReceivedp = MsgIdp;
    }
}

function process_one_message_passenger(id, msgdata) { //alert("msgtable");
    //console.log(JSON.stringify(msgdata));
    txt = '';
    if ("errorText" in msgdata)
        txt = msgdata.errorText;
    if (msgdata.type == 'startRide') {
        if ("driver" in msgdata) {
            if ("driver_request_status" in msgdata) {
                if (msgdata.driver_request_status == 2)
                    txt = 'Driver ' + msgdata.driver + ' has declined your ride request  (' + msgdata.formatDate + ')';
                if (msgdata.driver_request_status == 1 && msgdata.passenger_request_status_waiting == 0)
                    txt = 'Driver ' + msgdata.driver + ' is waiting for your acceptance  (' + msgdata.formatDate + ')';

            }
            if ("passenger_ride_label" in msgdata) {
                if (msgdata.driver_decline_journey == 0)
                    txt = 'Journey started with ' + msgdata.passenger_ride_label + '  (' + msgdata.formatDate + ')';
            }
        }
    } else if (msgdata.type == 'endRidepassenger') {

        if (msgdata.miles != 0) {
            // txt = 'Your passenger ' + msgdata.passenger + ' has completed their journey, you have been credited ';
            txt = 'You have travelled ' + msgdata.miles + ' miles with ' + msgdata.driver + '  (' + msgdata.formatDate + ')';
        } else {
            txt = 'Your journey with ' + msgdata.driver + ' was aborted, 0 miles travelled  (' + msgdata.formatDate + ')';
        }

        myVar = setInterval(function () {
            if(end_reload){
                reloadPayment();     
            }    
        }, 120000);

        // reloadPayment();    
    }
    /*else {
     
     if ("passenger_ride_label" in msgdata) {
     txt = 'Journey started with ' + msgdata.passenger_ride_label + '.';
     }
     if ("passenger_request_status" in msgdata) {
     if (msgdata.passenger_request_status == 1)
     txt = 'Passenger ' + msgdata.passenger + ' is waiting for your acceptance.';
     else
     txt = 'Passenger ' + msgdata.passenger + ' has declined the ride.';
     }
     if ("magic" in msgdata) {
     txt = 'Passenger '+msgdata.passenger + ' has requested drive, pass phrase is \'' + msgdata.magic + '\'.';
     }
     }
     } else if (msgdata.type == 'endRide') {
     if ("passenger" in msgdata) {
     if (msgdata.miles != 0) {
     // txt = 'Your passenger ' + msgdata.passenger + ' has completed their journey, you have been credited ';
     txt = 'Congratulations you have travelled ' + msgdata.miles + ' miles with ' + msgdata.passenger;
     } else {
     txt = 'Journey aborted 0 miles travelled with ' + msgdata.passenger;
     }
     
     } else {
     txt = 'End of journey, you have been charged';
     }
     //  txt += ' ' + msgdata.miles + ' miles.';
 } */
    else {
        // txt = 'Funny msg type ' + msgdata.type;
    }
    if (txt != '')
        msgTablep.innerHTML += "<p class='message-p' onclick=\"delete_messages_passenger(" + id + ")\">" + txt + "</p>";
}

function get_messages() {
    do_post('getMessages', {}, process_messages);
}

function process_messages(response) {
    var MsgId = 0;
    var response_ids = [];
    if (response.error !== null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } // alert("GetMessages: " + response.error.message);
        //        $('#buttonset').hide();
        //    $("#nav_list").trigger("click");
        //do_logout();    commented by Saneesh
    } else if (signedIn == 0) {;
    } else {
        msgTable.innerHTML = "";
        if (response.result.msgs) {
            $.each(response.result.msgs, function(id, value) {
                response_ids.unshift(parseInt(id));
            });
        }
        if (response_ids) {
            $.each(response_ids, function(k, id) {
                MsgId = id;
                var obj = [];
                obj = response.result.msgs[id];
                process_one_message(id, obj);
            });
        }
        //        if (response.result.msgs) {
        //            $.each(response.result.msgs, function (id, msgdata) {
        //                MsgId = id;
        //                process_one_message(id, msgdata);
        //            });
        //        }
    }
    if (MsgId != LastMsgIdReceived) {
        $('#msgTable').stop().animate({
            scrollTop: $("#msgTable")[0].scrollHeight
        }, 800);
        LastMsgIdReceived = MsgId;
    }
}

function process_one_message(id, msgdata) { //alert("msgtable");
    // alert(JSON.stringify(msgdata));
    txts = '';
    if ("errorText" in msgdata)
        txts = msgdata.errorText;
    else if (msgdata.type == 'startRide') {
        if ("driver" in msgdata) {
            if (typeof(msgdata.driver_request_status) == 'undefined' && typeof(msgdata.magic) != 'undefined')
                txts = 'Your driver is ' + msgdata.driver + ' and the pass phrase is \'' + msgdata.magic + '\'  (' + msgdata.formatDate + ')';
        } else {

            if ("passenger_ride_label" in msgdata) {
                if (msgdata.passenger_decline_journey == 0)
                    txts = 'Journey started with ' + msgdata.passenger_ride_label + '  (' + msgdata.formatDate + ')';
            }
            if ("passenger_request_status" in msgdata) {
                if (msgdata.passenger_request_status == 1 && msgdata.passenger_request_status_driver == 0)
                    txts = 'Passenger ' + msgdata.passenger + ' is waiting for your acceptance  (' + msgdata.formatDate + ')';
                else if (msgdata.passenger_request_status == 2)
                    txts = 'Passenger ' + msgdata.passenger + ' has declined the ride  (' + msgdata.formatDate + ')';
            }
            if ("magic" in msgdata) {
                txts = 'Passenger ' + msgdata.passenger + ' has requested drive, pass phrase is \'' + msgdata.magic + '\'  (' + msgdata.formatDate + ')';
            }
        }
    } else if (msgdata.type == 'endRide') {
        if ("passenger" in msgdata) {
            if (msgdata.miles != 0) {
                // txt = 'Your passenger ' + msgdata.passenger + ' has completed their journey, you have been credited ';
                txts = 'Congratulations you have travelled ' + msgdata.miles + ' miles with ' + msgdata.passenger + '  (' + msgdata.formatDate + ')';
            } else {
                txts = 'Your Journey with ' + msgdata.passenger + ' was aborted, 0 miles travelled  (' + msgdata.formatDate + ')';
            }

        } else {
            txts = 'End of journey, you have been charged  (' + msgdata.formatDate + ')';
        }
        //  txt += ' ' + msgdata.miles + ' miles.';

        reloadPayment();
    } else if (msgdata.type != 'endRidepassenger') {
        txts = 'Funny msg type ' + msgdata.type;
    }

    if (txts != '')
        msgTable.innerHTML += "<p class='message-p' onclick=\"delete_messages(" + id + ")\">" + txts + "</p>";
}

function delete_messages(id) {
    IdleCounter = 0;
    if (confirm("Delete messages?") == true) {
        do_post('ackMessages', {
            'msgId': id
        }, process_delete_messages);
    }
}

function process_delete_messages(response) {
    if (response.error != null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //  alert(response.error.message);
    } else {
        get_messages();
    }
}

function endride_button_press() {
    IdleCounter = 0;
    var miles = document.getElementById("miles").value;
    if (miles == '')
        miles = 0;
    var confirm_dialog = confirm("Are you sure?");
    if (confirm_dialog == true) {
        //  if (miles == "") {
        //      alert("You must enter the miles travelled");
        //   } else {
        navigator.geolocation.clearWatch(watch_id);
        navigator.geolocation.getCurrentPosition(
            function(position) {
                endride_post_location(true, position, miles);
            },
            function(error) {
                endride_post_location(false, error, miles);
            }, {
                maximumAge: 10000,
                timeout: 10000,
                enableHighAccuracy: true
            });
        show_refreshing(true);
        // }
    }

}

function driver_endride_button_press(journey_id, passenger_id) {
    IdleCounter = 0;
    var miles = '';
    if ($("#miles" + journey_id).length > 0)
        miles = document.getElementById("miles" + journey_id).value;
    if (miles == '')
        miles = 0;
    var confirm_dialog = confirm("Are you sure?");
    if (confirm_dialog == true) {
        //  if (miles == "") {
        //      alert("You must enter the miles travelled");
        //   } else {
        navigator.geolocation.clearWatch(watch_id);
        navigator.geolocation.getCurrentPosition(
            function(position) {
                driverendride_post_location(true, position, miles, journey_id, passenger_id);
            },
            function(error) {
                driverendride_post_location(false, error, miles, journey_id, passenger_id);
            }, {
                maximumAge: 10000,
                timeout: 10000,
                enableHighAccuracy: true
            });
        show_refreshing(true);
        // }
    }

}

function Passenger_requestendride_button_press() {
    IdleCounter = 0;
    var miles = document.getElementById("miles").value;
    if (miles == '')
        miles = 0;

    navigator.geolocation.clearWatch(watch_id);
    navigator.geolocation.getCurrentPosition(
        function(position) {
            endride_post_location(true, position, miles);
        },
        function(error) {
            endride_post_location(false, error, miles);
        }, {
            maximumAge: 10000,
            timeout: 10000,
            enableHighAccuracy: true
        });
    show_refreshing(true);
    // }


}

function endride_post_location(success, position, miles) {
    var loc;

    // if it failed or gave us a cached value more than 5 minutes old, report 0,0
    if (!success) {
        var error = position;
        debug_always('EndRide: GPS request failed, code: ' + error.code + ' / ' + 'message: ' + error.message);
        loc = {
            lat: 0,
            lng: 0,
            accuracy: 0
        };
    } else if ((Date.now() - position.timestamp) > (5 * 60 * 1000)) {
        debug_always("EndRide: GPS result was too old");
        loc = {
            lat: 0,
            lng: 0,
            accuracy: 0
        };
    } else {
        loc = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
            accuracy: position.coords.accuracy
        };
        initMap(loc);
        debug('endRide: ' + JSON.stringify(loc));
    }
    do_post('endRide', {
        miles: miles,
        location: loc,
        ratedby_passenger: 1
    }, process_endride);
    show_refreshing(true);
}

function driverendride_post_location(success, position, miles, journey_id, passenger_id) {
    var loc;

    // if it failed or gave us a cached value more than 5 minutes old, report 0,0
    if (!success) {
        var error = position;
        debug_always('EndRide: GPS request failed, code: ' + error.code + ' / ' + 'message: ' + error.message);
        loc = {
            lat: 0,
            lng: 0,
            accuracy: 0
        };
    } else if ((Date.now() - position.timestamp) > (5 * 60 * 1000)) {
        debug_always("EndRide: GPS result was too old");
        loc = {
            lat: 0,
            lng: 0,
            accuracy: 0
        };
    } else {
        loc = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
            accuracy: position.coords.accuracy
        };
        debug('driverendRide: ' + JSON.stringify(loc));
    }
    do_post('driverendRide', {
        miles: miles,
        location: loc,
        ratedby_passenger: 1,
        journey_id: journey_id,
        passenger_id: passenger_id
    }, process_endride);
    show_refreshing(true);
}

function driver_request(status, journey_id, passenger_id) {
    do_post('driverRequest', {
        status: status,
        journey_id: journey_id,
        passenger_id: passenger_id
    }, process_driver_request);
    if (status == 2)
        driver_endride_button_press(journey_id, passenger_id);
    show_refreshing(true);
}

function process_driver_request(response) {
    if (response.error != null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //alert("driverRequest: " + response.error.message);

    } else {

        if (response.result.replace_data == 0) {
            $('#Accept_Ride_' + response.result.journey_id).hide();
            document.getElementById("response_msg_" + response.result.journey_id).innerHTML = response.result.status_msg;
        } else if (response.result.replace_data == 1) {
            var className = $('#journey_id' + response.result.journey_id).attr('class');
            $('#journey_id' + response.result.journey_id).addClass('journey' + response.result.journey_id + '_1_1').removeClass(className);
            document.getElementById("replacerespone" + response.result.journey_id).innerHTML = '<input type="text" id="miles' + response.result.journey_id + '" name="miles" size="8" class="form-curve milesupdate"><br><br><input name="start_ride" type="button" value="End Ride" class="button" id="start_ride" onclick="driver_endride_button_press(' + response.result.journey_id + ',' + response.result.passenger_id + ')"/>';
        }
    }
}

function passanger_request(status) {
    do_post('passengerRequest', {
        status: status
    }, process_passanger_request);

    if (status == 2)
        Passenger_requestendride_button_press();
    show_refreshing(true);
}

function process_passanger_request(response) {
    if (response.error != null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //  alert("passengerRequest: " + response.error.message);

    } else {

        $('#Accept_Ride').hide();
        document.getElementById("response_msg").innerHTML = "Awaiting driver's acceptance";
    }
    show_refreshing(true);
}

function process_endride(response) { //alert(JSON.stringify(response));
    if (response.error != null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //  alert("connection error :" + response.error.message);
    } else {

        travelled_miles = document.getElementById("miles").value;
        document.getElementById("miles").value = "";
        var msg = '';
        if (travelled_miles == 0)
            msg = '<div style="color:red;">Journey aborted 0 miles travelled</div>';
        else
            msg = 'Congratulations you have travelled ' + travelled_miles + ' miles with "' + name_of_driver + '"</br>';
        document.getElementById("postcredits").innerHTML = msg;
        $('#passengerPassphrase').show().text('');
        $('#journey_id' + response.result.journey_id).remove();
        //function saneesh test list 
        var r_journey_id = response.result.journey_id;
        var p_miles = response.result.miles;
        //if (response.result.driverEndride == 2) {

            console.log('iiiiiiiiii');
            
            reloadPayment();
            do_post('paymentStatus', {
                pd_journey_id: r_journey_id,
                miles: p_miles
            }, process_paymentStatus);
        //}
        if (travelled_miles == 0)
            get_status();
        //
    }
    window.plugins.powerManagement.release(function() {}, function() {
        debug_always("Failed to release WakeLock");
    });
    show_refreshing(true);
}

function process_paymentStatus(response) { //alert(JSON.stringify(response));
    if (response.error != null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //  alert("connection error :" + response.error.message);
    } else {


        get_status();

    }
    window.plugins.powerManagement.release(function() {}, function() {
        debug_always("Failed to release WakeLock");
    });
    show_refreshing(true);
}

function startride_button_press() {
    //reloadPayment();
    document.getElementById("postcredits").innerHTML = '';
    $('#Accept_Ride').show();
    $('#map_box_list').show();
    $('#endridestatus').hide();
    if (document.getElementById("response_msg").length > 0)
        document.getElementById("response_msg").innerHTML = "";
    IdleCounter = 0;
    var registration = document.getElementById("registration").value;
    if (registration == "") {
        alert("You must enter the car registration");
    } else {
        window.plugins.powerManagement.dim(function() {}, function() {
            debug_always("Failed to grab WakeLock");
        });

        navigator.geolocation.getCurrentPosition(
            function(position) {
                //  alert('Intial request');
                //alert(JSON.stringify(position));
                console.log("geo test ------");
                startride_post_location(true, position, registration);
            },
            function(error) { //alert(JSON.stringify(error));
                startride_post_location(false, error, registration);
            }, {
                maximumAge: 10000,
                timeout: 10000,
                enableHighAccuracy: true
            });
        show_refreshing(true);
    }
}

function startride_post_location(success, position, registration) {
    var loc;
    console.log("geo test ++++pppp++");
    // if it failed or gave us a cached value more than 5 minutes old, report 0,0
    if (!success) {
        var error = position;
        lastreport_timestamp = 0;
        console.log("StartRide: GPS request failed");
        debug_always('StartRide: GPS request failed, code: ' + error.code + ' / ' + 'message: ' + error.message);
        loc = {
            lat: 0,
            lng: 0,
            accuracy: 0
        };
    } else if ((Date.now() - position.timestamp) > (5 * 60 * 1000)) {
        console.log("StartRide: GPS result was too old");
        debug_always("StartRide: GPS result was too old");
        loc = {
            lat: 0,
            lng: 0,
            accuracy: 0
        };
    } else {
        console.log("StartRide: GPS result was too old ------");
        lastreport_timestamp = position.timestamp;
        data = [];
        loc = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
            accuracy: position.coords.accuracy
        };
        initMap(loc);
        debug('startRide: ' + JSON.stringify(loc));
    }
    do_post('startRide', {
        regno: registration,
        location: loc,
        width: '140',
        height: '140'
    }, process_startride);

    // Start tracking the User
    watch_id = navigator.geolocation.watchPosition(
        // Success
        function(position) { //alert('watch'+position.timestamp+'===AAAAA=='+lastreport_timestamp );
            if (position.timestamp - lastreport_timestamp >= 60000) {
                //alert('watch'+position.timestamp+'====='+lastreport_timestamp );
                lastreport_timestamp = position.timestamp;
                loc = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    accuracy: position.coords.accuracy
                }; //joyal
                initMap(loc); //joyal
                do_post('updateLocation', {
                        'location': {
                            'lat': position.coords.latitude,
                            'lng': position.coords.longitude,
                            'accuracy': position.coords.accuracy
                        }
                    },
                    function(response) {
                        if (response.error != null)
                            debug_always("updateLocation: " + response.error.message);
                    }
                );
            }
        },
        // Error
        function(error) {
            debug_always('UpdateLocation: GPS request failed, code: ' + error.code + ' / ' + 'message: ' + error.message);
        },
        // Settings
        {
            enableHighAccuracy: true,
            maximumAge: 10000,
            timeout: 10000
        }
    );
    show_refreshing(true);
}

function process_startride(response) {
    //alert(JSON.stringify(response));

    if (response.error != null) {
        if (response.error.message == 'Registration number not in database') {
            alert(response.error.message);
        } else {
            if (response.error.message == 'Server Not Responding') {
                messageClear();
            } else {
                $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
                clearMessageData();
            }
        } // alert("connection error :" + response.error.message);
    } else {
        document.getElementById("registration").value = "";
        //alert("before ststus");
        get_status();
        //alert("After ststus");
        // $('#passengerPassphrase').show().text('The passphrase is "' + response.result.magic + '"');
        //display photo start
        var imgPass = "img/dummy_staff.gif";
        var imgPassDriver = "img/dummy_staff.gif";
        var pass_name = "";
        if (response.result.photoPass != '') {
            imgPass = response.result.photoPass;
        }
        if (response.result.DriverphotoPass != '') {
            imgPassDriver = response.result.DriverphotoPass;
        }
        if (response.result.magic != null) {
            pass_name += '<div> The passphrase is "' + response.result.magic + '"</div>';
        }
        var driver_first_name = response.result.driver_first_name;
        strHtml = '<div style="padding-top:10px;width:100%;" ><div style="width:37%;float:left;">' +
            '<img src="' + base_url + '/' + imgPassDriver + '" width="100" height="100" class="imgpass" /></div>' +
            '<div style="float:left; width:60%; text-align:left;">' +
            '<div class="drivpass"> The driver’s name is "' + driver_first_name + '"</br></br>' + pass_name + '</div></div></div><div style="clear:both;"></div><div style="color:#000; padding:5px 0px; font-size:14px; font-weight:300;">End Ride and do not accept a lift from a driver who’s photo or passphrase do not match</div></br>';
        //  alert(strHtml);

        document.getElementById("passengerphotoPassphrase").innerHTML = strHtml;
        show_refreshing(true);
        //display photo end
    }
}

function fmtdate(secs) {
    var d = new Date(secs * 1000);
    var dmy = d.getDate() + ' ' + monthNames[d.getMonth()] + ' ' + d.getFullYear();
    var m = d.getMinutes();
    if (m < 10)
        m = "0" + m;
    var hm = d.getHours() + ':' + m;

    return dmy + ' ' + hm;
}

function get_recent_journeys() {
    do_post('getRecentJourneys', {
        'maxNoOfJourneys': 30
    }, process_recent_journeys);
}

function process_recent_journeys(response) {
    if (response.error !== null) {
        if (response.error.message == 'Server Not Responding') {
            messageClear();
        } else {
            $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >' + response.error.message + '</div></div>');
            clearMessageData();
        } //alert("getRecentJourneys: " + response.error.message);
        //                        $('#buttonset').hide();
        //    $("#nav_list").trigger("click");
        //do_logout();    commented by Saneesh
    } else if (signedIn == 0) {;
    } else {
        response.result.journeys.sort(function(a, b) {
            return a.starttime - b.starttime;
        });
        content = '';
        for (var index in response.result.journeys) {
            journey = response.result.journeys[index];
            var sdate = fmtdate(journey.starttime);
            var edate = fmtdate(journey.endtime);
            content += '<table id="journey-table" width="100%"><tr><td width="40%" align="right">Driver</td><td width="40%">' +
                journey.driver_first_name + ' ' + journey.driver_last_name + '</td><td width="20%">&nbsp;</td></tr>' +
                '<tr><td align="right">Passenger</td><td>' + journey.passenger_first_name + ' ' + journey.passenger_last_name + '</td><td></td></tr>' +
                '<tr><td>Start</td><td>End</td><td align="right">Miles</td></tr>' +
                '<tr><td>' + sdate + '</td><td>' + edate + '</td><td align="right">' + journey.miles + '</td>' +
                '</table><hr>';
        }
        $('#JourneyTable').height(window.innerHeight - $('#JourneyTable').offset().top);
        JourneyTable.innerHTML = content;
    }
}

function cleardebug_button_pressed() {
    IdleCounter = 0;
    debugTable.innerHTML = "";
}

function debug(str) {
    if (debugEnable.checked == true) {
        debugTable.innerHTML += str + "<hr>";
    }
}

function debug_always(str) {
    debugTable.innerHTML += str + "<hr>";
}

function im_expiry_days_changed() {
    cookieData.IMExpiryDays = document.getElementById('im_expiry_days').value;
    save_cookie();
}

function clear_user_data_clicked() {
    if (confirm("Clear user data?")) {
        cookieData = new Object;
        cookieData.DataFormatVersion = DataFormatVersion;
        save_cookie();
        do_logout();
    }
}

function gps_wakelock() {
    window.plugins.powerManagement.dim(dimok, dimfail);
}

function dimok() {
    document.getElementById('gps_lockstat').innerHTML = 'Locked';
}

function dimfail() {
    document.getElementById('gps_lockstat').innerHTML = 'LockError';
}

function gps_release() {
    window.plugins.powerManagement.release(relok, relfail);
}

function relok() {
    document.getElementById('gps_lockstat').innerHTML = 'Unlocked';
}

function relfail() {
    document.getElementById('gps_lockstat').innerHTML = 'UnlockError';
}

function gps_test_clicked() {
    var element = document.getElementById('geolocation');
    if (watch_id != null) {
        navigator.geolocation.clearWatch(watch_id);
        document.getElementById('gps_runstat').innerHTML = 'Stopped';
        watch_id = null;
    } else {
        element.innerHTML = '';
        document.getElementById('gps_runstat').innerHTML = 'Running';
        // Start tracking the User
        lastreport_timestamp = 0;
        watch_id = navigator.geolocation.watchPosition(
            // Success
            onSuccessGet,
            // Error
            onErrorGet,
            // Settings
            {
                enableHighAccuracy: true,
                maximumAge: 10000,
                timeout: 10000
            }
        );
    }
}

// onSuccess Callback
// This method accepts a Position object, which contains the
// current GPS coordinates
//
var onSuccessGet1 = function(position) {
    alert('Latitude: ' + position.coords.latitude + '\n' +
        'Longitude: ' + position.coords.longitude + '\n' +
        'Altitude: ' + position.coords.altitude + '\n' +
        'Accuracy: ' + position.coords.accuracy + '\n' +
        'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
        'Heading: ' + position.coords.heading + '\n' +
        'Speed: ' + position.coords.speed + '\n' +
        'Timestamp: ' + position.timestamp + '\n');
};

// onError Callback receives a PositionError object
//
function onErrorGet(error) {
    debug_always('GPS request failed, code: ' + error.code + ' / ' +
        'message: ' + error.message);
}

function fmt_time(ms) {
    var d;
    if (ms == 0)
        d = new Date();
    else
        d = new Date(ms);

    var m = d.getMinutes();
    if (m <= 9)
        m = "0" + m;
    return d.getDate() + ' ' + monthNames[d.getMonth()] + ' ' + d.getFullYear() + ' ' + d.getHours() + ':' + m;
}

// onSuccess Callback
//   This method accepts a `Position` object, which contains
//   the current GPS coordinates
//
function onSuccessGet(position) {
    var element = document.getElementById('geolocation');
    element.innerHTML += fmt_time(0) + ' / ' +
        fmt_time(position.timestamp) + ' ' +
        position.coords.latitude + ',' +
        position.coords.longitude + ' (' +
        position.coords.accuracy + ')<br>';
    if (position.timestamp - lastreport_timestamp >= 60000) {
        lastreport_timestamp = position.timestamp;
        do_post('updateLocation', {
            'location': {
                'lat': position.coords.latitude,
                'lng': position.coords.longitude,
                'accuracy': position.coords.accuracy
            }
        }, process_update_location);
    }
}

// onError Callback receives a PositionError object
//
function onErrorWatch(error) {
    alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

function process_update_location(response) {
    if (response.error !== null) {
        debug_always("updateLocation: " + response.error.message);
    } else if (signedIn == 0) {;
    } else {}
}
window.addEventListener('load', function() {

    document.getElementById('tapping_logo').addEventListener('touchstart', function(e) {
        click_count++;
        if (click_count == 1) {
            invocation();


        }
        if (click_count == 12) {
            call_test_mode();
            clearTimeout(initial);
        }
    }, false)

}, false)

function invocation() {

    initial = window.setTimeout(
        function() {
            if (click_count == 12) {
                call_test_mode();
            }

            stopped();
            click_count = 0;
        }, 12000);
}

function stopped() {

    clearTimeout(initial);

}

function call_test_mode() {
    if (live_test_mode == 1) {
        alert('Test Mode');
        base_url = 'http://host.ipsrtraining.com/~phonegap/app/eco-gogo_valin-master/web';
        wsurl = base_url + '/ws/query.php?redirect=false';
        live_test_mode = 0;
        click_count = 0;
    } else {
        alert('live Mode');
        base_url = 'https://firsthumb.com';
        wsurl = base_url + '/ws/query.php?redirect=false';
        live_test_mode = 1;
        click_count = 0;
    }
}

function initMap(loc) {


    if (loc != "T") {
        data.push(loc);


    }

    if (data.length > 0) {
        if (data.length == 1) { //alert("map");
            var myLatLng = new google.maps.LatLng(data[0].lat, data[0].lng);
            var myOptions = {
                zoom: 15,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map"), myOptions);
        }

        var trackCoords = [];
        for (i = 0; i < data.length; i++) {
            new google.maps.Marker({
                position: {
                    lat: data[i].lat,
                    lng: data[i].lng
                },
                map: map
            });
            trackCoords.push(new google.maps.LatLng(data[i].lat, data[i].lng));
        }
        var trackPath = new google.maps.Polyline({
            path: trackCoords,
            strokeColor: "#FF0000",
            strokeOpacity: 1.0,
            strokeWeight: 2,
        });
        trackPath.setMap(map);
    }
}


function initMap1() {
    //don't remove

}

function messageClear() {
    if ($('.server_issues').text().indexOf('Server Not') == -1) {
        $('.server_issues').html('<div style="color: white; z-index: 1000000000;width: 100%;display:inline-block;background-color: #000;"><div style="text-align: center;" >Server Not Responding</div></div>');
    }
    setTimeout(function() {
        $('.server_issues').html('');
    }, 3000);
}

function clearMessageData() {
    setTimeout(function() {
        $('.server_issues').html('');
    }, 3000);
}