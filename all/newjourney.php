<?php

require_once('base.inc');
require_once('classes/common.inc');
require_once('classes/region.inc');
require_once('classes/schedule.inc');
require_once('classes/google.inc');
require_once('classes/yahoo.inc');
if (CConfig::RUN_IN_FB)
    require_once('classes/facebook.inc');

// Validate any current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->requireSession();
CWebSession::set('login', serialize($login));

// Get posted data
$op = CCommon::getRequestValue('op');
$from = CCommon::getRequestValue('from');
$to = CCommon::getRequestValue('to');
$refer = new CUrl(CCommon::getRequestValue('refer'));

$region = new CRegion('newjourney');

// XmlHttpRequest - Create schedule
if ($op == 'createSchedule') {
    $out = createSchedule($login, $region);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}

$schedule = new CSchedule;
$schedule->set(CSchedule::START_ADDRESS, $from);
$schedule->set(CSchedule::DESTINATION_ADDRESS, $to);

// Custom start data ( Glastonbury 2010 )
/*
  $schedule->set(CSchedule::ONEOFF_DATE, CCommon::tsToObj('20100623'));
  $schedule->set(CSchedule::JOURNEY_TYPE, CSchedule::JOURNEY_TYPE_ONEOFF);
 */

// Output HTML page
$rplc = array();
$rplc[1] = sprintf('%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
$rplc[2] = $region->msg(1001);
$rplc[3] = $region->msg(1002);
$rplc[37] = $region->msg(1003); // commented as part of making hearder and footer common
$rplc[4] = scriptLinks();
$rplc[5] = $region->msg(1004);
$rplc[6] = $region->msg(1005);
//$rplc[7] = $region->msg(1006);// commented as part of making hearder and footer common
$rplc[7] = script($login, $region, $schedule, $refer);
$rplc[8] = $region->msg(8, 'common');
//$rplc[9] = $region->msg(9, 'common');
$rplc[9] = script($login, $region, $schedule, $refer);
//$rplc[10] = scriptLinks();
$rplc[1111] = $region->msg(1101);
$rplc[11] = $region->msg(1100);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if ($isValidSession)
    $menu_header = file_get_contents('header_menus_login.php');
else
    $menu_header = file_get_contents('header_menus.php');

$rplc[777] = $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace('newjourney.htm', $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
    @include 'google_analytics.html';

/*
 * Create schedule from POSTed values
 * 
 * @param $login   CLogin object of current logged user
 * @param $region  A CRegion object
 */

function createSchedule($login, $region) {
    CCommon::dumpRequestVals();
    // Get request values
    $schedule = new CSchedule();
    CCommon::mapRequestValue($schedule, 'monday');
    CCommon::mapRequestValue($schedule, 'tuesday');
    CCommon::mapRequestValue($schedule, 'wednesday');
    CCommon::mapRequestValue($schedule, 'thursday');
    CCommon::mapRequestValue($schedule, 'friday');
    CCommon::mapRequestValue($schedule, 'saturday');
    CCommon::mapRequestValue($schedule, 'sunday');
    CCommon::mapRequestValue($schedule, 'monStartTime', CSchedule::MON_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'tueStartTime', CSchedule::TUE_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'wedStartTime', CSchedule::WED_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'thuStartTime', CSchedule::THU_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'friStartTime', CSchedule::FRI_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'satStartTime', CSchedule::SAT_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'sunStartTime', CSchedule::SUN_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'monReturnTime', CSchedule::MON_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'tueReturnTime', CSchedule::TUE_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'wedReturnTime', CSchedule::WED_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'thuReturnTime', CSchedule::THU_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'friReturnTime', CSchedule::FRI_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'satReturnTime', CSchedule::SAT_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'sunReturnTime', CSchedule::SUN_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'oneOffDate', CSchedule::ONEOFF_DATE, array('CCommon', 'jsonDateToInternal'));
    CCommon::mapRequestValue($schedule, 'oneOffTime', CSchedule::ONEOFF_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'scheduleType', CSchedule::SCHEDULE_TYPE);
    CCommon::mapRequestValue($schedule, 'journeyType', CSchedule::JOURNEY_TYPE);
    CCommon::mapRequestValue($schedule, 'requestType', CSchedule::REQUEST_TYPE);
    CCommon::mapRequestValue($schedule, 'standardStartTime', CSchedule::STANDARD_START_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'standardReturnTime', CSchedule::STANDARD_RETURN_TIME, array('CCommon', 'jsonTimeToInternal'));
    CCommon::mapRequestValue($schedule, 'destination', CSchedule::DESTINATION_ADDRESS);
    CCommon::mapRequestValue($schedule, 'destinationLat', CSchedule::DESTINATION_LAT);
    CCommon::mapRequestValue($schedule, 'destinationLng', CSchedule::DESTINATION_LNG);
    CCommon::mapRequestValue($schedule, 'startAddress', CSchedule::START_ADDRESS);
    CCommon::mapRequestValue($schedule, 'startAddressLat', CSchedule::START_LAT);
    CCommon::mapRequestValue($schedule, 'startAddressLng', CSchedule::START_LNG);
        CCommon::mapRequestValue($schedule, 'grouped_id', CSchedule::GROUPS_ID);
    if (CCommon::getRequestValue("grouped_id") == '')
        $schedule->set(CSchedule::GROUPS_ID, '0');


    // Determine selected days
    $tmp = 0;
    if ($schedule->get(CSchedule::SCHEDULE_TYPE) == CSchedule::SCHEDULE_TYPE_CUSTOM) {
        if ((int) $schedule->get('monday'))
            $tmp |= CSchedule::SCHEDULE_MON;
        if ((int) $schedule->get('tuesday'))
            $tmp |= CSchedule::SCHEDULE_TUE;
        if ((int) $schedule->get('wednesday'))
            $tmp |= CSchedule::SCHEDULE_WED;
        if ((int) $schedule->get('thursday'))
            $tmp |= CSchedule::SCHEDULE_THU;
        if ((int) $schedule->get('friday'))
            $tmp |= CSchedule::SCHEDULE_FRI;
        if ((int) $schedule->get('saturday'))
            $tmp |= CSchedule::SCHEDULE_SAT;
        if ((int) $schedule->get('sunday'))
            $tmp |= CSchedule::SCHEDULE_SUN;
    }
    $schedule->set(CSchedule::SCHEDULE_DAYS, $tmp);

    // Regular schedule?
    $userId = $login->userId();

    // Save the schedule
    $schedule->set(CSchedule::USER_ID, $userId);
    if ($schedule->save() == false)
        return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1016));

    return CCommon::makeErrorObj(0, ''); // Huh?
}

/*
 * Write <script> output
 * 
 * @param $login    A CLogin object
 * @param $region   A CRegion object
 * @param $schedule  A CSchedule object
 * @param $refer    A CUrl object
 * @return          Javascripts script statements
 */

function script($login, $region, $schedule, $refer) {
    $user = $login->getUser(true);
    $userId = (int) $login->userId();
    $msgs = $region->msgList();
    $scriptOut = array();
    $scriptOut[] = sprintf("ARRANGE_TRAVEL._msgList=new CMsgList('%s');", CCommon::toJson($msgs));
    $scriptOut[] = sprintf("ARRANGE_TRAVEL._schedule=%s;", $schedule->toJson());
    $scriptOut[] = sprintf("ARRANGE_TRAVEL._refer=new CUrl('%s');", CCommon::toJson($refer));
    return join("\n", $scriptOut);
}

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks() {
    $out = array();
    $out[] = CGoogle::scriptHtml();
    $out[] = CYahoo::scriptHtml(array('container', 'dom', 'calendar', 'menu', 'json', 'connection', 'autocomplete', 'button'));
    $out[] = '<script type="text/javascript" src="js/yahoo.js"></script>';
    $out[] = '<script type="text/javascript" src="js/common.js"></script>';
    $out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
    $out[] = '<script type="text/javascript" src="newjourney.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.tokeninput.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery_support_search.js"></script>';
    return join("\n", $out);
}

?>
