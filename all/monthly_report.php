<?php

date_default_timezone_set('Asia/Kolkata');

require_once('base.inc');
require_once("classes/root.inc");
require_once('classes/common.inc');
require_once('classes/group.inc');
require_once('classes/groupreport.inc');

mailReportGenarationCoSavings();
function mailReportGenarationCoSavings() {
   echo $first_date = date('Y-m-d', strtotime('first day of last month'));
    $last_date = date('Y-m-d', strtotime('last day of last month'));
    $user_id=81;
    $report = new CGroupReport();
   $rr= $report->loadAllGroup($user_id);print_r($rr);
}


/*
 * Report generation and send mail with database updation
 *  
 */

function reportGenaration() {
   
    $cgroup = new CGroup();
    $report = new CGroupReport();
    $monthname = Date('F', strtotime("last month"));
    $year = Date('Y', strtotime("last month"));
    $month = Date('m', strtotime("last month"));
    $grps = $cgroup->loadAll();

    $email = "";
    $table_headers = array("Serial No", "CO2 Savings", "Total Mileage");
    while ($groups = mysql_fetch_object($grps)) {
        $group_id = $groups->id;
        $group_tag = $groups->group_tag;
        $email = $groups->email;
        // $data = CDb::query("SELECT id,carbon_savings, total_mileage FROM groupreport WHERE month=$month and year=$year and group_id=$group_id");
        $data = $report->loadByMonth($group_id, $month, $year);
        if ($data != false && mysql_num_rows($data)!=0) {
            $filename = '/reports/' . $group_tag . '_' . $monthname . '_' . $year . '.pdf';
            $id = CCommon::generatePdf($table_headers, $data, $filename);
            if ($id != 0) {
                $params['attachment'] = getcwd() .$filename;
                $params['filename'] = $group_tag . '_' . $monthname . '_' . $year . '.pdf';
                $params['to'] = $email;
                $params['from'] = CConfig::INFO_EMAIL_NAME;
                $params['subject'] = "Monthly Report : $monthname, $year";
                $params['body'] = "<br>Hi, <br>Please find the attachment for your group's monthly report.<br><br><br>With regards,<br>FT";
                $status = CCommon::sendMail($params);
                if ($status == true) {
                    $report->updateStatus($id);
                }
            }
        }
    }
}

/*
 * Get Total Miles travelled in a month of a  group  by each user
 *  * @param $Month      Last Month.
 * @param  $year         Current Year/ in case of january year will be last year
 *  return MIles 
 */

function saveReport() {

    $month = Date('n', strtotime("last month"));
    $year = Date('Y', strtotime("last month"));
    $groupreport = new CGroupReport();
    $data = CGroupReport::totalMilesOfTheMonthByGroup($month, $year);
    $report = $groupreport->loadByMonth($data['group_Id'], $month, $year);
    if ($report !=false && mysql_num_rows($report)==0) {
    $groupReport = new CGroupReport();
    $groupReport->set(CGroupReport::GROUP_ID, $data['group_Id']);
    $groupReport->set(CGroupReport::CARBON_SAVINGS, carbonSavings($data['total_miles']));
    $groupReport->set(CGroupReport::TOTAL_MILEAGE, $data['total_miles']);
    $groupReport->set(CGroupReport::CREATED, 'now()');
    $groupReport->set(CGroupReport::MAIL_STATUS, '"n"');
    $groupReport->set(CGroupReport::MONTH, $month);
    $groupReport->set(CGroupReport::YEAR, $year);
    if ($groupReport->save() == false) {
        CLogging::error('Failed to save Miles');
    }
    }
    reportGenaration();
}

/*
 * Get total carbon savings by the group in a month
 * @param  $year         Miles
 *  return  carbons saving in kg 
 */

function carbonSavings($miles) {

    return (430 * $miles) / 1000;
}

?>