<?php

require_once('base.inc');
require_once('classes/user.inc');
require_once('classes/common.inc');
require_once('classes/region.inc');
require_once('classes/google.inc');
require_once('classes/yahoo.inc');
require_once('classes/inifile.inc');
if (CConfig::RUN_IN_FB)
    require_once('classes/facebook.inc');

// Validate any current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();
if ($op == 'search_group') {
    $str = CCommon::getRequestValue('q');
    $groupDetails = new CGroup();
    $out = $groupDetails->searchGroup($str);
    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}
// Output HTML page
$region = new CRegion("search");
$rplc = array();
if ($isValidSession)
    $rplc[3] = sprintf('|%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
else
    $rplc[3] = sprintf('|&nbsp;<a href="javascript:signIn()">%s</a>', $region->msg(3, "common"));
if ($op == "signin" && $isValidSession == false)
    $rplc[2] = sprintf('<span class="error">%s</span>', $region->msg(1001));
//$rplc[4] = $region->msg(1002);
$rplc[4] = scriptLinks();
$rplc[7] = script();
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1100);
$rplc[1111] = $region->msg(1111);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if ($isValidSession)
    $menu_header = file_get_contents('header_menus_login.php');
else
    $menu_header = file_get_contents('header_menus.php');

$rplc[777] = $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace("search.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
    @include 'google_analytics.html';

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks() {
    $out = array();
    $out[] = CGoogle::scriptHtml();
    $out[] = CYahoo::scriptHtml(array('json', 'button', 'autocomplete'));
    if (CConfig::RUN_IN_FB)
        $out[] = CFacebook::scriptHtml();
   // $out[] = sprintf("var _groups='%s';", '');
    $out[] = '<script src="js/common.js" type="text/javascript"></script>';
    $out[] = '<script src="js/xhr.js" type="text/javascript"></script>';
    $out[] = '<script src="js/xplatform.js" type="text/javascript"></script>';
    $out[] = '<script src="search.js" type="text/javascript"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.tokeninput.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery_support_search.js"></script>';
    return join("\n", $out);
}

/*
 * Print <script> element to output
 * 
 * @return HTML stream
 */

function script() {
    $out = array();
    if (CConfig::RUN_IN_FB)
        $out[] = CFacebook::script();
    // Load any custom destinatons
    $ini = new CIniFile();
    $destinations = array();
    if ($ini->load(sprintf('%s/search.ini', CConfig::CONTENT_DIR)))
        $destinations = array_values($ini->getSection('teams'));
    $out[] = sprintf("var _destinations='%s';", rawurlencode(CCommon::toJson($destinations)));
    return join('', $out);
}

?>
