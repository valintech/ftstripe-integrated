<?php

//ini_set('display_errors',1);
date_default_timezone_set('Asia/Kolkata');
require_once("classes/root.inc");
require_once('base.inc');
require_once('classes/user.inc');
require_once('classes/common.inc');
require_once('classes/region.inc');
require_once('classes/yahoo.inc');
require_once('classes/google.inc');
require_once('classes/captcha.inc');
require_once('classes/smsgw.inc');
require_once('classes/group.inc');
require_once('classes/usergroup.inc');
if (CConfig::RUN_IN_FB)
    require_once('classes/facebook.inc');

$op = CCommon::getRequestValue('op');
$refer = new CUrl(CCommon::getRequestValue('refer'));
// XmlHttpRequest:- Register
if ($op == 'owner_approve_mail') {
    $url_parameter = CCommon::getRequestValue('url_parameter');
    approveMail($url_parameter);
}

CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();

if (!($isValidSession)) {
    CCommon::redirect('login.php?' . $_SERVER['QUERY_STRING']);
} else {
    $cur_url = str_replace('approveownerparam=', '', urldecode($_SERVER['QUERY_STRING']));
    $encryotedurl = CCommon::mc_decrypt($cur_url, CConfig::ENCRYPTION_KEY);
    $encrypatedIds = explode(",", $encryotedurl);

    $groupId = $encrypatedIds['0'];
    $currentOwnerId = $encrypatedIds['1'];
    $userGropId = CGroup::loadIdByOwnerIdAndgroupId($groupId, $currentOwnerId);
    if ($userGropId == NULL)
        CCommon::redirect('register.php');
}

/*
 * Approve mail and update the staus accordingly
 *  
 */

function approveMail($url_parameter) {
    $approve_status = CCommon::getRequestValue('approve_status');
    $region = new CRegion('approve');
    $encryotedurl = CCommon::mc_decrypt($url_parameter, CConfig::ENCRYPTION_KEY);
    $encrypatedIds = explode(",", $encryotedurl);
    $groupId = $encrypatedIds['0'];
    $currentOwnerId = $encrypatedIds['1'];
    $NewRequestedOwnerId = $encrypatedIds['2'];
    $NewRequestedOwnerdetail = CUserGroup::getNameById($NewRequestedOwnerId);
    $currentOwnerdetail = CUserGroup::getNameById($currentOwnerId);
    $groupDetail = CGroup::getGroupNamesById($groupId);
    if ($approve_status == 'n') {
        $owner_to_memeber = CUserGroup::approvemailbody($currentOwnerdetail, $NewRequestedOwnerdetail, NULL, $groupDetail['0'], 'memeber_to_admin_declain');
        if ($owner_to_memeber) {
            $out = array('infoText' => 'You have declined the ownership change request');
        } else {
            $out = array('infoText' => $region->msg(1102));
        }
    } else {
        $userGropId = CGroup::loadIdByOwnerIdAndgroupId($groupId, $currentOwnerId); /* group Id is Passing */
        if ($userGropId) {
            $updateuserstatus = new CGroup();
            $updateuserstatus->set('id', $userGropId);
            $updateuserstatus->set('user_id', $NewRequestedOwnerId);
            $updatedstatsus = $updateuserstatus->updateStatus();

            if ($updatedstatsus) {
                $memeber_to_ownerin_usergroup = CUserGroup::changeOwnershipInUserGroup($groupId, $currentOwnerId, $NewRequestedOwnerId);
                $memeber_to_owner = CUserGroup::approvemailbody($NewRequestedOwnerdetail, NULL, NULL, $groupDetail['0'], 'memeber_to_admin');
                $owner_to_memeber = CUserGroup::approvemailbody($currentOwnerdetail, $NewRequestedOwnerdetail, NULL, $groupDetail['0'], 'admin_status_changed');
                if ($memeber_to_owner && $owner_to_memeber) {


                    $out = array('infoText' => $NewRequestedOwnerdetail['first_name'] . " " . $NewRequestedOwnerdetail['lst_name'] . 'is now the owner of this group');
                }
            } else {
                $out = array('infoText' => $region->msg(1102));
            }
        } else {
            $out = array('infoText' => $region->msg(1103));
        }
    }

    CCommon::xhrSend(CCommon::toJson($out));
    exit;
}

// Output HTML page
$region = new CRegion('approve');
$rplc = array();
$rplc[1] = $current_status['status'];

$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1101);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
$rplc[19] = scriptLinks();
$rplc[3] = approve_script();
$rplc[20] = script($region);
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace("approveowner.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
    @include 'google_analytics.html';


/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks() {
    $out = array();
    $out[] = CGoogle::scriptHtml();
    $out[] = CYahoo::scriptHtml(array('json', 'container', 'dom', 'connection', 'button'));
    if (CConfig::RUN_IN_FB)
        $out[] = CFacebook::scriptHtml();
    $out[] = '<script type="text/javascript" src="js/common.js"></script>';
    $out[] = '<script src="js/xhr.js" type="text/javascript"></script>';
     /*$out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
   $out[] = '<script type="text/javascript" src="js/jquery.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.tokeninput.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.sumoselect.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery_support.js"></script>';
    $out[] = '<script type="text/javascript" src="register.js"></script>';*/
    $out[] = '<script src="ownershipappoval.js" type="text/javascript"></script>';
    return join("\n", $out);
}

function script($region) {
    $out = array();
    $out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($region->msgList()));
    $out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($region->msgList()));
    return join('', $out);
}

function approve_script() {
    $new_urls = $_SERVER['QUERY_STRING'];
    if ($isValidSession) {
        $new_urls = urldecode($_SERVER['QUERY_STRING']);
    }
}

?>
