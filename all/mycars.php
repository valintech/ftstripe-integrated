<?php
require_once 'base.inc';
require_once 'classes/session.inc';
require_once 'classes/region.inc';
require_once 'classes/journey.inc';
require_once 'classes/google.inc';
require_once 'classes/yahoo.inc';
require_once 'classes/php2js.inc';
if (CConfig::RUN_IN_FB)
	require_once 'classes/facebook.inc';

//
// Potential request values:-
// "op" - operation code for specific script functions
// "journeyId" - journey id. to delete / leave / etc
//

// Get current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->requireSession();
CWebSession::set('login', serialize($login));

// Get posted data
$op = CCommon::getRequestValue('op');

// XmlHttpRequest:- Find journeys
if ($op == 'search')
{
	if (($journeyIdList = CJourney::getUserJourneyIds($login->userId())) != NULL)
		$journeyList1 = CJourney::load($journeyIdList);
	//$journeyList = CJourney::removeExpired($journeyList);
//	if (($journeyIdList = CJourneyRequest::getUserAccepted($login->userId())) != NULL)
//		$journeyList2 = CJourney::load($journeyIdList);
//	if (($journeyIdList = CJourneyRequest::getUserPending($login->userId())) != NULL)
//		$journeyList3 = CJourney::load($journeyIdList);
	$out = search($journeyList1, $journeyList2, $journeyList3);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// Delete the specified journey
if ($op == 'deleteJourney')
{
	$journeyId = CCommon::getRequestValue('journeyId');
	CLogging::info(sprintf("Deleting journey %ld", $journeyId));
	$journeys = CJourney::load($journeyId);
	if ($journeys[0]->get(CJourney::USER_ID) == $login->userId())
	{
		// XXX Can we wrap these in a transaction?
		CJourneyRequest::deleteByJourneyId($journeyId);
		CJourneyUser::deleteByJourneyId($journeyId);
		CJourney::delete($journeyId);
	}
	else
		CLogging::error(sprintf("User %d tried to delete journey %d, which they did not create", $login->userId(), $journeyId));
	// Redisplay the ride list
	header("Location: mycars.php");
	exit;
}

// Initialise if no opcode given
if ($op == '')
{
	// Output HTML page
	$region = new CRegion("mycars");
	$rplc = array();
	if ($isValidSession)
		$rplc[1] = sprintf('|%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
	else
		$rplc[1] = sprintf('|&nbsp;<a href="javascript:signIn()">%s</a>', $region->msg(3, "common"));
	//$rplc[2] = script($login, $isValidSession);
	//$rplc[4] = scriptLinks();
	$rplc[8] = $region->msg(8, 'common');
	$rplc[9] = $region->msg(9, 'common');
	$rplc[11] = $region->msg(1100);
	$rplc[30] = $region->msg(10, 'common');
	$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
	$rplc[32] = $region->msg(13, 'common');
	$rplc[33] = $region->msg(14, 'common');
	$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
	$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
	$out = CCommon::htmlReplace("mycars.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
	print($out);
	if (CConfig::RUN_IN_FB == 0)
		@include 'google_analytics.html';
}

/*
 * Print <script> element to output
 * 
 * @param $login            A CLogin object
 * @param $isValidSession   true if have current session otherwise false
 * @return                  HTML <script> stream
 */

function script ($login, $isValidSession)
{
	$php2Js = new Php2Js();
	$out = array();
	$out[] = '<script type="text/javascript">';
	$out = array_merge($out, CRoot::formatClassAsJs('CJourney'));
	$out = array_merge($out, CRoot::formatClassAsJs('CConfig', array(CConfig::CONTENT_DIR)));
	require_once('classes/region.inc');
	$region = new CRegion('mycars');
	$php2Js->add('_msgList', $region->msgList());
	$user = $login->getUser(true);
	$php2Js->add('_isUserRegistered', ($user && $user->isRegistered() ? true : false));
	$php2Js->add('_isValidSession', ($isValidSession ? true : false));
	$out[] = $php2Js->generateJs(); 
	$out[] = '</script>';
	return join('', $out);
}

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks ()
{
	$out = array();
	$out[] = CGoogle::scriptHtml();
	$out[] = CYahoo::scriptHtml(array('json', 'connection', 'container', 'menu', 'button'));
	$out[] = '<script type="text/javascript" src="js/common.js"></script>';
	$out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
	$out[] = '<script type="text/javascript" src="mycars.js"></script>';
	return join("\n", $out);
}

/*
 * Search for locations
 * 
 * @param $journeyList  List of CJourney objects
 */

function search ($journeyList1, $journeyList2, $journeyList3)
{
	$region = new CRegion('mycars');	
	$out = array();
	$html = array();
	$index = 0;
	$html[] = '<table class="resultsTable" cellpadding="0" cellspacing="0">';

//	$html[] = sprintf('<tr><td><h3>%s</h3><br></td></tr>', 'Journeys for which you are the driver:');
	if ($journeyList1 != NULL && count($journeyList1))
	{
		$journeyInfo = CJourney::journeyInfo($journeyList1);
		foreach ($journeyList1 as $journey)
		{
			$html[] = journeyHtml($index++, $journey, $region, $journeyInfo[$journey->get(CJourney::ID)], 'driver');
			$html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>',
								  CConfig::CONTENT_DIR);
			$out['journeys'][] = $journey;
		}
	}
	else
		$html[] = sprintf('<tr><td><span class="resultsDimmed">%s</span><br></td></tr>', $region->msg(1011));

//	$html[] = sprintf('<tr><td><br><h3>%s</h3><br></td></tr>', 'Journeys for which you are a passenger:');
//	if ($journeyList2 != NULL && count($journeyList2))
//	{
//		$journeyInfo = CJourney::journeyInfo($journeyList2);
//		foreach ($journeyList2 as $journey)
//		{
//			$html[] = journeyHtml($index++, $journey, $region, $journeyInfo[$journey->get(CJourney::ID)], 'passenger');
//			$html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>',
//								  CConfig::CONTENT_DIR);
//			$out['journeys'][] = $journey;
//		}
//	}
//	else
//		$html[] = sprintf('<tr><td><span class="resultsDimmed">%s</span><br></td></tr>', $region->msg(1011));
//
//	$html[] = sprintf('<tr><td><br><h3>%s</h3><br></td></tr>', 'Journeys for which you have a pending request');
//	if ($journeyList3 != NULL && count($journeyList3))
//	{
//		$journeyInfo = CJourney::journeyInfo($journeyList3);
//		foreach ($journeyList3 as $journey)
//		{
//			$html[] = journeyHtml($index++, $journey, $region, $journeyInfo[$journey->get(CJourney::ID)], 'pending');
//			$html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>',
//								  CConfig::CONTENT_DIR);
//			$out['journeys'][] = $journey;
//		}
//	}
//	else
//		$html[] = sprintf('<tr><td><span class="resultsDimmed">%s</span><br></td></tr>', $region->msg(1011));

	$html[] = '</table>';
	$out['html'] = join('', $html);
	return $out;
}

/*
 * Create HTML representation of the journey
 * 
 * @param $index        Journey index
 * @param $journey      A CJourney object
 * @param $region       A CRegion object
 * @param $journeyInfo  stdClass object containing extra journey
 *                      information "placesTaken", "placesAvailable",
 *                      "smoker", "femaleOnly"
 *                      members
 * @return              HTML
 */

function journeyHtml ($index, $journey, $region, $journeyInfo, $type)
{
	// Female only, smoker information
	$tmp = array();
	if ($journeyInfo->smoker)
		$tmp[] = $region->msg(1013);
	if ($journeyInfo->femaleOnly)	
		$tmp[] = $region->msg(1014);
	$hostPrefsHtml = (count($tmp) ? join(', ', $tmp) : '');

	$tmp2 = array();
	$tmp2[] = '<br/>I am ';
	if ($journey->get(CJourney::REQUEST_TYPE) == CJourney::REQUEST_TYPE_SEEKING) {
		$tmp2[] = 'seeking';
	} else if ($journey->get(CJourney::REQUEST_TYPE) == CJourney::REQUEST_TYPE_OFFERING) {
		$tmp2[] = 'offering';
	} else {
		$tmp2[] = 'offering to share';
	}
	$tmp2[] = ' a lift';
	$requestorHtml = join('', $tmp2);

	$html = array();
	$html[] = '<tr><td>';

	if ($journey->get(CJourney::JOURNEY_TYPE) == CJourney::JOURNEY_TYPE_ONEOFF)
	{
		$html[] = '<strong>';
		$html[] = $region->msg(1015);
		$html[] = '</strong>';
		$html[] = $requestorHtml;
		$html[] = '<span class="resultsDimmed">';
		$html[] = sprintf('<br/>On %s @ %s', strftime('%x', CCommon::tsToPhp($journey->get(CJourney::ONEOFF_DATE))),
						  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CJourney::ONEOFF_TIME))));
		if ($hostPrefsHtml != '')
			$html[] = '<br/>' . $hostPrefsHtml;
		$html[] = '</span><br/>';
	}
	else
	{
		$html[] = '<strong>';
		$html[]  = $region->msg(1016);
		$html[] = '</strong>';
		$html[] = $requestorHtml;
		$html[] = '<span class="resultsDimmed">';
		if ($journey->get(CJourney::SCHEDULE_TYPE) == CJourney::SCHEDULE_TYPE_STANDARD)
			$html[] = sprintf('<br/>%s %s %s - Start @ %s / Return @ %s',
							  $region->msg(17, 'common'), $region->msg(24, 'common'), $region->msg(21, 'common'),
							  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CJourney::STANDARD_START_TIME))),
							  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CJourney::STANDARD_RETURN_TIME))));
		elseif ($journey->get(CJourney::SCHEDULE_TYPE) == CJourney::SCHEDULE_TYPE_CUSTOM)
			$html[] = customTimesHtml($journey, $region);
		if ($hostPrefsHtml != '')
			$html[] = '<br/>' . $hostPrefsHtml;
		$html[] = '</span><br/>';
	}
	$html[] = sprintf('%s<br/><img align="absmiddle" src="%s/images/arrow.gif" width="12" height="12" align="absmiddle" />&nbsp;%s<br/>',
					  $journey->get(CJourney::START_ADDRESS),
					  CConfig::CONTENT_DIR,
					  $journey->get(CJourney::DESTINATION_ADDRESS));
	$html[] = '</td></tr>';
					  
	$html[] = '<tr><td align="right">';
	$tmp = array();
//	if ($type == 'driver')
		$tmp[] = sprintf('<a href="javascript:_deleteJourney(%d)">%s</a>', $index, $region->msg(1017));
	$tmp[] = sprintf('<a href="javascript:_searchJourney(%d)">%s</a>', $index, $region->msg(1018));
	$html[] = join('<span class="resultsOptions">&nbsp;|&nbsp;</span>', $tmp);
	$html[] = '</td></tr>';
	
	return join('', $html);	
}

/*
 * Create HTML for custom journey times
 * 
 * @param $journey    A CJourney object
 * @param $region     A CRegion object
 * @return            HTML
 */

function customTimesHtml ($journey, $region)
{
	$dayMsgs = array(17, 18, 19, 20, 21, 22, 23);
	$times = array(
		array(CJourney::MON_START_TIME, CJourney::MON_RETURN_TIME),
		array(CJourney::TUE_START_TIME, CJourney::TUE_RETURN_TIME),
		array(CJourney::WED_START_TIME, CJourney::WED_RETURN_TIME),
		array(CJourney::THU_START_TIME, CJourney::THU_RETURN_TIME),
		array(CJourney::FRI_START_TIME, CJourney::FRI_RETURN_TIME),
		array(CJourney::SAT_START_TIME, CJourney::SAT_RETURN_TIME),
		array(CJourney::SUN_START_TIME, CJourney::SUN_RETURN_TIME));
	$out = array();
	foreach ($times as $k => $time)
	{
		$start = $journey->get($time[0]);
		$end = $journey->get($time[1]);
		if ($start == '' || $end == '')
			continue;
		$out[] = sprintf('%s - Start @ %s / Return @ %s',
			$region->msg($dayMsgs[$k], 'common'),
			strftime($region->msg(25, 'common'), CCommon::tmToPhp($start)),
			strftime($region->msg(25, 'common'), CCommon::tmToPhp($end)));
	}
	$out = join('<br/>', $out);
	return $out != '' ? '<br/>'.$out : $out;
}
?>
