<?php
function hex2rgba($hex) {

	$hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}

#-----------------------------------------------------------------#
# Hex to darker RGBA function
#-----------------------------------------------------------------#

function hexDarker( $hex, $factor = 30 ) {
    $new_hex = '';
    if ( $hex == '' || $factor == '' ) {
        return false;
    }

    $hex = str_replace( '#', '', $hex );

    $base['R'] = hexdec( $hex{0}.$hex{1} );
    $base['G'] = hexdec( $hex{2}.$hex{3} );
    $base['B'] = hexdec( $hex{4}.$hex{5} );


    foreach ( $base as $k => $v ) {
        $amount = $v / 100;
        $amount = round( $amount * $factor );
        $new_decimal = $v - $amount;

        $new_hex_component = dechex( $new_decimal );
        if ( strlen( $new_hex_component ) < 2 ) { $new_hex_component = "0".$new_hex_component; }
        $new_hex .= $new_hex_component;
    }

    return '#'.$new_hex;
}

function get_related_posts($post_id,$items) {
	
	$query = new WP_Query();    
    $args = '';
	$args = wp_parse_args($args, array(
		'showposts' => $items,
		'post__not_in' => array($post_id),
		'ignore_sticky_posts' => 0,
        'category__in' => wp_get_post_categories($post_id)
	));
	
	$query = new WP_Query($args);
	
  	return $query;
}


function get_related_projects($post_id,$items) {
    $query = new WP_Query();
    
    $args = $item_array = '';

    $item_cats = get_the_terms($post_id, 'portfolio_category');
    if($item_cats):
    foreach($item_cats as $item_cat) {
        $item_array[] = $item_cat->term_id;
    }
    endif;

    $args = wp_parse_args($args, array(
        'showposts' => $items,
        'post__not_in' => array($post_id),
        'ignore_sticky_posts' => 0,
        'post_type' => 'creativo_portfolio',
        'tax_query' => array(
            array(
                'taxonomy' => 'portfolio_category',
                'field' => 'id',
                'terms' => $item_array
            )
        )
    ));
    
    $query = new WP_Query($args);
    
    return $query;
}

function modern_related_posts() {
  global $items, $related_columns;
  $relate = get_related_posts(get_the_ID(),$items);  
  if($relate->have_posts()): ?>
    <div class="posts-boxes related_posts">
      <div class="content_box_title">
        <h5><?php _e('Related Posts', 'Creativo'); ?></h5>
      </div>
      <div class="recent_posts_container related_posts">
        <div class="is_grid clearfix">
        <?php                   
          while($relate->have_posts()): $relate->the_post();                      
          ?>
          <div class="blogpost grid_posts columns-<?php echo $related_columns; ?>">
            <?php            
            if(has_post_thumbnail() || get_post_meta( get_the_ID(), 'pyre_youtube', true) || get_post_meta(get_the_ID(), 'pyre_vimeo', true)):
            ?>
              <div class="flexslider mini related_posts">
                  <ul class="slides">
                    <?php
                    if(get_post_meta( get_the_ID(), 'pyre_youtube', true)):
                      echo '<li><div class="video-container" style="height:12px;"><iframe title="YouTube video player" width="218px" height="134px" src="//www.youtube.com/embed/' . get_post_meta(get_the_ID(), 'pyre_youtube', true) . '" frameborder="0" allowfullscreen></iframe></div></li>';
                    endif;
                    if(get_post_meta(get_the_ID(), 'pyre_vimeo', true)):
                      echo '<li><div class="video-container" style="height:12px;"><iframe src="//player.vimeo.com/video/' . get_post_meta(get_the_ID(), 'pyre_vimeo', true) . '" width="220px" height="161px" frameborder="0"></iframe></div></li>';
                    endif;                          
                  
                    if(has_post_thumbnail()):
                  
                      $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'recent-posts');
                      $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                      $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id());                           
                      echo '<li><div class="one-fourth-recent"><figure><a href="'.get_permalink($post->ID).'"><div class="text-overlay"><div class="info"><i class="fa fa-search"></i></div></div></figure>'.get_the_post_thumbnail($post->ID, 'recent-posts').'</a></div></li>';
                        
                    endif;                                            
                    ?>
                  </ul>
              </div>
            <?php
            endif;
            ?>
            <div class="description">
              <h3><a href="<?php echo get_permalink($post->ID);?>"><?php echo get_the_title();?></a></h3>                   
            </div>
          </div>
                            
        <?php             
        endwhile;
        wp_reset_query();
        ?>
        </div>                          
      </div>   
  </div>                        
  <?php 
  endif; 
}

function modern_related_projects() {  
  
  $relate = get_related_projects(get_the_ID(),4);  
  if($relate->have_posts()): ?>
    <div class="posts-boxes related_posts">      
        <h5 class="related_posts_title"><?php _e('Related Projects', 'Creativo'); ?></h5>      
      <div class="recent_posts_container related_posts">
        <div class="is_grid clearfix">
        <?php                   
          while($relate->have_posts()): $relate->the_post();                      
          ?>
          <div class="blogpost grid_posts columns-4">
            <?php            
            if(has_post_thumbnail() || get_post_meta( get_the_ID(), 'pyre_youtube', true) || get_post_meta(get_the_ID(), 'pyre_vimeo', true)):
            ?>
              <div class="flexslider mini related_posts">
                  <ul class="slides">
                    <?php
                    if(get_post_meta( get_the_ID(), 'pyre_youtube', true)):
                      echo '<li><div class="video-container" style="height:12px;"><iframe title="YouTube video player" width="218px" height="134px" src="//www.youtube.com/embed/' . get_post_meta(get_the_ID(), 'pyre_youtube', true) . '" frameborder="0" allowfullscreen></iframe></div></li>';
                    endif;
                    if(get_post_meta(get_the_ID(), 'pyre_vimeo', true)):
                      echo '<li><div class="video-container" style="height:12px;"><iframe src="//player.vimeo.com/video/' . get_post_meta(get_the_ID(), 'pyre_vimeo', true) . '" width="220px" height="161px" frameborder="0"></iframe></div></li>';
                    endif;                          
                  
                    if(has_post_thumbnail()):
                  
                      $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'recent-posts');
                      $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                      $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id());                           
                      //echo '<li><div class="one-fourth-recent"><figure><a href="'.get_permalink($post->ID).'"><div class="text-overlay"><div class="info"><i class="fa fa-search"></i></div></div></figure>'.get_the_post_thumbnail($post->ID, 'recent-posts').'</a></div></li>';
                      ?>
                      <li>
                        <div class="related_posts_img">
                          <?php echo get_the_post_thumbnail($post->ID, 'portfolio-four'); ?>
                          <div class="title_categ_over">
                            <div class="title_categ_over_wrap">
                              <h4><a href="<?php echo get_permalink( get_the_ID() );?>"><?php echo get_the_title();?></a></h4> 
                              <span class="related_port_categ"><?php echo get_the_term_list( get_the_ID(), 'portfolio_category', '', ', ', ''); ?></span>
                            </div>
                          </div>
                        </div>
                      </li>
                      <?php  
                    endif;                                            
                    ?>
                  </ul>
              </div>
            <?php
            endif;
            ?>            
          </div>
                            
        <?php             
        endwhile;
        wp_reset_query();
        ?>
        </div>                          
      </div>   
  </div>                        
  <?php 
  endif; 
}

function portfolio_page_template( $cols, $thumb) {

  global $portfolio_style, $grid, $data;

  $thumbnail = get_post_meta(get_the_ID(), 'pyre_portfolio_style', true) != 'masonry' ? 'portfolio-' . $thumb : 'full';

  if(!get_post_meta(get_the_ID(), 'pyre_portfolio_category', true)):      
      
      $portfolio_category = !get_post_meta(get_the_ID(), 'pyre_exclude_portfolio_category', true) ? get_terms('portfolio_category') : get_terms( array( 'taxonomy' => 'portfolio_category', 'exclude' => get_post_meta(get_the_ID(), 'pyre_exclude_portfolio_category', true) ) );
      
      if($portfolio_category && ( sizeof($portfolio_category) > 1 )):
      ?>                    
          <ul class="portfolio-tabs portfolio-templates clearfix">
              <li class="active"><a data-filter="*" href="#"><?php _e('All', 'Creativo'); ?></a></li>
              <?php foreach($portfolio_category as $portfolio_cat): ?>
                <li><a data-filter=".<?php echo $portfolio_cat->slug; ?>" href="#"><?php echo $portfolio_cat->name; ?></a></li>
              <?php endforeach; ?>
          </ul>
      <?php endif; ?>
  <?php endif; ?>
  <div class="portfolio-wrapper <?php echo $grid; ?> clearfix" masonry-effect="<?php echo (get_post_meta(get_the_ID(), 'pyre_portfolio_style', true) == 'masonry') ? 'yes' : 'no' ?>">  
      <?php
      if(is_front_page()) {
        $paged = (get_query_var('page')) ? get_query_var('page') : 1;
      } else {
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
      }
      $args = array(
          'post_type' => 'creativo_portfolio',
          'paged' => $paged,
          'posts_per_page' => $data['portfolio_items']
      );
      if(get_post_meta(get_the_ID(), 'pyre_portfolio_category', true)){
          $args['tax_query'][] = array(
              'taxonomy' => 'portfolio_category',
              'field' => 'term_id',
              'terms' => get_post_meta(get_the_ID(), 'pyre_portfolio_category', true)
          );
      }
      else {        
        if(get_post_meta(get_the_ID(), 'pyre_exclude_portfolio_category', true)){
          $args['tax_query'][] = array(
              'taxonomy' => 'portfolio_category',
              'field' => 'term_id',
              'terms' => get_post_meta(get_the_ID(), 'pyre_exclude_portfolio_category', true),
              'operator' => 'NOT IN',
          );
        }
      }
      $gallery = new WP_Query($args);
  
      while($gallery->have_posts()): $gallery->the_post();
        if(has_post_thumbnail() || get_post_meta(get_the_ID(), 'pyre_youtube', true) || get_post_meta(get_the_ID(), 'pyre_vimeo', true)):                
          $item_classes = '';
          $item_cats = get_the_terms(get_the_ID(), 'portfolio_category');
          if($item_cats):
            foreach($item_cats as $item_cat) {
              $item_classes .= $item_cat->slug . ' ';
            }
          endif;                
          $thumb_link = wp_get_attachment_image_src(get_post_thumbnail_id(), $thumbnail);
          $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');           
          
          if ( $portfolio_style != 'flat' && $portfolio_style != 'masonry' ){          
          ?>
            <div class="portfolio-item  <?php echo $item_classes; ?>">   
                <div class="project-feed clearfix">                   
                    <div class="ch-item portfolio-<?php echo $cols; ?>"> 
                        <div class="ch-info portfolio-<?php echo $cols; ?>">
                            <div class="ch-info-front<?php echo $cols; ?> "><img src="<?php echo $thumb_link[0]; ?>" /></div>
                            <div class="ch-info-back<?php echo $cols; ?> portfolio-<?php echo $cols; ?>" style="background-image:url(<?php echo $thumb_link[0]; ?>);">
                                <?php                           
                                if (get_post_meta(get_the_ID(), 'pyre_custom_link', true) != '') {                         
                                ?>
                                    <div class="info"><a href="<?php echo get_post_meta(get_the_ID(), 'pyre_custom_link', true); ?>" target="<?php echo get_post_meta(get_the_ID(), 'pyre_custom_link_target', true); ?>"><i class="fa fa-search"></i></a>
                                    </div>                                                      
                                <?php
                                }
                                else{
                                ?>                            
                                    <div class="info"><a href="<?php echo get_permalink(get_the_ID())?>"><i class="fa fa-search"></i></a>
                                    </div>
                                <?php 
                                } 
                                ?>                          
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="portfolio_details">                                     
                    <h3><a href="<?php echo get_permalink(get_the_ID())?>"><?php echo get_the_title(); ?></a></h3>
                    <div class="tags"><?php echo get_the_term_list(get_the_ID(), 'portfolio_category', '', ', ', '');?></div>
                </div>                
            </div>  
          <?php 
          }
          else {
            if($data['single_portfolio_style'] != 'modern') {
            ?>  
              <figure class="effect-zoe cols-<?php echo $cols; ?> <?php echo $item_classes; ?>"> 
                <?php echo get_the_post_thumbnail(get_the_ID(), $thumbnail);?>
                <div class="effect-overlay">
                  <div class="zoomin"><a href="<?php echo $full_image[0]; ?>" rel="prettyPhoto"><i class="fa fa-search"></i></a></div>
                    <?php
                    if (get_post_meta(get_the_ID(), 'pyre_custom_link', true) != '') {
                    ?>  
                      <div class="launch"><a href="<?php echo get_post_meta(get_the_ID(), 'pyre_custom_link', true); ?>" target="<?php echo get_post_meta(get_the_ID(), 'pyre_custom_link_target', true); ?>"><i class="fa fa-link"></i></a></div>';
                    <?php    
                    }
                    else {
                    ?>
                      <div class="launch"><a href="<?php echo get_permalink(get_the_ID()); ?>"><i class="fa fa-link"></i></a></div>
                    <?php
                    }
                  ?>
                </div>
                <figcaption>
                  <h3><a href="<?php echo get_permalink(get_the_ID());?>"><?php echo get_the_title(); ?></a></h3>
                </figcaption>
              </figure>
          <?php
            }
            else {
              $link = ( get_post_meta ( get_the_ID(), 'pyre_custom_link', true ) != '' ) ? get_post_meta ( get_the_ID(), 'pyre_custom_link', true) : get_permalink( get_the_ID() );
              $target = ( get_post_meta( get_the_ID(), 'pyre_custom_link', true) != '' ) ? get_post_meta(get_the_ID(), 'pyre_custom_link_target', true) : '_self';
              ?>
              <figure class="cols-<?php echo $cols; ?> modern_portfolio_layout <?php echo $item_classes; ?>">              
                
                <a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><?php echo get_the_post_thumbnail(get_the_ID(), $thumbnail);?></a>
                  
                <div class="modern_overlay_effect">
                  <a href="<?php echo $link; ?>" class="make_this_full" target="<?php echo $target; ?>"></a>
                  <div class="portfolio_content_wrap">                  
                    <h3><a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><?php echo get_the_title(); ?></a></h3>
                    <span class="portfolio_categ_list"><?php echo get_the_term_list(get_the_ID(), 'portfolio_category', '', ', ', '');?></span>
                  </div>
                </div>  
                
              </figure>
            <?php
            } 
          }
        endif;
      endwhile;
      wp_reset_query();
      ?>
  </div>
  <?php
  cr_pagination($gallery->max_num_pages, $range = 2);
}

function cr_flat_portfolio_no_video( $cols, $item_classes, $css_out, $link, $target, $port_date, $full_image) {

  $html .= '<figure class="effect-zoe cols-'.$cols.' '. $item_classes . '">'; 
    $html .= '<div class="figure_image_holder">';
      $html .= get_the_post_thumbnail( get_the_ID(), $css_out );
      $html .= '<div class="effect-overlay">';

        $html .= '<div class="zoomin"><a href="'. $full_image .'" rel="prettyPhoto"><i class="fa fa-search"></i></a></div>';        
        $html .= '<div class="launch"><a href="'. $link .'" target="'. $target .'"><i class="fa fa-link"></i></a></div>';
        
      $html .= '</div>';
    $html .= '</div>';  
    $html .= '<figcaption>';
      
      $html .= '<h3><a href="'. $link .'" target="'. $target .'">'. get_the_title().'</a></h3>';
      
      if($port_date =='yes'){
        $html .= '<div class="portfolio_date">'.get_the_date('', get_the_ID()).'</div>';
      }
    $html .= '</figcaption>';
  $html .= '</figure>';

  return $html;
}

function cr_flat_portfolio_modern_style( $cols, $item_classes, $thumbnail, $link, $target ) {

  $html .= '<figure class="cols-' . $cols . ' modern_portfolio_layout ' . $item_classes . '">';
                
    $html .= '<a href="' . $link . '" target="' . $target . '">' . get_the_post_thumbnail( get_the_ID(), $thumbnail ) .'</a>';
      
    $html .= '<div class="modern_overlay_effect">';
      $html .= '<a href="' . $link . '" class="make_this_full" target="' . $target . '"></a>';
      $html .= '<div class="portfolio_content_wrap">';                 
        $html .= '<h3><a href="' . $link . '" target="' . $target . '">' . get_the_title() . '</a></h3>';
        $html .= '<span class="portfolio_categ_list">' . get_the_term_list(get_the_ID(), 'portfolio_category', '', ', ', '') .'</span>';
      $html .= '</div>';
    $html .= '</div>'; 
    
  $html .= '</figure>';

  return $html;
}



function cr_flexslider_render() {
  global $data;
  if(has_post_thumbnail() || get_post_meta( get_the_ID(), 'pyre_youtube', true ) || get_post_meta( get_the_ID(), 'pyre_vimeo', true )):
  ?>
    <div class="flexslider" data-interval="0" data-flex_fx="fade" data-smooth-height="true">
      <ul class="slides">
        <?php if( get_post_meta(get_the_ID(), 'pyre_youtube', true) != ''){ ?>
        <li class="video-container">                          
            <?php echo  do_shortcode('[youtube id="'.get_post_meta(get_the_ID(), 'pyre_youtube', true).'" ]'); ?>                               
        </li>
        <?php 
          $has_slides = true;
        }  
        if( get_post_meta(get_the_ID(), 'pyre_vimeo', true) != ''){ ?>
          <li class="video-container">                        
              <?php echo do_shortcode('[vimeo id="'.get_post_meta(get_the_ID(), 'pyre_vimeo', true).'" width="600" height="350"]'); ?>
          </li>
          <?php 
          $has_slides = true;
        } 

        if(has_post_thumbnail() && ( get_post_meta(get_the_ID(), 'pyre_skip_first', true) !='yes' )){ 
        ?>               
          <?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
          <?php $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id()); ?>
          <li>
            <a href="<?php echo $full_image[0]; ?>" rel="prettyPhoto['gallery']"><?php the_post_thumbnail('full');?></a>                                    
          </li>
        <?php 
        } 

        $i = 2;
        while($i <= $data['featured_images_count']):
          $attachment = new StdClass();
          $attachment->ID = kd_mfi_get_featured_image_id('featured-image-'.$i, 'creativo_portfolio');
          if($attachment->ID):                    
            ?>
          
            <?php $full_image = wp_get_attachment_image_src($attachment->ID, 'full'); ?>
            <?php $attachment_data = wp_get_attachment_metadata($attachment->ID); ?>
            <li>  
              <a href="<?php echo $full_image[0] ?>" rel="prettyPhoto['gallery']"><img src="<?php echo $full_image[0]; ?>" alt="<?php echo $attachment->post_title; ?>" /></a>  
            </li>                                                        
            <?php 
            $has_slides = true;
          endif; 
          $i++; 
        endwhile; 

        if(!$has_slides && ( get_post_meta(get_the_ID(), 'pyre_skip_first', true) =='yes') ) {
          echo '<li></li>';
        }
        ?>    
      </ul>
    </div>
    <?php
  endif;
}

function cr_project_details_modern() {
  global $data;

  $field_1 = ( $data['pd_custom' ] && ( $data['client_name_custom'] != 'Client name' ) ) ? $data['client_name_custom'] : __('Client name', 'Creativo');
  $field_2 = ( $data['pd_custom' ] && ( $data['skills_custom'] != 'Skills' ) ) ? $data['skills_custom'] : __('Skills', 'Creativo');
  $field_3 = ( $data['pd_custom' ] && ( $data['category_custom'] != 'Category' ) ) ? $data['category_custom'] : __('Category', 'Creativo');
  $field_4 = ( $data['pd_custom' ] && ( $data['website_custom'] != 'Website' ) ) ? $data['website_custom'] : __('Website', 'Creativo');

  ?>                  
  <div class="portfolio-misc-info">                       

    <?php if(get_post_meta(get_the_ID(), 'pyre_client_name', true)): ?>                    
      <div class="project-info-details">                          
        <h3><?php echo $field_1; ?></h3>
        <span><?php echo get_post_meta(get_the_ID(), 'pyre_client_name', true); ?></span>
      </div>
    <?php endif; ?>
    
    <?php if(get_post_meta(get_the_ID(), 'pyre_skills', true)): ?>
      <div class="project-info-details">
        <h3><?php echo $field_2; ?></h3>
        <span><?php echo get_post_meta(get_the_ID(), 'pyre_skills', true); ?></span>                                                    
      </div>
    <?php endif; ?>

    <?php if( $data['project_date'] != '0') : ?>
      <div class="project-info-details">
        <h3><?php echo _e('Year','Creativo'); ?></h3>
        <span><?php the_date( 'Y' ); ?></span>             
      </div>
    <?php endif; ?>
    
    <?php if(get_the_term_list(get_the_ID(), 'portfolio_category', '', '<br />', '') && (get_post_meta(get_the_ID(), 'pyre_width', true) != 'half') ): ?>
      <div class="project-info-details">
        <h3><?php echo $field_3; ?></h3>
        <span><?php echo get_the_term_list(get_the_ID(), 'portfolio_category', '', ', ', ''); ?></span>                                                      
      </div>
    <?php endif; ?>
    
    <?php if(get_post_meta(get_the_ID(), 'pyre_website_text', true) && get_post_meta(get_the_ID(), 'pyre_website_url', true)): ?>
      <div class="project-info-details">
        <h3><?php echo $field_4; ?></h3>  
        <span><a href="<?php echo get_post_meta(get_the_ID(), 'pyre_website_url', true); ?>" rel="nofollow" target="_blank"><?php echo get_post_meta(get_the_ID(), 'pyre_website_text', true); ?></a></span>
      </div>
    <?php endif; ?>

  </div>
  <?php
}

function one_column_modern_portfolio( $item_classes ) {
  global $data;
  $field_1 = ( $data['pd_custom' ] && ( $data['client_name_custom'] != 'Client name' ) ) ? $data['client_name_custom'] : __('Client name', 'Creativo');
  $field_2 = ( $data['pd_custom' ] && ( $data['skills_custom'] != 'Skills' ) ) ? $data['skills_custom'] : __('Skills', 'Creativo');
  $html = '';

  $html .= '<div class="portfolio-item-modern ' . $item_classes . ' clearfix">';
    $html .= '<div class="portfolio-modern-image">';
      $html .= '<div class="flexslider" data-interval="0" data-flex_fx="fade">';
        $html .= '<ul class="slides">';
          
          if( get_post_meta(get_the_ID(), 'pyre_youtube', true) != ''){
            $html .= '<li class="video-container">';
              $html .= do_shortcode('[youtube id="'.get_post_meta(get_the_ID(), 'pyre_youtube', true).'" ]');
            $html .= '</li>';
            $has_slides = true;
          }

          if( get_post_meta(get_the_ID(), 'pyre_vimeo', true) != ''){
            $html .= '<li class="video-container">';
              $html .= do_shortcode('[vimeo id="'.get_post_meta(get_the_ID(), 'pyre_vimeo', true).'" width="600" height="350"]');
            $html .= '</li>';
            $has_slides = true;
          }

          if( has_post_thumbnail() ){
            $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
            $attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id());
            $html .= '<li>';
              $html .= '<a href="' . $full_image[0] . '" rel="prettyPhoto[\'gallery_'.get_the_ID().'\']">' . get_the_post_thumbnail( get_the_ID(), 'portfolio-two') . '</a>';                                    
            $hml .= '</li>';
          }

        $html .= '</ul>';
      $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="portfolio-modern-description">';
      $html .= '<div class="potfolio-modern-content-wrap">';
        $html .= '<div class="portfolio-modern-categs">' . get_the_term_list(get_the_ID(), 'portfolio_category', '<h4>', ', ', '</h4>') . '</div>';
        $html .= '<div class="portfolio-modern-title"><h3><a href="' . get_permalink(get_the_ID()) . '">' . get_the_title() . '</a></h3></div>';
        $html .= '<div class="portfolio-modern-content">' . string_limit_words(get_the_excerpt(), $excerpt_length ? $excerpt_length : 30 ) . '...</div>';
      $html .= '</div>'; 
      
      if($data['project_details']) {
        $html .= '<div class="portfolio-modern-proj-details portfolio-modern">';
          $html .= '<div class="portfolio-misc-info">';

            if(get_post_meta(get_the_ID(), 'pyre_client_name', true)):
              $html .= '<div class="project-info-details">';
                $html .= '<h3>' . $field_1 . '</h3>';
                $html .= '<span>' . get_post_meta(get_the_ID(), 'pyre_client_name', true) . '</span>';
              $html .= '</div>';
            endif; 

            if(get_post_meta(get_the_ID(), 'pyre_skills', true)):
              $html .= '<div class="project-info-details">';
                $html .= '<h3>' . $field_2 . '</h3>';
                $html .= '<span>' . get_post_meta(get_the_ID(), 'pyre_skills', true) . '</span>';
              $html .= '</div>';
            endif; 

            if( $data['project_date'] != '0') :
              $html .= '<div class="project-info-details">';
                $html .= '<h3>' . __('Year','Creativo') . '</h3>';
                $html .= '<span>' . get_the_date( 'Y' ) . '</span>';
              $html .= '</div>';
            endif;

          $html .= '</div>';
        $html .= '</div>';
      }

    $html .= '</div>';
  $html .= '</div>';

  return $html;
}

function cr_checkIfMenuIsSetByLocation($menu_location = '') {
    if(has_nav_menu($menu_location)) {
        return true;
    }

    return false;
}

function nv_is_out_of_stock() {
	    global $post;
	    $post_id = $post->ID;
	    $stock_status = get_post_meta($post_id, '_stock_status',true);
	    
	    if ($stock_status == 'outofstock') {
	    return true;
	    } else {
	    return false;
	    }
	}

function cr_pagination($pages = '', $range = 2) {   

	global $data;

     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo '<div class="pagination ' . $data['pagination_style'] . ' clearfix">';
        // if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo '<a href="'.get_pagenum_link(1).'"><span class="arrows">&laquo;</span> First</a>';
         if($paged > 1 && $data['en_custom_pag'] && $data['en_navigation_arrows'] ) echo '<a class="pagination-prev" href="'.get_pagenum_link($paged - 1).'"><i class="fa fa-angle-left"></i></a>';

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i) ? '<span class=current>' . $i . '</span>' : '<a href="'.get_pagenum_link($i).'" class="inactive" >' . $i . '</a>';
             }
         }

         if ($paged < $pages  && $data['en_custom_pag'] && $data['en_navigation_arrows']) echo '<a class="pagination-next" href="' . get_pagenum_link($paged + 1) . '"><i class="fa fa-angle-right"></i></a>';  
         //if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last <span class='arrows'>&raquo;</span></a>";
         echo '</div>';
     }
}

function string_limit_words($string, $word_limit)
{
	$words = explode(' ', $string, ($word_limit + 1));
	
	if(count($words) > $word_limit) {
		array_pop($words);
	}
	
	return implode(' ', $words);
}

if(!function_exists('cr_breadcrumb')):
function cr_breadcrumb() {
        global $data,$post;
        echo '<ul class="breadcrumbs">';

         if ( !is_front_page() ) {
        echo '<li><a href="';
        echo home_url();
        echo '">';
        echo "</a></li>";
        }

        $params['link_none'] = '';
        $separator = '';

        if (is_category() && !is_singular('creativo_portfolio')) {
            $category = get_the_category();
            $ID = $category[0]->cat_ID;
            echo is_wp_error( $cat_parents = get_category_parents($ID, TRUE, '', FALSE ) ) ? '' : '<li>'.$cat_parents.'</li>';
        }

        if(is_singular('creativo_portfolio')) {
            echo get_the_term_list($post->ID, 'portfolio_category', '<li>', '&nbsp;/&nbsp;&nbsp;', '</li>');
            echo '<li>'.get_the_title().'</li>';
        }

        if (is_tax()) {
            $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
            echo '<li>'.$term->name.'</li>';
        }

        if(is_home() || is_front_page()) { echo '<li><i class="fa fa-home"></i></li>'; }
        if(is_page() && !is_front_page()) {
            $parents = array();
            $parent_id = $post->post_parent;
            while ( $parent_id ) :
                $page = get_page( $parent_id );
                if ( $params["link_none"] )
                    $parents[]  = get_the_title( $page->ID );
                else
                    $parents[]  = '<li><a href="' . get_permalink( $page->ID ) . '" title="' . get_the_title( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a></li>' . $separator;
                $parent_id  = $page->post_parent;
            endwhile;
            $parents = array_reverse( $parents );
            echo join( '', $parents );
            echo '<li>'.get_the_title().'</li>';
        }
        if(is_single() && !is_singular('creativo_portfolio')  && !is_singular('event')) {
            $categories_1 = get_the_category($post->ID);
            if($categories_1):
                foreach($categories_1 as $cat_1):
                    $cat_1_ids[] = $cat_1->term_id;
                endforeach;
                $cat_1_line = implode(',', $cat_1_ids);
            endif;
            if( $cat_1_line ) {
                $categories = get_categories(array(
                    'include' => $cat_1_line,
                    'orderby' => 'id'
                ));
                if ( $categories ) :
                    foreach ( $categories as $cat ) :
                        $cats[] = '<li><a href="' . get_category_link( $cat->term_id ) . '" title="' . $cat->name . '">' . $cat->name . '</a></li>';
                    endforeach;
                    echo join( '', $cats );
                endif;
            }
            echo '<li>'.get_the_title().'</li>';
        }
        if(is_tag()){ echo '<li>'."Tag: ".single_tag_title('',FALSE).'</li>'; }
        if(is_404()){ echo '<li>'.__("404 - Page not Found", 'Creativo').'</li>'; }
        if(is_search()){ echo '<li>'.__("Search", 'Creativo').'</li>'; }
        if(is_year()){ echo '<li>'.get_the_time('Y').'</li>'; }

        echo "</ul>";
}
endif;

if( ! function_exists( 'less_css' ) ) {
	function less_css( $minify ) {
		/* remove comments */
		$minify = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $minify );

		/* remove tabs, spaces, newlines, etc. */
		$minify = str_replace( array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $minify );
			
		return $minify;
	}
}

/* Get Attachment id from image url */
function pn_get_attachment_id_from_url( $attachment_url = '' ) {
 
    global $wpdb;
    $attachment_id = false;
 
    // If there is no url, return.
    if ( '' == $attachment_url )
        return;
 
    // Get the upload directory paths
    $upload_dir_paths = wp_upload_dir();
 
    // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
    if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {
 
        // If this is the URL of an auto-generated thumbnail, get the URL of the original image
        $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );
 
        // Remove the upload path base directory from the attachment URL
        $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
 
        // Finally, run a custom database query to get the attachment ID from the modified attachment URL
        $attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );
 
    }
 
    return $attachment_id;
}

function cr_share_post() {
  global $data, $post;
  $template_page = !is_single() ? 'share_archives' : ( is_singular('creativo_portfolio') ? ' social_ic_margin' : '' );
  $content = '<ul class="get_social '.$template_page.'">';

  if($data['share_facebook'] !='no') { 
    $content .= '<li><a class="fb  ntip" href="http://www.facebook.com/sharer.php?m2w&s=100&p&#91;url&#93;=' . get_the_permalink() . '&p&#91;images&#93;&#91;0&#93;=' . wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ) . '&p&#91;title&#93;=' . rawurlencode( get_the_title() ) .'" target="_blank" title="'. __("Share on Facebook", "Creativo").'"><i class="fa fa-facebook"></i></a></li>';    
  }

  if($data['share_twitter'] !='no') {
    $content .= '<li><a class="tw ntip" title="' . __("Share on Twitter", "Creativo") . '" href="https://twitter.com/share?text=' . rawurlencode( html_entity_decode( get_the_title(), ENT_COMPAT, 'UTF-8' ) ) . '&url=' . rawurlencode( get_the_permalink() ) . '" target="_blank"><i class="fa fa-twitter"></i></a></li>';
  } 

  if($data['share_linkedin'] !='no') {
    $content .= '<li><a class="lnk ntip" title="' . __("Share on LinkedIn", "Creativo") .'" href="https://www.linkedin.com/shareArticle?mini=true&url=' . get_the_permalink() . '&amp;title=' . rawurlencode( get_the_title() ) . '&amp;summary=' . rawurlencode( get_the_excerpt() ) .'" target="_blank"><i class="fa fa-linkedin"></i></a></li>';
  }

  if($data['share_pinterest'] !='no') {
    $content .= '<li><a class="pinterest ntip" title="'. __("Share on Pinterest", "Creativo").'" href="http://pinterest.com/pin/create/button/?url=' . urlencode( get_the_permalink() ) . '&amp;description=' . rawurlencode( get_the_excerpt() ) . '&amp;media=' . rawurlencode( wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ) ) .'" target="_blank"><i class="fa fa-pinterest"></i></a></li>';
  }

  if($data['share_reddit'] !='no') {
    $content .= '<li><a class="rd ntip" title="' . __("Share on Reddit", "Creativo") .'" href="http://reddit.com/submit?url=' . get_the_permalink() . '&amp;title=' . rawurlencode( get_the_title() ) .'" target="_blank"><i class="fa fa-reddit"></i></a></li>';
  }

  if($data['share_tumblr'] !='no') {
    $content .= '<li><a class="tu ntip" title="'. __("Share on Tumblr", "Creativo").'" href="http://www.tumblr.com/share/link?url=' . rawurlencode( get_the_permalink() ) . '&amp;name=' . rawurlencode( get_the_title() ) .'&amp;description=' . rawurlencode( get_the_excerpt() ) .'" target="_blank"><i class="fa fa-tumblr"></i></a></li>';
  }

  if($data['share_gplus'] !='no') {
    $content .= '<li><a class="gp ntip" title="'. __("Share on Google+", "Creativo").'" href="https://plus.google.com/share?url=' . get_the_permalink() .'" target="_blank"><i class="fa fa-google-plus"></i></a></li>';
  }  

  $content .= '</ul>';

  echo $content;
}

function cr_post_category() {
  global $data;
  if( $data['show_categories'] ) {
    $categories = get_the_category();
    if ( ! empty( $categories ) ) {
      foreach( $categories as $category ) {
      $cat_output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( 'View all posts in %s', 'Creativo' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . ', ';
      }   
    }
    if( $data['show_categories'] ){
      $content .= '<ul class="post_meta title_'.$data['post_meta_category_pos'].'">';
          $content .= '<li class="category_output">' . $categ_icon . trim( $cat_output, ', ' ) . '</li>';
      $content .= '</ul>';
    }
    echo $content;
  }
}

/* Create the Single Post Title Function */
function cr_post_meta_title() {
  global $data;
  /* Generate the Post Title tag - for Single Post or Archive page */
  $post_title_tag = is_single() ? ( $data['post_title_tag'] ? $data['post_title_tag'] : 'h1' ) : ($data['archives_post_title_tag'] ? $data['archives_post_title_tag'] : 'h2' );

  /* Generate extra class for title, depending on location: single or archive pages */
  $post_title_class = is_single() ? 'singlepost_title' : 'archives_title';

  /* Check the position of the Post Title and Meta */
  $post_meta_position = (get_post_meta(get_the_ID(), 'pyre_post_title_meta_pos', true) != NULL) ? get_post_meta(get_the_ID(), 'pyre_post_title_meta_pos', true) : $data['post_meta_style'];
  if( $post_meta_position == 'default' ) {
    $post_meta_position = $data['post_meta_style'];
  }

  /* Generate the actual title - no link for Single Post, with link for Archive Pages */
  $title = is_single() ? get_the_title() : '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a>';
  
  /* Render the Title tag and Title Content */
  $content = '<' . $post_title_tag .' class="' . $post_title_class . '">' . $title .'</' . $post_title_tag . '>';

  return $content;
}


/* Create the Post Meta Function - used for Single and Archive Pages */
function cr_post_meta() {
    global $data, $post;
    $content = $author_icon = $date_icon = $categ_icon = '';
    
    if( $data['post_meta_design'] != 'modern' ) {
      $author_icon = '<i class="fa fa-user"></i>';
      $date_icon = '<i class="fa fa-clock-o"></i>';
      $categ_icon = '<i class="fa fa-bookmark"></i>';
    }

    if( $data['show_categories'] ) {
      $categories = get_the_category();
      if ( ! empty( $categories ) ) {
        foreach( $categories as $category ) {
        $cat_output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( 'View all posts in %s', 'Creativo' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . ', ';
        }   
      }
    }

    if( $data['show_categories'] && ($data['post_meta_category_pos'] == 'above' )){
      $content .= '<ul class="post_meta above_title">';
          $content .= '<li class="category_output">' . $categ_icon . trim( $cat_output, ', ' ) . '</li>';
      $content .= '</ul>';
    }
       
    /* Render the Title tag and Title Content */
    $content .= cr_post_meta_title();
    
    
    if( $data['show_date'] || $data['show_author'] || $data['show_categories'] || $data['show_comments'] ) {
      $content .= '<ul class="post_meta ' . ( $post_meta_position != 'above' ? 'default' : 'style2' ) . '">';      

      if($data['show_author'] && !is_single()) {
        $content .= '<li>' . $author_icon . __('by ','Creativo') . get_the_author_posts_link() . '</li>';
      }

      if($data['show_author_single'] && is_single()) {
        $content .= '<li>' . $author_icon . __('by ','Creativo') . get_the_author_posts_link() . '</li>';
      }

      if( $data['show_date'] ) {
        $content .= '<li>' . $date_icon . get_the_time( get_option('date_format') ) . '</li>';
      }

      if( $data['show_categories'] && ($data['post_meta_category_pos'] != 'above' ) ) { 
        $content .= '<li>' . $categ_icon . trim( $cat_output, ', ' ) . '</li>';
      }

      if($data['show_comments']) {
        $content .= '<li><a href="' . get_comments_link() .'" class="comments_count"><i class="icon-message"></i>'. get_comments_number() . '</a></li>';
      }

      $content .= '</ul>';
    }

    echo '<div class="post_meta_wrap">'.$content.'</div>';
}

function single_post_title_modern() {

  global $data;

  if($data['post_meta_category_pos'] == 'above') { 
    cr_post_category();
    echo cr_post_meta_title();
    cr_post_meta_ul_only();             
  }
  else {
    echo cr_post_meta_title();              
    //cr_post_category(); 
    cr_post_meta_ul_only(); 
  }
        
}

function cr_post_meta_ul_only() {

  global $data;

  if( ( $data['show_date'] || $data['show_author'] || $data['show_categories'] || $data['show_comments'] ) && $data['post_meta_design']=='modern' ) {
    $content .= '<ul class="post_meta modern_layout">';     

  if(is_single() && $data['single_post_design'] =='modern') {
    if( $data['show_categories'] ) {
      $categories = get_the_category();
      if ( ! empty( $categories ) ) {
        foreach( $categories as $category ) {
        $cat_output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( 'View all posts in %s', 'Creativo' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . ', ';
        }   
      }
    }    
  }    
  if($data['show_categories'] && $data['post_meta_category_pos'] != 'above') {
    $content .= '<li>' . trim( $cat_output, ', ' ) . '</li>';
  }

  if($data['show_author'] && !is_single()) {
    $content .= '<li>' . __('by ','Creativo') . get_the_author_posts_link() . '</li>';
  }
  
  if($data['show_author_single'] && is_single()) {
    $content .= '<li>' . __('by ','Creativo') . get_the_author_posts_link() . '</li>';
  }

  if( $data['show_date'] ) {
    $content .= '<li>' . get_the_time( get_option('date_format') ) . '</li>';
  }

  if($data['show_comments']) {
    $content .= '<li class="comments_count"><a href="' . get_comments_link() .'"><i class="icon-message"></i>'. get_comments_number() . '</a></li>';
  }

    $content .= '</ul>';
  }

  echo $content;

}

function cr_post_meta_ul_only_big_images() {

  global $data;

  $ul_class = is_page_template('page-blog.php') ? 'big_images' : 'small_images';

  if( ( $data['show_date'] || $data['show_author'] || $data['show_categories'] || $data['show_comments'] ) && $data['post_meta_design']=='modern' ) {
    $content .= '<ul class="post_meta modern_layout '.$ul_class.'">'; 

  if($data['show_comments']) {
    $content .= '<li class="comments_count"><a href="' . get_comments_link() .'"><i class="icon-message"></i>'. get_comments_number() . '</a></li>';
  }       

  if($data['show_author']) {
    $content .= '<li>' . __('by ','Creativo') . get_the_author_posts_link() . '</li>';
  }

  if( $data['show_date'] ) {
    $content .= '<li>' . get_the_time( get_option('date_format') ) . '</li>';
  }  

    $content .= '</ul>';
  }

  echo $content;

}

function cr_featured_images( $thumbnail, $thumbnail_to_search) {
  global $data;
  $content = '';

  if(has_post_thumbnail() || get_post_meta(get_the_ID(), 'pyre_youtube', true) || get_post_meta(get_the_ID(), 'pyre_vimeo', true)) {
    $content .= '<div class="flexslider" data-interval="0" data-flex_fx="fade">';
      $content .= '<ul class="slides">';
        
        if( get_post_meta( get_the_ID(), 'pyre_youtube', true ) != '' ) {
          $content .= '<li class="video-container">';
            $content .= do_shortcode('[youtube id="'.get_post_meta(get_the_ID(), 'pyre_youtube', true).'"]');
          $content .= '</li>';
        }

        if( get_post_meta( get_the_ID(), 'pyre_vimeo', true ) != '' ) {
          $content .= '<li class="video-container">';
            $content .= do_shortcode('[vimeo id="'.get_post_meta(get_the_ID(), 'pyre_vimeo', true).'"]');
          $content .= '</li>';
        }

        /* Extra Featured Images */

        $extra ='';                                 
        $i = 2;
        while($i <= $data['featured_images_count']):
            $attachment = new StdClass();
            $attachment_id = kd_mfi_get_featured_image_id('featured-image-'.$i, 'post');
            if($attachment_id):                                     
                $attachment_image = wp_get_attachment_image_src($attachment_id, $thumbnail); 
                $full_image = wp_get_attachment_image_src($attachment_id, 'full'); 
                $attachment_data = wp_get_attachment_metadata($attachment_id);  
                if($attachment_image && strpos($attachment_image[0], $thumbnail_to_search)) {                                    
                    $extra .= '<li><a href="'.get_permalink().'"><img src="'.$attachment_image[0].'" alt="'.$attachment_data['image_meta']['title'].'" ></a></li>';  
                }                                          
                else {
                    $extra .= '<li><a href="'.get_permalink().'"><img src="'.$full_image[0].'" alt="'.$attachment_data['image_meta']['title'].'" ></a></li>'; 
                }
            endif; 
            $i++; 
        endwhile;

        /* Featured Images output */

        if(has_post_thumbnail()){  

          $custom_thumb = wp_get_attachment_image_src ( get_post_thumbnail_id( get_the_ID() ), $thumbnail );
          $full_thumb = wp_get_attachment_image_src( get_the_ID(),'full' );   
          
          if($extra == '') {
            $content .= '<li>';
              $content .= '<figure>';
                $content .= '<a href="' . get_permalink() . '">';
                  $content .= '<div class="text-overlay">';
                    $content .= '<div class="info">';
                        $content .= '<i class="fa fa-search"></i>';
                    $content .= '</div>';
                  $content .= '</div>';
                                                                                     
                  if($custom_thumb && $thumbnail_to_search && strpos($custom_thumb[0], $thumbnail_to_search)) {
                    $content .= '<img src="'.$custom_thumb[0].'">';
                  }
                  else {
                    $content .= get_the_post_thumbnail(get_the_ID(), 'full');
                  }   
                  
                $content .= '</a>';
              $content .= '</figure>';
            $content .= '</li>';
          }

          else {
            $content .= '<li>';
              $content .= '<a href="'.get_permalink().'">';
                if($custom_thumb && strpos($custom_thumb[0], $thumbnail_to_search)) {
                  $content .= '<img src="'.$custom_thumb[0].'">';
                }
                else {
                  $content .= get_the_post_thumbnail(get_the_ID(), 'full');
                }                            
              $content .= '</a>';
            $content .= '</li>';                                   
            $content .= $extra;             
          }

        }

      $content .= '</ul>';
    $content .= '</div>';
  }

  echo $content;

}

/* bbPress custom breadcrumb alteration */

function custom_forum_breadcrumb( $args ) {

  $args['sep'] = '/';
  $args['home_text'] = '<i class="fa fa-home"></i>';
  return $args;

}

add_filter( 'bbp_before_get_breadcrumb_parse_args', 'custom_forum_breadcrumb' );

function header_menu_extra_icons() {

  $items = '';

  global $data;

  if($data['woo_cart'] && class_exists('woocommerce')) {
    $count_output = ($data['woo_cart_count'] == 1) ? '<span>'. WC()->cart->get_cart_contents_count().'</span>' : '';
    $items .= '<li class="shopping_cart_icon">';
      $items .= '<a class="shopping-cart" href="'.wc_get_cart_url().'" ><i class="icon-shop"></i>'.$count_output.'</a>';    
      $items .= '<div class="shopping_cart_wrap"></div>';           
    $items .='</li>';
  }

  $items .= '<li class="header_search_li">';
    $items .= '<div id="header_search_wrap">';
      $items .= '<a href="#" id="header-search"><i class="icon-magnifier"></i><i class="icon-cancel"></i></a>';                                
    $items .= '</div>';
  $items .='</li>';  

  if( isset( $data['off_canvas_sidebar'] ) && $data['off_canvas_sidebar'] == true ) {
    $items .= '<li class="side_panel_li">';
      $items .= '<div id="side-panel-trigger" class="side-panel-trigger">';
        $items .= '<a href="#" ><i class="'.$data['off_cnv_icon'].'"></i></a>';                                
      $items .= '</div>';
    $items .='</li>';   
  }

  $items .= '<li class="menu-item-resp responsive-item">';
    $items .= '<div class="responsive-search">';
      $items .= '<form action="" method="get" class="header_search">';
        $items .='<input type="text" name="s" class="form-control" value="" placeholder="">';
        $items .='<input type="submit" value="'.__('GO','Creativo').'" class="responsive_search_submit">';
      $items .= '</form>';
    $items .= '</div>';
  $items .= '</li>';  

  return $items;

}


if( class_exists( 'WooCommerce' ) ) {

  function cr_header_products() {
    $items = '';
    
    global $woocommerce;
    
    if ( $woocommerce->cart->get_cart_contents_count() ) {
      $items .= '<div class="shopping_cart_items">';
      
        foreach($woocommerce->cart->cart_contents as $cart_item):
          $items .= '<div class="cart_item">';
            $items .= '<a class="" href="' . get_permalink( $cart_item['product_id'] ) . '">';
              $items .= get_the_post_thumbnail($cart_item['product_id'], 'shop_thumbnail');
              $items .= '<div class="cart_item_details">';
                $items .= '<span class="cart_item_title">' . $cart_item['data']->post->post_title . '</span>';
                $items .= '<span class="cart_item_price_quantity">' . $cart_item['quantity'] . ' x ' . $woocommerce->cart->get_product_subtotal( $cart_item['data'], 1 ) . '</span>';
              $items .= '</div>';
            $items .= '</a>'; 
          $items .= '</div>';
        endforeach;

        $items .= '<div class="shopping_cart_total">';
          $items .= '<span class="total_text">'. esc_html__( 'Total', 'Creativo' ).'</span>';
          $items .= '<span class="total_value">'. $woocommerce->cart->get_cart_total().'</span>';
        $items .= '</div>';

        $items .= '<div class="cart_checkout">';
          $items .= '<a href="' . get_permalink( get_option( 'woocommerce_cart_page_id' ) ) . '" class="button_header_cart">'. esc_html__( 'View Cart', 'Creativo' ).'</a>';
          $items .= '<a href="' . get_permalink( get_option('woocommerce_checkout_page_id') ) . '" class="button_header_cart inverse">'. esc_html__( 'Checkout', 'Creativo' ).'</a>';
        $items .= '</div>';

      $items .= '</div>';  

    }
    
    return $items;
  }

  // Support email login on my account dropdown
  if ( isset( $_POST['cr_log_box'] ) && 'true' == $_POST['cr_log_box'] ) {
    add_filter( 'authenticate', 'cr_email_login_auth', 10, 3 );
  }
  function cr_email_login_auth( $user, $username, $password ) {
    if ( is_a( $user, 'WP_User' ) ) {
      return $user;
    }

    if ( ! empty( $username ) ) {
      $username = str_replace( '&', '&amp;', stripslashes( $username ) );
      $user = get_user_by( 'email', $username );
      if ( isset( $user, $user->user_login, $user->user_status ) && 0 == (int) $user->user_status ) {
        $username = $user->user_login;
      }
    }

    return wp_authenticate_username_password( null, $username, $password );
  }

  // No redirect on woo my account dropdown login when it fails
  if ( isset( $_POST['cr_log_box'] ) && 'true' == $_POST['cr_log_box'] ) {
    add_action( 'init', 'cr_login_redirect_support' );
  }

  function cr_login_redirect_support() {
    if ( class_exists( 'WooCommerce' ) ) {

      // When on the my account page, do nothing
      if ( ! empty( $_POST['login'] ) && ! empty( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'woocommerce-login' ) ) {
        return;
      }

      add_action( 'login_redirect', 'cr_login_fail', 10, 3 );
    }
  }

  // Creativo Login Fail Test
  function cr_login_fail( $url = '', $raw_url = '', $user = '' ) {
    if ( ! is_account_page() ) {

      if ( isset( $_SERVER ) && isset( $_SERVER['HTTP_REFERER'] ) && $_SERVER['HTTP_REFERER'] ) {
        $referer_array = parse_url( $_SERVER['HTTP_REFERER'] );
        $referer = '//' . $referer_array['host'] . $referer_array['path'];

        // If there's a valid referrer, and it's not the default log-in screen
        if ( ! empty( $referer ) && ! strstr( $referer, 'wp-login' ) && ! strstr( $referer, 'wp-admin' ) ) {
          if ( is_wp_error( $user ) ) {
            // Let's append some information (login=failed) to the URL for the theme to use
            wp_redirect( add_query_arg( array( 'login' => 'failed' ), $referer ) );
          } else {
            wp_redirect( $referer );
          }
          exit;
        } else {
          return $url;
        }
      } else {
        return $url;
      }
    }
  }

  /* Show Product Stock and Availability */
  add_action( 'woocommerce_single_product_summary', 'cr_stock', 10 );

  function cr_stock() {
    global $product, $data;
    
    if( $data['woo_single_prod_stock'] ) {
      $availability      = $product->get_availability();
         
      ?>
      <div class="stock-options clearfix">
      <?php 

      $stock_output = ( $availability['class'] == 'in-stock' ) ? esc_html__( 'In Stock', 'Creativo' ) : esc_html__( 'Out of Stock', 'Creativo' );
      $stock_qty = (int)$product->stock;
      
      echo '<div class="stock-available"><span class="stock-available-text">' . esc_html__('Available', 'Creativo'). ':</span><span class="stock-text ' . $availability['class'] . '"> ' . $stock_output . '</span></div>';
      if( $data['woo_single_prod_stock_qty'] && ( $availability['class'] == 'in-stock' ) && !$product->is_type( 'grouped' ) ) {
        echo '<div class="stock-quantity"><span class="stock-quantity-text">' . esc_html__('Quantity', 'Creativo') . ':</span><span class="qty-text"> '. $stock_qty.'</span></div>';
      } 
      
      ?>
      
      </div>
    <?php
    }
  }

  /* Add Share Product above Product Meta */
  add_action( 'woocommerce_product_meta_end', 'cr_share_product', 10);

  function cr_share_product () {
    global $data;

    if( $data['woo_product_share'] ) {
      ?>
      <div class="cr_product_share clearfix"><span><?php echo esc_html('Share: ','Creativo' );?></span><?php cr_share_post(); ?></div>
      <?php
    }
  }

  /* Change WooCommerce Single Product Title */
  remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5);
  add_action('woocommerce_single_product_summary', 'cr_single_title',5);

  if ( ! function_exists( 'cr_single_title' ) ) {
     function cr_single_title() {
      global $data;

      $html_tag = $data['woo_prod_title_tag'] ? $data['woo_prod_title_tag'] : 'h2';
  ?>
              <?php echo '<' . $html_tag; ?> itemprop="name" class="product_title entry-title woo_single_prod_title"><?php the_title(); ?><?php echo '</'.$html_tag.'>'; ?>
  <?php
      }
  }


}
