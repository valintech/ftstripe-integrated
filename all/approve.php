<?php

//ini_set('display_errors',1);
date_default_timezone_set('Asia/Kolkata');
require_once("classes/root.inc");
require_once('base.inc');
require_once('classes/user.inc');
require_once('classes/common.inc');
require_once('classes/region.inc');
require_once('classes/yahoo.inc');
require_once('classes/google.inc');
require_once('classes/captcha.inc');
require_once('classes/smsgw.inc');
require_once('classes/group.inc');
require_once('classes/usergroup.inc');
if (CConfig::RUN_IN_FB)
    require_once('classes/facebook.inc');

$op = CCommon::getRequestValue('op');
$refer = new CUrl(CCommon::getRequestValue('refer'));
// XmlHttpRequest:- Register
if ($op == 'approve_mail') {
    $url_parameter = CCommon::getRequestValue('url_parameter');

    approveMail($url_parameter);
}
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();
if (!($isValidSession)) {
    CCommon::redirect('login.php?' . $_SERVER['QUERY_STRING']);
}


/*
 * Approve mail and update the staus accordingly
 *  
 */

function approveMail($url_parameter) {
     $approve_status = CCommon::getRequestValue('approve_status');
    $region = new CRegion('approve');
    $encryotedurl = CCommon::mc_decrypt($url_parameter, CConfig::ENCRYPTION_KEY);
    $encrypatedIds = explode(",", $encryotedurl);
    $ownerId = CUserGroup::getGroupOwnerMemeberDetails($encrypatedIds[0]); /* group Id is Passing */
    $updateuserstatus = new CUserGroup();
    $updateuserstatus->set('id', CUserGroupDb::loadIdByOwnerIdAndUserId($encrypatedIds[0], $encrypatedIds[1]));
    $updateuserstatus->set('status', '"' . $approve_status . '"');
    $updatedstatsus = $updateuserstatus->updateStatus();
    if ($updatedstatsus) {
        $userdetail = CUserGroup::getNameById($encrypatedIds[1]);
        $mail_status = CUserGroup::approvemailbody($userdetail, $userdetail, NULL, $ownerId, 'approve_notification');
        if ($mail_status) {
            $out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($region->msgList()));
            $out = "Procces Completed Succefully";

               if($approve_status=='n'){
                  $out_msg = $userdetail['first_name']." ".$userdetail['last_name']." declained succesfully"; 
             }else{
                 $out_msg = $userdetail['first_name']." ".$userdetail['last_name']." has now joined the group";  
             }
            
              $out = array('infoText' => $out_msg);
            
            CCommon::xhrSend(CCommon::toJson($out));
            exit;
        }
    }
}

// Output HTML page
$region = new CRegion('approve');
$rplc = array();
$rplc[1] = $current_status['status'];

$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1100);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
$rplc[19] = scriptLinks();
$rplc[3] = approve_script();
$rplc[20] = script($region);
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$out = CCommon::htmlReplace("approve.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
    @include 'google_analytics.html';


/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks() {
    $out = array();
    $out[] = CGoogle::scriptHtml();
    $out[] = CYahoo::scriptHtml(array('json', 'container', 'dom', 'connection', 'button'));
    if (CConfig::RUN_IN_FB)
        $out[] = CFacebook::scriptHtml();
    $out[] = '<script type="text/javascript" src="js/common.js"></script>';
    $out[] = '<script src="js/xhr.js" type="text/javascript"></script>';
   /* $out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.tokeninput.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery.sumoselect.js"></script>';
    $out[] = '<script type="text/javascript" src="js/jquery_support.js"></script>';
    $out[] = '<script type="text/javascript" src="register.js"></script>';*/
    $out[] = '<script src="approve.js" type="text/javascript"></script>';
    return join("\n", $out);
}

function script($region) {
    $out = array();
    $out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($region->msgList()));
    $out[] = sprintf("var _msgList=new CMsgList('%s');", CCommon::toJson($region->msgList()));
    return join('', $out);
}

function approve_script() {
      $new_urls = $_SERVER['QUERY_STRING'];
    if($isValidSession){
       $new_urls = urldecode($_SERVER['QUERY_STRING']); 
    }
    $currentencryotedurl = CCommon::mc_decrypt(str_replace("approveparam=", "", $new_urls), CConfig::ENCRYPTION_KEY);
    $currentencryotedIds = explode(",", $currentencryotedurl);
    $current_status = CUserGroup::getUserStatusInGroup($currentencryotedIds[0], $currentencryotedIds[1]); /* group Id is Passing */
    $current_status['status'];
    $out = array();
    $out[] = sprintf("var approve_status='%s';", $current_status['status']);
    return join('', $out);
}

?>
