<?php

include_once('PPHttpPost.inc');

// Set request-specific fields.
$emailSubject =urlencode('Funds from eco-gogo');
$receiverType = urlencode('EmailAddress');
$currency = urlencode('GBP');

// Add request-specific fields to the request string.
$nvpStr="&EMAILSUBJECT=$emailSubject&RECEIVERTYPE=$receiverType&CURRENCYCODE=$currency";

$i = 0;
$receiverEmail = urlencode('richardghirst@gmail.com');
$amount = urlencode('10');
$uniqueID = urlencode('a_unique_id');
$note = urlencode('a special note for you');
$nvpStr .= "&L_EMAIL$i=$receiverEmail&L_Amt$i=$amount&L_UNIQUEID$i=$uniqueID&L_NOTE$i=$note";

// Execute the API operation; see the PPHttpPost function above.
$httpParsedResponseAr = PPHttpPost('MassPay', $nvpStr);

if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	exit('MassPay Completed Successfully: ' .print_r($httpParsedResponseAr, true));
} else  {
	exit('MassPay failed: ' . print_r($httpParsedResponseAr, true));
}

?>

