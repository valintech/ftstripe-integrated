function CRequestRide (journeyId)
{
	CRequestRide.prototype._dlg;			// YUI "Dialog" object
	CRequestRide.prototype._dlgId;			// Dialog id.
	CRequestRide.prototype._journeyId;		// Journey id.
	CRequestRide.prototype._msgList;		// CMsgList object
	
	this._journeyId = journeyId;
	this._dlgId = 'requestRideDlg';

	/*
	 * Show dialog
	 */
	
	CRequestRide.prototype.show = function ()
	{
		CConnection.doPost('requestride.php', {op: 'init', journeyId: this._journeyId},
							_response, this);	

		// XmlHttpRequest response handler
		function _response (response)
		{
			this._msgList = new CMsgList(response.msgList);
			
			if (response.errorText)
			{
				new YUI_DIALOG_Popup(response.errorText, this._msgList.msg(1006),
									 'block', this._msgList.msg(1002));
				return;
			}

			var div = document.createElement('div');
			div.id = this._dlgId;
			document.body.appendChild(div);
			CXPlat.setContent(div, response.html);
			
			var myButtons = [
			                {text: this._msgList.msg(1001), handler: {fn: this._btn1Click, scope: this}, isDefault: true},
			                {text: this._msgList.msg(1002), handler: {fn: this._btn2Click, scope: this}}
			                ];
			var cfg = {visible: false, draggable: true, close: false,
					   modal: false, buttons: myButtons,
					   effect: {effect: YAHOO.widget.ContainerEffect.FADE, duration: 1}};
			this._dlg = new YAHOO.widget.Dialog(this._dlgId, cfg);
			this._dlg.render();
			this._dlg.show();
			this._dlg.center();
		}
	};
	
	/*
	 * Send request
	 */
	
	CRequestRide.prototype._btn1Click = function ()
	{
		CONNECTION_Post('requestride.php', {op: 'send', journeyId: this._journeyId,
						requestText: DOM_getElem('comments').value},
						{success: _response, failure: _response, scope: this});	

		// XmlHttpRequest response handler
		function _response (r)
		{
			if (r.status != CConnection.OK)
				return;
			var response = COMMON.fromJson(r.responseText);
			if (response.errorText)
				new YUI_DIALOG_Popup(response.errorText, this._msgList.msg(1006),
									 'block', this._msgList.msg(1002));
			else
			{
				new YUI_DIALOG_Popup(this._msgList.msg(1009), this._msgList.msg(1010),
						 			 'info', this._msgList.msg(1002));
				this._dlg.destroy();
			}
		}
	};
	
	/*
	 * Close form
	 */
	
	CRequestRide.prototype._btn2Click = function ()
	{
		this._dlg.destroy();
	};
}
