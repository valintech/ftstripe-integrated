<?php 
require_once('classes/root.inc');
require_once('includes/config.inc');
require_once('classes/inifile.inc');

/*
 * Regionalisation class
 */

class CRegion extends CRoot
{
	/*
	 * Constructor
	 * 
	 * @param $msgContext  Message context to use
	 */
	function __construct ($msgContext = "")
	{
		parent::__construct();
		if ($msgContext != "")
			$this->setMsgContext($msgContext);
	}

	/*
	 * Return region file name or content
	 * 
	 * @param $fileName       Name of file
	 * @param $returnContent  true to return content instead of name
	 * @return                File name or content
	 */
	
	public static function regionFile ($fileName, $returnContent = false)
	{
		$filePath = sprintf("language/%s/%s", CConfig::LANGUAGE_ID, $fileName);
		return $returnContent ? file_get_contents($filePath) : $filePath;
	}
	
	/*
	 * Get a message
	 * @param $msgNo        Message number
	 * @param $msgContext   Message context to use
	 */
	
	function msg ($msgNo, $msgContext = "")
	{
		$msgList = ($msgContext != "" ? $msgContext : $this->get("msg/currentContext"));
		if ($msgList != '')
			$msgList = $this->_getMsgList($msgList);
		$msg = (isset($msgList) ? $msgList->getIni($msgNo) : false);
		return $msg ? $msg : sprintf("#msg%d", $msgNo);
	}
	
	/*
	 * Return messages as keyed array
	 * 
	 * @param $msgNoList    [optional Array of message numbers or false to return all messages
	 * @param $msgContext   [optional] Message context to use
	 * @return              Keyed array as [Mxx][xxx], [Mxx][xxx], ...	
	 */
	
	function msgList ($msgNoList = false, $msgContext = "")
	{
		$msgContext = ($msgContext != "" ? $msgContext : $this->get("msg/currentContext"));
		$out = array();
		if ($msgNoList)
			foreach ($msgNoList as $msgNo)
				$out['M' . $msgNo] = $this->msg($msgNo, $msgContext);
		else
		{
			$msgList = $this->_getMsgList($msgContext);
			$tmp = $msgList->getSection();
			foreach ($tmp as $msgNo => $msg)
				$out['M' . $msgNo] = $msg;			
		}
		return $out;
	}
	
	/*
	 * Set message context
	 * @param $msgContext   Message context to set
	 */
	
	function setMsgContext ($msgContext)
	{
		$this->set("msg/currentContext", $msgContext);
	}

	/*
	 * Return a message list
	 * 
	 * @param $msgContext  Message context to laod
	 * @param @setActive   Set as active context
	 * @return true or false
	 */
	
	protected function _getMsgList ($msgContext, $setActive = false)
	{
		$out = $this->get(sprintf("msg/%s", $msgContext));
		if ($out == "")
		{
			$out = new CIniFile();
			if ($out->load(sprintf("language/%s/%s.ini", CConfig::LANGUAGE_ID, $msgContext)))
				$this->set(sprintf("msg/%s", $msgContext), serialize($out));
		}
		else
			$out = CIniFile::createFromStream('CIniFile', $out);
		if ($out && $setActive)
			$this->setMsgContext($msgContext);
		return $out;
	}
}
?>
