<?php
require_once("classes/root.inc");

class CWords extends CRoot
{

	public static function getone ()
	{
		static $words = array(
			"Aardvark",
			"African Elephant",
			"African Wild Dog",
			"African Lion",
			"Arabian Camel",
			"Arctic Fox",
			"Arctic Hare",
			"Armadillo",
			"Asian Elephant",
			"Asian Lion",
			"Aye-Aye",
			"Baboon",
			"Bactrian Camel",
			"Beaver",
			"Beluga Whale",
			"Bengal Tiger",
			"Black Bear",
			"Black-Footed Ferret",
			"Black Rhinoceros",
			"Blue Whale",
			"Bobcat",
			"Bottlenose Dolphin",
			"Brown Bear",
			"California Sea Lion",
			"Caribou",
			"Cheetah",
			"Chimpanzee",
			"Chipmunk",
			"Clouded Leopard",
			"Common Vampire Bat",
			"Common Wombat",
			"Cottontail Rabbit",
			"Coyote",
			"Dingo",
			"Domestic Cat",
			"Domestic Dog",
			"Dugong",
			"Elephant Seal",
			"Elk",
			"Fennec Fox",
			"Fossa",
			"Fur Seal",
			"Gelada",
			"Giant Anteater",
			"Giant Panda",
			"Giant River Otter",
			"Gibbon",
			"Giraffe",
			"Golden Lion Tamarin",
			"Eastern Gray Kangaroo",
			"Gray Whale",
			"Grizzly Bear",
			"Groundhog",
			"Harbor Porpoise",
			"Harp Seal",
			"Hawaiian Monk Seal",
			"Hedgehog",
			"Hippopotamus",
			"Horse",
			"Howler Monkey",
			"Humpback Whale",
			"Impala",
			"Indian Rhinoceros",
			"Jackrabbit",
			"Jaguar",
			"Killer Whale",
			"Kinkajou",
			"Koala",
			"Leopard",
			"Leopard Seal",
			"Little Red Flying Fox",
			"Llama",
			"Lynx",
			"Manatee",
			"Mandrill",
			"Mongoose",
			"Moose",
			"Mountain Goat",
			"Mountain Gorilla",
			"Mountain Lion",
			"Mouse Lemur",
			"Musk-Ox",
			"Mole Rat",
			"Narwhal",
			"North American River Otter",
			"Nutria",
			"Ocelot",
			"Opossum",
			"Orangutan",
			"Ozark Big-Eared Bat",
			"Platypus",
			"Polar Bear",
			"Porcupine",
			"Prairie Dog",
			"Proboscis Monkey",
			"Pronghorn",
			"Przewalski's Horse",
			"Raccoon",
			"Red Fox",
			"Red Kangaroo",
			"Red Leaf Monkey",
			"Red Panda",
			"Red Uakari",
			"Rhesus Monkey",
			"Right Whale",
			"Ringed Seal",
			"Ring-Tailed Lemur",
			"Rocky Mountain Bighorn Sheep",
			"Sea Otter",
			"Siberian Tiger",
			"Sifaka",
			"Skunk",
			"Sloth Bear",
			"Snow Leopard",
			"Snowshoe Hare",
			"Spectacled Bear",
			"Sperm Whale",
			"Spider Monkey",
			"Spotted Hyena",
			"Squirrel",
			"Steller Sea Lion",
			"Sumatran Rhinoceros",
			"Sun Bear",
			"Tapir",
			"Tasmanian Devil",
			"Thomson's Gazelle",
			"Three-Toed Sloth",
			"Two-Toed Sloth",
			"Wallaby",
			"Walrus",
			"Warthog",
			"Water Buffalo",
			"Weddell Seal",
			"Western Lowland Gorilla",
			"White Rhinoceros",
			"White-Eared Kob",
			"White-Tailed Deer",
			"Wildebeest",
			"Wolf",
			"Wolverine",
			"Zebra"
		);
		$wordcount = count($words);
		return $words[rand(0,$wordcount-1)];
	}
}

