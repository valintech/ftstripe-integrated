<?php
/*
 * Root for all classes
 */

class CRoot
{
	/*
	 * Class members
	 */
	
	protected $values;
	protected static $reflect;
	protected $xmlRootTag;
	
	/*
	 * Constructor
	 * 
	 */
	
	public function __construct ($xmlRootTag = 'root')
	{
		$this->clear();
		$this->xmlRootTag = $xmlRootTag;
	}
	
	/*
	 * Return class values
	 * 
	 * @return  Associate array of class members
	 */
	
	public function getValues ()
	{
		return $this->values;
	}
	
	/*
	 * Set a value
	 * 
	 * @param $key     A key path for the given value i.e "val1", "val2", "section1/val1",
	 *                 "section1/val2"
	 * @param $value   Value to store against the key
	 */

	public function set ($key, $value)
	{
		if (strpos($key, '/') === false)
			$this->values[$key] = $value;
		else
		{
			$keyParts = explode('/', $key);
			$dest = &$this->values;
			for ($i = 0; $i < count($keyParts) - 1; $i++)
			{
				$nextPart = $keyParts[$i];
				if (isset($dest[$nextPart]) == false)
					$dest[$nextPart] = array();
				$dest = &$dest[$nextPart];
			}
			$dest[$keyParts[$i]] = $value;
		}	
	}
	
	/*
	 * Get a value
	 * 
	 * @param $key       A key path for the given value i.e "val1", "val2", "section1/val1",
	 *                   "section1/val2"
	 * @param $default   Default value to return if "$key" value is not defined
	 * @return           The value for the key
	 */
	
	public function get ($key, $default = '')
	{
		if (strpos($key, '/') === false) {
			if (array_key_exists($key, $this->values))
				$out = $this->values[$key];
//			else {
//				CLogging::info('Attempt to read undefined member "'.$key.'" from '.get_class($this));
//				array_walk(debug_backtrace(), create_function('$a,$b', 'CLogging::info(\'Trace: \'.$a[\'file\'].\' \'.$a[\'function\'].\' line \'.$a[\'line\']);'));
//			}
		}
		else
		{
			$keyParts = explode('/', $key);
			$dest = &$this->values;
			for ($i = 0; $i < count($keyParts) - 1; $i++)
			{
				$nextPart = $keyParts[$i];
				if (isset($dest[$nextPart]) == false)
					$dest[$nextPart] = array();
				$dest = &$dest[$nextPart];
			}
			$out = $dest[$keyParts[$i]];
		}
		return isset($out) ? $out : $default;
	}

	/*
	 * Export class as XML
	 * 
	 * @return XML
	 */
	
	public function toXml ($_parent = false, $_values = false)
	{
		if ($_parent == false)
		{
			$dom = new domdocument();
			$_parent = $dom->appendChild($dom->createElement($this->xmlRootTag));
			$_values = $this->values;
		}

		foreach ($_values as $k => $v)
			if (is_array($v))
			{
				$_parent->appendChild($dom->createElement($k));
				$this->toXml($_parent, $v);
			}
			else
			{
				if (self::_attributeCandidate($v))
					$_parent->setAttribute($k, $v);
				else
				{
					$search = array('&', "'", '"', '<', '>');
					$replace = array('&amp;', '&apos;', '&quot;', '&lt;', '&gt;');
					$v = str_replace($search, $replace, $v);
					$_parent->appendChild($dom->createElement($k, $v));
				}
			}

		return $dom->savexml($dom->documentElement);
	}

	/*
	 * Return true if value could be stored as attribute
	 * 
	 * @return true or false
	 */
	
	private static function _attributeCandidate ($value)
	{
		//return preg_match('/^[:alnum:]+$/', $value);
		return false; //preg_match('/^[\d]+$/', $value);
	}
	
	/*
	 * Export class as associative array
	 * 
	 * @return              Associative array of class values
	 */
	
	public function toArray ()
	{
		return $this->values;
	}
	
	/*
	 * Export class data as JSON
	 * 
	 * @return JSON representation of data
	 */
	
	public function toJson ()
	{
		return json_encode($this->values);
	}

	/*
	 * Import class data as JSON
	 * 
	 * @param  $jsonData  JSON representation of data
	 */
	
	public function fromJson ($jsonData)
	{
		$this->values = json_decode($jsonData);	
	}
	
	/*
	 * Create an object from serialised stream
	 * 
	 * @param $className   Class of object to create
	 * @param $data        Serialised object data
	 * @return             Object
	 */
	
	public static function createFromStream ($className, $data)
	{
		return isset($data) ? unserialize($data) : new $className();
	}
	
	/*
	 * Log values
	 */
	
	public function logValues ()
	{
		CLogging::debug(sprintf('CRoot::logValues - Class (%s)', $this->className));
		foreach ($this->values as $key => $val)
			CLogging::debug(sprintf('%s=%s', $key, $val));
	}

	/*
	 * Establish Reflection object for a class
	 * 
	 * @param $className      Name of class
	 * @return                Reflection object
	 */
	
	protected static function _establishReflect ($className)
	{
		if (isset(CRoot::$reflect) == false)
			CRoot::$reflect = array();
		if (isset(CRoot::$reflect[$className]) == false)
			CRoot::$reflect[$className] = new ReflectionClass($className);
		return CRoot::$reflect[$className];
	}
	
	/*
	 * Get list of defined constants for a class
	 * 
	 * @param $className    Name of class
	 * @return              Array of constant names
	 */

	public static function getClassConstants ($className)
	{
		$r = CRoot::_establishReflect($className);
		return $r->getConstants();
	}
	
	/*
	 * Get list of static properties for a class
	 * 
	 * @param $className      Name of class
	 * @param $prefixFilter   [optional] Filter returned list by prefix
	 * @return                Keyed array of property names/values
	 */
	
	public static function getStaticMembers ($className, $prefixFilter = '')
	{
		$r = CRoot::_establishReflect($className);
		$out = $r->getStaticProperties();
		if ($prefixFilter != '')
			foreach ($out as $key => $val)
				if (strpos($key, $prefixFilter) !== 0)
					unset($out[$key]);
		return $out;
	}
	
	/*
	 * Get member property definitions with webservice tags
	 * 
	 * @param $className    Name of class
	 * @return 				Keyed array where key is member name, value is an array containing
	 *         				'key', 'type' and 'size' members. The 'key' member defines the access
	 *         				key to retrieve the member via the 'CRoot::get' method, 'type' is the
	 *         				member type ( see CRoot xxx_MEMBER class constants ) and 'size' is the
	 *         				field size. Only applies to string and integer fields.
	 */
	
	public static function getWsDbMembers ($className)
	{
		$tmp = CRoot::getStaticMembers($className, 'DBM');
		$out = array();
		foreach ($tmp as $val)
			if ($val['wsKey'])
			{
				$key = $val['key'];
				$out[$key] = $val;
			}
		return $out;
	}
	
	/*
	 * Get list of static, database specific member properties from a class
	 * 
	 * @param $className    Name of class
	 * @return 				Keyed array where key is member name, value is an array containing
	 *         				'key', 'type' and 'size' members. The 'key' member defines the access
	 *         				key to retrieve the member via the 'CRoot::get' method, 'type' is the
	 *         				member type ( see CRoot xxx_MEMBER class constants ) and 'size' is the
	 *         				field size. Only applies to string and integer fields.
	 */
	
	public static function getDbMembers ($className)
	{
		$tmp = CRoot::getStaticMembers($className, 'DBM');
		$out = array();
		foreach ($tmp as $val)
		{
			$key = $val['key'];
			unset($val['key']);
			$out[$key] = $val;
		}
		return $out;
	}
	
	/*
	 * Return filtered list of data columns for a user
	 * 
	 * @param $className    Name of class
	 * @param $include      [optional Array of columns ids. to include in returned list
	 * @param $exclue       [optional Array of columns ids. to exclude from returned list
	 * @return              Array of arrays containing column id. and data type
	 */

	public static function getFilteredDbMembers ($className, $includeIds = false, $excludeIds = false)
	{
		$cols = CRoot::getDbMembers($className);
		if (is_array($includeIds))
			foreach ($cols as $colId => $col)
				if (in_array($colId, $includeIds) == false)
					unset($cols[$colId]);
		if (is_array($excludeIds))
			foreach ($cols as $colId => $col)
				if (in_array($colId, $excludeIds))
					unset($cols[$colId]);
		return $cols;
	}
	
	/*
	 * Format a class as a Javascript class definition
	 * 
	 * @param $className    Name of class
	 * @param $include      [optional Array of columns ids. to include in returned list
	 * @param $exclue       [optional Array of columns ids. to exclude from returned list
	 * @return              An array of lines making up a Javascript
	 * 						class definition.
	 */
	
	public static function formatClassAsJs ($className, $includeIds = false, $excludeIds = false)
	{
		$constants = CRoot::getClassConstants($className);
		if (is_array($includeIds))
			foreach ($constants as $key => $val)
				if (in_array($val, $includeIds) == false)
					unset($constants[$key]);
		if (is_array($excludeIds))
			foreach ($constants as $key => $val)
				if (in_array($val, $excludeIds))
					unset($constants[$key]);
		$out = array();
		$out[] = sprintf('function %s (classData)', $className);
		$out[] = '{';
		foreach ($constants as $key => $val)
			$out[] = sprintf('%s.%s="%s";', $className, $key, $val);
		$out[] = '}';
		$out[] = sprintf('new %s();', $className);
		return $out;
	}

	/*
	 * Clear all members from object
	 */
	
	public function clear ()
	{
		$this->values = array();
	}
}
?>
