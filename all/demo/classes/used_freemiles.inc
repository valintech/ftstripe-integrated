<?php
require_once('db/used_freemilesdb.inc');
//require_once('classes/user.inc');
require_once("classes/root.inc");
/*
 * Journey
 */

class CUsedFreeMiles extends CRoot
{
	// Members relating to databsae columns
	const ID = 'id';
	const USERID = 'user_id';
	const USEDMILES = 'used_miles';
	const JOURNEYID = 'journey_id';
        const CREATEDBY = 'created_at';
	// Database column member definitions
	private static $DBM01 = array('key' => CUsedFreeMiles::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CUsedFreeMiles::USERID, 'type' => CCommon::INT_TYPE, 'size' => 11);
        private static $DBM03 = array('key' => CUsedFreeMiles::USEDMILES, 'type' => CCommon::INT_TYPE, 'size' => 11);
        private static $DBM04 = array('key' => CUsedFreeMiles::JOURNEYID, 'type' => CCommon::INT_TYPE, 'size' => 2);
        private static $DBM05 = array('key' => CUsedFreeMiles::USEDMILES, 'type' => CCommon::INT_TYPE, 'size' => 5);
         private static $DBM06 = array('key' => CUsedFreeMiles::CREATEDBY, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
 

	/*
	 * Constructor
	 */

	function __construct ()
	{
		parent::__construct();
	}


         public function assignNormalUserFreeMiles() {
        return CUsedFreeMilesDb::save($this);
    }

 public function assignedUserFreeMiles($userId) {
        return CUsedFreeMilesDb::assignedUserFreeMiles($userId);
    }

   
}
?>
