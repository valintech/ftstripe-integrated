<?php
require_once('classes/common.inc');
require_once('classes/login.inc');

/*
 * SMS class (Webservice)
 */

class CMsgsWs
{
	/**
	 * getMessages - retrieve pending messages
	 * 
	 * @param int $sessionId       Session id.
	 * @return object              stdClass object containing method return or
         *                             'errorId' and 'errorText' members on error
         *                             'errorId' and 'errorText' members on error
         */

	public static function getMessages ($sessionId)
	{
		$login = new CLogin($sessionId);
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$userId = $login->userId();
		$out = new stdClass;
		$msgs = array();
		$msgs[23] = 'message twentythree';
		$msgs[27] = 'This is all I wanted to say';
		$out->msgs = $msgs;
		return $out;
	}
}
