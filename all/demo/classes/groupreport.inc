<?php

require_once("classes/root.inc");
require_once("classes/websession.inc");
require_once("db/groupreportdb.inc");

/*
 * Group class
 */

class CGroupReport extends CRoot {
    // Standard values
    const ID = 'id';
    const GROUP_ID = 'group_id';
    const CARBON_SAVINGS = 'carbon_savings';
    const MONTH = 'month';
    const YEAR = 'year';
    const TOTAL_MILEAGE = 'total_mileage';
    const CREATED = 'created';
    const MAIL_STATUS = 'mail_status';

    private static $DBM01 = array('key' => CGroupReport::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM02 = array('key' => CGroupReport::CARBON_SAVINGS, 'type' => CCommon::FLOAT_TYPE);
    private static $DBM03 = array('key' => CGroupReport::GROUP_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM04 = array('key' => CGroupReport::CREATED, 'type' => CCommon::DATETIME_TYPE);
    private static $DBM05 = array('key' => CGroupReport::MAIL_STATUS, 'type' => CCommon::STRING_TYPE, 'size' => 1);
    private static $DBM06 = array('key' => CGroupReport::MONTH, 'type' => CCommon::INT_TYPE, 'size' => 4);
    private static $DBM07 = array('key' => CGroupReport::YEAR, 'type' => CCommon::INT_TYPE, 'size' => 4);
    private static $DBM08 = array('key' => CGroupReport::TOTAL_MILEAGE,'type' => CCommon::FLOAT_TYPE);

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct();
    }

    
   /*
     * Save user details
     * @return   true or false 
     */

    public function save() {
        return CGroupReportDb::save($this);
    }





    /*
     * Updfate mail status
     * @return   true or false 
     */

    public function updateStatus($id) {
        
        return CGroupReportDb::updateStatus($id);
    }

    /*
     * Load a group report by month.
     * 
     * @param $id   Id
     * @return      true of false
     */

    public function loadByMonth($group_id, $month,$year) {
           return CGroupReportDb::loadByMonth($group_id, $month,$year);
    }
     public function loadAllGroup($user_id) {
           return CGroupReportDb::loadAllGroup($user_id, $this);
    }
/*
     * Load a group by id.
     * 
     * @param $id   Id
     * @return      true of false
     */

    public function load($id) {
        CGroupReportDb::load($id, $this);
    }

    /*
     * Establish group for a user
     * 
     * @param $userId      User id.
     * @param $groupName   Carpool name
     */

    public function establishGroup($userId, $groupName = '') {
        
    }
    /*
    * Get Total Miles travelled in a month of a  group  by each user
    *  * @param $Month      Last Month.
    * @param  $year         Current Year/ in case of january year will be last year
    *  return MIles
    */

   public function totalMilesOfTheMonthByGroup($Month, $year) {
       return CGroupReportDb::totalMilesOfTheMonthByGroup($Month, $year);
       
   }

}

?>
