<?php 
require_once('thirdparty/facebook-0.1.0/facebook.php');
require_once('classes/common.inc');
require_once('classes/logging.inc');
require_once('classes/user.inc');

/*
 * Class to handle Facebook integration
 */

class CFacebook
{
	// Possible values to pass to Facebook API 'getinfo' method
	const GETINFO_UID = 'uid';
	const GETINFO_NAME = 'name';
	const GETINFO_LAST_NAME = 'last_name';
	const GETINFO_FIRST_NAME = 'first_name';
	const GETINFO_PROXIED_EMAIL = 'proxied_email';
	const GETINFO_EMAIL = 'mail';
	const GETINFO_SEX = 'sex';
	
	const FACEBOOK_MALE = 'male';							// Gender available from 'getInfo' API call in the 'sex' field.
	const FACEBOOK_FEMALE = 'female';						// Gender available from 'getInfo' API call in the 'sex' field.
	
	const FACEBOOK_MAIL_PERM = 'email';						// Email extended permission
	
	// Class variables
	protected static $fb = null;						// A 'Facebook' object
	protected static $fbParams = array();				// Array of values passed into Facebook application
	
	/*
	 * Initialise Facebook PHP interface object
	 */
	
	public static function init ()
	{
		//required for IE in iframe FB environments if sessions are to work.
		header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		
		CLogging::debug('CFacebook::init');

		if (CConfig::DEBUG == 0)
		{
			CCommon::dumpGetPostCookeVals();
			self::$fb = new Facebook(CConfig::FB_API_KEY, CConfig::FB_SECRET);
			if (self::$fb->fb_params['app_id'] != '')
			{
				foreach (self::$fb->fb_params as $k => $v)
					self::$fbParams[$k] = $v;
				$user = self::$fb->get_loggedin_user();
				if ($user == 0)
				{
					$perms = array(self::FACEBOOK_MAIL_PERM);
					self::$fb->redirect(self::$fb->get_login_url(sprintf('%s/%s/', CConfig::FB_APP_URL,
																 CConfig::FB_CANVAS_APP_NAME), true,
																 join(',', $perms)));
				}
				//self::$fb->require_login($required_permissions = 'email');
			}
			else
			{
				$prefix = sprintf('%s_', CConfig::FB_API_KEY);
				$prefixLen = strlen($prefix);
				foreach ($_COOKIE as $k => $v)
					if (strpos($k, $prefix) === 0)
					{
						$k = substr($k, $prefixLen);
						self::$fbParams[$k] = $v;
					}
			}
			
			CLogging::info('-------- F/B Params ---------');
			foreach (self::$fbParams as $k => $v)
				CLogging::info(sprintf('%s=%s', $k, $v));
			CLogging::info('-----------------------------');
			
			//$s = self::$fb->do_get_session($_REQUEST['auth_token']);
			//self::$fb->set_user($s['uid'], $s['session_key'], $s['expires'], $s['secret']);			
			//self::$fb->validate_fb_params();		
			//$p = self::$fb->get_valid_fb_params($_REQUEST);
		}
		else
		{
			CFacebook::$fbParams = array();
			CFacebook::$fbParams['in_iframe'] = 1;
			CFacebook::$fbParams['iframe_key'] = 'c16a5320fa475530d9583c34fd356ef5';
			CFacebook::$fbParams['base_domain'] = '=erideshare.co.uk';
			CFacebook::$fbParams['locale'] = 'en_GB';
			CFacebook::$fbParams['in_new_facebook'] = 1;
			CFacebook::$fbParams['time'] = '1270087354.5136';
			CFacebook::$fbParams['added'] = 1;
			CFacebook::$fbParams['profile_update_time'] = '1267969482';
			CFacebook::$fbParams['expires'] = '1270094400';
			CFacebook::$fbParams['user'] = '100000844235714';
			CFacebook::$fbParams['session_key'] = '2.tvfBF_4CwcOxw_ltr9QLtw__.3600.1270094400-100000844235714';
			CFacebook::$fbParams['ss'] = '_oBd__m0gkAtRZhIuBfMWg__';
			CFacebook::$fbParams['cookie_sig'] = 'b564e2af7625c6ee08891124e803a9f3';
			CFacebook::$fbParams['ext_perms'] = 'auto_publish_recent_activity';
			CFacebook::$fbParams['api_key'] = '00be8925ac51c8c9b4502b49e2b509a4';
			CFacebook::$fbParams['app_id'] = '382392681825';
			CFacebook::$fb = new stdClass();
		}

		// Ensure Facebook session is available
		self::requireSession();	
		
		// Set PHP session id. to Facebook session id.
		session_id(md5(self::getSessionId()));
	}
	
	/*
	 * Get a Facebook parameter
	 * 
	 * For example, 'user', 'session_key' etc.
	 * Got to http://wiki.developers.facebook.com/index.php/Authorizing_Applications#Parameters_Passed_to_Your_Application
	 * for a comprehensive list of parameters passed to Facebook 'canvas' applications. Note that they are
	 * stored in this object without the prefix 'fb_sig_'.
	 * 
	 * @param $key  Facebook parameter key
	 * @return      The corresponding value
	 */
	
	public static function getFbParam ($key)
	{
		return self::$fbParams[$key];
	}
	
	/*
	 * Get Facebook user information
	 * 
	 * @param $userIds  A user id or array of user ids
	 * @return          Array of user information. Each entry is an array containing
	 *                  'name', 'first_name', 'last_name' entries.
	 *                  Return false if could not retrieve details
	 */
	
	public static function getUserInfo ($userIds)
	{
		CLogging::debug(sprintf('CFacebook::getUserInfo - (%s)', (is_array($userIds) ? join(', ', $userIds) : $userIds)));
//		$userInfoKeys = array(self::GETINFO_UID, self::GETINFO_NAME, self::GETINFO_LAST_NAME, self::GETINFO_FIRST_NAME,
//							  self::GETINFO_PROXIED_EMAIL, self::GETINFO_EMAIL, self::GETINFO_SEX);
		$userInfoKeys = array(self::GETINFO_UID, self::GETINFO_NAME, self::GETINFO_LAST_NAME, self::GETINFO_FIRST_NAME,
							  self::GETINFO_SEX);
		$userInfoList = CFacebook::_fbGetInfo($userIds, $userInfoKeys);
		if ($userInfoList == false)
			CLogging::debug('CFacebook::getUserInfo - FAILED');
		else
			foreach ($userInfoList as $userInfo)
			{
				$tmp = array();
				foreach ($userInfoKeys as $key)
					$tmp[] = sprintf('%s=%s', $key, $userInfo[$key]);
				CLogging::debug(sprintf('CFacebook::getUserInfo - (%s)', join(', ', $tmp)));
			}
		return $userInfoList;
	}
	
	/*
	 * Call Facebook 'api_client->users_getInfo' method
	 * 
	 * @return Array of user information. Each entry is an array containing
	 *         'name', 'first_name', 'last_name' entries.
	 *         Return false if could not retrieve details
	 */
	
	protected static function _fbGetInfo ($userIds, $userInfoKeys)
	{
		if (CConfig::DEBUG == 0)
			$userInfoList = (CFacebook::$fb ? CFacebook::$fb->api_client->users_getInfo($userIds, $userInfoKeys) : false);
		else
		{
			$userInfoList = array(array());
			$userInfoList[0]['uid'] = '100000844235714';
			$userInfoList[0]['name'] = 'David Wood';
			$userInfoList[0]['last_name'] = 'Wood';
			$userInfoList[0]['first_name'] = 'David';
			$userInfoList[0]['proxied_email'] = 'apps+382392681825.100000844235714.27256c48a47282e7318b10f1ca17141e@proxymail.facebook.com';
			$userInfoList[0]['email'] = 'apps+382392681825.100000844235714.27256c48a47282e7318b10f1ca17141e@proxymail.facebook.com';
		}
		return $userInfoList;
	}
	
	/*
	 * Return the Facebook session id
	 * 
	 * @return The id. i.e ( 2.CupTHfK9qW0k3vn8SJw4pA__.3600.1269410400-100000844235714 )
	 */
	
	public static function getSessionId ()
	{
		$out = CFacebook::getFbParam('session_key');
		return $out;
	}

	/*
	 * Get the Facebook user id.
	 * 
	 * @return   id. or false
	 */
	
	public static function getUserId ()
	{
		$out = CFacebook::getFbParam('user');
		if ($out == '')
			$out = CFacebook::getFbParam('canvas_user');
		CLogging::debug(sprintf('CFacebook::getUserId gave (%s)', $out));
		return $out;
	}
	
	/*
	 * Get Facebook status
	 * 
	 * @return true if Facebook active
	 */
	
	public static function isActive ()
	{
		//return CFacebook::getFbParam('app_id') != '' ? true : false;
		$out = (CFacebook::getUserId() != '' ? true : false);
		//$out = (CFacebook::getFbParam('in_new_facebook') == 1 ? true : false);
		CLogging::info(sprintf('CFacebook::isActive - %d', $out));
		return $out;
	}

	/*
	 * Return <script> statement
	 * 
	 * @return  An HTML <script> statement containing link to Facebook API script
	 */
	
	public static function scriptHtml ()
	{
		if (self::isActive() == false)
			return '';	
		$out = array();
		$out[] = '<script src="http://connect.facebook.net/en_US/all.js"></script>';
		$out[] = '<script src="js/facebook.js"></script>';
		return join('', $out);
	}
	
	/*
	 * Print <script> element to output
	 * 
	 * @return HTML stream
	 */
	
	public static function script ()
	{
		if (self::isActive() == false)
			return '';	
		$out = array();
		$out[] = sprintf('new CFacebook({isActive: %d, appId: "%s"});',
						 CFacebook::isActive() ? 1 : 0,
						 CConfig::FB_API_KEY);
		$out[] = 'CFacebook.init();';
//		$out[] = 'CFacebook.getPerm();';
		return join('', $out);
	}
	
	/*
	 * Send mail to Facebook user
	 * 
	 * @param cfg  Config object. Contains 'to', 'subject' and 'text'. The
	 *             'to' member is a Facebook user id. and the 'body' member
	 *             may contain XFBML
	 */
	
	public static function sendMail ($cfg)
	{
		self::$fb->api_client->notifications_sendEmail($cfg->to, $cfg->subject, '', $cfg->body);	
	}
	
	/*
	 * FBML to describe a user name
	 * 
	 * @return FBML
	 */
	
	public static function fbmlUser ($uid)
	{
		return sprintf("<fb:name uid='%s' />", $uid);
	}
	
	/*
	 * Redirect to a given URL
	 * 
	 * @param $url  URL to redirect to
	 */
	
	public static function redirect ($url)
	{
		if (CConfig::DEBUG == 0)
			self::$fb->redirect($url);
		else
			CCommon::redirect($url);
	}
	
	/*
	 * Get user details relating to Facebook id.
	 * 
	 * @return A CUser object or false if could not establish user
	 */
	
	static public function establishUser ()
	{
		$facebookId = self::getUserId();
		CLogging::debug(sprintf("CFacebook::establishUser - F/B id. [%s]", $facebookId));
		$user = new CUser();
		if ($user->loadWithFbId($facebookId) == false)
		{
			$userDetails = self::getUserInfo($facebookId);
			if ($userDetails == false)
				return false;
			$user->set(CUser::FIRST_NAME, $userDetails[0][self::GETINFO_FIRST_NAME]);
			$user->set(CUser::LAST_NAME, $userDetails[0][self::GETINFO_LAST_NAME]);
			//$user->set(CUser::FACEBOOK_EMAIL, $userDetails[0][self::GETINFO_EMAIL]);
			$user->set(CUser::FACEBOOK_ID, $facebookId);
			$user->set(CUser::GENDER, ($userDetails[0][self::GETINFO_SEX] == self::FACEBOOK_MALE ? 'm' : 'f'));
			$user->set(CUser::VERIFIED, 1);
			if ($user->save() == false)
				return false;
		}
		return $user;
	}
	
	/*
	 * Check if session available and redirect if not
	 * 
	 * @param $redirect   true to redirect if session invalid
	 */
	
	static public function requireSession ()
	{
		CLogging::info('CFacebook::requireSession - Checking session ...');
		if (self::getSessionId())
		{
			CLogging::info(sprintf('CFacebook::requireSession - ... got [%s]', self::getSessionId()));
			return;
		}

		// Inside XmlHttpRequest?
		$op = CCommon::getRequestValue('_xmlHttpReq');
		if ($op)
		{
			CLogging::info('CFacebook::requireSession - XmlHttpRequest in progress, returning status as session expired');
			$out = CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, '');
			$out->url = CConfig::DEBUG == 0
						? CConfig::FB_HOME_URL
						//? sprintf('%s/%s/', CConfig::FB_APP_URL, CConfig::FB_CANVAS_APP_NAME)
						: 'login.php';
			CCommon::xhrSend(CCommon::toJson($out), 'text/json');
			exit;
		}
		else
		{
			CLogging::info('CFacebook::requireSession - redirecting to home');
			if (CConfig::DEBUG == 0)
				CFacebook::redirect(CConfig::FB_HOME_URL);
				//CFacebook::redirect(sprintf('%s/%s/', CConfig::FB_APP_URL, CConfig::FB_CANVAS_APP_NAME));
				else
					CCommon::redirect('login.php');
			exit;
		}		
	}
}

// Initialise Facebook class
CFacebook::init();
?>
