<?php

require_once("classes/root.inc");
require_once("classes/userdb.inc");
require_once('classes/group.inc');
require_once('classes/usergroup.inc');
/*
 * User class
 */

class CUser extends CRoot {
    // Members relating to database columns

    const ID = "id";
    const FIRST_NAME = "first_name";
    const LAST_NAME = "last_name";
    const ADDRESS1 = "address1";
    const ADDRESS2 = "address2";
    const ADDRESS3 = "address3";
    const POSTCODE = "postcode";
    const EMAIL = "email";
    const MOBILE = "mobile";
    const CARREG = "carreg";
    const PASSWORD = "password";
    const CREATED = 'created';
    const UPDATED = 'updated';
    const VERIFIED = 'verified';
    const CREDIT = 'credit';
    const APPUSER = 'appuser';
    const LAST_ACK_ID = 'last_ack_id';
     const PASS_LAST_ACK_ID = 'pass_last_ack_id';
    const PHONE_CODE = "phone_code";
    const BONUS_CREDIT = "bonus_credit";
    const DEVICE_TOKEN = "device_token";
    const GROUP = "organisation";
    const LAST_IM_ACK_ID = 'last_im_ack_id';
    const PUSH_HANDLER_ID = 'push_handler_id';
    const PAYPAL_EMAIL = "paypal_email";
    const LAST_LAT = 'last_lat';
    const LAST_LNG = 'last_lng';
    const LAST_ACC = 'last_acc';
    const PHOTO = 'photo';
    const PHOTO_ID = 'photo_id';
    const SEX = 'sex';
    const SMOKING_HABIT = 'smoking_habit';
    const DRIVING_LICENSE_IMAGE = 'driving_license_image';
    const DRIVING_LICENSE_NUMBER = 'driving_license_number';
    const SOCIAL_MEDIA_IDS = 'social_media_ids';
    const DRIVER = 'driver';
    const PASSENGER = 'passenger';
    const CROP_X = 'crop_x';
    const CROP_Y = 'crop_y';
    const CROP_W = 'crop_w';
    const CROP_H = 'crop_h';
     const CROP_IMG_ORIGINAL = 'crop_img_original';

    // Database column member definitions
    private static $DBM01 = array('key' => CUser::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM02 = array('key' => CUser::FIRST_NAME, 'type' => CCommon::STRING_TYPE, 'size' => 20);
    private static $DBM03 = array('key' => CUser::LAST_NAME, 'type' => CCommon::STRING_TYPE, 'size' => 20);
    private static $DBM04 = array('key' => CUser::EMAIL, 'type' => CCommon::STRING_TYPE, 'size' => 256);
    private static $DBM05 = array('key' => CUser::MOBILE, 'type' => CCommon::STRING_TYPE, 'size' => 15);
    private static $DBM06 = array('key' => CUser::PASSWORD, 'type' => CCommon::STRING_TYPE, 'size' => 20);
    private static $DBM07 = array('key' => CUser::CREATED, 'type' => CCommon::DATETIME_TYPE);
    private static $DBM08 = array('key' => CUser::UPDATED, 'type' => CCommon::DATETIME_TYPE);
    private static $DBM09 = array('key' => CUser::VERIFIED, 'type' => CCommon::INT_TYPE, 'size' => 1);
    private static $DBM10 = array('key' => CUser::CARREG, 'type' => CCommon::STRING_TYPE, 'size' => 15);
    private static $DBM11 = array('key' => CUser::CREDIT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM12 = array('key' => CUSer::APPUSER, 'type' => CCommon::INT_TYPE, 'size' => 4);
    private static $DBM13 = array('key' => CUSer::LAST_ACK_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
     private static $DBM47 = array('key' => CUSer::PASS_LAST_ACK_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM14 = array('key' => CUser::ADDRESS1, 'type' => CCommon::STRING_TYPE, 'size' => 20);
    private static $DBM15 = array('key' => CUser::ADDRESS2, 'type' => CCommon::STRING_TYPE, 'size' => 20);
    private static $DBM16 = array('key' => CUser::ADDRESS3, 'type' => CCommon::STRING_TYPE, 'size' => 20);
    private static $DBM17 = array('key' => CUser::POSTCODE, 'type' => CCommon::STRING_TYPE, 'size' => 10);
    private static $DBM18 = array('key' => CUser::PHONE_CODE, 'type' => CCommon::STRING_TYPE, 'size' => 10);
    private static $DBM19 = array('key' => CUser::BONUS_CREDIT, 'type' => CCommon::INT_TYPE, 'size' => 1);
    private static $DBM20 = array('key' => CUser::DEVICE_TOKEN, 'type' => CCommon::STRING_TYPE, 'size' => 240);
    private static $DBM21 = array('key' => CUser::GROUP, 'type' => CCommon::STRING_TYPE, 'size' => 80);
    private static $DBM22 = array('key' => CUSer::LAST_IM_ACK_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
    private static $DBM23 = array('key' => CUser::PUSH_HANDLER_ID, 'type' => CCommon::INT_TYPE, 'size' => 1);
    private static $DBM24 = array('key' => CUser::PAYPAL_EMAIL, 'type' => CCommon::STRING_TYPE, 'size' => 256);
    private static $DBM26 = array('key' => CUser::DRIVING_LICENSE_IMAGE, 'type' => CCommon::STRING_TYPE, 'size' => 500);
    private static $DBM25 = array('key' => CUser::DRIVING_LICENSE_NUMBER, 'type' => CCommon::STRING_TYPE, 'size' => 50);
    private static $DBM29 = array('key' => CUser::PHOTO, 'type' => CCommon::STRING_TYPE, 'size' => 500);
    private static $DBM33 = array('key' => CUser::PHOTO_ID, 'type' => CCommon::STRING_TYPE, 'size' => 500);
    private static $DBM34 = array('key' => CUser::SMOKING_HABIT, 'type' => CCommon::STRING_TYPE, 'size' => 1);
    private static $DBM37 = array('key' => CUser::SOCIAL_MEDIA_IDS, 'type' => CCommon::STRING_TYPE, 'size' => 1000);
    private static $DBM38 = array('key' => CUser::DRIVER, 'type' => CCommon::INT_TYPE, 'size' => 3);
    private static $DBM39 = array('key' => CUser::PASSENGER, 'type' => CCommon::INT_TYPE, 'size' => 3);
    private static $DBM30 = array('key' => CUser::SEX, 'type' => CCommon::STRING_TYPE, 'size' => 1);
    private static $DBM40 = array('key' => CUser::LAST_LAT, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM41 = array('key' => CUser::LAST_LNG, 'type' => CCommon::FLOAT_TYPE, 'size' => 0);
    private static $DBM42 = array('key' => CUser::CROP_X, 'type' => CCommon::INT_TYPE, 'size' => 50);
    private static $DBM43 = array('key' => CUser::CROP_Y, 'type' => CCommon::INT_TYPE, 'size' => 50);
    private static $DBM44 = array('key' => CUser::CROP_W, 'type' => CCommon::INT_TYPE, 'size' => 50);
    private static $DBM45 = array('key' => CUser::CROP_H, 'type' => CCommon::INT_TYPE, 'size' => 50);

    /*
     * Constructor
     */

    function __construct() {
        parent::__construct('user');
    }

    /*
     * Make a password from clear text login/pwd combo
     *
     * @param $login       User login
     * @param $password    User password
     * @return             Encrypted password
     */

    public static function makePassword($login, $password) {
        return crypt(md5($password), md5($login));
    }

    /*
     * Check if a user login id. already exists
     *
     * @param $login - User login name
     * @return true or false
     */

    public static function userExists($login) {
        return CUserDb::userExists($login);
    }

    /*
     * Save user details
     *
     * @param $includeMembers    [optional] Member keys to include
     * @return                   true or false
     */

    public function save($includeMembers = false) {

      //  print_r($this);
       // echo 'user.inc  ';
        return CUserDb::save($this, $includeMembers);
    }

    /*
     * Load a user by login id.
     *
     * @param $login  Login id.
     * @return 		  true or false
     */

    public function load($login) {
        return CUserDb::load($login, $this);
    }

    /*
     * Load a user by Facebook id.
     *
     * @param $login  Login id.
     * @return 		  true or false
     */

    public function loadWithFbId($facebookId) {
        return CUserDb::loadWithFbId($facebookId, $this);
    }

    /*
     * Load a user by id.
     *
     * @param $userId  User id.
     * @return 		   true or false
     */

    public function loadWithUserId($userId) {
        return CUserDb::loadWithUserId($userId, $this);
    }

    /*
     * Load a user by car registration.
     *
     * @param $carReg          car registration
     * @return 		   true or false
     */

    public function loadWithCarReg($carReg) {
        return CUserDb::loadWithCarReg($carReg, $this);
    }
      public function registered_details_rows($all,$email,$name) {
        return CUserDb::registered_details_rows($all,$email,$name, new CUser());
    }
      public function registered_details($sort_range,$limit_range,$all,$email,$name) {
        return CUserDb::registered_details($sort_range,$limit_range,$all,$email,$name, new CUser());
    }
 
        public function loadWithCarRegCheck($carReg) {
        return CUserDb::loadWithCarRegCheck($carReg);
    }
  public function loadWithGroupUserId($userId) {
        return CUserDb::loadWithGroupUserId($userId, $this);
    }
    public function loadWithGroupUserIdPdf($userId) {
        return CUserDb::loadWithGroupUserIdPdf($userId, $this);
    }
      public function loadWithGroupUserIdList($userId) {
        return CUserDb::loadWithGroupUserIdList($userId, $this);
    }
        public function loadUpdateUserLocationList($lat,$lng,$accuracy,$userId) {
        return CUserDb::loadWithGroupUserIdList($lat,$lng,$accuracy,$userId, $this);
    }
      public function nUsersScheme($userId) {
        return CUserDb::nUsersScheme($userId, $this);
    }
    public function numberUsersScheme($userId) {
        return CUserDb::numberUsersScheme($userId, $this);
    }
   public function numberMilesShared($userId) {
        return CUserDb::numberMilesShared($userId, $this);
    }
   public function kg_C0_saved($userId) {
        return CUserDb::kg_C0_saved($userId, $this);
    }
    /*
     * Load a user by phone number.
     *
     * @param $phoneNo         phone number
     * @return 		   true or false
     */

    public function loadWithPhoneNo($phoneNo) {
        return CUserDb::loadWithPhoneNo($phoneNo, $this);
    }

    /*
     * Load user by given criteria
     *
     * @param $fieldId   Id. of field to query
     * @param $fieldVal  Query field value
     * @param $user      A CUser object to load into
     * @return          true or false
     */

    public function loadBy($fieldId, $fieldVal) {
        return CUserDb::loadBy($fieldId, $fieldVal, $this);
    }

    /*
     * Check if user is registered
     *
     * @return true or false
     */

    public function isRegistered() {
        $out = true;
        if ($this->get(self::FIRST_NAME) == '')
            $out = false;
        elseif ($this->get(self::LAST_NAME) == '')
            $out = false;
        elseif ($this->get(self::EMAIL) == '')
            $out = false;
        elseif ($this->get(self::PASSWORD) == '')
            $out = false;
        return $out;
    }

    /*
     * Generate a carpool name from user details
     * @return Name
     */

    public function generateCarpoolName() {
        $out = array();
        $first = $this->get(self::FIRST_NAME);
        if ($first != '')
            $out[] = $first;
        $last = $this->get(self::LAST_NAME);
        if ($last != '')
            $out[] = $last;
        $len = count($out);
        if ($len == 0)
            return '';
        $out[$len - 1] .= ((strtolower(substr($out[$len - 1], -1)) == 's' ? "'" : "'s"));
        $out[] = 'Carpool';
        return join(' ', $out);
    }

    /*
     * Get users' full name
     *
     * @return  Generated full name
     */

    public function getFullName() {
        $out = array();
        $first = $this->get(self::FIRST_NAME);
        if ($first != '')
            $out[] = $first;
        $last = $this->get(self::LAST_NAME);
        if ($last != '')
            $out[] = $last;
        return join(' ', $out);
    }

    /*
     * Return an  md5 has of user details
     * @return md5 hash
     */

    public function getUserKey() {
        return md5($this->get(self::ID) . $this->get(self::FIRST_NAME)
                . $this->get(self::LAST_NAME) . $this->get(self::PASSWORD));
    }

    /*
     * Removes domain matching groups of user
     */

    public function removeDomainGroups($userid) {
        $usergroup = new CUserGroup();
        return $usergroup->removeUserGroups($userid);
    }

    /*
     * Generate random password
     * @param $length      Password length
     * @paran $strength    Strength bit flag ( 1, 2, 4 and/or 8 )
     * @return             Password
     */

    public static function generatePassword($length = 9, $strength = 15) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';

        if ($strength & 1)
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        if ($strength & 2)
            $vowels .= "AEUY";
        if ($strength & 4)
            $consonants .= '23456789';
        if ($strength & 8)
            $consonants .= '@#$%';

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++)
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }

        return $password;
    }

    public static function getAllIds() {
        return CUserDb::getAllIds();
    }

    public static function getAllGroups() {
        return CUserDb::getAllGroups();
    }

    public static function getAllGroupsOfUser($userId) {
        return CUserDb::getAllGroupsOfUser($userId);
    }
    public static function getAllGroupsOfThisUser($userId) {
        return CUserDb::getAllGroupsOfThisUser($userId);
    }

    public static function getUserDetails($userId, $width, $height) {
        return CUserDb::getUserDetails($userId, $width, $height);
    }

    public static function getExistingGroupsOfUser($userId) {
        return CUserDb::getExistingGroupsOfUser($userId);
    }
public static function getExistingGroupsOfUserListingSearch($userId) {
        return CUserDb::getExistingGroupsOfUserListingSearchDb($userId);
    }
    public static function getMatchingEmails($domain) {
        return CUserDb::getMatchingEmails($domain);
    }

    public static function getMatchingGroups($domain) {
        return CUserDb::getMatchingGroups($domain);
    }

    public static function getAllEmailsInGroup($group) {
        return CUserDb::getAllEmailsInGroup($group);
    }
    
    public static function getRecentMessageInGroup($userId,$group) {
        return CUserDb::getRecentMessageInGroup($userId,$group);
    }
    
    public static function getAllEmailsInUserGroup($group) {
        return CUserDb::getAllEmailsInUserGroup($group);
    }

    public function toJsonForUser() {

        $data = $this->values;
        foreach ($data as $key => $value) {
            if (json_decode($value) != NULL)
                $data[$key] = json_decode($value);
        }
        return json_encode($data);
    }

    /*
     * Send invitation to matching domains and mail group owner
     * 
     * @param $mailStatus    Mail status
     * @param $user    A CUser object
     * @param $email_domain  email domain of user
     * @return none
     */
 function searchUserMember($str,$user){
        
        return CUserDb::searchUserMember($str,$user);
    }  
    public function sendInvitation($mailStatus, $email_domain, $user, $newgroup) {
        if ((int) $mailStatus == 1) {
            $usermail = array($user->get(CUser::EMAIL));
            $emails = CUser::getMatchingEmails($email_domain);
            $to_send = array_diff($emails['mail'], $usermail);
            $rplc = array();
            $rplc[1] = $user->get(CUser::FIRST_NAME);
            $rplc[2] = $user->get(CUser::EMAIL);
            $rplc[3] = implode(',', $newgroup);
            $params['to'] = $to_send;
            $params['from'] = CConfig::INFO_EMAIL;
            $params['subject'] = "Invitation to join group in FirstThumb";
            $params['body'] = CCommon::htmlReplace(CRegion::regionFile('groupinvitation.html'), $rplc);
            CCommon::sendEMail($params);
        }
        $rplc = array();
        $rplc[1] = $user->get(CUser::FIRST_NAME);
        $rplc[2] = implode(',', $newgroup);
        $params['to'] = $user->get(CUser::EMAIL);
        $params['from'] = CConfig::INFO_EMAIL;
        $params['subject'] = "Congratulations";
        $params['body'] = CCommon::htmlReplace(CRegion::regionFile('groupcreation.html'), $rplc);

        CCommon::sendEMail($params);
    }

    /*
     * Save domain matched and searched groups 
     * 
     * @param $userid    user id
     * @param $domain_groups    domain matched groups
     * @param $searched_groups  searched groups
     * @return none
     */

    public function saveUserGroup($userid, $domain_groups = 0, $searched_groups = 0, $newgroup = 0) {

        $usergroup = new CUserGroup();
        $existing_groups = $usergroup->loadExistingGroups($userid);
        $new_group = $usergroup->loadNewGroups($userid);
        $new_group_deleted = $usergroup->loadNewGroupsDel($userid, $newgroup);
        $new_group_delete_list = array_values(array_diff($new_group, $new_group_deleted));
        $searched_groups = explode(",", $searched_groups);
        $domain_groups = explode(",", $domain_groups);
        $extra_groups = array_values(array_diff($existing_groups, $domain_groups, $searched_groups, $new_group));

        if (count($existing_groups) > 0) {
            $domain_groups = array_values(array_diff($domain_groups, $existing_groups));
        }
        $searched_groups = array_values(array_diff(array_unique($searched_groups), $domain_groups));

        if (count($existing_groups) > 0) {
            $searched_groups = array_values(array_diff($searched_groups, $existing_groups));
        }

        $rows = "";
        if (is_array($domain_groups)) {
            $i = 1;
            while ($i <= count($domain_groups) - 1) {
                $rows.=',(' . $userid . ',' . $domain_groups[$i] . ',\'d\',\'y\',\'' . date('Y-m-d h:i:s') . '\')';
                $i++;
            }
        }

        if (is_array($searched_groups)) {
            $i = 1;

            /* while ($i <= count($searched_groups)) {
              $rows.=',(' . $userid . ',' . $searched_groups[$i] . ',\'s\',\'n\',\'' . date('Y-m-d h:i:s') . '\')';
              $i++;
              } */

            foreach ($searched_groups as $val) {
                $rows.=',(' . $userid . ',' . $val . ',\'s\',\'n\',\'' . date('Y-m-d h:i:s') . '\')';
            }
        }

        $res = $usergroup->save(substr($rows, 1), 0);
        if (count($existing_groups) > 0) {
            $extra_groups = implode(",", $extra_groups);
            if (count($extra_groups) > 0) {
                $usergroup->deleteUserGroup($extra_groups, $userid);
            }
        }


        $del_group_id = implode(",", $new_group_delete_list);
        if (count($new_group_delete_list) > 0) {
            $usergroup->deleteUserGroup($del_group_id, $userid);
        }





        $unapproved_groups = $usergroup->loadUnapprovedGroups($userid);

        $i = 0;
        while ($i < count($unapproved_groups)) {

            $this->approveRequestMail($userid, $unapproved_groups[$i]);
            $i++;
        }
        return $res;
    }

    /*
     * ApproveRequestMail will send request pending mail for owner and requested memeber
     * @param  $userId requested member user Id
     * @param $groupId requested group id
     * Result mail will send to correponding
     */

    /* mc_encrypt function
     * Requested group Id, RequestedMember Id, Group Owner Id
     */

    public function approveRequestMail($userId, $groupId) {

        $ownerId = CUserGroup::getGroupOwnerMemeberDetails($groupId);
        $ownerdetails = CUserGroup::getNameById($ownerId['user_id']);

        $userdetail = CUserGroup::getNameById($userId);
        $encryotedurl = CCommon::mc_encrypt($groupId . "," . $userId . "," . $ownerId['user_id'], CConfig::ENCRYPTION_KEY);
        $encryotedurl = urlencode($encryotedurl);
        //echo '-----'.$ownerdetails.'hh'.$userdetail.'hh'.$encryotedurl.'hh'.$ownerId;
        $mail_approve_status = CUserGroup::approvemailbody($ownerdetails, $userdetail, $encryotedurl, $ownerId, 'aprove_req');
        $mail_request_status = CUserGroup::approvemailbody($userdetail, $userdetail, NULL, $ownerId, 'req_notification');
    }

    /*
     * New group creation
     * 
     * @param $userid    user id
     * @param $newgroup   organization_location
     *  @return none
     */

    public function createGroup($userid, $newgroup, $region, $status) {
        $group = new CGroup();
        $usergroup = new CUserGroup();
        $i = 0;
        $rows = "";

        while ($i < count($newgroup)) {
            $group_tag = $newgroup[$i];
            $details = explode("_", $group_tag);
            $group_organization = $details[0];
            $group_location = $details[1];
            $newGroupCreate = array();

            $keyMap = array();
            $keyMap[] = CCommon::makeRequestKeyMap("organization", CGroup::ORGANIZATION);
            $keyMap[] = CCommon::makeRequestKeyMap("location", CGroup::LOCATION);
            $keyMap[] = CCommon::makeRequestKeyMap("group_tag", CGroup::GROUP_TAG);
            $keyMap[] = CCommon::makeRequestKeyMap("created", CGroup::CREATED);
            $keyMap[] = CCommon::makeRequestKeyMap("modified", CGroup::MODIFIED);
            $keyMap[] = CCommon::makeRequestKeyMap("user_id", CGroup::USER_ID);
            CCommon::mapRequestValues($keyMap, $group);

            $count = $group->validateGroupname($group_tag);

            if ($count == 0) {
                $group_id = $group->save($userid, $group_tag, $group_organization, $group_location);
                $rows.= ',(' . $userid . ',' . $group_id . ',\'n\',\'y\',\'' . date('Y-m-d h:i:s') . '\')';
                $newGroupCreate[] = $group_tag;
            } else {
                $out = CCommon::makeErrorObj(CCommon::GROUP_EXISTS, sprintf($region->msg(1035), $group_tag));

                //return $out;
            }
            $i++;
        }

        $res = $usergroup->save(substr($rows, 1), 1);
        if ($status == 0) {
            return 0;
        } else {
            return $newGroupCreate;
        }
    }

    /*
     * Send a mail asking for account verification
     * 
     * @param $user     A CUser object
     */

    public function verifyAccountMail($user) {
        $region = new CRegion;
        $rplc = array();
        $urlParams = array();
        $urlParams['userId'] = $user->get(CUser::ID);
        $urlParams['userKey'] = $user->getUserKey();
        $rplc[1] = sprintf('%s/confirmaccount.php?v=%s', CConfig::SITE_BASE, rawurlencode(base64_encode(CCommon::toJson($urlParams))));
        $rplc[8] = $region->msg(8, 'common');
        $params['to'] = $user->get(CUser::EMAIL);
        $params['from'] = CConfig::INFO_EMAIL_NAME;
        $params['subject'] = $region->msg(8, 'common') . ' registration';
        $params['body'] = CCommon::htmlReplace(CRegion::regionFile('verifyaccountmail.html'), $rplc);
        
        CCommon::sendEMail($params);
       /* $cfg = new stdClass;
        $cfg->body = CCommon::htmlReplace(CRegion::regionFile('verifyaccountmail.html'), $rplc);
        ;
        $cfg->subject = $region->msg(8, 'common') . ' registration';
        $cfg->fromName = CConfig::INFO_EMAIL_NAME;
        $cfg->toName = $user->getFullName();
        $cfg->from = CConfig::INFO_EMAIL;
        $cfg->to = $user->get(CUser::EMAIL);
        
        CCommon::sendMail($cfg);*/
    }

    public function verifyPhoneMail($user, $region) {
        $code = rand(1000, 9999);
        $user->set(CUser::PHONE_CODE, $code);

        $rplc = array();
        $rplc[1] = $user->getFullName();
        $rplc[2] = $code;
        $rplc[3] = CConfig::PHONE_NO;
        $rplc[8] = $region->msg(8, 'common');
        $params['to'] = $user->get(CUser::EMAIL);
        $params['from'] = CConfig::INFO_EMAIL_NAME;
        $params['subject'] = $region->msg(8, 'common') . ' phone verification';
        $params['body'] = CCommon::htmlReplace(CRegion::regionFile('phoneverificationemail.html'), $rplc);
        CCommon::sendEMail($params);
       /* $cfg = new stdClass;
        $cfg->body = CCommon::htmlReplace(CRegion::regionFile('phoneverificationemail.html'), $rplc);
        $cfg->subject = $region->msg(8, 'common') . ' phone verification';
        $cfg->fromName = CConfig::INFO_EMAIL_NAME;
        $cfg->toName = $user->getFullName();
        $cfg->from = CConfig::INFO_EMAIL;
        $cfg->to = $user->get(CUser::EMAIL);
        
        
        CCommon::sendMail($cfg);*/
    }
    
    
    
    public function verifyPhoneMail_old($user, $region) {
       $code = rand(1000, 9999);
        $user->set(CUser::PHONE_CODE, $code);

        $rplc = array();
        $rplc[1] = $user->getFullName();
        $rplc[2] = $code;
        $rplc[3] = CConfig::PHONE_NO;
        $rplc[8] = $region->msg(8, 'common');
        
        
        $params['to'] = $user->get(CUser::EMAIL);
        $params['from'] = CConfig::INFO_EMAIL;
        $params['subject'] = "Congratulations";
        $params['body'] = CCommon::htmlReplace(CRegion::regionFile('groupcreation.html'), $rplc);

        CCommon::sendEMail($params);
    }

    /*
     * Send a mail to indicate registration complete
     * 
     * @param $user     A CUser object
     * @param $region   A CRegion object
     */

    public function registerCompleteEmail($user, $region) {
        $rplc = array();
        $rplc[1] = $user->getFullName();
        $rplc[2] = sprintf('%s/%s/', CConfig::FB_APP_URL, CConfig::FB_CANVAS_APP_NAME);
        $rplc[8] = $region->msg(8, 'common');

        $cfg = new stdClass;
        $cfg->body = CCommon::htmlReplace(CRegion::regionFile('registeremail.html'), $rplc);
        $cfg->subject = $region->msg(8, 'common') . ' registration';
        $cfg->fromName = CConfig::INFO_EMAIL_NAME;
        $cfg->toName = $user->getFullName();
        $cfg->from = CConfig::INFO_EMAIL;
        $cfg->to = $user->get(CUser::EMAIL);
        $cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile('registeremail.txt'), $rplc, false);
        CCommon::sendMail($cfg);
    }

}

?>
