<?php
/*
 * User class
 */

class CSmsGwDb
{
	const NEW_SQL = "INSERT INTO smsgateway (%s) VALUES (%s)";
	const UPDATE_SQL = "UPDATE smsgateway SET %s WHERE id=%s";
	const GET_GW_SQL = "SELECT %s FROM smsgateway WHERE %s='%s'";
	
	/*
	 * Constructor
	 */
	
	function __construct ()
	{
	}
	
	/*
	 * Get a gateway using name.
	 * 
	 * @param $id      Gateway name.
	 * @param $gw      A CSmsGw object to load into
	 * @return         true or false
	 */
	
	public static function load ($name, $gw)
	{
		return self::_loadCore(CSmsGw::NAME, $name, $gw);
	}
	
	/*
	 * Core for "load..." methods
	 * 
	 * @param $fieldId   Id. of field to query
	 * @param $fieldVal  Query field value
	 * @param $gw        A CSmsGw object to load into
	 * @return           true or false
	 */
	
	protected static function _loadCore ($fieldId, $fieldVal, $gw)
	{
		$cols = CRoot::getDbMembers('CSmsGw');
		foreach ($cols as $colId => $col)
			$cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
		$sql = sprintf(CSmsGwDb::GET_GW_SQL, join(',', $cols), $fieldId, $fieldVal);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = mysql_fetch_object($res);
		if ($o)
			foreach ($o as $colId => $val)
				$gw->set($colId, CDb::makePhpValue($val, $cols[$colId]['type']));
		return true;
	}
	
	/*
	 * Save the gw
	 * 
	 * @param $gw                A CSmsGw object to save
	 * @return                   true if no reported DB error otherwise false
	 */
	
	public static function save ($gw)
	{
		// Get list of columns. Include id. column if already exists
		$id = $gw->get(CSmsGw::ID);
		$cols = CRoot::getFilteredDbMembers('CSmsGw', false, array(CSmsGw::ID));

		// Turn data into SQL compatible strings
		$vals = array();
		foreach ($cols as $colId => $col)
			$vals[$colId] = CDb::makeSqlValue($gw->get($colId), $col['type']);

		// Create SQL statement for insert or update
		$vals[CSmsGw::UPDATED] = 'now()';
		if ($id == 0)
		{
			$vals[CSmsGw::CREATED] = 'now()';
			$sql = sprintf(CSmsGwDb::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
		}
		else
		{
			unset($cols[CSmsGw::CREATED]);
			$tmp = array();
			foreach ($cols as $colId => $col)
				$tmp[] = sprintf("%s=%s", $colId, $vals[$colId]);
			$sql = sprintf(CSmsGwDb::UPDATE_SQL, join(",", $tmp), $id);
		}

		CDb::BeginTrans();
		$res = CDb::query($sql);
		if ($res)
		{
			// Retrieve the auto ID if "insert" performed
			if ($id == 0)
			{
				$id = CDb::getLastAutoId();
				$gw->set(CSmsGw::ID, $id);
			}
			CDb::commitTrans();
		}
		else
			CDb::rollbackTrans();
		
		return $res ? true : false;
	}
	
}

