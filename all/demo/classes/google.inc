<?php
require_once('includes/config.inc');

class CGoogle
{
	/*
	 * Return <script> statement
	 * 
	 * @return  HTML <script> statements defining scripts required for Google API function
	 */
	
	public static function scriptHtml ()
	{
		$out = array();
		switch(CConfig::GOOGLE_API_VER)
		{
			case 2:
			$out[] = sprintf('<script type="text/javascript" src="https://maps.google.com/maps?file=api&amp;v=2.x&amp;sensor=false&amp;key=%s"></script>', CConfig::GOOGLE_API_KEY);
			$out[] = '<script type="text/javascript" src="js/googlev2.js"></script>';
			$out[] = '<script type="text/javascript" src="thirdparty/parseuri/parseuri.js"></script>';
			break;
			
			case 3:
			$out[] = '<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3&sensor=false"></script>';
			$out[] = '<script type="text/javascript" src="js/googlev3.js"></script>';
			$out[] = '<script type="text/javascript" src="thirdparty/parseuri/parseuri.js"></script>';
			break;
		}
		return join("\n", $out);
	}
}
?>
