<?php
require_once("classes/root.inc");
require_once("db/sessiondb.inc");

class CSession extends CRoot
{
	// Members relating to databsae columns
	const ID = "id";
	const CREATED = "created";
	const UPDATED = "updated";
	const USER_ID = "user_id";
	
	// Database column member definitions
	private static $DBM01 = array('key' => CSession::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CSession::CREATED, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
	private static $DBM03 = array('key' => CSession::UPDATED, 'type' => CCommon::DATETIME_TYPE, 'size' => 0);
	private static $DBM04 = array('key' => CSession::USER_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	
	/*
	 * Constructor
	 */
	
	function __construct ()
	{
		parent::__construct();
	}
	
	/*
	 * Create a session
	 * 
	 * @param $userId   User id. to create for
	 * @return          true or false
	 */
	
	public function create ($userId)
	{
		$now = time();
		$this->set(CSession::CREATED, $now);
		$this->set(CSession::UPDATED, $now);
		$this->set(CSession::USER_ID, $userId);
		return CSessionDb::save($this);
	}
	
	/*
	 * Validate session
	 * @param $pollSesiond   true to poll session if validation ok
	 * @return               true of false
	 */
	
	public function validate ($pollSession = true)
	{
		$sessionId = $this->get(CSession::ID);
		if ($sessionId == 0)
			return false;
		if	(
			CSessionDb::load($sessionId, $this) == false
			|| $this->get(CSession::UPDATED, 0) == 0
			)
			return false;
		$now = $this->get('serverTime');
		$diff = $now - $this->get(CSession::UPDATED);
		CLogging::Info(sprintf("validate session %d now %ld last %ld diff %ld (%d)", $sessionId, $now,
					   $this->get(CSession::UPDATED), $diff, ($diff / 60 )));
		if ($diff > (CConfig::SESSION_TIMEOUT * 60))
			return false;	
		if ($pollSession)
			CSessionDb::poll($this);
		return true;
	}

	/*
	 * Delete session
	 * 
	 * @param $sessionId   Session id. to delete
	 */
	
	public static function delete ($sessionId)
	{
		return CSessionDb::delete($sessionId);
	}

	/*
	 * Load session details
	 * 
	 * @param $sessionId   Session id.
	 * @return             true or false
	 */
	
	public function load ($sessionId)
	{
		$out = CSessionDb::load($sessionId, $this);
		return $out;
	}
}
?>
