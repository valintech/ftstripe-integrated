<?php
require_once('classes/root.inc');
require_once('classes/user.inc');
require_once('classes/session.inc');
require_once('classes/carpool.inc');
if (CConfig::RUN_IN_FB)
	require_once('classes/facebook.inc');

/*
 * Login class
 */

class CLogin extends CRoot
{
	/*
	 * Class error codes. 1 to 9 are reserved for this class, all others
	 * are in common.inc.
	 */
	
	const SUCCESS = 0;
	const INVALID_LOGIN_ERROR = 1;
	const NOT_VERIFIED_ERROR = 2;
	const UNKNOWN_ERROR = 99;
	
	/*
	 * Constructor
	 * 
	 * @param $sessionId  Initial session id.
	 */
	
	function __construct ($sessionId = 0)
	{
		parent::__construct();
		$session = new CSession;
		$user = new CUser;
		if ($sessionId)
		{
			$session->load($sessionId);
			$user->loadWithUserId($session->get(CSession::USER_ID));
		}	
		$this->set('session', serialize($session));
		$this->set('user', serialize($user));
		$this->set('lastError', 0);
	}

	/*
	 * Logout of system
	 */
	
	function logout ()
	{
		$session = unserialize($this->get('session'));
		if ($session->get(CSession::ID))
		{
			CSession::delete($session->get(CSession::ID));
			$session->clear();
			$this->set('session', serialize($session));
		}
	}
	
	/*
	 * Login to system
	 * 
	 * @param $userName    Login user name
	 * @param $password    Login password
	 * @return             true or false ( 'lastError' set if false )
	 */
	
	function login ($userName, $password, $appuser = false)
	{
		$userName = strtolower($userName);
		CLogging::info(sprintf('CLogin/login - %s', $userName));
		$user = new CUser();
		if ($user->load($userName) == false)
		{
			CLogging::info(sprintf('CLogin/login - Invalid user'));
			$this->set('lastError', self::INVALID_LOGIN_ERROR);
			return false;
		}
		$password = CUser::makePassword($userName, $password);
		CLogging::info(sprintf('stored pwd [%s] entered pwd [%s]', $user->get(CUser::PASSWORD), $password));
                if ($user->get(CUser::PASSWORD) != $password)
		{
			CLogging::info(sprintf('CLogin/login - Invalid password'));
			$this->set('lastError', self::INVALID_LOGIN_ERROR);
			return false;
		}
		if ($user->get(CUser::VERIFIED) == 0)
		{
			CLogging::info(sprintf('CLogin/login - Not verified'));
			$this->set('lastError', self::NOT_VERIFIED_ERROR);
			return false;
		}
		if ($appuser == true && $user->get('appuser') == 0) {
			$user->set('appuser', 1);
			$msgIds = CCallLog::getActiveIds($user->get('id'), $user->get('last_ack_id'));
			if (count($msgIds))
				$user->set('last_ack_id', $msgIds[count($msgIds)-1]);
                        $msgIdsp = CCallLog::getActiveIdsPassenger($user->get('id'), $user->get('pass_last_ack_id'));
			if (count($msgIdsp))
				$user->set('last_ack_id', $msgIdsp[count($msgIdsp)-1]);
			$user->save();
		}
		return $this->_loginCore($user);
	}

	/*
	 * Login with existing user details
	 * 
	 * @param $user  A CUser object
	 * @return       true or false ( 'lastError' set if false )
	 */
	
	function loginWithUser ($user)
	{
		CLogging::info(sprintf('CLogin/loginWithUser - %ld', $user->get(CUser::ID)));
		if ($user->get(CUser::VERIFIED) == 0)
		{
			CLogging::info(sprintf('CLogin/loginWithUser - Not verified'));
			$this->set('lastError', self::INVALID_LOGIN_ERROR);
			return false;
		}
		return $this->_loginCore($user);
	}
	
	/*
	 * Core method for login
	 * 
	 * @param $user  CUser object
	 * @return       true or false
	 */
	
	function _loginCore ($user)
	{
		$userId = $user->get(CUser::ID);
		if ($userId == 0)
			return false;
		$session = new CSession();
		if ($session->create($userId) == false)
			return false;
		$this->set('session', serialize($session));
		$this->set('user', serialize($user));
		return true;
	}
	
	/*
	 * Return login sessiond id.
	 * 
	 * @return  Session id.
	 */
	
	public function sessionId ()
	{
		$session = unserialize($this->get('session'));
		return $session->get(CSession::ID);	
	}
	
	/*
	 * Return email of logged user
	 * 
	 * @return  email address
	 */

	public function userEmail ()
	{
		$user = $this->getUser();
		return $user ? $this->get(CUser::EMAIL) : '';
	}
	
	/*
	 * Return friendly name of logged user
	 * 
	 * @return  friendly name
	 */

	public function userFriendlyName ()
	{
		$user = $this->getUser();
		return $user ? $user->get(CUser::FIRST_NAME) : '';
	}
	
	/*
	 * Return user id. of logged user
	 * 
	 * @return User id.
	 */

	public function userId ()
	{
		$user = $this->getUser();
		return $user ? $user->get(CUser::ID) : 0;
	}
	
	/*
	 * Check if session is valid
	 * 
	 * @return true of false
	 */
	
	public function isValidSession ()
	{
		$session = unserialize($this->get('session'));
		CLogging::info(sprintf('Checking session %ld ...', $session->get(CSession::ID)));
		if ($session->get(CSession::ID))
			$out = $session->validate();
		else
			$out = false;
		CLogging::info($out ? 'Ok' : 'INVALID');
		return $out;
	}

	/*
	 * Get login user
	 * 
	 * @param $reload   Reload current user details
	 * @return          CUser object or false
	 */
	
	public function getUser ($reload = false)
	{
		$user = unserialize($this->get('user'));
		$id = $user->get(CUser::ID);
		if ($id == 0)
			return false;
		if ($reload == false)
			return $user;
		if ($user->loadWithUserId($id) == false)
			return false;
		$this->set('user', serialize($user));
		return $user;
	}

	/*
	 * Check if session available and redirect if not
	 * 
	 * @param $redirect   true to redirect if session invalid
	 */
	
	public function requireSession ($redirect = true)
	{
		// Return is session is ok
		CLogging::info('CLogin::requireSession - Checking session ...');
		if ($this->isValidSession())
		{
			CLogging::info('CLogin::requireSession - Session OK');
			return true;
		}
		CLogging::info('CLogin::requireSession - Session invalid ...');
		
		// For Facebook, try and automatically allocate a new session		
		if (CConfig::RUN_IN_FB)
		{
			$user = CFacebook::establishUser();
			$isValidSession = ($user ? $this->loginWithUser($user) : false );
			if ($isValidSession)
			{
				CLogging::info('CLogin::requireSession - Reapplied session, Session OK');
				return true;
			}
		}

		CLogging::info('CLogin::requireSession - Cant establish session ...');
		if ($redirect == false)
			return false;

		// Inside XmlHttpRequest?
		$op = CCommon::getRequestValue('_xmlHttpReq');
		if ($op)
		{
			CLogging::info('CLogin::requireSession - XmlHttpRequest in progress, returning status as session expired');
			$out = CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, '');
			$out->url = CConfig::DEBUG == 0 && CConfig::RUN_IN_FB
						? sprintf('%s/%s/', CConfig::FB_APP_URL, CConfig::FB_CANVAS_APP_NAME)
						: 'login.php';
			CCommon::xhrSend(CCommon::toJson($out), 'text/json');
		}
		else
		{
			CLogging::info('CLogin::requireSession - redirecting to home');
			if (CConfig::DEBUG == 0 && CConfig::RUN_IN_FB)
				CFacebook::redirect(sprintf('%s/%s/', CConfig::FB_APP_URL, CConfig::FB_CANVAS_APP_NAME));
			else{
                            CCommon::redirect('home.php');
                            //CCommon::redirect('intro.php');
                            
                        }
				
		}		

		exit;
	}
	
	/*
	 * Return last error code
	 * 
	 * @return a CLogin::xxx constant
	 */
	
	function lastError ()
	{
		return $this->get('lastError');		
	}
}
?>
