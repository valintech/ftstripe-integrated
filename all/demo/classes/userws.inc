<?php

require_once('common.inc');
require_once('user.inc');
require_once('region.inc');

/*
 * User class (Webservice)
 */

class CUserWs {
    /*
     * User registration
     * 
     * @return object          stdClass object {journeyList: []} on success
     *                         or containing 'errorId' and 'errorText' on error
     */

    public static function registerUser($sessionId, $id, $firstname, $lastname, $address1, $address2, $address3, $postcode, $email, $mobile, $phonecode, $carreg, $password, $verified, $paypalEmail, $photo, $birthcertificate, $photoId, $sex, $smokingHabit, $licenceImage, $licenceNumber, $socialMedia, $driver, $passenger, $newgroup, $domain_groups, $searched_groups, $mailStatus) {

        date_default_timezone_set('Europe/London');
        $user = new CUser();
        $login = new CLogin($sessionId);
        if ($sessionId != 0) {


            if ($login->isValidSession() != false) {
                $userId = $login->userId();
                if ($userId)
                    $user->loadWithUserId($userId);
                $image = $user->get(CUser::DRIVING_LICENSE_IMAGE);
                $oldemail = $user->get(CUser::EMAIL);
            }
        }
        else {
            CWebSession::init();
            $sessionId = session_id();
        }


        // Establish language context
        $region = new CRegion('register');

        $oldmask = umask(0);
        if (!file_exists('../upload/' . $sessionId))
            mkdir('../upload/' . $sessionId, 0777);
        umask($oldmask);
        $photofile = CUserWs::saveImage($photo, "Photo", $sessionId);
        $photoIdfile = CUserWs::saveImage($photoId, "PhotoId", $sessionId);
        $licensefile = CUserWs::saveImage($licenceImage, "DrivingLicense_Active", $sessionId);

        $user->set(CUser::FIRST_NAME, $firstname);
        $user->set(CUser::LAST_NAME, $lastname);
        $user->set(CUser::ADDRESS1, $address1);
        $user->set(CUser::ADDRESS2, $address2);
        $user->set(CUser::ADDRESS3, $address3);
        $user->set(CUser::POSTCODE, $postcode);
        $user->set(CUser::EMAIL, $email);
        $user->set(CUser::PAYPAL_EMAIL, $paypalEmail);
        $user->set(CUser::MOBILE, $mobile);
        $user->set(CUser::CARREG, $carreg);
        $user->set(CUser::PASSWORD, $password);
        $user->set(CUser::PHONE_CODE, $phonecode);
        $user->set(CUser::PHOTO, $photofile);
        $user->set(CUser::DRIVING_LICENSE_NUMBER, $licenceNumber);

        if (isset($image)) {

            $license = json_decode($image, true);
            $old_active_photo = $license['active'];
            $license['active'] = $licensefile;
            $license[] = $old_active_photo;
            $new_license_json = json_encode($license);
        }
        else
            $new_license_json = json_encode(array("active" => $licensefile));

        $user->set(CUser::DRIVING_LICENSE_IMAGE, $new_license_json);
        $user->set(CUser::PHOTO_ID, $photoIdfile);
        $user->set(CUser::SEX, $sex);
        $user->set(CUser::SMOKING_HABIT, $smokingHabit);
        $user->set(CUser::SOCIAL_MEDIA_IDS, $socialMedia);
        $user->set(CUser::DRIVER, $driver);
        $user->set(CUser::PASSENGER, $passenger);
        $user->set(CUser::VERIFIED, $verified);
        $user->set(CUser::CROP_X, $crop_x);
        $user->set(CUser::CROP_Y, $crop_y);
        $user->set(CUser::CROP_W, $crop_w);
        $user->set(CUser::CROP_H, $crop_h);


        $keyMap[] = CCommon::makeRequestKeyMap("crop_x", CUser::CROP_X);
        $keyMap[] = CCommon::makeRequestKeyMap("crop_y", CUser::CROP_Y);
        $keyMap[] = CCommon::makeRequestKeyMap("crop_w", CUser::CROP_W);
        $keyMap[] = CCommon::makeRequestKeyMap("crop_h", CUser::CROP_H);

        if ($login->isValidSession() == false) {
            $user->set(CUser::CREDIT, '0');
            $user->set(CUser::BONUS_CREDIT, 0);
            $user->set(CUser::APPUSER, '0');
            $user->set(CUser::LAST_ACK_ID, '0');
            $user->set(CUser::PASS_LAST_ACK_ID, '0');
            $user->set(CUser::LAST_IM_ACK_ID, '0');
            $user->set(CUser::PUSH_HANDLER_ID, '0');
        } else {
            $user->set(CUser::CREDIT, $user->get(CUser::CREDIT));
            $user->set(CUser::BONUS_CREDIT, $user->get(CUser::BONUS_CREDIT));
            $user->set(CUser::APPUSER, $user->get(CUser::APPUSER));
            $user->set(CUser::LAST_ACK_ID, $user->get(CUser::LAST_ACK_ID));
            $user->set(CUser::PASS_LAST_ACK_ID, $user->get(CUser::PASS_LAST_ACK_ID));
            $user->set(CUser::LAST_IM_ACK_ID, $user->get(CUser::LAST_IM_ACK_ID));
            $user->set(CUser::PUSH_HANDLER_ID, $user->get(CUser::PUSH_HANDLER_ID));
        }
        if ($user->get(CUser::MOBILE) != '')
            $user->verifyPhoneMail($user, $region);
        $out = new stdClass;
        if ($login->isValidSession() != false && $oldemail && $oldemail == $email)
            ; /* do nothing */
        else {
            $user2 = new CUser();
            if ($user2->load($email) == false) {
                CLogging::error('Failed to load user with email ' . $email);

                return CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));
            }
            if ($user2->get(CUser::ID)) {
                return CCommon::makeErrorObj(CCommon::USER_EXISTS, sprintf($region->msg(1000), $email));
            }
        }
        if ($login->isValidSession() != false) {
            $email = $user->get(CUser::EMAIL);
            if (strcmp($email, $oldemail) != 0)
                $user->removeDomainGroups($session_id);
        }

        if ($user->save() == false) {
            CLogging::error('Failed to save user');

            $out = CCommon::makeErrorObj(CCommon::DATABASE_ERROR, $region->msg(1004));

            return $out;
        }

        // Update the current websession with new user details
        $login->getUser(true);

        $newgroup = json_decode($newgroup);

        $email_domain = substr(strrchr($user->get(CUser::EMAIL), "@"), 1);
        if (count($newgroup) > 0) {
            $res = $user->createGroup($user->get(CUser::ID), $newgroup, $region);
            if ($res != false)
                $user->sendInvitation($mailStatus, $email_domain, $user);
        }

        if (strcmp(trim($domain_groups), "0") != 0 || strcmp(trim($searched_groups), "0") != 0) {


            $res = $user->saveUserGroup($user->get(CUser::ID), $domain_groups, $searched_groups);
        }

        CWebSession::set('login', serialize($login));

        if ($res == false) {
            $out->message = $region->msg(1021) . " " . $region->msg(1036);
        } else {

            $out->message = $region->msg(1021);
        }
        if (CConfig::RUN_IN_FB)
            $user->registerCompleteEmail($user, $region);
        else {

            $user->verifyAccountMail($user);
        }

        return $out;
    }

    private static function saveImage($binaryData, $filetype, $sessionId) {
        $binaryData = str_replace('data:image/png;base64,', '', $binaryData);
        $binaryData = str_replace(' ', '+', $binaryData);
        $data = base64_decode($binaryData);
        $im = imagecreatefromstring($data);
        if ($im !== false) {

            $fileName = '../upload/' . $sessionId . '/' . $sessionId . '_' . $filetype . '.png'; // path to png image
            imagealphablending($im, false); // setting alpha blending on
            imagesavealpha($im, true); // save alphablending setting (important)
            // Generate image and print it
            $resp = imagepng($im, $fileName);
            chmod($fileName, 777);
            // frees image from memory
            imagedestroy($im);
            return $sessionId . '_' . $filetype . '.png';
        } else {
            return 0;
        }
    }

}

?>
