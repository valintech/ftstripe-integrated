<?php

/*
 * User class
 */

class CUserDb {

    const USER_EXISTS_SQL = "SELECT COUNT(*) user_count FROM user WHERE login='%s'";
    const NEW_SQL = "INSERT INTO user (%s) VALUES (%s)";
    const UPDATE_SQL = "UPDATE user SET %s WHERE id=%s";
    const GET_USER_SQL = "SELECT %s FROM user  WHERE %s='%s'";
    const GET_USER_LIKESQL = "SELECT %s FROM user WHERE %s like '%s'";
    const GROUP_MATCHES="SELECT %s from user inner join groups on groups.user_id=user.id where user.email like '%s' limit 0,5"; 
    const USER_GROUPS="SELECT g.group_tag, g.id, ug.created_type, ug.status FROM user AS u LEFT JOIN `user_group` AS ug ON u.id = ug.user_id
LEFT JOIN groups AS g ON g.id = ug.group_id WHERE u.id=%d";/*
     * Constructor
     */

    function __construct() {
        
    }

    /*
     * Get a user using Facebook id.
     *
     * @param $facebookId   Facebook id.
     * @param $user         A CUser object to load into
     * @return              true or false
     */

    public static function loadWithFbId($facebookId, $user) {
        return self::_loadCore(CUser::FACEBOOK_ID, $facebookId, $user);
    }

    /*
     * Get a user using login id.
     *
     * @param $login   Login id.
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    public static function load($loginId, $user) {
        return self::_loadCore(CUser::EMAIL, $loginId, $user);
    }

    /*
     * Get a user using user id.
     *
     * @param $userId  User id.
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    public static function loadWithUserId($userId, $user) {
        return self::_loadCore(CUser::ID, $userId, $user);
    }

    /*
     * Get a user using car registration number.
     *
     * @param $carReg  Car registration
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    public static function loadWithCarReg($carReg, $user) {
        return self::_loadCore(CUser::CARREG, $carReg, $user);
    }

    /*
     * Get a user using phone number.
     *
     * @param $phoneNo Phone number
     * @param $user    A CUser object to load into
     * @return         true or false
     */

    //smsgw sends +44
    public static function loadWithPhoneNo($phoneNo, $user) {
        // deal with smsgw sending leading '+44' rather than '0', and v-a-v
        if (substr($phoneNo, 0, 1) == '0')
            $phone2 = preg_replace('/^0/', '+44', $phoneNo);
        else if (substr($phoneNo, 0, 3) == '+44')
            $phone2 = preg_replace('/^\+44/', '0', $phoneNo);
        else
            $phone2 = false;

        // also match any 44 numbers in the database
        if (substr($phoneNo, 0, 3) == '+44')
            $phone3 = preg_replace('/^\+44/', '44', $phoneNo);
        else
            $phone3 = false;

        $cols = CRoot::getDbMembers('CUser');
        foreach ($cols as $colId => $col)
            $cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
        $sql = sprintf(CUserDb::GET_USER_SQL, join(',', $cols), CUser::MOBILE, $phoneNo);
        if ($phone2)
            $sql .= " OR mobile='" . $phone2 . "'";
        if ($phone3)
            $sql .= " OR mobile='" . $phone3 . "'";
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = mysql_fetch_object($res);
        if ($o)
            foreach ($o as $colId => $val)
                $user->set($colId, CDb::makePhpValue($val, $cols[$colId]['type']));
        return true;
    }

    /*
     * Load user by given criteria
     *
     * @param $fieldId   Id. of field to query
     * @param $fieldVal  Query field value
     * @param $user      A CUser object to load into
     * @return          true or false
     */

    public static function loadBy($fieldId, $fieldVal, $user) {
        return self::_loadCore($fieldId, $fieldVal, $user);
    }

    /*
     * Core for "load..." methods
     *
     * @param $fieldId   Id. of field to query
     * @param $fieldVal  Query field value
     * @param $user      A CUser object to load into
     * @return           true or false
     */

    protected static function _loadCore($fieldId, $fieldVal, $user) {
        $cols = CRoot::getDbMembers('CUser');
        $selectVals = array();
        foreach ($cols as $colId => $col)
            $selectVals[$colId] = CDb::makeSelectValue($colId, $col['type']);
        $sql = sprintf(CUserDb::GET_USER_SQL, join(',', $selectVals), $fieldId, $fieldVal);
        $res = CDb::query($sql);
        if ($res === false)
            return false;
        $o = mysql_fetch_object($res);
        if ($o)
            foreach ($o as $colId => $val)
                $user->set($colId, CDb::makePhpValue($val, $cols[$colId]['type']));

        return true;
    }

    /*
     * Save the user
     *
     * @param $user              A CUser object to save
     * @param $includeMembers    [optional] Member keys to include
     * @return                   true if no reported DB error otherwise false
     */

    public static function save($user, $includeMembers = false) {
        // Get list of columns. Include id. column if already exists
        $id = $user->get(CUser::ID);
        $cols = CRoot::getFilteredDbMembers('CUser', $includeMembers, array(CUser::ID));

        // Turn data into SQL compatible strings
        $vals = array();
        foreach ($cols as $colId => $col)
            $vals[$colId] = CDb::makeSqlValue($user->get($colId), $col['type']);

        // Create SQL statement for insert or update
        $vals[CUser::UPDATED] = 'now()';
        $status = true;
        if ($id == 0) {
            $vals[CUser::CREATED] = 'now()';
            $status = true;
            $sql = sprintf(CUserDb::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
        } else {
            unset($cols[CUser::CREATED]);
            unset($vals[CUser::CREATED]);
            $tmp = array();
            foreach ($cols as $colId => $col)
                $tmp[] = sprintf("%s=%s", $colId, $vals[$colId]);
            $sql = sprintf(CUserDb::UPDATE_SQL, join(",", $tmp), $id);
            $status = false;
        }

        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
            if ($id == 0) {
                $id = CDb::getLastAutoId();
                $user->set(CUser::ID, $id);
            }
            CDb::commitTrans();
            CUserDb::moveFiles($id, $status, $user, $cols);
        }
        else
            CDb::rollbackTrans();

        return $res ? true : false;
    }

    /*
     * Move uploaded files to user directory
     *
     * @param $id - User id
     * @return none
     */

    private static function moveFiles($id, $status, $user) {
        $session_id = session_id();
        if (!$status)
            $session_id = $id;
 
        $dir = '../upload/' . $id;
        rename('../upload/' . $session_id, $dir);
        
        $tmp = array();

        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    $filename = str_replace(session_id(), $id, $file);
                    if (!file_exists('../upload/' . $id . '/' . $filename))
                        rename('../upload/' . $id . '/' . $file, '../upload/' . $id . '/' . $filename);

                    $filenameStart = explode(".", $file);


                    if (strcmp($filenameStart[0], $session_id . "_BirthCertificate") == 0 && ($user->get(CUser::BIRTH_CERTIFICATE) != 0 || $user->get(CUser::BIRTH_CERTIFICATE) != NULL ))
                        $tmp[] = sprintf("%s='%s'", 'birth_certificate', $filename);
                    if (strcmp($filenameStart[0], $session_id . "_Photo") == 0 && ($user->get(CUser::PHOTO) != 0 || $user->get(CUser::PHOTO) != NULL ))
                        $tmp[] = sprintf("%s='%s'", 'photo', $filename);
                    if (strcmp($filenameStart[0], $session_id . "_PhotoId") == 0 && ($user->get(CUser::PHOTO_ID) != 0 || $user->get(CUser::PHOTO_ID) != NULL ))
                        $tmp[] = sprintf("%s='%s'", 'photo_id', $filename);
                    if (strcmp($filenameStart[0], $session_id . "_Passport") == 0 && ($user->get(CUser::PASSPORT_IMAGE) != 0 || $user->get(CUser::PASSPORT_IMAGE) != NULL ))
                        $tmp[] = sprintf("%s='%s'", 'passport_image', $filename);
                    if (strcmp($filenameStart[0], $session_id . "_DrivingLicense_Active") == 0 ) {
                        if (!$status) {
                            $image = $user->get(CUser::DRIVING_LICENSE_IMAGE);
                            $license = json_decode($image, true);

                            $old_active_photo = $license['active'];

                            $license['active'] = $filename;
                            $license[] = $old_active_photo;

                            $new_photo_json = json_encode($license);
                        }
                        else
                            $new_photo_json = json_encode(array("active" => $filename));
                        $tmp[] = sprintf("%s='%s'", 'driving_license_image', $new_photo_json);
                    }
                }
                closedir($dh);
            }
        }
        $sql = sprintf(CUserDb::UPDATE_SQL, join(",", $tmp), $id);

        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {

            CDb::commitTrans();
        }
        else
            CDb::rollbackTrans();
    }
    /*
     * Check if a user login id. already exists
     *
     * @param $login - User login name
     * @return true or false
     */

    public static function userExists($login) {
        $sql = sprintf(CUserDb::USER_EXISTS_SQL, trim($login));
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = CDb::getRowObject($res);
        if ($o == false)
            return false;
        return (int) $o->user_count ? true : false;
    }

    /*
     * Return array all user Ids
     *
     * @return           Array of record IDs
     */

    public static function getAllIds() {
        $sql = sprintf("select id from user order by id");
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUser::ID];
        }
        return $out;
    }
    /*
     * Return array of all user group details
     *
     * @return           Array of record IDs
     */

    public static function getExistingGroupsOfUser($userId) {
        $sql = sprintf(CUserDb::USER_GROUPS, $userId);
        $res = CDb::query($sql);
        if ($res == false)
            return false; 
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out['type'][]=$o[CUserGroup::CREATED_TYPE];
            $out['status'][]=$o[CUserGroup::STATUS];
            $out['group_id'][]=$o[CGroup::ID];
            $out['group_tag'][]=$o[CGroup::GROUP_TAG];
      }
        return $out;
    }
    /*
     * Return array of all groups
     * Ignore "null" which is the default value when
     * someone isn't in a group.
     *
     * @return           Array of groups.
     */

    public static function getAllGroups() {
        $sql = sprintf("SELECT DISTINCT(%s) FROM user", CUser::GROUP);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $group = $o[CUser::GROUP];
            if (null !== $group) {
                $out[] = $group;
            }
        }
        return $out;
    }

    /*
     * Return array of all groups of which $user
     * is a member.
     * Ignore "null" which is the default value when
     * someone isn't in a group.
     * @param 			 User ID
     * @return           Array of groups.
     */

    public static function getAllGroupsOfUser($userId) {
        $sql = sprintf("SELECT %s FROM user WHERE id=%s", CUser::GROUP, $userId);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $group = $o[CUser::GROUP];
            if (null !== $group) {
                $out[] = $group;
            }
        }
        return $out;
    }

    /*
     * Return array of all user emails in group.
     *
     * @return           Array of email addresses.
     */

    public static function getAllEmailsInGroup($group) {
        $sql = sprintf(CUserDb::GET_USER_SQL, CUser::EMAIL, CUser::GROUP, trim($group));
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUser::EMAIL];
        }
        return $out;
    }
    
    public static function getMatchingGroups($domain) {
        
        $sql = sprintf(CUserDb::GROUP_MATCHES, "email,groups.id,group_tag", "%".$domain."%");
        $res = CDb::query($sql);
        
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out['mail'][] = $o[CUser::EMAIL];
            $out['group_id'][] = $o['id'];
            $out['group_tag'][] = $o['group_tag'];
        }
        return $out;
    //SELECT user.email,groups.id,groups.group_tag from user inner join groups on groups.user_id=user.id where user.email like "%gmail.com%"
    }
    public static function getMatchingEmails($domain) {
        
        $sql = sprintf(CUserDb::GET_USER_LIKESQL, CUser::EMAIL.",".CUser::FIRST_NAME,CUser::EMAIL, "%".$domain."%");
        $res = CDb::query($sql);
        
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out['mail'][] = $o[CUser::EMAIL];
            $out['name'][] = $o[CUser::FIRST_NAME];
         }
        return $out;
    //SELECT user.email,groups.id,groups.group_tag from user inner join groups on groups.user_id=user.id where user.email like "%gmail.com%"
    }
    
}

?>
