<?php
require_once('classes/common.inc');
require_once('classes/journey.inc');
require_once('classes/region.inc');

/*
 * Journey class (Webservice)
 */

class CJourneyWs
{
	/*
	 * Find rides
	 * 
	 * @param int $sessionId
	 * @param float $startLat
	 * @param float $startLng
	 * @param float $endLat
	 * @param float $endLng
	 * @return object          stdClass object {journeyList: []} on success
	 *                         or containing 'errorId' and 'errorText' on error
	 */
	
	public static function findRides ($sessionId, $startLat, $startLng, $endLat, $endLng)
	{
		$start = new CLocation('', $startLat, $startLng);
		$end = new CLocation('', $endLat, $endLng);
		$journeyList = CJourney::distance($start, $end);
		if ($journeyList)
		{
			$journeyInfo = CJourney::journeyInfo($journeyList);
			$allowedDefs = CRoot::getWsDbMembers('CJourney');
			foreach ($journeyList as $k => $journey)
			{
				$journeyVals = array();
				foreach ($allowedDefs as $k1 => $v1)
					$journeyVals[$v1['wsKey']] = $journey->get($k1);
				$journeyId = $journey->get(CJourney::ID);
				if ($journeyInfo[$journeyId])
					$journeyVals['extra'] = $journeyInfo[$journeyId];  
				$journeyList[$k] = $journeyVals;
			}
			$out = new stdClass;
			$constants = new stdClass;
			$constants->regularJourney = CJourney::JOURNEY_TYPE_REGULAR;
			$constants->oneOffJourney = CJourney::JOURNEY_TYPE_ONEOFF;
			$constants->seekingLift = CJourney::REQUEST_TYPE_SEEKING;
			$constants->offeringLift = CJourney::REQUEST_TYPE_OFFERING;
			$constants->seekOrOfferLift = CJourney::REQUEST_TYPE_EITHER;
			$constants->standardSchedule = CJourney::SCHEDULE_TYPE_STANDARD;
			$constants->customSchedule = CJourney::SCHEDULE_TYPE_CUSTOM;
			$out->constants = $constants;
			$out->journeyList = $journeyList;
		}
		else
			$out = CCommon::makeErrorObj(CCommon::DATABASE_ERROR, 'Problem retrieving journeys');
		return $out;
	}
	
	/*
	 * Request ride for a specific journey
	 * 
	 * @param int $sessionId       Session id.
	 * @param int $journeyId       Journey id.
	 * @param string $comments     Additional comments to be added
	 *                             to ride request
	 * @return object              stdClass object containing method return or
	 *                             Zend error response object on error
	 */
	
	public static function requestRide ($sessionId, $journeyId, $comments)
	{
		$login = new CLogin($sessionId);
		if ($login->isValidSession() == false)
			return CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session specified');
		$userId = $login->userId();
		$region = new CRegion('requestride');
		$out = CJourney::requestRide($userId, $journeyId, $comments,
									 $region->msg(1012), $region->msg(1011));
		if ($out == 0)
			$out = new stdClass;
		elseif ($out == CCommon::JOURNEY_NOT_FOUND)
			$out = CCommon::makeErrorObj($out, 'Journey does not exist');
		elseif ($out == CCommon::JOURNEY_USER_NOT_FOUND)
			$out = CCommon::makeErrorObj($out, 'Journey user does not exist');
		elseif ($out == CCommon::JOURNEY_ALREADY_REQUESTED)
			$out = CCommon::makeErrorObj($out, 'Journey already requested by this user');
		elseif ($out == CCommon::DATABASE_ERROR)
			$out = CCommon::makeErrorObj($out, 'Database error');
		elseif ($out == CCommon::CANNOT_JOIN_OWN_JOURNEY)
			$out = CCommon::makeErrorObj($out, 'Journey requester and organiser are the same');
		else
			$out = CCommon::makeErrorObj(CCommon::UNKNOWN_ERROR, 'Unknown error');
		return $out;
	}

	/*
	 * Check if a journey is already requested
	 * 
	 * @param int $journeyId   Journey id.
	 * @param int $userId      User id.
	 * @return object          true if journey already requested by this user
	 */
	
	protected static function isJourneyRequested ($journeyId, $userId)
	{
		$journeyRequest = new CJourneyRequest;
		$journeyRequest->loadByJourneyAndUser($journeyId, $userId);
		return $journeyRequest->get(CJourneyRequest::ID) ? true : false;
	}
}
?>
