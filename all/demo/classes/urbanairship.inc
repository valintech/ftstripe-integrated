<?php
require_once('classes/common.inc');
/*
 * Interface to urban airship web API
 * ==================================
 * The following docs offer good explanations of the functionality herein:
 *	API REFERENCE:
 *		http://docs.urbanairship.com/reference/api/index.html
 *		http://docs.urbanairship.com/reference/api/v1/index.html
 *	CURL EXAMPLES:
 *		https://support.urbanairship.com/customer/portal/articles/1069013-helpful-curl-examples
 * ERROR CODES:
 * 	https://support.urbanairship.com/customer/portal/articles/60923-troubleshooting-http-status-codes
 *
 * NOTE:
 * In our system the iPhone App tells urban airship about new device tokens
 * so we don't have that functionality in this class.
 */

/* TODO
 * maintenance - will be able to cull old tokens using these features:
	Get your device token list for an app

	    curl -X GET -u "<appkey>:<MasterSecret>" https://go.urbanairship.com/api/device_tokens/


	Get device token status using our feedback service

	    curl -X GET -u "<appkey>:<MasterSecret>" https://go.urbanairship.com/api/device_tokens/feedback/?since=year-month-day
*/


class CUrbanAirship
{
	// Tags can be used to create groups on Urban airship.  This function
	// associates a device with the given tag
	//
	//	curl -X PUT -u "<AppKey>:<Secret>" -H "Content-Type: application/json" --data '{"tags": ["<tag>"]}' https://go.urbanairship.com/api/device_tokens/<deviceToken>/
    //	curl -X PUT -u "<AppKey>:<Secret>" https://go.urbanairship.com/api/device_tokens/<device_token>/tags/<tag>
	public static function associateDeviceWithTag($deviceToken, $tag)
	{
		CLogging::debug("CUrbanAirship::associateDeviceWithTag(".$deviceToken.",".$tag.")");
		define('APPKEY', CConfig::UA_APP_KEY);
		define('PUSHSECRET', CConfig::UA_APP_MASTER_SECRET);
		define('PUSHURL', CConfig::UA_APP_URL.'/api/device_tokens/'
			.$deviceToken.'/tags/'.$tag);

		$session = curl_init(PUSHURL);
		curl_setopt($session, CURLOPT_USERPWD, APPKEY . ':' . PUSHSECRET);
		curl_setopt($session, CURLOPT_POST, True);
		curl_setopt($session, CURLOPT_HEADER, False);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, True);
		curl_setopt($session, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		$content = curl_exec($session);

		// Check if any error occurred
		$response = curl_getinfo($session);
		if($response['http_code'] != 200) {
			CLogging::debug("CUrbanAirship::associateDeviceWithTag() error - ".$response['http_code']);
		} else {
			CLogging::debug("CUrbanAirship::associateDeviceWithTag() sent");
		}

		curl_close($session);
		return $response;
	}

	// Mark a device token inactive.
	//
	// curl -X DELETE -u "<appkey>:<Secret>" -H "Content-Type: application/json" https://go.urbanairship.com/api/device_tokens/<device_token>/
	public static function deleteDeviceToken($deviceToken)
	{
		CLogging::debug("CUrbanAirship::deleteDeviceToken(".$deviceToken.")");
		return false;
	}

	// Send an alert containing $message to $deviceToken
	// curl -v -X POST -u "<appkey>:<Secret>" -H "Content-Type: application/json" --data '{"device_tokens": ["<device_token>"], "aps": {"alert": "<message>"}}' https://go.urbanairship.com/api/push/
	public static function alertDevice($deviceToken, $message)
	{
		CLogging::debug("CUrbanAirship::alertDevice(".$deviceToken.",".$message.")");

		define('APPKEY', CConfig::UA_APP_KEY);
		define('PUSHSECRET', CConfig::UA_APP_MASTER_SECRET);
		define('PUSHURL', CConfig::UA_APP_URL.'/api/push/');

		//'{"audience" : {"device_token" : "<DeviceToken>"}, "notification" : {"alert" : "Hello iOS device!"}, "device_types" : ["ios"]}'
		$payload = array(
            'audience' => array("device_token" => $deviceToken),
            'notification' => array(
            	"alert" => $message,
            	"ios" => array( "sound" => "cow" )
            ),
            'device_types' => "all"
      );

		$json = json_encode($payload);
		CLogging::debug("CUrbanAirship::alertDevice(".$json.")");

		$session = curl_init(PUSHURL);
		curl_setopt($session, CURLOPT_USERPWD, APPKEY . ':' . PUSHSECRET);
		curl_setopt($session, CURLOPT_POST, True);
		curl_setopt($session, CURLOPT_POSTFIELDS, $json);
		curl_setopt($session, CURLOPT_HEADER, False);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, True);
		curl_setopt($session, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
			'Accept: application/vnd.urbanairship+json; version=3;')
		);
		$content = curl_exec($session);

		// Check if any error occurred
		$response = curl_getinfo($session);
		if(
			 ! ( ($response['http_code'] === 200) || ($response['http_code'] === 202) )
		)
		{
			CLogging::debug("CUrbanAirship::alertDevice() error - ".$response['http_code']);
		} else {
			CLogging::debug("CUrbanAirship::alertDevice() sent");
		}

		curl_close($session);
		return $response['push_id'];
	}

	// Broadcast $message to all devices associated with $tag
	// curl -X POST -u "<appkey>:<Secret>" -H "Content-Type: application/json" --data '{"tags": ["<tag>"], "aps": {"alert": "<message>"}}' https://go.urbanairship.com/api/push/

	//curl -v -X POST -u "<AppKey>:<MasterSecret>" -H "Content-type: application/json" -H "Accept: application/vnd.urbanairship+json; version=3;" --data '{"audience" : {"device_token" : "<DeviceToken>"}, "notification" : {"alert" : "Hello iOS device!"}, "device_types" : ["ios"]}' https://go.urbanairship.com/api/push/
	public static function broadcastToTag($tag, $message)
	{
		CLogging::debug("CUrbanAirship::broadcastToTag(".$tag.",".$message.")");


		define('APPKEY', CConfig::UA_APP_KEY);
		define('PUSHSECRET', CConfig::UA_APP_MASTER_SECRET);
		define('PUSHURL', CConfig::UA_APP_URL.'/api/push/');

		//'{"audience" : {"device_token" : "<DeviceToken>"}, "notification" : {"alert" : "Hello iOS device!"}, "device_types" : ["ios"]}'
		$payload = array(
            'audience' => array("tag" => $tag),
            'notification' => array(
            	"alert" => $message,
            	"ios" => array( "sound" => "cow" )
            ),
            'device_types' => "all"
      	);

		$json = json_encode($payload);
		CLogging::debug("CUrbanAirship::broadcastToTag(".$json.")");

		$session = curl_init(PUSHURL);
		curl_setopt($session, CURLOPT_USERPWD, APPKEY . ':' . PUSHSECRET);
		curl_setopt($session, CURLOPT_POST, True);
		curl_setopt($session, CURLOPT_POSTFIELDS, $json);
		curl_setopt($session, CURLOPT_HEADER, False);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, True);
		curl_setopt($session, CURLOPT_HTTPHEADER, array(
			'Content-Type:application/json',
			'Accept: application/vnd.urbanairship+json; version=3;')
		);
		$content = curl_exec($session);

		// Check if any error occurred
		$response = curl_getinfo($session);
		if(
			 ! ( ($response['http_code'] === 200) || ($response['http_code'] === 202) )
		)
		{
			CLogging::debug("CUrbanAirship::broadcastToTag() error - ".$response['http_code']);
		} else {
			CLogging::debug("CUrbanAirship::broadcastToTag() sent");
		}

		curl_close($session);
		return $response['push_id'];
	}

	// Broadcast $message to all app subscribers
	// curl -X POST -u "<appkey>:<Secret>" -H "Content-Type: application/json" --data '{"aps": {"alert": "<message>"}}' https://go.urbanairship.com/api/push/broadcast/
	public static function broadcast($message)
	{
		CLogging::debug("CUrbanAirship::broadcast(".$message.")");
		define('APPKEY', CConfig::UA_APP_KEY);
		define('PUSHSECRET', CConfig::UA_APP_MASTER_SECRET);
		define('PUSHURL', CConfig::UA_APP_URL.'/api/push/broadcast/');

		$contents = array();
		$contents['badge'] = "+1";
		$contents['alert'] = $message;
		$contents['sound'] = "cow";
		$push = array("aps" => $contents);

		$json = json_encode($push);

		$session = curl_init(PUSHURL);
		curl_setopt($session, CURLOPT_USERPWD, APPKEY . ':' . PUSHSECRET);
		curl_setopt($session, CURLOPT_POST, True);
		curl_setopt($session, CURLOPT_POSTFIELDS, $json);
		curl_setopt($session, CURLOPT_HEADER, False);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, True);
		curl_setopt($session, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		$content = curl_exec($session);

		// Check if any error occurred
		$response = curl_getinfo($session);
		if($response['http_code'] != 200) {
			CLogging::debug("CUrbanAirship::broadcast() error - ".$response['http_code']);
		} else {
			CLogging::debug("CUrbanAirship::broadcast() sent");
		}

		curl_close($session);
		return $response['push_id'];
	}
}
?>
