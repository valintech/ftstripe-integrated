<?php
require_once("classes/common.inc");
require_once("classes/smsgwdb.inc");
require_once("Zend/Http/Client.php");

class CSmsGw extends CRoot
{
	const ID = "id";
	const NAME = "name";
	const URL = "url";
	const CREATED = 'created';
	const UPDATED = 'updated';


	private static $DBM01 = array('key' => CSmsGw::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CSmsGw::NAME, 'type' => CCommon::STRING_TYPE, 'size' => 64);
	private static $DBM03 = array('key' => CSmsGw::URL, 'type' => CCommon::STRING_TYPE, 'size' => 128);
	private static $DBM04 = array('key' => CSmsGw::CREATED, 'type' => CCommon::DATETIME_TYPE);
	private static $DBM05 = array('key' => CSmsGw::UPDATED, 'type' => CCommon::DATETIME_TYPE);


	/*
	 * Constructor
	 */
	
	function __construct ()
	{
		parent::__construct();
	}

	/*
	 * Save gateway details
	 * 
	 * @return                   true or false 
	 */
	
	public function save ()
	{
		return CSmsGwDb::save($this);
	}
	
	/*
	 * Load a gateway by ID
	 * 
	 * @param $id  Gateway id.
	 * @return     true or false
	 */
	
	public function load ($id)
	{
		return CSmsGwDb::load($id, $this);
	}

	public static function send($phoneNo, $text)
	{
		switch (CConfig::SMS_SERVER)
		{
			case 'TextLocal':
				CSmsGw::send_TextLocal($phoneNo, $text);
				break;
			case 'Android':
			default:
				CSmsGw::send_Android($phoneNo, $text);
				break;
		}
	}
	
	/*
	 * The android phone requires phone numbers to start with 0 and have
	 * the international "44" prefix removed
	 *
	 * This function attempts to ensure that the single passed number meets
	 * this requirement.
	 */
	private static function androidSanitisePhoneNo($phoneNo)
	{
		$phoneNo = preg_replace('/ /', '', $phoneNo);	//remove any spaces
		if (substr($phoneNo, 0, 3) == '+44')
		{
			$phoneNo = preg_replace('/^\+44/', '0', $phoneNo);
		}
		else if (substr($phoneNo, 0, 2) == '44')
		{
			$phoneNo = preg_replace('/^44/', '0', $phoneNo);
		}
		return $phoneNo;
	}


	private static function send_Android($phoneNo, $text)
	{
		$gw = new CSmsGw();
		$name = "Android-01";
		$gw->load($name);
		$msg = new stdClass;
		$msg->method = 'outgoingText';
		$params = new stdClass;
		$params->phoneNo = CSmsGw::androidSanitisePhoneNo($phoneNo);
		$params->msgText = $text;
		$msg->params = $params;
		try {
			$client = new Zend_Http_Client($gw->get('url'));
			$client->setParameterPost('q', CCommon::toJson($msg));
			$response = $client->request('POST');
			if ($response != '')
				CLogging::debug("SMS gateway said '".$response."'");
		}
		catch (Exception $e) {
			CLogging::error('Failed to contact SMS gateway.  Error '.$e->getCode().': '.$e->getMessage());
		}
	}
	
	/*
	 * The Text local requirements for phone numbers are:
	 * 		"A comma separated list of international mobile numbers. 
	 * 		Each number must be PURELY numeric, no + symbols or hyphens or spaces. 
	 * 		The numbers must start with the international prefix. In UK this would
	 * 		be 447xxxxxxxxx."
	 * 
	 * This function attempts to ensure that the numbers meet this requirement.
	 * It assumes only a single number at once, though the textlocal interface may
	 * accept a CSV list.
	 */
	private static function textLocalSanitisePhoneNo($phoneNo)
	{
		$phoneNo = preg_replace('/ /', '', $phoneNo);	//remove any spaces
		if (substr($phoneNo, 0, 3) == '+44')
		{
			$phoneNo = preg_replace('/^\+44/', '44', $phoneNo);
		}
		else if (substr($phoneNo, 0, 1) == '0')
		{
			$phoneNo = preg_replace('/^0/', '44', $phoneNo);
		}
		return $phoneNo;
	}
	
	private static function send_TextLocal($phoneNo, $text)
	{
		// Configuration variables
		$info = "1";	// Extra info returned in response to request.	
		$test = "0";	// 0 = send message , 1 = don't send SMS (is free)
		
		// Data for text message
		$from = CConfig::TL_FROM;
		$selectednums = CSmsGw::textLocalSanitisePhoneNo($phoneNo);
		$message = urlencode($text);
		
		// Prepare data for POST request
		$data = "uname=".CConfig::TL_USER
			."&pword=".CConfig::TL_PWD
			."&message=".$message
			."&from=".$from
			."&selectednums=".$selectednums
			."&info=".$info."&test=".$test;
		
		// Send the POST request with cURL
		$ch = curl_init(CConfig::TL_URL);		
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		if ( 0 != curl_errno($ch) )
		{
			CLogging::debug("TextLocal SMS gateway curl error#" 
					. curl_errno($ch) . ': ' . curl_error($ch) );
		}
		curl_close($ch);
		
		CLogging::debug("TextLocal SMS gateway response:'".$response."'");
	}
}

