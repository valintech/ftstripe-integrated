<?php
require_once("classes/root.inc");
require_once("classes/imlogdb.inc");

/*
 * Instant Message log class
 */

class CImLog extends CRoot
{
	// Members relating to database columns
	const ID = "id";
	const FROM_UID = 'from_user_id';
	const TO_UID = 'to_user_id';
        const GROUP_ID = 'group_id';
	const TEXT = "text";
	const CREATED = 'created';

	// Database column member definitions
	private static $DBM01 = array('key' => CImLog::ID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM02 = array('key' => CImLog::FROM_UID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM03 = array('key' => CImLog::TO_UID, 'type' => CCommon::INT_TYPE, 'size' => 11);
	private static $DBM04 = array('key' => CImLog::TEXT, 'type' => CCommon::STRING_TYPE, 'size' => 140);
	private static $DBM05 = array('key' => CImLog::CREATED, 'type' => CCommon::DATETIME_TYPE);
        private static $DBM06 = array('key' => CImLog::GROUP_ID, 'type' => CCommon::INT_TYPE, 'size' => 11);

	/*
	 * Constructor
	 */

	function __construct ()
	{
		parent::__construct('imlog');
	}

	/*
	 * Get record count
	 *
	 * @return count or false
	 */

	public static function records ()
	{
		return CImLogDb::records();
	}

	public static function log($from_user_id, $to_user_id, $msgText,$group)
	{
		$entry = new CImLog();

		$entry->set(CImLog::FROM_UID, $from_user_id);
		$entry->set(CImLog::TO_UID, $to_user_id);
                $entry->set(CImLog::GROUP_ID, $group);
                
		$entry->set(CImLog::TEXT, $msgText);

		return $entry->save();
	}

	/*
	 * Save log entry
	 *
	 * @param $includeMembers    [optional] Member keys to include
	 * @return                   true or false
	 */

	public function save ($includeMembers = false)
	{
		return CImLogDb::save($this, $includeMembers);
	}

	/*
	 * Load log entry by id
	 *
	 * @param $id             Entry id
	 * @return 		  true or false
	 */

	public function load ($id)
	{
		return CImLogDb::load($id, $this);
	}

	/*
	 * Return array of N most recent record IDs (newest first)
	 *
	 * @param $cnt       Number of record IDs to return
	 * @return           Array of record IDs
	 */

	public static function getLatestIds($cnt)
	{
		return CImLogDb::getLatestIds($cnt);
	}

	/*
	 * Return array of unacknowledged messages for a given user
	 *
	 * @param $user_id       Number of record IDs to return
	 * @param $last_acked    Last message user ack'ed
	 * @return               Array of record IDs
	 */

	public static function getActiveIds($user_id, $last_acked)
	{
		return CImLogDb::getActiveIds($user_id, $last_acked);
	}
}
?>
