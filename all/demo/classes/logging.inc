<?php
	require_once("includes/config.inc");
	require_once("classes/common.inc");
	
	/*
	 * Logging class
	 */
	
	class CLogging
	{
		const OPEN_TIMEOUT = 60;				// Seconds to try to open log
		
		/*
		 * Write logging information
		 * 
		 * @param $info Informatioon to be written
		 */
		
		public static function info ($info)
		{
			CLogging::_writeLog($info, '', 2);
		}
		
		/*
		 * Write error information
		 * 
		 * @param $info Informatioon to be written
		 */
		
		public static function error ($info)
		{
			CLogging::_writeLog($info, "[ERROR]", 0);
		}
		
		/*
		 * Write debug information
		 * 
		 * @param $info Information to be written
		 */
		
		public static function debug ($info)
		{
			CLogging::_writeLog($info, '', 1);
		}
		
		/*
		 * Write data to current log file.
		 *
		 * @param $data            Data to write
		 * @param $logDescription  Extra description info. for the entry i.e "[ERROR]"
		 * @param $level           Log level 0/1/2
		 */
		
		private static function _writeLog ($data, $logDescription = "", $level = 0)
		{
			// On my local setup I need to specify a timezone to stop
			// apache complaining, and as a side effect it also seemed
			// to fix issues with IM timestamps...
			//date_default_timezone_set('Europe/London');
			/*if ($level > CConfig::LOG_LEVEL)
				return;
			if ($logDescription != "")
				$logDescription .= " ";
			$retries = CLogging::OPEN_TIMEOUT / 2;
			$ip = CCommon::remoteAddress(true);
			do
			{
				$path = sprintf("%s/%s_%s.log", CConfig::LOG_DIR, CConfig::DB_NAME, date("Y-m-d"));
				$f = fopen($path, 'a');
				if ($f === false)
					sleep(2);
				else
				{
					$tmp = sprintf("%s %s - %s%s\n", date("Y-m-d H:i:s"), $ip, $logDescription, $data);
					fwrite($f, $tmp);
					fclose($f);
					$retries = 0;
				}
			}
			while ($retries--);*/
		}
	}
?>
