<?php
require_once('classes/root.inc');
require_once('classes/session.inc');

class CSessionWs extends CRoot
{
	/*
	 * Constructor
	 */
	
	function __construct ()
	{
		parent::__construct();
	}

	/*
	 * Validate session
	 * @param $sessionId     Session id.
	 * @param $pollSesiond   true to poll session if validation ok
	 * @return               true of false
	 */
	
	public static function validate ($sessionId, $pollSession = true)
	{
		$out = CSession::validate($sessionId, $pollSession);
		if ($out == true)
			$out = new stdClass;
		else
			$out = CCommon::makeErrorObj(CCommon::SESSION_EXPIRED, 'Invalid or expired session');
		return $out;
	}	
}
?>
