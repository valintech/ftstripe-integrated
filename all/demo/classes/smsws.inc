<?php
require_once('classes/common.inc');
require_once('classes/journey.inc');
require_once('classes/calllog.inc');
require_once('classes/msg.inc');
require_once('classes/smsgw.inc');

/*
 * SMS class (Webservice)
 */

class CSmsWs
{
	/**
	 * incomingText - used by SMS service to deliver text messages
	 * 
	 * @param string $phoneNo
	 * @param string $msgText
	 * @param $location        array of lng, lat, accuracy, etc
	 * @return object	null error on success or error plus 
	 *               	message string on failure.
	 */
	
	public static function incomingText ($phoneNo, $msgText, $location)
	{
		$user = new CUser();
		$user->loadWithPhoneNo($phoneNo);
		$user_id = $user->get(CUser::ID);
		$msgText = strtoupper(trim($msgText));

		if ($user_id == 0) {
			$user_id = '0';
			CCallLog::log($user_id, $phoneNo, 'R', $msgText);
			$txt = 'Mobile number not recognized';
			CLogging::error($txt." (".$phoneNo.")");
			CCallLog::log($user_id, $phoneNo, 'T', $txt);
			return new stdClass;
		}
		else
			CCallLog::log($user_id, $user->getFullName(), 'R', $msgText);

		if ($user->get(CUser::PHONE_CODE) != '') {
			if ($user->get(CUser::PHONE_CODE) == $msgText) {
				$user->set(CUser::PHONE_CODE, '');
				$user->save();
				CLogging::info('Mobile verification code received');
				// XXX Should we txt back confirmation to the user?
			} else {
				$txt = 'Phone number not verified, see email and profile page';
				CLogging::error($txt);
				CCallLog::log($user_id, $user->getFullName(), 'T', $txt);
				CSmsGw::send($phoneNo, $txt);
			}
			return new stdClass;
		}

		// Got a text, clearly not an app user
		if ($user->get('appuser') == 1) {
			$user->set('appuser', 0);
			$user->save();
		}
		$journey = new CJourney();
		$journey->loadActiveByPassengerId($user_id);
		$journey_id = $journey->get('id');
		if (!strncasecmp($msgText, 'END#', 4) || !strncasecmp($msgText, 'END.', 4)) {
			$miles = trim(substr($msgText, 4));
			return CMsg::endRide($user_id, $miles, $location);
		} else if ($journey_id == '') {
			// Expect a registration number
			return CMsg::startRide($user_id, $msgText, $location);
		} else {
			$txt = 'Please send "END.<distance>" when you finish your current journey';
			CLogging::error($txt);
			CCallLog::log($user_id, $user->getFullName(), 'T', $txt);
			CSmsGw::send($phoneNo, $txt);
			return new stdClass;
		}
	}
}
?>
