<?php

require_once("classes/logging.inc");
$tmp = pathinfo(__FILE__);
set_include_path(get_include_path() . PATH_SEPARATOR . $tmp["dirname"] . PATH_SEPARATOR . 'thirdparty/ZendFramework-1.10.5/library/');
require_once 'thirdparty/ZendFramework-1.10.5/library/Zend/Loader/Autoloader.php';

// register auto-loader
$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('My_');

/*
 * Common functions
 */

class CCommon {

    const EMAIL_ADDRESS_MAX = 256;     // Maximum length of an email address
    const INT_TYPE = 'i';       // 'integer' data type
    const FLOAT_TYPE = 'f';       // 'float' data type
    const STRING_TYPE = 's';      // 'string' data type
    const DATE_TYPE = 'd';       // 'date' data type
    const TIME_TYPE = 't';       // 'time' data type
    const DATETIME_TYPE = 'dt';      // 'datetime' data type
    const DECIMAL_TYPE = 'dc';      // 'DECIMAL' data type
    // Error codes (reported as negative offsets from 32000 in json replies.
    // Codes below 10 are reserved for the CLogin class.
    const JOURNEY_NOT_FOUND = 10;
    const JOURNEY_USER_NOT_FOUND = 11;
    const JOURNEY_ALREADY_REQUESTED = 12;
    const CANNOT_JOIN_OWN_JOURNEY = 13;
    const SESSION_EXPIRED = 14;
    const REG_NOT_IN_DB = 15;
    const ALREADY_IN_JOURNEY = 16;
    const INVALID_MILES_VALUE = 17;
    const NOT_IN_JOURNEY = 18;
    const BAD_EMAIL_ADDR = 19;
    const DATABASE_ERROR = 20;
    const NOT_LOGIN = 90;
    const USER_EXISTS = 21;
    const BAD_CAPTCHA = 22;
    const INVALID_PASSWORD = 23;
    const INSUFFICIENT_CREDIT = 24;
    const INVALID_PHONE_CODE = 25;
    const MOBILE_NOT_VERIFIED = 26;
    const MISSING_DEVICE_TOKEN = 27;
    const INVALID_PUSH_HANDLER_ID = 28;
    const UNKNOWN_ERROR = 99;
    const GROUP_EXISTS = 89;

    // Regular expression to recognise certain URI references. Results in $1
    // containing source attribute ( href, src, background ) , $2 contains
    // all between the "" characters i.e src="images/image1.jpg" would yeild
    // 'images/image1.jpg' in the $2 group. This is used to dynamically
    // modify HTML content to contect correct references when loaded via
    // a PHP script further back up the application tree.
    const HTML_MOD_REGEX = '/(href|src|background)="([\w\/_]+(.htm|.html|.css|.gif|.jpg|.png))"/';

    /*
     * Make a JSON error return object
     *
     * @param id     Error id.
     * @param text   Error message
     * @return       Object containing 'errorId' and 'errorText' members
     */

    public static function makeErrorObj($id, $text) {
        $out = new stdClass;
        $out->errorId = $id;
        $out->errorText = $text;
        return $out;
    }

    /*
     * Return IP address of remote user
     *
     * @param $padded  true to return as xxx.xxx.xxx.xxx
     * @return         IP address x.x.x.x or xxx.xxx.xxx.xxx if $padded is true
     */

    public static function remoteAddress($padded = false) {
        $out = $_SERVER["REMOTE_ADDR"];
        $out = ($out == "::1" || $out == "" ? "127.0.0.1" : $out);
        if ($padded) {
            $out = explode(".", $out);
            $out = sprintf("%03d.%03d.%03d.%03d", $out[0], $out[1], $out[2], $out[3]);
        }
        return $out;
    }

    /*
     * Dump request values to log
     */

    public static function dumpRequestVals() {
        CLogging::debug('----------REQUEST VALUES----------');
        foreach ($_REQUEST as $key => $val)
            CLogging::debug(sprintf('%s=%s', $key, $_REQUEST[$key]));
        CLogging::debug('----------------------------------');
    }

    /*
     * Dump request values to log
     */

    public static function dumpGetPostCookeVals() {
        CLogging::debug('-------------GET------------------');
        foreach ($_GET as $key => $val)
            CLogging::debug(sprintf('%s=%s', $key, $_GET[$key]));
        CLogging::debug('-------------POST-----------------');
        foreach ($_POST as $key => $val)
            CLogging::debug(sprintf('%s=%s', $key, $_POST[$key]));
        CLogging::debug('-------------COOKIE---------------');
        foreach ($_COOKIE as $key => $val)
            CLogging::debug(sprintf('%s=%s', $key, $_COOKIE[$key]));
        CLogging::debug('----------------------------------');
    }

    /*
     * Return a GET/POST/COOKIE value
     *
     * @param $key      The parameter key
     * @param $trimIt   Trim value
     * @return          The value
     */

    public static function getRequestValue($key, $trimIt = false) {
        $out = $_REQUEST[$key];

        if (isset($out) == false)
            return $out;
        $out = rawurldecode($out);
        if (get_magic_quotes_gpc())
            $out = stripslashes($out);
        return $trimIt ? trim($out) : $out;
    }

    /*
     * Map a list of values from $_REQUEST object into an object
     * based on "CRoot"
     *
     * @param $keys         Array of mapping keys as "xxx=yyy". The xxx key is an index
     *                      into the superglobal $_REQUEST array. The yyy key is a key
     *                      to story in the $o object
     * @param $o            Object derived from CRoot to set request values into
     * @param $convertJson  True to automatically convert from JSON
     */

    public static function mapRequestValues($keys, $o, $convertJson = false) {
        foreach ($keys as $key)
            if ($key != "") {
                $tmp = explode("=", $key);
                $val = CCommon::getRequestValue($tmp[0], true);

                if ($convertJson)
                    $val = CCommon::fromJson($val);
                if (trim($tmp[1]) != "")
                    $o->set($tmp[1], $val);
                else
                    $o->set($tmp[0], $val);
            }
    }

    /*
     * Map a request variable into an object member
     *
     * @param $o            Object derived from CRoot to set request values into
     * @param $fromKey      The key for the incoming request variable
     * @param $toKey        [optional] The key to store the value under in the CRoot based $o object
     * @param $callback     [optional] A callback passed directly to the PHP method 'call_user_func'
     */

    public static function mapRequestValue($o, $fromKey, $toKey = '', $callback = false) {
        $val = CCommon::getRequestValue($fromKey, true);
        if ($callback)
            $val = call_user_func($callback, $val);
        if ($toKey != '')
            $o->set($toKey, $val);
        else
            $o->set($fromKey, $val);
    }

    /*
     * Make a map key suitable for passing to "mapRequestValues" method
     *
     * @param $left   Value on the left of xxx=yyy
     * @param $right  Value on the right of xxx=yyy
     * @return        xxx=yyy type string
     * @see           mapRequestValues
     */

    public static function makeRequestKeyMap($left, $right = '') {
        return sprintf("%s=%s", $left, $right);
    }

    /*
     * Send back XmlHttpRequest result
     *
     * @param $data   Data to send
     * @param @mime   Mime type ( default to text/plain )
     */

    public static function xhrSend($data, $mimeType = "text/plain") {
        header(sprintf("Content-Type: %s", $mimeType));

        print($data);
    }

    /*
     * Export data to JSON
     *
     * @param $data  Data to export
     * @return       JSON encoded version of $data
     */

    protected static function _objToArray(&$val, $key) {
        if (is_object($val) && is_a($val, 'CRoot'))
            $val = $val->toArray();
    }

    public static function toJson($data) {
        // If input data is an array and it contains CRoot based objects,
        // convert them to arrays first
        // Walk arrays to convert CRoot objects to arrays
        if (is_array($data))
            array_walk_recursive($data, 'CCommon::_objToArray');
        // Encode it
        return json_encode($data);
    }

    /*
     * Import JSON data
     *
     * @param $data   JSON data to import
     * @param $assoc  true to return associative array
     * @return        The data or null if failed
     */

    public static function fromJson($data, $assoc = false) {
        return json_decode($data, $assoc);
    }

    /*
     * Enable conditional includes using magic strings within
     * the include statement.
     * It is passed the
     *
     * For example
     *
     * @param $includeString
     * 			String containing the text of an include directive,
     * 			which may be a vanilla filename, such as:
     * 				"loginws.inc"
     * 			which shall be returned to the caller verbatim, or a
     * 			directive (always starting with '#'), such as
     * 				"#SITE_PAYMENT_FRAMEWORK_INCLUDE"
     * 			the precise meaning of which is hardcoded within this function.
     *
     * 			Typically an include file name will be returned that
     * 			varies with the value of a particular PHP global constant.
     *
     * @return
     * 			Filename to be included.
     */

    private static function conditionalInclude($includeString) {
        switch ($includeString) {
            case '#SITE_PAYMENT_FRAMEWORK_INCLUDE':
                switch (CConfig::SITE_PAYMENT_FRAMEWORK) {
                    case 'PaypalSandbox':

                        $filename = "payment_pp_sandbox.html";
                        break;
                    case 'PaypalLive':
                        $filename = "payment_pp_live.html";
                        break;
                    case 'DumbButtons':
                    default:
                        $filename = "payment_dumb_button.html";
                        break;
                }
                break;

            case '#SMS_SERVER_INCLUDE':
                switch (CConfig::SMS_SERVER) {
                    case 'TextLocal':
                        $filename = "";
                        break;
                    case 'Android':
                    default:
                        $filename = "";
                        break;
                }
                break;

            default:
                $filename = $includeString;
                break;
        }
        return $filename;
    }

    /*
     * Replace 'includes' directives in HTML output
     *
     * @param $in		HTML file containing directives of the form
     * 			<!--incl='filename'-->.  If fileanme contains
     * 			a '/' it is used as is, otherwise it is
     * 			assumed to live in teh relevant content dir.
     * @return		HTML stream with directives replaced with
     * 			appropriate file contents
     */

    protected static function handleIncludes($in) {
        $head = "<!--incl='";
        $tail = "'-->";
        $out = "";
        $offset = 0;
        while ($offset < strlen($in)) {
            if (($i = strpos($in, $head, $offset)) === false) {
                $out .= substr($in, $offset);
                break;
            }
            $out .= substr($in, $offset, $i - $offset);
            $i += strlen($head);
            if (($j = strpos($in, $tail, $i)) === false) {
                $out .= "...incl= error...";
                break;
            }
            $includeString = substr($in, $i, $j - $i);
            $filename = CCommon::conditionalInclude($includeString);
            if (strpos($filename, "/") === false)
                $filename = sprintf('%s/%s', CConfig::CONTENT_DIR, $filename);
            if (($content = file_get_contents($filename, FILE_USE_INCLUDE_PATH)) == false)
                $out .= "...missing file " . $srcFile . " ...";
            else
                $out .= $content;
            $offset = $j + strlen($tail);
        }
        return $out;
    }

    /*
     * Replace placeholders in HTML output
     *
     * @param $srcFile            HTML file containing placeholders
     * @param $replaceList        An array of replacements as [0][...] [1][...] etc.
     *                            The array index translates into an HTML comment
     *                            <!--#rplc0#--> which is to be replaced by PHP output.
     * @param $removeWhiteSpace   [optional] true = remove tab, CR etc
     * @param $modLinks           [optional] true = modify certain links ( href, src, background )
     *                            to contain correct relative link to content.
     * @param @removeComments     [optional] remove all other comments
     * @return                    HTML stream with placeholders replaced
     */

    public static function htmlReplace($srcFile, $replaceList, $removeWhiteSpace = true, $modLinks = true, $removeComments = false) {
        $tmp = pathinfo($srcFile);
        if ($tmp['dirname'] == '' || $tmp['dirname'] == '.')
            $srcFile = sprintf('%s/%s', CConfig::CONTENT_DIR, $srcFile);
        $data = file_get_contents($srcFile, FILE_USE_INCLUDE_PATH);
        if ($data == false) {
            CLogging::error(sprintf("Cannot load HTML file [%s]", $srcFile));
            return sprintf('<div style="font-weight:bold;font-size:18pt">The file %s is missing or not accessible</div>', $srcFile);
        }

        $data = CCommon::handleIncludes($data);

        $searchList1 = array();
        $searchList2 = array();
        foreach ($replaceList as $key => $replace) {
            $searchList1[] = sprintf("<!--#rplc%d#-->", $key);
            $searchList2[] = sprintf("/**#rplc%d#**/", $key);
        }
        $data = str_replace($searchList1, $replaceList, $data);
        $data = str_replace($searchList2, $replaceList, $data);

        // Remove any other HTML comments <!-- ... -->
        if ($removeComments)
            for (;;)
                if (
                        ($start = strpos($data, "<!--")) !== false && ($end = strpos($data, "-->", $start)) !== false
                )
                    $data = substr_replace($data, "", $start, ($end + 3) - $start);
                else
                    break;

        // Remove any white space
        if ($removeWhiteSpace)
            $data = str_replace(array(">\n<", ">\r<", "\t", "\n"), array("><", "><", "", ""), $data);

        // Modify links
        if ($modLinks) {
            $patterns = self::htmlReplacePatterns();
            if (is_array($modLinks))
                $patterns = array_merge($modLinks, $patterns);
            foreach ($patterns as $pattern)
                $data = preg_replace($pattern->search, $pattern->replace, $data);
        }

        return $data;
    }

    /*
     * Return standard set of HTML replace regex patterns
     *
     * @return Array of objects containing "search", "replace" members
     */

    public static function htmlReplacePatterns() {
        $out = array();
        $out[] = self::createHtmlReplacePattern(CCommon::HTML_MOD_REGEX, sprintf('$1="%s/$2"', CConfig::CONTENT_DIR));
        $out[] = self::createHtmlReplacePattern('/background-image: url\(images/', sprintf('background-image: url(%s/images', CConfig::CONTENT_DIR));
        $out[] = self::createHtmlReplacePattern('/background="images/', sprintf('background="%s/images', CConfig::CONTENT_DIR));
        return $out;
    }

    /*
     * Return eRideShare/eco-gogo/firstthumb/whatever HTML replace patterns
     *
     * @param[in] $isValidSession   [optional] True or false. This determines which navigations
     * 								bar options are shown i.e register / profile, sign in / sign out
     * @return						Array of pattern objects
     * 							    (see CCommon::createHtmlReplacePattern )
     */

    public static function ersReplacePatterns($isValidSession = false) {
        $opt2 = ($isValidSession ? 'login.php?op=signOut' : 'login.php');
        $signInOut = ($isValidSession ? 'Sign Out' : 'Sign In');
        $regProf = ($isValidSession ? 'My Profile' : 'Register');
        $patterns = array();
        $patterns[] = CCommon::createHtmlReplacePattern('/href="login\.html"/', sprintf('href="%s"', $opt2));
        $patterns[] = CCommon::createHtmlReplacePattern("/location='login\.htm'/", sprintf("location='%s'", $opt2));
        $patterns[] = CCommon::createHtmlReplacePattern('/href="([a-zA-Z_]+)\.(htm|html)"/', 'href="$1.php"');
        $patterns[] = CCommon::createHtmlReplacePattern("/location='([a-zA-Z_]+)\.(htm|html)'/", "location='$1.php'");
        $patterns[] = CCommon::createHtmlReplacePattern('/@import "iphonenav\.css"/', sprintf('@import "%s/iphonenav.css"', CConfig::CONTENT_DIR));
        $patterns[] = CCommon::createHtmlReplacePattern('/#SignInOrSignOut#/', $signInOut);
        $patterns[] = CCommon::createHtmlReplacePattern('/#RegisterOrProfile#/', $regProf);
        return $patterns;
    }

    /*
     * Create a pattern object to be passed to 'CCommon::htmlReplace'
     *
     * @param $search   A 'preg_replace' pattern
     * @param $replace  Replace string
     * @return stdClass object containing "search", "replace" members
     */

    public static function createHtmlReplacePattern($search, $replace) {
        $out = new stdClass;
        $out->search = $search;
        $out->replace = $replace;
        return $out;
    }

    /*
     * Make a PHP timestamp from a JSON encoded date
     *
     * The $json data will expand into an object containing members
     * day, month, year
     *
     * @param $json   JSON encoded date object
     * @return        PHP timestamp
     */

    public static function jsonDateToInternal($json) {
        $o = CCommon::fromJson($json);
        return $o && $o->year ? mktime(0, 0, 0, $o->month, $o->day, $o->year) : 0;
    }

    /*
     * Make a PHP timestamp from a JSON encoded time
     *
     * The $json data will expand into an object containing members
     * hour, minute, second
     *
     * @param $json   JSON encoded time object
     * @return        PHP timestamp
     */

    public static function jsonTimeToInternal($json) {
        $o = CCommon::fromJson($json);
        return $o && $o->hour ? mktime($o->hour, $o->minute, $o->second) : 0;
    }

    /*
     * Redirect to another page
     */

    public static function redirect($url) {
        header(sprintf('Location: %s', $url));
        exit;
    }

    /*
     * Convert timestamp string to Array
     *
     * @param $ts  A timestamp (yyyymmddhhmmss, yyyymmdd)
     * @return     PHP timestamp
     */

    public static function tsToObj($ts) {
        $tmp = getdate(self::tsToPhp($ts));
        $out = new stdClass;
        $out->year = $tmp['year'];
        $out->month = $tmp['mon'];
        $out->day = $tmp['mday'];
        $out->hour = $tmp['hours'];
        $out->minute = $tmp['minutes'];
        $out->second = $tmp['seconds'];
        return $out;
    }

    /*
     * Convert time string to Array
     *
     * @param $ts  A time (hhmmss)
     * @return     PHP timestamp
     */

    public static function tmToObj($ts) {
        $tmp = getdate(self::tmToPhp($ts));
        $out = new stdClass;
        $out->year = 0;
        $out->month = 0;
        $out->day = 0;
        $out->hour = $tmp['hours'];
        $out->minute = $tmp['minutes'];
        $out->second = $tmp['seconds'];
        return $out;
    }

    /*
     * Convert timestamp to a PHP value
     *
     * @param $ts  A timestamp (yyyymmddhhmmss, yyyymmdd)
     * @return     PHP timestamp
     */

    public static function tsToPhp($ts) {
        $phpTs = new stdClass;
        if (strlen($ts) >= 8) {
            $phpTs->year = substr($ts, 0, 4);
            $phpTs->month = substr($ts, 4, 2);
            $phpTs->day = substr($ts, 6, 2);
        }
        if (strlen($ts) >= 14) {
            $phpTs->hour = substr($ts, 8, 2);
            $phpTs->minute = substr($ts, 10, 2);
            $phpTs->second = substr($ts, 12, 2);
        }
        return mktime((int) $phpTs->hour, (int) $phpTs->minute, (int) $phpTs->second, (int) $phpTs->month, (int) $phpTs->day, (int) $phpTs->year);
    }

    /*
     * Convert time to a PHP value
     *
     * @param $ts  A time (hhmmss)
     * @return     PHP timestamp
     */

    public static function tmToPhp($ts) {
        $phpTs = new stdClass;
        if (strlen($ts) >= 6) {
            $phpTs->hour = substr($ts, 0, 2);
            $phpTs->minute = substr($ts, 2, 2);
            $phpTs->second = substr($ts, 4, 2);
        }
        return mktime((int) $phpTs->hour, (int) $phpTs->minute, (int) $phpTs->second);
    }

    /*
     * Send email message
     *
     * @param cfg     Config. object contains members 'from', 'to', 'toName', 'fromName'
     *                'subject', 'body'
     * @return        true or false
     */

    public static function sendMail($cfg) {
        CLogging::debug(sprintf('CCommon::sendMail - from [%s] to [%s] subject [%s]', $cfg->from, $cfg->to, $cfg->subject));

        // use PEAR "mail" package?
        if (CConfig::USE_PEAR_MAIL) {
            if (!class_exists('Mail')) {
                include("Mail.php");
                include("Mail/mime.php");
            }

            $hdrs = array();
            $from = ($cfg->fromName ? sprintf('%s <%s>', $cfg->fromName, $cfg->from) : $cfg->from);
            $to = ($cfg->toName ? sprintf('%s <%s>', $cfg->toName, $cfg->to) : $cfg->to);
            if ($cfg->subject)
                $subject = $cfg->subject;
            else {
                $region = new CRegion();
                $subject = 'Mail from ' . $region->msg(8, 'common');
            }
            $hdrs['From'] = $from;
            $hdrs['To'] = $to;
            $hdrs['Subject'] = $subject;

            $mime = new Mail_mime("\n");
            if ($cfg->bodyText)
                $mime->setTXTBody($cfg->bodyText);
            if ($cfg->body)
                $mime->setHTMLBody($cfg->body);
            //do not ever try to call these lines in reverse order
            $body = $mime->get();
            $hdrs = $mime->headers($hdrs);

            $params = array();
            if (CConfig::MAIL_TYPE == 'smtp') {
                $params['host'] = CConfig::MAIL_SMTP_SERVER;
                $params['port'] = CConfig::MAIL_SMTP_PORT;
                $params['auth'] = true;
                $params['username'] = CConfig::MAIL_ACCOUNT_USER;
                $params['password'] = CConfig::MAIL_ACCOUNT_PWD;
            }
            $mailer = Mail::factory(CConfig::MAIL_TYPE, $params);
            if (($err = $mailer->send($to, $hdrs, $body)) !== true) {
                CLogging::error(sprintf('CCommon::sendMail - failed to send to user [%s] ... got [%s]', $to, $err->getMessage()));
                return false;
            }
        } else {
            $uid = strtoupper(md5(uniqid(time())));
            $header = "From: {$cfg->from}\nReply-To: {$cfg->from}\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/mixed; boundary=$uid\n";
            $header .= "--$uid\n";
            $header .= "Content-Type: text/html\n";
            $header .= "Content-Transfer-Encoding: 8bit\n\n";
            $header .= "{$cfg->body}\n";
            $header .= "--$uid--";
            if ($cfg->bodyText) {
                $header .= "Content-Type: text/plain\n";
                $header .= "Content-Transfer-Encoding: 8bit\n\n";
                $header .= "{$cfg->bodyText}\n";
                $header .= "--$uid--";
            }
            if (mail($cfg->to, $cfg->subject, "", $header) == false) {
                CLogging::error(sprintf('CCommon::sendMail - failed to send to user [%s]', $cfg->to));
                return false;
            }
        }

        return true;
    }

    /*
     *  Pdf generation
     * 
     * @params $table_headers - table headings
     * @params $data - mysql result
     * @params $filename - pdf file name
     * 
     * returns $group - group report id
     * 
     */

    public static function generatePdf($table_headers, $data, $filename) {
        try {

            $pdf = new My_Pdf_Document($filename, '.');

            // create page
            $page = $pdf->createPage();

            // define font resource
            $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

            // set font
            $page->setFont($font, 24);

            // create table
            $table = new My_Pdf_Table(3);

            $row_h = new My_Pdf_Table_Row();
            $cols_header = array();
            foreach ($table_headers as $header) {
                $head = new My_Pdf_Table_Column();
                $head->setWidth(100);
                $head->setText($header);
                $cols_header[] = $head;
            }
            $row_h->setColumns($cols_header);
            $table->addRow($row_h);
            $row_h->setFont($font, 14);
            $row_h->setBorder(My_Pdf::TOP, new Zend_Pdf_Style());
            $row_h->setBorder(My_Pdf::BOTTOM, new Zend_Pdf_Style());
            $row_h->setBorder(My_Pdf::LEFT, new Zend_Pdf_Style());
            $row_h->setBorder(My_Pdf::RIGHT, new Zend_Pdf_Style());
            $row_h->setCellPaddings(array(10, 10, 10, 10));
            $i = 0;
            $group = 0;
            //  $data = CDb::query("SELECT id,carbon_savings, total_mileage FROM groupreport WHERE month=$month and year=$year and group_id=$group_id");

            while ($o = mysql_fetch_object($data)) {
                $row = new My_Pdf_Table_Row();
                $cols = array();

                $col_s = new My_Pdf_Table_Column();
                $col_s->setWidth(100);
                $col_s->setText(++$i);
                $cols[] = $col_s;

                foreach ($o as $k => $v) {
                    if (strcmp($k, "id") !== 0) {
                        $col = new My_Pdf_Table_Column();
                        $col->setWidth(100);
                        $col->setText($v);
                        $cols[] = $col;
                    }
                    else
                        $group = $v;
                }
                $row->setColumns($cols);
                $row->setFont($font, 14);
                $row->setBorder(My_Pdf::TOP, new Zend_Pdf_Style());
                $row->setBorder(My_Pdf::BOTTOM, new Zend_Pdf_Style());
                $row->setBorder(My_Pdf::LEFT, new Zend_Pdf_Style());
                $row->setBorder(My_Pdf::RIGHT, new Zend_Pdf_Style());
                $row->setCellPaddings(array(10, 10, 10, 10));
                $table->addRow($row);
            }

            mysql_free_result($data);

            $page->addTable($table, 0, 0);

            // add page to document
            $pdf->addPage($page);

            // save as file
            $pdf->save();
            // save as file

            return $group;
        } catch (Zend_Pdf_Exception $e) {
            die('PDF error: ' . $e->getMessage());
        } catch (Exception $e) {
            die('Application error: ' . $e->getMessage());
        }
    }

    /*
     *  Send  mail
     * 
     * @params $params - array of parameters with to,from,filename,body
     * 
     * returns $send - sent status
     * 
     */
 public static function sendMailForgot($cfg) {
        CLogging::debug(sprintf('CCommon::sendMail - from [%s] to [%s] subject [%s]', $cfg->from, $cfg->to, $cfg->subject));

  
            $uid = strtoupper(md5(uniqid(time())));
		$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers

$headers .=  "From: {$cfg->from}\nReply-To: {$cfg->from}\n";

           /* $header = "From: {$cfg->from}\nReply-To: {$cfg->from}\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/mixed; boundary=$uid\n";
            $header .= "--$uid\n";
            $header .= "Content-Type: text/html\n";
            $header .= "Content-Transfer-Encoding: 8bit\n\n";
            $header .= "{$cfg->body}\n";
            $header .= "--$uid--";*/
         /*   if ($cfg->bodyText) {
                $header .= "Content-Type: text/plain\n";
                $header .= "Content-Transfer-Encoding: 8bit\n\n";
                $header .= "{$cfg->bodyText}\n";
                $header .= "--$uid--";
            }*/
 

           if (mail($cfg->to, $cfg->subject,$cfg->body, $headers) == false) {
                CLogging::error(sprintf('CCommon::sendMail - failed to send to user [%s]', $cfg->to));
                return false;
            }


        return true;
    }
    public static function sendEMail($params) {


        $mail = new Zend_Mail();
        $mail->setBodyHtml($params['body']);

        $mail->setFrom(CConfig::INFO_EMAIL);
        if (isset($params['to'])) {
            //$mail->addTo($params['to']);
            if (count($params['to']) == 1) {
                $mail->addTo($params['to']);
            } else {
                $mail->addTo(CConfig::INFO_ACKEMAIL);
                $mail->addBcc($params['to']);
            }
        }
        if (isset($params['bcc'])) {
            $mail->addBcc($params['bcc']);
        }
        if (isset($params['cc'])) {
            //$mail->addCc($params['cc']);  /*Changing  cc   to bcc as per client request* /
            $mail->addBcc($params['cc']);
        }

        $mail->setSubject($params['subject']);
        if (isset($params['attachment'])) {
            $content = file_get_contents($params['attachment']); // e.g. ("attachment/abc.pdf")
            $attachment = new Zend_Mime_Part($content);
            $attachment->type = 'application/pdf';
            $attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $attachment->encoding = Zend_Mime::ENCODING_BASE64;
            $attachment->filename = $params['filename']; // name of file

            $mail->addAttachment($attachment);
        }
        $sent = true;
        try {
            
        } catch (Exception $e) {

            $sent = false;
        }


        $mail->send();
        //echo $params['body'];

        return $sent;
    }

    // Encrypt Function
    public function mc_encrypt($encrypt, $key) {
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', $key);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt . $mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt) . '|' . base64_encode($iv);
        return $encoded;
    }

// Decrypt Function
    function mc_decrypt($decrypt, $key) {
        $decrypt = explode('|', $decrypt . '|');
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        if (strlen($iv) !== mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)) {
            return false;
        }
        $key = pack('H*', $key);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
        if ($calcmac !== $mac) {
            return false;
        }
        $decrypted = unserialize($decrypted);
        return $decrypted;
    }

}

/*
 * Class to handler a URL string
 */

class CUrl {

    public $_url;
    public $_params;

    /*
     * Constructor
     */

    function __construct($url) {
        $tmp = parse_url($url);
        $this->_url = $tmp['scheme'] . $tmp['host'] . $tmp['user'] . $tmp['pass'] . $tmp['path'];
        $params = ($tmp['query'] != '' ? explode('&', $tmp['query']) : array());
        $this->_params = array();
        foreach ($params as $param) {
            $tmp = explode('=', $param);
            $this->_params[$tmp[0]] = $tmp[1];
        }
    }

    /*
     * Get full URL string
     *
     * @return URL string http://xxx.yyy?a=1&b=2&c=3
     */

    public function getUrl() {
        $params = array();
        foreach ($this->_params as $key => $val)
            $params[] = sprintf('%s=%s', $key, $val);
        return $this->_url . '?' . join('&', $params);
    }

}

/*
 * Location class
 */

class CLocation {

    public $lat;
    public $lng;
    public $label;

    function __construct($label, $lat = '', $lng = '') {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->label = $label;
    }

}

?>
