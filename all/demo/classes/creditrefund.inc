<?php

require_once('classes/root.inc');
require_once('classes/user.inc');
require_once('classes/session.inc');
require_once('classes/carpool.inc');
require_once('classes/user.inc');
require_once('classes/common.inc');
require_once('classes/journey.inc');
require_once('classes/region.inc');
require_once('classes/google.inc');
require_once('classes/yahoo.inc');
require_once('classes/inifile.inc');
require_once('classes/paypal.inc');
if (CConfig::RUN_IN_FB)
    require_once('classes/facebook.inc');

/*
 * REFUND class
 */

class CCreditRefund extends CRoot {
    /*
     * Class error codes. 1 to 9 are reserved for this class, all others
     * are in common.inc.
     */

    const SUCCESSREFUND = 1;
    const INVALID_REFUND_ERROR = 1;
    const INVALID_REFUND_ERROR_FAIL = 1;
    const INVALID_REFUND_ERROR_PAYPAL = 1;

    /*
     * Constructor
     * 

     */

    function __construct() {
        parent::__construct();
        $this->set('lastErrorRefund', 0);
    }

    /*
     * Refund to system
     * 

     */

    function creditRefunds($isValidSession, $credit, $login) {

        // If we are using dumb buttons (i.e. debug), then have a clause
// for increasing balance:
        $out = new stdClass;
        $journey = new CJourney();
        if (CConfig::SITE_PAYMENT_FRAMEWORK === 'DumbButtons') {
            if ($isValidSession && $credit != '') {
                $user = $login->getUser(true);
                if (
                        $credit < 0 &&
                        ( ($user->get(CUser::CREDIT) + $credit) < CConfig::MIN_CREDIT_IN_MILES )
                ) {

                    $out = CCommon::makeErrorObj(40, 'Refund failed: Insufficient Credit. ' .
                                    'A minimum balance of ' . CConfig::MIN_CREDIT_IN_MILES .
                                    ' miles must be maintained.');
                    CCommon::xhrSend(CCommon::toJson($out));
                    exit;
                }
                $user->set('credit', $user->get('credit') + $credit);
                $user->save();
                $user = $login->getUser(true);
                CWebSession::set('login', serialize($login));
                if ($credit > 0) {
                    header("Location: next_step.php");
                } else {

                    $out = CCommon::makeErrorObj(0, 'Job done!');
                    CCommon::xhrSend(CCommon::toJson($out));
                }
                exit;
            }
        } else {
            // If $credit is negative, then it's a refund request
            // e.g.: "Please let me cash in 50 miles from my eco-gogo account"
            // Ignore requests for increasing balance, as they go
            // though paypal and ipn.php.

            if ($isValidSession && $credit != '' && $credit < 0) {
                $user = $login->getUser(true);

                //If user is in the middle of a ride, then exit
              
                $journey->loadActiveByPassengerId($user->get(CUser::ID));
                if ($journey->get('id') != 0) {

                    $this->set('lastErrorRefund', self::INVALID_REFUND_ERROR);
                    return false;
                }

                //If user doesn't have enough credit then exit.
                //In a request for 50 miles, $credit=='-50'
                if (($user->get(CUser::CREDIT) + $credit) < CConfig::MIN_CREDIT_IN_MILES) {


                    $this->set('lastErrorRefund', self::INVALID_REFUND_ERROR_FAIL);
                    return false;
                }

                //Send refund request to paypal and decrement website credit
                //only if successful.
                //CPayPal::refund takes +ve args so invert $credit before passing.
                $user_paypal_email = $user->get(CUser::PAYPAL_EMAIL);
                if (NULL === $user_paypal_email) {
                    // Resort to standard email in the hope that it works.
                    $user_paypal_email = $user->get(CUser::EMAIL);
                }
                $refund_gbp = CPayPal::refund((0 - $credit), $user_paypal_email);
                if (-1 != $refund_gbp) {
                    //Decrease user's credit by the amount of the refund.
                    //TODO - log these increases in the database.
                    //They will need to be reconciled against the IPNs
                    //and our paypal balance in future.
                    $user->set(CUser::CREDIT, $user->get(CUser::CREDIT) + $credit);
                    $user->save();
                    return "Refund successful: Your paypal account: " .
                            $user_paypal_email . " has been credited £" .
                            number_format($refund_gbp, 2) . "\n";
                } else {
                    $this->set('lastErrorRefund', self::INVALID_REFUND_ERROR_PAYPAL);
                    return false;
                }


                //TODO - should we email them ourselves when the IPN comes in or?

                $user = $login->getUser(true);
                CWebSession::set('login', serialize($login));
                header("Location: next_step.php");
                exit;
            }
        }
    }

    function lastErrorRefund() {
        return $this->get('lastErrorRefund');
    }

}

?>
