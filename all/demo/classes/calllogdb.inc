<?php
/*
 * Call log class
 */

class CCallLogDb
{
	const RECORDS_SQL = "SELECT COUNT(*) calllog_count FROM calllog";
	const NEW_SQL = "INSERT INTO calllog (%s) VALUES (%s)";
	const GET_CALLLOG_SQL = "SELECT %s FROM calllog WHERE %s='%s'";
	
	/*
	 * Constructor
	 */
	
	function __construct ()
	{
	}
	
	/*
	 * Get an entry using CallLog ID.
	 * 
	 * @param $id      Entry id.
	 * @param $log     A CCallLog object to load into
	 * @return         true or false
	 */
	
	public static function load ($id, $log)
	{
		return self::_loadCore(CCallLog::ID, $id, $log);
	}
	
	/*
	 * Core for "load..." methods
	 * 
	 * @param $fieldId   Id. of field to query
	 * @param $fieldVal  Query field value
	 * @param $log       A CCallLog object to load into
	 * @return           true or false
	 */
	
	protected static function _loadCore ($fieldId, $fieldVal, $log)
	{
		$cols = CRoot::getDbMembers('CCallLog');
		$types = array();
		foreach ($cols as $colId => $col) {
			$cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
			$types[$colId] = $col['type'];
		}
		$sql = sprintf(CCallLogDb::GET_CALLLOG_SQL, join(',', $cols), $fieldId, $fieldVal);
               /*  $file = fopen("/var/www/html/eco-gogo_valin/sample1.txt","a+");
          fwrite($file,$sql);
          fclose($file); */

		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = mysql_fetch_object($res);
		if ($o) {
			foreach ($o as $colId => $val)
				$log->set($colId, CDb::makePhpValue($val, $types[$colId]));
		}
		return true;
	}
	
	/*
	 * Save a log entry
	 * 
	 * @param $log               A CCallLog object to save
	 * @param $includeMembers    [optional] Member keys to include
	 * @return                   true if no reported DB error otherwise false
	 */
	
	public static function save ($log, $includeMembers = false)
	{
		// Get list of columns. Include id. column if already exists
		$id = $log->get(CCallLog::ID);
		$cols = CRoot::getFilteredDbMembers('CCallLog', $includeMembers, array(CCallLog::ID));

		// Turn data into SQL compatible strings
		$vals = array();
		foreach ($cols as $colId => $col)
			$vals[$colId] = CDb::makeSqlValue($log->get($colId), $col['type']);

		// Create SQL statement for insert or update
		if ($id == 0)
		{
			$vals[CCallLog::CREATED] = 'now()';
                        if($vals[CCallLog::JOURNEY_ID]== 'NULL' ){
                        $vals[CCallLog::JOURNEY_ID]=0;
 //$file = fopen('/home1/eridesh1/public_html/demo/classes/newfilecalllog.txt', "w");
      // fwrite($file, print_r('kkkk', true));
      // fclose($file);

}
			$sql = sprintf(CCallLogDb::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
                        
		}
		else
		{
			return false;
		}

		CDb::BeginTrans();
		$res = CDb::query($sql);
		if ($res)
		{
			// Retrieve the auto ID if "insert" performed
			if ($id == 0)
			{
				$id = CDb::getLastAutoId();
				$log->set(CCallLog::ID, $id);
			}
			CDb::commitTrans();
		}
		else
			CDb::rollbackTrans();
		
		return $res ? true : false;
	}
	
	/*
	 * Get count of log records
	 * 
	 * @return count
	 */
	
	public static function records ()
	{
		$sql = sprintf(CCallLogDb::RECORDS_SQL);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$o = CDb::getRowObject($res);
		if ($o == false)
			return false;
		return (int)$o->calllog_count;
	}

	/*
	 * Return array of N most recent record IDs (newest first)
	 *
	 * @param $cnt       Number of record IDs to return
	 * @return           Array of record IDs
	 */

	public static function getLatestIds($cnt)
	{
		$sql = sprintf("select id from calllog order by id desc limit %d", $cnt);
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$out = array();
		while (($o = CDb::getRowArray($res)))
		{
			$out[] = $o[CCallLog::ID];
		}
		return $out;
	}

	/*
	 * Return array of IDs of unacknowledged messages for a given user
	 *
	 * @param $user_id          User ID
	 * @param $last_acked       Last message acked
	 * @return                  Array of record IDs
	 */

	public static function getActiveIds($user_id, $last_acked)
	{
		$sql = sprintf("select id from calllog where id > '%d' and user_id = '%d' and direction = 'T' and queued = 'y' order by created DESC", $last_acked, $user_id);


		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$out = array();
		while (($o = CDb::getRowArray($res)))
		{
			$out[] = $o[CCallLog::ID];
		}
		return $out;
	}
        public static function getActiveIdsPassenger($user_id, $last_acked)
	{
		$sql = sprintf("select id from calllog where id > '%d' and user_id = '%d' and direction = 'T' and queued = 'y' order by created DESC", $last_acked, $user_id);
//$sql = sprintf("select id from calllog  where user_id = '%d' and direction = 'T' and queued = 'y' order by created DESC", $user_id);
/*$file = fopen("/var/www/html/eco-gogo_valin/sample1.txt","w");
fwrite($file,$sql);
fclose($file);*/
		$res = CDb::query($sql);
		if ($res == false)
			return false;
		$out = array();
		while (($o = CDb::getRowArray($res)))
		{
			$out[] = $o[CCallLog::ID];
		}
		return $out;
	}
        	public static function getacceptDeclineStatus($journey_id)
	{
		$sql = sprintf("select text,journey_id from calllog where journey_id=$journey_id");


		$res = CDb::query($sql);
		if ($res == false)
			return false;
                unset($out);
		$out = array();
                $status=0;
		while (($o = CDb::getRowArray($res)))
		{
		    
                    $obj=CCommon::fromJson($o[CCallLog::TEXT]);
                   if($obj->driver_request_status==1)
                      $out['driver_request_status']=1; 
                   if($obj->passenger_request_status==1)
                       $out['passenger_request_status']=1; 
                   
		}
		return $out;
	}
        public static function getRideStartPassenger($journey_id)
	{
		$sql = sprintf("select text,journey_id from calllog where journey_id=$journey_id");


		$res = CDb::query($sql);
		if ($res == false)
			return false;
                unset($out);
		$out = array();
                $status=0;
                $out['driver_request_status']=0;
		while (($o = CDb::getRowArray($res)))
		{
		    
                    $obj=CCommon::fromJson($o[CCallLog::TEXT]);
                   if($obj->driver_request_status==1)
                      $out['driver_request_status']=1; 
                   
		}
		return $out;
	}
         public static function getRideStartDriver($journey_id)
	{
		$sql = sprintf("select text,journey_id from calllog where journey_id=$journey_id");


		$res = CDb::query($sql);
		if ($res == false)
			return false;
                unset($out);
		$out = array();
                $status=0;
                $out['passenger_request_status']=0; 
		while (($o = CDb::getRowArray($res)))
		{
		    
                    $obj=CCommon::fromJson($o[CCallLog::TEXT]);
                 
                   if($obj->passenger_request_status==1)
                   {
                       $out['passenger_request_status']=1; 
                       break;
                   }
                   
		}
		return $out;
	}
}
?>
