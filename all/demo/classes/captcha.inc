<?php
class CCaptcha
{
	/*
	 * Generate captcha information
	 */
	
	public static function generate ()
	{
		// 3D-Captcha
		//require_once("thirdparty/3dcaptcha/textgen.php");
		//require_once("thirdparty/3dcaptcha/3dcaptcha.php");
		
		// Securimage
		require_once('thirdparty/securimage/securimage.php');
		$img = new securimage();
		$gd_info = gd_info();
		if ($gd_info['FreeType Support'] == false)
			$img->use_gd_font = true;
		$img->wordlist_file = 'thirdparty/securimage/' . $img->wordlist_file;
		$img->gd_font_file = 'thirdparty/securimage/' . $img->gd_font_file;
		$img->ttf_file = 'thirdparty/securimage/' . $img->ttf_file;
		$img->show(); // alternate use:  $img->show('/path/to/background.jpg');
	}
	
	/*
	 * Check if captcha information matches
	 * @return true or false
	 */
	
	public static function check ($captchaText = '')
	{
		require_once('thirdparty/securimage/securimage.php');
		$img = new securimage();
		return $img->check($captchaText);
	}
}