<?php
require_once('wsbase.inc');
require_once('classes/smsws.inc');

/*
 * Incoming text Fields
 * (from http://www.textlocal.com/developers/docs )
 *
 * sender:
 * 		The mobile number of the handset.
 * content:
 * 		The message content.
 * inNumber:
 * 		The number the message was sent to (your inbound number).
 * email:
 * 		Any email address extracted.
 * credits:
 * 		The number of credits remaining on your Messenger account.
 */


$sender = $_REQUEST['sender'];
$content = $_REQUEST['content'];
$inNumber = $_REQUEST['inNumber'];
$email = $_REQUEST['email'];
$credits = $_REQUEST['credits'];

// The incoming numbers from textlocal arrive including country code.
// Leave the country code intact.
// In the unexpected case that a number comes in without country code,
// replace '07" with UK country code + 7 (which is the mobile prefix in the UK)
// e.g.  07966123456 -> 447966123456 
if (0 == strncmp($sender, "07", 2))
{
	$sender = "447" . substr($sender, 2);
}

CSmsWs::incomingText($sender, $content, "");
?>
