<?php
global $data;
$grid_columns = ( $data['grid_cols'] ) ? $data['grid_cols'] : 4;        
?>
                    <?php $posttags = get_the_tags(); ?>    
                    <div class="blogpost grid_posts columns-<?php echo $grid_columns; ?>">
                        <?php                       
                        if(has_post_thumbnail() || get_post_meta(get_the_ID(), 'pyre_youtube', true) || get_post_meta(get_the_ID(), 'pyre_vimeo', true)):
                        ?>                        
                            <div class="flexslider" data-interval="0" data-flex_fx="fade">
                                <ul class="slides">
                                    <?php if( get_post_meta($post->ID, 'pyre_youtube', true) != ''){ ?>
                                    <li class="video-container">                            
                                        <?php echo  do_shortcode('[youtube id="'.get_post_meta($post->ID, 'pyre_youtube', true).'"]'); ?>                               
                                    </li>
                                    <?php } ?>
                                    <?php if( get_post_meta($post->ID, 'pyre_vimeo', true) != ''){ ?>
                                    <li class="video-container">                        
                                        <?php echo  do_shortcode('[vimeo id="'.get_post_meta($post->ID, 'pyre_vimeo', true).'"]'); ?>
                                    </li>
                                    <?php } ?>
                                    
                                    
                                    <?php
                                    $extra ='';                                 
                                    $i = 2;
                                    while($i <= $data['featured_images_count']):
                                        $attachment = new StdClass();
                                        $attachment_id = kd_mfi_get_featured_image_id('featured-image-'.$i, 'post');
                                        if($attachment_id):                                     
                                            $attachment_image = wp_get_attachment_image_src($attachment_id, 'blog-large'); 
                                            $full_image = wp_get_attachment_image_src($attachment_id, 'full'); 
                                            $attachment_data = wp_get_attachment_metadata($attachment_id);  
                                            if($attachment_image && strpos($attachment_image[0], '600x400')) {                                    
                                                $extra .= '<li><a href="'.get_permalink().'"><img src="'.$attachment_image[0].'" alt="'.$attachment_data['image_meta']['title'].'" ></a></li>';  
                                            }                                          
                                            else {
                                                $extra .= '<li><a href="'.get_permalink().'"><img src="'.$full_image[0].'" alt="'.$attachment_data['image_meta']['title'].'" ></a></li>'; 
                                            }
                                        endif; 
                                        $i++; 
                                    endwhile; 
                                    
                                    ?>                                   
                                    
                                    <?php
                                    if(has_post_thumbnail()){ 
                                        $custom_thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'blog-large');
                                        $full_thumb = wp_get_attachment_image_src($post->ID,'full');  
                                        if($extra == '') {
                                    ?>                                 
                                            <li>
                                                <figure>
                                                    <a href="<?php the_permalink(); ?>">
                                                        <div class="text-overlay">
                                                            <div class="info">
                                                                <i class="fa fa-search"></i>                                                                
                                                            </div>
                                                        </div>
                                                        <?php                                                             
                                                            if($custom_thumb && strpos($custom_thumb[0], '600x400')) {
                                                                echo '<img src="'.$custom_thumb[0].'">';
                                                            }
                                                            else {
                                                                echo get_the_post_thumbnail($post->ID, 'full');
                                                            }   
                                                            ?>
                                                    </a>  
                                                </figure>                                      
                                            </li>
                                        <?php 
                                        }
                                        else{
                                            ?>
                                            <li>
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php
                                                    if($custom_thumb && strpos($custom_thumb[0], '600x400')) {
                                                         echo '<img src="'.$custom_thumb[0].'">';
                                                    }
                                                    else {
                                                         echo get_the_post_thumbnail($post->ID, 'full');
                                                    }   
                                                    ?>
                                                </a>
                                            </li>
                                            <?php                                           
                                            echo $extra; 
                                        }
                                        ?>
                                    <?php
                                    }
                                    ?>

                                </ul>
                                <div class="clear"></div>
                            </div>                           
                            
                        <?php                        
                        endif;
                        ?>                        
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <?php 
                        if($data['show_date'] || $data['show_author'] || $data['show_categories'] || $data['show_comments']){
                        ?>
                            <ul class="post_meta">
                                <?php if($data['show_author']){ ?>
                                    <li><i class="fa fa-user"></i><?php _e('by ','Creativo'); the_author_posts_link(); ?></li>
                                <?php } ?> 
                                
                                <?php if($data['show_date']){ ?>
                                    <li><i class="fa fa-clock-o"></i><?php the_time( get_option('date_format') ); ?></li>
                                <?php } ?>
                                
                                <?php if($data['show_categories']){ ?>   
                                    <li><i class="fa fa-bookmark"></i><?php the_category(', '); ?></li>
                                <?php } ?>
                                
                                <?php if($data['show_tags']){ ?>    
                                    <li><i class="fa fa-tag"></i><?php echo count($posttags); ?></li>
                                <?php } ?> 
                                
                                <?php if($data['show_comments']){ ?>    
                                    <li><i class="fa fa-comment"></i><?php comments_popup_link('0', '1', '%'); _e(" Comments" , "Creativo"); ?></li>
                                <?php } ?> 
                            </ul>                                           
                        <?php 
                        }
                        ?>
                        <div class="post-content">
                            <?php 
                            if($data['post_content']!='Full Content') { 
                                 echo string_limit_words(get_the_excerpt(), ($data['post_excerpt_length']) ? $data['post_excerpt_length'] : 30  ).'...'; 
                            }
                            else{
                                the_content();
                            }
                            ?>
                        </div>
                        
                        <?php 
                        if ( $data['show_view_more'] ){
                        ?>
                            <div class="post-atts archive">                                
                                <?php if($data['show_view_more']){ ?>
                                    <a href="<?php the_permalink(); ?>" class="button small button_default "><?php _e('Continue Reading &rarr;', 'Creativo'); ?></a>
                                <?php } ?>   
                            </div>
                        <?php } ?>               
                        
                    </div>                      