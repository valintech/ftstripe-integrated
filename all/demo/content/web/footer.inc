<section class="msgbox-1 grey">
    <div class="container">
        <div class="row">
            <!--#rplc1211#-->
                <div class="col-sm-8">
                    <blockquote><h2>Firsthumb Mobile Apps</h2><p>Firsthumb provides a Mobile App solution to enable car drivers and passengers to securely connect together to share lifts or rides when they need to.</p></blockquote>
                </div>
                <div class="col-sm-4">
                    <br><br>
              <a href="http://itunes.apple.com/gb/app/first-thumb/id481971973?mt=8"><img src="assets/images/ios_app.png" width="153" height="52"></a><a href="https://firsthumb.com/main/content/web/androidapp/FirstThumb-debug.apk"><img src="assets/images/andriod_app.png" width="153" height="52"></a>
                 <br><br><br>
                </div>
        </div>
    </div>
</section>

<footer class="footer-1">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
			
                <p>Copyright © 2015 Firsthumb &nbsp;&nbsp; &nbsp; </p>
				
            </div>
			<div class="col-md-8 col-sm-6">
			<div class="footer-link">
			<a href="terms_and_conditions.htm" class="foot">Terms & Conditions</a>&nbsp;  &nbsp; <a href="privacy.htm" class="foot">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://icebergng.com" target="_blank" class="foot">An Iceberg Infotainment Design</a></div>
			</div>
        </div>
    </div>
</footer>

<script src="content/web/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<div class="mobirise_engine" style="position: absolute; left: -1000px; top: -1000px; opacity: 0.1;"></div>
<script src="content/web/assets/jquery-parallax/jquery.parallax.js" type="text/javascript"></script>
<script src="content/web/assets/script.js" type="text/javascript"></script>

</body>
</html>
