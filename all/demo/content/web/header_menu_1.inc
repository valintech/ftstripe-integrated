<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9"><![endif]-->
<!--[if IE 8]><html class="preIE9"><![endif]-->
<!--[if gte IE 9]><!--><html><!--<![endif]-->
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
  <link href="http://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese" rel="stylesheet" type="text/css">
  <title>Firsthumb - Driving People Together!<!--#rplc11#--> @ <!--#rplc8#--> UK</title>
    <meta name="keywords" content="lift share, carpools, rideshare, ridesharing, carpool, carpooling, carpool information, share a ride, ride board, ride share, road trip, cross country, commute, carsharing, carshare, car share, search, find, information, long distance, together, map, ride, rider, car pool, car rental, travel, commuter share, vanpool, car pooling, driver, liftshare, lift">
    <meta name="description" content="Lift sharing, carpool and ride sharing site, worldwide coverage.">
  <link rel="stylesheet" href="content/web/assets/bootstrap/css/bootstrap.min.css" type="text/css">
  <link rel="stylesheet" href="assets/style.css" type="text/css">
<script src="content/web/assets/jquery/jquery.min.js" type="text/javascript"></script>
  <!--#rplc4#-->
  <!--External scripts goes here-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>
<body class="yui-skin-sam" onload="<!--#rplc1111#-->" onmousemove=" <!--#rplc1155#-->" >
<!-- { inverse | static | collapsed | black | mbr-nav-collapse } -->
<section class="menu-1 mbr-fixed-top mbr-nav-collapse inverse static">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                    <div class="brand">
                        <a href="home.htm"><img src="assets/images/1st_logo.png" alt="" class="img-responsive"> </a>
                        <span class="mbr-nav-toggle"></span>
                    </div>
					
					<ul class="first-nav"><li>
                                <a href="register.htm">Register</a>
                            </li>
                          <li>
                                <a href="search.htm">Search</a>
                            </li>
							<li > 
                                <a href="about_us.htm">About Us</a>
                </li>
							</ul>
							
                <nav>
                    
<ul class="nav nav-pills pull-right">
                             <li>
                                <a href="safety.htm">Safety</a>
                            </li> 
                             <li> <!-- class="active"  -->
                                <a href="login.htm">Log inn</a>
                            </li>
                              
                            <li>
                                <a href="newjourney.php">New Journey</a>
                            </li>
                              <li>
                                <a href="myjourneys.php">My Journeys</a>
                            </li>
                              <li>
                                <a href="index.php">Buy Miles</a>
                            </li>
                             <li>
                                <a href="myhistory.htm">My History</a>
                            </li>
                             
                          <!--  <li>
                                <a href="safety.htm">Safety</a>
                            </li>  -->
                           
                          
                           
                           
                          
                    </ul>
                </nav>
				
            </div>
        </div>
    </div>
</section>
