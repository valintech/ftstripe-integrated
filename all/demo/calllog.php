<?php
require_once('base.inc');
require_once('classes/common.inc');
require_once('classes/calllog.inc');
require_once('classes/journey.inc');

function htmlify ($text)
{
  /* There must be a better way than this */
  $text = ereg_replace ('&', '&amp;', $text);
  $text = ereg_replace ('<', '&lt;', $text);
  $text = ereg_replace ('>', '&gt;', $text);
  return $text;
}

printf("<HTML><HEAD></HEAD><BODY>\n");

printf("<h2>Call log:</h2>\n");
{
	$ids = CCallLog::getLatestIds(180);

	printf("<TABLE BORDER=\"1\">\n");
	printf("<tr><td>ID</td><td>UserID</td><td>User</td><td>Direction</td><td>Time</td><td>Text</td></tr>\n");
	$n = count($ids);
	for ($i = $n; $i; $i--) {
		$log = new CCallLog();
		$log->load($ids[$i-1]);
		printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n",
				$log->get('id'), $log->get('user_id'), $log->get('mobile'), $log->get('direction'),
				strftime("%F %T", $log->get('created')), htmlify($log->get(CCallLog::TEXT)));
	}
	printf("</TABLE>\n");
}

printf("<h2>Journey log:</h2>\n");
{
	$journeys = CJourney::getRecentJourneysAllUsers(30);

	printf("<TABLE BORDER=\"1\">\n");
	printf("<tr><td>ID</td><td>Driver</td><td>Passenger</td><td>Start</td><td>End</td><td>Miles</td><td>Status</td></tr>\n");
	foreach(array_reverse($journeys) as $journey) {
		printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n",
				$journey['id'],
				$journey['driver_first_name'].' '.$journey['driver_last_name'].' ('.$journey['driver_id'].')',
				$journey['passenger_first_name'].' '.$journey['passenger_last_name'].' ('.$journey['passenger_id'].')',
				strftime("%F %T", $journey['starttime']),
				$journey['endtime'] == null ? '' : strftime("%F %T", $journey['endtime']),
				$journey['miles'], $journey['status']);
	}
	printf("</TABLE>\n");
}

printf("<h2>Users:</h2>\n");
{
	$ids = CUser::getAllIds();

	printf("<TABLE BORDER=\"1\">\n");
	printf("<tr><td>ID</td><td>email</td><td>AppUser</td><td>Mobile</td><td>Car Reg</td><td>Credit</td></tr>\n");
	foreach ($ids as $id) {
		$user = new CUser();
		$user->loadWithUserId($id);
		printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%d</td></tr>\n",
				$user->get('id'), $user->get('email'), $user->get('appuser'),
				$user->get('mobile'), $user->get('carreg'), $user->get('credit'));
	}
	printf("</TABLE>\n");
}

printf("</BODY></HTML>\n");

?>
