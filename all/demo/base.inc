<?php
$tmp = pathinfo(__FILE__);
set_include_path(get_include_path().PATH_SEPARATOR.$tmp["dirname"].PATH_SEPARATOR.'thirdparty/ZendFramework-1.10.5/library/');

require_once("includes/config.inc");
require_once("classes/logging.inc");	
require_once("classes/login.inc");
require_once("classes/websession.inc");
require_once(CConfig::DB_INCLUDE);

CDb::setDb(CConfig::DB_TYPE, CConfig::DB_HOST, CConfig::DB_NAME, CConfig::DB_USER, CConfig::DB_PWD);

/*
 * Returns a compacted version number:-
 * Version 4.1.2 = (4*100) + (1*10) + 2 = 412
 * Version 5.1.2 = (5*100) + (1*10) + 2 = 512
 */

function phpVer()
{
	$aryTemp = explode( '.', PHP_VERSION );
	return ( (int)$aryTemp[0] * 100 ) + ( (int)$aryTemp[1] * 10 ) + (int)$aryTemp[2];
}

if (phpVer() >= 430)
	setlocale(LC_ALL, CConfig::LOCALE);
else
	setlocale('LC_ALL', CConfig::LOCALE);
	
// Set internal and external encoding to UTF8
mb_internal_encoding('utf-8');
mb_http_output('utf-8');
ob_start('mb_output_handler');
?>
