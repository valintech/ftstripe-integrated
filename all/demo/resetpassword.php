<?php
require_once('base.inc');
require_once('classes/region.inc');

// Validate any current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();

// Get posted data
$v = CCommon::getRequestValue('v');
$v = CCommon::fromJson(base64_decode($v));
$userId = $v->userId;
$userKey = $v->userKey;
$region = new CRegion('resetpassword');
$rplc = array();
$rplc[1] = ($userId ? resetPassword($userId, $userKey, $region) : $region->msg(1010));
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1100);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace('resetpassword.html', $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
	@include 'google_analytics.html';

/*
 * Reset password
 * 
 * @param $userId      User id.
 * @param $userKey     User verification key
 * @param $region      A CRegion object
 * @return             Confirmation text
 */

function resetPassword ($userId, $userKey, $region)
{
	// Load the user details
	$user = new CUser;
	if ($user->loadWithUserId($userId) == false)
	{
		CLogging::error(sprintf('resetpassword.php - cannot load user %ld',
								$userId));
		return $region->msg(1000);
	}
		
	// Check if request keys match
	if ($userKey != $user->getUserKey())
	{
		CLogging::error(sprintf('resetpassword.php - key mismatch [%s][%s]',
								$userKey, $user->getUserKey()));
		return $region->msg(1001);
	}

	// Save the user
	$newPassword = CUser::generatePassword();
	$user->set(CUser::PASSWORD, CUser::makePassword($user->get(CUser::EMAIL), $newPassword));
	if ($user->save(array(CUser::PASSWORD)) == false)
	{
		CLogging::error(sprintf('resetpassword.php - cannot save user %ld',
								$user->get(CUser::ID)));
		return $region->msg(1000);
	}

	// Send mail
	passwordResetMail($user, $newPassword, $region);	
	
	return $region->msg(1005);
}

/*
 * Send reset confirmation mail
 * 
 * @param $toUser        A CUser object
 * @param $newPassword   The new password
 * @param $regio         A CRegion object
 */

function passwordResetMail ($toUser, $newPassword, $region)
{
	// Send message
	$fromUser = new CUser;
	$rplc = array();
	$rplc[1] = $newPassword;
	$rplc[2] = $toUser->get(CUser::EMAIL);
	$rplc[8] = $region->msg(8, 'common');
	$cfg = new stdClass;
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->to = $toUser->get(CUser::EMAIL);
	$cfg->toName = $toUser->getFullName();
	$cfg->subject = $region->msg(8, 'common') . ' ' . $region->msg(1007);
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('resetpasswordcompletemail.html'), $rplc);
	$cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile('resetpasswordcompletemail.txt'), $rplc, false);
	CCommon::sendMailForgot($cfg);
}
?>
