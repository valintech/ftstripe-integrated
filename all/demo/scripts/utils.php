<?php
/*
 * XmlHttpRequest utility functions
 */
require_once('../base.inc');
require_once('classes/common.inc');
require_once('classes/region.inc');

// Get posted data
$op = CCommon::getRequestValue("op");

// Return a date string
if ($op == "date")
{
	// Object contaiing year, month, day members
	$date = CCommon::getRequestValue("date");
	CLogging::info(sprintf('utils.php/date - %s ...', $date));
	$date = CCommon::fromJson($date);
	CLogging::info(sprintf('utils.php/date - %d-%d-%d (%s) ...', $date->year, $date->month, $date->day, gettype($date)));
	$t = mktime(0, 0, 0, $date->month, $date->day, $date->year);
	$out = array("result" => strftime("%x", $t));
	CLogging::info(sprintf('... returning %s', CCommon::toJson($out)));
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// Return a date string
if ($op == "time")
{
	// Object containing hour, minute, second members
	$region = new CRegion("common");
	$time = CCommon::fromJson(CCommon::getRequestValue("time"));
	$t = mktime($time->hour, $time->minute, $time->second);
	$out = array("result" => strftime($region->msg(25, 'common'), $t));
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}
