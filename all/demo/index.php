<?php

require_once('base.inc');
require_once('classes/user.inc');
require_once('classes/common.inc');
require_once('classes/journey.inc');
require_once('classes/region.inc');
require_once('classes/google.inc');
require_once('classes/yahoo.inc');
require_once('classes/inifile.inc');
require_once('classes/paypal.inc');
if (CConfig::RUN_IN_FB)
	require_once('classes/facebook.inc');

// Validate any current session

CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();

if (!$isValidSession) {
	header("Location: home.php");
	exit;
}

$credit = CCommon::getRequestValue(CUser::CREDIT);

// If we are using dumb buttons (i.e. debug), then have a clause
// for increasing balance:
if (CConfig::SITE_PAYMENT_FRAMEWORK === 'DumbButtons')
{
	if ($isValidSession && $credit != '') {
		$user = $login->getUser(true);
		if (
			$credit < 0 && 
			( ($user->get(CUser::CREDIT) + $credit)  <  CConfig::MIN_CREDIT_IN_MILES )
		)
		{
			$out = new stdClass;
			$out = CCommon::makeErrorObj(40, 'Refund failed: Insufficient Credit. ' .
			'A minimum balance of '. CConfig::MIN_CREDIT_IN_MILES . 
			' miles must be maintained.' );
			CCommon::xhrSend(CCommon::toJson($out));
			exit;
		}
		$user->set('credit', $user->get('credit') + $credit);
		$user->save();
		$user = $login->getUser(true);
		CWebSession::set('login', serialize($login));
		if ($credit > 0) {
			        header("Location: next_step.php");
		}
		else {
			$out = new stdClass;
			$out = CCommon::makeErrorObj(0, 'Job done!');
			CCommon::xhrSend(CCommon::toJson($out));
		}
		exit;
	}
}
else
{
	// If $credit is negative, then it's a refund request
	// e.g.: "Please let me cash in 50 miles from my eco-gogo account"
	// Ignore requests for increasing balance, as they go
	// though paypal and ipn.php.
	if ($isValidSession && $credit != '' && $credit < 0) {	
		$user = $login->getUser(true);
		
		//If user is in the middle of a ride, then exit
		$journey = new CJourney();
		$journey->loadActiveByPassengerId($user->get(CUser::ID));
		if ($journey->get('id') != 0) {
			$out = new stdClass;
			$out = CCommon::makeErrorObj(50, 'Refund failed: Cannot recover credit during a ride. ');
			CCommon::xhrSend(CCommon::toJson($out));
			exit;
		}

		//If user doesn't have enough credit then exit.
		//In a request for 50 miles, $credit=='-50'
		if ( ($user->get(CUser::CREDIT) + $credit)  <  CConfig::MIN_CREDIT_IN_MILES ) {
			$out = new stdClass;
			$out = CCommon::makeErrorObj(40, 'Refund failed: Insufficient Credit. ' .
			'A minimum balance of '. CConfig::MIN_CREDIT_IN_MILES . 
			' miles must be maintained.' );
			CCommon::xhrSend(CCommon::toJson($out));
			exit;
		}
	
		//Send refund request to paypal and decrement website credit
		//only if successful.
		//CPayPal::refund takes +ve args so invert $credit before passing.
		$user_paypal_email = $user->get(CUser::PAYPAL_EMAIL);
		if (NULL === $user_paypal_email)
		{
			// Resort to standard email in the hope that it works.
			$user_paypal_email = $user->get(CUser::EMAIL);
		}
		$refund_gbp = CPayPal::refund( (0-$credit), $user_paypal_email );
		if ( -1 != $refund_gbp )
		{
			//Decrease user's credit by the amount of the refund.
			//TODO - log these increases in the database.
			//They will need to be reconciled against the IPNs
			//and our paypal balance in future.
			$user->set(CUser::CREDIT, $user->get(CUser::CREDIT) + $credit);
			$user->save();
			
			$out = new stdClass;
			$out = CCommon::makeErrorObj(0, "Refund successful: Your paypal account: " . 
				$user_paypal_email . " has been credited £" .
				number_format($refund_gbp,2) . "\n");
			CCommon::xhrSend(CCommon::toJson($out));
			exit;
		}
		else
		{
			$out = new stdClass;
			$out = CCommon::makeErrorObj(60, "Refund failed: Paypal error.");
			CCommon::xhrSend(CCommon::toJson($out));
			exit;
		}
	
		
		//TODO - should we email them ourselves when the IPN comes in or?
		
		$user = $login->getUser(true);
		CWebSession::set('login', serialize($login));
		header("Location: next_step.php");
		exit;
	}	
}



if ($isValidSession)
	$login->getUser(true);

// Output HTML page
$region = new CRegion("index");
$rplc = array();
if ($isValidSession)
	$rplc[3] = sprintf('|%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
else
	$rplc[3] = sprintf('|&nbsp;<a href="javascript:signIn()">%s</a>', $region->msg(3, "common"));
if ($op == "signin" && $isValidSession == false)
	$rplc[2] = sprintf('<span class="error">%s</span>', $region->msg(1001));
//$rplc[4] = $region->msg(1002);
$rplc[5] = ($isValidSession ? sprintf("%d miles", $login->getUser()->get(CUser::CREDIT)) : "(please sign in)");
$rplc[4] = scriptLinks();
$rplc[7] = script();
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[1111] = $region->msg(1111);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace("index.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
	@include 'google_analytics.html';

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks ()
{
	$out = array();
	$out[] = CGoogle::scriptHtml();
	$out[] = CYahoo::scriptHtml(array('json', 'button', 'autocomplete', 'connection', 'container'));
	if (CConfig::RUN_IN_FB)
		$out[] = CFacebook::scriptHtml();
	$out[] = '<script src="js/common.js" type="text/javascript"></script>';
	$out[] = '<script src="js/xhr.js" type="text/javascript"></script>';
	$out[] = '<script src="js/xplatform.js" type="text/javascript"></script>';
	$out[] = '<script src="index.js" type="text/javascript"></script>';
	return join("\n", $out);
}

/*
 * Print <script> element to output
 * 
 * @return HTML stream
 */

function script ()
{
	$out = array();
	if (CConfig::RUN_IN_FB)
		$out[] = CFacebook::script();
	// Load any custom destinatons
	$ini = new CIniFile();
	$destinations = array();
	if ($ini->load(sprintf('%s/index.ini', CConfig::CONTENT_DIR)))
		$destinations = array_values($ini->getSection('teams'));
	//$out[] = sprintf("var _destinations='%s';", rawurlencode(CCommon::toJson($destinations))); /* Commented becase of printing a string*/
	return join('', $out);
}
?>
