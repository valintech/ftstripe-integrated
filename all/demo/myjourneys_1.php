<?php
require_once 'base.inc';
require_once 'classes/session.inc';
require_once 'classes/region.inc';
require_once 'classes/schedule.inc';
require_once 'classes/google.inc';
require_once 'classes/yahoo.inc';
require_once 'classes/php2js.inc';
if (CConfig::RUN_IN_FB)
	require_once 'classes/facebook.inc';

//
// Potential request values:-
// "op" - operation code for specific script functions
// "journeyId" - journey id. to delete / leave / etc
//

// Get current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->requireSession();
CWebSession::set('login', serialize($login));

// Get posted data
$op = CCommon::getRequestValue('op');

// XmlHttpRequest:- Find journeys
if ($op == 'search')
{
	if (($journeyIdList = CSchedule::getUserScheduleIds($login->userId())) != NULL)
		$journeyList1 = CSchedule::load($journeyIdList);
	//$journeyList = CSchedule::removeExpired($journeyList);
//	if (($journeyIdList = CScheduleRequest::getUserAccepted($login->userId())) != NULL)
//		$journeyList2 = CSchedule::load($journeyIdList);
//	if (($journeyIdList = CScheduleRequest::getUserPending($login->userId())) != NULL)
//		$journeyList3 = CSchedule::load($journeyIdList);
	$out = search($journeyList1, $journeyList2, $journeyList3);
	CCommon::xhrSend(CCommon::toJson($out));
	exit;
}

// Delete the specified journey
if ($op == 'deleteJourney')
{
	$journeyId = CCommon::getRequestValue('journeyId');
	CLogging::info(sprintf("Deleting journey %ld", $journeyId));
	$journeys = CSchedule::load($journeyId);
	if ($journeys[0]->get(CSchedule::USER_ID) == $login->userId())
	{
		// XXX Can we wrap these in a transaction?
		//CScheduleRequest::deleteByJourneyId($journeyId);
		//CScheduleUser::deleteByJourneyId($journeyId);
		CSchedule::delete($journeyId);
	}
	else
		CLogging::error(sprintf("User %d tried to delete journey %d, which they did not create", $login->userId(), $journeyId));
	// Redisplay the ride list
	header("Location: myjourneys.php");
	exit;
}

// Initialise if no opcode given
if ($op == '')
{
	// Output HTML page
	$region = new CRegion("myjourneys");
	$rplc = array();
	if ($isValidSession)
		$rplc[1] = sprintf('|%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
	else
		$rplc[1] = sprintf('|&nbsp;<a href="javascript:signIn()">%s</a>', $region->msg(3, "common"));
	$rplc[2] = script($login, $isValidSession);
	$rplc[4] = scriptLinks();
	$rplc[8] = $region->msg(8, 'common');
	$rplc[9] = $region->msg(9, 'common');
	$rplc[11] = $region->msg(1100);
	$rplc[30] = $region->msg(10, 'common');
	$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
	$rplc[32] = $region->msg(13, 'common');
	$rplc[33] = $region->msg(14, 'common');
	$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
	$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
	$out = CCommon::htmlReplace("myjourneys.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
	print($out);
	if (CConfig::RUN_IN_FB == 0)
		@include 'google_analytics.html';
}

/*
 * Print <script> element to output
 * 
 * @param $login            A CLogin object
 * @param $isValidSession   true if have current session otherwise false
 * @return                  HTML <script> stream
 */

function script ($login, $isValidSession)
{
	$php2Js = new Php2Js();
	$out = array();
	$out[] = '<script type="text/javascript">';
	$out = array_merge($out, CRoot::formatClassAsJs('CSchedule'));
	$out = array_merge($out, CRoot::formatClassAsJs('CConfig', array(CConfig::CONTENT_DIR)));
	require_once('classes/region.inc');
	$region = new CRegion('myjourneys');
	$php2Js->add('_msgList', $region->msgList());
	$user = $login->getUser(true);
	$php2Js->add('_isUserRegistered', ($user && $user->isRegistered() ? true : false));
	$php2Js->add('_isValidSession', ($isValidSession ? true : false));
	$out[] = $php2Js->generateJs(); 
	$out[] = '</script>';
	return join('', $out);
}

/*
 * Generate <script> links
 * 
 * @return HTML <script> links
 */

function scriptLinks ()
{
	$out = array();
	$out[] = CGoogle::scriptHtml();
	$out[] = CYahoo::scriptHtml(array('json', 'connection', 'container', 'menu', 'button'));
	$out[] = '<script type="text/javascript" src="js/common.js"></script>';
	$out[] = '<script type="text/javascript" src="js/xplatform.js"></script>';
	$out[] = '<script type="text/javascript" src="myjourneys.js"></script>';
	return join("\n", $out);
}

/*
 * Search for locations
 * 
 * @param $journeyList  List of CSchedule objects
 */

function search ($journeyList1, $journeyList2, $journeyList3)
{
	$region = new CRegion('myjourneys');	
	$out = array();
	$html = array();
	$index = 0;
	$html[] = '<table class="resultsTable" cellpadding="0" cellspacing="0">';

//	$html[] = sprintf('<tr><td><h3>%s</h3><br></td></tr>', 'Journeys for which you are the driver:');
	if ($journeyList1 != NULL && count($journeyList1))
	{
		$journeyInfo = CSchedule::scheduleInfo($journeyList1);
		foreach ($journeyList1 as $journey)
		{
			$html[] = journeyHtml($index++, $journey, $region, $journeyInfo[$journey->get(CSchedule::ID)], 'driver');
			$html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>',
								  CConfig::CONTENT_DIR);
			$out['journeys'][] = $journey;
		}
	}
	else
		$html[] = sprintf('<tr><td><span class="resultsDimmed">%s</span><br></td></tr>', $region->msg(1011));

//	$html[] = sprintf('<tr><td><br><h3>%s</h3><br></td></tr>', 'Journeys for which you are a passenger:');
//	if ($journeyList2 != NULL && count($journeyList2))
//	{
//		$journeyInfo = CSchedule::scheduleInfo($journeyList2);
//		foreach ($journeyList2 as $journey)
//		{
//			$html[] = journeyHtml($index++, $journey, $region, $journeyInfo[$journey->get(CSchedule::ID)], 'passenger');
//			$html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>',
//								  CConfig::CONTENT_DIR);
//			$out['journeys'][] = $journey;
//		}
//	}
//	else
//		$html[] = sprintf('<tr><td><span class="resultsDimmed">%s</span><br></td></tr>', $region->msg(1011));
//
//	$html[] = sprintf('<tr><td><br><h3>%s</h3><br></td></tr>', 'Journeys for which you have a pending request');
//	if ($journeyList3 != NULL && count($journeyList3))
//	{
//		$journeyInfo = CSchedule::scheduleInfo($journeyList3);
//		foreach ($journeyList3 as $journey)
//		{
//			$html[] = journeyHtml($index++, $journey, $region, $journeyInfo[$journey->get(CSchedule::ID)], 'pending');
//			$html[] = sprintf('<tr><td background="%s/images/horizontal_dot.gif" scope="col">&nbsp;</td></tr>',
//								  CConfig::CONTENT_DIR);
//			$out['journeys'][] = $journey;
//		}
//	}
//	else
//		$html[] = sprintf('<tr><td><span class="resultsDimmed">%s</span><br></td></tr>', $region->msg(1011));

	$html[] = '</table>';
	$out['html'] = join('', $html);
	return $out;
}

/*
 * Create HTML representation of the journey
 * 
 * @param $index        Journey index
 * @param $journey      A CSchedule object
 * @param $region       A CRegion object
 * @param $journeyInfo  stdClass object containing extra journey
 *                      information "placesTaken", "placesAvailable",
 *                      "smoker", "femaleOnly"
 *                      members
 * @return              HTML
 */

function journeyHtml ($index, $journey, $region, $journeyInfo, $type)
{
	// Female only, smoker information
	$tmp = array();
	if ($journeyInfo->smoker)
		$tmp[] = $region->msg(1013);
	if ($journeyInfo->femaleOnly)	
		$tmp[] = $region->msg(1014);
	$hostPrefsHtml = (count($tmp) ? join(', ', $tmp) : '');

	$tmp2 = array();
	$tmp2[] = '<br/>I am ';
	if ($journey->get(CSchedule::REQUEST_TYPE) == CSchedule::REQUEST_TYPE_SEEKING) {
		$tmp2[] = 'seeking';
	} else if ($journey->get(CSchedule::REQUEST_TYPE) == CSchedule::REQUEST_TYPE_OFFERING) {
		$tmp2[] = 'offering';
	} else {
		$tmp2[] = 'offering to share';
	}
	$tmp2[] = ' a lift';
	$requestorHtml = join('', $tmp2);

	$html = array();
	$html[] = '<tr><td>';

	if ($journey->get(CSchedule::JOURNEY_TYPE) == CSchedule::JOURNEY_TYPE_ONEOFF)
	{
		$html[] = '<strong>';
		$html[] = $region->msg(1015);
		$html[] = '</strong>';
		$html[] = $requestorHtml;
		$html[] = '<span class="resultsDimmed">';
		$html[] = sprintf('<br/>On %s @ %s', strftime('%x', CCommon::tsToPhp($journey->get(CSchedule::ONEOFF_DATE))),
						  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CSchedule::ONEOFF_TIME))));
		if ($hostPrefsHtml != '')
			$html[] = '<br/>' . $hostPrefsHtml;
		$html[] = '</span><br/>';
	}
	else
	{
		$html[] = '<strong>';
		$html[]  = $region->msg(1016);
		$html[] = '</strong>';
		$html[] = $requestorHtml;
		$html[] = '<span class="resultsDimmed">';
		if ($journey->get(CSchedule::SCHEDULE_TYPE) == CSchedule::SCHEDULE_TYPE_STANDARD)
			$html[] = sprintf('<br/>%s %s %s - Start @ %s / Return @ %s',
							  $region->msg(17, 'common'), $region->msg(24, 'common'), $region->msg(21, 'common'),
							  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CSchedule::STANDARD_START_TIME))),
							  strftime($region->msg(25, 'common'), CCommon::tmToPhp($journey->get(CSchedule::STANDARD_RETURN_TIME))));
		elseif ($journey->get(CSchedule::SCHEDULE_TYPE) == CSchedule::SCHEDULE_TYPE_CUSTOM)
			$html[] = customTimesHtml($journey, $region);
		if ($hostPrefsHtml != '')
			$html[] = '<br/>' . $hostPrefsHtml;
		$html[] = '</span><br/>';
	}
	$html[] = sprintf('%s<br/><img align="absmiddle" src="%s/images/arrow.gif" width="12" height="12" align="absmiddle" />&nbsp;%s<br/>',
					  $journey->get(CSchedule::START_ADDRESS),
					  CConfig::CONTENT_DIR,
					  $journey->get(CSchedule::DESTINATION_ADDRESS));
	$html[] = '</td></tr>';
					  
	$html[] = '<tr><td align="right">';
	$tmp = array();
//	if ($type == 'driver')
		$tmp[] = sprintf('<a href="javascript:_deleteJourney(%d)">%s</a>', $index, $region->msg(1017));
	$tmp[] = sprintf('<a href="javascript:_searchJourney(%d)">%s</a>', $index, $region->msg(1018));
	$html[] = join('<span class="resultsOptions">&nbsp;|&nbsp;</span>', $tmp);
	$html[] = '</td></tr>';
	
	return join('', $html);	
}

/*
 * Create HTML for custom journey times
 * 
 * @param $journey    A CSchedule object
 * @param $region     A CRegion object
 * @return            HTML
 */

function customTimesHtml ($journey, $region)
{
	$dayMsgs = array(17, 18, 19, 20, 21, 22, 23);
	$times = array(
		array(CSchedule::MON_START_TIME, CSchedule::MON_RETURN_TIME),
		array(CSchedule::TUE_START_TIME, CSchedule::TUE_RETURN_TIME),
		array(CSchedule::WED_START_TIME, CSchedule::WED_RETURN_TIME),
		array(CSchedule::THU_START_TIME, CSchedule::THU_RETURN_TIME),
		array(CSchedule::FRI_START_TIME, CSchedule::FRI_RETURN_TIME),
		array(CSchedule::SAT_START_TIME, CSchedule::SAT_RETURN_TIME),
		array(CSchedule::SUN_START_TIME, CSchedule::SUN_RETURN_TIME));
	$out = array();
	foreach ($times as $k => $time)
	{
		$start = $journey->get($time[0]);
		$end = $journey->get($time[1]);
		if ($start == '' || $end == '')
			continue;
		$out[] = sprintf('%s - Start @ %s / Return @ %s',
			$region->msg($dayMsgs[$k], 'common'),
			strftime($region->msg(25, 'common'), CCommon::tmToPhp($start)),
			strftime($region->msg(25, 'common'), CCommon::tmToPhp($end)));
	}
	$out = join('<br/>', $out);
	return $out != '' ? '<br/>'.$out : $out;
}
?>
