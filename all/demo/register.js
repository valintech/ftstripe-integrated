var _googleMap;						// A CGoogleMap object
var _isYuiLoaded = false;			// true if YUI modules loaded
var _places;						// YUI "Button" to specify carpool place count
var _passwordCheckState = false;

/*
 * "onload" event
 */

function onLoad()
{
    _googleMap = new CGoogleMap;
    YAHOO_Require(['json', 'container', 'dom', 'connection', 'button'], _yuiLoaded);
}

/*
 * YUI modules loaded
 */
function keyPressedFileRename(id)
{
    var a = DOM_getElem(id);
    var label_id = id + '_File';
    if (a.value == "")
    {
        DOM_getElem(label_id).innerHTML = "No file attached";
    }
    else
    {
        DOM_getElem(label_id).innerHTML = a.value;
    }
    uploadFile(id);
}
function _yuiLoaded()
{
    _isYuiLoaded = true;
    _user = COMMON.fromJson(_user);

    DOM_setAttribute('firstname', 'value', _user[CUser.FIRST_NAME]);
    DOM_setAttribute('surname', 'value', _user[CUser.LAST_NAME]);
    DOM_setAttribute('address', 'value', _user[CUser.ADDRESS1]);
    DOM_setAttribute('address1', 'value', _user[CUser.ADDRESS2]);
    DOM_setAttribute('address2', 'value', _user[CUser.ADDRESS3]);
    DOM_setAttribute('postcode', 'value', _user[CUser.POSTCODE]);
    DOM_setAttribute('mobile', 'value', _user[CUser.MOBILE]);
    DOM_setAttribute('carreg', 'value', _user[CUser.CARREG]);
    DOM_setAttribute('email', 'value', _user[CUser.EMAIL]);
    DOM_setAttribute('retypeEmail', 'value', _user[CUser.EMAIL]);
    DOM_setAttribute('paypalEmail', 'value', _user[CUser.PAYPAL_EMAIL]);
    DOM_setAttribute('retypePaypalEmail', 'value', _user[CUser.PAYPAL_EMAIL]);
    DOM_setAttribute('phone_code', 'value', _user[CUser.PHONE_CODE]);
   // DOM_setAttribute('captcha_code', 'value', _user[CUser.CAPTCHA_CODE]); 
    DOM_setAttribute('crop_x', 'value', _user[CUser.CROP_X]);
    DOM_setAttribute('crop_y', 'value', _user[CUser.CROP_Y]);
    DOM_setAttribute('crop_w', 'value', _user[CUser.CROP_W]);
    DOM_setAttribute('crop_h', 'value', _user[CUser.CROP_H]);
    DOM_setAttribute('crop_img_original', 'value', _user[CUser.CROP_IMG_ORIGINAL]);
    

    DOM_setStyle('target', 'display', 'none');
    DOM_setStyle('profile_photo_id', 'display', 'none');
    DOM_setStyle('profile_license', 'display', 'none');
    DOM_setAttribute('driving_license_number', 'value', _user[CUser.DRIVING_LICENSE_NUMBER]);
   
    if (_user[CUser.SEX] == "f")
    {
       // YAHOO.util.Dom.setAttribute("female", "checked", true);
        //YAHOO.util.Dom.setAttribute("male", "checked", false);
document.getElementById("female").checked = true;
document.getElementById("male").checked = false;
        }
    else
    {
document.getElementById("male").checked = true;
document.getElementById("female").checked = false;
       // YAHOO.util.Dom.setAttribute("male", "checked", true);
        //YAHOO.util.Dom.setAttribute("female", "checked", false);
        }
    if (_user[CUser.SMOKING_HABIT] == "y")
    {
        //YAHOO.util.Dom.setAttribute("smoker", "checked", true);
         //YAHOO.util.Dom.setAttribute("non_smoker", "checked", false);
document.getElementById("smoker").checked = true;
document.getElementById("non_smoker").checked = false;
        }
    else
    {
document.getElementById("non_smoker").checked = true;
document.getElementById("smoker").checked = false;
       // YAHOO.util.Dom.setAttribute("non_smoker", "checked", true);
       // YAHOO.util.Dom.setAttribute("smoker", "checked", false);
        }



    if (_user[CUser.DRIVER] == 1)
    {

        YAHOO.util.Dom.setAttribute("driver", "checked", true);
        DOM_setStyle('driver_details', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('driver_details_insurance', 'display', CXPlat.tableDisplayAttr());

    }
    if (_user[CUser.PASSENGER] == 1)
    {
        YAHOO.util.Dom.setAttribute("passenger", "checked", true);
        DOM_setStyle('passenger_details', 'display', CXPlat.tableDisplayAttr());
         DOM_setStyle('birth_details', 'display', CXPlat.tableDisplayAttr());
    }


    if (typeof _user[CUser.DRIVING_LICENSE_IMAGE] != 'undefined' && _user[CUser.DRIVING_LICENSE_IMAGE].length != 0) {

        DOM_setStyle('profile_license', 'display', 'block');
        DOM_getElem('driving_license_image_File').innerHTML = 'upload/' + _user[CUser.ID] + '/' + _user[CUser.DRIVING_LICENSE_IMAGE].active;
        YAHOO.util.Dom.setAttribute("driving_license_image", "value", 'upload/' + _user[CUser.ID] + '/' + _user[CUser.DRIVING_LICENSE_IMAGE].active);
        YAHOO.util.Dom.setAttribute("driving_license_image_hidden", "value", 1);
        var ext = (_user[CUser.DRIVING_LICENSE_IMAGE].active).split('.').pop();
        if (ext == "pdf" || ext == "PDF" || ext == "Pdf")
            YAHOO.util.Dom.setAttribute("profile_license", "src", 'upload/pdf.jpg');
        else
            YAHOO.util.Dom.setAttribute("profile_license", "src", 'upload/' + _user[CUser.ID] + '/' + _user[CUser.DRIVING_LICENSE_IMAGE].active);
    }

        if (typeof _user[CUser.PHOTO_ID] != 'undefined' && _user[CUser.PHOTO_ID].length != 0) {

            DOM_setStyle('profile_photo_id', 'display', 'block');
            YAHOO.util.Dom.setAttribute("photo_id_hidden", "value", 1);
            DOM_getElem('photo_id_File').innerHTML = 'upload/' + _user[CUser.ID] + '/' + _user[CUser.PHOTO_ID];
            YAHOO.util.Dom.setAttribute("photo_id", "value", 'upload/' + _user[CUser.ID] + '/' + _user[CUser.PHOTO_ID]);
            var ext = _user[CUser.PHOTO_ID].split('.').pop();
            if (ext == "pdf" || ext == "PDF" || ext == "Pdf")
                YAHOO.util.Dom.setAttribute("profile_photo_id", "src", 'upload/pdf.jpg');
            else
                YAHOO.util.Dom.setAttribute("profile_photo_id", "src", 'upload/' + _user[CUser.ID] + '/' + _user[CUser.PHOTO_ID]);
      
        // YAHOO.util.Dom.setAttribute('id_type_p', 'checked', false);
        YAHOO.util.Dom.setAttribute('id_type_b', 'checked', true);
        }
       
      
    if (typeof _user[CUser.PHOTO] != 'undefined' && _user[CUser.PHOTO].length != 0) {
        DOM_setStyle('target', 'display', 'block');
        YAHOO.util.Dom.setAttribute("photo_hidden", "value", 1);
        DOM_getElem('photo_File').innerHTML = 'upload/' + _user[CUser.ID] + '/' + _user[CUser.PHOTO];
        YAHOO.util.Dom.setAttribute("photo", "value", 'upload/' + _user[CUser.ID] + '/' + _user[CUser.PHOTO]);
        var ext = _user[CUser.PHOTO].split('.').pop();
        if (ext == "pdf" || ext == "PDF" || ext == "Pdf")
            {
            YAHOO.util.Dom.setAttribute("target", "src", 'upload/pdf.jpg');
            }
        else
            {
             res_filename = _user[CUser.PHOTO].replace(_user[CUser.ID]+'_',_user[CUser.ID]+'_'+'200');
          YAHOO.util.Dom.setAttribute("target", "src", 'upload/' + _user[CUser.ID] + '/' + _user[CUser.PHOTO]);
YAHOO.util.Dom.setAttribute("target", "width", '200');
            }
    }

    if (typeof _user[CUser.SOCIAL_MEDIA_IDS] != 'undefined') {
        DOM_setAttribute('facebook_id', 'value', _user[CUser.SOCIAL_MEDIA_IDS].facebook);
        DOM_setAttribute('linked_id', 'value', _user[CUser.SOCIAL_MEDIA_IDS].linkedin);
        DOM_setAttribute('twitter_id', 'value', _user[CUser.SOCIAL_MEDIA_IDS].twitter);
    }

    if (_register) {
        DOM_setStyle('emailDetails', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('emailDivider', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('paypalEmailDetails', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('paypalEmailDivider', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('captchaDetails', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('tandcDetails', 'display', CXPlat.tableDisplayAttr());
        if (_passwordReq)
            DOM_setStyle('passwordDetails', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('phoneCode', 'display', 'none');
    }
    else {
        DOM_setStyle('emailCheckbox', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('paypalEmailCheckbox', 'display', CXPlat.tableDisplayAttr());
        DOM_setStyle('passwordCheckbox', 'display', CXPlat.tableDisplayAttr());
        if (_user[CUser.PHONE_CODE] != "")
            DOM_setStyle('phoneCode', 'display', CXPlat.tableDisplayAttr());
        else
            DOM_setStyle('phoneCode', 'display', 'none');
    }
     var groups_matched_table = DOM_getElem('matchedGroups');

    if (_groups != '0') {

        $('.SlectBox').SumoSelect();
        var i = 0;
        _selectedGroups = "0";
        var j = 0;
        $('select.SlectBox')[0].sumo.unload();
        $('.SlectBox').SumoSelect();



        if (typeof _groups.group_tag != 'undefined' && _groups.group_tag.length > 0) {
            
            while (i < _groups.group_tag.length) {
                if (_groups.type[i] === "d") {
                    $('select.SlectBox')[0].sumo.add(_groups.group_id[i], _groups.group_tag[i], j);
                    $('select.SlectBox')[0].sumo.selectItem(j);
                    _selectedGroups += "," + _groups.group_id[i];
                    j++;
                }

                if (_groups.type[i] === "n") {
                      $("#managegroup").css("display", "inline-block");
                    var rowCount = groups_matched_table.rows.length;
                    var row = groups_matched_table.insertRow(rowCount);
                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);

                    cell1.innerHTML = rowCount;
                    cell2.innerHTML = _groups.group_tag[i];
                    cell3.innerHTML = "<img src='content/web/images/close.gif' border='0' onclick=deleteGroup(" + (rowCount) + ")>";
                   // j++;
                }

                i++;
            }
            if (_remainingGroups != '0') {
                i = 0;
                _remainingGroups = JSON.parse(_remainingGroups);

                while (i < _remainingGroups.group_tag.length) {
                    $('select.SlectBox')[0].sumo.add(_remainingGroups.group_id[i], _remainingGroups.group_tag[i], i);
                    
                    i++;
                }
            }
            $('.options li').click(function() {
                var val = $(this).attr('data-val');
                if (!$(this).hasClass('selected'))
                {
                    _selectedGroups = _selectedGroups.replace("," + val, "");
                }
                else
                {
                    var i = _selectedGroups.indexOf("," + val);
                    if (i < 0)
                        _selectedGroups += "," + val;

                }


            });
        }

    }

    new YAHOO.widget.Button('save', {type: 'submit'});
    var btn = new YAHOO.widget.Button('quit', {type: 'push'});
    btn.on('click', function() {
        CCommon.redirect('index.php');
    });
    btn = new YAHOO.widget.Button('reqCode', {type: 'push'});
    btn.on('click', function() {
        requestPhoneCode();
    });

    DOM_setStyle('upload_progress_birth_certificate', 'display', 'none');
    DOM_setStyle('upload_progress_photo', 'display', 'none');
    DOM_setStyle('upload_progress_photo_id', 'display', 'none');
    DOM_setStyle('upload_progress_driving_license_image', 'display', 'none');
    DOM_setStyle('matchedGroups', 'display', 'none');
    var rowCount = groups_matched_table.rows.length;
    if (rowCount > 1)
        DOM_setStyle('matchedGroups', 'display', 'block');


}

function postcodeOnblur(e)
{
    function _response(response, status)
    {
        if (status != 'OK')
            return;

        var position = new google.maps.LatLng(response[0].geometry.location.lat(),
                response[0].geometry.location.lng())
        /*
         if (sourceMarker)
         {
         sourceMarker.setPosition(position);
         sourceMarker.setTitle(response[0].formatted_address);
         }
         else
         sourceMarker = new google.maps.Marker({position: position, map: map,
         draggable: true, title: response[0].formatted_address});
         */
    }

    var source = document.getElementById('postcode');
    geocoder.geocode({address: source.value, region: 'GB'}, _response);

}

/*
 * "onsubmit" event
 */

function onSubmit()
{

    var txt = validate();
    if (txt != '') {
        new YUI_DIALOG_Popup(txt, _msgList.msg(1012), 'block', _msgList.msg(1013));
        return false;
    }
    id = new Array();
    group = new Array();
    var groups_matched_table = DOM_getElem('matchedGroups');
    var rowCount = groups_matched_table.rows.length;
    for (var r = 1; r < rowCount; r++) {
        group.push(groups_matched_table.rows[r].cells[1].innerHTML);
    }

    CConnection.doPost('register.php', {op: 'register', mailStatus: _mailAllOrganizationUsers, newGroup: JSON.stringify(group), domainGroups: _selectedGroups, searchedGroups: _searchedSelectedGroup}, _response, null, 'registerForm');

    // XmlHttpRequest response handler
    function _response(o)
    {
        //alert(o['infoText']);
        if (o['errorText'])
            new YUI_DIALOG_Popup(o['errorText'], _msgList.msg(1012), 'block', _msgList.msg(1013));
        else
        {
            if (o['errorText'])
                new YUI_DIALOG_Popup(o['errorText'], _msgList.msg(1012), 'block', _msgList.msg(1013));
            else if (o['infoText'])
                new YUI_DIALOG_Popup(o['infoText'], _msgList.msg(1020), 'info', _msgList.msg(1013), _redirect);
            else{
                
                var url = _refer.getUrl();
                 var myDialog=new YUI_DIALOG_Popup("Profile updated successfully", "Message", 'block','Ok',function(){window.location.href = (url == '' ? 'register.php' : url);});
            }
               

                //_redirect();
        }

        function _redirect()
        {
             var url = _refer.getUrl();
//var myDialog=new YUI_DIALOG_Popup("Profile updated successfully", "Message", 'block','Ok',function(){window.location.href = (url == '' ? 'register.php' : url);});


             window.location.href = (url == '' ? 'register.php' : url);
        }
    }

    // Stop standard event behaviour
    return false;
}

function requestPhoneCode()
{
    CConnection.doPost('register.php', {op: 'request_phone_code'}, _response, null, 'registerForm');

    // XmlHttpRequest response handler
    function _response(o)
    {
        if (o['errorText'])
            new YUI_DIALOG_Popup(o['errorText'], _msgList.msg(1012), 'block', _msgList.msg(1013));
        else if (o['infoText'])
            new YUI_DIALOG_Popup(o['infoText'], _msgList.msg(1020), 'info', _msgList.msg(1013), _redirect);
        else
            _redirect();

        function _redirect()
        {
            var url = _refer.getUrl();
            window.location.href = (url == '' ? 'login.php' : url);
        }
    }

    // Stop standard event behaviour
    return false;
}
function send_co_report()
{
    CConnection.doPost('register.php', {op: 'send_co_savings'}, _response, null, 'registerForm');

    // XmlHttpRequest response handler
    function _response(o)
    {
      new YUI_DIALOG_Popup(o, _msgList.msg(22222), 'block', _msgList.msg(1013));
     
    }

    // Stop standard event behaviour
    return false;
}
/*
 * "onclick" event
 */

function onClick(e)
{
   
    if (_isYuiLoaded == false)
        return;
    var elem = CXPlat.eventSrcElement(e);
    _onClickCore(elem);
}

/*
 * Core for click function
 *
 * @param elem  Element id. or reference
 */

function _onClickCore(elem)
{

    if (typeof elem == 'string')
        elem = DOM_getElem(elem);
    if (elem.name == 'setPassword' && elem.disabled == false)
        DOM_setStyle('passwordDetails', 'display', (elem.checked == true ? CXPlat.tableDisplayAttr() : 'none'));
    else if (elem.name == 'setEmail')
    {
        // If you change email you must change password, because password encryption
        // seems to depend on email address.
        if (elem.checked == true)
        {
            DOM_setStyle('emailDetails', 'display', CXPlat.tableDisplayAttr());
            DOM_getElem('setPassword').disabled = true;
            _passwordCheckState = DOM_getElem('setPassword').checked;
            DOM_getElem('setPassword').checked = true;
            DOM_setStyle('passwordDetails', 'display', CXPlat.tableDisplayAttr());
        }
        else
        {
            DOM_setStyle('emailDetails', 'display', 'none');
            DOM_getElem('setPassword').checked = _passwordCheckState;
            DOM_setStyle('passwordDetails', 'display', (_passwordCheckState ? CXPlat.tableDisplayAttr() : 'none'));
            DOM_getElem('setPassword').disabled = false;
        }
    }
    else if (elem.name == 'setPaypalEmail')
    {
        // This doesn't require change of password as paypal email isn't involved
        // in password hashing.
        if (elem.checked == true)
        {
            DOM_setStyle('paypalEmailDetails', 'display', CXPlat.tableDisplayAttr());
        }
        else
        {
            DOM_setStyle('paypalEmailDetails', 'display', 'none');
        }
    }
   
    if (typeof _user[CUser.DRIVING_LICENSE_IMAGE] != 'undefined' && _user[CUser.DRIVING_LICENSE_IMAGE].length != 0) {
        DOM_setStyle('profile_license', 'display', 'block');
    }

    if (typeof _user[CUser.BIRTH_CERTIFICATE] != 'undefined' && _user[CUser.BIRTH_CERTIFICATE].length != 0) {

        DOM_setStyle('profile_birth_certificate', 'display', 'block');

        if (typeof _user[CUser.PHOTO_ID] != 'undefined' && _user[CUser.PHOTO_ID].length != 0) {

            DOM_setStyle('profile_photo_id', 'display', 'block');

        }

    }
    if (typeof _user[CUser.PHOTO] != 'undefined' && _user[CUser.PHOTO].length != 0) {
        DOM_setStyle('target', 'display', 'block');

    }

   if (elem.name == 'driver')
    {
        // This doesn't require change of password as paypal email isn't involved
        // in password hashing.
        if (elem.checked == true)
        {
            DOM_setStyle('driver_details', 'display', CXPlat.tableDisplayAttr());
            DOM_setStyle('driver_details_insurance', 'display', CXPlat.tableDisplayAttr());
        }
        else
        {
            DOM_setStyle('driver_details', 'display', 'none');
            DOM_setStyle('driver_details_insurance', 'display', 'none');
        }
    }
     if (elem.name == 'passenger')
    {
        // This doesn't require change of password as paypal email isn't involved
        // in password hashing.
        if (elem.checked == true)
        {
          DOM_setStyle('passenger_details', 'display', 'block');
            DOM_setStyle('birth_details', 'display', 'block');
        }
        else
        {
            DOM_setStyle('passenger_details', 'display', 'none');
            DOM_setStyle('birth_details', 'display', 'none');

        }
    }

   if($("#passenger").prop('checked') == true && $("#driver").prop('checked') == true){
       $("#driver_details").show();
       $("#birth_details").hide();
   
      }else if($("#passenger").prop('checked') == true){
            
            $("#driver_details").hide();
       $("#birth_details").show();
      }else if($("#driver").prop('checked') == true){
            
            $("#driver_details").show();
       $("#birth_details").hide();
      }


   /* if (elem.id == 'id_type_b') {

        DOM_setStyle('birth_details', 'display', CXPlat.tableDisplayAttr());

    }*/

    if (typeof _user[CUser.SOCIAL_MEDIA_IDS] != 'undefined') {
        DOM_setAttribute('facebook_id', 'value', _user[CUser.SOCIAL_MEDIA_IDS].facebook);
        DOM_setAttribute('linked_id', 'value', _user[CUser.SOCIAL_MEDIA_IDS].linkedin);
        DOM_setAttribute('twitter_id', 'value', _user[CUser.SOCIAL_MEDIA_IDS].twitter);
    }

}

/*
 * Validate form fields
 *
 * @return   true of false
 */

function validate()
{
    var checkElems = ['firstnameStatus', 'surnameStatus', 'addressStatus',
        'postcodeStatus', 'emailStatus', 'retypeEmailStatus',
        'PaypalEmailStatus', 'retypePaypalEmailStatus',
        'passwordStatus', 'retypePasswordStatus', 'mobileStatus', 'driving_l_number_Status', 'driving_l_pdf_Status',
        'photo_id_status','facebook_id_status', 'captchaStatus', 'photov_status', 'ERROR', 'facebooktocLabel', 'driver_passenger'];
    DOM_removeClass(checkElems, 'ERROR');
    var flag_validate = true;
    var errorElems = [];
    var errorText = [];
    if (DOM_getElem('firstname').value == '')
        errorElems.push('firstnameStatus');
    if (DOM_getElem('surname').value == '')
        errorElems.push('surnameStatus');
    if (DOM_getElem('address').value == '')
        errorElems.push('addressStatus');
    if (DOM_getElem('postcode').value == '')
        errorElems.push('postcodeStatus');
    if (DOM_getElem('photo_hidden').value != '1')
        errorElems.push('photov_status');



    if (_register || DOM_getElem('setEmail').checked)
    {
        var email1 = DOM_getElem('email').value;
        var email2 = DOM_getElem('retypeEmail').value;
        if (email1 == '')
            errorElems.push('emailStatus');
        if (email2 == '')
            errorElems.push('retypeEmailStatus');
        if (email1 != '' && email2 != '' && email1 != email2)
        {
            errorElems.push('emailStatus');
            errorElems.push('retypeEmailStatus');
            errorText.push(_msgList.msg(1014));
        }
    }
  //  alert(_register);
    
   // alert(DOM_getElem('setPaypalEmail'));
    if (_register || DOM_getElem('setPaypalEmail').checked)
    {
        var email1 = DOM_getElem('paypalEmail').value;
        var email2 = DOM_getElem('retypePaypalEmail').value;
        if (email1 == '')
            errorElems.push('paypalEmailStatus');
        if (email2 == '')
            errorElems.push('retypePaypalEmailStatus');
        if (email1 != '' && email2 != '' && email1 != email2)
        {
            errorElems.push('paypalEmailStatus');
            errorElems.push('retypePaypalEmailStatus');
            errorText.push(_msgList.msg(1014));
        }
    }

    if (DOM_getElem('mobile').value == '')
        errorElems.push('mobileStatus');
    
    if(DOM_getElem('driver').checked == true && DOM_getElem('passenger').checked == true ){
        if (YAHOO.lang.trim(DOM_getElem('carreg').value) == '')
            errorElems.push('driving_reg_Status');
        if (YAHOO.lang.trim(DOM_getElem('driving_license_number').value) == '')
            errorElems.push('driving_l_number_Status');
        if (DOM_getElem('driving_license_image_hidden').value != '1')
            errorElems.push('driving_l_pdf_Status');
        
    }else if (DOM_getElem('driver').checked == true)
    {

if (YAHOO.lang.trim(DOM_getElem('carreg').value) == '')
            errorElems.push('driving_reg_Status');
        if (YAHOO.lang.trim(DOM_getElem('driving_license_number').value) == '')
            errorElems.push('driving_l_number_Status');
        if (DOM_getElem('driving_license_image_hidden').value != '1')
            errorElems.push('driving_l_pdf_Status');
       

    }else if (DOM_getElem('passenger').checked == true)
    {
 if (DOM_getElem('photo_id_hidden').value != '1')
                errorElems.push('photo_id_status');
        
        /*if (DOM_getElem('id_type_b').checked == true)
        {
          
            if (DOM_getElem('photo_id_hidden').value != '1')
                errorElems.push('photo_id_status');
        }*/

    }
    DOM_getElem('facebooktocLabel').innerHTML = '';
    if (YAHOO.lang.trim(DOM_getElem('facebook_id').value) == '' && YAHOO.lang.trim(DOM_getElem('linked_id').value) == '' && YAHOO.lang.trim(DOM_getElem('twitter_id').value) == '')
    {
        DOM_getElem('facebooktocLabel').innerHTML = 'Please add atleast one social media id';
        errorElems.push('facebooktocLabel');
    }
    DOM_getElem('driver_passenger').innerHTML = '';
    if (DOM_getElem('driver').checked == false && DOM_getElem('passenger').checked == false)
    {
        DOM_getElem('driver_passenger').innerHTML = 'Please select atleast one checkbox';
        errorElems.push('driver_passenger');
    }

    if (_register && _passwordReq)
    {
        if (DOM_getElem('captcha').value == '')
            errorElems.push('captchaStatus');
    }
    if ((_register || DOM_getElem('setPassword').checked) && _passwordReq)
    {
        var pwd1 = DOM_getElem('password').value;
        var pwd2 = DOM_getElem('retypePassword').value;
        if (pwd1 == '')
            errorElems.push('passwordStatus');
        if (pwd2 == '')
            errorElems.push('retypePasswordStatus');
        if (pwd1 != '' && pwd2 != '' && pwd1 != pwd2)
        {
            errorElems.push('passwordStatus');
            errorElems.push('retypePasswordStatus');
            errorText.push(_msgList.msg(1026));
        }
        else if (pwd1 != '' && pwd1 == pwd2 && CCommon.checkPassword(pwd1) == false)
        {
            errorElems.push('passwordStatus');
            errorElems.push('retypePasswordStatus');
            errorText.push(_msgList.msg(1027));
        }
    }

    DOM_addClass(errorElems, 'ERROR');
    // Check if TOC checkbox is checked
    if (_register)
    {
        var toc = DOM_getElem('toc');
        COMMON.setError('tocLabel', '');
        if (toc.checked == false)
        {
            COMMON.setError('tocLabel', _msgList.msg(1017));
            errorElems.push('ERROR');
        }
    }

    if (errorElems.length)
        return(_msgList.msg(1024) + "\n" + errorText.join('\n'));
    else
        return('');


}

/*
 * Show Terms & Conditions popup
 */

function _toc()
{
    _commonDlg('tos');
}

/*
 * Shoe Privacy Policy popup
 */

function _pp()
{
    _commonDlg('pp');
}

function _commonDlg(op)
{
    CConnection.doGet('register.php', {op: op}, _response);

    function _response(o)
    {
        var cfg = {visible: false, draggable: true,
            close: false, zIndex: 9999,
            effect: {effect: YAHOO.widget.ContainerEffect.FADE, duration: 1}};
        var dlg = new YAHOO.widget.Panel('dlg', cfg);
        dlg.setHeader(o.header);
        dlg.setBody(o.body);
        dlg.render(document.body);
        dlg.center();
        var inp = DOM_getElem('closeBtn');
        var btn = new YAHOO.widget.Button('closeBtn', {label: inp.value});
        btn.on('click', function() {
            dlg.destroy();
        });
        dlg.show();
        CContainer.registerDlg(dlg);
    }
}

/*
 * To upload files
 * @param file input
 * 
 */
function uploadFile(fileInput)
{

    var _file = document.getElementById(fileInput);
    var filename = _file.value;
    var fullpath_file=filename.replace(/^.*[\\\/]/, '');
    if (_file.files.length === 0) {
        return;
    }

    var type = "", imageType = '', userColumn = '';
    switch (fileInput) {
        case "photo_id":
            type = "PhotoId";
            imageType = "profile_photo_id";
            _uploadStatusPhotoId = 1;
            userColumn = _sessionid + '_PhotoId.' + filename.split('.').pop();
           // userColumn = _sessionid + '_PhotoId_' + fullpath_file;
            YAHOO.util.Dom.setAttribute("photo_id_hidden", "value", 1);
            break;
        case "photo":
            type = "Photo";
            imageType = "target";
            _uploadStatusPhoto = 1;
            userColumn = _sessionid + '_Photo.' + filename.split('.').pop();
            //userColumn = _sessionid + '_Photo_' + fullpath_file;
            YAHOO.util.Dom.setAttribute("photo_hidden", "value", 1);
            break;
      
    
        case "driving_license_image":
            type = "DrivingLicense";
            imageType = "profile_license";
            _uploadStatusLicense = 1;
            userColumn = _sessionid + '_DrivingLicense_Active.' + filename.split('.').pop();
            //userColumn = _sessionid + '_DrivingLicense_Active_' + fullpath_file;
            YAHOO.util.Dom.setAttribute("driving_license_image_hidden", "value", 1);
            break;
    }
    DOM_setStyle(fileInput + '_div', 'display', 'none');
    DOM_setStyle('upload_progress_' + fileInput, 'display', 'block');

    var data = new FormData();
    data.append(type, _file.files[0]);
    DOM_getElem('save-button').disabled = true;
    try {

        upload(data, imageType, userColumn, fileInput, filename.split('.').pop());

    }
    catch (e) {

    }


    return false;
}
/*
 * To search matched groups for email domain and fill in the list
 * @param email id
 * 
 */
function searchDomain(val)
{
    var domain = val.split("@");
    if (val != '' && _emailExists != domain[1]) {
        _emailExists = domain[1];
        CConnection.doGet('register.php', {op: 'domain', value: domain[1]}, _response);
    }

    // XmlHttpRequest response handler
    function _response(o)
    {
        groups_matched = DOM_getElem('emailMatched');
        groups_matched.options.length = 0;
        addOptions(o);
    }

    // Stop standard event behaviour
    return false;
}

/*
 * To add new group to temporary group list
 * 
 */
function saveGroup()
{
     var organization = DOM_getElem('organization').value;
    var location = DOM_getElem('location').value;
    var group_tag = organization + '_' + location;
    var checkElemsGroups = ['organizationStatus', 'locationStatus'];
    DOM_removeClass(checkElemsGroups, 'ERROR');
    var flag_validate = true;
    var errorElemsGroups = [];
    var errorTextGroups = [];
    if (organization == '')
        errorElemsGroups.push('organizationStatus');
    if (location == '')
        errorElemsGroups.push('locationStatus');
    DOM_addClass(errorElemsGroups, 'ERROR');
   
    if (errorElemsGroups.length>0)
        {
        return false;
        }
    yuiConfirm(
            'Confirm Group name', 'Your group name is ' + group_tag,
            function() {
                CConnection.doPost('register.php', {op: 'validateGroup', group_tag: group_tag}, _response, null, 'registerForm');

                // XmlHttpRequest response handler
                function _response(o)
                {
                    if (o['infoText'] == 'Invalid or expired session specified') {
                        window.open('login.php', 'self');
                    }
                    else {
                    if (o['infoText'])
                        new YUI_DIALOG_Popup(o['infoText'], _msgList.msg(1034), 'info', _msgList.msg(1013));
                    else
                    {
                        DOM_setStyle('matchedGroups', 'display', 'block');
                        var groups_matched_table = DOM_getElem('matchedGroups');
                        var rowCount = groups_matched_table.rows.length;
                        var k = 1, flag = 0;
                        while (k < rowCount)
                        {
                            group_name = groups_matched_table.rows[k].cells[1].innerHTML;
                            if (group_name == group_tag)
                                flag = 1;
                            k++;
                        }
                        if (flag == 0) {
                            var row = groups_matched_table.insertRow(rowCount);

                            var cell1 = row.insertCell(0);
                            var cell2 = row.insertCell(1);
                            var cell3 = row.insertCell(2);

                            cell1.innerHTML = rowCount;
                            cell2.innerHTML = group_tag;
                            cell3.innerHTML = "<img src='content/web/images/close.gif' border='0' onclick=deleteGroup(" + (rowCount) + ")>";
                            yuiConfirm(
                                    _msgList.msg(1034),
                                    _msgList.msg(1033),
                                    function() {
                                        _mailAllOrganizationUsers = 1;

                                    }
                            );
                        }
                    }

                }
                }

            }

    );
}
/*
 * Delete button action for new groups list
 * @param row index
 */

function deleteGroup(rowindex)
{
    groups_matched_table = DOM_getElem('matchedGroups');
    yuiConfirm(
            'Confirm deletion', 'Do you want to delete this group ?',
            function() {
    groups_matched_table.deleteRow(rowindex);
            });
    var rowCount = groups_matched_table.rows.length;
    if (rowCount == 1)
        DOM_setStyle('matchedGroups', 'display', 'none');

}

function groupmanagement() {
    $("#managegroup").removeAttr("onclick");
    var x = document.getElementById("myGroups");
    if (x.length > 0) {
        x.remove(x.length - 1);
    }
    var y = document.getElementById("delmyGroups");
    if (y.length > 0) {
        y.remove(y.length - 1);
    }
    CConnection.doPost('register.php', {op: 'groupmanagement'}, _response, null, 'registerForm');
    function _response(o)
    {
        if (o['infoText'] == 'no_session') {
            document.location.href = 'login.php';

        }

        if (o['infoText'] == 'no_group') {
            $('#managegroup').prop('disabled', false);
            $("#manage_group_area").css("display", "none");
        } else {
        $("#myGroups").css("display", "block");
            $(".myGroupsRows").css("display", "block");
        var selectList = document.getElementById("myGroups");
        var option = document.createElement("option");
        option.value = 0;
        option.text = 'Select Group';
        selectList.appendChild(option);
        for (var i = 0; i < o.length; i++) {
            var option = document.createElement("option");
            option.value = o[i].id;
            option.text = o[i].group_tag;
            selectList.appendChild(option);
        }
            $("#group_delete").css("display", "block");

            var selectList = document.getElementById("delmyGroups");
            var option = document.createElement("option");
            option.value = 0;
            option.text = 'Select Group';
            selectList.appendChild(option);
            for (var i = 0; i < o.length; i++) {
                var option = document.createElement("option");
                option.value = o[i].id;
                option.text = o[i].group_tag;
                selectList.appendChild(option);
            }
        $('#managegroup').prop('disabled', true);
    }
}
}

function removeGroupMemebrs() {
    var searched_group_members = $("#searched_group_members").val();
    var selected_group_id = $("#myGroups option:selected").val();

    if (searched_group_members == '') {
        new YUI_DIALOG_Popup(_msgList.msg(1040), _msgList.msg(1038), 'info', _msgList.msg(1013));
        return false;
    }
    yuiConfirm(
            _msgList.msg(1038), _msgList.msg(1039),
            function() {
                CConnection.doPost('register.php', {op: 'deleteMemebers', searched_group_members: searched_group_members, selected_group_id: selected_group_id}, _response, null, 'registerForm');

                // XmlHttpRequest response handler
                function _response(o)
                {
                    if (o['infoText'] == 'no_session') {
                        document.location.href = 'login.php';

                    }
                   $("#search_group_member").tokenInput("clear");
                    if (o['infoText'])
                        new YUI_DIALOG_Popup(o['infoText'], _msgList.msg(1038), 'info', _msgList.msg(1013));

                    

                }

            }

    );
}


function delete_group() {
    var selected_group_id = $("#delmyGroups option:selected").val();
    if (selected_group_id == 0) {
        new YUI_DIALOG_Popup(_msgList.msg(1043), _msgList.msg(1038), 'info', _msgList.msg(1013));
        return false;
    }
    yuiConfirm(
            _msgList.msg(1038), _msgList.msg(1046),
            function() {
                CConnection.doPost('register.php', {op: 'deleteGroup', selected_group_id: selected_group_id}, _response, null, 'registerForm');

                // XmlHttpRequest response handler
                function _response(o)
                {
                    if (o['infoText'] == 'no_session') {
                        document.location.href = 'login.php';

                    }
                    document.location.reload();
                    if (o['infoText'])
                        new YUI_DIALOG_Popup(o['infoText'], _msgList.msg(1038), 'info', _msgList.msg(1013));

                }

            }

    );
}

function ownership_change() {

    var searched_group_member_for_admin = $("#search_group_memberforadmin").val();
    var selected_group_id = $("#myGroups option:selected").val();
    if (searched_group_member_for_admin == '') {
        new YUI_DIALOG_Popup(_msgList.msg(1040), _msgList.msg(1038), 'info', _msgList.msg(1013));
        return false;
    }
    yuiConfirm(
            _msgList.msg(1038), _msgList.msg(1041),
            function() {
                CConnection.doPost('register.php', {op: 'chanagegroupownership', searched_group_member_for_admin: searched_group_member_for_admin, selected_group_id: selected_group_id}, _response, null, 'registerForm');

                // XmlHttpRequest response handler
                function _response(o)
                {
                     $("#search_group_member").tokenInput("clear");
                    if (o['infoText'] == 'no_session') {
                        document.location.href = 'login.php';

                    }
                     $("#search_group_memberforadmin").tokenInput("clear");
                    if (o['infoText'])
                        new YUI_DIALOG_Popup(o['infoText'], _msgList.msg(1042), 'info', _msgList.msg(1013));
                        document.location.href = 'register.php';

                   


                }

            }

    );
}
