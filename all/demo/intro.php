<?php
require_once 'base.inc';
require_once 'classes/session.inc';
require_once 'classes/region.inc';

//
// Potential request values:-
// "op" - operation code for specific script functions
// "journeyId" - journey id. to delete / leave / etc
//

// Get current session
CWebSession::init();

$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();
CWebSession::set('login', serialize($login));

// Output HTML page
$region = new CRegion('intro');
$rplc = array();
if ($isValidSession)
	$rplc[1] = sprintf('|%s&nbsp;<a href="javascript:signOut()">%s</a>', $login->userEmail(), $region->msg(1, "common"));
else
	$rplc[1] = sprintf('|&nbsp;<a href="javascript:signIn()">%s</a>', $region->msg(3, "common"));
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1100);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace("intro.htm", $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);

?>
