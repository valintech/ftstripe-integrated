$(document).ready(function() {
       var url=document.getElementById('base_url').value;
       	jQuery("#users").flexigrid({
			url : url+'user_search_list.php',
			dataType : 'xml', 
			colModel : [  

                         {
				display : 'Sl No',
				name : 'id',
				width : 50,
				sortable : true,
				align : 'left'
			}
                        ,{
				display : 'First Name',
				name : 'first_name',
				width : 200,
				sortable : true,
				align : 'left'
			},{
				display : 'Last Name',
				name : 'last_name',
				width : 200,
				sortable : true,
				align : 'left'
			},{
				display : 'Email',
				name : 'email',
				width : 200,
				sortable : true,
				align : 'left'
			}, {
				display : 'Mobile',
				name : 'mobile',
				width : 100,
				sortable : true,
				align : 'left'
			}, {
				display : 'Balance',
				name : 'Balance',
				width : 100,
				sortable : false,
				align : 'left'
			}
                       ,{
				display : 'View',
				name : 'view',
				width : 100,
                                editable:true,
				
				align : 'left'
			}],
			
			
			sortname : "id",
			sortorder : "desc",
			usepager : true,
			title : "USER LIST",
			useRp : true,
			rp : 15,
			showTableToggleBtn :true,
			width : 700,
			height : 356
		});
});
function Freemiles()
{  $(".error").html('');
   var free_distance=0;
    if($('#free-distance').prop('checked')==true)
      free_distance=1;
      var free_miles=$('#free_miles').val();
     if($.isNumeric(free_miles)==false)
{
  $('#err_free_miles').html("Enter valid Miles");
         $('#free_miles').focus();  
}else{ 
$.ajax({
  method: "POST",
  url: "user_lists.php",
   dataType: 'json',
  data: { free_distance: free_distance, free_miles: free_miles, save_button: 1 }
})
  .done(function( msg ) {
    if(msg['id']==1)
{
       $('#success').html("<div id='msgdiv' style='font-weight: bold; color: red; '>Updated Successfully</div>");
       $('#free_miles').val(msg['free_miles']);
       if(msg['active']==1)
       $('.myCheckbox').prop('checked', true);
       if(msg['active']==0)
       $('.myCheckbox').prop('checked', false);
        setTimeout(function(){
                    $("#msgdiv").fadeOut("slow", function () {
                        $("#msgdiv").remove();
                    });
                }, 5000);
}
  });}
  }
function clearListFree()
{
 $('#free_miles').val("");

}
function clearList()
{
    $('#firstname').val("");
$('#email').val("");
  searchUser();  
}
function searchUser(){
   var url=document.getElementById('base_url').value;
  var name = document.getElementById('firstname').value.replace(/^\s+|\s+$/g, '') ;

   var email = document.getElementById('email').value.replace(/^\s+|\s+$/g, '');
   paramdata ='name='+name+'&email=' +email;
   if(paramdata!=0){
    jQuery('#users').flexOptions({url: url+'user_search_list.php?type=1&'+paramdata, newp: 1}).flexReload(); 
   }
   else{
       jQuery('#users').flexOptions({url:  url+'user_search_list.php?type=1', newp: 1}).flexReload(); 
   }
}
function enter_pressedoccupation(e){
 var characterCode; //literal character code will be stored in this variable
    if(e && e.which){ //if which property of event object is supported (NN4)
        e = e;
        characterCode = e.which; //character code is contained in NN4's which property
    }
    else{
        e = event;
        characterCode = e.keyCode; //character code is contained in IE's keyCode property
    }

    if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
       //producerSave();
 return true;

    }
    else{
       return false;
    }

}
function assignMiles(user_id,free)
 {
        $("body").append('<div id="popup_overlay" class="popup_overlayBG" onclick="popup_close()"></div>');
        document.getElementById('popup_overlay').style.display="block";
	
        //document.getElementById('freemilesd').innerHTML=free;
       paramdata = "user_id="+user_id+'&free='+free;
       
              $.ajax({
    type: "POST",
    url: 'load-modal.php',
    data:paramdata,
    success: function(msg){
      
       // document.getElementById('popup_overlay').style.display="block";
        $('#light').css({
                     top:	getPageScroll()[1] + (getPageHeight()[1] / 5),
                     left:	getPageScroll()[0] + (getPageHeight()[0] / 3)
                  }).fadeIn('slow');
        document.getElementById('light').innerHTML=msg;
	//document.getElementById('light').style.display="block";

        }
    });
     
 }
 function callUpdate()
{
   $(".error").html('');
 var assign_free_miles=$('#assign_free_miles').val();
var user_id=$('#user_id').val();
if($.isNumeric(assign_free_miles)==false)
{
  $('#err_assign_free_miles').html("Enter valid Miles");
         $('#assign_free_miles').focus();  
}
else{
 paramdata = "assign_free_miles="+assign_free_miles+'&user_id='+user_id+'&update_button=1';
   $.ajax({
    type: "POST",
    url: 'user_lists.php',
    data:paramdata,
     dataType: 'json',
    success: function(msg){
      
   if(msg=='0')
       var msg_box='Free Miles added successfully';
       else
          var msg_box='Free Miles added  failed';   
       
       
        $('#success-msg').html("<div id='msgdiv' style='font-weight: bold; color: red; '>"+msg_box+"</div>");

        setTimeout(function(){
                    $("#msgdiv").fadeOut("slow", function () {
                        $("#msgdiv").remove();
                    });
                }, 5000);
                searchUser();
        }
    });
}

 /*     var assign_free_miles=$('#assign_free_miles').val();//!assign_free_miles.match(alphaExp) || 
      var user_id=$('#user_id').val();
      if(assign_free_miles=="")
{
//document.getElementById('err_edit_name').innerHTML="Enter a valid Area Name";
     //    $('#edit_name').focus();
}
 else
    {
         document.getElementById('overlay').style.visibility='visible';
$.ajax({
  method: "POST",
  url: "user_lists.php",
   dataType: 'json',
  data: { user_id: user_id, assign_miles: free_miles, update_button: 1 }
})
  .done(function( msg ) {
    if(msg['id']==1)
{
       $('#success').html("<div id='msgdiv' style='font-weight: bold; color: red; '>Updated Successfully</div>");

        setTimeout(function(){
                    $("#msgdiv").fadeOut("slow", function () {
                        $("#msgdiv").remove();
                    });
                }, 5000);
                 document.getElementById('overlay').style.visibility='hidden';
}
  });
  }*/
}
