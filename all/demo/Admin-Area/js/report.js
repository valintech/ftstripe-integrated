$(document).ready(function() {
    $('#datePicker')
        .datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
        $('#datePickerend')
        .datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

   
});
function searchReport()
{
     $(".error").html('');
      errFlag=0;
        errFocus=0;
          var datefrom = document.getElementById('datefrom').value.replace(/^\s+|\s+$/g, '');
        var dateto = document.getElementById('dateto').value.replace(/^\s+|\s+$/g, '');
    if(datefrom=="")
    {
        document.getElementById('datefrom_err').innerHTML='Please select Start Date';
        errFlag=1;
        errFocus='datefrom';
    }

    if(dateto==0)
    {
        document.getElementById('dateto_err').innerHTML='Please select End Date';
        errFlag=1;
        if(errFocus==0) errFocus='dateto';
    }

                                                   var dt12  = parseInt(datefrom.substring(0,2),10);

                                                    var mon12 = parseInt(datefrom.substring(3,5),10);
                                                    var yr12  = parseInt(datefrom.substring(6,10),10);
                                                    var dt22  = parseInt(dateto.substring(0,2),10);
                                                    var mon22 = parseInt(dateto.substring(3,5),10);
                                                    var yr22  = parseInt(dateto.substring(6,10),10);
                                                    var date12 = new Date(yr12, mon12, dt12);
                                                    var date22 = new Date(yr22, mon22, dt22);

                                            if(dateto != "" && datefrom != "")
                                               {
                                                  if(Date.parse(date12) > Date.parse(date22))
                                                   {
                                                     $('#dateto_err').html("Must Be Greater Than Start Date");
                                                       errFlag=1;
                                                    if(errFocus==0) errFocus='dateto';
                                                  }



                                              }
                    if(errFlag==1)
    {
        focustag=document.getElementById(errFocus);
        focustag.focus();
        return false;
    }                           
 paramdata = "datefrom="+datefrom+'&dateto='+dateto+'&search=1';
   $.ajax({
    type: "POST",
    url: 'freemiles_report.php',
    data:paramdata,
     dataType: 'json',
    success: function(msg){
      
        $('#userslist').html("<div style='font-weight:bold;font-size:16px;'>Free Miles Statistics from '"+datefrom+"' To '"+dateto+"'</div><div id='msgdiv' style='font-weight: bold;'>Free Miles Used :  "+msg['used_free_miles']+"</div><div id='msgdiv' style='font-weight: bold;'>Free Miles Allocated :  "+msg['alotted_miles']+"</div><div id='msgdiv' style='font-weight: bold; color: red;font-size:16px;'>Total Unused Free Miles:  "+msg['balance_miles']+"  (as of now)</div>");

      
             
        }
    });
}
