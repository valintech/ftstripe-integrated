<?php

/*
 * Class handler for schedule related DB queries
 */

class CScheduleDb {

    // Class constants
    const MIN_DISTANCE = 10;
    // SQL statements relating to 'schedule' table
    const GEOSPATIAL_START_SQL = "SELECT schedule.groups_id as tagslist,3956 * 2 * asin( sqrt( power( sin( ( %s - abs(schedule.from_lat ) ) * pi() / 180 / 2 ), 2) + cos( %s * pi() / 180 ) * cos( abs( schedule.from_lat ) * pi() / 180 ) * power( sin( ( %s - schedule.from_lng ) * pi() / 180 / 2), 2) ) ) as distance, schedule.%s FROM schedule  where schedule.id!=0 ";
    const GEOSPATIAL_DEST_SQL = "SELECT  schedule.groups_id as tagslist,3956 * 2 * asin( sqrt( power( sin( ( abs( %s ) - abs( schedule.to_lat ) ) * pi() / 180 / 2 ), 2) + cos( abs( %s ) * pi() / 180 ) * cos( abs( schedule.to_lat ) * pi() / 180 ) * power( sin( ( %s - schedule.to_lng ) * pi() / 180 / 2), 2) ) ) as distance, schedule.%s FROM schedule where schedule.id!=0 ";
    const GEOSPATIAL_ADDENDUM_SQL = " AND schedule.%s in (%s)";
    const GEOSPATIAL_ADDENDUM_SQL_GROUP_LIST = " AND groups.id in (%s)";

    /* const GEOSPATIAL_START_SQL = "SELECT 3956 * 2 * asin( sqrt( power( sin( ( %s - abs(schedule.from_lat ) ) * pi() / 180 / 2 ), 2) + cos( %s * pi() / 180 ) * cos( abs( schedule.from_lat ) * pi() / 180 ) * power( sin( ( %s - schedule.from_lng ) * pi() / 180 / 2), 2) ) ) as distance, %s FROM schedule HAVING distance < %s";
      const GEOSPATIAL_DEST_SQL = "SELECT 3956 * 2 * asin( sqrt( power( sin( ( abs( %s ) - abs( schedule.to_lat ) ) * pi() / 180 / 2 ), 2) + cos( abs( %s ) * pi() / 180 ) * cos( abs( schedule.to_lat ) * pi() / 180 ) * power( sin( ( %s - schedule.to_lng ) * pi() / 180 / 2), 2) ) ) as distance, %s FROM schedule HAVING distance < %s";
      const GEOSPATIAL_ADDENDUM_SQL = " AND %s in (%s)";
     */
    const NEW_SQL = 'INSERT INTO schedule (%s) VALUES (%s)';
    const UPDATE_SQL = 'UPDATE schedule SET %s WHERE id=%s';
    const SELECT_SQL = 'SELECT %s FROM schedule WHERE %s';
    const SELECT_SQL_GROUP = 'SELECT %s FROM schedule';
    const DELETE_SQL = 'DELETE FROM schedule WHERE %s';

    /*
     * Return schedule destinations close to a location
     * 
     * @param $start  CLocation object for start
     * @param $end    CLocation object for end
     * @return        Array of matching CSchedule objects or false if
     *                error during retrieval
     */

    public static function distance($start, $end, $grouped_id, $userId) {

        $groups_id_lists = '0';
        $sql_group = "SELECT  GROUP_CONCAT(ug.group_id SEPARATOR ',') as groups_id FROM  user_group as ug where ug.user_id=".$userId;
        $res_group = CDb::query($sql_group);

        if ($res_group == false)
            return false;

        while (($o_group = CDb::getRowArray($res_group))) {
            $groups_id_lists.="," . $o_group['groups_id'];
        }
        $sqll = 'SELECT * FROM groups';
        $resl = CDb::query($sqll);
        if ($resl == false)
            return false;
        $outl = array();
        while (($ol = CDb::getRowArray($resl))) {

            $outl["'" . $ol['group_tag'] . "'"] = $ol['id'];
        }
        $scheduleIds = array();
        $scheduleIdslist = array();
        if ($start->lat != '' && $start->lng != '') {
            $sql = sprintf(CScheduleDb::GEOSPATIAL_START_SQL, $start->lat, $start->lat, $start->lng, CSchedule::ID, CScheduleDb::MIN_DISTANCE);
            /* $sql = sprintf(CScheduleDb::GEOSPATIAL_START_SQL, $start->lat, $start->lat,
              $start->lng, CSchedule::ID, CScheduleDb::MIN_DISTANCE); */
            if ($grouped_id != '') {
                $sql .= " and (FIND_IN_SET(" . str_replace(",", ", groups_id) OR FIND_IN_SET(", $grouped_id) . ",groups_id)) ";
            } else {

                $sql .= " and (FIND_IN_SET(" . str_replace(",", ", groups_id) OR FIND_IN_SET(", $groups_id_lists) . ",groups_id)) ";
            }
            //   $sql .= sprintf(CScheduleDb::GEOSPATIAL_ADDENDUM_SQL_GROUP_LIST,
            //	$grouped_id);
            $sql.=' HAVING distance <' . CScheduleDb::MIN_DISTANCE;
            $res = CDb::query($sql);
            if ($res == false)
                return false;
            while (($o = CDb::getRowArray($res))) {
                $scheduleIds[$o[CSchedule::ID]] = $o['distance'];
                $scheduleIdslist[$o[CSchedule::ID]] = $o['tagslist'];
            }
            if (count($scheduleIds) == 0)
                return array();
        }
        if ($end->lat != '' && $end->lng != '') {
            $sql = sprintf(CScheduleDb::GEOSPATIAL_DEST_SQL, $end->lat, $end->lat, $end->lng, CSchedule::ID); //, CScheduleDb::MIN_DISTANCE
            if (count($scheduleIds))
                $sql .= sprintf(CScheduleDb::GEOSPATIAL_ADDENDUM_SQL, CSchedule::ID, join(',', array_keys($scheduleIds)));
            if ($grouped_id != '') {
                $sql .= " and (FIND_IN_SET(" . str_replace(",", ", groups_id) OR FIND_IN_SET(", $grouped_id) . ",groups_id)) ";
            } else {

                $sql .= " and (FIND_IN_SET(" . str_replace(",", ", groups_id) OR FIND_IN_SET(", $groups_id_lists) . ",groups_id)) ";
            }
            // $sql .= sprintf(CScheduleDb::GEOSPATIAL_ADDENDUM_SQL_GROUP_LIST,
            //$grouped_id);
            $sql.=' HAVING distance <' . CScheduleDb::MIN_DISTANCE;
            $res = CDb::query($sql);
            if ($res == false)
                return false;
            $scheduleIds = array();
            $scheduleIdslist = array();
            while (($o = CDb::getRowArray($res))) {
                $scheduleIds[$o[CSchedule::ID]] = $o['distance'];
                $scheduleIdslist[$o[CSchedule::ID]] = $o['tagslist'];
            }
        }
        $out = (count($scheduleIds) ? CScheduleDb::load(array_keys($scheduleIds)) : array());
        foreach ($out as $schedule) {
            $schedule->set('distance', round($scheduleIds[$schedule->get(CSchedule::ID)], 2));
            $array2 = explode(",", $scheduleIdslist[$schedule->get(CSchedule::ID)]);
            //print_r($outl);print_r($array2);
            $result = array_intersect($outl, $array2);
            $group_tags = '';
            foreach ($result as $key => $value) {
                $group_tags.=$key . ',';
            }
            $schedule->set('group_tags', substr($group_tags, 0, -1));
        }
        return $out;
    }

    /*
     * Save the schedule
     * 
     * @param $schedule   A CSchedule object to save
     * @return           true or false
     */

    public static function save($schedule) {
        // Get list of columns. Include id. column if already exists
        $id = $schedule->get(CSchedule::ID);
        $cols = CRoot::getFilteredDbMembers('CSchedule', false, ($id ? array(CSchedule::ID) : false));

        // Turn data into SQL compatible strings
        $vals = array();
        foreach ($cols as $colId => $col)
            $vals[$colId] = CDb::makeSqlValue($schedule->get($colId), $col['type']);

        // Create SQL statement for insert or update
        $vals[CSchedule::UPDATED] = 'now()';
        $id = $schedule->get(CSchedule::ID);
        if ($id == 0) {
            $vals[CSchedule::CREATED] = 'now()';
            $sql = sprintf(self::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
        } else {
            unset($cols[CSchedule::CREATED]);
            unset($vals[CSchedule::CREATED]);
            $tmp = array();
            foreach ($cols as $colId => $col)
                $tmp[] = sprintf("%s=%s", $colId, $vals[$colId]);
            $sql = sprintf(self::UPDATE_SQL, join(",", $tmp), $id);
        }

        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
            if ($id == 0) {
                $id = CDb::getLastAutoId();
                $schedule->set(CSchedule::ID, $id);
            }
            CDb::commitTrans();
        } else
            CDb::rollbackTrans();

        return $res ? true : false;
    }

    /*
     * Load schedule details
     * 
     * @param $scheduleId  Schedule id. or array of ids. to load
     * @return            Array of CSchedule objects or false if failed
     */

    public static function load($scheduleId) {
        CLogging::info(sprintf("CScheduleDb::load - %s", (is_array($scheduleId) ? join(', ', $scheduleId) : $scheduleId)));
        $cols = CRoot::getDbMembers('CSchedule');
        foreach ($cols as $colId => $col)
            $cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
        if (is_array($scheduleId) == false)
            $scheduleId = (array) $scheduleId;
        $where = (count($scheduleId) > 1 ? sprintf('%s in (%s)', CSchedule::ID, join(',', $scheduleId)) : sprintf("%s=%s", CSchedule::ID, $scheduleId[0]));
        $sql = sprintf(self::SELECT_SQL, join(',', $cols), $where);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowObject($res))) {
            $schedule = new CSchedule();
            foreach ($o as $field => $val)
                $schedule->set($field, $val);
            $out[] = $schedule;
            CLogging::info(sprintf("CScheduleDb::load - found %ld", $schedule->get(CSchedule::ID)));
        }
        return $out;
    }

    /*
     * Get schedule ids for a given user's schedules (where they are driving)
     * 
     * @param $userId     User id, whose schedule ids to return
     * @return            Array of schedule ids or false if failed
     */

    public static function getUserScheduleIds($userId) {
        CLogging::info(sprintf("CScheduleDb::getUserScheduleIds - %s", $userId));
        $sql = sprintf(self::SELECT_SQL, CSchedule::ID, CSchedule::USER_ID . '=' . $userId);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            CLogging::info(sprintf("CScheduleDb::getUserScheduleIds - found %ld", $o[CSchedule::ID]));
            $out[] = $o[CSchedule::ID];
        }
        return $out;
    }

    /*
     * Delete a schedule
     * 
     * @param $scheduleId  ID of schedule to delete
     */

    public static function delete($scheduleId) {
        CLogging::info(sprintf("CScheduleDb::delete - %s", $scheduleId));
        $sql = sprintf(self::DELETE_SQL, sprintf('%s=%ld', CSchedule::ID, $scheduleId));
        CDb::query($sql);
    }

}

?>
