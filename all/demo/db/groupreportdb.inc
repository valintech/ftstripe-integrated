<?php

/*
 * Group class
 */


class CGroupReportDb {

    const NEW_SQL = "INSERT INTO groupreport (%s) VALUES (%s)";
    const UPDATE_SQL = "UPDATE groupreport SET %s WHERE id=%s";
    const GET_SQL = "SELECT %s FROM groupreport WHERE id=%s";
    const GET_MONTHSQL = "SELECT id,carbon_savings, total_mileage FROM groupreport 
        WHERE month=%d and year=%d and group_id=%d";

    /*
     * Constructor
     */

    function __construct() {
        
    }

    /*
     * Load
     * 
     * @param $id     Group id.
     * @param $o      A CGroupReport object to load into
     * @return        true or false
     */
 
    public static function loadAllGroup($user_id, $o) {
       $sql = "SELECT id as id,group_tag as name FROM groups".$user_id;
        $res = CDb::query($sql);
        $first_date = date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $id = CDb::makePhpValue($o['id'], $o['type']);
            $o['name'] = CDb::makePhpValue($o['name'], $o['type']);
            $o['date'] = $first_date;
        $sql_total = "SELECT  (( 430 * sum( miles ) ) /1000) as total_miles FROM journey WHERE DATE(journey.endtime) BETWEEN '$first_date' AND '$last_date' and journey.passenger_id in(SELECT ug.user_id FROM user_group as ug where ug.group_id=$id))";
        $res_total = CDb::query($sql_total);
         if ($res_total == false)
            return false;
           while (($ol = CDb::getRowArray($res_total))) {
            $o['total_miles']  =$ol['total_miles'];
            
           }
           $out[] = $o;
        }print_r($out);exit;
        return $out;
    }
    public static function load($id, $o) {
        $cols = CGroupReportDb::makeColsList();
        foreach ($cols as $key => $col)
            $cols[$key] = $col[0];
        $sql = sprintf(CGroupReportDb::GET_SQL, join(",", $cols), $id);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = mysql_fetch_object($res);
        if ($res == false)
            return false;
        foreach ($o as $field => $val)
            $o->set($field, $val);
        return true;
    }

    /*
     * Load
     * 
     * @param $id     Group id.
     * @param $o      A CGroupReport object to load into
     * @return        true or false
     */

    public static function loadByMonth($group_id, $month,$year) {
        $sql = sprintf(CGroupReportDb::GET_MONTHSQL,$month,$year,$group_id);
        $res = CDb::query($sql);
       if ($res == false)
            return false;
        return $res;
    }

    /*
     * Save
     * 
     * @param $o   A CGroupReport object to save
     * @return     true or false
     */

    public static function save($o) {

        // Get list of columns. Include id. column if already exists
        $cols = CGroupReportDb::makeColsList(false, false);
        // Turn data into SQL compatible strings
        
        $vals = array();
        foreach ($cols as $columnId => $col) {
            $vals[$columnId] = CDb::makeSqlValue($o->get($columnId), $col[0]);
            $cols[$columnId] = $col[0];
        }
        // Create SQL statement for insert or update
            $sql = sprintf(CGroupReportDb::NEW_SQL, join(",", $cols), join(",", $vals));
           
        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
                $id = CDb::getLastAutoId();
                $o->set(CGroupReport::ID, $id);
            CDb::commitTrans();
        }
        else
            CDb::rollbackTrans();

        return $res ? true : false;
    }

    /*
     * Get Total Miles travelled in a month of a  group  by each user
     *  * @param $Month      Last Month.
     * @param  $year         Current Year/ in case of january year will be last year
     *  return MIles 
     */

    public static function totalMilesOfTheMonthByGroup($Month, $year) {

        $sql = "SELECT  g.id As 'group_Id',SUM( journey.`miles` ) AS 'total_miles'
FROM  `journey` 
INNER JOIN  `user_group` ug ON journey.`passenger_id` = ug.`user_id` 
INNER JOIN  `groups` g ON ug.group_id = g.id  WHERE YEAR(journey.`endtime`) =" . $year . "  AND MONTH(journey.`endtime`) = $Month GROUP BY ug.group_id";
      
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = CDb::getRowArray($res);
        return $out;
    }
 /*
     * Return list of columns
     * 
     * @param $includeIdCol  true to return id. column in list
     * @param $colIds        Array of columns ids. to include in returned list
     * @return               Array of arrays, key is column id, data is array
     * 						 containing datatype
     */

    static function makeColsList($includeIdCol = true, $colIds = false) {
        $cols = array();
        $cols[CGroupReport::GROUP_ID] = array("GROUP_ID");
        $cols[CGroupReport::CARBON_SAVINGS] = array("CARBON_SAVINGS");
        $cols[CGroupReport::TOTAL_MILEAGE] = array("TOTAL_MILEAGE");
        $cols[CGroupReport::CREATED] = array("CREATED");
        $cols[CGroupReport::MAIL_STATUS] = array("MAIL_STATUS");
        $cols[CGroupReport::MONTH] = array("MONTH");
          $cols[CGroupReport::YEAR] = array("YEAR");
   
        if ($includeIdCol)
            $cols[CGroupReport::ID] = array("i");
        if (is_array($colIds))
            foreach ($colIds as $colId)
                if (isset($cols[$colId]) == false)
                    unset($cols[$colId]);
        return $cols;
    }


    public static function updateStatus($id) {
        
        $sql = sprintf(CGroupReportDb::UPDATE_SQL, "mail_status='y'", $id);
        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
           CDb::commitTrans();
        }
        else
            CDb::rollbackTrans();

        return $res ? true : false;
    }

}

?>
