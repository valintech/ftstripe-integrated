<?php

/*
 * Class handler for journey requests
 */

class CUsedFreeMilesDb {

    // SQL statements

    const NEW_SQL = 'INSERT INTO used_freemiles (%s) VALUES (%s)';
    const GET_SQL = 'SELECT %s FROM used_freemiles WHERE %s';
    const UPDATE_SQL = 'UPDATE used_freemiles SET %s WHERE id=%s';

    /*
     * Load request by id.
     *
     * @param $adminusers         A CJourney object to fill
     * @param $id              Journey request id.
     * @return                 true or false
     */

   public static function save($usedfreemiles) {

        $id = $usedfreemiles->get(CUsedFreeMiles::ID);
       
        $cols = CRoot::getFilteredDbMembers('CUsedFreeMiles', false,array(CUsedFreeMiles::ID));
        // Turn data into SQL compatible strings
        $vals = array();

        foreach ($cols as $colId => $col)
            $vals[$colId] = CDb::makeSqlValue($usedfreemiles->get($colId), $col['type']);
        $vals[CUsedFreeMiles::CREATEDBY] = 'now()';

        // Create SQL statement for insert or update
        if ($id == 0) {
             if($usedfreemiles->get(CUsedFreeMiles::ID)==NULL)  
            $vals[CUsedFreeMiles::JOURNEYID] = 0;
            $sql = sprintf(self::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
        } 

        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
            if ($id == 0) {
                $id = CDb::getLastAutoId();
                $usedfreemiles->set(CUsedFreeMiles::ID, $id);
            }
            else{
                $id=0;
            }
            CDb::commitTrans();
        }
        else
            CDb::rollbackTrans();

        return $id;
    }
       public static function assignedUserFreeMiles($userId) {
            $sql_results = "SELECT  CASE WHEN sum(uf.miles) IS NULL THEN 0 ELSE sum(uf.miles)-(SELECT  CASE WHEN sum(usf.used_miles) IS NULL THEN 0 ELSE sum(usf.used_miles) END AS used_sum_miles FROM used_freemiles as usf where usf.user_id=$userId) END AS sum_miles FROM user_freemiles as uf where uf.user_id=$userId";
        $results = CDb::query($sql_results);
        if ($results == false)
            return false;
        $out = CDb::getRowArray($results);
         
        return $out;
   
       }
}
?>
