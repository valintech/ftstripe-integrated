<?php

/*
 * Group class
 */

class CUserGroupDb {

    const NEW_SQL = "INSERT INTO user_group (%s) VALUES %s";
    const UPDATE_SQL = "UPDATE user_group SET %s WHERE id=%s";
    const GET_SQL = "SELECT %s FROM user_group WHERE id=%s";
    const GET_USERSQL = "SELECT group_id FROM user_group WHERE user_id=%s";
    const GET_USERSQLNEW = "SELECT id FROM groups WHERE user_id=%s";
    const GET_USERSQLNEWDEL = "SELECT id FROM groups WHERE user_id=%s and group_tag in(%s)";
    const DELETE_SQL = "DELETE FROM user_group WHERE user_id=%d and group_id in(%s)";
    const GET_USER_STATUS_SQL = "SELECT id FROM user_group WHERE group_id=%s AND user_id=%s";
    const UPDATE_USER_STATUS_SQL = "UPDATE user_group SET %s WHERE group_id=%s AND user_id=%s";
 const GET_APPROVESQL = "select group_id from user_group where status='n' and created_type='s' AND user_id=%s";
 const DELETE_MEMBER_SQL = "DELETE FROM `user_group` WHERE user_id IN (%s)";
 const DELETE_DOMIAN_USER = "DELETE FROM user_group WHERE user_id=%d and created_type ='%s'";
  const DELETE_MEMBER_BY_GROUPID_SQL = "DELETE FROM `user_group` WHERE group_id=(%s)";
    const GET_ALL_USERS_By_GROUPID_SQL = "SELECT GROUP_CONCAT(CONVERT(u.`email`, CHAR))  FROM user u left join `user_group` ug ON ug.`user_id`=u.id WHERE ug.`group_id`=%s";

 /*
     * Constructor
     */

    function __construct() {
        
    }

    /*
     * Load
     * 
     * @param $id     Group id.
     * @param $o      A CUserGroup object to load into
     * @return        true or false
     */

    public static function load($id, $o) {
        $cols = CUserGroupDb::makeColsList();
        foreach ($cols as $key => $col)
            $cols[$key] = $col[0];
        $sql = sprintf(CUserGroupDb::GET_SQL, join(",", $cols), $id);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = mysql_fetch_object($res);
        if ($res == false)
            return false;
        foreach ($o as $field => $val)
            $o->set($field, $val);
        return true;
    }

    /*
     * Load
     * 
     * @param $id     Group id.
     * @param $o      A CUserGroup object to load into
     * @return        true or false
     */

    public static function loadExistingGroups($id, $o) {

        $sql = sprintf(CUserGroupDb::GET_USERSQL, $id);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUserGroup::GROUP_ID];
        }
        return $out;
    }
    
    
    
     public static function loadNewGroups($id, $o) {

        $sql = sprintf(CUserGroupDb::GET_USERSQLNEW, $id);
       
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUserGroup::ID];
        }
        return $out;
    }
    
    
     public static function loadNewGroupsDel($id,$newgroup, $o) {
        $group_name = implode(',',$newgroup);
       $group_name = str_replace(',',"','",$group_name);
       $group_name ="'".$group_name."'";
        $sql = sprintf(CUserGroupDb::GET_USERSQLNEWDEL, $id,$group_name);
       
       $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUserGroup::ID];
        }
        return $out;
    }

    /*
     * Load unapproved groups
     * 
     * @return      array of group ids
     */

    public static function loadUnapprovedGroups($userid) {

        $sql = sprintf(CUserGroupDb::GET_APPROVESQL,$userid);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $out[] = $o[CUserGroup::GROUP_ID];
        }
        return $out;
    }

    /*
     * Return list of columns
     * 
     * @param $includeIdCol  true to return id. column in list
     * @param $colIds        Array of columns ids. to include in returned list
     * @return               Array of arrays, key is column id, data is array
     * 						 containing datatype
     */

    static function makeColsList($includeIdCol = true, $colIds = false) {
        $cols = array();
        $cols[CUserGroup::NAME] = array("s");
        $cols[CUserGroup::CREATED] = array("dt");
        $cols[CUserGroup::UPDATED] = array("dt");
        $cols[CUserGroup::USER_ID] = array("i");
        if ($includeIdCol)
            $cols[CUserGroup::ID] = array("i");
        if (is_array($colIds))
            foreach ($colIds as $colId)
                if (isset($cols[$colId]) == false)
                    unset($cols[$colId]);
        return $cols;
    }

    /*
     * Delete user group
     * 
     * @param $extra_groups   extra groups in current session
     * @param $userid user id
     * @return     none
     */

    public static function deleteUserGroup($extra_groups, $userid) {

        $sql = sprintf(CUserGroupDb::DELETE_SQL, $userid, $extra_groups);

        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            CDb::commitTrans();
        }
        else
            CDb::rollbackTrans();
        return $res ? true : false;
    }

    /*
     * Save
     * 
     * @param $o   A CUserGroup object to save
     * @return     true or false
     */

    public static function save($values, $from) {
        $cols = CUserGroup::USER_ID . ',' . CUserGroup::GROUP_ID . ',' . CUserGroup::CREATED_TYPE . ',' .
                CUserGroup::STATUS . ',' . CUserGroup::CREATED;
        $sql = sprintf(CUserGroupDb::NEW_SQL, $cols, $values);

         if ($from != 1)
        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {

            CDb::commitTrans();
        }
        else
            CDb::rollbackTrans();

        return $res ? true : false;
    }

    public static function loadIdByOwnerIdAndUserId($groupId, $userId) {

        $sql = sprintf(CUserGroupDb::GET_USER_STATUS_SQL, $groupId, $userId);

        CDb::BeginTrans();
        $res = CDb::query($sql);
        $out = mysql_fetch_object($res);
        return $out->id;
    }
      public static function getUserStatusInGroup($groupId, $userId) {

       $sql = "SELECT status FROM user_group WHERE group_id=$groupId AND user_id=$userId";
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        $out = CDb::getRowArray($res);
        return $out;
    }


    public function updateStatus($o) {
        $sql = sprintf(CUserGroupDb::UPDATE_SQL, "status=" . $o->get(CUserGroup::STATUS), $o->get(CUserGroup::ID));
        CDb::BeginTrans();
        $res = CDb::query($sql);
        CDb::rollbackTrans();
        return $res ? true : false;
    }

    public static function getGroupOwnerMemeberDetails($groupId) {
        $sql = "SELECT user_id,	group_tag FROM `groups` WHERE `id` = $groupId ";
        
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        $out = CDb::getRowArray($res);
        return $out;
    }

    public static function getNameById($userId) {

        $sql = "SELECT email,first_name,last_name,photo FROM `user` WHERE `id` = $userId ";
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        $out = CDb::getRowArray($res);
        return $out;
    }
    
    /*
     * Removes domain matching groups of user
     */

    public function removeUserGroups($userid, $createdtype) {
        $sql = sprintf(CUserGroupDb::DELETE_DOMIAN_USER, $userid, $createdtype);
        CDb::BeginTrans();
        $res = CDb::query($sql);
        CDb::rollbackTrans();
        return $res ? true : false;
    }
    
 function getUserGroupMemebers($str,$group_id,$user){
        
        
     $sql = "SELECT u.id,u.first_name,u.last_name FROM groups g join user_group ug on g.id=ug.group_id join user u on ug.user_id=u.id where g.user_id=$user and ug.group_id=$group_id and CONCAT(first_name, ' ', last_name) LIKE '%$str%' limit 0,5";
       
       
        $res = CDb::query($sql);
        if ($res == false)
            return false;
      
        while (($o = CDb::getRowArray($res))) {
            $o['id'] = CDb::makePhpValue($o['id'], $o['type']);
            $o['name'] = CDb::makePhpValue($o['first_name'], $o['type']).' '.CDb::makePhpValue($o['last_name'], $o['type']);
            unset($o['first_name']);
            unset($o['last_name']);
            $out[] = $o;
        }
        return $out;
    }  
    
    function RemoveGroupMemebrsByGroupAdmin($searched_group_members){
         $sql = sprintf(CUserGroupDb::DELETE_MEMBER_SQL, $searched_group_members);
        CDb::BeginTrans();
        $res = CDb::query($sql);
        CDb::rollbackTrans();
        return $res ? true : false; 
    }

    function RemoveGroupMemebrsByGroupId($groupId) {
        $sql = sprintf(CUserGroupDb::DELETE_MEMBER_BY_GROUPID_SQL, $groupId);
        CDb::BeginTrans();
        $res = CDb::query($sql);
        CDb::rollbackTrans();
        return $res ? true : false;
}

public function changeOwnershipInUserGroup($groupId, $currentOwnerId, $NewRequestedOwnerId) {

         $sql = "UPDATE user_group  SET created_type = 's' WHERE group_id = $groupId AND user_id = $currentOwnerId AND created_type = 'n'";

        CDb::BeginTrans();
        $res = CDb::query($sql);
        CDb::commitTrans();

         $new_sql = "SELECT id FROM user_group WHERE group_id = $groupId AND user_id = $NewRequestedOwnerId";
        $res = CDb::query($new_sql);
        $out = array();
        $out = CDb::getRowArray($res);
        


        if (is_array($out)&&count($out)>0) {

          $update_sql = "UPDATE user_group  SET created_type = 'n' WHERE group_id = $groupId AND user_id = $NewRequestedOwnerId";
            CDb::BeginTrans();
            $res = CDb::query($update_sql);
           CDb::commitTrans();

        } else if($out==FALSE) {

             $cur_date ='now()';
            $insert_sql = "INSERT INTO `user_group` ( `user_id`, `group_id`, `created_type`, `status`, `created`) VALUES ( $NewRequestedOwnerId, $groupId, 'n', 'y',$cur_date )";
            CDb::BeginTrans();
            $res = CDb::query($insert_sql);
            CDb::commitTrans();
        }

        return true;
    }

    

    public static function getallMemebersByGroupId($groupId) {

        $sql = sprintf(CUserGroupDb::GET_ALL_USERS_By_GROUPID_SQL, $groupId);

        CDb::BeginTrans();
        $res = CDb::query($sql);
        $out = mysql_fetch_array($res);
        return $out[0];
    }

}

?>
