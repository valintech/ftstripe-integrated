<?php

/*
 * Class handler for journey requests
 */

class CJourneyDb {

    // SQL statements

    const NEW_SQL = 'INSERT INTO journey (%s) VALUES (%s)';
    const GET_SQL = 'SELECT %s FROM journey WHERE %s';
    const UPDATE_SQL = 'UPDATE journey SET %s WHERE id=%s';

    /*
     * Load request by id.
     *
     * @param $journey         A CJourney object to fill
     * @param $id              Journey request id.
     * @return                 true or false
     */

    public static function load($journey, $id) {
        return self::_loadCore($journey, array(array('fieldId' => CJourney::ID, 'fieldVal' => $id)));
    }

    /*
     * Load active journey for a given passenger
     *
     * @param $journey         A CJourney object to fill
     * @param $passId          Passenger id.
     * @return                 true or false
     */

    public static function loadActiveByPassengerId($journey, $passId) {
        $active = "A";
        $queryFields = array();
        $queryFields[] = array('fieldId' => CJourney::PASSENGER_ID, 'fieldVal' => $passId);
        $queryFields[] = array('fieldId' => CJourney::STATUS, 'fieldVal' => "'$active'");
        return self::_loadCore($journey, $queryFields);
    }

    public static function loadJGetDriverDetails($driver_id, $width, $height) {
        //$sql = sprintf(CUserDb::GET_USER_SQL, CUser::FIRST_NAME,CUser::LAST_NAME, CUser::PHOTO, CUser::ID, trim($userId));
        //$sql = $sql = "SELECT first_name,last_name,photo FROM `user` WHERE `id` = $userId ";
        $sql = "SELECT id,photo FROM `user` WHERE `id` = $driver_id ";


        $photo = '';
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out_details = null;
        $out = array();
        $out = CDb::getRowArray($res);
        if (is_array($out)) {


            $file = '';
            $filett = '';
            $dir = '../upload/' . $out['id'] . "/";
            if (isset($out['photo'])) {
                if (is_dir($dir)) {
                    if ($dh = opendir($dir)) {
                        while (($file = readdir($dh)) !== false) {



                            $pos = strpos($file, $out['id'] . "_" . $width);
                            if ($pos !== false)
                                $filett = $file;
                        }
                        closedir($dh);
                    }
                }
            }
            $photo = 'upload/' . $out['id'] . '/' . $filett;
        }
        return $photo;
    }

    public static function loadActiveByAverageRating($journey, $driver_id) {
        $sql = "SELECT sum(ratedby_passenger) / count(*) as average_rating FROM journey WHERE driver_id =$driver_id";

        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $journey = CDb::getRowObject($res);
        if ($journey == false)
            return false;
        return $journey->average_rating;
    }

    public static function getFetchJIDReport($date_from, $date_to) {
$arr=array();
        $sql = "SELECT CASE WHEN sum(used_miles) IS NULL THEN 0 ELSE sum(used_miles) END AS used_sum_miles FROM used_freemiles where created_at >= STR_TO_DATE('$date_from','%d-%m-%Y') and created_at <= DATE_ADD(STR_TO_DATE('$date_to','%d-%m-%Y'),INTERVAL 1 DAY)";

        $res = CDb::query($sql);
     
        $journey = CDb::getRowObject($res);
        $arr['used_free_miles'] = $journey->used_sum_miles;
        $sql_results = "SELECT  CASE WHEN sum(uf.miles) IS NULL THEN 0 ELSE sum(uf.miles)-(SELECT  CASE WHEN sum(usf.used_miles) IS NULL THEN 0 ELSE sum(usf.used_miles) END AS used_sum_miles FROM used_freemiles as usf) END AS sum_miles FROM user_freemiles as uf";
        $results = CDb::query($sql_results);
      
        $journeyy = CDb::getRowObject($results);
       
        $arr['balance_miles'] = $journeyy->sum_miles;
         $sql_allotedresult = "SELECT CASE WHEN sum(miles) IS NULL THEN 0 ELSE sum(miles) END AS tsum_miles FROM user_freemiles
  where (transferred_by IS NULL OR transferred_by = '')  and created_at >= STR_TO_DATE('$date_from','%d-%m-%Y') and created_at <= DATE_ADD(STR_TO_DATE('$date_to','%d-%m-%Y'),INTERVAL 1 DAY)";
//echo $sql_allotedresult;exit;
        $allotresult = CDb::query($sql_allotedresult);
       
        $journeysalot = CDb::getRowArray($allotresult);
    
        
        
         $arr['alotted_miles'] = $journeysalot['tsum_miles'];
        return $arr;
    }

    public static function getWhereJID($userId) {
        $sql = "SELECT id FROM journey WHERE passenger_id =$userId and status='A'";
        /*$file = fopen("/var/www/html/test_firsthumb22-sep-2016/upload/sample2.txt", "a+");
        $msg = $sql;

        fwrite($file, $msg);
        fclose($file);*/
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $journey = CDb::getRowObject($res);
        if ($journey == false)
            return false;
        return $journey->id;
    }

    /*
     * Load active journey  details for the for a given passenger with journey id
     * @param $journeyId       cjourney id
     * @return                 return from latitude,from longitude, to latitude, to longitude, tracked miles
     */

    public static function getRecentPostionByJourneyId($journeyId) {
        $sql = 'select from_lat,from_lng,to_lat,to_lng,tracked_miles from journey where status="A" AND  id=' . $journeyId;
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = CDb::getRowArray($res);
        return $out;
    }
public static function paymentStatusMissingInternet($userId) {
        $sql = 'SELECT id,miles FROM journey WHERE payment_status  IS NULL AND status="P" and passenger_id=' . $userId;
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = CDb::getRowArray($res);
        return $out;
    }
    public static function getRecentPostionByJourneyIdStatus($userId) {
        $sql = 'select transaction_id,driver_amount,owner_amount,paid_miles,payment_mode,amount,miles,passenger_id,driver_id,id from journey where passenger_id=' . $userId . ' and payment_status=1 and status="P"';
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = CDb::getRowArray($res);

        return $out;
    }

    public static function getFetchJID($journeyId) {
        $sql = 'select id,driver_amount,owner_amount,miles,paid_miles,request,transaction_id,passenger_id,driver_id from journey where id=' . $journeyId;
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = CDb::getRowArray($res);


        return $out;
    }

    public static function updatedJourneyAmountStatus($journey_id, $payment_status, $amount, $payment_mode, $paidmiles, $driver_amount, $owner_amount, $paydonebyowner) {
        $sql = 'UPDATE journey SET paydoneby=' . $paydonebyowner . ', paid_miles=' . $paidmiles . ', driver_amount=' . $driver_amount . ', owner_amount=' . $owner_amount . ', payment_status=' . $payment_status . ', amount="' . $amount . '" , payment_mode=' . $payment_mode . '  where id=' . $journey_id;
        $res = CDb::query($sql);
 $file = fopen("/home1/eridesh1/public_html/demo/upload/testr.txt", "a+");
        fwrite($file, $sql);
        fclose($file);
        return $res ? true : false;
    }

    public static function getSetFetchSend($journey_id) {
        $sql = 'UPDATE journey SET request=1  where id=' . $journey_id;
        $res = CDb::query($sql);

        return $res ? true : false;
    }

    public static function getRecentUpdatePayJourneyIdStatus($journey_id) {
        $now = 'now()';
        $sql = 'UPDATE journey SET passenger_pay_date_time=' . $now . ', payment_status=2  where id=' . $journey_id;
        $res = CDb::query($sql);

        return $res ? true : false;
    }

    public static function getRecentUpdatePayDriverJourneyIdStatus($journey_id, $date_time, $payKey, $transaction_id, $status) {
        //ini_set('display_errors', '1');
        $sql = 'UPDATE journey SET pay_key="' . $payKey . '",driver_got_amount_date_time="' . $date_time . '", payment_status=2, transaction_id="' . $transaction_id . '",paydoneby=' . $status . '  where id=' . $journey_id;
        $res = CDb::query($sql);
        /*$file = fopen("/var/www/html/test_firsthumb22-sep-2016/upload/testr.txt", "a+");
        fwrite($file, $sql);
        fclose($file);*/
        return $res ? true : false;
    }

    public static function getRecentUpdatePayDriverJourneyIdStatusMicro($journey_id, $payKey, $date_time, $transaction_id, $status) {
        //ini_set('display_errors', '1');
        $sql = 'UPDATE journey SET pay_key="' . $payKey . '",driver_got_amount_date_time="' . $date_time . '", payment_status=' . $status . ', transaction_id="' . $transaction_id . '",paydoneby=' . $status . '  where id=' . $journey_id;
        $res = CDb::query($sql);
      
        return $res ? true : false;
    }

    public static function getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id, $transaction_id) {
        // ini_set('display_errors', '1');
        $sql = 'UPDATE journey SET payment_status=2,payout_transation="' . $transaction_id . '"  where id=' . $journey_id;
        $res = CDb::query($sql);
        /*$file = fopen("/home1/eridesh1/public_html/demo/upload/testr.txt", "a+");
        fwrite($file, $sql);
        fclose($file);*/
        return $res ? true : false;
    }

    /*
     * Core code for 'loadxxx' methods
     *
     * @param $journey         A CJourney object to fill
     * @param $queryFields     Array of query arrays containing 'fieldId' and
     *                         'fieldVal' members
     * @return                 true or false
     */

    protected static function _loadCore($journey, $queryFields) {
        $cols = CRoot::getDbMembers('CJourney');
        $types = array();
        foreach ($cols as $colId => $col) {
            $cols[$colId] = CDb::makeSelectValue($colId, $col['type']);
            $types[$colId] = $col['type'];
        }
        $where = array();
        foreach ($queryFields as $queryField)
            $where[] = sprintf('%s=%s', $queryField['fieldId'], $queryField['fieldVal']);
        $sql = sprintf(self::GET_SQL, join(',', array_keys($cols)), join(' AND ', $where));
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = mysql_fetch_object($res);
        if ($o) {
            foreach ($o as $fieldId => $fieldVal)
                $journey->set($fieldId, CDb::makePhpValue($fieldVal, $types[$fieldId]));
        }
        return true;
    }

    /*
     * Save the journey
     *
     * @param $journey           A CJourney object to save
     * @return                   true or false
     */

    public static function save($journey) {
//$file = fopen("/home1/eridesh1/public_html/demo/upload/test.txt","a+");
//              fwrite($file,'mmmm');
//              fclose($file); 
        // Get list of columns. Include id. column if already exists
        $id = $journey->get(CJourney::ID);

        $cols = CRoot::getFilteredDbMembers('CJourney', false, array(CJourney::ID));
        // Turn data into SQL compatible strings
        $vals = array();

        foreach ($cols as $colId => $col)
            $vals[$colId] = CDb::makeSqlValue($journey->get($colId), $col['type']);

        // Create SQL statement for insert or update
        if ($id == 0) {
            $vals[CJourney::DEBIT] = 0;
            $vals[CJourney::STARTTIME] = 'now()';
            $sql = sprintf(self::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
        } else {
            unset($cols[CJourney::STARTTIME]);
            if ($journey->get(CJourney::STATUS) == 'P')
                $vals[CJourney::ENDTIME] = 'now()';
            else
                unset($cols[CJourney::ENDTIME]);
            $tmp = array();

            foreach ($cols as $colId => $col)
                $tmp[] = sprintf("%s=%s", $colId, $vals[$colId]);



            $sql = sprintf(self::UPDATE_SQL, join(",", $tmp), $id);
        }
       /* $file = fopen("/home1/eridesh1/public_html/demo/upload/test.txt", "a+");
        fwrite($file, $sql);
        fclose($file);*/
        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
            if ($id == 0) {
                $id = CDb::getLastAutoId();
                $journey->set(CJourney::ID, $id);
            }
            CDb::commitTrans();
        } else
            CDb::rollbackTrans();

        return $res ? true : false;
    }

    /*
     * Get journey ids for where a given user is accepted as a passenger
     *
     * @param $userId     User id, whose journey ids to return
     * @return            Array of journey ids or false if failed
     */

    public static function getUserAccepted($userId) {
        CLogging::info(sprintf("CJourneyRequestDb::getUserAccepted - %s", $userId));
        $where = sprintf("%s = %s AND %s IS NOT NULL", CJourneyRequest::USER_ID, $userId, CJourneyRequest::RESPONDED);
        $sql = sprintf(self::GET_SQL, CJourneyRequest::JOURNEY_ID, $where);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            CLogging::info(sprintf("CJourneyRequest::getUserAccepted - found %ld", $o[CJourneyRequest::JOURNEY_ID]));
            $out[] = $o[CJourneyRequest::JOURNEY_ID];
        }
        return $out;
    }

    public static function loadJourneysAverageRating($id, $type_id) {

        if ($type_id == 1)
            $sql = "SELECT max(id) as id,sum(ratedby_driver) / count(*) as average_rating,passenger_id,(select CONCAT( first_name, ' ', last_name ) from user where id = passenger_id) as passenger_name FROM journey WHERE passenger_id in(" . $id . ") group by passenger_id";
        else
            $sql = "SELECT id,sum(ratedby_passenger) / count(*) as average_rating,driver_id,(select CONCAT( first_name, ' ', last_name ) from user where id = driver_id) as driver_id_name FROM journey WHERE driver_id=$id group by driver_id";

        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {

            $out[] = $o;
        }

        return $out;
    }

    public static function loadPassengerPassPhrase($id) {

        $sql = "select text from calllog where journey_id=$id";

        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $text = "";
        while (($o = CDb::getRowArray($res))) {

            $text = json_decode((string) $o[text]);
        }

        $out = $text->magic;



        if (isset($out)) {
            return $out;
        }
        return;
    }

    public static function loaddriverid($j_id) {

        $sql = "select driver_id from journey where id=$j_id and status='A'";

        $res = CDb::query($sql);
        if ($res == false)
            return false;

        $out = array();

        while (($o = CDb::getRowArray($res))) {
            $driver = $o[CJourney::DRIVER_ID];
        }
        return $driver;
    }

    /*
     * Return "$cnt" most recent journeys of all users at once,
     * active or not.
     * Used only by the calllog.php page at the time of writing (June 2014)
     *
     * @param $cnt       Number of records to return
     * @return           Array of records
     */

    public static function getRecentJourneysAllUsers($cnt) {
        $sql = "select id,driver_id,(select first_name from user where id = driver_id) as driver_first_name,";
        $sql .= "(select last_name from user where id = driver_id) as driver_last_name,passenger_id,";
        $sql .= "(select first_name from user where id = passenger_id) as passenger_first_name,";
        $sql .= "(select last_name from user where id = passenger_id) as passenger_last_name,starttime,";
        $sql .= "endtime,miles,credit,debit,status from journey order by id desc limit " . sprintf("%d", $cnt);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $o['starttime'] = CDb::makePhpValue($o['starttime'], 'dt');
            $o['endtime'] = CDb::makePhpValue($o['endtime'], 'dt');
            $out[] = $o;
        }
        return $out;
    }

    /*
     * Return array of $cnt most recent records (oldest first) for $userId.
     * If $cnt==0 then all are returned.
     * Returns only active journeys if true == $active, else returns
     * both completed and active journeys.
     *
     * @param $cnt       Number of records to return
     * @param $active 	Boolean to determine whether to return list of
     * 						active or completed journeys.
     * @return           Array of records
     */

    public static function getRecentJourneysOneUser($userId, $active, $cnt) {
//		$actstr = sprintf("%s",($active ? "active" : "complete"));
//		CLogging::debug("CJourneyDb::getRecentJourneysOneUser(".$userId.",".$actstr.",".$cnt.")");
        $sql = "select id,driver_id,(select first_name from user where id = driver_id) as driver_first_name,";
        $sql .= "(select last_name from user where id = driver_id) as driver_last_name,passenger_id,";
        $sql .= "(select first_name from user where id = passenger_id) as passenger_first_name,";
        $sql .= "(select last_name from user where id = passenger_id) as passenger_last_name,starttime,";
        $sql .= "endtime,miles,credit,debit from journey where ";
        if ($active)
            $sql .= "status = 'A' and ";
        $sql .= sprintf("( driver_id = %d or passenger_id = %d )", $userId, $userId);
        $sql .= " order by id desc";
        if ("0" != $cnt) {
            $sql .= " limit " . sprintf("%d", $cnt);
        }
        /*$file = fopen("/home1/eridesh1/public_html/demo/upload/test.txt", "a+");
        fwrite($file, $sql);
        fclose($file);*/
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $o['starttime'] = CDb::makePhpValue($o['starttime'], 'dt');
            $o['endtime'] = CDb::makePhpValue($o['endtime'], 'dt');
            $out[] = $o;
        }
        return $out;
    }

    public static function driveremail($driver_id) {
        $sql = "SELECT paypal_email FROM user WHERE id =$driver_id";

        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $journey = CDb::getRowObject($res);
        if ($journey == false)
            return false;
        return $journey->paypal_email;
    }

}

?>
