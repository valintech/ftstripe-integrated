<?php

/*
 * Group class
 */

class CGroupDb {

    const NEW_SQL = "INSERT INTO groups (%s) VALUES (%s)";
    const UPDATE_SQL = "UPDATE groups SET %s WHERE id=%s";
    const GET_SQL = "SELECT %s FROM groups WHERE id=%s";
    const GET_GROUPS = "SELECT user . email , g . * FROM user
                        INNER JOIN groups AS g ON g.user_id = user.id";
    const VALIDATE_GROUP_TAG = "select count(*) from groups where group_tag='%s'";
    const GET_GROUP_NAMES = "SELECT id,group_tag FROM groups WHERE user_id=%s";
     const GET_GROUP_NAMES_BY_ID = "SELECT group_tag FROM groups WHERE id=%s";
    const GET_GROUP_AND_USER_STATUS_SQL = "SELECT id FROM groups WHERE id=%s AND user_id=%s";
    const DELETE_GROUP_BY_GROUPID_SQL="DELETE FROM groups WHERE id=%s";

    /*
     * Constructor
     */

    function __construct() {
        
    }

    /*
     * Load search matched groups
     * 
     * @param $id     Group id.
     * @param $str     search value
     * @return        array of group details
     */
 public static function loadSearchUserList($str,$userId, $o) {

        $sql = "SELECT groups.group_tag as name,groups.id as id FROM groups inner join user_group on groups.id=user_group.group_id WHERE groups.group_tag LIKE '%" . $str . "%' and user_group.user_id=".$userId;
        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $o['id'] = CDb::makePhpValue($o['id'], $o['type']);
            $o['name'] = CDb::makePhpValue($o['name'], $o['type']);

            $out[] = $o;
        }
        return $out;
    }
    public static function loadSearch($str, $o) {

        $sql = "SELECT groups.group_tag as name,groups.id as id FROM groups WHERE group_tag LIKE '%" . $str . "%'";
        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $out = array();
        while (($o = CDb::getRowArray($res))) {
            $o['id'] = CDb::makePhpValue($o['id'], $o['type']);
            $o['name'] = CDb::makePhpValue($o['name'], $o['type']);

            $out[] = $o;
        }
        return $out;
    }

    /*
     * Validate group name
     * 
     * @param $str    Group tag
     * @return        count of group tag matches
     */

    public static function validateGroupname($str) {

        $sql = sprintf(CGroupDb::VALIDATE_GROUP_TAG, $str);
        $res = CDb::query($sql);

        if ($res == false)
            return false;
        $duplicates = 0;
        while (($o = CDb::getRowArray($res))) {
            $duplicates = $o['count(*)'];
        }
        return $duplicates;
    }

    /*
     * Load 
     * 
     * @param $id     Group id.
     * @param $o     CGroup object
     * @return        true or falses
     */

    public static function load($id, $o) {
        $cols = CGroupDb::makeColsList();
        foreach ($cols as $key => $col)
            $cols[$key] = $col[0];
        $sql = sprintf(CGroupDb::GET_SQL, join(",", $cols), $id);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        $o = mysql_fetch_object($res);
        if ($res == false)
            return false;
        foreach ($o as $field => $val)
            $o->set($field, $val);
        return true;
    }

    /*
     * Load al groups
     * 
     * @param $id     Group id.
     * @param $o      A CGroup object to load into
     * @return        true or false
     */

    public static function loadAll() {

        $sql = CGroupDb::GET_GROUPS;
        $res = CDb::query($sql);
        if ($res == false)
            return false;

        return $res;
//		foreach ($o as $field => $val)
//			$o->set($field, $val);
//		return true;
    }

    /*
     * Return list of columns
     * 
     * @param $includeIdCol  true to return id. column in list
     * @param $colIds        Array of columns ids. to include in returned list
     * @return               Array of arrays, key is column id, data is array
     * 						 containing datatype
     */

    static function makeColsList($includeIdCol = true, $colIds = false) {
        $cols = array();
        $cols[CGroup::ORGANIZATION] = array("organization");
        $cols[CGroup::LOCATION] = array("location");
        $cols[CGroup::GROUP_TAG] = array("group_tag");
        $cols[CGroup::CREATED] = array("created");
        $cols[CGroup::MODIFIED] = array("modified");
        $cols[CGroup::USER_ID] = array("user_id");
        if ($includeIdCol)
            $cols[CGroup::ID] = array("group_id");
        if (is_array($colIds))
            foreach ($colIds as $colId)
                if (isset($cols[$colId]) == false)
                    unset($cols[$colId]);
        return $cols;
    }

    /*
     * Save
     * 
     * @param $o   A CGroup object to save
     * @return     id
     */

    public static function save($o, $includeMembers = false) {
        // Get list of columns. Include id. column if already exists
        $id = 0;
        $cols = CRoot::getFilteredDbMembers('CGroup', $includeMembers, array(CGroup::ID));

        // Turn data into SQL compatible strings
        $vals = array();
        foreach ($cols as $colId => $col)
            $vals[$colId] = CDb::makeSqlValue($o->get($colId), $col['type']);

        // Create SQL statement for insert or update
        $vals[CGroup::MODIFIED] = 'now()';
        $status = true;
        if ($id == 0) {
            $vals[CGroup::CREATED] = 'now()';
            $status = true;
            $sql = sprintf(CGroupDb::NEW_SQL, join(",", array_keys($cols)), join(",", $vals));
        } else {
            unset($cols[CGroup::CREATED]);
            unset($vals[CGroup::CREATED]);
            $tmp = array();
            foreach ($cols as $colId => $col)
                $tmp[] = sprintf("%s=%s", $colId, $vals[$colId]);
            $sql = sprintf(CGroupDb::UPDATE_SQL, join(",", $tmp), $id);
            $status = false;
        }

        CDb::BeginTrans();
        $res = CDb::query($sql);
        if ($res) {
            // Retrieve the auto ID if "insert" performed
            if ($id == 0) {
                $id = CDb::getLastAutoId();
                $o->set(CGroup::ID, $id);
            }
            // CDb::commitTrans();
        }
        // else
        //     CDb::rollbackTrans();

        return $id;
    }

    public function getGroupNamesByOwnerId($userId) {
          $sql = sprintf(CGroupDb::GET_GROUP_NAMES, $userId);
          
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        while (($o = CDb::getRowArray($res))) {
            $o['id'] = CDb::makePhpValue($o['id'], $o['type']);
            $o['group_tag'] = CDb::makePhpValue($o['group_tag'], $o['type']);

            $out[] = $o;
        }
        return $out;
    }

      public function getGroupNamesById($groupId) {
          $sql = sprintf(CGroupDb::GET_GROUP_NAMES_BY_ID, $groupId);
        $res = CDb::query($sql);
        if ($res == false)
            return false;
        while (($o = CDb::getRowArray($res))) {
            $o['group_tag'] = CDb::makePhpValue($o['group_tag'], $o['type']);

            $out[] = $o;
        }
        return $out;
    }
    
    public static function loadIdByOwnerIdAndgroupId($groupId, $userId) {
    
        $sql = sprintf(CGroupDb::GET_GROUP_AND_USER_STATUS_SQL, $groupId, $userId);
        CDb::BeginTrans();
        $res = CDb::query($sql);
        $out = mysql_fetch_object($res);
        return $out->id;
    }

    public function updateStatus($o) {


        $sql = sprintf(CGroupDb::UPDATE_SQL, "user_id=" . $o->get(CGroup::USER_ID), $o->get(CGroup::ID));
        CDb::BeginTrans();
        $res = CDb::query($sql);
        CDb::rollbackTrans();
        return $res ? true : false;
}

    public function RemoveGroupByGroupId($groupId){
        $sql = sprintf(CGroupDb::DELETE_GROUP_BY_GROUPID_SQL, $groupId);
        CDb::BeginTrans();
        $res = CDb::query($sql);
        CDb::rollbackTrans();
        return $res ? true : false;  
    }

}

?>