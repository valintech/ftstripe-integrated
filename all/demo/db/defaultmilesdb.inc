<?php

/*
 * Class handler for journey requests
 */

class CDefaultMilesDb {
    // SQL statements

    const NEW_SQL = 'INSERT INTO default_miles (%s) VALUES (%s)';
    const GET_SQL = 'SELECT %s FROM default_miles WHERE %s';
    const UPDATE_SQL = 'UPDATE default_miles SET %s WHERE id=%s';

    public static function editDefaultMiles($o,$enabled,$free_miles) {

        if($free_miles=='')
            $free_miles=0;
          if($enabled=='')
             $enabled=0;
        $sql = "UPDATE default_miles SET free_miles=$free_miles,active=$enabled where id=1";
        $res = CDb::query($sql);
         $sql_results = 'select * from default_miles where id=1';
        $results = CDb::query($sql_results);
        if ($results == false)
            return false;
        $out = CDb::getRowArray($results);
        return $out;
   

    }
    public static function displayDefaultMiles() {

         $sql_results = 'select * from default_miles where id=1';
        $results = CDb::query($sql_results);
        if ($results == false)
            return false;
        $out = CDb::getRowArray($results);
        return $out;
   

    }
}

?>
