<?php
class CConfig
{
	// Databases constants
	const DB_INCLUDE = "db/mysqldb.inc";
	const DB_TYPE = "mysql";
	const DB_HOST = "127.0.0.1:3306";
	const DB_NAME = "eridesh1_ecotest";
	const DB_USER = "eridesh1_ecotest";
	const DB_PWD = "VHF4[h.TXn+@";

	// Groups
	const GROUP_MEDIACITY = "MediaCityUK";

	// Mail constants
	const MAIL_TYPE = 'smtp';												// 'smtp', 'sendmail' or 'mail'
	const MAIL_ACCOUNT_USER = 'app+erideshare.co.uk';						// If MAIL_TYPE == 'smtp'
	const MAIL_ACCOUNT_PWD = 'W%)P9f(O;k,3';								// If MAIL_TYPE == 'smtp'
	const MAIL_SMTP_SERVER = 'ssl://just86.justhost.com';					// If MAIL_TYPE == 'smtp'
	const MAIL_SMTP_PORT = 465;												// If MAIL_TYPE == 'smtp'
	
	//TextLocal constants
	const TL_FROM = 'FirstThumb.com';
	const TL_USER = 'Steve.Dickson@firsthumb.com';
	const TL_PWD = 'Juvat12$$';
	const TL_URL = 'https://www.txtlocal.com/sendsmspost.php';
	
	// General constants
	const MIN_CREDIT_IN_MILES = 10;								// We won't refund below this amount
	const MAX_JOURNEY_LENGTH_IN_MILES = 250;					// We won't allow journeys longer than this.
	const SITE_PAYMENT_FRAMEWORK = 'PaypalSandbox';				// [ DumbButtons | PaypalSandbox | PaypalLive ]
	const SMS_SERVER = 'Android';								// [ Android | TextLocal ]
	const RUN_IN_FB = false;												// Running as Facebook canvas application?
	const USE_PEAR_MAIL = false;												// true to use PEAR "mail" extension for mail
	const INFO_EMAIL = 'FT@firstthumb.com';							// General information email
	const INFO_ACKEMAIL='info@firsthumb.com';
	const INFO_EMAIL_NAME = 'firstthumb FT';								// General information name
	const SITE_BASE = 'http://test.firsthumb.com';				// URL of site base
	const LOG_DIR = '/home/eridesh1/logs/test';
	const CONTENT_DIR = 'content/web';
	const LOCALE = "en_GB.UTF-8";
	const DEBUG = 1;
	const LOG_LEVEL = 2;													// 0 = no logging (apart from errors)
	const PHONE_NO = '07935 455213';
																			// 1 = debug statements only
																			// 2 = debug + informative logging
	const LANGUAGE_ID = 'en-uk';
	const GOOGLE_API_VER = 3;												// 2 or 3
																			// Generated for "http://erideshare.co.uk"
	const GOOGLE_API_KEY = 'ABQIAAAAe06VG-Y5-VwyqM23svPRsRQE3vimosQs-Ll6KOFZnN_iaMYeFxTo4aC38uOJd93lnT5GLu4Bccnq1w';												
	
	// Facebook constants
	const FB_API_KEY = '00be8925ac51c8c9b4502b49e2b509a4';
	const FB_SECRET = '42c3587c14b23e3d498159b33378db2d';
	const FB_APP_ID = '382392681825';
	const FB_CANVAS_APP_NAME = 'erideshare';
	const FB_APP_URL = 'http://apps.facebook.com';
	const FB_HOME_URL = 'http://www.facebook.com';
	
	// Paypal constants
	const PRIMARY_PAYPAL_EMAIL = "bank_1312907588_biz@eco-gogo.co.uk";		// This is the paypal account that receives customer payments.
	const PAYMENT_LOGGING_EMAIL = "PaymentNotifications@eco-gogo.co.uk";

	const PAYPAL_ENVIRONMENT = 'sandbox';								// [sandbox|beta-sandbox|live]
	const PAYPAL_API_USERNAME = 'bank_1312907588_biz_api1.eco-gogo.co.uk';
	const PAYPAL_API_PASSWORD = '1312907642';
	const PAYPAL_API_SIGNATURE = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AI4R0byKQJT5XNxLUbwrvVyUSpV8';

	//Urban airship constants
	// live
	const UA_APP_KEY = "GNm1zqlfTuitx6L_bRPpHQ";
	const UA_APP_SECRET = "XHsyryWjSzerw_9rIKTzSg";
	const UA_APP_MASTER_SECRET = "yrtT93gSQSuwANOZYG6f4w";
	// test
	#const UA_APP_KEY = "72u6tpW5TeGcaBmW5x0pgw";
	#const UA_APP_SECRET = "KBdGo_akQtakqErddFBUGQ";
	#const UA_APP_MASTER_SECRET = "R8UPB_F1Rt-AzUfT0UAWPQ";
	const UA_APP_URL = "https://go.urbanairship.com";

	// Google Cloud Messaging constants
	const GCM_API_KEY = "AIzaSyDmJ0FFtc8h-I_cPGCDZa9yJ4SWUpo7VAA";

	// Return a suitable PAYPAL_IPN_ACK_URL based upon the value of PAYPAL_ENVIRONMENT.
	// We Ack each IPN POST at this URL.
	static function getPaypalIpnAckUrl() {
		$IPN_AckUrl = "ssl://www.paypal.com";
		if( "sandbox" === self::PAYPAL_ENVIRONMENT || "beta-sandbox" === self::PAYPAL_ENVIRONMENT ) {
			$IPN_AckUrl = "ssl://www." . self::PAYPAL_ENVIRONMENT . ".paypal.com";
		}
		return $IPN_AckUrl;
	}
		
	// Return a suitable PAYPAL API ENDPOINT URL based upon the value of PAYPAL_ENVIRONMENT.
	static function getPaypalApiEndpoint() {
		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === self::PAYPAL_ENVIRONMENT || "beta-sandbox" === self::PAYPAL_ENVIRONMENT) {
			$API_Endpoint = "https://api-3t." . self::PAYPAL_ENVIRONMENT . ".paypal.com/nvp";
		}
		return $API_Endpoint;
	}
	
	const PENCE_PER_MILE_PURCHASE_RATE = 18;	// Customers pay this much to buy miles
	const PENCE_PER_MILE_SALE_RATE = 15;		// We refund customers this much per mile

	// Session timeout
	const SESSION_TIMEOUT = 60;	
 // This encryption is used to encrypt the user and group owner mail sending
    // Define a 32-byte (64 character) hexadecimal encryption key
// Note: The same encryption key used to encrypt the data must be used to decrypt the data
    const ENCRYPTION_KEY = 'd0a7e7997b6d5fca1b2c3d4f5611b87cd923e88837b63bf2941ef819dc8ca282';											// minutes
}
?>