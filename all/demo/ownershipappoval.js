function approveMemeberToOwner()
{
    if(document.getElementById('approve').checked){
        var approve_status='y';
    }else{
       var approve_status='n';
    }
    var parameter = getUrlParameters("approveownerparam", "", false);
    CConnection.doPost('ownershipappoval.php', {op: 'owner_approve_mail', url_parameter: parameter,approve_status:approve_status}, _response, null, 'approveForm');
    function _response(o)
    {
        new YUI_DIALOG_Popup(o['infoText'], 'Ownership management' , 'info', 'OK', _redirect);

        function _redirect()
        {
            CCommon.redirect('register.php');
        }

    }
}

function getUrlParameters(parameter, staticURL, decode) {

    var currLocation = (staticURL.length) ? staticURL : window.location.search,
            parArr = currLocation.split("?")[1].split("&"),
            returnBool = true;

    for (var i = 0; i < parArr.length; i++) {
        parr = parArr[i].split("=");
        if (parr[0] == parameter) {
            return (decode) ? decodeURIComponent(parr[1]) : parr[1];
            returnBool = true;
        } else {
            returnBool = false;
        }
    }

    if (!returnBool)
        return false;
}