var Y, yuiLoaded = false;
var _googleMap;
var _requestType;

var ARRANGE_TRAVEL =
{
	_msgList: null,				// A CMsgList object
	_schedule: null,				// Schedule details
	_refer: null				// CUrl object containing referral URL
}

/*
 * "onload" event for <body> element
 */

function bodyOnload ()
{
	_googleMap = new CGoogleMap();
	YAHOO_Require(['container', 'dom', 'calendar', 'menu', 'json', 'connection',
	               'autocomplete', 'button'], _yuiLoaded);
}

/*
 * Callback executed when YUI modules loaded
 */

function _yuiLoaded ()
{
	yuiLoaded = true;
	if (typeof TIME != "undefined")
		TIME._createMenu();
	//new YAHOO.widget.Button('createSchedule', {type: 'submit', label: ARRANGE_TRAVEL._msgList.msg('Create Journey')});
        new YAHOO.widget.Button('createSchedule', {type: 'submit', label:'Create Journey'});
	_requestType = new YAHOO.widget.Button("requestType", { type: "menu", menu: "requestTypeSelect" });
	_requestType.getMenu().subscribe('click', _requestTypeOnClick);
	_requestType.set('label', 'Please Select');
	_requestType.value = 'u';
	_loadSchedule(ARRANGE_TRAVEL._schedule);
}

/*
 * Load schedule details into page
 */

function _loadSchedule ()
{
	if (ARRANGE_TRAVEL._schedule.request_type == 's') {
		_requestType.set('label', 'I am seeking a lift');
		_requestType.value = 's';
	} else if (ARRANGE_TRAVEL._schedule.request_type == 'o') {
		_requestType.set('label', 'I am offering a lift');
		_requestType.value = 'o';
	} else if (ARRANGE_TRAVEL._schedule.request_type == 'e') {
		_requestType.set('label', 'I am offering to share a lift');
		_requestType.value = 'e';
	} else {
		_requestType.set('label', 'Please Select');
		_requestType.value = 'u';
	}

	if (ARRANGE_TRAVEL._schedule.schedule_type == 'o')
	{
		DOM_getElem('oneOffJourney').checked = true;
		_oneOffScheduleSelected();
	}
	else if (ARRANGE_TRAVEL._schedule.schedule_type == 'r')
	{
		DOM_getElem('regularJourney').checked = true;
		_regularScheduleSelected();
	}
	if (ARRANGE_TRAVEL._schedule.oneoff_date)
		_dateSelected ('_oneOffDate', ARRANGE_TRAVEL._schedule.oneoff_date.year, 
					   ARRANGE_TRAVEL._schedule.oneoff_date.month,
					   ARRANGE_TRAVEL._schedule.oneoff_date.day);
	if (ARRANGE_TRAVEL._schedule.oneoff_time)
		_timeSelected ('_oneOffTime', hour, minutes);
	if (ARRANGE_TRAVEL._schedule.from_location)
	{
		var elem = DOM_getElem('startAddress');
		elem.value = ARRANGE_TRAVEL._schedule.from_location;
		_reverseGeoLocation(elem);
	}
	if (ARRANGE_TRAVEL._schedule.to_location)
	{
		var elem = DOM_getElem('destination');
		elem.value = ARRANGE_TRAVEL._schedule.to_location;
		_reverseGeoLocation(elem);
	}
}

/*
 * Sign out
 */

function signOut ()
{
	var frm = document.getElementById("mainForm");
	frm.action = "index.php";
	frm.op.value = "signout";
	frm.submit();
}

///
/// "onblur" event for a location edit field
///

function locationOnBlur (e)
{
	e = CXPlat.eventObj(e);
	var src = CXPlat.eventSrcElement(e);
	if (src.value.length)
		_reverseGeoLocation(src);
}

/*
 * Reverse geotag a location
 * 
 * @param elem   DOM element containing address. THe 'name' attribute
 *               must be set.
 */

function _reverseGeoLocation (elem)
{
	var frm = DOM_getElem('mainForm');
	var tmp = DOM_getElem(elem.name + 'Lat');
	if (tmp)
		frm.removeChild(tmp);
	var tmp = DOM_getElem(elem.name + 'Lng');
	if (tmp)
		frm.removeChild(tmp);
	
	_googleMap.geocode(elem.value, _response);

	function _response (response)
	{
		if (response.length)
		{
			elem.value = response[0].label;
			var lat = response[0].lat;
			var lng = response[0].lng;
			COMMON.createSubmitElem("mainForm", elem.name + "Lat", lat);
			COMMON.createSubmitElem("mainForm", elem.name + "Lng", lng);
		}
	}
}

/*
 * "onclick" event
 * 
 * @param e   DOM event object
 */

function journeyTypeOnClick (e)
{
	e = CXPlat.eventObj(e);
	var src = CXPlat.eventSrcElement(e);
	if (src.value == "r")
		_regularScheduleSelected();
	else if (src.value == "o")
		_oneOffScheduleSelected();
}

/*
 * "One Off" schedule type selected
 */

function _oneOffScheduleSelected ()
{
	YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("regularOptions"), "display", "none");
	YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("oneOffOptions"), "display", "block");
	YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("customSchedule"), "display", "none");
	YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("standardSchedule"), "display", "none");
}

/*
 * "Regular" schedule type selected
 */

function _regularScheduleSelected ()
{
	YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("regularOptions"), "display", "block");
	YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("oneOffOptions"), "display", "none");
	var elems = document.getElementsByName("scheduleType");
	for (key in elems)
		if (elems[key].value == "s")
			YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("standardSchedule"), 
									"display", (elems[key].checked ? "block" : "none"));
		else if (elems[key].value == "c")
			YAHOO.util.Dom.setStyle(YAHOO.util.Dom.getElementsByClassName("customSchedule"), 
									"display", (elems[key].checked ? "block" : "none"));
}

/*
 * "onclick" event
 * 
 * @param e   DOM event object
 */

function scheduleTypeOnClick (e)
{
	e = CXPlat.eventObj(e);
	var src = CXPlat.eventSrcElement(e);
	var styleAttr = (src.value == "s" ? "block" : "none");
	var elems = YAHOO.util.Dom.getElementsByClassName("standardSchedule");
	YAHOO.util.Dom.setStyle(elems, "display", styleAttr);
	styleAttr = (src.value == "c" ? "block" : "none");
	elems = YAHOO.util.Dom.getElementsByClassName("customSchedule");
	YAHOO.util.Dom.setStyle(elems, "display", styleAttr);
}

/*
 * Select a date
 * 
 * @param focusId  Element id. starting with a _ character. A seperate element
 *                 to contain the date selection is created without the _ so for example
 *                 an element id of '_date1' would create a submit element 'date1'
 */

function selectDate (focusId)
{
	Calendar.instance({focusId: focusId, callback: _dateSelected, limitDate: true});
}

/*
 * Calendar callback
 * 
 * @param id      Focus id ( should be of the form '_date1' which will cause an equivalant
 *                hidden element called 'date1' to be created which holds the time as a
 *                JSON encoded object )
 * @param year    Year (nnnn)
 * @param month   Month (1-12)
 * @param day     Day (1-nn)
 */

function _dateSelected (id, year, month, day)
{
	var d = COMMON.toJson({year: year, month: month, day: day});
	CONNECTION_Get("scripts/utils.php", {op: "date", date: d}, _XmlHttpReply);
	
	function _XmlHttpReply (o)
	{
		if (o.status != 200)
			return;
		var response = COMMON.fromJson(o.responseText);
		var a = document.getElementById(id);
		COMMON.removeChildNodes(a);
		a.appendChild(document.createTextNode(response["result"]));
		COMMON.createSubmitElem("mainForm", id.substring(1, 99), encodeURIComponent(d));
	}
}

/*
 * Select a date
 * @param id  element id.
 */

function selectTime (id)
{
	TIME.showPicker(_timeSelected, id);
}

/*
 * Callback for time selection
 * 
 * @param id      Focus id ( should be of the form '_time1' which will cause an equivalant
 *                hidden element called 'time1' to be created which holds the time as a
 *                JSON encoded object )
 * @param hour    Hour portion of time (0 - 23)
 * @param minute  Minute portion of time (0 - 59)
 */

function _timeSelected (id, hour, minutes)
{
	var t = COMMON.toJson({hour: hour, minute: minutes, seconds: 0});
	CONNECTION_Get("scripts/utils.php", {op: "time", time: t}, _XmlHttpReply);
	
	function _XmlHttpReply (o)
	{
		if (o.status != 200)
			return;
		var response = COMMON.fromJson(o.responseText);
		var a = document.getElementById(id);
		COMMON.removeChildNodes(a);
		a.appendChild(document.createTextNode(response["result"]));
		COMMON.createSubmitElem("mainForm", id.substring(1,99), encodeURIComponent(t));
	}
}

/*
 * "onsubmit" event
 * 
 * @param e   DOM event object
 * @return    true of false to bubble or cancel the evennt
 */

function onSubmit (e)
{
	if (validate())
	{
		YUI_CONTAINER_Wait(ARRANGE_TRAVEL._msgList.msg(1019));
		CConnection.doPost('newjourney.php', {op: 'createSchedule'}, _response, null, 'mainForm');
                
	}
	
	function _response (r)
	{
		YUI_CONTAINER_KillWait();		
		if (r.errorId)
			new YUI_DIALOG_Popup({text: r.errorText, header: ARRANGE_TRAVEL._msgList.msg(1014),
								  icon: 'block', buttonText: ARRANGE_TRAVEL._msgList.msg(1007),
								  modal: true});
		else
		{
			function _refer ()
			{
				var url = ARRANGE_TRAVEL._refer.getUrl();
				CCommon.redirect(url == '' ? 'index.php' : url);
			}
			new YUI_DIALOG_Popup({text: ARRANGE_TRAVEL._msgList.msg(1018),
								  header: ARRANGE_TRAVEL._msgList.msg(1017),
								  icon: 'info', modal: true,
								  buttonText: ARRANGE_TRAVEL._msgList.msg(1007),
								  callback: _refer});
		}
	}
	
	return false;
}

/*
 * Validate input fields
 * 
 * @return true or false
 */

function validate ()
{
	var regularSchedule = DOM_getElem('regularJourney').checked;
	var oneOffSchedule = DOM_getElem('oneOffJourney').checked;
	
        
        
	// Must have a schedule type
	if (regularSchedule == false && oneOffSchedule == false)
	{
		
        COMMON.setError('journeyType', ARRANGE_TRAVEL._msgList.msg(1009));
		return false;
	}	

	// Must have date and time for one-off schedule
	var oneOffDate = DOM_getElem('oneOffDate');
	var oneOffTime = DOM_getElem('oneOffTime');
	if (oneOffSchedule && (oneOffDate == null || oneOffTime == null))
	{
		COMMON.setError('oneOffFields', ARRANGE_TRAVEL._msgList.msg(1010));
		return false;
	}	
	
	// Must have schedule type if regular schedule selected
	var standardSchedule = DOM_getElem('standardSchedule').checked;
	var customSchedule = DOM_getElem('customSchedule').checked;
	if (regularSchedule && standardSchedule == false && customSchedule == false)
	{
		COMMON.setError('scheduleType', ARRANGE_TRAVEL._msgList.msg(1009));
		return false;
	}	

	// Standard schedule selected
	var standardStartTime = DOM_getElem('standardStartTime');
	var standardReturnTime = DOM_getElem('standardReturnTime');
	if (regularSchedule && standardSchedule && (standardStartTime == null || standardReturnTime == null))
	{
		COMMON.setError('standardScheduleFields', ARRANGE_TRAVEL._msgList.msg(1010));
		return false;
	}	
	
	// Must have a source
	var startAddress = DOM_getElem('startAddress');
	if	(
		startAddress.value == ''
		|| document.getElementById('startAddressLat') == null
		|| document.getElementById('startAddressLng') == null
		)
	{
		startAddress.focus();
		COMMON.setError('startAddressTd', ARRANGE_TRAVEL._msgList.msg(1006));
		return false;
	}
	
	// Must have a desinstation
	var destination = DOM_getElem('destination');
	if	(
		destination.value == ''
		|| document.getElementById('destinationLat') == null
		|| document.getElementById('destinationLng') == null
		)
	{
		destination.focus();
		COMMON.setError('destinationTd', ARRANGE_TRAVEL._msgList.msg(1008));
		return false;
	}
        
	
	COMMON.createSubmitElem('mainForm', 'requestType', _requestType.value);
	return true;
}

/*
 * "click" event handlers
 */

function _placesOnClick (p_sType, p_aArgs)
{ 
	var oEvent = p_aArgs[0],    //  DOM event 
	oMenuItem = p_aArgs[1]; 	//  MenuItem instance that was the target of the event 
	if (oMenuItem)
		_places.set('label', oMenuItem.cfg.getProperty('text'));
}

function _requestTypeOnClick (p_sType, p_aArgs)
{ 
	var oEvent = p_aArgs[0],    //  DOM event 
	oMenuItem = p_aArgs[1]; 	//  MenuItem instance that was the target of the event 
	if (oMenuItem) {
		_requestType.set('label', oMenuItem.cfg.getProperty('text'));
		_requestType.value = oMenuItem.value;
	}
}
