<?php
require_once('base.inc');
require_once('classes/journey.inc');
require_once('classes/region.inc');


// Validate any current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();

// Confirm the request
$v = CCommon::getRequestValue('v');
$v = CCommon::fromJson(base64_decode($v));
$journeyId = $v->journeyId;
$requestKey = $v->requestKey;
$requestId = $v->requestId;
$region = new CRegion('confirmrequest');
$rplc = array();
$rplc[1] = ($journeyId ? acceptRequest($requestId, $requestKey, $region) : $region->msg(1010));
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1100);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace('confirmrequest.html', $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
	@include 'google_analytics.html';

/*
 * Accept ride request
 * 
 * @param $requestId   Request id.
 * @param $requestKey  Request verification key
 * @param $region      A CRegion object
 * @return             Confirmation text
 */

function acceptRequest ($requestId, $requestKey, $region)
{
	// Load the request details
	$journeyRequest = new CJourneyRequest;
	if ($journeyRequest->load($requestId) == false)
	{
		CLogging::error(sprintf('CJourney::acceptRequest - cannot load journey request %ld',
								$requestId));
		return $region->msg(1000);
	}
	$userId = $journeyRequest->get(CJourneyRequest::USER_ID);
	$journeyId = $journeyRequest->get(CJourneyUser::JOURNEY_ID);
		
	// Check if request keys match
	if ($requestKey != $journeyRequest->get(CJourneyRequest::UNIQUE_ID))
	{
		CLogging::error(sprintf('CJourney::acceptRequest - key mismatch [%s][%s]',
								$requestKey, $journeyRequest->get(CJourneyRequest::UNIQUE_ID)));
		return $region->msg(1001);
	}

	// Indicate the journey request is responded to
	if ($journeyRequest->responded() == false)
	{
		CJourneyUser::remove($journeyId, $userId);
		return $region->msg(1000);
	}
	
	// Check if user already assigned
	$journeyUsers = CJourneyUserDb::loadByJourneyId($journeyId);
	if ($journeyUsers === false)
		return false;
	foreach ($journeyUsers as $journeyUser)
		if ($journeyUser->get(CJourneyUser::USER_ID) == $userId)
		{
			CLogging::error(sprintf('CJourney::acceptRequest - user %ld is already assigned to journey %ld',
									$userId, $journeyId));
		return $region->msg(1002);
		}

	// Load journey
	$journeys = CJourney::load($journeyId);
	if ($journeys === false || count($journeys) == 0)
	{
		CLogging::error(sprintf('CJourney::acceptRequest - cannot load journey %ld',
								$journeyId));
		return $region->msg(1000);
	}

	// Cant assign youself to your own journey
	if ($userId == $journeys[0]->get(CJourney::USER_ID))
	{
		CLogging::error(sprintf('CJourney::acceptRequest - cannot assign yourself to journey %ld',
								$journeyId));
		return $region->msg(1003);
	}

	// Save the journey user
	$journeyUser = new CJourneyUser;
	$journeyUser->set(CJourneyUser::JOURNEY_ID, $journeyId);
	$journeyUser->set(CJourneyUser::USER_ID, $userId);
	if ($journeyUser->add() == false)
	{
		CLogging::error(sprintf('CJourney::acceptRequest - cannot add user to journey %ld users list',
								$journeyId));
		return $region->msg(1000);
	}
		
	// Carpool active?
	$carpoolId = $journeys[0]->get(CJourney::CARPOOL_ID);
	if ($carpoolId)
	{
		// Get journey carpool
		$carpool = new CCarpool;
		if ($carpool->load($carpoolId) == false)
		{
			CJourneyUser::remove($journeyId, $userId);
			CLogging::error(sprintf('CJourney::acceptRequest - cannot load carpool %ld',
									$carpoolId));
			return $region->msg(1000);
		}
	
		// Get users currently on this journey
		$journeyUsers = CJourneyUser::loadByJourneyId($journeyId);
		if ($journeyUsers === false)
		{
			CJourneyUser::remove($journeyId, $userId);
			CLogging::error(sprintf('CJourney::acceptRequest - cannot load users for journey %ld',
									$journeyId));
			return $region->msg(1000);
		}
		
		// Any space?
		if (count($journeyUsers) > $carpool->get(CCarpool::PLACES))
		{
			// Remove the user already added because no space available
			CJourneyUser::remove($journeyId, $userId);
			CLogging::info(sprintf('CJourney::acceptRequest - no places remaining in journey %ld',
									$journeyId));
			journeyFullMail($userId, $journeys[0], $region);
			return $region->msg(1004);
		}
	}
	
	// Send message
	confirmedMail($userId, $journeys[0], $region);
	
	return $region->msg(1005);
}

/*
 * Send journey confirmation mail
 * 
 * @param $userId   User id to mail
 * @param $journey  A CJourney object
 * @param $regio    A CRegion object
 */

function confirmedMail ($userId, $journey, $region)
{
	// Send message
	$toUser = new CUser;
	$toUser->loadWithUserId($userId);
	$fromUser = new CUser;
	$fromUser->loadWithUserId($journey->get(CJourney::USER_ID));
	$rplc = array();
	$rplc[1] = $fromUser->getFullName();
	$rplc[2] = $journey->get(CJourney::START_ADDRESS);
	$rplc[3] = $journey->get(CJourney::DESTINATION_ADDRESS);
	/* rplc4 for html format message, rplc5 for txt format */
	/* msg1009 for facebook contact, msg1011 for email contact */
	if ($fromUser->get(CUser::FACEBOOK_ID) != '') {
		$rplc[4] = str_replace('%1', sprintf('<a href="%s/profile.php?id=%s">%s/profile.php?id=%s</a>', CConfig::FB_HOME_URL, $fromUser->get(CUser::FACEBOOK_ID), CConfig::FB_HOME_URL, $fromUser->get(CUser::FACEBOOK_ID)), $region->msg(1009));
		$rplc[5] = str_replace('%1', sprintf('%s/profile.php?id=%s', CConfig::FB_HOME_URL, $fromUser->get(CUser::FACEBOOK_ID)), $region->msg(1009));
	} else {
		$rplc[4] = str_replace('%1', sprintf('<a href="mailto:%s">%s</a>', $fromUser->get(CUser::EMAIL), $fromUser->get(CUser::EMAIL)), $region->msg(1011));
		$rplc[5] = str_replace('%1', $fromUser->get(CUser::EMAIL), $region->msg(1011));
	}
	$rplc[8] = $region->msg(8, 'common');
	$cfg = new stdClass;
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->to = $toUser->get(CUser::EMAIL);
	$cfg->toName = $toUser->getFullName();
	$cfg->subject = $region->msg(1007);
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('confirmrequestfbmail.html'), $rplc);
	$cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile('confirmrequestfbmail.txt'), $rplc, false);
	CCommon::sendMail($cfg);
}

/*
 * Send mail informing requested journey is full
 * 
 * @param $userId   User id to mail
 * @param $journey  A CJourney object
 * @param $regio    A CRegion object
 */

function journeyFullMail ($userId, $journey, $region)
{
	// Send message
	$toUser = new CUser;
	$toUser->loadWithUserId($userId);
	$fromUser = new CUser;
	$fromUser->loadWithUserId($journey->get(CJourney::USER_ID));
	$rplc = array();
	$rplc[1] = $fromUser->getFullName();
	$rplc[2] = $journey->get(CJourney::START_ADDRESS);
	$rplc[3] = $journey->get(CJourney::DESTINATION_ADDRESS);
	/* rplc4 for html format message, rplc5 for txt format */
	/* msg1009 for facebook contact, msg1011 for email contact */
	if ($fromUser->get(CUser::FACEBOOK_ID) != '') {
		$rplc[4] = str_replace('%1', sprintf('<a href="%s/profile.php?id=%s">%s/profile.php?id=%s</a>', CConfig::FB_HOME_URL, $fromUser->get(CUser::FACEBOOK_ID), CConfig::FB_HOME_URL, $fromUser->get(CUser::FACEBOOK_ID)), $region->msg(1009));
		$rplc[5] = str_replace('%1', sprintf('%s/profile.php?id=%s', CConfig::FB_HOME_URL, $fromUser->get(CUser::FACEBOOK_ID)), $region->msg(1009));
	} else {
		$rplc[4] = str_replace('%1', sprintf('<a href="mailto:%s">%s</a>', $fromUser->get(CUser::EMAIL), $fromUser->get(CUser::EMAIL)), $region->msg(1011));
		$rplc[5] = str_replace('%1', $fromUser->get(CUser::EMAIL), $region->msg(1011));
	}
	$rplc[8] = $region->msg(8, 'common');
	$cfg = new stdClass;
	$cfg->from = CConfig::INFO_EMAIL;
	$cfg->fromName = CConfig::INFO_EMAIL_NAME;
	$cfg->to = $toUser->get(CUser::EMAIL);
	$cfg->toName = $toUser->getFullName();
	$cfg->subject = $region->msg(1008);
	$cfg->body = CCommon::htmlReplace(CRegion::regionFile('journeyfullmail.html'), $rplc);
	$cfg->bodyText = CCommon::htmlReplace(CRegion::regionFile('journeyfullmail.txt'), $rplc, false);
	CCommon::sendMail($cfg);
}
?>
