///
/// XHR class 
///

var XHR =
        {
            __obj: null,
            __code200Func: null,
            //
            // Return callback
            //

            getCallback: function()
            {
                return this.__code200Func;
            },
            //
            // Initialise
            //

            __init: function()
            {
                if (this.__obj)
                    return;

                try
                {
                    this.__obj = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e)
                {
                    try
                    {
                        this.__obj = new XMLHttpRequest();
                    }
                    catch (ee)
                    {
                    }
                }
            },
            //
            // Member reference for the XMLHttpRequest object:-
            //
            // onreadystatechange - An event handler for an event that fires
            // at every state change. Takes one parameter which is a reference
            // to a XMLHttpRequest object.
            //
            // readyState    - Returns the state of the object:
            //
            // 	0 = uninitialized
            // 	1 = loading
            // 	2 = loaded
            // 	3 = interactive
            // 	4 = complete
            //
            // 	responseText - Returns the response as a string
            //
            // 	responseXML  - Returns the response as XML. This property returns
            //                 an XML document object, which can be examined and
            //                 parsed using W3C DOM node tree methods and
            //                 properties
            //
            // 	status       - Returns the status as a number (e.g. 404 for "Not
            //                 Found" or 200 for "OK")
            //
            // 	statusText   - Returns the status as a string (e.g. "Not Found"
            //                 or "OK")
            //

            ///
            /// XmlHttpRequest GET
            /// @param[in] config - configuration options:-
            /// "url" - request url
            /// "callback" - [optional] the name of a function to call for asynchronous request. Takes single
            /// parameters of XmlHttpRequest object
            /// "params" - [optional] request parameters as object or keyed array
            /// "form" - [optional] a DOM form object or id. of a form to apply parameters from
            /// "fieldIds" - [optional] a list of element "id"s to submit 
            /// return XmlHttpRequest object
            ///

            doGET: function(config)
            {
                this.__init();

                if (typeof config.url == "undefined")
                    return false;

                var asyncReq = false;
                if (typeof config.callback != "undefined")
                {
                    this.__code200Func = config.callback;
                    this.__obj.onreadystatechange = __processReqChange;
                    asyncReq = true;
                }

                var pList = [];
                if (typeof config.params != "undefined")
                    for (key in config.params)
                        pList.push(key + "=" + config.params[key]);

                if (typeof config.form != "undefined")
                    _getFormVals((typeof config.form == "string"
                            ? document.getElementById(config.form)
                            : config.form), pList);

                if (typeof config.fieldIds != "undefined")
                    for (var i = 0; i < config.fieldIds.length; i++)
                    {
                        var tmp = document.getElementById(config.fieldIds[i]);
                        if (tmp.type == "checkbox")
                            pList.push(tmp.id + "=" + (tmp.checked ? "1" : "0"));
                        else
                            pList.push(tmp.id + "=" + tmp.value);
                    }

                var p = (pList.length ? "?" + pList.join("&") : "");
                this.__obj.open("GET", config.url + p, asyncReq);
                this.__obj.send(null);

                return this.__obj;
            },
            ///
            /// XmlHttpRequest GET
            /// "url" - request url
            /// "callback" - [optional] the name of a function to call for asynchronous request. Takes single
            /// parameters of XmlHttpRequest object
            /// "params" - [optional] request parameters as object or keyed array
            /// "form" - [optional] a DOM form object or id. of a form to apply parameters from
            /// "fieldIds" - [optional] a list of element "id"s to submit 
            /// return XmlHttpRequest object
            ///

            doPOST: function(config)
            {
                this.__init();

                if (typeof config.url == "undefined")
                    return false;

                var asyncReq = false;
                if (typeof config.callback != "undefined")
                {
                    this.__code200Func = config.callback;
                    this.__obj.onreadystatechange = __processReqChange;
                    asyncReq = true;
                }

                var pList = [];
                if (typeof config.params != "undefined")
                    for (key in config.params)
                        pList.push(key + "=" + config.params[key]);

                if (typeof config.form != "undefined")
                    _getFormVals((typeof config.form == "string"
                            ? document.getElementById(config.form)
                            : config.form), pList);

                if (typeof config.fieldIds != "undefined")
                    for (var i = 0; i < config.fieldIds.length; i++)
                    {
                        var tmp = document.getElementById(config.fieldIds[i]);
                        if (tmp.type == "checkbox")
                            pList.push(tmp.id + "=" + (tmp.checked ? "1" : "0"));
                        else
                            pList.push(tmp.id + "=" + tmp.value);
                    }

                this.__obj.open("POST", config.url, asyncReq);
                this.__obj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                this.__obj.send(pList.join("&"));

                return this.__obj;
            }
        }

/*
 * Create array list from <form> object
 * 
 * @param pList   List array to fill (xxx=yy,xxx=yyy,...)
 * @param form    Form object to mine
 */

function _getFormVals(form, pList)
{
    for (var i = 0; i < form.elements.length; i++)
        if (form.elements[i].type == "checkbox")
            pList.push(form.elements[i].name + "=" + (form.elements[i].checked ? "1" : "0"));
        else if (form.elements[i].type == "radio")
        {
            if (form.elements[i].checked)
                pList.push(form.elements[i].value);
        }
        else
            pList.push(form.elements[i].name + "=" + form.elements[i].value);

}

///
/// Callback for XmlHttpRequest object
/// Scope is within the XmlHttpRequest
///

function __processReqChange()
{
    if (this.readyState == 4)
    {
        // only if "OK"
        if (this.status == 200)
        {
            callback = XHR.getCallback();
            if (callback)
                callback(this);
        }
        else
        {
            alert("There was a problem retrieving the XML data:\n" + this.statusText);
        }
    }
}
var upload = function(data, imageType, userColumn, fileInput,ext) {
    var request = new XMLHttpRequest();
    if (typeof XDomainRequest != "undefined") {
        request = new XDomainRequest();
    }
    request.onreadystatechange = function() {
        if (request.readyState === 4) {
            try {
                var resp = COMMON.fromJson(request.response);
            } catch (e) {
                var resp = {
                    infoText: request.responseText
                };

            }
            if (resp['infoText'] == 'Invalid or expired session specified' ){
                window.open('login.php','self');
            }
            else {
            if (resp['infoText'] != 'Uploaded')
            {
                document.getElementById(fileInput + '_error').innerHTML = resp['infoText'];
            }
            else {

                var ext=resp['extensions'];
                document.getElementById(fileInput + '_error').innerHTML = "";
		DOM_setStyle(imageType, 'display', 'block');
                YAHOO.util.Dom.setAttribute(imageType, "src", '');
                if(ext=="pdf" || ext == "PDF" || ext== "Pdf")
                    YAHOO.util.Dom.setAttribute(imageType, "src", 'images/pdf.jpg');
                //else
                    //YAHOO.util.Dom.setAttribute(imageType, "src", 'upload/' + _sessionid + '/' + userColumn);
                if(imageType=='target'){
$('#target').attr('src', '');
console.log('cc');
$("#preview-pane").html('');
$("#preview-pane").empty();                 
                 var image_tag='upload/' + _sessionid + '/' + ext; 

                 $("#crop_img_original").val(ext);
$('#target').attr('src', '');
            $("#preview-pane").html('<img src="'+image_tag+'?temp='+Math.floor(Math.random()*1000)+'" id="target" class="'+Math.floor(Math.random()*1000)+'">'); 


//$('#target').attr('src', image_tag);
             callCropFunction(); 
                }
else if(imageType=='profile_photo_id' || imageType=='profile_license')
YAHOO.util.Dom.setAttribute(imageType, "src", 'upload/' + _sessionid + '/' + ext+'?temp='+Math.floor(Math.random()*1000));
            }

		                
                DOM_setStyle(fileInput + '_div', 'display', 'block');
            DOM_setStyle('upload_progress_' + fileInput, 'display', 'none');
            //YAHOO.util.Dom.setAttribute(imageType, "src", 'upload/' + _sessionid + '/' + userColumn);

            switch (fileInput) {
                case "photo_id":
                    _uploadStatusPhotoId = 0;
                    break;
                case "photo":
                    _uploadStatusPhoto = 0;
                    break;
              
                case "driving_license_image":
                    _uploadStatusLicense = 0;
                    break;
            }
            if (_uploadStatusPhotoId == 0 && _uploadStatusLicense == 0
                    && _uploadStatusBirthCert == 0  && _uploadStatusPhoto == 0) {

                DOM_getElem('save-button').disabled = false;
            }
            else
                DOM_getElem('save-button').disabled = true;
        }
        }
    };

    request.open('POST', 'register.php?op=upload_file&anti_cache='+Math.floor(Math.random()*1000));
    request.send(data);
};
