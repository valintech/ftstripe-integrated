/*
 * Get 1 or more DOM elements
 * 
 * @param elem  Element id, reference or array of ids.
 * 
 * @reuturn element or array of elements
 */

function DOM_getElem (elem)
{
	return YAHOO.util.Dom.get(elem);
}

/*
 * Get list of elements using callback filter
 * 
 * @param method  Callback filter
 * @param tag     [optional] HTML tag to filter by
 * @param root    [optional] Id. or element to use as base
 * @return        Array of DOM elements found
 */

function DOM_getElemsBy (method, tag, root)
{
	return YAHOO.util.Dom.getElementsBy(method, tag, root);
}

/*
 * Insert a DOM node after another
 * 
 * @param newNode  New node
 * @param refNode  Reference node
 */

function DOM_insertAfter (newNode, refNode)
{
	YAHOO.util.Dom.insertAfter(newNode, refNode);
}

/*
 * Set style for 1 or more elements
 * 
 * @param elem  Element id, reference or array of ids.
 * @param prop  CSS property name
 * @param val   CSS property value
 */

function DOM_setStyle (elem, prop, val)
{
	YAHOO.util.Dom.setStyle(elem, prop, val);
}

/*
 * Set attribute for 1 or more elements
 * 
 * @param elem  Element id, reference or array of ids.
 * @param prop  Attribute name
 * @param val   Attribute value
 */

function DOM_setAttribute (elem, prop, val)
{
	if (val == null)
		val = '';
	YAHOO.util.Dom.setAttribute(elem, prop, val);
}

/*
 * Remove CSS class from 1 or more elements
 * 
 * @param elem        Element id, reference or array of ids.
 * @param className   Name of class to remove
 */

function DOM_removeClass (elem, className)
{
	YAHOO.util.Dom.removeClass(elem, className);
}

/*
 * Add CSS class from 1 or more elements
 * 
 * @param elem        Element id, reference or array of ids.
 * @param className   Name of class to add
 */

function DOM_addClass (elem, className)
{
	YAHOO.util.Dom.addClass(elem, className);
}
