/*
 * Wrapper for YUI "autocomplete" object
 * 
*/

function Autocomplete (id, values)
{
	this._create(id, values);
}

/*
 * Instance and class members
 */

Autocomplete.prototype.id = null;
Autocomplete.prototype.btn = null;
Autocomplete.prototype.ac = null;
Autocomplete.prototype.ds = null;
Autocomplete._autocompleteList = [];

/*
 * Create an auto-complete control from existing HTML markup
 * 
 * @param id       Id. of the autocomplete markup
 */

Autocomplete.createFromMarkup = function (id)
{
    var tmp = DOM_getElem(id + '_values');
    var values = COMMON.fromJson(decodeURIComponent(tmp.value));
	var o = new Autocomplete(id, values);
	Autocomplete._autocompleteList[id] = o;
	return o;
};

/*
 * Create an auto-complete control from an existing input text box
 * 
 * @param id       Id. of the input text box
 * @param values   Array of values for the autocomplete data source
 * @param value    [optional] Initial value of text box
 * @param zIndex   [optional] CSS z-Index layer
 */

Autocomplete.createFromInput = function (id, values, value, zIndex)
{
	var divOuter = document.createElement('div');
	DOM_setAttribute(divOuter, 'id', 'YUI_AC_%1'.format(id));
	if (zIndex)
		DOM_setStyle(divOuter, 'zIndex', zIndex);
	var inp = DOM_getElem(id);
	var insertPos = inp.previousSibling;
	var insertParent = inp.parentNode;
	inp.parentNode.removeChild(inp);
	DOM_setAttribute(inp, 'name', id);
	if (value)
		DOM_setAttribute(inp, 'value', value);
	divOuter.appendChild(inp);
	var span = document.createElement('span');
	DOM_setAttribute(span, 'id', '%1_btn'.format(id));
	divOuter.appendChild(span);
	var divInner = document.createElement('div');
	DOM_setAttribute(divInner, 'id', '%1_list'.format(id));
	divOuter.appendChild(divInner);
	if (insertPos)
		DOM_insertAfter(divOuter, insertPos);
	else
		insertParent.appendChild(divOuter);
	var o = new Autocomplete(id, values);
	Autocomplete._autocompleteList[id] = o;
	return o;
};

/*
 * Core method to create a YUI Autocomplete control
 * 
 * @param id       Id. of the input text box
 * @param values   Array of values for the autocomplete data source
 */

Autocomplete.prototype._create = function (id, values)
{
	this.id = id;
	this.ds = new YAHOO.util.LocalDataSource(values);  
    var cfg = { prehighlightClassName: "yui-ac-prehighlight", useShadow: true, queryDelay: 0,
			minQueryLength: 0, animVert: false, maxResultsDisplayed: values.length }
    this.ac = new YAHOO.widget.AutoComplete(this.id, this.id+"_list", this.ds, cfg);
    // Create the associated button
    var btn = DOM_getElem( this.id+"_btn" );
    this.btn = new YAHOO.widget.Button( { container:btn } );
    this.btn.on( "mousedown", this.mousedown, null, this );
    this.ac.containerCollapseEvent.subscribe( function() { YAHOO.util.Dom.removeClass( btn, "open" ); } );
};

/*
 * YUI event - "mousedown"
 * 
 * @param e - YUI event object
 */

Autocomplete.prototype.mousedown = function (e)
{
	var btn = DOM_getElem( this.id+"_btn" );
	if( !YAHOO.util.Dom.hasClass( btn, "open" ) )
		YAHOO.util.Dom.addClass( btn, "open" );
  
	// Is open
	if( this.ac.isContainerOpen() )
		this.ac.collapseContainer();
	// Is closed
	else
	{
		this.ac.getInputEl().focus(); // Needed to keep widget active
		var ac = this.ac;
		setTimeout( function() { /* For IE */ ac.sendQuery(""); }, 0 );
	}
};

/*
 * Set the options list for an autocomplete
 * 
 * @param list   Array of options to set
 */

Autocomplete.prototype.setList = function (options)
{
	this.ds.liveData = options;
};

Autocomplete.prototype.setText = function (text)
{
	DOM_setAttribute(this.id, 'value', text);
};

/*
 * Return id
 * 
 * @return id.
 */

Autocomplete.prototype.getId = function ()
{
	return this.id;
};

/*
 * Create all autocomplete combo boxes
 */

Autocomplete.createAll = function ()
{
	var nodes = DOM_getElemsBy(_callback, 'div')
	
	function _callback (elem)
	{
		return elem.id.startsWith('YUI_AC_');
	}
	
	for (var i = 0; i < nodes.length; i++)
	{
		var id = nodes[i].id.substring('YUI_AC_'.length, 99);
		Autocomplete.createFromMarkup(id);
	}
};

/*
 * Get the object for a created Autocomplete widget
 * 
 * @param id   Autocomplete id.
 * @return     An "Autocomplete" object or null if
 * 			   non found.
 */

Autocomplete.getObj = function (id)
{
	return Autocomplete._autocompleteList[id]
		   ? Autocomplete._autocompleteList[id] : null;
};
