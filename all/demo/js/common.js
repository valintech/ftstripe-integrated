/*
 * Collection of common methods
 */

function CCommon ()
{
}

/*
 * ============================================================
 * Class data
 * ============================================================
 */

CCommon._cookies;			// Array of objects containiner "key", "value" members

/*
 * ============================================================
 * Class methods
 * ============================================================
 */

/*
 * Parse cookie string
 */

CCommon._parseCookieString = function ()
{
	if (CCommon._cookies)
		return;
	CCommon._cookies = [];
	var tmp = document.cookie;
	if (tmp == '')
		return;
	tmp = tmp.split('; ');
	for (var i = 0; i < tmp.length; i++)
	{
		var tmp1 = tmp[i].split('=');
		CCommon._cookies[tmp1[0]] = tmp1[1];
	}
};

/*
 * Get cookie value
 * 
 * @param key   Key for cookie pair "key=value"
 * @return      Value or null if not found
 */

CCommon.getCookie = function (key)
{
	CCommon._parseCookieString();
	var out = CCommon._cookies[key];
	return CCommon._cookies[key] ? CCommon._cookies[key] : null;
};

/*
 * Set cookie value
 * 
 * @param key    Key for cookie pair "key=value"
 * @param value  Value for cookie pair "key=value"
 */

CCommon.setCookie = function (key, value)
{
	// Set expiry to now + 1 year
	var d = new Date();
	d.setTime(d.getTime() + (((60 * 60 * 1000) * 24) * 365));
	document.cookie = '%1=%2;expires=%3'.format(key, value, d.toGMTString());
};

CCommon.deleteCookie = function (key)
{
	CCommon._parseCookieString();
	if (CCommon._cookies[key])
	{
		delete CCommon._cookies[key];
		CCommon._storeCookies();
	}	
};

/*
 * Check password
 * 
 * @param pwd   Password to check
 * @return      true or false depending on whether the
 *              password conforms to length and content
 *              limitations.
 *              No content limitations at present..
 */

CCommon.checkPassword = function (pwd)
{
	// Must be at least 6 chars
	return (pwd.length >= 6);
};

/*
 * Redirect to new page
 * 
 * @param url   URL to link to
 */

CCommon.redirect = function (url)
{
	window.location.href = url;
};

/*
 * Add HTML node
 * 
 * @param dom      DOM document object
 * @param parent   Parent node
 * @param name     Node name
 * @param value    Node value
 */

CCommon.addChildNode = function (parent, name, value)
{
	var nodeList = parent.getElementsByTagName(name);
	if (nodeList.length)
		node = nodeList[0];
	else
	{
		node = document.createElement(name);
		parent.appendChild(node);
	}
	if (node.firstChild == null)
	{
		text = document.createTextNode("");
		node.appendChild(text);
	}
	node.firstChild.nodeValue = value;
	return node;
};

/*
 * Remove all child and descendant child nodes
 * 
 * @param node          Base node or node name
 * @param removeParent  [optional] true to also remove parent node
 */

CCommon.removeChildNodes = function (node, removeParent)
{
	if (typeof node == 'string')
		node = document.getElementById(node);
	if (typeof node.childNodes != "undefined")
		for (var i = node.childNodes.length; i--;)
		{
			if (typeof node.childNodes[i].childNodes != 'undefined')
				this.removeChildNodes(node.childNodes[i]);
			node.removeChild(node.childNodes[i]);
		}
	if (removeParent)
		node.parentNode.removeChild(node);
};

/*
 * Create a node and attach a text node
 * 
 * @param nodeName  Node to create i.e "td", "span" etc
 * @param text      Text to set into created node
 * @return          The created node
 */

CCommon.createNodeWithText = function (nodeName, text)
{
	var node = document.createElement(nodeName);
	node.appendChild(document.createTextNode(text));
	return node;
};

/*
 * Convert data to JSON format
 * Uses native JSON object if available otherwise YUI "json" module
 * 
 * @param data   Object or Array to convert
 * @return       JSON encoded data
 */

CCommon.toJson = function (data)
{
	var out;
	try {out = (typeof JSON != "undefined" ? JSON.stringify(data) : YAHOO.lang.JSON.stringify(data));}
	catch (e) {}
	return out;
};

/*
 * Convert data from JSON format
 * Uses native JSON object if available otherwise YUI "json" module
 * 
 * @param data   JSON encoded data to convert
 * @return       Object or Array
 */

CCommon.fromJson = function (data)
{
	var out;
	try {out = (typeof JSON != "undefined" ? JSON.parse(data) : YAHOO.lang.JSON.parse(data));}
	catch (e) {}
	return out;
};

/*
 * Create a submit element from a key/value pair
 * 
 * @param frm   Element reference or name of <form> to add element to
 * @param key   key
 * @param val   value
 */

CCommon.createSubmitElem = function (frm, elemName, elemVal)
{
	if (typeof frm == 'string')
		frm = document.getElementById(frm);
	var inp = frm.elements[elemName];
	if (inp == null)
	{
		inp = document.createElement('input');
		inp.type = 'hidden';
		inp.id = elemName;
		inp.name = elemName;
		frm.appendChild(inp);
	}
	inp.value = elemVal;
};

/*
 * Create an error element and insert into DOM
 * 
 * @param errorId    Id. of element to insert after
 * @param errorText  Error text
 */

CCommon.setError = function (errorId, errorText)
{
	var nodes = document.getElementsByTagName("div");
	for (var i = nodes.length; i--;)
		if (nodes[i].id == "ERROR")
			nodes[i].parentNode.removeChild(nodes[i]);
	var node = document.getElementById(errorId);
	var div = COMMON.addChildNode(node, "div", errorText);
	div.id = "ERROR";
	DOM_addClass(div, 'ERROR');
};

/*
 * Create an error element and insert into DOM
 * 
 * @param errorId    Id. of element to insert after
 * @param errorText  Error text
 */

CCommon.setError1 = function (elem, errorText)
{
	var nodes = document.getElementsByTagName("div");
	for (var i = nodes.length; i--;)
		if (nodes[i].id == "ERROR")
			nodes[i].parentNode.removeChild(nodes[i]);
	var div = COMMON.createNodeWithText('div', errorText);
	div.id = "ERROR";
	DOM_addClass(div, 'ERROR');
	DOM_insertAfter(div, elem);
};

/*
 * Get message
 * 
 * @para msgNo   Message number
 */

CCommon.msg = function (msgNo)
{
	if (typeof _msgList == 'string')
		_msgList = COMMON.fromJson(_msgList);
	var out = (typeof _msgList == 'undefined' ? null : _msgList['M' + msgNo]);
	if (out == null)
		out = '#M%1'.format(msgNo);
	return out;
};

/*
 * Singleton instance of CCommon
 */

var COMMON = CCommon;

/*
 * Method to add into standard Array class
 * 
 * @param value  The value to find
 * @return       Index 0-n or -1 if failed to find
 */

Array.prototype.find = function (value)
{
	for (var i=0; i < this.length; i++)
		if (this[i] == value)
			return i;
	return -1;	
};

/*
 * A "starts with" function associated to all String objects
 * 
 * @param str   String to check
 * @return      true of false
 */

String.prototype.startsWith = function(str)
{ 
    return (this.indexOf(str) === 0); 
};

/*
 * Format a string replacing %1, %2 etc
 */

String.prototype.format = function ()
{
	var out = this;
	for (var i = 0; i < arguments.length; i++)
		out = out.replace(new RegExp('%'+(i+1)), arguments[i]);
	return out;
};

/*
 * CUrl class
 */

function CUrl (json)
{
	CUrl.prototype._url;
	CUrl.prototype._params;
	
	/*
	 * Set object from JSON representation
	 * 
	 * @param json  JSON encoded object data
	 */
	
	CUrl.prototype.setAsJson = function (json)
	{
		var tmp = COMMON.fromJson(json);
		this._url = tmp._url;
		this._params = tmp._params;
	};

	/*
	 * Get full URL string
	 * 
	 * @return URL string http://xxx.yyy?a=1&b=2&c=3
	 */
	
	CUrl.prototype.getUrl = function ()
	{
		if (this._url == '')
			return '';
		var params = [];
		for (key in this._params)
			if (typeof key == 'string')
				params.push('%1=%2'.format(key, encodeURIComponent(this._params[key])));
		return this._url + '?' + params.join('&');
	};

	if (json)
		this.setAsJson(json);
	else
	{
		this._url = '';
		this._params = [];
	}
}

/*
 * Message list class
 * 
 * @param msgs  JSON encoced message list (string) or actual message
 * 				list (array)
 */

function CMsgList (msgs)
{
	CMsgList.prototype._msgList;
	
	this._msgList = (typeof msgs == 'string' ? COMMON.fromJson(msgs) : msgs);

	/*
	 * Get message
	 * 
	 * @param msgNo   Message number
	 */
	
	CMsgList.prototype.msg = function (msgNo)
	{
		var out = this._msgList['M' + msgNo];
		return out == null ? '#' + msgNo : out;
	}
}
