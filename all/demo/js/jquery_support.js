$(document).ready(function() {

    $('.SlectBox').SumoSelect();

    $(".hiddenrow").hide();
    $(".group-button").click(function() {
        $(".hiddenrow").fadeIn(1000);
    });
    $(".group-button-close").click(function() {
        $(".hiddenrow").fadeOut(1000);
    });
    var _searchedGroupMember = '';

    var _currentGroup = 0;
    $('#myGroups').on('change', function() {
        $("#group_member").css("display", "block");
                $(".myGroupsRowstable").css("display", "block");
        $("#remove-member").css("display", "block");


        $("#group_member_toadmin").css("display", "block");
        $("#member_toadmin").css("display", "block");

        _currentGroup = this.value;
        $("#search_group_member").tokenInput("clear");

    });


    function getData()
    {
        return "register.php?op=search_group_member&g=" + _currentGroup;
    }

    $("#demo-input-prevent-duplicates").change(function() {

        $('#search_group .token-input-input-token').insertAfter('#search_group .token-input-list li:last');
        // show_dropdown();
        $("#token-input-demo-input-prevent-duplicates").focus();
        // $('#search_group .token-input-list').attr('style', 'top:'+$("#search_group .token-input-list").offset().top + $("#search_group .token-input-list").outerHeight()+' !important');


        _searchedSelectedGroup =  $("#demo-input-prevent-duplicates").val();
    });
    
     if (_groups != '0') {
        _groups = COMMON.fromJson(_groups);
        var searches = new Array();
        var j=0;
        if (typeof _groups.group_tag != 'undefined' && _groups.group_tag.length > 0) {
            while (i < _groups.group_tag.length) {
                if (_groups.type[i] === "s") {
                    var search = {};
                    search['id'] = parseInt(_groups.group_id[i]);
                    search['name'] = _groups.group_tag[i];
                    searches[j] = search;
                    _searchedSelectedGroup += ',' + _groups.group_id[i];
                    j++;
                }

                i++;
            }
        }
    }
    else
    {
        searches = "";
        }//console.log(JSON.stringify(searches));
    $("#demo-input-prevent-duplicates").tokenInput("register.php?op=search_group", {
        method: "POST", preventDuplicates: true,
        prePopulate: JSON.parse(JSON.stringify(searches))
    });

    $("#search_group_member").change(function() {

        $('#group_member .token-input-input-token').insertAfter('#group_member .token-input-list li:last');
        // show_dropdown();
        $("#token-input-search_group_member").focus();
        // $('#group_member .token-input-list').attr('style', 'top:'+$("#group_member .token-input-list").offset().top + $("#group_member .token-input-list").outerHeight()+' !important');

        _searchedGroupMember = $("#search_group_member").val();
        document.getElementById("searched_group_members").value = _searchedGroupMember;


    });


    $("#search_group_member").tokenInput(getData, {
        method: "POST", preventDuplicates: true
    });


    $("#search_group_memberforadmin").change(function() {
        $("#search_group_memberforadmin").tokenInput("remove", {id: _searchedGroupMember});
        $('#group_member_toadmin .token-input-input-token').insertAfter('#group_member_toadmin .token-input-list li:last');
        // show_dropdown();
        $("#token-input-search_group_memberforadmin").focus();
        // $('#group_member .token-input-list').attr('style', 'top:'+$("#group_member .token-input-list").offset().top + $("#group_member .token-input-list").outerHeight()+' !important');

        _searchedGroupMember = $("#search_group_memberforadmin").val();
        document.getElementById("searched_group_member_for_admin").value = _searchedGroupMember;


    });


    $("#search_group_memberforadmin").tokenInput(getData, {
        method: "POST", preventDuplicates: true
    });



});
function addOptions(o)
{
    $('.SlectBox').SumoSelect();
    var i = 0;
    _selectedGroups = "0";

    $('select.SlectBox')[0].sumo.unload();
    $('.SlectBox').SumoSelect();

    if (typeof o.group_id != 'undefined' && o.group_id.length > 0) {
        while (i < o.mail.length) {
            $('select.SlectBox')[0].sumo.add(o.group_id[i], o.group_tag[i], i);
            $('select.SlectBox')[0].sumo.selectItem(i);
            _selectedGroups += "," + o.group_id[i];
            i++;
        }
        $('.options li').click(function() {
            var val = $(this).attr('data-val');
            if (!$(this).hasClass('selected'))
            {
                _selectedGroups = _selectedGroups.replace("," + val, "");
            }
            else
            {
                var i = _selectedGroups.indexOf("," + val);
                if (i < 0)
                    _selectedGroups += "," + val;

            }


        });
    }


}

