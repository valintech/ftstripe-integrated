/*
 * Facebook class
 * 
 * @param cfg  Config object {isActive: <true|false>, appid: <...>}
 */

function CFacebook (cfg)
{
	CFacebook.FB_API_KEY = cfg.appId;
	CFacebook._isActive = (cfg.isActive ? true : false);

	/*
	 * Initialise Javascript API
	 */
	
	CFacebook.init = function ()
	{
		if (this.isActive())
			FB.init({appId: this.FB_API_KEY, status: true, cookie: true, xfbml: true});
	};
	
	/*
	 * Is Facebook active
	 * 
	 * @return true or false
	 */
	
	CFacebook.isActive = function ()
	{
		return this._isActive;
	};
	
	/*
	 * Reqeust permissions
	 * 
	 * @param perms   Array of permission ids.
	 */
	
	CFacebook.getPerm = function (perms)
	{
		function _complete (granted)
		{
		}
		FB.login(_complete, {perms: 'email'});		
	};
	
	/*
	 * Authorise Facebook application
	 * 
	 * @param url  URL to redirect to
	 */
	
	CFacebook.authorise = function (url)
	{
		var tmp = 'https://graph.facebook.com/oauth/authorize';
		tmp += '?client_id=' + this.FB_API_KEY;
		tmp += '&redirect_uri=' + url;
		tmp += '&scope=email';
		window.location.href = tmp; 
	};
}
