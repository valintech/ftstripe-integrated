/*
 * CCalendar class
 * 
 * @param cfg  Configuration object:-
 *             "focusElem" - Id. or element to align against
 *             "limitDate" - [optional] true to limit date selection
 *             to today onwards
 *             "callback" - [optional] callback method taking
 *             parameters "callback(focusId, year, month, day)" 
 */

function Calendar (cfg)
{
	Calendar.prototype._callback = cfg.callback;
	Calendar.prototype._focusId = cfg.focusId;
	Calendar.prototype_yuiCal;
	Calendar._yuiCalFactory;
	var div = document.createElement('div');
	div.style.position = 'absolute';
	div.style.display = 'none';
	document.body.appendChild(div);
	var tmp = {}
	this._yuiCal = new YAHOO.widget.Calendar(null, div,
			(cfg.limitDate ? {minDate: this._today()} : {})); 
	this._yuiCal.selectEvent.subscribe(this._selectEvent, this, true);
	this._yuiCal.render();
	this._yuiCal.show();
	YAHOO.util.Dom.setXY(div, YAHOO.util.Dom.getXY(cfg.focusId));	
}

/*
 * Create instance of Calendar object
 * 
 * @param cfg - See "Calendar" constructor
 */

Calendar.instance = function (cfg)
{
	if (Calendar._yuiCalFactory)
		Calendar._yuiCalFactory.destroy();
	Calendar._yuiCalFactory = new Calendar(cfg);
}

/*
 * "selectEvent" event handler
 */

Calendar.prototype._selectEvent = function (type, args, obj)
{
	if (this._callback)
	{
		var dates = args[0]; 
		var date = dates[0]; 
		var year = date[0], month = date[1], day = date[2];
		this._callback(this._focusId, year, month, day);
	}
	this._yuiCal.hide();
};

/*
 * Destroy this calendar object
 */

Calendar.prototype.destroy = function ()
{
	this._yuiCal.destroy();
	this._yuiCal = null;
}

/*
 * Return todays date as 'mm/dd/yyyy'
 * 
 * @return string
 */

Calendar.prototype._today = function ()
{
	var d = new Date();
	return '%1/%2/%3'.format(new String(100 + d.getMonth() + 1).substring(1, 99),
			new String(100 + d.getDate()).substring(1, 99),
			d.getFullYear());
};
