
var DataFormatVersion = 1;

var wsurl = "http://test.firsthumb.com/ws/query.php";
//var wsurl = "/ws/query.php";

var IdleTimeoutInSeconds = 1800;
var RefreshPeriodInSeconds = 5;
var msgTable;
var sessionId;
var signedIn = 0;
var active = '';
var IdleCounter = 0;
var RefreshCounter = 0;
var LastMsgIdReceived = 0;
var cur_page = '';
var username = '';
var cookieData;
var im_detail_contact = '';
var im_detail_group;

var monthNames = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

$(function() {
	var $menu = $('nav#menu');

	$menu.mmenu({
		//classes: 'mm-light'
	});

	$menu.find( 'li span' ).not( '.mm-subopen' ).not( '.mm-subclose' ).bind(
		'click.example',
		function( e )
		{
			var $option = $.trim( $(this).text() );
			e.preventDefault();
			$('#menu').trigger( 'close' );
			set_page($option);
			$(this).trigger( 'setSelected.mm' );
		}
	);
});

var last_page;

$.fn.mmenu.debug = function( msg ) {
    alert( msg );
};

$(document).ready(function(){
	set_page('Sign In');
	msgTable = document.getElementById("msgTable");
	setInterval("refresh()", 1000);
	document.getElementById('showViewport').innerHTML = "Viewport: " + window.innerWidth + "x" + window.innerHeight;
	//document.getElementById('username').value = 'pam@sleepie.demon.co.uk';
	//password.value = 'r1d3m3';
	set_page('Sign In');
	document.getElementById('signin').style.display = 'block';
	document.getElementById('menu_button').style.display = 'none';
	$('#refresh_button').hide();
	//$('#splash').height(window.innerHeight/2);
});

var entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;'
};

function escapeHtml(string) {
  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}

function set_page(page)
{
	var pages = {
		'Passenger'			: 'passenger',
		'Driver'				: 'driver',
		'Recent Journeys'	: 'recentjourneys',
		'Message Centre'	: 'messagecentre',
		'Settings'			: 'settings',
		'Debug'				: 'debug',
		'About'				: 'about',
		'Sign Out'			: 'signout'
	};

	if (active == 'driver' && page == 'Passenger')
		page = 'Driver';
	if (active == 'passenger' && page == 'Driver')
		page = 'Passenger';

	document.getElementById('signin').style.display = 'none';
	document.getElementById('menu_button').style.display = 'block';
	for (var txt in pages) {
		if (txt == page)
			document.getElementById(pages[txt]).style.display = 'block';
		else
			document.getElementById(pages[txt]).style.display = 'none';
	}
	last_page = page;
	$('#header2').show().text(page);
	if (page == 'Sign Out') {
		do_logout();
	} else if (page == 'Message Centre') {
		$('#messagecentredetail').hide();
		$('#messagecentresummary').show();
		render_im_list();
		im_detail_contact = '';
	} else if (page == 'Debug') {
		$('#debugTable').height(window.innerHeight - $('#debugTable').offset().top);
	} else if (page == 'Driver') {
		$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
	}
	cur_page = page;
}

function signin_button_pressed()
{
  // Try to log in
  IdleCounter = 0;
  username = document.getElementById("username").value;
  passwd = document.getElementById("password").value;

  str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "login", "params" : { "userId" : "' + username + '", "pwd" : "' + passwd + '" } }';
  debug(str);
  $.post(wsurl, 'q=' + str, function(response) { process_login_result(response)});
}

function load_cookie()
{
	var save = false;
	var value = localStorage.getItem(username);
	//console.log("Value: " + value);
	try {
	   cookieData = jQuery.parseJSON(unescape(value));
	} catch(e) {
	   console.log("Failed to parse '" + value + "'");
	}
	if (cookieData == null || cookieData == undefined) {
		cookieData = new Object;
		cookieData.DataFormatVersion = DataFormatVersion;
		save = true;
	}
	if (!cookieData.hasOwnProperty('DataFormatVersion') || cookieData.DataFormatVersion != DataFormatVersion) {
		cookieData = new Object;
		cookieData.DataFormatVersion = DataFormatVersion;
		save = true;
		alert("Data and settings deleted due to format change");
	}
	if (cookieData.hasOwnProperty('RefreshEnable')) {
	   document.getElementById('RefreshEnable').checked = cookieData.RefreshEnable;
		if (!cookieData.RefreshEnable)
			$('#refresh_button').show();
	} else {
		cookieData.RefreshEnable = true;
		save = true;
	}
	if (cookieData.hasOwnProperty('DebugEnable')) {
	   document.getElementById('debugEnable').checked = cookieData.DebugEnable;
	} else {
		cookieData.DebugEnable = false;
		save = true;
	}
	if (!cookieData.hasOwnProperty('Groups')) {
		cookieData.Groups = [];
		save = true;
	}
	if (!cookieData.hasOwnProperty('IMs')) {
		cookieData.IMs = {};
		save = true;
	}
	if (!cookieData.hasOwnProperty('IMExpiryDays')) {
		cookieData.IMExpiryDays = 5;
		save = true;
	}
	document.getElementById('im_expiry_days').value = cookieData.IMExpiryDays;
	if (save) {
		save_cookie();
	}
}

function save_cookie()
{
	var value = escape(JSON.stringify(cookieData, null, '\t'));
	//console.log("Value=" + value);
	localStorage.setItem(username, value);
}

function refresh_enable_clicked()
{
	cookieData.RefreshEnable = document.getElementById('RefreshEnable').checked;
	if (cookieData.RefreshEnable)
		$('#refresh_button').hide();
	else
		$('#refresh_button').show();
	save_cookie();
}

function debug_enable_clicked()
{
	cookieData.DebugEnable = document.getElementById('debugEnable').checked;
	save_cookie();
}

function process_login_result(response)
{
	debug(JSON.stringify(response, null, '\t'));
	document.getElementById("password").value = "";
	if (response.error !== null) {
	 	alert(response.error.message);
	} else {
		load_cookie();
		msgTable.innerHTML = "";
		sessionId = response.result.sessionId;
		signedIn = 1;
		// Make it look like a idle passenger until we know better
		active = '';
		document.getElementById("passengerActivePanel").style.display="none";
		$('#passenger_credit').show().text('???');
		$('#driver_credit').show().text('Credit: ??? miles');
		set_page("Passenger");
		get_status();
		get_messages();
	}
}

function do_logout()
{
	set_page('Sign In');
	document.getElementById('signin').style.display = 'block';
	document.getElementById('menu_button').style.display = 'none';
	str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "logout", "params" : { "sessionId" : "' + sessionId + '" } }';
	debug(str);
	$.post(wsurl, 'q=' + str, function(response) { process_logout(response)});
	$('#passengerPassphrase').show().text('');
	msgTable.innerHTML = "";
	sessionId = "";
	active = '';
	LastMsgIdReceived = 0;
	signedIn = 0;
}

function process_logout(response)
{
  debug(JSON.stringify(response, null, '\t'));
}

function refresh()
{
  if (signedIn == 1) {
    if (++IdleCounter > IdleTimeoutInSeconds) {
      do_logout();
    } else if (++RefreshCounter > RefreshPeriodInSeconds && RefreshEnable.checked == true) {
      RefreshCounter = 0;
      get_status();
      get_messages();
		get_group_list();
		get_ims();
    }
  }
}

function refresh_button_pressed()
{
  IdleCounter = 0;
  if (signedIn == 1) {
    get_status();
    get_messages();
		get_group_list();
		get_ims();
  }
}

function get_status()
{
  str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "status", "params" : { "sessionId" : "' + sessionId + '" } }';
  debug(str);
  $.post(wsurl, 'q=' + str, function(response) { process_status(response)});
}

function process_status(response)
{
  debug(JSON.stringify(response, null, '\t'));
  if (response.error != null) {
    alert("GetStatus: " + response.error.message);
		do_logout();
  } else if (signedIn == 0) {
    ;
  } else {
	 $('#passenger_credit').show().text(response.result.credit);
	 $('#driver_credit').show().text('Credit: ' + response.result.credit + ' miles');
    if (response.result.passengers == undefined && response.result.driver == undefined) {
      active = '';
    }
    if (response.result.passengers == undefined) {
      document.getElementById("driverActivePanel").style.display="none";
		$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
    } else {
		if (cur_page == 'Passenger') {
      	set_page('Driver');
		}
      active = 'driver';
      document.getElementById("driverActivePanel").style.display="block";
      if (response.result.passengers.length == 1) {
        document.getElementById("driverPassengers").innerHTML = "Your passenger is ";
      } else {
        document.getElementById("driverPassengers").innerHTML = "Your passengers are ";
      }
      document.getElementById("driverPassengers").innerHTML += response.result.passengers.join(", ");
		$('#msgTable').height(window.innerHeight - $('#msgTable').offset().top);
    }
    if (response.result.driver == undefined) {
      document.getElementById("passengerActivePanel").style.display="none";
      document.getElementById("passengerIdlePanel").style.display="block";
    } else {
		if (cur_page == 'Driver') {
      	set_page('Passenger');
		}
      active = 'passenger';
      document.getElementById("passengerActivePanel").style.display="block";
      document.getElementById("passengerIdlePanel").style.display="none";
      document.getElementById("passengerDriver").innerHTML = "Your driver is " + response.result.driver;
    }
  }
}

function get_group_list()
{
	str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "getGroupList", "params" : { "sessionId" : "' + sessionId + '" } }';
	debug(str);
	$.post(wsurl, 'q=' + str, function(response) { process_group_list(response)});
}

function process_group_list(response)
{
	var MsgId = 0;

	debug(JSON.stringify(response, null, '\t'));
	if (response.error !== null) {
		alert("GetGroupList: " + response.error.message);
		do_logout();
	} else if (signedIn == 0) {
		;
	} else {
		cookieData.Groups = response.result.groups;
		save_cookie();
	}
}

function get_ims()
{
	str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "getIMs", "params" : { "sessionId" : "' + sessionId + '" } }';
	debug(str);
	$.post(wsurl, 'q=' + str, function(response) { process_ims(response)});
}

function process_ims(response)
{
	debug(JSON.stringify(response, null, '\t'));
	if (response.error !== null) {
		alert("GetIMs: " + response.error.message);
		do_logout();
	} else if (signedIn == 0) {
		;
	} else {
		// Add any IMs to our cookieData, then ack them all
		var lastId = 0;

		for (var id in response.result.msgs) {
			var msg = response.result.msgs[id];
			if (id > lastId)
				lastId = id;
			if (!cookieData.hasOwnProperty('IMs'))
				cookieData['IMs'] = new Array;
			if (!cookieData.IMs[msg.from])
				cookieData.IMs[msg.from] = { 'New':1, 'Msgs':[] };
			cookieData.IMs[msg.from].New = 1;
			cookieData.IMs[msg.from].Msgs.push({ 'Incoming':1, 'Time':msg.time, 'Text':escapeHtml(msg.text) });
		}
		if (lastId > 0) {
			do_post('ackIMs', { 'msgId':lastId }, process_ack_ims);
			save_cookie();
		}
		// Delete any old messages and threads that become empty as a result
		var d = new Date();
		var cutoff = d.getTime()/1000 - cookieData.IMExpiryDays * 24 * 60 * 60;
		var filtered_ims = {};
		for (var contact in cookieData.IMs) {
			var filtered_msgs = [];
			for (var index in cookieData.IMs[contact].Msgs) {
				if (cookieData.IMs[contact].Msgs[index].Time > cutoff) {
					filtered_msgs.push(cookieData.IMs[contact].Msgs[index]);
				}
			}
			if (filtered_msgs.length > 0) {
				filtered_ims[contact] = { 'New':cookieData.IMs[contact].New, 'Msgs':filtered_msgs };
			}
		}
		cookieData.IMs = filtered_ims;
		save_cookie();
		render_im_list();
		if (im_detail_contact != '') {
			im_render_detail(im_detail_group, im_detail_contact);
		}
	}
}

function process_ack_ims(response)
{
	debug(JSON.stringify(response, null, '\t'));
	if (response.error !== null) {
		alert("AckIMs: " + response.error.message);
		do_logout();
	}
}

function render_im_list()
{
	for (var contact in cookieData.IMs) {
		cookieData.IMs[contact].Msgs.sort(function(a, b) { return(b.Time - a.Time); });
	}

	var contacts = [];
	for (var key in cookieData.IMs)
		contacts.push(key);
	contacts.sort(function(a, b) { return cookieData.IMs[b].Msgs[0].Time - cookieData.IMs[a].Msgs[0].Time; });
	cookieData.Groups.sort();
	var new_content = '<div><b>Groups</b></div>';
	for (var index in cookieData.Groups) {
		new_content += '<div class="oldIMThread" onclick="im_render_detail(true, \'' + cookieData.Groups[index] + '\')">' + cookieData.Groups[index] + '</div>';
	}
	new_content += '<div><b>Messages</b></div>';
	for (var index in contacts) {
		contact = contacts[index];
		var d = new Date(cookieData.IMs[contact].Msgs[0].Time * 1000);
		var dmy = d.getDate()+' '+monthNames[d.getMonth()]+' '+d.getFullYear();
		var m = d.getMinutes();
		if (m < 9)
			m = "0" + m;
		var hm = d.getHours()+':'+m;
		if (cookieData.IMs[contact].New == 1)
			state = 'newIMThread';
		else
			state = 'oldIMThread';
		new_content +=
			'<div class="' + state + '" onclick="im_render_detail(false, \'' + contact + '\')">' +
				'<span style="display:inline; float: right;">' + dmy + '</span>' +
				'<span> <span id="im_list_contact">' + contact + '</span></span>' +
				'<span style="display:inline; float: right;">' + hm + '</span>' +
				'<span> <span id="im_list_text">' + cookieData.IMs[contact].Msgs[0].Text + '</span></span>' +
			'</div>';
	}
	$('#imSummaryTable').height(window.innerHeight - $('#imSummaryTable').offset().top);
	if (imSummaryTable.innerHTML != new_content) {
		imSummaryTable.innerHTML = new_content;
		$('#imSummaryTable').animate({ scrollTop: 0 }, "slow");
	}
}

function im_render_detail(is_group, name)
{
	if (im_detail_contact == '') {
		// First display of page, scroll to top
		$('#imDetailTable').scrollTop(0);
	}
	im_detail_contact = name;
	im_detail_group = is_group;
	$('#messagecentresummary').hide();
	$('#messagecentredetail').show();
	$('#header2').show().text(name);
	var new_content = '';
	if (is_group) {
		;
	} else if (cookieData.IMs.hasOwnProperty(name)) {
		var msgs = cookieData.IMs[name].Msgs;

		if (cookieData.IMs[name].New == 1) {
			cookieData.IMs[name].New = 0;
			save_cookie();
		}
		msgs.sort(function(a, b) { return a.Time - b.Time; });
		for (var index in msgs) {
			var d = new Date(msgs[index].Time * 1000);
			var m = d.getMinutes();
			if (m < 9)
				m = "0" + m;
			var dmyhm = d.getDate()+' '+monthNames[d.getMonth()]+' '+d.getFullYear()+' '+d.getHours()+':'+m;
			new_content += '<div id="im_detail_date"><center>'+dmyhm+'</center></div>';
			if (msgs[index].Incoming == 1)
				new_content += '<table width="100%"><tr><td class="incoming_im" width="80%">'+msgs[index].Text+'</td><td width="20%">&nbsp;</td></tr></table>';
			else
				new_content += '<table width="100%"><tr><td width="20%">&nbsp;</td><td class="outgoing_im" width="80%" style="text-align:right">'+msgs[index].Text+'</td></tr></table>';
		}
	}
	$('#imDetailTable').height(window.innerHeight - $('#imDetailTable').offset().top - 40);
	if (imDetailTable.innerHTML != new_content) {
		imDetailTable.innerHTML = new_content;
		$('#imDetailTable').stop().animate({
		scrollTop: $("#imDetailTable")[0].scrollHeight
		}, 800);
	}
}

function im_send_clicked()
{
	var txt = document.getElementById('imSendText').value;
	var params;
	var d = new Date();

	document.getElementById('imSendText').value = '';
	if (txt != '') {
		if (im_detail_group) {
			params = { 'emailList':[], 'groupList':[ im_detail_contact ], 'message':txt };
		} else {
			params = { 'emailList':[ im_detail_contact ], 'groupList':[], 'message':txt };
			if (!cookieData.IMs.hasOwnProperty(im_detail_contact)) {
				cookieData.IMs[im_detail_contact] = { 'New':0, 'Msgs':[] };
			}
			cookieData.IMs[im_detail_contact].Msgs.push({ 'Incoming':0, 'Time':d.getTime()/1000, 'Text':escapeHtml(txt) });
			save_cookie();
			im_render_detail(im_detail_group, im_detail_contact);
		}
		do_post('sendIM', params, process_send_im);
	}
}

function process_send_im(response)
{
	debug(JSON.stringify(response, null, '\t'));
	if (response.error !== null) {
		alert("sendIM: " + response.error.message);
		//do_logout();
	}
}

function signin_keypress(e)
{
	var keyCode = e.keyCode || e.which;

	if (keyCode == 13)
		signin_button_pressed();
}

function do_post(type, params, handler)
{
	var msg = new Object;

	msg.jsonrpc = '2.0';
	msg.Id = 1;
	msg.method = type;
	params.sessionId = sessionId;
	msg.params = params;
	debug(JSON.stringify(msg, null, '\t'));
	$.post(wsurl, 'q=' + JSON.stringify(msg, null, '\t'), function(response) { handler(response)});
}

function send_im(emails, groups, text)
{
	var params = { 'emailList':emails, 'groupList':groups, 'message':text };

	do_post('sendIM', params, process_send_im2);
}

function process_send_im2(response)
{
	debug(JSON.stringify(response, null, '\t'));
	if (response.error !== null) {
		alert("sendIM: " + response.error.message);
		do_logout();
	} else {
	}
}

function get_messages()
{
  str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "getMessages", "params" : { "sessionId" : "' + sessionId + '" } }';
  debug(str);
  $.post(wsurl, 'q=' + str, function(response) { process_messages(response)});
}

function process_messages(response)
{
  var MsgId = 0;

  debug(JSON.stringify(response, null, '\t'));
  if (response.error !== null) {
    alert("GetMessages: " + response.error.message);
		do_logout();
  } else if (signedIn == 0) {
    ;
  } else {
    msgTable.innerHTML = "";
    if (response.result.msgs) {
      $.each(response.result.msgs, function(id, msgdata) { MsgId = id; process_one_message(id, msgdata); });
    }
  }
  if (MsgId != LastMsgIdReceived) {
    $('#msgTable').stop().animate({
      scrollTop: $("#msgTable")[0].scrollHeight
    }, 800);
    LastMsgIdReceived = MsgId;
  }
}

function process_one_message(id, msgdata)
{
  txt = 'huh?';
  if ("errorText" in msgdata)
     txt = msgdata.errorText;
  else if (msgdata.type == 'startRide') {
    if ("driver" in msgdata) {
      txt = 'Your driver is ' + msgdata.driver + ' and the passphrase is \'' + msgdata.magic + '\'';
    } else {
      txt = msgdata.passenger + ' is requesting a ride, and the passphase is \'' + msgdata.magic + '\'.';
    }
  } else if (msgdata.type == 'endRide') {
    if ("passenger" in msgdata) {
      txt = 'Your passenger ' + msgdata.passenger + ' has completed their journey, you have been credited ';
    } else {
      txt = 'End of journey, you have been charged';
    }
    txt += ' ' + msgdata.miles + ' miles.';
  }
  else {
    txt = 'Funny msg type ' + msgdata.type;
  }
  msgTable.innerHTML += "<hr width=\"94%\"><p onclick=\"delete_messages(" + id + ")\">" + txt;
}

function delete_messages(id)
{
  IdleCounter = 0;
  if (confirm("Delete messages?") == true) {
    str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "ackMessages", "params" : { "sessionId" : "' + sessionId + '", "msgId" : "' + id + '" } }';
    debug(str);
    $.post(wsurl, 'q=' + str, function(response) { process_delete_messages(response)});
  }
}

function process_delete_messages(response)
{
  debug(JSON.stringify(response, null, '\t'));
  if (response.error != null) {
    alert(esponse.error.message);
  } else {
    get_messages();
  }
}

function endride_button_press()
{
  IdleCounter = 0;
  var miles = document.getElementById("miles").value;
  if (miles == "") {
    alert("You must enter the miles travelled");
  } else {
    str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "endRide", "params" : { "sessionId" : "' + sessionId + '", "miles" : "' + miles + '", "location" : { "lat" : 37.812, "lng" : -122.3482, "accuracy": 0 } } }';
    debug(str);
    $.post(wsurl, 'q=' + str, function(response) { process_endride(response)});
  }
}

function process_endride(response)
{
  debug(JSON.stringify(response, null, '\t'));
  if (response.error != null) {
    alert(esponse.error.message);
  } else {
    document.getElementById("miles").value = "";
    $('#passengerPassphrase').show().text('');
    get_status();
  }
}

function startride_button_press()
{
  IdleCounter = 0;
  var registration = document.getElementById("registration").value;
  if (registration == "") {
    alert("You must enter the car registration");
  } else {
    str = '{ "jsonrpc" : "2.0", "id" : 1, "method" : "startRide", "params" : { "sessionId" : "' + sessionId + '", "regno" : "' + registration + '", "location" : { "lat" : 37.812, "lng" : -122.3482, "accuracy": 0 } } }';
    debug(str);
    $.post(wsurl, 'q=' + str, function(response) { process_startride(response)});
  }
}

function process_startride(response)
{
  debug(JSON.stringify(response, null, '\t'));
  if (response.error != null) {
    alert(response.error.message);
  } else {
    document.getElementById("registration").value = "";
    get_status();
    $('#passengerPassphrase').show().text('The passphrase is "' + response.result.magic + '"');
  }
}

function cleardebug_button_pressed()
{
  IdleCounter = 0;
  debugTable.innerHTML = "";
}

function debug(str)
{
  if (debugEnable.checked == true) {
    debugTable.innerHTML += str + "<hr>";
  }
}

function im_expiry_days_changed()
{
	cookieData.IMExpiryDays = document.getElementById('im_expiry_days').value;
	save_cookie();
}

