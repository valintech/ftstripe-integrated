<?php
require_once('base.inc');
require_once('classes/region.inc');

// Validate any current session
CWebSession::init();
$login = CRoot::createFromStream('CLogin', CWebSession::get('login'));
$isValidSession = $login->isValidSession();

// Get posted data
$v = CCommon::getRequestValue('v');
$v = CCommon::fromJson(base64_decode($v));
$userId = $v->userId;
$userKey = $v->userKey;
$region = new CRegion('confirmaccount');
$rplc = array();
$rplc[1] = ($userId ? confirmAccount($userId, $userKey, $region) : $region->msg(1010));
$rplc[8] = $region->msg(8, 'common');
$rplc[9] = $region->msg(9, 'common');
$rplc[11] = $region->msg(1100);
$rplc[30] = $region->msg(10, 'common');
$rplc[31] = $region->msg(($isValidSession ? 12 : 11), 'common');
$rplc[32] = $region->msg(13, 'common');
$rplc[33] = $region->msg(14, 'common');
$rplc[34] = $region->msg(($isValidSession ? 16 : 15), 'common');
if($isValidSession)
  $menu_header=file_get_contents('header_menus_login.php');
        else
    $menu_header=file_get_contents('header_menus.php');
    
$rplc[777]= $menu_header;
$rplc[36] = ($isValidSession ? sprintf("%s %s", $region->msg(4, 'common'), $login->userFriendlyName()) : '');
$out = CCommon::htmlReplace('confirmaccount.html', $rplc, true, CCommon::ersReplacePatterns($isValidSession));
print($out);
if (CConfig::RUN_IN_FB == 0)
	@include 'google_analytics.html';

/*
 * Confirm account
 * 
 * @param $userId      User id.
 * @param $userKey     User verification key
 * @param $region      A CRegion object
 * @return             Confirmation text
 */

function confirmAccount ($userId, $userKey, $region)
{
	// Load the user details
	$user = new CUser;
	if ($user->loadWithUserId($userId) == false)
	{
		CLogging::error(sprintf('confirmaccount.php - cannot load user request %ld',
								$userId));
		return $region->msg(1000);
	}
		
	// Check if account already verified
	if ($user->get(CUser::VERIFIED))
		return $region->msg(1002);

	// Check if request keys match
	if ($userKey != $user->getUserKey())
	{
		CLogging::error(sprintf('confirmaccount.php - key mismatch [%s][%s]',
								$userKey, $user->getUserKey()));
		return $region->msg(1001);
	}

	// Save the user
	$user->set(CUser::VERIFIED, 1);
	if ($user->save() == false)
	{
		CLogging::error(sprintf('confirmaccount.php - cannot save user %ld',
								$user->get(CUser::ID)));
		return $region->msg(1000);
	}

	return $region->msg(1005);
}
?>
