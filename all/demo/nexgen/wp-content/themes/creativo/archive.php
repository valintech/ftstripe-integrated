<?php get_header(); ?>	
    <?php
    if($data['en_sidebar'] !='no') {
        if($data['sidebar_pos'] == 'left') {
            $content_css = 'float:right;';
            $sidebar_css = 'float:left;';
        } else {
            $content_css = 'float:left;';
            $sidebar_css = 'float:right;';
        }
    }
    else {
      $content_css = 'width: 100%;';
      $sidebar_css = 'display:none;';
    }
    
    $style = ( $data['blog_images'] != 'grid' && $data['blog_images'] != 'masonry' ) ? 'style-default' : 'style-'.$data['blog_images'] ;
    if( $data['blog_images'] == 'grid' ) : $post_container_in = 'is_grid'; endif;
    if ($data['blog_images'] == 'masonry') : $post_container_in = 'grid-masonry js-masonry'; endif;
    $force_full_width = ( ( $data['blog_images'] == 'grid' ||  $data['blog_images'] == 'masonry' ) && ( $data['index_full_width'] == 'yes' ) ) ? 'force_full_width' : '';
        ?>
        <div class="row <?php echo $force_full_width; ?>">
        	<div class="post_container" style="<?php echo $content_css; ?>">
            	<?php if(is_author()){ ?>
            		<div class="posts-related">                    	
                        <div class="author_box">
                        	<div class="author_pic">
								<?php 
                                    echo get_avatar( get_the_author_meta('id'), $size = '80'); 
                                ?>
                            </div>
                            <h3><?php the_author_posts_link(); ?></h3>
                        	<?php the_author_meta( 'description'); ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                    
                <?php } ?> 
            	<?php if (have_posts()) { ?>
                    <div class="<?php echo $post_container_in; ?> clearfix">
                        <?php if ($data['blog_images'] == 'masonry') : echo '<div class="gutter-sizer"></div>'; endif; ?>
    					<?php while(have_posts()): the_post(); ?>
    						<?php get_template_part('index',$style); ?>                	   
                    	<?php endwhile; ?>
                    </div>
                    <?php kriesi_pagination($blog->max_num_pages, $range = 2); ?>
                <?php        
				}	
                ?>                                 
            </div>
            <div class="sidebar" style="<?php echo $sidebar_css; ?>">                
                <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar() ) ?>            
            </div>
             <div class="clr"></div>   
        </div>        
<?php get_footer(); ?>