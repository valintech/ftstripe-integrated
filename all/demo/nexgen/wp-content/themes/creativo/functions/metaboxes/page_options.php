<div class="pyre_metabox">

<?php /*
$this->select(	'show_title',
				__('Show Page Title & Breadcrumb', 'Creativo'),
				array('yes' => __('Yes', 'Creativo'), 'no' => __('No', 'Creativo')),
				''
			);
?>
<?php
$this->select(	'show_title_sec',
				__('Show Linkable Page Title', 'Creativo'),
				array('yes' => __('Yes', 'Creativo'), 'no' => __('No', 'Creativo')),
				''
			);
*/?>
<?php
$this->select(	'show_featured',
				__('Show All Featured Images', 'Creativo'),
				array('yes' => __('Yes', 'Creativo'), 'no' => __('No', 'Creativo')),
				__('Display featured images at the beggining of the page.', 'Creativo')
			);
?>
<?php /*
$this->select(	'show_featured_one',
				__('Show Only Main Featured Image', 'Creativo'),
				array('yes' => __('Yes', 'Creativo'), 'no' => __('No', 'Creativo')),
				__('Display only the main featured image, at the beginning of the post. The rest of the images will not be included and will not be displayed on the top of the page. <br />* Show Featured Images option (above) needs to be turned off - Select No', 'Creativo')
			);*/
?>
<?php
/*
$this->select(	'en_sidebar',
				__('Enable Sidebar', 'Creativo'),
				array('yes' => __('Yes', 'Creativo'), 'no' => __('No', 'Creativo')),
				''
			);
			*/
?>
<?php
$this->select(	'en_sidebar',
				__('Enable Sidebar', 'Creativo'),
				array('yes' => __('Yes', 'Creativo'), 'no' => __('No', 'Creativo')),
				__('Enable or disable the Sidebar.', 'Creativo')
			);
?>
<?php
$this->select(	'sidebar_pos',
				__('Sidebar Position', 'Creativo'),
				array('right' => __('Right', 'Creativo'), 'left' => __('Left', 'Creativo')),
				''
			);
?>
<?php
$this->select(	'force_full_width',
				__('Force 100% Width', 'Creativo'),
				array('no' => __('No', 'Creativo'), 'yes' => __('Yes', 'Creativo')),
				__('Select Yes to force the page width to be 100%. Only works when the Blog Style Grid and Blog Style Masonry template is being used.', 'Creativo')
			);
?>
<?php
$this->select(	'grid_cols',
				__('Grid Columns', 'Creativo'),
				array('2' => __('2 Columns', 'Creativo'), '3' => __('3 Columns', 'Creativo'), '4' => __('4 Columns', 'Creativo'), '5' => __('5 Columns', 'Creativo')),
				__('Select the number of Grid Columns to use. Only works when the Blog Style Grid and Blog Style Masonry template is being used.', 'Creativo')
			);
?>

<?php
$terms = get_categories( 'hide_empty=1');
$terms_array[0] = __('All categories', 'Creativo');
if($terms) {
	foreach ($terms as $term) {
		$terms_array[$term->term_id] = $term -> name;
	}
	$this->select(	'posts_category',
		__('Posts Category', 'Creativo'),
		$terms_array,
		__('Choose the category you want to display posts from.', 'Creativo')
	);
}
?>

<?php
$types = get_terms('portfolio_category', 'hide_empty=0');
$types_array[0] = __('All categories', 'Creativo');
if($types) {
	foreach($types as $type) {
		$types_array[$type->term_id] = $type->name;
	}
	$this->select(	'portfolio_category',
		__('Portfolio Category', 'Creativo'),
		$types_array,
		__('Choose what portfolio category you want to display on this page.', 'Creativo')
	);
}
?>
<?php
$this->select(	'portfolio_style',
				__('Portfolio Style', 'Creativo'),
				array('3d' => __('3D', 'Creativo'), 'flat' => __('Flat', 'Creativo')),
				__('Select the style of the Portfolio page', 'Creativo')
			);
?>


</div>