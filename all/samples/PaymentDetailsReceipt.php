<?php
session_start();

use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\PaymentDetailsRequest;
use PayPal\Types\Common\RequestEnvelope;

require_once('PPBootStrap.php');
require_once('../base.inc');
require_once('../classes/user.inc');
require_once('../classes/common.inc');
require_once('../classes/region.inc');

require_once('../classes/group.inc');
require_once('../classes/usergroup.inc');
require_once('../classes/journey.inc');
require_once('../classes/used_freemiles.inc');
require_once('../classes/used_freemiles.inc');

/*
 *  # PaymentDetails API
  Use the PaymentDetails API operation to obtain information about a payment. You can identify the payment by your tracking ID, the PayPal transaction ID in an IPN message, or the pay key associated with the payment.
  This sample code uses AdaptivePayments PHP SDK to make API call
 */
/*
 * 
  PaymentDetailsRequest which takes,
  `Request Envelope` - Information common to each API operation, such
  as the language in which an error message is returned.
 */
$requestEnvelope = new RequestEnvelope("en_GB");
/*
 * 		 PaymentDetailsRequest which takes,
  `Request Envelope` - Information common to each API operation, such
  as the language in which an error message is returned.
 */
$paymentDetailsReq = new PaymentDetailsRequest($requestEnvelope);
/*
 * 		 You must specify either,

 * `Pay Key` - The pay key that identifies the payment for which you want to retrieve details. This is the pay key returned in the PayResponse message.
 * `Transaction ID` - The PayPal transaction ID associated with the payment. The IPN message associated with the payment contains the transaction ID.
  `paymentDetailsRequest.setTransactionId(transactionId)`
 * `Tracking ID` - The tracking ID that was specified for this payment in the PayRequest message.
  `paymentDetailsRequest.setTrackingId(trackingId)`
 */
if ($_POST['payKey'] != "") {
    $paymentDetailsReq->payKey = $_POST['payKey'];
    $payKey = $_POST['payKey'];
}
if ($_POST['transactionId'] != "") {
    $paymentDetailsReq->transactionId = $_POST['transactionId'];
}
if ($_POST['trackingId'] != "") {
    $paymentDetailsReq->trackingId = $_POST['trackingId'];
}

/*
 * 	 ## Creating service wrapper object
  Creating service wrapper object to make API call and loading
  Configuration::getAcctAndConfig() returns array that contains credential and config parameters
 */
$service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());
try {
    /* wrap API method calls on the service object with a try catch */
    $response = $service->PaymentDetails($paymentDetailsReq);
} catch (Exception $ex) {
    require_once 'Common/Error.php';
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>PayPal Adaptive Payments - Payment Details</title>
        <meta name="viewport" content="width=device-width,height=device-height initial-scale=1" />
        <link href="Common/sdk.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="Common/sdk_functions.js"></script>
    </head>

    <body>
        <div id="wrapper">

            <div id="response_form">

                <?php
                $ack = strtoupper($response->responseEnvelope->ack);
                if ($ack != "SUCCESS") {
                    
                } else {
                    // $jDetails = CJourney::getRecentPostionByJourneyIdStatus($_SESSION['user_id']);
                    /*
                     * 			 The status of the payment. Possible values are:

                     * CREATED - The payment request was received; funds will be
                      transferred once the payment is approved
                     * COMPLETED - The payment was successful
                     * INCOMPLETE - Some transfers succeeded and some failed for a
                      parallel payment or, for a delayed chained payment, secondary
                      receivers have not been paid
                     * ERROR - The payment failed and all attempted transfers failed
                      or all completed transfers were successfully reversed
                     * REVERSALERROR - One or more transfers failed when attempting
                      to reverse a payment
                     * PROCESSING - The payment is in progress
                     * PENDING - The payment is awaiting processing
                     */
                    if ($ack == 'SUCCESS') {
                        // echo $payKey."<br>";

                        $journey = new CJourney();
                        $transaction = $response->paymentInfoList->paymentInfo[0];
                        $transaction_id = $transaction->transactionId;
                        $journey_id = $_POST['jid'];
                        $date_time = date_format(date_create($response->responseEnvelope->timestamp), "Y-m-d H:i:s");
                        if (isset($transaction_id)) {
                            $journey->getRecentUpdatePayDriverJourneyIdStatus($journey_id, $date_time, $payKey, $transaction_id, 2);
                            echo "<div><img src='https://firsthumb.com/content/web/assets/images/1st_logo.png'></div><div><h2>Payment Details</h2></div><div style='color: #000;font-size: 20px;font-weight: bold;text-align: center;'>Your Payment is successful. Now you may close the browser.</div>";
                        }
                        $journey_lists = $journey->getFetchJID($journey_id);
                       /*
                        $miles = $journey_lists['miles'];
                        $paid_miles = $journey_lists['paid_miles'];
                        $driver_amount1 = $miles - $paid_miles;
                        $driver_charge = 15;
                        $driver_amount = ($driver_amount1 * $driver_charge) / 100;
                        if ($driver_amount1 > 0) {
                            $driver_amount = ( $driver_amount + .20 ) * 1.035196687;
                            if ($_SESSION['payment_mode'] == 2) {

                                $driver_amount = ( $driver_amount + .05 ) * 1.052631579;
                            }
                        }
                        
                        $journey_lists = CJourney::getFetchJID($journey_id);

                        //     if (empty($journey_lists['request'])) {

                        $driver_amount = number_format((float) $driver_amount, 2, '.', '');*/
                        $journey_id = $_SESSION['jid'];
                        if($journey_lists['owner_amount'] < 0){$driver_amount =$journey_lists['driver_amount'];
                        CJourney::getSetFetchSend($journey_id);
                        $driver_id = $journey_lists['driver_id'];
                        $driver_email = CJourney::driveremail($driver_id);

                        $data = [
                            "sender_batch_header" => [
                                "email_subject" => "Payment"
                            ],
                            "items" => [
                                [
                                    "recipient_type" => "EMAIL",
                                    "amount" => [
                                        "value" => $driver_amount,
                                        "currency" => "GBP"
                                    ],
                                    "receiver" => $driver_email,
                                    "note" => "Driver Payment",
                                    "sender_item_id" => "Firsthumb"
                                ]
                            ]
                        ];

                        $data_json = json_encode($data);
                        $url = 'https://api.sandbox.paypal.com/v1/payments/payouts?sync_mode=true';
                        $url1 = 'https://api.sandbox.paypal.com/v1/oauth2/token';
                        $username = 'Advk1RkuPdOhbjoU96Oaj2LO2WhXhxKuJLvBtGxPuU28zaRCd4Trv6hz_wPzBb4llG27DMtycmmP3Jtr';
                        $password = 'EF1m1IZbG7-FXEfwTkJD4dER9RzlgZjB-bOjHbEWvHuvM8kmIhJO2GXLx2MXjpcjKj6mnsXMr_QppoUY';
                        $ch1 = curl_init();
                        curl_setopt($ch1, CURLOPT_URL, $url1);
                        curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Accept-Language: en_GB'));
                        curl_setopt($ch1, CURLOPT_USERPWD, $username . ":" . $password);

                        curl_setopt($ch1, CURLOPT_POST, 1);
                        curl_setopt($ch1, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
                        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                        $response1 = curl_exec($ch1);
                        curl_close($ch1);
                        $result = json_decode($response1, true);
                        $access_token = $result['access_token'];

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer ' . $access_token));
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($ch);
                        curl_close($ch);
                        $result_r = json_decode($response, true);

                        if (isset($result_r['items'][0]['transaction_id'])) {
                            $date_time = date_format(date_create($result_r['batch_header']['time_completed']), "Y-m-d H:i:s");

                            $transaction_id = $result_r['items'][0]['transaction_id'];
                            $transaction_status = $result_r['items'][0]['transaction_status'];
                            $status = 1;
                            if ($transaction_status == 'SUCCESS') {
                                $status = 2;
                            }
                            CJourney::getRecentUpdatePayDriverJourneyIdStatusPayout($journey_id, $transaction_id);
                        }}
                    }
                    // }
                }
                //require_once 'Common/Response.php';
                ?>
            </div>
        </div>
    </body>
</html>
