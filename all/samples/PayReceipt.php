<?php
session_start();
ini_set('display_errors','on');
use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\FundingConstraint;
use PayPal\Types\AP\FundingTypeInfo;
use PayPal\Types\AP\FundingTypeList;
use PayPal\Types\AP\PayRequest;
use PayPal\Types\AP\Receiver;
use PayPal\Types\AP\ReceiverList;
use PayPal\Types\AP\SenderIdentifier;
use PayPal\Types\Common\PhoneNumberType;
use PayPal\Types\Common\RequestEnvelope;

/**
 * PayReceipt.php
 * This file is called after the user clicks on a button during
 * the Pay process to use PayPal's AdaptivePayments Pay features'. The
 * user logs in to their PayPal account.
 * Called by Pay.php
 */

/*
 * Use the Pay API operation to transfer funds from a sender�s PayPal account to one or more receivers� PayPal accounts. You can use the Pay API operation to make simple payments, chained payments, or parallel payments; these payments can be explicitly approved, preapproved, or implicitly approved.

Use the Pay API operation to transfer funds from a sender's PayPal account to one or more receivers' PayPal accounts. You can use the Pay API operation to make simple payments, chained payments, or parallel payments; these payments can be explicitly approved, preapproved, or implicitly approved. 
 */

/*
 * Create your PayRequest message by setting the common fields. If you want more than a simple payment, add fields for the specific kind of request, which include parallel payments, chained payments, implicit payments, and preapproved payments.
 */
require_once('PPBootStrap.php');
require_once('Common/Constants.php');
require_once('../base.inc');
require_once('../classes/user.inc');
require_once('../classes/common.inc');
require_once('../classes/region.inc');

require_once('../classes/group.inc');
require_once('../classes/usergroup.inc');
require_once('../classes/journey.inc');
require_once('../classes/used_freemiles.inc');
require_once('../classes/used_freemiles.inc');
define("DEFAULT_SELECT", "- Select -");

if(isset($_POST['receiverEmail'])) {
	$receiver = array();
	/*
	 * A receiver's email address 
	 */
	for($i=0; $i<count($_POST['receiverEmail']); $i++) {
		$receiver[$i] = new Receiver();
		$receiver[$i]->email = $_POST['receiverEmail'][$i];
		/*
		 *  	Amount to be credited to the receiver's account 
		 */
		$receiver[$i]->amount = $_POST['receiverAmount'][$i];
		/*
		 * Set to true to indicate a chained payment; only one receiver can be a primary receiver. Omit this field, or set it to false for simple and parallel payments. 
		 */
		$receiver[$i]->primary = $_POST['primaryReceiver'][$i];

		/*
		 * (Optional) The invoice number for the payment. This data in this field shows on the Transaction Details report. Maximum length: 127 characters 
		 */
		if($_POST['invoiceId'][$i] != "") {
			$receiver[$i]->invoiceId = $_POST['invoiceId'][$i];
		}
		/*
		 * (Optional) The transaction type for the payment. Allowable values are:

    GOODS � This is a payment for non-digital goods
    SERVICE � This is a payment for services (default)
    PERSONAL � This is a person-to-person payment
    CASHADVANCE � This is a person-to-person payment for a cash advance
    DIGITALGOODS � This is a payment for digital goods
    BANK_MANAGED_WITHDRAWAL � This is a person-to-person payment for bank withdrawals, available only with special permission.

Note: Person-to-person payments are valid only for parallel payments that have the feesPayer field set to EACHRECEIVER or SENDER.
		 */
		if($_POST['paymentType'][$i] != "" && $_POST['paymentType'][$i] != DEFAULT_SELECT) {
			$receiver[$i]->paymentType = $_POST['paymentType'][$i];
		}
		/*
		 *  (Optional) The transaction subtype for the payment.
		 */
		if($_POST['paymentSubType'][$i] != "") {
			$receiver[$i]->paymentSubType = $_POST['paymentSubType'][$i];
		}
		if($_POST['phoneCountry'][$i] != "" && $_POST['phoneNumber'][$i]) {
			$receiver[$i]->phone = new PhoneNumberType($_POST['phoneCountry'][$i], $_POST['phoneNumber'][$i]);
			if($_POST['phoneExtn'][$i] != "") {
				$receiver[$i]->phone->extension = $_POST['phoneExtn'][$i];
			}
		}
	}
	$receiverList = new ReceiverList($receiver);
}

/*
 * The action for this request. Possible values are:

    PAY � Use this option if you are not using the Pay request in combination with ExecutePayment.
    CREATE � Use this option to set up the payment instructions with SetPaymentOptions and then execute the payment at a later time with the ExecutePayment.
    PAY_PRIMARY � For chained payments only, specify this value to delay payments to the secondary receivers; only the payment to the primary receiver is processed.

 */
/*
 * The code for the currency in which the payment is made; you can specify only one currency, regardless of the number of receivers 
 */
/*
 * URL to redirect the sender's browser to after canceling the approval for a payment; it is always required but only used for payments that require approval (explicit payments) 
 */
/*
 * URL to redirect the sender's browser to after the sender has logged into PayPal and approved a payment; it is always required but only used if a payment requires explicit approval 
 */
$payRequest = new PayRequest(new RequestEnvelope("en_GB"), $_POST['actionType'], $_POST['cancelUrl'], $_POST['currencyCode'], $receiverList, $_POST['returnUrl']);
// Add optional params
/*
 *  (Optional) The payer of PayPal fees. Allowable values are:

    SENDER � Sender pays all fees (for personal, implicit simple/parallel payments; do not use for chained or unilateral payments)
    PRIMARYRECEIVER � Primary receiver pays all fees (chained payments only)
    EACHRECEIVER � Each receiver pays their own fee (default, personal and unilateral payments)
    SECONDARYONLY � Secondary receivers pay all fees (use only for chained payments with one secondary receiver)

 */
$payRequest->cancelUrl = $_POST['cancelUrl'];
$payRequest->returnUrl = $_POST['returnUrl'];
if($_POST["feesPayer"] != "") {
	$payRequest->feesPayer = $_POST["feesPayer"];
}
/*
 *  (Optional) The key associated with a preapproval for this payment. The preapproval key is required if this is a preapproved payment.
Note: The Preapproval API is unavailable to API callers with Standard permission levels.
 */
if($_POST["preapprovalKey"] != "") {
	$payRequest->preapprovalKey  = $_POST["preapprovalKey"];
}
/*
 * (Optional) The URL to which you want all IPN messages for this payment to be sent. Maximum length: 1024 characters 
 */
if($_POST['ipnNotificationUrl'] != "") {
	$payRequest->ipnNotificationUrl = $_POST['ipnNotificationUrl'];
}
if($_POST["memo"] != "") {
	$payRequest->memo = $_POST["memo"];
}
/*
 * (Optional) The sender's personal identification number, which was specified when the sender signed up for a preapproval. 
 */
if($_POST["pin"] != "") {
	$payRequest->pin  = $_POST["pin"];
}
if($_POST['preapprovalKey'] != "") {
	$payRequest->preapprovalKey  = $_POST["preapprovalKey"];
}
if($_POST['reverseAllParallelPaymentsOnError'] != "") {
	$payRequest->reverseAllParallelPaymentsOnError  = $_POST["reverseAllParallelPaymentsOnError"];
}
if($_POST['senderEmail'] != "") {
	$payRequest->senderEmail  = $_POST["senderEmail"];
}
/*
 *(Optional) A unique ID that you specify to track the payment.
Note: You are responsible for ensuring that the ID is unique. 
 */
if($_POST['trackingId'] != "") {
	$payRequest->trackingId  = $_POST["trackingId"];
}
/*
 * 
 */
if($_POST['fundingConstraint'] != "" && $_POST['fundingConstraint'] != DEFAULT_SELECT) {
	$payRequest->fundingConstraint = new FundingConstraint();
	/*
	 * Specifies a list of allowed funding selections for the payment. This is a list of funding selections that can be combined in any order to allow payments to use the indicated funding type. If this field is omitted, the payment can be funded by any funding type that is supported for Adaptive Payments. Allowable values are:

    ECHECK � Electronic check
    BALANCE � PayPal account balance
    CREDITCARD � Credit card

Note: ECHECK and CREDITCARD include BALANCE implicitly.
Note: FundingConstraint is unavailable to API callers with standard permission levels; for more information, refer to the section Adaptive Payments Permission Levels.
	 */
	$payRequest->fundingConstraint->allowedFundingType = new FundingTypeList();
	$payRequest->fundingConstraint->allowedFundingType->fundingTypeInfo = array();
	$payRequest->fundingConstraint->allowedFundingType->fundingTypeInfo[]  = new FundingTypeInfo($_POST["fundingConstraint"]);
}
/*
 *(Optional) If true, use credentials to identify the sender; default is false. 
 */
if($_POST['emailIdentifier'] != "" || $_POST['senderCountryCode'] != "" || $_POST['senderPhoneNumber'] != "" 
		|| $_POST['senderExtension'] != "" || $_POST['useCredentials'] != "" ) {
	$payRequest->sender = new SenderIdentifier();
	if($_POST['emailIdentifier'] != "") {
		$payRequest->sender->email  = $_POST["emailIdentifier"];
	}
	if($_POST['senderCountryCode'] != "" || $_POST['senderPhoneNumber'] != "" || $_POST['senderExtension'] != "") {
		$payRequest->sender->phone = new PhoneNumberType();
		if($_POST['senderCountryCode'] != "") {
			$payRequest->sender->phone->countryCode  = $_POST["senderCountryCode"];
		}
		if($_POST['senderPhoneNumber'] != "") {
			$payRequest->sender->phone->phoneNumber  = $_POST["senderPhoneNumber"];
		}
		if($_POST['senderExtension'] != "") {
			$payRequest->sender->phone->extension  = $_POST["senderExtension"];
		}
	}
	if($_POST['useCredentials'] != "") {
		$payRequest->sender->useCredentials  = $_POST["useCredentials"];
	}
}
/*
 * 	 ## Creating service wrapper object
Creating service wrapper object to make API call and loading
configuration file for your credentials and endpoint
*/

$service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());
try {
	/* wrap API method calls on the service object with a try catch */
	$response = $service->Pay($payRequest);
} catch(Exception $ex) {
	require_once 'Common/Error.php';
	exit;
}
/* Make the call to PayPal to get the Pay token
 If the API call succeded, then redirect the buyer to PayPal
to begin to authorize payment.  If an error occured, show the
resulting errors */


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>PayPal Adaptive Payments - Pay Response</title>
<link href="Common/sdk.css" rel="stylesheet" type="text/css" />
<style>
    

#fountainG{
	position:relative;
	width:234px;
	height:28px;
	margin:auto;
}

.fountainG{
	position:absolute;
	top:0;
	background-color:rgb(153,250,211);
	width:28px;
	height:28px;
	animation-name:bounce_fountainG;
		-o-animation-name:bounce_fountainG;
		-ms-animation-name:bounce_fountainG;
		-webkit-animation-name:bounce_fountainG;
		-moz-animation-name:bounce_fountainG;
	animation-duration:1.5s;
		-o-animation-duration:1.5s;
		-ms-animation-duration:1.5s;
		-webkit-animation-duration:1.5s;
		-moz-animation-duration:1.5s;
	animation-iteration-count:infinite;
		-o-animation-iteration-count:infinite;
		-ms-animation-iteration-count:infinite;
		-webkit-animation-iteration-count:infinite;
		-moz-animation-iteration-count:infinite;
	animation-direction:normal;
		-o-animation-direction:normal;
		-ms-animation-direction:normal;
		-webkit-animation-direction:normal;
		-moz-animation-direction:normal;
	transform:scale(.3);
		-o-transform:scale(.3);
		-ms-transform:scale(.3);
		-webkit-transform:scale(.3);
		-moz-transform:scale(.3);
	border-radius:19px;
		-o-border-radius:19px;
		-ms-border-radius:19px;
		-webkit-border-radius:19px;
		-moz-border-radius:19px;
}

#fountainG_1{
	left:0;
	animation-delay:0.6s;
		-o-animation-delay:0.6s;
		-ms-animation-delay:0.6s;
		-webkit-animation-delay:0.6s;
		-moz-animation-delay:0.6s;
}

#fountainG_2{
	left:29px;
	animation-delay:0.75s;
		-o-animation-delay:0.75s;
		-ms-animation-delay:0.75s;
		-webkit-animation-delay:0.75s;
		-moz-animation-delay:0.75s;
}

#fountainG_3{
	left:58px;
	animation-delay:0.9s;
		-o-animation-delay:0.9s;
		-ms-animation-delay:0.9s;
		-webkit-animation-delay:0.9s;
		-moz-animation-delay:0.9s;
}

#fountainG_4{
	left:88px;
	animation-delay:1.05s;
		-o-animation-delay:1.05s;
		-ms-animation-delay:1.05s;
		-webkit-animation-delay:1.05s;
		-moz-animation-delay:1.05s;
}

#fountainG_5{
	left:117px;
	animation-delay:1.2s;
		-o-animation-delay:1.2s;
		-ms-animation-delay:1.2s;
		-webkit-animation-delay:1.2s;
		-moz-animation-delay:1.2s;
}

#fountainG_6{
	left:146px;
	animation-delay:1.35s;
		-o-animation-delay:1.35s;
		-ms-animation-delay:1.35s;
		-webkit-animation-delay:1.35s;
		-moz-animation-delay:1.35s;
}

#fountainG_7{
	left:175px;
	animation-delay:1.5s;
		-o-animation-delay:1.5s;
		-ms-animation-delay:1.5s;
		-webkit-animation-delay:1.5s;
		-moz-animation-delay:1.5s;
}

#fountainG_8{
	left:205px;
	animation-delay:1.64s;
		-o-animation-delay:1.64s;
		-ms-animation-delay:1.64s;
		-webkit-animation-delay:1.64s;
		-moz-animation-delay:1.64s;
}



@keyframes bounce_fountainG{
	0%{
	transform:scale(1);
		background-color:rgb(153,250,211);
	}

	100%{
	transform:scale(.3);
		background-color:rgb(255,255,255);
	}
}

@-o-keyframes bounce_fountainG{
	0%{
	-o-transform:scale(1);
		background-color:rgb(153,250,211);
	}

	100%{
	-o-transform:scale(.3);
		background-color:rgb(255,255,255);
	}
}

@-ms-keyframes bounce_fountainG{
	0%{
	-ms-transform:scale(1);
		background-color:rgb(153,250,211);
	}

	100%{
	-ms-transform:scale(.3);
		background-color:rgb(255,255,255);
	}
}

@-webkit-keyframes bounce_fountainG{
	0%{
	-webkit-transform:scale(1);
		background-color:rgb(153,250,211);
	}

	100%{
	-webkit-transform:scale(.3);
		background-color:rgb(255,255,255);
	}
}

@-moz-keyframes bounce_fountainG{
	0%{
	-moz-transform:scale(1);
		background-color:rgb(153,250,211);
	}

	100%{
	-moz-transform:scale(.3);
		background-color:rgb(255,255,255);
	}
}
</style>
<script type="text/javascript" src="Common/tooltip.js">
    </script>
<script type="text/javascript" src="Common/jquery-1.3.2.min.js"></script>
 <script>
         $(document).ready(function(){
              text = '<div id="fountainG">' +
            '<div id="fountainG_1" class="fountainG"></div>' +
            '<div id="fountainG_2" class="fountainG"></div>' +
            '<div id="fountainG_3" class="fountainG"></div>' +
            '<div id="fountainG_4" class="fountainG"></div>' +
            '<div id="fountainG_5" class="fountainG"></div>' +
            '<div id="fountainG_6" class="fountainG"></div>' +
            '<div id="fountainG_7" class="fountainG"></div>' +
            '<div id="fountainG_8" class="fountainG"></div>' +
            '</div>';
    $("<table id='overlay'><tbody><tr><td>" + text + "</td></tr></tbody></table>").css({
        "position": "fixed",
        "top": "0px",
        "left": "0px",
        "width": "100%",
        "height": "100%",
        "background-color": "rgba(0,0,0,.5)",
        "z-index": "10000000000000000000000000000",
        "vertical-align": "middle",
        "text-align": "center",
        "color": "#fff",
        "font-size": "40px",
        "font-weight": "bold",
        "cursor": "wait"
    }).appendTo("body");
          
     });
        </script>
<!--<script src="https://www.paypalobjects.com/js/external/apdg.js">
</script>-->
<script src="https://www.paypalobjects.com/js/external/dg.js">
</script>
</head>

<body>	
	
					
<?php
$ack = strtoupper($response->responseEnvelope->ack);
if($ack != "SUCCESS") {

} else {

	$token = $response->payKey;
$_SESSION['pay_key']=$token;

        if($_POST['paypal_payment_set_value']==1){
            $payPalURL = PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $token;
        
       header('Location:'.$payPalURL);
         //  echo '<a id="payPalRedirect" href="https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?paykey='.$token.'&expType=mini">Complete PayPal Payment</a>';
        //$payPalURL = PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $token;
        
       //header('Location:'.$payPalURL);

//$payPalURL = PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $token;
      //  $payPalURL='https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?paykey='.$token.'&expType=mini';
      //  header('Location:'.$payPalURL);
      // header('Location:'.$payPalURL);
               // header('Location:https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?paykey='.$token.'&expType=mini');
        }else{
             $payUrl = 'https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?expType=light&paykey=' . $token;
            ?>
                        <script>
	document.addEventListener("DOMContentLoaded", function(event) { 
 loadPayPalPage('<?php echo $payUrl; ?>');
});</script>
       <?php }
	
}
//require_once 'Common/Response.php';
?>

    
   
       
<script>
    function loadPayPalPage(paypalURL)
    {
        var ua = navigator.userAgent;
        var pollingInterval = 0;
        var win;
        // mobile device
        if (ua.match(/iPhone|iPod|Android|Blackberry.*WebKit/i)) {
            //VERY IMPORTANT - You must use '_blank' and NOT name the window if you want it to work with chrome ios on iphone
                //See this bug report from google explaining the issue: https://code.google.com/p/chromium/issues/detail?id=136610
            win = window.open(paypalURL,'_blank');

            pollingInterval = setInterval(function() {
                if (win && win.closed) {
                    clearInterval(pollingInterval);
                    returnFromPayPal();
                }
            } , 1000);
        }
        else
        {
            //Desktop device
            var width = 400,
                height = 550,
                left,
                top;

            if (window.outerWidth) {
                left = Math.round((window.outerWidth - width) / 2) + window.screenX;
                top = Math.round((window.outerHeight - height) / 2) + window.screenY;
            } else if (window.screen.width) {
                left = Math.round((window.screen.width - width) / 2);
                top = Math.round((window.screen.height - height) / 2);
            }

            //VERY IMPORTANT - You must use '_blank' and NOT name the window if you want it to work with chrome ios on iphone
                //See this bug report from google explaining the issue: https://code.google.com/p/chromium/issues/detail?id=136610
            win = window.open(paypalURL,'_blank','top=' + top + ', left=' + left + ', width=' + width + ', height=' + height + ', location=0, status=0, toolbar=0, menubar=0, resizable=0, scrollbars=1');

            pollingInterval = setInterval(function() {
                if (win && win.closed) {
                    clearInterval(pollingInterval);
                    returnFromPayPal();
                }
            } , 1000);
        }
    }

    var returnFromPayPal = function()
    {
       location.replace("https://firsthumb.com/samples/WebflowReturnPage.php");
        // Here you would need to pass on the payKey to your server side handle (use session variable) to call the PaymentDetails API to make sure Payment has been successful
        // based on the payment status- redirect to your success or cancel/failed page
    }
</script>
    </body>
</html>

