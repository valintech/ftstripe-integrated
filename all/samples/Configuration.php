<?php 
class Configuration
{
	// For a full list of configuration parameters refer in wiki page (https://github.com/paypal/sdk-core-php/wiki/Configuring-the-SDK)
	public static function getConfig()
	{
		$config = array(
				// values: 'sandbox' for testing
				//		   'live' for production
                //         'tls' for testing if your server supports TLSv1.2
				"mode" => "sandbox"

                // TLSv1.2 Check: Comment the above line, and switch the mode to tls as shown below
                // "mode" => "tls"
	
				// These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
				// "http.ConnectionTimeOut" => "5000",
				// "http.Retry" => "2",
			);
		return $config;
	}
	
	// Creates a configuration array containing credentials and other required configuration parameters.
	public static function getAcctAndConfig()
	{
	if($_SESSION['payment_mode']==1){	
            $config = array(
				// Signature Credential
				"acct1.UserName" => "steve.dickson-facilitator-1_api1.firsthumb.com",
				"acct1.Password" => "85MTE59U7HPXNQB3",
				"acct1.Signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31AWy0WGhTM-f09keKjsTey5R1JMwX",
				"acct1.AppId" => "APP-80W284485P519543T"
				
				// Sample Certificate Credential
				// "acct1.UserName" => "certuser_biz_api1.paypal.com",
				// "acct1.Password" => "D6JNKKULHN3G5B8A",
				// Certificate path relative to config folder or absolute path in file system
				// "acct1.CertPath" => "cert_key.pem",
				// "acct1.AppId" => "APP-80W284485P519543T"
				);
      }
        else
        {
             $config = array(
				// Signature Credential
				"acct1.UserName" => "steve.dickson.micropayments-facilitator-2_api1.firsthumb.com",
				"acct1.Password" => "LE758YXUGEGD3R5N",
				"acct1.Signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31AEU3AW1yOk5X5EL.np3JCiwX29xq",
				"acct1.AppId" => "APP-80W284485P519543T"
				/*"acct1.UserName" => "steve.dickson.micropayments-facilitator-1_api1.firsthumb.com",
				"acct1.Password" => "EHEL3MNHGA3YSPZD",
				"acct1.Signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31AeyLykbTjpOmP90xYhrqcUFN6eOP",
				"acct1.AppId" => "APP-80W284485P519543T"*/
				// Sample Certificate Credential
				// "acct1.UserName" => "certuser_biz_api1.paypal.com",
				// "acct1.Password" => "D6JNKKULHN3G5B8A",
				// Certificate path relative to config folder or absolute path in file system
				// "acct1.CertPath" => "cert_key.pem",
				// "acct1.AppId" => "APP-80W284485P519543T"
				);
        }
		return array_merge($config, self::getConfig());
	}

}
